<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>

<section class="content-header">
    <div class="btn-group btn-breadcrumb">
            <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
            <a  class="btn btn-default  btn-xs active">Article</a>
        </div>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Article Payment</h3>
          <a style="float:right"  href="<?php echo base_url('index.php/Article/Article_payment_add');?>" class="btn btn-primary">
            <i class="glyphicon glyphicon-saved"></i>
            Add Article Payment
          </a>
        </div>
    <!-- /.box-header -->
        <div class="box-body">
        <div class="table-responsive">
          <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr >
                                        <th>ID</th>
                                        <th>Articel Name</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach($data as $u){
                                    ?>
                                    <tr  class='odd gradeX context'>
                                        <td><?php echo $u->id ?></td>
                                        <td><?php echo $u->articlepayment?></td>
                                        <td align="center">
                                          <a class="btn btn-warning btn-xs"   href="<?php echo base_url('index.php/Article/Article_payment_edit/'.$u->id); ?>">  <span class="fa fa-fw fa-edit" ></span> Edit</a>
                                        </td>
                                        <td align="center">
                                          <a class="btn btn-danger btn-xs"   href="<?php echo base_url('index.php/Article/Article_payment_delete/'.$u->id); ?>">  <span class="fa fa-fw fa-trash" ></span> Delete</a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
          </div>



        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->

</section><!-- /.content -->


<?php
$this->load->view('template/Js');
$this->load->view('template/Foot');
?>
<script>
  // $(function () {
  //   $("#example1").DataTable();
  //   $('#example2').DataTable({
  //     "paging": true,
  //     "lengthChange": false,
  //     "searching": false,
  //     "ordering": true,
  //     "info": true,
  //     "autoWidth": false
  //   });
  // });
</script>

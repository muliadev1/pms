
        <div class="table-responsive">
          <table id="example111" class="table table-bordered table-striped">
                                <thead>
                                    <tr >
                                        <th>Item Code</th>
                                        <th>Description</th>
                                        <th>Unit</th>
                                         <th>Price</th>
                                         <th>Qty</th>
                                         <th>Subtotal</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i=1; $TotalHutang = 0;
                                    foreach($data as $u){
                                     
                                    ?>
                                    <tr  class='odd gradeX context'>
                                        <td class="code"><?php echo $u->codeitem ?></td>
                                        <td class="desc"><?php echo $u->description?></td>
                                        <td class="desc"><?php echo $u->unit?></td>
                                        <td class="total" align="right"><?php echo number_format($u->price, 0, '.', '.')?></td>
                                        <td class="total" align="right"><?php echo $u->qty ?></td>
                                        <td class="total" align="right"><?php echo number_format($u->subtotal, 0, '.', '.')?></td> 
                                        

                                    </tr>
                                    <?php $i++; $TotalHutang += $u->tosubtotaltal; } ?>
                                </tbody>
                                
                            </table>
          </div>



        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->





<script type="text/javascript">
  function File_Kosong() {
  $.alert({
    title: 'Caution!!',
    content: 'Transaction Is Invalid!',
    icon: 'fa fa-warning',
    type: 'orange',
});
}
</script>

<script>
  $(function () {
    
    $('#example111').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
  });
</script>

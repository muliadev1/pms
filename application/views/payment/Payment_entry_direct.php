<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>
<script src="<?php echo base_url('assets/js/accounting.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/cleave.min.js') ?>" type="text/javascript"></script>
<style type="text/css">
.modal.modal-wide .modal-dialog {
  width: 60%;
}
.modal-wide .modal-body {
  overflow-y: auto;
}


 


#tallModal .modal-body p { margin-bottom: 900px }
.pull-right {
    float: right;
}

.pull-left {
    float: left;
}

select {
    width: 300px;
}

</style>
    <script type="text/javascript">
      jQuery(document).ready(function($) {
        new Cleave('.input-1', {
           numeral: true,
           numeralDecimalMark: ',',
           delimiter: '.'
      });
          });
    </script>
    <script type="text/javascript">
      function doMath()
      {
          var qty = document.getElementById('Qty1').value;
          document.getElementById('Qty').value =  qty.replace(/\./g, "");
      }
    </script>

<!-- Content Header (Page header) -->
<section class="content-header">
   <div class="btn-group btn-breadcrumb">
            <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
             <a href="<?php echo base_url('index.php/Purchase_requisition/purchase_requisition_view');?>" class="btn btn-default  btn-xs">Payment</a>
            <a  class="btn btn-default  btn-xs active">Payment Direct</a>
        </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
             <div class="box box-info">
              <form method="post" id="Simpan" action="<?php echo base_url(). 'index.php/Payment/payment_add_direct'; ?>" enctype="multipart/form-data">
                <div class="box-header">
                  <h3 class="box-title" ><strong>Payment Entry</strong> </h3>
                  <hr style="border-top: 1px solid #8c8b8b; padding: 1px; margin-top: 5px; margin-bottom: 0px;">
                </div>
                <div class="box-body">
              <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                          <label class="control-label">Date</label>
                          <input type="text" name="ReqDate" id="ReqDate" value="<?= date('d/m/Y') ?>" class="form-control">
                     </div>
                  </div>

                  
                </div>

              <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                       <label>Payment Method</label>
                     <select id="Method" class="form-control select2"  style="width: 100%;" onchange="gantiMethod()" name="Method">
                       <option value=""></option>
                       <option value="1"  >Cash</option>
                       <option value="2"  >Cheque</option>
                     </select>
                     <input type="hidden" name="NamaVendor" id="NamaVendor">
                   </div>
                 </div>
                 

                <div class="col-md-6">
                  <div class="form-group">
                     <label>Check No.</label>
                   <input  type="text" name="check" id="check"  class="form-control"> </input>
                 </div>
               </div>
              </div>
               <!-- hidden akun untuk pocer -->
            <div class="row">
              <div class="col-md-6" >
                   <div class="form-group">
                      <label>Akun</label>
                    <select id="IdVendor" class="form-control select2"  style="width: 100%;" name="Akun">
                      <option value="1101"></option>
                      <?php
                          foreach($dataakun as $akun){
                      ?>
                        <option value="<?php echo $akun->NoAkun; ?>" ><?php echo $akun->nama; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
              </div>
              <!-- hidden akun untuk pocer -->

              
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                     <label></label>
                  </div>
               </div>
             </div>

               <div class="row">
                  <div class="col-md-2">
                    <div class="form-group">
                          <label class="control-label">Invoice No.</label>
                          <input type="text" name="invoiceno" id="invoiceno"  class="form-control">
                     </div>
                  </div>
                  <div class="col-md-2">
                     <label>Invoice Date</label>
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text"  data-validation="date" data-validation-format="mm/dd/yyyy" data-validation-error-msg="Tanggal Lahir Belum Dipilih"  data-validation-require-leading-zero="false" class="form-control pull-right" name= "TglLahir" id="TglInvoice">
                      </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                          <label class="control-label">Description</label>
                          <input type="text" name="Description1" id="Description"  class="form-control">
                     </div>
                  </div>

                  <div class="col-md-3">
                    <div class="form-group">
                          <label class="control-label">Amount</label>
                          <input  type="text" name="amount" id="amount"  class="form-control input-1"  >

                        </div>
                  </div>

                  <div class="col-md-1">
                    <div class="form-group" >
                        <a style="margin-top:25px; margin-right:10px; " class="btn btn-primary btn-flat btn-xs Add"   ><i class='glyphicon glyphicon-arrow-down'></i>  Proses  </a>
                     </div>
                  </div>
                </div>
                <div class="row">
                <div class="col-md-6" >


                </div>


                </div>



                      <div class="table-responsive">

                         <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                <th >No</th>
                                    <th>Invoice No.</th>
                                    <th>Invoice Date</th>
                                    <th>Description</th>
                                    <th>Amount </th>
                                    <th >Action </th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                      </div>

                       <div class="col-md-12" align="left" style="margin-top :30px;">
                          <div class="form-group" >
                              <a   type="submit" class="btn btn-success btn-flat"  id="simpan1"  ><i class='fa fa-check-square-o'></i>  Simpan  </a>
                          </div>
                      </div>
        </form>
      </div>
     </div>
</div>

    <div class="modal modal-wide fade" id="myModal" role="dialog" style="display:none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" title="close" data-dismiss="modal">&times;</button>
                    <h4 align="center" class="modal-title">Invoice List</h4>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>



</section>



<?php
$this->load->view('template/Js');
$this->load->view('template/Foot');
?>

<script type="text/javascript">
  // $(document).ready(function(){
  //    $("#invoiceno").click(function(e){
  //      var IdVendor=$('#IdVendor').val();
  //     $.ajax({
  //       type: "POST",
  //      url: '<?php echo site_url('Payment/receive_history_unpaid_by_vendor');?>',
  //       data: {idvendor: IdVendor},
  //       success: function(data){
  //         //  alert(data);
  //           e.preventDefault();
  //           var mymodal = $('#myModal');
  //           var height = $(window).height() - 200;
  //           mymodal.find(".modal-body").css("max-height", height);
  //           mymodal.find('.modal-body').html(data);
  //           mymodal.modal('show');
  //             //  alert(data);
  //       }
  //     });
  //   });
  // });
</script>

<script type="text/javascript">
$('#IdVendor').on("select2:select", function(e) {
// what you would like to happen
  var IdVendor = $('#IdVendor').val();
 // alert(itemcode);
//  var kodepo = $('#kodepo').val();
  $.ajax({
    type: "POST",
    dataType:'json',
   url: '<?php echo site_url('Payment/select_vendor_by_id');?>',
    data: {id: IdVendor},
    success: function(data){
         $('#address').val(data[0].address);

    }
  });
});

function gantiMethod() {
  var id = $('#Method').val();
 //alert(id);
 $('#check').val('');
 if (id==1) {

   $('#check').attr('readonly','readonly');
 }else{

   $('#check').removeAttr('readonly');
 }
}
</script>

<script type="text/javascript">
  $(document).ready(function() {
    var t = $('#example1').DataTable();
    var i = 1;

    $('.Add').on( 'click', function () {
        var invoice = $('#invoiceno').val();
        var invoicedate = $('#TglInvoice').val();
        var amount = $('#amount').val();
      //  var amount1 = $('#amount1').val();
        var Description = $('#Description').val();

        var btndelete =  "<a class='btn btn-danger btn-xs' title='Remove Item'>  <span class=' fa fa-minus-circle' ></span> </a> "
       //  alert(i);

        if (invoice=='' ||invoice==null  ) {
         
         File_Kosong();
       }else if (amount.replace(/\./g, "")=='' || parseFloat(amount.replace(/\./g, ""))<=0) {
         //alert('joss');
         File_Kosong();
        }else{


       var row = t.row.add( [
            i,
            invoice,
            invoicedate,
            Description,
            amount,
            btndelete
        ] ).draw(false);
        t.row(row).column(0).nodes().to$().addClass('No');

        $('<input>').attr({
        type: 'hidden',
        class: i,
        name: 'invoice['+i+']',
        value: invoice
        }).appendTo('form');

        $('<input>').attr({
        type: 'hidden',
        class: i,
        name: 'invoicedate['+i+']',
        value: invoicedate
        }).appendTo('form');

         $('<input>').attr({
        type: 'hidden',
        class: i,
        name: 'Description['+i+']',
        value: Description
        }).appendTo('form');

        $('<input>').attr({
        type: 'hidden',
        class: i,
        name: 'amount['+i+']',
        value: amount
        }).appendTo('form');

        $('<input>').attr({
        type: 'hidden',
        class: i,
        name: 'amount1['+i+']',
        value: amount
        }).appendTo('form');

        i++;
         $('#invoice').val('');
         $('#invoicedate').val('');
         $('#amount').val('');
         $('#amount1').val('');
         $('#invoiceno').val('')
          $('#Description').val('')

    }
    });

} );
</script>

<script type="text/javascript">
$("#simpan1").click(function() {
        event.preventDefault();
        $.confirm({
          title: 'Confirmation',
          content: 'Are You Sure to Save?',
          buttons: {
              Simpan: function () {
                  $.LoadingOverlay("show");
                  $("#Simpan").submit();
              },
              Batal: function () {

                  $.alert('Data Tidak Disimpan...');
              },
          }
      });

});
</script>

<?php if($this->session->flashdata('kodepv')): ?>
<?php $kodepvcetak =  $this->session->flashdata('kodepv');?> 
<script type="text/javascript">
  $(document).ready(function() {
    var kodepvcetak = "<?php echo $kodepvcetak;?>"  
    window.open("<?php echo base_url(). 'index.php/Purchase_order/CetakPV/';?>?NoPay="+kodepvcetak ,"MyTargetWindowName")

  });
  </script>

  <?php endif; ?>



 <script>
  $('table').on('click', 'a', function(e){
    var t = $('#example1').DataTable();
    var id =$(this).closest('tr').children('td.No').text();
     $('.'+id).remove();
      t.row($(this).closest('tr')).remove().draw( false );
   //$(this).closest('tr').remove().draw(false);

})
  </script>

  <script type="text/javascript">
$('#example1').dataTable({
    "paging": false,
    "ordering": false,
    "searching": false
});
  </script>

 <script>
  $( function() {
    $( "#TglInvoice" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });
    


  });

</script>
  <script>
  $(function () {
    $(".select2").select2();
     $(".select3").select2();

  });
  </script>


<script type="text/javascript">
   $(document).on('change', '#IdDepartement', function(){
    var NamaDep =$("#IdDepartement option:selected").text();
    $("#NamaDepartemen").val('');
      $("#NamaDepartemen").val(NamaDep);
    });
</script>

   <script type="text/javascript">
  function File_Kosong() {
  $.alert({
    title: 'Caution!!',
    content: 'Invalid Data!',
    icon: 'fa fa-warning',
    type: 'orange',
});
}
</script>


<?php echo $header?>

<style type="text/css">
  .TabelKonten tr td {
    padding-right: 7px;
    padding-left:  7px;
    font-size: 11px;
  }
</style>
<table class="TabelKonten"  border="0" style="border-collapse: collapse; border-color:#000000; padding-bottom : 50px ;"  width="100%" >
<tr>
  <td colspan="2" align="center" style="font-size: 14px;"> <strong>ROOM REVENUE BREAKDOWN</strong></td>
</tr>
<tr>
  <td width="10%"> <strong>Date From</strong></td>
  <td>: <?php echo $periode['TglAwal'] ?></td>
</tr>
<tr>
  <td width="10%"> <strong>Date To</strong></td>
  <td>: <?php echo $periode['TglAkhir'] ?></td>
</tr>
</table>
<br>

<table class="TabelKonten"  border="1" style="border-collapse: collapse; border-color:#000000; padding-top : 50px;"  width="100%" >         
                                <thead>
                                     <tr style=" font-size: 8px;">
                                        <th style=" font-size: 11px;">NO</th>
                                        <th style=" font-size: 11px;">Room</th>
                                        <th style=" font-size: 11px;">Room Type</th>
                                        <th style=" font-size: 11px;">Remark</th>
                                        <th style=" font-size: 11px;">Pax</th>                         
                                        <th style=" font-size: 11px;">Rate</th>
                                       <!--  <th style=" font-size: 11px;">Breakfast</th> -->
                                        <th style=" font-size: 11px;">Room Only</th>
                                        <th style=" font-size: 11px;">Tax</th>
                                        <th style=" font-size: 11px;">Service</th>
                                        <th style=" font-size: 11px;">Room Nett</th>
                                        <th style=" font-size: 11px;">Guest</th>
                                        <th style=" font-size: 11px;">Arrival</th>
                                        <th style=" font-size: 11px;">Departure</th>
                                </thead>
                                <tbody>
                      <?php $no=1;$totalroom=0; $totaltax=0; $totalservice=0;$totalroomtaxservice =0;  foreach ($konten as $clo){  ?>
                                 <?php  $room = $clo->rate;
                                        $tax = 10/100*($clo->rate);
                                        $service =  ($tax+$clo->rate)*10/100 ;
                                        $roomtaxservice =  $room+ $tax +$service;
                                 ?> 
                                    <tr>
                                      <td><?php echo $no ?></td>
                                      <td><?php echo $clo->roomname ?></td>
                                      <td width="10%"><?php echo $clo->roomtype ?></td>
                                      <td><?php echo $clo->notes ?></td>
                                      <td><?php echo $clo->pax ?></td>
                                      <td align="right"><?php echo number_format($clo->rate, 2, ',', '.') ?></td>
                                      <!-- <td><?php echo $clo->articlerate ?></td> -->
                                      <td align="right"><?php echo number_format($roomtaxservice, 2, ',', '.') ?></td>
                                      <td align="right"><?php echo number_format($tax, 2, ',', '.') ?></td>
                                      <td align="right"><?php echo number_format($service, 2, ',', '.') ?></td>
                                      <td align="right"><?php echo number_format($room, 2, ',', '.') ?></td>
                                       <td><?php echo $clo->nama ?></td>
                                       <td><?php echo $clo->checkin ?></td>
                                       <td><?php echo $clo->checkout ?></td>
                                   </tr>
                                  
                                    
                                  <?php $no++;$totalroom+=$room;$totaltax+=$tax; $totalservice+=$service;  
                                  $totalroomtaxservice+=$roomtaxservice;} ?>
                                    
                                </tbody>
                                <tfoot>
                                  <tr style="background-color: #f0f0f0; font-weight: bold;">
                                    <td align="center"  colspan="5"><strong>TOTAL</strong></td>
                                     <td align="right"><strong><?php echo number_format($totalroomtaxservice, 2, ',', '.') ?></strong></td>
                                     <td align="right"><strong><?php echo number_format($totalroomtaxservice, 2, ',', '.') ?></strong></td>
                                     <td align="right"><strong><?php echo number_format($totaltax, 2, ',', '.') ?></strong></td>
                                     <td align="right"><strong><?php echo number_format($totalservice, 2, ',', '.') ?></strong></td>
                                     <td align="right"><strong><?php echo number_format($totalroom, 2, ',', '.') ?></strong></td>
                                      <td rowspan="2" colspan="3">&nbsp;</td>
                                  </tr>
                                   <tr style="background-color: #f0f0f0; font-weight: bold;">
                                <td align="center" colspan="5"><strong>AVERAGE</strong></td>
                                <td align="right"><strong><?php echo number_format($totalroomtaxservice/$no, 2, ',', '.') ?></strong></td>
                                <td align="right"><strong><?php echo number_format($totalroomtaxservice/$no, 2, ',', '.') ?></strong></td>
                                <td align="right"><strong><?php echo number_format($totaltax/$no, 2, ',', '.') ?></strong></td>
                                <td align="right"><strong><?php echo number_format($totalservice/$no, 2, ',', '.') ?></strong></td>
                                <td align="right"><strong><?php echo number_format($totalroom/$no, 2, ',', '.') ?></strong></td>
                                  </tr>
                                </tfoot>
                            </table>

         

<?php

class M_room extends CI_Model{

	function Room_view() {
		$data = $this->db->query("SELECT tbroom.`id`, tbroomtype.`roomtype`, tbroomstatus.`status`, tbroomstatus.`description`, roomname, tbroom.`desc`
															FROM tbroom INNER JOIN tbroomtype ON tbroom.`idroomtype` = tbroomtype.`id`
															INNER JOIN tbroomstatus ON tbroom.`idroomstatus` = tbroomstatus.`id` WHERE tbroom.`isaktif` = 1");
		return $data->result();
   }

	 function Room_addDB($table,$data) {
			 $this->db->insert($table,$data);
	}

	function Room_view_edit($id) {
	 $data = $this->db->query("SELECT tbroom.`id`, tbroomtype.`roomtype`, tbroomstatus.`status`, tbroomstatus.`description`, roomname, tbroom.`desc`, tbroom.`idroomtype`, tbroom.`idroomstatus`
											FROM tbroom INNER JOIN tbroomtype ON tbroom.`idroomtype` = tbroomtype.`id`
											INNER JOIN tbroomstatus ON tbroom.`idroomstatus` = tbroomstatus.`id` WHERE tbroom.`isaktif`=1 and tbroom.`id` = $id");
	 return $data->result();
	}

	function Room_editDB($table,$data,$id) {
 			 $this->db->where('Id',$id );
 			 $this->db->update($table,$data);
  }

	function Room_type() {
		$data = $this->db->query("SELECT id, roomtype FROM tbroomtype");
		return $data->result();
   }

	 function Room_status() {
 		$data = $this->db->query("SELECT id, status, description FROM tbroomstatus");
 		return $data->result();
    }

}
?>

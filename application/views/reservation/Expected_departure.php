<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>

<section class="content-header">
    <div class="btn-group btn-breadcrumb">
      <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
      <a  class="btn btn-default  btn-xs active">Expectd Derpature</a>
    </div>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-xs-12">
      <div class="box box-primary">
        <div class="box-header">
         <h3 class="box-title">Expected Derpature</h3>
         <div class="form-group">
         <br>
           <div class="row" style="background-color:#F2F2FF; margin-left:5px; margin-right:5px; margin-bottom:10px;">
             <div class="row">
               <div class="col-md-3">
                 <label style="padding-left: 15px;">From</label>
               </div>
             </div>
<!--              <form method="post" action="<?php echo base_url(). 'index.php/Reservation/ExpectedDeparture_view'; ?>">
 -->             
            
                <div class="row">
               <div class="col-md-3" >
                 <div class="form-group">
                   <div class="input-group date" style="padding-left: 15px;">
                     <div class="input-group-addon" style="background: #DDD;">
                       <i class="fa fa-calendar"></i>
                     </div>
                     <input type="text" value="<?= date('m/d/Y') ?>" class="form-control pull-right" name= "tglAwal" id="tglAwal">
                   </div>
                   <input type="hidden" value="<?php echo $tglAwal?>" id="Tawal" name="Takhir">
                 </div>
               </div>
               <div class="col-md-3">
                <div class="form-group">
                  <div class="input-group date">
                    <div class="input-group-addon" style="background: #DDD;">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" value="<?= date('m/d/Y') ?>" class="form-control pull-right" name= "tglAkhir" id="tglAkhir">
                  </div>
                  <input type="hidden" value="<?php echo $tglAkhir ?>" id="Takhir" name="Takhir">
                </div>
              </div>
               <div class="col-sm-1">
                <div class="form-group">
                  <a  class="btn btn-info Proses"><i class="fa fa-check-square"></i> Process</a>
                </div>
              </div>
              <div class="col-sm-1">
               <div class="form-group">
                <a class="btn btn-default ED"><i class='glyphicon glyphicon-print'></i> Print</a>
               </div>
             </div>
          </div>
        <!-- </form> -->
        </div>
       </div>
    <!-- /.box-header -->
        <div class="box-body">
        <div class="table-responsive" id="tabelkosong">
          <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr >
                                        <th >No</th>
                                        <th >R. Status</th>
                                        <th >#Room</th>
                                        <th >R. Type</th>
                                        <th >Guest Name</th>
                                        <th >Arrival</th>
                                        <th >Departure</th>
                                        <th >Night</th>
                                        <th >Nationality</th>
                                        <th >Segment</th>
                                        <th >Company</th>
                                        <th >Res. Notes</th>
                                        <th >Res. Message</th>
                                        <th >Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
          </div>
           <div id="tabelada" hidden="hidden">
           </div>



        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->

</section><!-- /.content -->


<?php
$this->load->view('template/Js');
$this->load->view('template/Foot');
?>


<script type="text/javascript">
    $(".Proses").click(function(e){
      var tglAwal = $( "#tglAwal" ).datepicker().val();
      var tglAkhir  = $( "#tglAkhir" ).datepicker().val();
        $.LoadingOverlay("show");
        
        $.ajax({
        type: "POST",
        dataType: 'html',
        url: '<?php echo site_url('Reservation/ExpectedDeparture_view_data');?>',
        data: {tglAwal: tglAwal, tglAkhir:tglAkhir},
        success: function(data){
            e.preventDefault();
            $('#tabelkosong').fadeOut(30);
             $('#tabelada').html(data);
             $('#tabelada').fadeIn(0,300);  
              $("#example2").DataTable();
            $.LoadingOverlay("hide");
        }
      });
    });
</script>

<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>

<script>
$( function() {
$( "#tglAwal" ).datepicker({
autoclose: true,
dateFormat: 'yy/mm/dd'
});
$( "#tglAkhir" ).datepicker({
autoclose: true,
dateFormat: 'yy/mm/dd'
});
});
</script>

<script type="text/javascript">
  $(function () {  
  $(".ED").click(function(){
      var TglAwal = $( "#tglAwal" ).val();
      var TglAkhir = $( "#tglAkhir" ).val();
      window.open("<?php echo base_url(). 'index.php/Reservation/CetakLapED/';?>?Status="+ 'Semua' +"&TglAwal="+ TglAwal+"&TglAkhir="+TglAkhir ,"MyTargetWindowName")
  }); 
});
</script>




<?php

class Reservation_status extends CI_Controller{

	function __construct(){
		parent::__construct();
		if($this->session->userdata('Kode_userPms')==NULL) {
           redirect('Login');
		}
		$this->load->library('session');
		$this->load->helper(array('form', 'url'));
		$this->load->model(array('M_reservationstatus'));
		date_default_timezone_set('Asia/Hong_Kong');
		
		if (!empty($this->session->userdata("active_db"))) {
			$this->load->database($this->session->userdata("active_db"), FALSE, TRUE);
		}
	}

	function index(){
		redirect('Reservation_status/ReservationStatus_view');
	}

	function ReservationStatus_view(){
		$data['data'] = $this->M_reservationstatus->ReservationStatus_view();
		$this->load->view('reservation_status/Reservation_status',$data);
	}

	function ReservationStatus_add(){
		$this->load->view('reservation_status/Reservation_status_add');
	}

	function ReservationStatus_addDB(){
		$data = array(
			'status' => $this->input->post('status'),
			'description' => $this->input->post('description'),
		);
		$this->M_reservationstatus->ReservationStatus_addDB('tbreservationstatus',$data) ;
		$this->session->set_flashdata('msg', 'Add Item Reservation Status successfully ...');
		redirect('reservation_status/ReservationStatus_view');
	}

	function ReservationStatus_edit($id){
		$data['data'] = $this->M_reservationstatus->ReservationStatus_view_edit($id) ;
		$this->load->view('reservation_status/Reservation_status_edit',$data);
	}

	function ReservationStatus_editDB(){
		$id = $this->input->post('Id');
		$data = array(
			'status' => $this->input->post('status'),
			'description' => $this->input->post('description'),
	);

		$this->M_reservationstatus->ReservationStatus_editDB('tbreservationstatus',$data,$id) ;
		$this->session->set_flashdata('msg', 'Edit Reservation Status successfully...');
		redirect('reservation_status/ReservationStatus_view');
	}

}
?>

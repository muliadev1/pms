.<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>
<script src="<?php echo base_url('assets/js/accounting.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/cleave.min.js') ?>" type="text/javascript"></script>



    <script type="text/javascript">
      function doMathPrice()
      {
          var price = document.getElementById('price1').value;
          price = price.replace(/\./g, "");
          document.getElementById('price').value =  price;
          price = parseFloat(price);
          var service = price * 0.1;
          var tax = (service + price) * 0.1;
          var finalprice = price + tax + service;
          // alert(finalprice);
          //document.getElementById('fp').value = finalprice;
          document.getElementById('tax1').value = tax.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
          document.getElementById('tax').value = tax;
          document.getElementById('service1').value = service.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
          document.getElementById('service').value = service;
          document.getElementById('fp1').value = finalprice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
          document.getElementById('fp').value = finalprice;
          //document.getElementById('price').value =  price;//.replace(/\./g, "");
          // total = document.getElementById('Total').value;
          // var discHitung = disc.replace(/\./g, "");
          //  var TotalSetDisc = parseFloat(total) - parseFloat(discHitung);
          //  document.getElementById('TotalSetDiskon').value = TotalSetDisc;
          //  document.getElementById('TotalSetDiskon1').value =  TotalSetDisc.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
      }
    </script>
    <script type="text/javascript">
      jQuery(document).ready(function($) {
        new Cleave('.input-1', {
           numeral: true,
           numeralDecimalMark: ',',
           delimiter: '.'
      });
        new Cleave('.input-2', {
           numeral: true,
           numeralDecimalMark: ',',
           delimiter: '.'
      });
        new Cleave('.input-3', {
           numeral: true,
           numeralDecimalMark: ',',
           delimiter: '.'
      });
        new Cleave('.input-4', {
           numeral: true,
           numeralDecimalMark: ',',
           delimiter: '.'
      });
          });
    </script>

<!-- Content Header (Page header) -->
<section class="content-header">
   <div class="btn-group btn-breadcrumb">
            <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
             <a href="<?php echo base_url('index.php/Menu/Menu_view');?>" class="btn btn-default  btn-xs">Menu</a>
            <a  class="btn btn-default  btn-xs active">Add Menu</a>
        </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
             <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Add New Menu</h3>
                </div>
                <div class="box-body">
                    <form method="post"  id="Simpan" action="<?php echo base_url().'index.php/Menu/Menu_addDB'; ?>">
                    <div class="form-group">
                      <label>Sub Category</label>
                      <select class="form-control select2" style="width: 100%;" name="idmenusubcategory">
                      <?php
                          foreach($data as $u){
                      ?>
                        <option value="<?php echo $u->id; ?>" ><?php echo $u->subcategory; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                    <div class="form-group">
                            <label class="control-label" for="menu">Menu</label>
                            <input type="text" name="menu" id="menu"  data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out menu..."  class="form-control"  placeholder="">
                    </div>
                    <div class="form-group">
                            <label class="control-label" for="description">Description</label>
                            <input type="text" name="description" id="description"  data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out description..."  class="form-control"  placeholder="">
                    </div>
                    <div class="form-group">
                            <label class="control-label" for="price1">Price</label>
                            <input type="text" name="price1" id="price1"  onkeyup="doMathPrice()" data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out price..."  class="form-control input-2"  placeholder="">
                            <input type="hidden" name="price" id="price" onkeyup="doMathPrice()" data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out price..."  class="form-control"  placeholder="">
                  </div>
                  <div class="form-group">
                          <label class="control-label" for="service1">Service</label>
                          <input readonly type="text" name="service1" id="service1" data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out price..."  class="form-control input-1"  placeholder="">
                          <input type="hidden" name="service" id="service" data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out price..."  class="form-control"  placeholder="">
                  </div>
                    <div class="form-group">
                            <label class="control-label" for="tax1">Tax</label>
                            <input readonly type="text" name="tax1" id="tax1" autocomplete="off" data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out price..."  class="form-control input-3" >
                            <input type="hidden" name="tax" id="tax" autocomplete="off" data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out price..."  class="form-control" >
                    </div>
                    <div class="form-group">
                            <label class="control-label" for="fp1">Final Price</label>
                            <input readonly type="text" name="fp1" id="fp1" autocomplete="off" data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out price..."  class="form-control input-4" >
                            <input type="hidden" name="fp" id="fp" autocomplete="off" data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out price..."  class="form-control" >
                    </div>

                    <!-- <div class="form-group">
                            <label class="control-label" for="fp">Final Price</label>
                            <input readonly type="text" name="fp" id="fp" autocomplete="off" data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out price..."  class="form-control input-1" >

                    </div> -->

                   <div class="form-group">
                      <button type="submit" value="Validate" class="btn btn-default"><i class='glyphicon glyphicon-ok'></i> Save</button>
                    </div>
                    </form>
                </div>
               </div>
         </div>
    </div>


</section>



<script type="text/javascript">
$("#Simpan").submit(function() {
    var price = $('#price').val();
     var description = $('#description').val();
        if (description == ''|| price==''){
            File_Kosong(); return false;
        }else{
        event.preventDefault();
        $.confirm({
          title: 'Confirmation',
          content: 'Are You Sure to Save?',
           type: 'blue',
          buttons: {
              Simpan: function () {
                  $.LoadingOverlay("show");
                  $("#Simpan").submit();
              },
              Batal: function () {

                  $.alert('Data Tidak Disimpan...');
              },
          }
      });
    }

});
</script>

<?php
$this->load->view('template/Js');
$this->load->view('template/Foot');
?>



<script type="text/javascript">
  function File_Kosong() {
  $.alert({
    title: 'Caution!!',
    content: 'Transaction Is Invalid!',
    icon: 'fa fa-warning',
    type: 'orange',
});
}
</script>

<script>
  $(function () {
    $(".select2").select2();
  });
  </script>

<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>

<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="btn-group btn-breadcrumb">
   <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
   <a href="<?php echo base_url('index.php/Segment/Segment_view');?>" class="btn btn-default  btn-xs">Segment</a>
   <a  class="btn btn-default  btn-xs active">Edit Segment</a>
  </div>
</section>

<!-- Main content -->
<section class="content">
 <div class="row">
  <div class="col-md-12">
   <div class="box box-info">
     <div class="box-header">
      <h3 class="box-title">Edit Item Segment</h3>
     </div>
     <div class="box-body">
      <form method="post" id="Simpan" action="<?php echo base_url(). 'index.php/Segment/Segment_editDB'; ?>">

      <div class="form-group">
       <label class="control-label">Segment</label>
       <input type="text" name="segment" id="segment" value="<?php echo $data[0]->segment; ?>"  data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out firstname..."  class="form-control"  placeholder="">
       <input type="hidden" name="Id"  value="<?php echo $data[0]->id; ?>" class="form-control"  placeholder="">
      </div>

       <div class="form-group">
        <button type="submit" value="Validate" class="btn btn-default"><i class='glyphicon glyphicon-ok'></i> Save</button>
       </div>
      </form>
     </div>
    </div>
   </div>
  </div>
</section>

<script type="text/javascript">
$("#Simpan").submit(function() {
     var segment = $('#segment').val();
        if (segment == ''){
            File_Kosong(); return false;
        }else{
        event.preventDefault();
        $.confirm({
          title: 'Confirmation',
          content: 'Are You Sure to Save?',
           type: 'blue',
          buttons: {
              Save: function () {
                  $.LoadingOverlay("show");
                  $("#Simpan").submit();
              },
              Cancel: function () {

                  $.alert('Data Not Saved...');
              },
          }
      });
    }

});
</script>

<?php
$this->load->view('template/Foot');
$this->load->view('template/Js');
?>


<script>

  $.validate({
    modules : 'location, date, file',
    onModulesLoaded : function() {
      $('#country').suggestCountry();
    }
  });

  // Restrict presentation length
  $('#presentation').restrictLength( $('#pres-max-length') );

</script>
<script type="text/javascript">
  function File_Kosong() {
  $.alert({
    title: 'Caution!!',
    content: 'Edit Data Invalid!',
    icon: 'fa fa-warning',
    type: 'orange',
});
}
</script>


<?php 

class M_bidang_studi extends CI_Model{	



	function item_view() {   
		$this->db->select("*");
		$this->db->from('tbitem');	
		return $this->db->get()->result();     
       
   } 

  function pr_view() {   
    $query = $this->db->query("SELECT kodepr,departement,date_format(dateadd,'%d-%m-%Y') as dateadd ,description,idoperator,nama_user FROM tbpr 
                            INNER JOIN tb_user ON tbpr.idoperator = tb_user.id_user"); 
    return $query->result()    
       
   } 

   

       function get_kodepr($table) { 
        $tahun = date('y'); 
        $bulan = date('m'); 
        $q = $this->db->query("SELECT MAX(RIGHT(kodepr,4)) AS idmax FROM ".$table);
        $kd = ""; //kode awal
        if($q->num_rows()>0){ //jika data ada
            foreach($q->result() as $k){
                $tmp = ((int)$k->idmax)+1; //string kode diset ke integer dan ditambahkan 1 dari kode terakhir
                $kd = "0000".$tmp; //kode ambil 4 karakter terakhir
                $id = substr($kd, -5);
            }
        }else{ //jika data kosong diset ke kode awal
            $kd = $tahun.$bulan."00001";
        }
        $kar = "PR"; //karakter depan kodenya
        //gabungkan string dengan kode yang telah dibuat tadi
        return $kar.$tahun.$bulan.$id;
   }


   function Pr_add($data,$dataDetil) { 
       $this->db->trans_strict(FALSE);
       $this->db->trans_start();  

   		$this->db->insert( 'tbpr', $data);
 		$this->db->insert_batch( 'tbprdetil', $dataDetil);  

 		
 		$this->db->trans_complete();
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else{
            $this->db->trans_commit();
        }      
   } 

  

  


  


      



}

        <div class="table-responsive">
          <table id="example11" class="table table-bordered table-striped">
                                <thead>
                                    <tr >
                                        <th>Date</th>
                                        <th>Description</th>
                                        <th>Unit</th>
                                        <th>Old Price (IDR)</th>
                                        <th>New Price (IDR)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach($data as $u){

                                    ?>
                                    <tr  class='odd gradeX context'>
                                        <td><?php echo $u->tgl ?></td>
                                        <td><?php echo $u->description?></td>
                                        <td><?php echo $u->unit?></td>
                                        <td><?php echo number_format($u->price, 0, '.', '.') ?></td>
                                        <td><?php echo number_format($u->newprice, 0, '.', '.') ?></td>
                                        
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
 
<script>
  $(function () {
    $("#example11").DataTable();
  });
</script>




        <div class="table-responsive">
          <table id="example14" class="table table-bordered table-striped">
                                <thead>
                                    <tr >
                                      <th >No</th>
                                       <th >Description</th>
                                        <th >Unit</th>
                                        <th >Price</th>
                                        <th >Qty Order</th>
                                        <th >Qty Receive</th>                         
                                        <th >Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i=1;$total = 0;
                                    foreach($konten as $u){
                                      //codeaset,description, baik,kurangbaik, $set as masuk, out
                                    ?>
                                    <tr  class='odd gradeX context'>
                                       <td ><?php echo $i ?></td> 
                                        <td ><?php echo $u->description ?></td>
                                        <td ><?php echo $u->unit?></td>
                                        <td ><?php echo number_format($u->price, 0, '.', '.')?></td>
                <td align="right" style="background-color: #8CFF8C"><?php echo number_format($u->qtyorder, 0, '.', '.')?></td>
                <td align="right" style="background-color: #70B8FF"><?php echo number_format($u->qty, 0, '.', '.')?></td>
                 <td align="right" ><?php echo number_format($u->subtotal, 0, '.', '.')?></td>
                                    </tr>
                                    <?php $i++;  $total+=$u->subtotal;} ?>
                                </tbody>
                                <tfoot>
                                  <tr>
                                    <td colspan="6" style="background-color: #F0F0F0">TOTAL</td>
        <td align="right" style="background-color: #F0F0F0"><strong><?php echo number_format($total, 0, '.', '.')?></strong></td>
                                    </tr>
                                </tfoot>
                            </table>
          </div>



        </div><!-- /.box-body -->
        </form>
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->







<script>
  $(function () {
    
    $('#example14').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
  });
</script>

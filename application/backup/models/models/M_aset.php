<?php

class M_aset extends CI_Model{

	function getkodeaset() { 
        $q = $this->db->query("SELECT MAX(RIGHT(codeaset,5)) AS idmax FROM tbaset");
        $kd = ""; //kode awal
        if($q->num_rows()>0){ //jika data ada
            foreach($q->result() as $k){
                $tmp = ((int)$k->idmax)+1; //string kode diset ke integer dan ditambahkan 1 dari kode terakhir
                $kd = "0000".$tmp; //kode ambil 4 karakter terakhir
                $id = substr($kd, -5);
            }
        }else{ //jika data kosong diset ke kode awal
            $kd = "00001";
        }
        //$kar = "I"; //karakter depan kodenya
        //gabungkan string dengan kode yang telah dibuat tadi
        return $id;
   } 

	function Aset_view($Istok, $set,$baik, $kurangbaik,$out,$used,$lose,$idsub,$idaset) {
	if (empty($idsub)) {
	
		if (!empty($Istok)) {
			$data = $this->db->query("SELECT tbitem.codeitem,codeaset,tbitem.description,tbsubcategory.subcategory,$baik as baik,$kurangbaik as kurangbaik, $set as masuk, $out as keluar, $used as used ,$lose as lose  FROM tbaset
								 INNER JOIN tbitem ON tbaset.`codeitem` =  tbitem.`codeitem`
								 INNER JOIN tbsubcategory ON tbitem.`idsubcategory` =  tbsubcategory.`idsubcategory`
								 WHERE tbaset.`isaktif` =1  and $Istok=1  and idcategory= '$idaset' ");
		}else{
			$data = $this->db->query("SELECT tbitem.codeitem,codeaset,tbitem.description,tbsubcategory.subcategory,$baik as baik,$kurangbaik as kurangbaik, $set as masuk, $out as keluar,$used as used ,$lose as lose  FROM tbaset 
								INNER JOIN tbitem ON tbaset.`codeitem` =  tbitem.`codeitem` 
								 INNER JOIN tbsubcategory ON tbitem.`idsubcategory` =  tbsubcategory.`idsubcategory`
								WHERE tbaset.`isaktif` =1   and idcategory= '$idaset' ");
		};
	}else{
		if (!empty($Istok)) {
			$data = $this->db->query("SELECT tbitem.codeitem,codeaset,tbitem.description,$baik as baik,$kurangbaik as kurangbaik, $set as masuk, $out as keluar,$used as used ,$lose as lose  FROM tbaset
								 INNER JOIN tbitem ON tbaset.`codeitem` =  tbitem.`codeitem`
								 INNER JOIN tbsubcategory ON tbitem.`idsubcategory` =  tbsubcategory.`idsubcategory`
								 WHERE tbaset.`isaktif` =1  and $Istok=1 and tbitem.idsubcategory='$idsub' and idcategory= '$idaset' ");
		}else{
			$data = $this->db->query("SELECT tbitem.codeitem,codeaset,tbitem.description,$baik as baik,$kurangbaik as kurangbaik, $set as masuk, $out as keluar,$used as used ,$lose as lose  FROM tbaset 
								INNER JOIN tbitem ON tbaset.`codeitem` =  tbitem.`codeitem` 
								 INNER JOIN tbsubcategory ON tbitem.`idsubcategory` =  tbsubcategory.`idsubcategory`
								WHERE tbaset.`isaktif` =1    and tbitem.idsubcategory='$idsub' and idcategory= '$idaset' ");
		};

	}
		
		return $data->result();

   }
   function Aset_view_detil($codeaset,$iddepartement) {
			$data = $this->db->query(" SELECT tbasetdetil.`codeaset`, tbitem.`description`, kondisiawal, kondisiakhir, qty,note,DATE_FORMAT(tbasetdetil.`inputdate`,'%d-%m-%Y') 
										AS inputdate   FROM `tbaset` INNER JOIN `tbitem`
										ON tbaset.`codeitem` =  tbitem.`codeitem` INNER JOIN tbasetdetil
										ON tbaset.`codeaset` =  tbasetdetil.codeaset 
										WHERE tbasetdetil.`codeaset` = '$codeaset' AND iddepartement='$iddepartement'
										ORDER BY tbasetdetil.`inputdate` ASC");
			
		return $data->result();

   }
  

	 function Aset_addDB($table,$data) {
			 $this->db->insert($table,$data);
	}

	function Add_detilAset($table,$data,$Departement) {

       //`baik``kurangbaik``out``jumlah````````jumlahHK``baikFO``kurangbaikFO``outFO`
		//`jumlahFO``baikFB``kurangbaikFB``outFB``jumlahFB````````jumlahBO``baikEng`
		//`kurangbaikEng``outEng``jumlahEng``baikPG``kurangbaikPG``outPG``jumlahPG``baikSec``kurangbaikSec`
		//`outSec``jumlahSec``baikBar``kurangbaikBar``outBar``jumlahBar`
	foreach ($data as $row) {
	 if ($Departement==1) {
		  if ($row['kondisiawal']=='Baik') {
			 $kondisiawal = 'baikBO';
		  }elseif ($row['kondisiawal']=='Kurang Baik') {
		  	 $kondisiawal = 'kurangbaikBO';
		  }elseif ($row['kondisiawal']=='Used') {
		  	 $kondisiawal = 'usedBO';
		  }elseif ($row['kondisiawal']=='Lose') {
		  	 $kondisiawal = 'loseBO';
		  }else{
		  	 $kondisiawal = 'outBO';
		  }
		  if ($row['kondisiakhir']=='Baik') {
			 $kondisiakhir = 'baikBO';
		  }elseif ($row['kondisiakhir']=='Kurang Baik') {
		  	 $kondisiakhir = 'kurangbaikBO';
		  }elseif ($row['kondisiakhir']=='Used') {
		  	 $kondisiakhir = 'usedBO';
		  }elseif ($row['kondisiakhir']=='Lose') {
		  	 $kondisiakhir = 'loseBO';
		  }else{
		  	 $kondisiakhir= 'outBO';
		  }

       }elseif ($Departement==2) {
         if ($row['kondisiawal']=='Baik') {
			 $kondisiawal = 'baikHK';
		  }elseif ($row['kondisiawal']=='Kurang Baik') {
		  	 $kondisiawal = 'kurangbaikHK';
		  }elseif ($row['kondisiawal']=='Used') {
		  	 $kondisiawal = 'usedHK';
		  }elseif ($row['kondisiawal']=='Lose') {
		  	 $kondisiawal = 'loseHK';
		  }else{
		  	 $kondisiawal = 'outHK';
		  }
		  if ($row['kondisiakhir']=='Baik') {
			 $kondisiakhir = 'baikHK';
		  }elseif ($row['kondisiakhir']=='Kurang Baik') {
		  	 $kondisiakhir = 'kurangbaikHK';
		  }elseif ($row['kondisiakhir']=='Used') {
		  	 $kondisiakhir = 'usedHK';
		  }elseif ($row['kondisiakhir']=='Lose') {
		  	 $kondisiakhir = 'loseHK';
		  }else{
		  	 $kondisiakhir= 'outHK';
		  }

       }elseif ($Departement==3) {
        if ($row['kondisiawal']=='Baik') {
			 $kondisiawal = 'baikFO';
		  }elseif ($row['kondisiawal']=='Kurang Baik') {
		  	 $kondisiawal = 'kurangbaikFO';
		   }elseif ($row['kondisiawal']=='Used') {
		  	 $kondisiawal = 'usedFO';
		  }elseif ($row['kondisiawal']=='Lose') {
		  	 $kondisiawal = 'loseFO';
		  }else{
		  	 $kondisiawal = 'outFO';
		  }
		  if ($row['kondisiakhir']=='Baik') {
			 $kondisiakhir = 'baikFO';
		  }elseif ($row['kondisiakhir']=='Kurang Baik') {
		  	 $kondisiakhir = 'kurangbaikFO';
		  }elseif ($row['kondisiakhir']=='Used') {
		  	 $kondisiakhir = 'usedFO';
		  }elseif ($row['kondisiakhir']=='Lose') {
		  	 $kondisiakhir = 'loseFO';
		  }else{
		  	 $kondisiakhir= 'outFO';
		  }
       }elseif ($Departement==4) {
         if ($row['kondisiawal']=='Baik') {
			 $kondisiawal = 'baikFB';
		  }elseif ($row['kondisiawal']=='Kurang Baik') {
		  	 $kondisiawal = 'kurangbaikFB';
		  }elseif ($row['kondisiawal']=='Used') {
		  	 $kondisiawal = 'usedFB';
		  }elseif ($row['kondisiawal']=='Lose') {
		  	 $kondisiawal = 'loseFB';
		  }else{
		  	 $kondisiawal = 'outFB';
		  }
		  if ($row['kondisiakhir']=='Baik') {
			 $kondisiakhir = 'baikFB';
		  }elseif ($row['kondisiakhir']=='Kurang Baik') {
		  	 $kondisiakhir = 'kurangbaikFB';
		  }elseif ($row['kondisiakhir']=='Used') {
		  	 $kondisiakhir = 'usedFB';
		  }elseif ($row['kondisiakhir']=='Lose') {
		  	 $kondisiakhir = 'loseFB';
		  }else{
		  	 $kondisiakhir= 'outFB';
		  }
       }elseif ($Departement==5) {
        if ($row['kondisiawal']=='Baik') {
			 $kondisiawal = 'baikBO';
		  }elseif ($row['kondisiawal']=='Kurang Baik') {
		  	 $kondisiawal = 'kurangbaikBO';
		  }elseif ($row['kondisiawal']=='Used') {
		  	 $kondisiawal = 'usedBO';
		  }elseif ($row['kondisiawal']=='Lose') {
		  	 $kondisiawal = 'loseBO';
		  }else{
		  	 $kondisiawal = 'outBO';
		  }
		  if ($row['kondisiakhir']=='Baik') {
			 $kondisiakhir = 'baikBO';
		  }elseif ($row['kondisiakhir']=='Kurang Baik') {
		  	 $kondisiakhir = 'kurangbaikBO';
		  }elseif ($row['kondisiakhir']=='Used') {
		  	 $kondisiakhir = 'usedBO';
		  }elseif ($row['kondisiakhir']=='Lose') {
		  	 $kondisiakhir = 'loseBO';
		  }else{
		  	 $kondisiakhir= 'outBO';
		  }
       }elseif ($Departement==6) {
         if ($row['kondisiawal']=='Baik') {
			 $kondisiawal = 'baikBar';
		  }elseif ($row['kondisiawal']=='Kurang Baik') {
		  	 $kondisiawal = 'kurangbaikBar';
		  }elseif ($row['kondisiawal']=='Used') {
		  	 $kondisiawal = 'usedBar';
		  }elseif ($row['kondisiawal']=='Lose') {
		  	 $kondisiawal = 'loseBar';
		  }else{
		  	 $kondisiawal = 'outBar';
		  }
		  if ($row['kondisiakhir']=='Baik') {
			 $kondisiakhir = 'baikBar';
		  }elseif ($row['kondisiakhir']=='Kurang Baik') {
		  	 $kondisiakhir = 'kurangbaikBar';
		  }elseif ($row['kondisiakhir']=='Used') {
		  	 $kondisiakhir = 'usedBar';
		  }elseif ($row['kondisiakhir']=='Lose') {
		  	 $kondisiakhir = 'loseBar';
		  }else{
		  	 $kondisiakhir= 'outBar';
		  }
      }elseif ($Departement==7) {
         if ($row['kondisiawal']=='Baik') {
			 $kondisiawal = 'baikEng';
		  }elseif ($row['kondisiawal']=='Kurang Baik') {
		  	 $kondisiawal = 'kurangbaikEng';
		  }elseif ($row['kondisiawal']=='Used') {
		  	 $kondisiawal = 'usedEng';
		  }elseif ($row['kondisiawal']=='Lose') {
		  	 $kondisiawal = 'loseEng';
		  }else{
		  	 $kondisiawal = 'outEng';
		  }
		  if ($row['kondisiakhir']=='Baik') {
			 $kondisiakhir = 'baikEng';
		  }elseif ($row['kondisiakhir']=='Kurang Baik') {
		  	 $kondisiakhir = 'kurangbaikEng';
		  }elseif ($row['kondisiakhir']=='Used') {
		  	 $kondisiakhir = 'usedEng';
		  }elseif ($row['kondisiakhir']=='Lose') {
		  	 $kondisiakhir = 'loseEng';
		  }else{
		  	 $kondisiakhir= 'outEng';
		  }
      }elseif ($Departement==8) {
        if ($row['kondisiawal']=='Baik') {
			 $kondisiawal = 'baikPG';
		  }elseif ($row['kondisiawal']=='Kurang Baik') {
		  	 $kondisiawal = 'kurangbaikPG';
		  }elseif ($row['kondisiawal']=='Used') {
		  	 $kondisiawal = 'usedPG';
		  }elseif ($row['kondisiawal']=='Lose') {
		  	 $kondisiawal = 'losePG';
		  }else{
		  	 $kondisiawal = 'outPG';
		  }
		  if ($row['kondisiakhir']=='Baik') {
			 $kondisiakhir = 'baikPG';
		  }elseif ($row['kondisiakhir']=='Kurang Baik') {
		  	 $kondisiakhir = 'kurangbaikPG';
		  }elseif ($row['kondisiakhir']=='Used') {
		  	 $kondisiakhir = 'usedPG';
		  }elseif ($row['kondisiakhir']=='Lose') {
		  	 $kondisiakhir = 'losePG';
		  }else{
		  	 $kondisiakhir= 'outPG';
		  }
      }elseif ($Departement==9) {
       if ($row['kondisiawal']=='Baik') {
			 $kondisiawal = 'baikSec';
		  }elseif ($row['kondisiawal']=='Kurang Baik') {
		  	 $kondisiawal = 'kurangbaikSec';
		  }elseif ($row['kondisiawal']=='Used') {
		  	 $kondisiawal = 'usedSec';
		  }elseif ($row['kondisiawal']=='Lose') {
		  	 $kondisiawal = 'loseSec';
		  }else{
		  	 $kondisiawal = 'outSec';
		  }
		  if ($row['kondisiakhir']=='Baik') {
			 $kondisiakhir = 'baikSec';
		  }elseif ($row['kondisiakhir']=='Kurang Baik') {
		  	 $kondisiakhir = 'kurangbaikSec';
		  }elseif ($row['kondisiakhir']=='Used') {
		  	 $kondisiakhir = 'usedSec';
		  }elseif ($row['kondisiakhir']=='Lose') {
		  	 $kondisiakhir = 'loseSec';
		  }else{
		  	 $kondisiakhir= 'outSec';
		  }
       }else{
         if ($row['kondisiawal']=='Baik') {
			 $kondisiawal = 'baik';
		  }elseif ($row['kondisiawal']=='Kurang Baik') {
		  	 $kondisiawal = 'kurangbaik';
		  }elseif ($row['kondisiawal']=='Used') {
		  	 $kondisiawal = 'used';
		  }elseif ($row['kondisiawal']=='Lose') {
		  	 $kondisiawal = 'lose';
		  }else{
		  	 $kondisiawal = 'out';
		  }
		  if ($row['kondisiakhir']=='Baik') {
			 $kondisiakhir = 'baik';
		  }elseif ($row['kondisiakhir']=='Kurang Baik') {
		  	 $kondisiakhir = 'kurangbaik';
		  }elseif ($row['kondisiakhir']=='Used') {
		  	 $kondisiakhir = 'used';
		  }elseif ($row['kondisiakhir']=='Lose') {
		  	 $kondisiakhir = 'lose';
		  }else{
		  	 $kondisiakhir= 'out';
		  }
       }
		       
	       //menambahkan ksondisi awal dengan qty
	        $this->db->set($kondisiawal, $kondisiawal.'-'.$row['qty'].'', FALSE);
	        $this->db->where('codeaset',$row['codeaset'] );
	        $this->db->update('tbaset');

			//Mengurangi ksondisi akhir dengan qty
	        $this->db->set($kondisiakhir, $kondisiakhir.'+'.$row['qty'].'', FALSE);
	        $this->db->where('codeaset',$row['codeaset'] );
	        $this->db->update('tbaset');
    }
			 $this->db->insert_batch($table,$data);
}


	function Add_detilAset_byorder($table,$data,$Departement) {

       //`baik``kurangbaik``out``jumlah````````jumlahHK``baikFO``kurangbaikFO``outFO`
		//`jumlahFO``baikFB``kurangbaikFB``outFB``jumlahFB````````jumlahBO``baikEng`
		//`kurangbaikEng``outEng``jumlahEng``baikPG``kurangbaikPG``outPG``jumlahPG``baikSec``kurangbaikSec`
		//`outSec``jumlahSec``baikBar``kurangbaikBar``outBar``jumlahBar`
	foreach ($data as $row) {
	 if ($Departement==1) {
		  if ($row['kondisiawal']=='Baik') {
			 $kondisiawal = 'baikBO';
		  }elseif ($row['kondisiawal']=='Kurang Baik') {
		  	 $kondisiawal = 'kurangbaikBO';
		  }else{
		  	 $kondisiawal = 'outBO';
		  }
		  if ($row['kondisiakhir']=='Baik') {
			 $kondisiakhir = 'baikBO';
		  }elseif ($row['kondisiakhir']=='Kurang Baik') {
		  	 $kondisiakhir = 'kurangbaikBO';
		  }else{
		  	 $kondisiakhir= 'outBO';
		  }

       }elseif ($Departement==2) {
         if ($row['kondisiawal']=='Baik') {
			 $kondisiawal = 'baikHK';
		  }elseif ($row['kondisiawal']=='Kurang Baik') {
		  	 $kondisiawal = 'kurangbaikHK';
		  }else{
		  	 $kondisiawal = 'outHK';
		  }
		  if ($row['kondisiakhir']=='Baik') {
			 $kondisiakhir = 'baikHK';
		  }elseif ($row['kondisiakhir']=='Kurang Baik') {
		  	 $kondisiakhir = 'kurangbaikHK';
		  }else{
		  	 $kondisiakhir= 'outHK';
		  }

       }elseif ($Departement==3) {
        if ($row['kondisiawal']=='Baik') {
			 $kondisiawal = 'baikFO';
		  }elseif ($row['kondisiawal']=='Kurang Baik') {
		  	 $kondisiawal = 'kurangbaikFO';
		  }else{
		  	 $kondisiawal = 'outFO';
		  }
		  if ($row['kondisiakhir']=='Baik') {
			 $kondisiakhir = 'baikFO';
		  }elseif ($row['kondisiakhir']=='Kurang Baik') {
		  	 $kondisiakhir = 'kurangbaikFO';
		  }else{
		  	 $kondisiakhir= 'outFO';
		  }
       }elseif ($Departement==4) {
         if ($row['kondisiawal']=='Baik') {
			 $kondisiawal = 'baikFB';
		  }elseif ($row['kondisiawal']=='Kurang Baik') {
		  	 $kondisiawal = 'kurangbaikFB';
		  }else{
		  	 $kondisiawal = 'outFB';
		  }
		  if ($row['kondisiakhir']=='Baik') {
			 $kondisiakhir = 'baikFB';
		  }elseif ($row['kondisiakhir']=='Kurang Baik') {
		  	 $kondisiakhir = 'kurangbaikFB';
		  }else{
		  	 $kondisiakhir= 'outFB';
		  }
       }elseif ($Departement==5) {
        if ($row['kondisiawal']=='Baik') {
			 $kondisiawal = 'baikBO';
		  }elseif ($row['kondisiawal']=='Kurang Baik') {
		  	 $kondisiawal = 'kurangbaikBO';
		  }else{
		  	 $kondisiawal = 'outBO';
		  }
		  if ($row['kondisiakhir']=='Baik') {
			 $kondisiakhir = 'baikBO';
		  }elseif ($row['kondisiakhir']=='Kurang Baik') {
		  	 $kondisiakhir = 'kurangbaikBO';
		  }else{
		  	 $kondisiakhir= 'outBO';
		  }
       }elseif ($Departement==6) {
         if ($row['kondisiawal']=='Baik') {
			 $kondisiawal = 'baikBar';
		  }elseif ($row['kondisiawal']=='Kurang Baik') {
		  	 $kondisiawal = 'kurangbaikBar';
		  }else{
		  	 $kondisiawal = 'outBar';
		  }
		  if ($row['kondisiakhir']=='Baik') {
			 $kondisiakhir = 'baikBar';
		  }elseif ($row['kondisiakhir']=='Kurang Baik') {
		  	 $kondisiakhir = 'kurangbaikBar';
		  }else{
		  	 $kondisiakhir= 'outBar';
		  }
      }elseif ($Departement==7) {
         if ($row['kondisiawal']=='Baik') {
			 $kondisiawal = 'baikEng';
		  }elseif ($row['kondisiawal']=='Kurang Baik') {
		  	 $kondisiawal = 'kurangbaikEng';
		  }else{
		  	 $kondisiawal = 'outEng';
		  }
		  if ($row['kondisiakhir']=='Baik') {
			 $kondisiakhir = 'baikEng';
		  }elseif ($row['kondisiakhir']=='Kurang Baik') {
		  	 $kondisiakhir = 'kurangbaikEng';
		  }else{
		  	 $kondisiakhir= 'outEng';
		  }
      }elseif ($Departement==8) {
        if ($row['kondisiawal']=='Baik') {
			 $kondisiawal = 'baikPG';
		  }elseif ($row['kondisiawal']=='Kurang Baik') {
		  	 $kondisiawal = 'kurangbaikPG';
		  }else{
		  	 $kondisiawal = 'outPG';
		  }
		  if ($row['kondisiakhir']=='Baik') {
			 $kondisiakhir = 'baikPG';
		  }elseif ($row['kondisiakhir']=='Kurang Baik') {
		  	 $kondisiakhir = 'kurangbaikPG';
		  }else{
		  	 $kondisiakhir= 'outPG';
		  }
      }elseif ($Departement==9) {
       if ($row['kondisiawal']=='Baik') {
			 $kondisiawal = 'baikSec';
		  }elseif ($row['kondisiawal']=='Kurang Baik') {
		  	 $kondisiawal = 'kurangbaikSec';
		  }else{
		  	 $kondisiawal = 'outSec';
		  }
		  if ($row['kondisiakhir']=='Baik') {
			 $kondisiakhir = 'baikSec';
		  }elseif ($row['kondisiakhir']=='Kurang Baik') {
		  	 $kondisiakhir = 'kurangbaikSec';
		  }else{
		  	 $kondisiakhir= 'outSec';
		  }
       }else{
         if ($row['kondisiawal']=='Baik') {
			 $kondisiawal = 'baik';
		  }elseif ($row['kondisiawal']=='Kurang Baik') {
		  	 $kondisiawal = 'kurangbaik';
		  }else{
		  	 $kondisiawal = 'out';
		  }
		  if ($row['kondisiakhir']=='Baik') {
			 $kondisiakhir = 'baik';
		  }elseif ($row['kondisiakhir']=='Kurang Baik') {
		  	 $kondisiakhir = 'kurangbaik';
		  }else{
		  	 $kondisiakhir= 'out';
		  }
       }
		       
	       //menambahkan ksondisi awal dengan qty
	        // $this->db->set($kondisiawal, $kondisiawal.'+'.$row['qty'].'', FALSE);
	        // $this->db->where('codeaset',$row['codeaset'] );
	        // $this->db->update('tbaset');

			//Mengurangi ksondisi akhir dengan qty
	        // $this->db->set($kondisiakhir, $kondisiakhir.'+'.$row['qty'].'', FALSE);
	        // $this->db->where('codeaset',$row['codeaset'] );
	        // $this->db->update('tbaset');
    }
			 $this->db->insert_batch($table,$data);
}
	function loaddepartementall() {
		$data = $this->db->query("Select * from tbdepartement where isaktif='1' order by iddepartement asc");
		return $data->result();

   }
   function Select_subcategory_bydepartememnt($isdep) {
		$data = $this->db->query("SELECT * FROM tbsubcategory WHERE idsubcategory IN 
								(SELECT idsubcategory FROM tbitem WHERE $isdep = 1)");
		return $data->result();

   }
    function load_request_in($iddep,$codeitem) {
		$data = $this->db->query(" SELECT SUM(qtyorder) AS qty FROM `tbprdetil` INNER JOIN `tbpr`ON
								tbprdetil.`kodepr` = tbpr.`kodepr` WHERE tbprdetil.iddepartement = '$iddep' AND codeitem='$codeitem'");
		return $data->row()->qty;

   }

  
   	function item_view($Istok,$kategoriaset) { 
   		//print_r($Istok);exit();
   	if (!empty($Istok)) {  
		 $query = $this->db->query("SELECT tbaset.codeaset,tbitem.* ,subcategory FROM tbaset INNER JOIN tbitem
								 ON tbaset.`codeitem` = tbitem.`codeitem` INNER JOIN `tbsubcategory` 
								ON tbitem.`idsubcategory` = tbsubcategory.`idsubcategory` INNER JOIN `tbcategory`
								ON tbsubcategory.`idcategory` = tbcategory.`idcategory` WHERE tbaset.`isaktif` =1  and $Istok=1 and tbcategory.idcategory='$kategoriaset'"); 
		 }else{
		 	$query = $this->db->query(" SELECT tbaset.codeaset,tbitem.* ,subcategory FROM tbaset INNER JOIN tbitem
									 ON tbaset.`codeitem` = tbitem.`codeitem` INNER JOIN `tbsubcategory` 
									ON tbitem.`idsubcategory` = tbsubcategory.`idsubcategory` INNER JOIN `tbcategory`
									ON tbsubcategory.`idcategory` = tbcategory.`idcategory` WHERE tbaset.`isaktif` =1   and tbcategory.idcategory='$kategoriaset' "); 
		 }

    return $query->result() ;   


   } 



}
?>


        <div class="table-responsive">
          <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr >
                                        <th>Tanggal</th>
                                          <?php if ($idvendor==0){ ?>
                                            <th>Vendor</th>
                                          <?php } ?>
                                        <th>NO MI</th>
                                        <th>Tanggal Jatuh Tempo</th>
                                         <th>Jumlah</th>
                                         <th>&nbsp;</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i=1; $TotalHutang = 0;
                                    foreach($datahutang as $u){
                                    ?>
                                    <tr  class='odd gradeX context'>
                                        <td class="date"><?php echo $u->datereceive ?></td>
                                         <?php if ($idvendor==0){ ?>
                                            <td class="vendor"><?php echo $u->vendor?></td>
                                          <?php } ?>
                                        <td class="code"><?php echo $u->kodero?></td>
                                        <td class="desc"><?php echo $u->duedate?></td>
                                        <td class="total" align="right"><?php echo number_format($u->total, 0, '.', '.')?></td> 
                                        <td align="center">
                      <a class="btn btn-warning btn-xs fa  fa-check-square  Bayar" title="Koreksi"   style="cursor:pointer" > </a>
                    <a class="btn btn-info btn-xs fa  fa-list ViewDetail" title="View History Hutang"   style="cursor:pointer" >  </a>
                                        </td>

                                    </tr>
                                    <?php $i++; $TotalHutang += $u->total; } ?>
                                </tbody>
                                <tfoot>
                                  <tr style="background-color: #FFC9C9;">
                                     <?php if ($idvendor==0){ ?>
                                      <td colspan="4">&nbsp;</td>
                                     <?php }else{ ?>
                                      <td colspan="3">&nbsp;</td>
                                     <?php } ?>
                                   
                                     <td align="right"><strong><?php echo number_format($TotalHutang, 0, '.', '.')?></strong></td>
                                     <td>&nbsp;</td>
                                  </tr>
                                </tfoot>
                            </table>
          </div>



        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->


<script type="text/javascript">
  $(document).ready(function(){
     $(".ViewDetail").click(function(e){ 
     //var nomi = $('#IdDepartement').val();  
      var nomi = $(this).closest('tr').children('td.code').text(); 
       var total = $(this).closest('tr').children('td.total').text();     
      $.ajax({
        type: "POST",
       url: '<?php echo site_url('Payment/viwedetilmi');?>',
        data: {nomi:nomi  },
        success: function(data){
            e.preventDefault();
            var mymodal1 = $('#myModalNew');
            mymodal1.modal({backdrop: 'static', keyboard: false}) 
            var height = $(window).height() - 200;
            mymodal1.find(".modal-body").css("max-height", height);
             $("#NO span").text(nomi);
              $("#Total span").text(total);
            mymodal1.find('.modal-body').html(data);
            mymodal1.modal('show');            
        }
      });
    });
  });
</script>

<script type="text/javascript">
  $(document).ready(function(){
     $(".Bayar").click(function(e){ 
       var kodero = $(this).closest('tr').children('td.code').text(); 

      $.confirm({
          title: 'Confirmation',
          content: 'Are You Sure to Save?',
          buttons: {
              Simpan: function () {
                  $.ajax({
                    type: "POST",
                   url: '<?php echo site_url('Payment/acc_payment_koreksi');?>',
                    data: {kodero:kodero  },
                    success: function(data){
                       $("#Proses").trigger('click');
                       Berhasil_bayar()          
                    }
                  });
              },
              Batal: function () {

                  $.alert('Data Tidak Disimpan...');
              },
          }
      }); 
    });
  });
</script>


<script type="text/javascript">
  function File_Kosong() {
  $.alert({
    title: 'Caution!!',
    content: 'Transaction Is Invalid!',
    icon: 'fa fa-warning',
    type: 'orange',
});
}
</script>

<script type="text/javascript">
  function Berhasil_bayar() {
  $.alert({
    title: 'Success!!',
    content: 'Data Berhasil Diproses...',
    icon: 'fa fa-check-circle',
    type: 'green',
});
}
</script>

<script>
  $(function () {
    
    $('#example1').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
  });
</script>

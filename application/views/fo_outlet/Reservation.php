<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
//$this->load->view('template/Topbar');
//$this->load->view('template/Sidebar');
?>
<!-- <script src="<?php echo base_url('assets/js/accounting.js') ?>" type="text/javascript"></script> -->
<!-- <script src="<?php echo base_url('assets/js/cleave.min.js') ?>" type="text/javascript"></script> -->

 <script type="text/javascript">
      // jQuery(document).ready(function($) {
      //   new Cleave('.input-1', {
      //      numeral: true, 
      //      numeralDecimalMark: ',',
      //      delimiter: '.' 
      // });
      //   new Cleave('.input-2', {
      //      numeral: true, 
      //      numeralDecimalMark: ',',
      //      delimiter: '.' 
      // });
      //    new Cleave('.input-3', {
      //      numeral: true, 
      //      numeralDecimalMark: ',',
      //      delimiter: '.' 
      // });
      //     });
    </script>

<style type="text/css">
	.col-hg{
		padding-bottom:5px;
		padding-left:2px;
		padding-right:2px;
	}
	.btn-float{
		width:35px; height:35px; float:left; margin-left:5px;
	}
.modal.modal-wide .modal-dialog {
  width: 80%;
}
.modal-wide .modal-body {
  overflow-y: auto;
}
	.label {
		font-weight:normal;
		color:#333;
		font-size:14px;
		line-height: 1.42857143;
		text-align:left;
	}
	.button-grn{
		background-color:#01b3a3;
		border-color:#01b3a3;
		color:#FFF;
	}
	.capt{
		border-bottom:#eee solid 1px; margin-bottom:10px
	}
	.btn-canc{
		height:75px; width:100%; background-color:#666; color:#FFF; font-size:20px;
	}
	.btn-checkin{
		height:75px; width:100%; background-color:#2970e4; color:#FFF; font-size:20px;
	}
    td.action { text-align: center; }
</style>


<!-- Main content -->
<section class="content">
<form method="post" id="Simpan"  action="<?php echo base_url(). 'index.php/Fo_outlet/Res_add'; ?>" autocomplete="off" >
	<div class="row">
        <div class="col-lg-12" style=" background:#CCC; top:0; padding-top:5px; padding-bottom:5px; color:#FFF"><strong>New single reservation</strong> 
          <a  class="btn btn-danger btn-md"  title="close" href="<?php echo base_url(). 'index.php/Reservation/ExpectedArrival_view'; ?>" style="float: right;"><span class="fa fa-times"></span></a>
        </div>

        <div class="col-lg-4" style="margin-top:30px;">
        <!-- GUEST -->
        	<div class="col-sm-12 col-hg capt">GUEST</div>
        	<div class="col-sm-3 col-hg">
             <div align="right">Name :</div></div>
              <div class="col-sm-8 col-hg">
                <input type="text" id="NamaGuest" class="form-control" readonly="readonly"> 
                <input type="hidden"  id="IdGuest" name="IdGuest" class="form-control"></div>

            <div class="col-sm-1 col-hg">
              <a class="btn button-grn" id="AddGuest" title="Add Guest"><span class="fa fa-plus-circle"></span></a>
            </div>
            <div class="col-sm-3 col-hg">
              <div align="right">Phone :</div>
            </div>
            <div class="col-sm-8 col-hg">
              <input type="text" id="PhoneGuest" class="form-control" readonly="readonly">
             </div>
            <div class="col-sm-1 col-hg"></div>
            <div class="col-sm-3 col-hg"></div>
            <div class="col-sm-8 col-hg" style="margin-bottom:10px;"><a  class="btn button-grn SelectGuest">Select Guest</a></div>
            <div class="col-sm-1 col-hg"></div>
            <div class="col-sm-3 col-hg"><div align="right">Source :</div></div>
            <div class="col-sm-8 col-hg">
               <select  class="form-control select2"  style="width: 100%;" id="Source" name="Source">
               <?php 
                          foreach($source as $peksur){
                      ?>
                        <option value="<?php echo $peksur->id; ?>" ><?php echo $peksur->source; ?></option>
                        <?php } ?>
               </select>
            </div>
            <div class="col-sm-3 col-hg"><div align="right">Purpose :</div></div>
            <div class="col-sm-8 col-hg">
              <select  class="form-control select2"  style="width: 100%;" id="Purpose" name="Purpose">
               <?php 
                          foreach($purpose as $pekpur){
                      ?>
                        <option value="<?php echo $pekpur->id; ?>" ><?php echo $pekpur->purpose; ?></option>
                        <?php } ?>
               </select>
            </div>
          <div class="col-sm-3 col-hg"><div align="right">Segment :</div></div>
            <div class="col-sm-8 col-hg">
              <select  class="form-control select2"  style="width: 100%;" id="Segment" name="Segment">
               <?php 
                          foreach($segment as $pekseg){
                      ?>
                        <option value="<?php echo $pekseg->id; ?>" ><?php echo $pekseg->segment; ?></option>
                        <?php } ?>
               </select>
            </div>
        	<div class="col-sm-1 col-hg"></div>
            <div class="col-sm-3 col-hg"><div align="right">Agent :</div></div>
            <div class="col-sm-8 col-hg">
              <select  class="form-control select2"  style="width: 100%;" id="Agent" name="Agent">
               <?php 
                          foreach($company as $pekcom){
                      ?>
                        <option value="<?php echo $pekcom->idcompany; ?>" ><?php echo $pekcom->name; ?></option>
                        <?php } ?>
               </select>
            </div>

            <div class="col-sm-1 col-hg"></div>
            <div class="col-sm-3 col-hg"><div align="right">VIP :</div></div>
            <div class="col-sm-8 col-hg">
             <input  readonly="readonly" type="checkbox" name="VIP1" class="flat-red" id="VIP1" checked="1"></span>
               
            </div>

        	<div class="col-sm-1 col-hg"></div>
            <div class="col-sm-3 col-hg"><div align="right">Adult :</div></div>
            <div class="col-sm-8 col-hg">
            <input type="number" name = "Adult" class="form-control" style="width:60px; float:left" value="1">
            <!-- <button class="btn button-grn btn-float" style="font-size:18px">+</button>
            <button class="btn button-grn btn-float" style="font-size:18px">-</button> -->
            </div>
        	<div class="col-sm-1 col-hg"></div>
            <div class="col-sm-3 col-hg"><div align="right">Child :</div></div>
            <div class="col-sm-8 col-hg">
            <input type="number" name="Child" class="form-control" style="width:60px; float:left" value="0">
        
          
            <!-- <button class="btn button-grn btn-float" style="font-size:18px">+</button>
            <button class="btn button-grn btn-float" style="font-size:18px">-</button> -->
            </div>
        	<div class="col-sm-1 col-hg"></div>
            <div class="col-sm-3 col-hg"> 
            </div>
            <div class="col-sm-8 col-hg" style="margin-bottom:10px;">
              <a class="btn button-grn SelectGuestAll">Guest List</a> 
              <a class="btn button-grn " id="ViewGuest">  <span class="fa fa-fw fa-list" ></span> </a>
            </div>
            <div class="col-sm-1 col-hg"></div>
            
    	<!-- FLIGHT INFORMATION -->
<div style="display: none;">
<div class="col-sm-12 col-hg capt" style="margin-top:20px;">FLIGHT INFORMATION</div>
  <div class="col-sm-3 col-hg">
    <div class="input-group">
        <span class="input-group-addon"><input  readonly="readonly" type="checkbox" class="flat-red" checked></span>
        <label for="cbflg" class="form-control label">Pick Up</label>
    </div>
  </div>
<div class="col-sm-4 col-hg">
   <div style="float:left" class="label">Flight</div>
      <input type="text" class="form-control" style="float:left; width:80px;"></div>
        <div class="col-sm-5 col-hg">
          <div style="float:left" class="label">ETA
            </div>
            <div class="bootstrap-timepicker">
            <div class="form-group">
              <div class="input-group">
                <input type="text"  name = "DropTime" class="form-control timepicker">
                <div class="input-group-addon">
                  <i class="fa fa-clock-o"></i>
                </div>
              </div>
            </div>
          </div>
            </div>
    <div class="col-sm-3 col-hg">
        <div class="input-group"><span class="input-group-addon">
        <input  type="checkbox" class="flat-red" checked></span>
        <label for="cbdrop" class="form-control label">Drop &emsp;</label>
        </div>
    </div>
    <div class="col-sm-4 col-hg">
      <div style="float:left" class="label">Flight</div>
        <input type="text" class="form-control" style="float:left; width:80px;"></div>
          <div class="col-sm-5 col-hg">
            <div style="float:left" class="label">ETD
          </div>
         <div class="bootstrap-timepicker">
            <div class="form-group">
              <div class="input-group">
                <input type="text"  name = "DropTime" class="form-control timepicker">
                <div class="input-group-addon">
                  <i class="fa fa-clock-o"></i>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>
</div>


        <div class="col-lg-4" style="margin-top:30px;">
        	<div class="col-sm-12 col-hg capt">RESERVATION INFORMATION</div>
            <div class="col-sm-3 col-hg"><div align="right">Check in :</div></div>
            <div class="col-sm-9 col-hg">
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" name= "checkin" id="checkin" value="<?php echo 
                    date('m/d/Y') ?>" >
                  </div>
            </div>
            <div class="col-sm-3 col-hg"><div align="right">Check out :</div></div>
            <div class="col-sm-9 col-hg">
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text"  class="form-control pull-right" name="checkout" id="checkout" value="<?php echo 
                    date('m/d/Y') ?>" >
                  </div>
            </div>
            <div class="col-sm-3 col-hg"><div align="right">Duration :</div></div>
            <div class="col-sm-9 col-hg">
              <input type="number" name = "Duration" id = "Duration" min="1" class="form-control" style="float:left; width:70px;" value="0"   
                  onchange="SumWaktuResv()" autocomplete="off" >
            <div style="float:left" class="label">night(s)</div></div>
            <div class="col-sm-3 col-hg"><div align="right">Room type :</div></div>
        <div class="col-sm-9 col-hg">
                    <select id="RoomType" class="form-control select2" data-placeholder="-- Select Room Type --" style="width: 100%;" name="RoomType" required> 
                    <option value="" ></option>                
                      <?php //print_r($roomtype);exit();
                          foreach($roomtype as $pek){
                      ?>
                        <option value="<?php echo $pek->id; ?>" ><?php echo $pek->roomtype; ?></option>
                        <?php } ?>
                    </select>
        </div>
            <div class="col-sm-3 col-hg"><div align="right">Room no :</div></div>
            <div class="col-sm-9 col-hg" id="RoomNodiv">
                <select id="RoomNo" class="form-control select2" data-placeholder="-- Select Room No --" style="width: 100%;" name="RoomNo" required>                 
                    <option value=""></option>                
                     
                </select>
            </div>
            <div class="col-sm-3 col-hg"><div align="right">Cut-off date :</div></div>
            <div class="col-sm-9 col-hg">
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
              <input type="text"  class="form-control pull-right" name="cod" id="cod" value="<?php echo date('m/d/Y') ?>">
                  </div>
            </div>
            <div class="col-sm-3 col-hg"><div align="right">Rsv Status :</div></div>
            <div class="col-sm-9 col-hg">
              <select  class="form-control select2"  style="width: 100%;" name="RsvStatus" id="RsvStatus">                 
                      <?php //print_r($roomtype);exit();
                          foreach($rsvstatus as $pekr){
                      ?>
                        <option value="<?php echo $pekr->id; ?>" ><?php echo $pekr->status; ?></option>
                        <?php } ?>
                    </select>
            </div>
        	<div class="col-sm-3 col-hg"><div align="right">Remark :</div></div>
            <div class="col-sm-9 col-hg"><textarea class="form-control" style="height:150px"></textarea></div>
      	</div>
        <div class="col-lg-4" style="margin-top:30px;">
        	<div class="col-sm-12 col-hg capt">PACKAGE & RATE</div>
            <div class="col-sm-3 col-hg"><div align="right">Package :</div></div>
            <div class="col-sm-9 col-hg">
             <select  class="form-control select2"  style="width: 100%;" id="Package" name="Package">
               <!-- <option value="-" >-- Select Package --</option> -->
               <?php 
                          foreach($package as $pekage){
                      ?>
                        <option value="<?php echo $pekage->id; ?>" ><?php echo $pekage->package; ?></option>
                        <?php } ?>
               </select>
            </div>
            <div class="col-sm-3 col-hg"><div align="right">Rate :</div></div>
            <div class="col-sm-4 col-hg">
              <input type="text" class="form-control" id="RatePackage1">
              <input type="hidden" class="form-control" id="RatePackage" name="RatePackage">
            </div>
        	<div class="col-sm-5 col-hg"><div class="input-group"><span class="input-group-addon"> <input  type="checkbox" class="flat-red" checked id="flat-rate"></span><label for="cbflr" class="form-control label">Flat rate</label></div></div>
            <div class="col-sm-3 col-hg"></div>
            <div class="col-sm-9 col-hg" style="margin-bottom:10px;"><button type="button" class="btn button-grn" id="package_rate">Advance package & rate</button></div>
        	
            <div class="col-sm-12 col-hg capt" style="margin-top:20px">PAYMENT</div>
            <div class="col-sm-3 col-hg"><div align="right">Bill reciever :</div></div>
            <div class="col-sm-9 col-hg">
              <select  class="form-control select2"  style="width: 100%;" id="Company" name="Company">
                <option value="0" selected>Personal Account</option>
               <?php 
                          foreach($company as $pekcom){
                      ?>
                        <option value="<?php echo $pekcom->idcompany; ?>" ><?php echo $pekcom->name; ?></option>
                        <?php } ?>
               </select>
            </div>

            <div class="col-sm-3 col-hg"><div align="right">Room Total</div></div>
            <div class="col-sm-9 col-hg">
             <input type="text" name="BiayaRoom1" id="BiayaRoom1" class="form-control" style=" text-align: right; " readonly>  <input type="hidden" name="BiayaRoom" id="BiayaRoom" readonly>
            </div>

<div style="display: none;">
            <div class="col-sm-3 col-hg"><div align="right">Deposit</div></div>
            <div class="col-sm-9 col-hg">
               <input type="text" name="BiayaDeposit1" id="BiayaDeposit1" class="form-control input-1" style=" text-align: right;"
               onkeyup="SumTotal()">
             <input type="hidden" name="BiayaDeposit" id="BiayaDeposit" class="form-control input-1" style=" text-align: right; ">
            </div>

            <div class="col-sm-3 col-hg"><div align="right">Article Payment</div></div>
            <div class="col-sm-9 col-hg">
             <select  class="form-control select2"  style="width: 100%;" id="ArticlePayment" name="ArticlePayment" >
                <option value="0" selected>Personal Account</option>
               <?php 
                          foreach($article_payment as $article){
                      ?>
                        <option value="<?php echo $article->id; ?>" ><?php echo $article->articlepayment; ?></option>
                        <?php } ?>
               </select>
            </div>

             <div class="col-sm-3 col-hg"><div align="right">Invoice NO</div></div>
            <div class="col-sm-9 col-hg">
              <input type="text" name="NoInvoice" id="NoInvoice" class="form-control" style=" text-align:right;">
            </div>

            
             <div class="col-sm-3 col-hg"><div align="right">Balance</div></div>
            <div class="col-sm-9 col-hg">
              <input type="text" name="BiayaBalance1" id="BiayaBalance1" class="form-control" style=" text-align:right;" readonly>
             <input type="hidden" name="BiayaBalance" id="BiayaBalance" class="form-control" style=" text-align:right;" readonly>
            </div>
</div>
            <div class="col-sm-4">
              &nbsp; <input type="hidden" name="isSaveCheckin" id="isSaveCheckin" value="0">
            </div>
            <div class="col-sm-3 col-hg" style="margin-top:200px;">
              <a type="submit" class="btn btn-success btn-lg"  id="simpan1" class="btn button-grn ">Save <i class='fa fa-check-square-o'></i> </a>
            </div>
            <div class="col-sm-2 col-hg" style="margin-top:200px;">
              <a type="submit" class="btn btn-primary btn-lg" id="simpan2" class="btn button-grn ">Save & Check In <i class='fa fa-check-square-o'></i> </a>
            </div>
        </div>
       
    </div>
   
        </form>
</section>

   <div class="modal modal-wide fade" id="myModalGuest" role="dialog" style="display:none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                          <p id="notify"></p>
                    <button type="button" class="close" title="close" data-dismiss="modal">&times;</button>
                    <h4 align="center" class="modal-title">Guest List</h4>
                </div>
                <div class="modal-body">   
                </div>
            </div>
        </div>     
    </div>

    <div class="modal modal-wide fade" id="myModalGuestAdd" role="dialog" style="display:none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" title="close" data-dismiss="modal">&times;</button>
                    <h4 align="center" class="modal-title">Guest List</h4>
                </div>
                <div class="modal-body">   
                </div>
            </div>
        </div>     
    </div>

    <!-- <div class="modal modal-wide fade" id="myModalAddDeposit" role="dialog" style="display:none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" title="close" data-dismiss="modal">&times;</button>
                    <h4 align="center" class="modal-title">Add Deposit</h4>
                </div>
                <div class="modal-body"> 
                <div class="row">
                  <div class="col-sm-12">
                  <div class="form-group">
                            <label class="control-label">Amount</label>
                            <input type="text" name="Amount" id="Amount"   class="form-control input-1"  placeholder="">
                    </div>
                     <div class="form-group">
                            <label class="control-label">Invoice NO</label>
                            <input type="text" name="InvoiceNo" id="InvoiceNo"  data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out description..."  class="form-control"  placeholder="">
                    </div>
                    <div class="form-group">
                            <label class="control-label">Description</label>
                            <input type="text" name="Description" id="Description"  data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out description..."  class="form-control"  placeholder="">
                    </div>
                  </div>
                  
                </div>  
                </div>
            </div>
        </div>     
    </div> -->

    <div class="modal modal-wide fade" id="myModalGuestAll" role="dialog" style="display:none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" title="close" data-dismiss="modal">&times;</button>
                    <h4 align="center" class="modal-title">Guest</h4>
                </div>
        <div class="modal-body" id="modalbody">
            <div class="table-responsive">
              <table id="example12" class="table table-bordered table-striped">                
                <input type="hidden" value="0" id="index"> 
                    <thead>
                        <tr >
                            <th>index</th>
                            <th>id Guest</th>
                            <th>Name</th>
                            <th>Address</th>
                            <th>State</th>
                            <th>E-Mail</th>
                            <th>Phone</th>
                            <th >Action</th>
                        </tr>
                    </thead>  
                 <tbody id="mytbody">              
                 </tbody>
               </table>
            </div>
        </div>
        </div>     
    </div>


<script type="text/javascript">
$("#simpan1").click(function() {
  $("#isSaveCheckin").val(0);
  var cod  =  $("#cod").val();
  var checkout  =  $("#checkout").val();
   var checkin  =  $("#checkin").val();
   var roomtype = $("#RoomType").val();
   var IdGuest = $("#IdGuest").val();
  // if (new Date(cod).getTime()  < new Date(checkout).getTime()  ) {
  //   File_Kosong('Cut-off Date or Checkout Date invalid'); return false;
  // }
  if (IdGuest == '') {
    File_Kosong(' Invalid Guest Name'); return false;
  }else if(new Date(checkout).getTime()  == new Date(checkin).getTime()){
    File_Kosong(' Checkin Date or Checkout Date invalid'); return false;
  }else if(roomtype == ''){
    File_Kosong(' Invalid Room Type'); return false;
  }

        event.preventDefault();
        $.confirm({
          title: 'Confirmation',
          content: 'Are You Sure to Save?',
          type:'blue',
          buttons: {
              Simpan: function () {
                  $.LoadingOverlay("show");
                  $("#Simpan").submit();
              },
              Batal: function () {
                
                  $.alert('Data Tidak Disimpan...');
              },
          }
      }); 

});

$("#simpan2").click(function() {
  $("#isSaveCheckin").val(1);
  var cod  =  $("#cod").val();
  var checkout  =  $("#checkout").val();
   var checkin  =  $("#checkin").val();
   var roomtype = $("#RoomType").val();
   var IdGuest = $("#IdGuest").val();
  // if (new Date(cod).getTime()  < new Date(checkout).getTime()  ) {
  //   File_Kosong('Cut-off Date or Checkout Date invalid'); return false;
  // }
  if (IdGuest == '') {
    File_Kosong(' Invalid Guest Name'); return false;
  }else if(new Date(checkout).getTime()  == new Date(checkin).getTime()){
    File_Kosong(' Checkin Date or Checkout Date invalid'); return false;
  }else if(roomtype == ''){
    File_Kosong(' Invalid Room Type'); return false;
  }

        event.preventDefault();
        $.confirm({
          title: 'Confirmation',
          content: 'Are You Sure to Save?',
          type:'blue',
          buttons: {
              Simpan: function () {
                  $.LoadingOverlay("show");
                  $("#Simpan").submit();
              },
              Batal: function () {
                
                  $.alert('Data Tidak Disimpan...');
              },
          }
      }); 

});
</script>
<?php
$this->load->view('template/Js');
//$this->load->view('template/Foot');
?>


   <script type="text/javascript">
  function File_Kosong(text) {
  $.alert({
    title: 'Caution!!',
    content: text,
    icon: 'fa fa-warning',
    type: 'orange',
});
}
</script>


<script type="text/javascript">
function SumWaktuResv()
{

  var Duration  =$('#Duration').val();  
var Duration1 =  parseInt(Duration)
var checkin = $('#checkin').val();
//alert(checkin);

var date = new Date(checkin);
var newdate = new Date(date);

 newdate.setDate(newdate.getDate() + Duration1); 
var nd = new Date(newdate);

$('#checkout').val(  nd.getMonth()+1 + "/" + nd.getDate() +  "/" + nd.getFullYear() );

    var data = $('#RatePackage').val(); 
    var duration = $('#Duration').val();
    var BiayaRoom = data * parseInt(duration);
    // $('#BiayaRoom1').val(BiayaRoom.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")); 
    $('#BiayaRoom1').autoNumeric('set', BiayaRoom); 
    $('#BiayaRoom').val(BiayaRoom);  
   // alert(BiayaRoom);

}
</script>

<script type="text/javascript">
//   $('#checkout').on('dp.change', function(e){
// alert('tes');
//  var checkout  =  $("#checkout").val();
//    var checkin  =  $("#checkin").val();
//  var selisih =  new Date(checkout).getTime() -  new Date(checkin).getTime() ; 
//  $('#Duration').val(selisih);
// })

</script>
<script type="text/javascript">
   $(function () {
   $('#checkout').datepicker().on('changeDate', function(ev){
     var checkout  =  new Date($("#checkout").val());
     var checkin  =  new Date($("#checkin").val());
     var timeDiff = Math.abs(checkin.getTime() - checkout.getTime());
      var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
     $('#Duration').val(diffDays);
     $(this).datepicker('hide');
});     
    });
</script>

<script type="text/javascript">
   $(function () {
   $('#checkin').datepicker().on('changeDate', function(ev){
     var checkout  =  new Date($("#checkout").val());
     var checkin  =  new Date($("#checkin").val());
     var timeDiff = Math.abs(checkin.getTime() - checkout.getTime());
      var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
     $('#Duration').val(diffDays);
     $(this).datepicker('hide');
});     
    });
</script>

<script type="text/javascript">
function SumTotal()
{

  var BiayaRoom  =$('#BiayaRoom').val();  
var Deposit = $('#BiayaDeposit1').val().replace(/\./g, "");
$('#BiayaDeposit').val(Deposit);
//alert(BiayaRoom);
var total = parseFloat(BiayaRoom) - parseFloat(Deposit);

$('#BiayaBalance1').val(total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
$('#BiayaBalance').val(total);



}
</script>

<script type="text/javascript">
 $('#VIP1').is(':checked')
// alert('tes');
</script>

<script type="text/javascript">
$('#Package').on("select2:select", function(e) {
  var idpackage = $('#Package').val();
  $.ajax({
    type: "POST",
    dataType:'json',
    url: '<?php echo site_url('Fo_outlet/Select_package_by_code');?>',
    data: {id: idpackage},
    success: function(data){
      // $('#RatePackage1').val(data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
      $('#RatePackage1').autoNumeric('set', data);
      $('#RatePackage').val(data); 
      var duration = $('#Duration').val();
      var BiayaRoom = data* parseInt(duration);
      // $('#BiayaRoom1').val(BiayaRoom.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")); 
      $('#BiayaRoom1').autoNumeric('set', BiayaRoom); 
      $('#BiayaRoom').val(BiayaRoom);  
    }
  });
});

</script>


<script type="text/javascript">
  $(document).ready(function(){
    $('#example12 tbody').on('click', '.Hapus', function(e){
    var t = $('#example12').DataTable();
    var selectedIndex = t.row($(this).closest('tr')).data()[0]

      $('.'+selectedIndex).remove();
      t.row($(this).closest('tr')).remove().draw(false);
    });
  });
</script>

<script type="text/javascript">
  $(document).ready(function(){
     $("#ViewGuest").click(function(e){ 
        e.preventDefault();
            var mymodal = $('#myModalGuestAll');
            mymodal.modal({backdrop: 'static', keyboard: false}) 
            mymodal.modal('show');            

    });
  });
</script>

<script type="text/javascript">
  $(document).ready(function(){
     $(".SelectGuestAll").click(function(e){ 
      $.ajax({
        type: "POST",
       url: '<?php echo site_url('Fo_outlet/Guest_view');?>',
        data: {status: 'All'  },
        success: function(data){
            e.preventDefault();
            var mymodal = $('#myModalGuest');
            mymodal.modal({backdrop: 'static', keyboard: false}) 
            var height = $(window).height() - 200;
            mymodal.find(".modal-body").css("max-height", height);
            mymodal.find('.modal-body').html(data);
            mymodal.modal('show');            
        }
      });
    });
  });
</script>

<script type="text/javascript">
  $(document).ready(function(){
     $(".AddDeposit").click(function(e){ 
      // $.ajax({
      //   type: "POST",
      //  url: '<?php echo site_url('Fo_outlet/Guest_view');?>',
      //   data: {status: 'All'  },
      //   success: function(data){
      //       e.preventDefault();
      //       var mymodal = $('#myModalGuest');
      //       mymodal.modal({backdrop: 'static', keyboard: false}) 
      //       var height = $(window).height() - 200;
      //       mymodal.find(".modal-body").css("max-height", height);
      //       mymodal.find('.modal-body').html(data);
      //       mymodal.modal('show');            
      //   }
      // });

       e.preventDefault();
            var mymodal = $('#myModalAddDeposit');
            mymodal.modal({backdrop: 'static', keyboard: false}) 
            var height = $(window).height() - 200;
            mymodal.find(".modal-body").css("max-height", height);
            mymodal.find('.modal-body').html(data);
            mymodal.modal('show');  
    });
  });
</script>

<script type="text/javascript">
  $(document).ready(function(){
     $(".SelectGuest").click(function(e){ 
      $.ajax({
        type: "POST",
       url: '<?php echo site_url('Fo_outlet/Guest_view');?>',
        data: {status: '1'  },
        success: function(data){
            e.preventDefault();
            var mymodal = $('#myModalGuest');
            mymodal.modal({backdrop: 'static', keyboard: false}) 
            var height = $(window).height() - 200;
            mymodal.find(".modal-body").css("max-height", height);
            mymodal.find('.modal-body').html(data);
            mymodal.modal('show');            
        }
      });
    });
  });
</script>

<script type="text/javascript">
  $(document).ready(function(){
     $("#AddGuest").click(function(e){ 
      $.ajax({
        type: "POST",
       url: '<?php echo site_url('Fo_outlet/Guest_add');?>',
        data: {id: 'all'  },
        success: function(data){
            e.preventDefault();
            var mymodal = $('#myModalGuestAdd');
            mymodal.modal({backdrop: 'static', keyboard: false}) 
            var height = $(window).height() - 200;
            mymodal.find(".modal-body").css("max-height", height);
            mymodal.find('.modal-body').html(data);
            mymodal.modal('show');            
        }
      });
    });
  });
</script>

 <script type="text/javascript">
      $(document).ready(function() {
       $('#fullscreen').trigger('click');
          });
    </script>

<script type="text/javascript">
   $(document).on('change', '#RoomType', function(){
  var id = $('#RoomType').val();
        $.ajax({
            type: "POST",
            dataType:'html',
           url: '<?php echo site_url("Fo_outlet/select_room_no");?>',
            data: {id: id},
            success: function(data){
                 $('#RoomNodiv').html(data);  
                 $(".select2").select2();
            }
          });
    });
</script>
<script language="JavaScript">
    function fullScreen() {
    var el = document.documentElement
        , rfs = // for newer Webkit and Firefox
               el.requestFullScreen
            || el.webkitRequestFullScreen
            || el.mozRequestFullScreen
            || el.msRequestFullScreen
    ;
    if(typeof rfs!="undefined" && rfs){
      rfs.call(el);
    } else if(typeof window.ActiveXObject!="undefined"){
      // for Internet Explorer
      var wscript = new ActiveXObject("WScript.Shell");
      if (wscript!=null) {
         wscript.SendKeys("{F11}");
      }
    }

    }
// End -->
</script>

 <script>
  $(function () {
    $(".select2").select2();
    $(".select3").select2();
    // $('#Package').val('5');
    $('#Package option').eq(1).prop('selected', true);
    var idpackage = $('#Package').val();
    $.ajax({
      type: "POST",
      dataType:'json',
     url: '<?php echo site_url('Fo_outlet/Select_package_by_code');?>',
      data: {id: idpackage},
      success: function(data){
        // $('#RatePackage1').val(data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
    	$('#RatePackage1').autoNumeric('set', data);
        $('#RatePackage').val(data); 
        var duration = $('#Duration').val();
        var BiayaRoom = data* parseInt(duration);
        // $('#BiayaRoom1').val(BiayaRoom.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")); 
    	$('#BiayaRoom1').autoNumeric('set', BiayaRoom); 
        $('#BiayaRoom').val(BiayaRoom);  
       // alert(BiayaRoom);
      }
    });

  });
  </script>
      <script>
  $( function() {
    $( "#checkin" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });
	$( "#checkout" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });
	$( "#cod" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });

  });
  </script>
     <script type="text/javascript">
    $(".timepicker").timepicker({
      showInputs: false
    });
</script>

		<script>
        $('.value-plus1').on('click', function(){
            var divUpd = $(this).parent().find('.value1'), newVal = parseInt(divUpd.text(), 10)+1;
            divUpd.text(newVal);
            $('#qty').val(newVal);
        });

        $('.value-minus1').on('click', function(){
            var divUpd = $(this).parent().find('.value1'), newVal = parseInt(divUpd.text(), 10)-1;
            if(newVal>=1) divUpd.text(newVal);
            if(newVal>=1) $('#qty').val(newVal);
        });
        </script>
<script>
  $(function () {
     $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });
  });
</script>
<script>
  $(function () {
    $("#example12").DataTable();
    dtTable= $("#example12").DataTable();
    dtTable.columns([0,1]).visible(false);

    $("#package_rate").DataTable();
    $('#RatePackage1').autoNumeric('init', autoNumDecimal);
    $('#BiayaRoom1').autoNumeric('init', autoNumDecimal);
    $('#package_rate').hide();
  });
	$('#RatePackage1').on('input', function() {
		$('#RatePackage').val($('#RatePackage1').autoNumeric('get'));
		var duration = $('#Duration').val();
		var BiayaRoom = parseFloat($('#RatePackage').val()) * parseInt(duration);
		$('#BiayaRoom1').autoNumeric('set', BiayaRoom); 
		$('#BiayaRoom').val(BiayaRoom); 
	});
  $('#flat-rate').on('ifToggled', function(evt){
    if (evt.target.checked) {
      $('#package_rate').hide();
    }else{
      $('#package_rate').show();
    }
  });
  function update_package_rate() {
    var night = $('#duration').val();
    var rate = $('#RatePackage1').autoNumeric('get');
    var t = $("#package_rate").DataTable();

    for (var i = 1; i <= night; i++) {
      t.row.add( [
        i,
        '<input type="text" name="advance[]" class="form-control advance">'
      ] ).draw( false );
    }
    
  }
</script>

<table id="package_rate" class="table table-bordered table-striped"> 
  <thead>
    <tr>
      <th>Night(s)</th>
      <th>Rate</th>
    </tr>
  </thead>  
  <tbody id="bdy_package_rate">
  </tbody>
</table>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/** 
 * @author I Made Subrata Sandhiyasa
 */

class Akuntan_function_template {
    private $TanggalSistem;
	var $CI = NULL;
	function __construct(){
		$this->ci =& get_instance();
        $this->ci->load->model('M_akuntan');
        
        $this->TanggalSistem = date('Y-m-d H:i:s');     
	}   
	
    function generate_jurnal($id,$BuktiTransaksi,$Nominal, $KodeOperator){ 
        date_default_timezone_set('Asia/Hong_Kong');
        $dataJU['kodeunik']= $this->ci->M_akuntan->getkodeunikJU('tbmasterjurnal',$id);
        $IdJurnal = $dataJU['kodeunik'];

        $dataSetting = $this->ci->M_akuntan->loadSetingAkunTemplate($id);

        foreach($dataSetting->result() as $dataSetting1){
                $AkunDebet = $dataSetting1->AkunDebet;
                $AkunKredit = $dataSetting1->AkunKredit;
                $NamaTransaksi = $dataSetting1->NamaTransaksi;
                $IdJurnal = $IdJurnal;
                $TanggalInput = date('Y-m-d H:i:s');
                $TanggalSistem = $this->TanggalSistem;
            }

            $Akun= array($AkunDebet,$AkunKredit);
            $Akun1 = array_combine(range(1, count($Akun)), array_values($Akun));

            $Debet= array($Nominal,0);
            $Debet1 = array_combine(range(1, count($Debet)), array_values($Debet));

            $Kredit= array(0,$Nominal);
            $Kredit11 = array_combine(range(1, count($Kredit)), array_values($Kredit));

            $IdJurnal1= array($IdJurnal,$IdJurnal);
            $IdJurnal11 = array_combine(range(1, count($IdJurnal1)), array_values($IdJurnal1));

            $TanggalInput1= array($TanggalInput,$TanggalInput);
            $TanggalInput11 = array_combine(range(1, count($TanggalInput1)), array_values($TanggalInput1));

            $TanggalSistem1= array($TanggalSistem,$TanggalSistem);
            $Tanggalsistem11 = array_combine(range(1, count($TanggalSistem1)), array_values($TanggalSistem1));
        
    $data11 = 
        array(
            'NoAkun' =>$Akun1,
            'Debet'=> $Debet1,
            'Kredit'=>$Kredit11,
            'IdJurnal' => $IdJurnal11,
            'TanggalInput' =>  $TanggalInput11,
            'TanggalSistem' =>  $Tanggalsistem11
        );


       $data[] = array(
            'IdJurnal' => $IdJurnal,
            'BuktiTransaksi' => $BuktiTransaksi,
            'Keterangan' =>  $NamaTransaksi,
            'TanggalInput' => date('Y-m-d H:i:s'), 
            'Tanggalsistem' => $TanggalSistem, 
            'Isaktif' => "1",
            'KodeOperator' => $KodeOperator
        );


       $banyakData4 = count($data11['NoAkun']);
            for($i =1; $i <= $banyakData4; ++$i) {
            $data[] =
             array(
                    'NoAkun' =>$data11['NoAkun'][$i],
                    'Debet'=> $data11['Debet'][$i],
                    'Kredit'=>$data11['Kredit'][$i],
                    'IdJurnal' =>$data11['IdJurnal'][$i],
                    'TanggalInput' => $data11['TanggalInput'][$i],
                    'TanggalSistem' => $data11['TanggalSistem'][$i]
                    
                    )   ;
        }

    //  print_r($data1);
     //  exit();

        return $data;



        }
    
}



        <div class="table-responsive">
          <table id="example2" class="table table-bordered table-striped">
                                <thead>
                                    <tr >
                                        <th >Reservation ID</th>
                                        <th >R. Status</th>
                                        <th >#Room</th>
                                        <th >R. Type</th>
                                        <th >Guest Name</th>
                                        <th >Arrival</th>
                                        <th >Departure</th>
                                        <th >Night</th>
                                        <th style="display: none;">Nationality</th>
                                        <th >Segment</th>
                                        <th >Company</th>
                                        <th >Res. Notes</th>
                                        <th style="display: none;">Res. Message</th>
                                         <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $No = 1; foreach($data as $u){
                                    ?>
                                    <tr  class='odd gradeX context'>
                                        <td><?php echo $u->idreservation ?></td>
                                        <td><?php echo $u->status?></td>
                                        <td><?php echo $u->Expr1?></td>
                                        <td><?php echo $u->desc?></td>
                                        <td><?php echo $u->saluation?> <?php echo $u->firstname?> <?php echo $u->lastname?></td>
                                        <td><?php echo $u->checkin?></td>
                                        <td><?php echo $u->checkout?></td>
                                        <td><?php echo $u->duration?></td>
                                        <td style="display: none;"><?php echo $u->Expr2?></td>
                                        <td><?php echo $u->Expr3?></td>
                                        <td><?php echo $u->Expr4?></td>
                                        <td style="display: none;"><?php echo $u->notes?></td>
                                        <td><?php echo $u->msg?></td>
                                        <td>
                                         <a class="btn btn-warning btn-xs" title="Update Data"  
                              href="<?php echo base_url('index.php/Fo_outlet/checkin/'.$u->idreservation); ?>">  <span class="fa fa-fw fa-edit" ></span> </a>
                              </td>

                                    </tr>
                                    <?php $No++; } ?>
                                </tbody>
                            </table>
          </div>



       


<?php 

class M_payment extends CI_Model{	


 function get_kodevoucher($table) { 
        $tahun = date('y'); 
        $bulan = date('m'); 
        $q = $this->db->query("SELECT MAX(RIGHT(voucherno,4)) AS idmax FROM ".$table);
        $kd = ""; //kode awal
        if($q->num_rows()>0){ //jika data ada
            foreach($q->result() as $k){
                $tmp = ((int)$k->idmax)+1; //string kode diset ke integer dan ditambahkan 1 dari kode terakhir
                $kd = "0000".$tmp; //kode ambil 4 karakter terakhir
                $id = substr($kd, -5);
            }
        }else{ //jika data kosong diset ke kode awal
            $kd = $tahun.$bulan."00001";
        }
        $kar = "01"; //karakter depan kodenya
        //gabungkan string dengan kode yang telah dibuat tadi
        return $kar.$tahun.$bulan.$id;
   }
   

    function load_akun() { 
         $data = $this->db->query("SELECT `NoAkun`,CONCAT(`NamaSub`, ' - '  ,tbakun.`NamaAkun`) AS nama FROM tbakun INNER JOIN `tbsubklasifikasi`
                    ON tbakun.`IdSub`  =  tbsubklasifikasi.`IdSub` ORDER BY tbsubklasifikasi.IdSub ASC");
         return $data->result();

    }

   function Payment_add($tablemasterJurnal,$datamasterJurnal,$tableDetilJurnal,$datadetilJurnal,$tablepayment,
                        $datapayment,$tablepaymentDetil,$datapaymentDetil,$iscredit) { 

       $this->db->trans_strict(FALSE);
       $this->db->trans_start();

       if ($iscredit=='0') {
          $this->db->insert($tablepayment,$datapayment);
        $this->db->insert($tablepaymentDetil,$datapaymentDetil);

      

        //Modul Akuntansi Start
        date_default_timezone_set('Asia/Hong_Kong');
        $this->db->insert($tablemasterJurnal,$datamasterJurnal);
        $this->db->insert_batch($tableDetilJurnal, $datadetilJurnal);

        foreach($datadetilJurnal as $datadetil1){
         $NoAkun = $datadetil1['NoAkun'];
         $Debet = $datadetil1['Debet'];
         $Kredit = $datadetil1['Kredit'];

        
        $dataSetting = $this->db->query("select left(NoAkun,1) as Klas, IsKontra from tbakun where NoAkun = '".$NoAkun."' ");
        foreach($dataSetting->result() as $dataSetting1){
                $Klas = $dataSetting1->Klas;
                $IsKontra = $dataSetting1->IsKontra;
            }
            if(($Klas == '1' or $Klas=='5' or $Klas=='6' or  $Klas=='9') and $IsKontra == '0' ){
                $this->db->query("update tbakun set Saldo = Saldo +".$Debet. " - ".$Kredit." where NoAkun = '".$NoAkun. "'");   
            }else{
                $this->db->query("update tbakun set Saldo = Saldo -".$Debet. " + ".$Kredit." where NoAkun = '".$NoAkun. "'");
            }       
        }
     }
        //Modul Akuntansi End

       
 		$this->db->trans_complete();
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else{
            $this->db->trans_commit();
        }      
   } 

      function Payment_add_manual($tablemasterJurnal,$datamasterJurnal,$tableDetilJurnal,$datadetilJurnal,$tablepayment,
                                   $datapayment,$tablepaymentDetil,$datapaymentDetil) { 

       $this->db->trans_strict(FALSE);
       $this->db->trans_start();

        $this->db->insert($tablepayment,$datapayment);
        $this->db->insert_batch($tablepaymentDetil,$datapaymentDetil);

        foreach ($datapaymentDetil as $pek ) {
            $this->db->set('ispaid', 1);
            $this->db->where('kodero', $pek['kodero']);
            $this->db->update('tbro');
        }

        


        //Modul Akuntansi Start
        date_default_timezone_set('Asia/Hong_Kong');
        $this->db->insert($tablemasterJurnal,$datamasterJurnal);
        $this->db->insert_batch($tableDetilJurnal, $datadetilJurnal);

        foreach($datadetilJurnal as $datadetil1){
         $NoAkun = $datadetil1['NoAkun'];
         $Debet = $datadetil1['Debet'];
         $Kredit = $datadetil1['Kredit'];

        
        $dataSetting = $this->db->query("select left(NoAkun,1) as Klas, IsKontra from tbakun where NoAkun = '".$NoAkun."' ");
        foreach($dataSetting->result() as $dataSetting1){
                $Klas = $dataSetting1->Klas;
                $IsKontra = $dataSetting1->IsKontra;
            }
            if(($Klas == '1' or $Klas=='5' or $Klas=='6' or  $Klas=='9') and $IsKontra == '0' ){
                $this->db->query("update tbakun set Saldo = Saldo +".$Debet. " - ".$Kredit." where NoAkun = '".$NoAkun. "'");   
            }else{
                $this->db->query("update tbakun set Saldo = Saldo -".$Debet. " + ".$Kredit." where NoAkun = '".$NoAkun. "'");
            }       
        }
        //Modul Akuntansi End

       
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else{
            $this->db->trans_commit();
        }      
   } 

   function Payment_add_manual_dropdepartement($tablemasterJurnal,$datamasterJurnal,$tableDetilJurnal,$datadetilJurnal) { 

       $this->db->trans_strict(FALSE);
       $this->db->trans_start();

        //Modul Akuntansi Start
        date_default_timezone_set('Asia/Hong_Kong');
        $this->db->insert($tablemasterJurnal,$datamasterJurnal);
        $this->db->insert_batch($tableDetilJurnal, $datadetilJurnal);

        foreach($datadetilJurnal as $datadetil1){
         $NoAkun = $datadetil1['NoAkun'];
         $Debet = $datadetil1['Debet'];
         $Kredit = $datadetil1['Kredit'];

        
        $dataSetting = $this->db->query("select left(NoAkun,1) as Klas, IsKontra from tbakun where NoAkun = '".$NoAkun."' ");
        foreach($dataSetting->result() as $dataSetting1){
                $Klas = $dataSetting1->Klas;
                $IsKontra = $dataSetting1->IsKontra;
            }
            if(($Klas == '1' or $Klas=='5' or $Klas=='6' or  $Klas=='9') and $IsKontra == '0' ){
                $this->db->query("update tbakun set Saldo = Saldo +".$Debet. " - ".$Kredit." where NoAkun = '".$NoAkun. "'");   
            }else{
                $this->db->query("update tbakun set Saldo = Saldo -".$Debet. " + ".$Kredit." where NoAkun = '".$NoAkun. "'");
            }       
        }
        //Modul Akuntansi End

       
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else{
            $this->db->trans_commit();
        }      
   } 

function acc_payment($kodero,$data) {
      $this->db->trans_strict(FALSE);
    $this->db->trans_start();

        $this->db->insert('tbhistoryhutang',$data);

     $this->db->set('isinvoicing', 0);
    $this->db->where('kodero', $kodero);
    $this->db->update('tbro');

     $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else{
           
    }
}

function payment_add_voucher_addDB($data,$datadetil) {
    // $this->db->trans_strict(FALSE);
    // $this->db->trans_start();

        $this->db->insert('tbcheck',$data);
        $idmaster = $this->db->insert_id();
        foreach ($datadetil as $rowdataDetil) {
        $dataDetilInsert[] = array(
                     'idpayment' => $rowdataDetil['idpayment'],
                     'amount' => $rowdataDetil['amount'], 
                     'idmaster' =>  $idmaster 
                 );
      }
      $this->db->insert_batch('tbcheckdetil',$dataDetilInsert);

     

    //  $this->db->trans_complete();
    //     if ($this->db->trans_status() === FALSE){
    //         $this->db->trans_rollback();
    //     }else{
           
    // }
}

    function load_voucher_all() {
    $data = $this->db->query("SELECT `voucherno`,`voucherdate`,  tbvendor.`vendor`,`total` FROM tbpayment INNER JOIN `tbvendor`
                                ON tbpayment.`vendor`  = tbvendor.`idvendor` order by voucherno desc  ");
    return $data->result();

   } 

   function load_voucher($KodeVoucher) {
    $data = $this->db->query("SELECT voucherdate, tbpayment.voucherno,tbvendor.`vendor`, paymentmethod,total, rodate, kodero,description ,tbpaymentdetil.amount FROM tbpayment INNER JOIN tbpaymentdetil
                            ON tbpayment.voucherno =  tbpaymentdetil.voucherno INNER JOIN `tbvendor`
                            ON tbpayment.`vendor`  = tbvendor.`idvendor` where tbpayment.voucherno = '$KodeVoucher' ");
    return $data->result();

   } 

    function load_ro() {
        $data = $this->db->query("SELECT `kodero`,DATE_FORMAT(`datereceive`,'%d-%m-%Y') AS datereceive ,`vendor`,`noreceipt`,`total` FROM `tbro` order by kodero desc" );
        return $data->result();

   } 

   function load_MI($KodeRo) {
    $data = $this->db->query("SELECT tbro.`kodero`,DATE_FORMAT(`datereceive`,'%d-%m-%Y') AS datereceive ,`vendor`,`noreceipt`,`itemcode`,`unit`,`description`,`price`,`subtotal` 
                            FROM `tbro` INNER JOIN tbrodetil ON tbro.kodero = tbrodetil.kodero where tbro.kodero = '$KodeRo' ");
    return $data->result();
 
}
function load_detilmi_byvoucher($KodeVoucher) {
    $data = $this->db->query("SELECT tbro.`kodero`,DATE_FORMAT(`datereceive`,'%d-%m-%Y') AS datereceive ,qty,`vendor`,`noreceipt`,`itemcode`,`unit`,`description`,`price`,`subtotal` 
              FROM `tbro` INNER JOIN tbrodetil ON tbro.kodero = tbrodetil.kodero 
WHERE tbro.kodero IN(SELECT kodero FROM tbpaymentdetil WHERE voucherno = '$KodeVoucher') ");
    return $data->result();
 
}


function load_MI_detil($KodeRo) {
    $data = $this->db->query("SELECT * from tbrodetil where kodero = '$KodeRo' ");
    return $data->result();
 
}

function load_voucher_unposting() {
    $data = $this->db->query("SELECT voucherno FROM `tbpayment` WHERE isposting = '0' ");
    return $data->result();
 
}

function load_dropdepartement_byro($kodero) {
    $data = $this->db->query("SELECT iddepartement FROM `tbro` WHERE kodero = '$kodero' ");

    $Departement = $data->row()->iddepartement;
    if ($data->row()->iddepartement !=0) {
       if ($Departement==1) { 
        $akun = 'akunBO';
       }elseif ($Departement==2) {
        $akun = 'akunHK';
       }elseif ($Departement==3) {
         $akun = 'akunFO';
       }elseif ($Departement==4) {
          $akun = 'akunFB';
       }elseif ($Departement==5) {
        $akun = 'akunBO';
       }elseif ($Departement==6) {
        $akun = 'akunBar';
      }elseif ($Departement==7) {
        $akun = 'akunEng';
      }elseif ($Departement==8) {
        $akun = 'akunPG';
      }elseif ($Departement==9) {
        $akun = 'akunSec';
       }
      $dataItem = $this->db->query("SELECT $akun as akun,subtotal FROM tbrodetil INNER JOIN tbitem
                                    ON tbrodetil.`itemcode` =  tbitem.`codeitem` 
                                    WHERE kodero = '$kodero' ");
      return $dataItem->result();
     }else{
      return '';
     }
 
}


function select_vendor_by_id($id) {
        $data = $this->db->query("select * from tbvendor where idvendor = $id ");
     return $data->result();
    }

    function load_total_voucher_byid($id) {
        $data = $this->db->query("SELECT SUM(subtotal) AS total FROM tbrodetil WHERE kodero IN(
                                SELECT kodero FROM `tbpaymentdetil` WHERE voucherno = '01171200168'
                                ) 
                                UNION ALL
                                SELECT SUM(subtotal) AS total FROM tbrodetil WHERE kodero IN(
                                SELECT kodero FROM `tbpaymentdetil` WHERE voucherno = '$id'
                                ) AND tbrodetil.`isposting` = 0
                                ");
     return $data->result();
    }

    function load_mi_unpaid_by_voucher($id) {
        $data = $this->db->query("SELECT * FROM tbrodetil WHERE kodero IN 
                                (SELECT kodero FROM tbpaymentdetil WHERE voucherno = '$id')
                                AND tbrodetil.isposting = 0
                                ");
     return $data->result();
    }
    function load_bank() {
        $data = $this->db->query("SELECT * FROM tbbank order by id asc
                                ");
     return $data->result();
    }
    function load_check_all() {
        $data = $this->db->query(" SELECT tbcheck.*, DATE_FORMAT(tglinput,'%d-%m-%Y') AS tglview, namabank FROM tbcheck 
INNER JOIN tbbank ON tbcheck.`bank` = tbbank.`id` ORDER BY id ASC");
     return $data->result();
    }
    function load_mi_bycheck($nocheck) {
        $data = $this->db->query("SELECT tbpayment.*,tbvendor.vendor FROM tbpayment INNER JOIN tbvendor
                                ON tbpayment.`vendor` = tbvendor.`idvendor` WHERE checkno = '$nocheck'  ORDER BY voucherno ASC");
     return $data->result();
    }

    
    function load_voucher_byvendor($idvendor) {
        $data = $this->db->query("SELECT tbpayment.*,tbvendor.vendor, date_format(voucherdate,'%d-%m-%Y') as tglvoucher FROM `tbpayment` INNER JOIN tbvendor 
                                    ON tbpayment.`vendor` =  tbvendor.`idvendor` WHERE idvendor='$idvendor'");
     return $data->result();
    }

    

     function load_Hutang_byvendor($tglawal, $tglakhir,$tgper,$idvendor,$format) {
        if ($format =='1') {
           $data = $this->db->query("SELECT kodero, DATE_FORMAT(datereceive,'%d-%m-%y') AS datereceive , DATE_FORMAT(duedate,'%d-%m-%y') AS duedate, total,vendor FROM `tbro` WHERE isinvoicing = '1' AND  date(datereceive) >='$tglawal' AND date(datereceive) <= '$tglakhir' 
        and idvendor= '$idvendor' ");
        }else{
             $data = $this->db->query("SELECT kodero, DATE_FORMAT(datereceive,'%d-%m-%y') AS datereceive , DATE_FORMAT(duedate,'%d-%m-%y') AS duedate, total,vendor FROM `tbro` WHERE isinvoicing = '1' AND  date_format(datereceive,'%y-%m-%d') <= '$tgper'  
        and idvendor= '$idvendor' ");

        }
    
    return $data->result();
 
}

  function load_check_report($tglsampai, $idvendor,$bank) {
         if ($bank!=0 and $idvendor!=0 ) {
            $where  = "WHERE tglinput <='".$tglsampai."' and tbcheck.bank='$bank' and tbvendor.idvendor ='$idvendor' ";
         }elseif ($idvendor!=0) {
             $where  = "WHERE tglinput <='".$tglsampai."' and tbvendor.idvendor ='$idvendor'  ";
        }elseif ($bank!=0) {
             $where  = "WHERE tglinput <='".$tglsampai."' and tbcheck.bank='$bank'  ";
         }else{
            $where  = "WHERE tglinput <= '".$tglsampai."' ";
         }
           $data = $this->db->query("SELECT DATE_FORMAT(tglinput,'%d-%m-%Y')AS tglinput,checkno, tbvendor.`vendor`, amount,namabank  FROM `tbcheck` INNER JOIN tbcheckdetil
            ON tbcheck.`id` =  tbcheckdetil.`idmaster` INNER JOIN tbvendor
            ON tbcheck.`idvendor` =  tbvendor.`idvendor` INNER JOIN tbbank
            ON tbcheck.`bank` =  tbbank.`id` ".$where );
        
    
    return $data->result();
 
}

 function load_cash_report($tglsampai, $idvendor) {
         if ($idvendor!=0) {
             $where  = "WHERE voucherdate <='".$tglsampai."' and tbvendor.idvendor ='$idvendor' and paymentmethod=0  ";
         }else{
            $where  = "WHERE voucherdate <= '".$tglsampai."' paymentmethod=0 ";
         }
           $data = $this->db->query("SELECT DATE_FORMAT(voucherdate,'%d-%m-%Y')AS tglinput,checkno, tbvendor.`vendor`, total  FROM `tbpayment` INNER JOIN tbvendor ON tbpayment.`vendor` = tbvendor.`idvendor` ".$where );

//         SELECT DATE_FORMAT(voucherdate,'%d-%m-%Y')AS tglinput,checkno, tbvendor.`vendor`, total FROM `tbpayment` 
// INNER JOIN tbvendor ON tbpayment.`vendor` = tbvendor.`idvendor` WHERE voucherdate <='2018-12-02' 
    
    return $data->result();
}

    function load_rekap_voucher($tgldari,$tglsampai, $method) {
         if ($method==0) {
             $data = $this->db->query("SELECT tbpayment.`voucherno`, tbpayment.`voucherdate`,paymentmethod,tbcheck.checkno, 
                date_format(tbpayment.`voucherdate`,'%d-%m-%Y') as voucherdate1,
             tbvendor.`vendor`,tbpayment.`total`,tbbank.`namabank`, tbcheck.`total` AS totalcheck FROM tbpayment LEFT JOIN tbcheckdetil
            ON tbpayment.`voucherno` =  tbcheckdetil.`idpayment` LEFT JOIN tbcheck
            ON tbcheckdetil.`idmaster` = tbcheck.`id` LEFT JOIN tbbank
            ON tbcheck.`bank` =  tbbank.`id` LEFT JOIN tbvendor 
            ON tbpayment.`vendor` = tbvendor.`idvendor` WHERE   date(tbpayment.`voucherdate`) >='$tgldari' AND date(tbpayment.`voucherdate`)
             <= '$tglsampai' ");
         }elseif ($method==1) {
            $data = $this->db->query("SELECT tbpayment.`voucherno`, tbpayment.`voucherdate`,paymentmethod,
                 date_format(tbpayment.`voucherdate`,'%d-%m-%Y') as voucherdate1,
             tbvendor.`vendor`,tbpayment.`total` FROM tbpayment  INNER JOIN tbvendor 
            ON tbpayment.`vendor` = tbvendor.`idvendor` WHERE   date(tbpayment.`voucherdate`) >='$tgldari' AND date(tbpayment.`voucherdate`)
             <= '$tglsampai'  and paymentmethod=0 ");
         }else{
           $data = $this->db->query("SELECT tbpayment.`voucherno`, tbpayment.`voucherdate`,tbcheck.checkno, 
                date_format(tbpayment.`voucherdate`,'%d-%m-%Y') as voucherdate1, date_format(tbcheck.`tglinput`,'%d-%m-%Y') as tglinput1,
             tbvendor.`vendor`,tbpayment.`total`,tbbank.`namabank`, tbcheck.`total` AS totalcheck FROM tbpayment INNER JOIN tbcheckdetil
            ON tbpayment.`voucherno` =  tbcheckdetil.`idpayment` INNER JOIN tbcheck
            ON tbcheckdetil.`idmaster` = tbcheck.`id` INNER JOIN tbbank
            ON tbcheck.`bank` =  tbbank.`id` INNER JOIN tbvendor 
            ON tbpayment.`vendor` = tbvendor.`idvendor` WHERE   date(tbpayment.`voucherdate`) >='$tgldari' AND date(tbpayment.`voucherdate`)
             <= '$tglsampai'  and paymentmethod=1 ");
         }

    return $data->result();
 
     }





  

  

  


      



}
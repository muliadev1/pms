<?php 

class Rbac extends CI_Controller{

	function __construct(){
		$this->CI =& get_instance(); 
		parent::__construct();		
		$this->load->model(array('M_login','M_rbac'));
		$this->load->helper(array('url'));	
		$this->load->library( array('create_menu'));	 

		
		if (!empty($this->session->userdata("active_db"))) {
			$this->load->database($this->session->userdata("active_db"), FALSE, TRUE);
		}
		
	}


		function index(){
				redirect('Dashboard/index');
			}

	  


	 function change_role(){
			$username = $this->session->userdata('UsernamePms');
			$password = $this->session->userdata('PasswordPms');
			$id_role_new = $this->input->post('id_role');
			$result = $this->M_login->getdataOP($username, $password)->result();

		
			$role_user =  $this->M_rbac->get_role_user($result[0]->id_user);
			$role_user_new =  $this->M_rbac->get_role_user_new($id_role_new);

			$menu_user =  $this->M_rbac->get_menu_user($id_role_new);
			$menu_aktif =  $this->create_menu->generate_menu($menu_user);		
			$perusahaan = $this->M_rbac->getPerusahaan();
			$submenu_all = $this->M_rbac->getLinkSubmenu();
			$departement =  $this->M_rbac->get_departement_user($result[0]->id_user); 
			
			$user_sessionPms= array(
				'session_idPms' => $this->session->userdata('session_id'),
				'Kode_userPms' => $result[0]->id_user,
				'Nama_userPms' => $result[0]->nama_user,
				'UsernamePms' => $result[0]->username,
				'PasswordPms' => $result[0]->password,
				'ParentPms' => $result[0]->Parent,
				'id_ParentPms' => $result[0]->Id_Parent,
				'id_DepartementPms' => $departement->iddepartement,
				'rolePms'			=> $role_user,
				'role_aktifPms'	=> $role_user_new[0]->nama_role,
				'id_role_aktifPms'	=> $id_role_new,
				'submenu_allPms'	=> $submenu_all,
				'menu_aktifPms'	=> $menu_aktif,
				'NamaPerusahaan'=> $perusahaan[0]->NamaPerusahaan,
				'AlamatPerusahaan'=> $perusahaan[0]->Alamat,
				'NoTlpPerusahaan'=> $perusahaan[0]->NoTlp,
				'KodeKategoriAset'=> $perusahaan[0]->KategoriAset

			);
			$this->session->set_userdata($user_sessionPms);


			$response= site_url('Rbac');
			echo json_encode($response);
	}



	function change_tgl(){
		$from1 = $this->input->post('Tgl');
		$date1 = DateTime::createFromFormat('m/d/Y',$from1);
		$from_date1 = $date1->format("Y-m-d");

		$from2 = $this->session->userdata('TanggalSistem');
		$date2 = DateTime::createFromFormat('m/d/Y',$from2);
		$from_date2 = $date2->format("Y-m-d");
		$data = array(	'TanggalSistemSebelumnya' => $from_date2,'TanggalSistemAktif' =>$from_date1);
		$this->M_rbac->addTglSistem($data);

			$username = $this->session->userdata('UsernameKop');
			$password = $this->session->userdata('PasswordKop');
			$id_role_new = $this->session->userdata('id_role_aktifKop'); 
			$result = $this->M_login->getdataOP($username, $password)->result();
		
			$role_user =  $this->M_rbac->get_role_user($result[0]->id_user);
			$role_user_new =  $this->M_rbac->get_role_user_new($id_role_new);

			$menu_user =  $this->M_rbac->get_menu_user($id_role_new);
			$menu_aktif =  $this->create_menu->generate_menu($menu_user);		
			$koperasi = $this->M_rbac->getKoperasi();
			$tgl_sistem = $this->M_rbac->getTanggalSistem();

			$from = $tgl_sistem[0]->TanggalSistemAktif;
			$date = DateTime::createFromFormat('m/d/Y',$from);
			$from_date = $date->format("d-m-Y");
			
			$user_sessionKop = array(
				'session_idKop' => $this->session->userdata('session_id'),
				'Kode_userKop' => $result[0]->id_user,
				'Nama_userKop' => $result[0]->nama_user,
				'UsernameKop' => $result[0]->username,
				'PasswordKop' => $result[0]->password,
				'ParentKop' => $result[0]->Parent,
				'id_ParentKop' => $result[0]->Id_Parent,
				'roleKop'		=> $role_user,
				'role_aktifKop'	=> $role_user_new[0]->nama_role,
				'id_role_aktifKop'	=>$id_role_new,
				'menu_aktifKop'	=> $menu_aktif,
				'NamaKoperasi'=> $koperasi[0]->NamaKoperasi,
				'Alamat'=> $koperasi[0]->Alamat,
				'TanggalSistem'=> $tgl_sistem[0]->TanggalSistemAktif,
				'TanggalSistemView'=> $from_date

			);
			$this->session->set_userdata($user_sessionKop);
			$this->session->set_flashdata('msg', 'Perubahan Tanggal Sistem Berhasil...');
			$response= site_url('Rbac');
			echo json_encode($response);
	}
}
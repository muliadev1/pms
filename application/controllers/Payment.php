<?php

class Payment extends CI_Controller{
	private $IdOperator;
	private $IdVendorDirect;

	function __construct(){
		parent::__construct();
		if($this->session->userdata('Kode_userPms')==NULL) {
           redirect('Login');
		}
		$this->load->library(array('session','user_check','akuntan_function','create_pdf','ciqrcode'));
		$this->load->model(array('M_purchase_requisition','M_payment','M_vendor','M_receive_order'));
		$this->IdOperator = $this->session->userdata('Kode_userPms');
		$this->IdVendorDirect = '89';
		
		if (!empty($this->session->userdata("active_db"))) {
			$this->load->database($this->session->userdata("active_db"), FALSE, TRUE);
		}
	}

	function index(){
	//	$this->load->view('dashboard1');
//$this->load->view('dashboard1');
	//redirect('Payment_entry/payment_entry');

	}
	
	function LaporanAkuntingHome(){
		$data['datavendor'] = $this->M_vendor->Vendor_view();
		$data['bank'] = $this->M_payment->load_bank();
		$data['voucherno'] = $this->M_payment->load_novoucher();
		$this->load->view('purchase_order/Cetak/Home_back_office',$data);
	}
	function hutang(){
		$data['datavendor'] = $this->M_vendor->Vendor_view();
		$this->load->view('payment/Hutang',$data);
	}
	function hutang_koreksi(){
		$data['datavendor'] = $this->M_vendor->Vendor_view();
		$this->load->view('payment/Hutang_koreksi',$data);
	}
	function check_view(){
		$data['data'] =$this->M_payment->load_check_all();
		$this->load->view('payment/Check',$data);
	}
	function Check_view_data(){
		$idcek = $this->input->post('idcek');
		$data['data'] =$this->M_payment->load_mi_bycheck($idcek);
		$this->load->view('payment/Check_data',$data);
	}
	function Check_view_data_detil(){
		$idv = $this->input->post('idv');
		$data['data'] =$this->M_payment->load_detilmi_byvoucher($idv);
		//print_r($data['data']);exit();
		$this->load->view('payment/Check_data_detil',$data);
	}
	
	function viwedetilmi(){
		$NOMI = $this->input->post('nomi');
		//print_r($NOMI);exit();
		$data['mi'] = $this->M_payment->load_MI_detil($NOMI);
		$this->load->view('payment/Hutang_mi_detil',$data);
	}
	
	function hutang_data(){
		$format = $this->input->post('format');
		$TglAwal = $this->input->post('TglAwalHutang');
		$TglAkhir = $this->input->post('TglAkhirHutang');
		$TglPer = $this->input->post('TglPerHutang');
		$IdVendor =$this->input->post('IdVendor');

		if ($format ==1) {
		    $TglAwal1 = DateTime::createFromFormat('m/d/Y',$TglAwal);
		    $TglAwal2 = $TglAwal1->format("Y-m-d");
		    $TglAkhir1 = DateTime::createFromFormat('m/d/Y',$TglAkhir);
		    $TglAkhir2 = $TglAkhir1->format("Y-m-d");
		}else{
			$TglPer1 = DateTime::createFromFormat('m/d/Y',$TglPer);
		    $TglPer2 = $TglPer1->format("Y-m-d");
		}
	   $data['idvendor'] = $IdVendor;
		$data['datahutang'] = $this->M_payment->load_Hutang_byvendor($TglAwal2, $TglAkhir2,$TglPer2,$IdVendor,$format);
		$this->load->view('payment/Hutang_data',$data);
	}
	function hutang_koreksi_data(){
		$format = $this->input->post('format');
		$TglAwal = $this->input->post('TglAwalHutang');
		$TglAkhir = $this->input->post('TglAkhirHutang');
		$TglPer = $this->input->post('TglPerHutang');
		$IdVendor =$this->input->post('IdVendor');

		if ($format ==1) {
		    $TglAwal1 = DateTime::createFromFormat('m/d/Y',$TglAwal);
		    $TglAwal2 = $TglAwal1->format("Y-m-d");
		    $TglAkhir1 = DateTime::createFromFormat('m/d/Y',$TglAkhir);
		    $TglAkhir2 = $TglAkhir1->format("Y-m-d");
		}else{
			$TglPer1 = DateTime::createFromFormat('m/d/Y',$TglPer);
		    $TglPer2 = $TglPer1->format("Y-m-d");
		}
	   $data['idvendor'] = $IdVendor;
		$data['datahutang'] = $this->M_payment->load_Hutang_koreksi_byvendor($TglAwal2, $TglAkhir2,$TglPer2,$IdVendor,$format);
		$this->load->view('payment/Hutang_koreksi_data',$data);
	}

	function payment_entry(){
		$data['data'] = $this->M_purchase_requisition->item_view();
		$data['datavendor'] = $this->M_vendor->Vendor_view();
		$data['dataakun'] = $this->M_payment->load_akun();
		$this->load->view('payment/Payment_entry',$data);
	}

	function payment_entry_direct(){
		$data['data'] = $this->M_purchase_requisition->item_view();
		$data['dataakun'] = $this->M_payment->load_akun();
		$data['datavendor'] = $this->M_vendor->Vendor_view();

		$this->load->view('payment/Payment_entry_direct',$data);
	}
	function payment_add_voucher(){
		$data['datavendor'] = $this->M_vendor->Vendor_view();
		$data['bank'] = $this->M_payment->load_bank();
		$this->load->view('payment/Payment_entry_add_check',$data);
	}
	function view_voucher_byvendor(){
		$idv = $this->input->post('idvendor');
		$data['datav'] = $this->M_payment->load_voucher_byvendor($idv);
		$this->load->view('payment/Daftar_voucher',$data);
	}
	function payment_add_voucher_addDB(){
		$datadetil = $this->input->post('datadetil');
		$Total=0;
		$Jumlah = count($datadetil)-1;
		for ($i=0; $i <= $Jumlah ; $i++) { 
		    $amount = str_replace('.', '', $datadetil[$i][2]); 
            $amount = str_replace(',', '.', $amount);

			$datacheckdetil[] = array(
		 		'idpayment' => $datadetil[$i][0],
				'amount' => $amount
 			);
				 $Total+= $amount;
		}
		//print_r($datacheckdetil);exit();

		$datacheck = array(
				'checkno'=>$this->input->post('check'),
				'bank'=>$this->input->post('bank'),
				'idvendor'=>$this->input->post('idvendor'),
				'total'=>$Total,
				'idoperator'=>$this->IdOperator,
				'tglinput'=>date('Y-m-d H:i:s')
 			);

				$this->M_payment->payment_add_voucher_addDB($datacheck,$datacheckdetil);
				$this->session->set_flashdata('msg', 'Add Check successfully...');
		
	}



	function payment_add(){
		//Payment
		$Total = 0;
		$amount = $this->input->post('amount1');
		$idvendor = $this->input->post('IdVendor');
		$Method = $this->input->post('Method'); 
		$invoice = $this->input->post('invoice');
		$amount= $this->input->post('amount1');
		$akun= $this->input->post('Akun');

		if ($Method=='1') {
			$paymentmethod =0; //cash
			$checkno ='-';
		}else{
			$paymentmethod =1; //check
			$checkno =$this->input->post('check');	
		}
		//print_r($amount);exit();

		$kodeVoucher =$this->M_payment->get_kodevoucher('tbpayment');
 		

		$amount1 = array_combine(range(1, count($amount)), array_values($amount));
		$invoice1 = array_combine(range(1, count($invoice)), array_values($invoice));
		
		$Jumlah = count($amount);
		for ($i=1; $i <= $Jumlah ; $i++) { 
			$dataPayDetil[] = array('voucherno' => $kodeVoucher,
		 		'amount' => $amount1[$i],
				'description' => '',
				'rodate' => date('Y-m-d H:i:s'),
				'kodero' => $invoice1[$i]
 			);

			
				 $Total+= $amount1[$i];
		}

		$dataPay =  array('voucherno' => $kodeVoucher,
		 		'voucherdate' => date('Y-m-d H:i:s'),
		 		'vendor' => $idvendor,
				'address' => '-',
				'paymentmethod' => $paymentmethod ,
				'bank' => '-',
				'checkno' => $checkno,
				'total' => $Total,
				'idoperator'=> $this->IdOperator
			);
				//print_r($dataPay);exit();
		$dataAkun =  array('NamaTransaksi' => 'Voucher'.' '.$kodeVoucher,
		 		'AkunDebet' => $akun,
		 		'AkunKredit' => '11001'
			);

		
 				$dataJurnal = $this->akuntan_function->generate_jurnal('2',$kodeVoucher,$Total, $this->IdOperator,$dataAkun);
				$dataDetilJurnal=  array($dataJurnal[1],$dataJurnal[2]);
				$dataMasterJurnal = $dataJurnal[0];
				$this->M_payment->Payment_add_manual('tbmasterjurnal',$dataMasterJurnal,'tbdetiljurnal',$dataDetilJurnal,
									  'tbpayment',$dataPay,'tbpaymentdetil',$dataPayDetil);
				
			
			//jika dia drop ke departement
				for ($i=1; $i <= $Jumlah ; $i++) { 
					$dataItem = $this->M_payment->load_dropdepartement_byro($invoice1[$i]);
					if (!empty($dataItem)) {
						
						foreach ($dataItem as $row) {
							$dataAkunDrop =  array('NamaTransaksi' => 'RO'.' '.$invoice1[$i],
											 		'AkunDebet' => $row->akun,
											 		'AkunKredit' => '12001'
													);	
							$dataJurnalDrop = $this->akuntan_function->generate_jurnal('',$invoice1[$i],$row->subtotal, 
							$this->IdOperator,$dataAkunDrop);

							$dataMasterJurnaldrop = $dataJurnalDrop[0];
							$dataDetilJurnaldrop=  array($dataJurnalDrop[1],$dataJurnalDrop[2]);
							
							$this->M_payment->Payment_add_manual_dropdepartement('tbmasterjurnal',$dataMasterJurnaldrop,
							'tbdetiljurnal',$dataDetilJurnaldrop);
							
					}

						
				}
						
			}
		
				
				$this->session->set_flashdata('msg', 'Add Payment successfully...');
				$this->session->set_flashdata('kodepv', $kodeVoucher);
				redirect('Payment/payment_entry','refresh');
	}



		function payment_add_direct(){
		//Payment
		$Total = 0;
		$amount = $this->input->post('amount');
		$idvendor = $this->input->post('IdVendor');
		$Method = $this->input->post('Method'); 
		$invoice = $this->input->post('invoice');
		$amount= $this->input->post('amount1');
		$description= $this->input->post('Description');
		$akun= $this->input->post('Akun');

		if ($Method=='1') {
			$paymentmethod =0; //cash
			$checkno ='-';
		}else{
			$paymentmethod =1; //check
			$checkno =$this->input->post('check');	
		}
		//print_r($amount);exit();

		$kodeVoucher =$this->M_payment->get_kodevoucher('tbpayment');
 		

		$amount1 = array_combine(range(1, count($amount)), array_values($amount));
		$invoice1 = array_combine(range(1, count($invoice)), array_values($invoice));
		
		$Jumlah = count($amount);
		for ($i=1; $i <= $Jumlah ; $i++) { 
			
			$AmountSave = str_replace('.', '', $amount1[$i]); 
			$AmountSave = str_replace(',', '.', $AmountSave);


			$dataPayDetil[] = array('voucherno' => $kodeVoucher,
		 		'amount' => $AmountSave,
				'description' => $description[$i],
				'rodate' => date('Y-m-d H:i:s'),
				'kodero' => $invoice1[$i]
 			);
			
				 $Total+= $AmountSave;
		}


		$dataPay =  array('voucherno' => $kodeVoucher,
		 		'voucherdate' => date('Y-m-d H:i:s'),
		 		'vendor' => $this->IdVendorDirect,
				'address' => '-',
				'paymentmethod' => $paymentmethod ,
				'bank' => '-',
				'checkno' => $checkno,
				'total' => $Total,
				'idoperator'=> $this->IdOperator
			);
				//print_r($dataPayDetil);exit(); pertama kali drop ke persediaan
				$dataAkun =  array('NamaTransaksi' => 'Voucher'.' '.$kodeVoucher,
				 		'AkunDebet' => $akun,
				 		'AkunKredit' => '11001'
					);
 		
 				$dataJurnal = $this->akuntan_function->generate_jurnal('',$kodeVoucher,$Total, $this->IdOperator,$dataAkun);
				$dataDetilJurnal=  array($dataJurnal[1],$dataJurnal[2]);
				$dataMasterJurnal = $dataJurnal[0];
				$this->M_payment->Payment_add_manual('tbmasterjurnal',$dataMasterJurnal,'tbdetiljurnal',$dataDetilJurnal,
									  'tbpayment',$dataPay,'tbpaymentdetil',$dataPayDetil);

				$this->session->set_flashdata('msg', 'Add Payment Order successfully...');
				$this->session->set_flashdata('kodepv', $kodeVoucher);
				redirect('Payment/payment_entry_direct','refresh');
	}




function acc_payment(){
			$kodero = $this->input->post('kodero');
			$data =  array( 'ispaid'=>0,
							'isinvoicing' => 0 
							 );

			$this->M_payment->acc_payment($kodero,$data);
		}

function acc_payment_koreksi(){
			$kodero = $this->input->post('kodero');
			$data =  array( 'ispaid'=>0,
							'isinvoicing' => 1 
							 );

			$this->M_payment->acc_payment($kodero,$data);
		}

	function receive_history_unpaid_by_vendor(){
			$idvendor = $this->input->post('idvendor');
			$data['data'] = $this->M_receive_order->load_receive_unpaid_by_vendor($idvendor);
			$this->load->view('receive_item/Daftar_receive_unpaid',$data);
		}

	function mi_unpaid_by_voucher(){
			$idvendor = $this->input->post('id');
			$data['data'] = $this->M_payment->load_mi_unpaid_by_voucher($idvendor);
			$this->load->view('payment/Daftar_item_payment_posting',$data);
	}

	function select_vendor_by_id(){
			$idvendor = $this->input->post('id');
			$data = $this->M_payment->select_vendor_by_id($idvendor);
			echo json_encode($data);
	}
	function select_total_voucher_byid(){
			$novo = $this->input->post('id');
			$data = $this->M_payment->load_total_voucher_byid($novo);
			echo json_encode($data);
	}

	function Cetak($JenisLaporan,$Text){
		$data['hotel'] = $this->M_rbac->getPerusahaan();
		$data['qr'] = $this->ciqrcode->generateQRcode($JenisLaporan, $Text,1.9);
		$data1= $this->load->view('template/Cetak_head',$data, TRUE);
		return $data1;
	}

	function Cetak_Hutang_byvendor(){
	   $format = $this->input->get('format',TRUE);
		$TglAwal = $this->input->get('TglAwalHutang',TRUE);
		$TglAkhir = $this->input->get('TglAkhirHutang',TRUE);
		$TglPer = $this->input->get('TglPerHutang',TRUE);
		$IdVendor =$this->input->get('IdVendor',TRUE);
		//print_r($format);exit();

		if ($format ==1) {
		    $TglAwal1 = DateTime::createFromFormat('m/d/Y',$TglAwal);
		    $TglAwal2 = $TglAwal1->format("Y-m-d");
		    $TglAwal3 = $TglAwal1->format("d-m-Y");

		    $TglAkhir1 = DateTime::createFromFormat('m/d/Y',$TglAkhir);
		    $TglAkhir2 = $TglAkhir1->format("Y-m-d");
		    $TglAkhir3 = $TglAkhir1->format("d-m-Y");
		}else{
			$TglPer1 = DateTime::createFromFormat('m/d/Y',$TglPer);
		    $TglPer2 = $TglPer1->format("Y-m-d");
		     $TglPer3 = $TglPer1->format("d-m-Y");
		}
$data['periode'] =  array('TglAwal' => $TglAwal3 , 'TglAkhir' => $TglAkhir3,'TglPer' => $TglPer3,'format' => $format,'idvendor' => $IdVendor);
	    $data['header'] = $this->Cetak("Memorandum_invoice",  $this->session->userdata('Nama_userPms'). date("d-m-Y_H-i-s"), 1);
		$data['konten'] = $this->M_payment->load_Hutang_byvendor($TglAwal2, $TglAkhir2,$TglPer2,$IdVendor,$format);
		 $html=$this->load->view('payment/cetak/Hutang_vendor',$data,TRUE);

	  $this->create_pdf->load($html,'Hutang'.' '.$TglAwal3.' '.'-'.' '.$TglAkhir3, 'A4-P','');
		
		
	}


	function Cetak_payment_cek(){
		$TglSampaiCek = $this->input->get('DateCek',TRUE);
		$IdVendor =$this->input->get('IdVendor',TRUE);
		$Bank =$this->input->get('BankCek',TRUE);
		
	    $TglSampaiCek1 = DateTime::createFromFormat('m/d/Y',$TglSampaiCek);
	    $TglSampaiCek2 = $TglSampaiCek1->format("Y-m-d");
		$TglSampaiCek3 = $TglSampaiCek1->format("d-m-Y");
		//print_r($IdVendor);exit();
		$data['periode'] =  array('TglSampaiCek' => $TglSampaiCek3 ,'Bank' => $Bank,'vendor'=> $IdVendor,'format'=> '-');
	    $data['header'] = $this->Cetak("Paymemnt",  $this->session->userdata('Nama_userPms'). date("d-m-Y_H-i-s"), 1);
		$data['konten'] = $this->M_payment->load_check_report($TglSampaiCek2, $IdVendor,$Bank);
		
		 $html=$this->load->view('purchase_order/cetak/Payment_check',$data,TRUE);
//print_r($html);exit();
	  $this->create_pdf->load($html,'Payment_check'.' '.$TglSampaiCek2.' '.'-'.' '.$Method, 'A4-P','');
		
		
	}


function Cetak_payment_cek_periode(){
		$TglSampaiCek = $this->input->get('DateCek',TRUE);
		$TglDariCek = $this->input->get('DateCekFromPeriode',TRUE);
		$IdVendor =$this->input->get('IdVendor',TRUE);
		$Bank =$this->input->get('BankCek',TRUE);
		
	    $TglSampaiCek1 = DateTime::createFromFormat('m/d/Y',$TglSampaiCek);
	    $TglSampaiCek2 = $TglSampaiCek1->format("Y-m-d");
		$TglSampaiCek3 = $TglSampaiCek1->format("d-m-Y");

		  $TglDariCek1 = DateTime::createFromFormat('m/d/Y',$TglDariCek);
	    $TglDariCek2 = $TglDariCek1->format("Y-m-d");
		$TglDariCek3 = $TglDariCek1->format("d-m-Y");
		//print_r($IdVendor);exit();
		$data['periode'] =  array('TglDariCek' => $TglDariCek3 ,'TglSampaiCek' => $TglSampaiCek3 ,'Bank' => $Bank,'vendor'=> $IdVendor,'format'=> 'periode');
	    $data['header'] = $this->Cetak("Paymemnt",  $this->session->userdata('Nama_userPms'). date("d-m-Y_H-i-s"), 1);
		$data['konten'] = $this->M_payment->load_check_report_periode($TglDariCek2,$TglSampaiCek2, $IdVendor,$Bank);
		
		 $html=$this->load->view('purchase_order/cetak/Payment_check',$data,TRUE);
//print_r($html);exit();
	  $this->create_pdf->load($html,'Payment_check'.' '.$TglSampaiCek2.' '.'-'.' '.$Method, 'A4-P','');
		
		
	}

	function Cetak_payment_cash(){
		$TglSampaiCek = $this->input->get('DateCash',TRUE);
		$IdVendor =$this->input->get('IdVendorCash',TRUE);
		
	    $TglSampaiCek1 = DateTime::createFromFormat('m/d/Y',$TglSampaiCek);
	    $TglSampaiCek2 = $TglSampaiCek1->format("Y-m-d");
		$TglSampaiCek3 = $TglSampaiCek1->format("d-m-Y");
		//print_r($IdVendor);exit();
		$data['periode'] =  array('TglSampaiCek' => $TglSampaiCek3 ,'vendor'=> $IdVendor);
	    $data['header'] = $this->Cetak("Paymemnt",  $this->session->userdata('Nama_userPms'). date("d-m-Y_H-i-s"), 1);
		$data['konten'] = $this->M_payment->load_cash_report($TglSampaiCek2, $IdVendor);
		
		 $html=$this->load->view('purchase_order/cetak/Payment_cash',$data,TRUE);
//print_r($html);exit();
	  $this->create_pdf->load($html,'Payment_cash'.' '.$TglSampaiCek2.' '.'-'.' '.$Method, 'A4-P','');
			
	}

	function Cetak_rekap_voucher(){
		$TglDari = $this->input->get('DateFromVo',TRUE);
		$TglSampai = $this->input->get('DateToVo',TRUE);
		$Method =$this->input->get('MethodVoucher',TRUE);

		
	    $TglSampai1 = DateTime::createFromFormat('m/d/Y',$TglSampai);
	    $TglSampai2 = $TglSampai1->format("Y-m-d");
		$TglSampai3 = $TglSampai1->format("d-m-Y");

		$TglDari1 = DateTime::createFromFormat('m/d/Y',$TglDari);
	    $TglDari2 = $TglDari1->format("Y-m-d");
		$TglDari3 = $TglDari1->format("d-m-Y");

		$data['periode'] =  array('TglSampai' => $TglSampai3 ,'TglDari' => $TglDari3 ,'Method'=> $Method);
	    $data['header'] = $this->Cetak("Voucher-Rekap",  $this->session->userdata('Nama_userPms'). date("d-m-Y_H-i-s"), 1);
		$data['konten'] = $this->M_payment->load_rekap_voucher($TglDari2,$TglSampai2, $Method);
		if ($Method==0){ 
          $html=$this->load->view('payment/cetak/Voucher_rekap',$data,TRUE); //print_r($html);exit();
  		  $this->create_pdf->load($html,'Payment_cash'.' '.$TglDari3.'-'.$TglSampai3.' '.'/'.' '.$Method, 'A4-L','');
        }elseif ($Method==1) {
          $html=$this->load->view('payment/cetak/Voucher_rekap',$data,TRUE);
  		  $this->create_pdf->load($html,'Payment_cash'.' '.$TglDari3.'-'.$TglSampai3.' '.'/'.' '.$Method, 'A4-P','');
        }else{
          $html=$this->load->view('payment/cetak/Voucher_rekap',$data,TRUE);
  		  $this->create_pdf->load($html,'Payment_cash'.' '.$TglDari3.'-'.$TglSampai3.' '.'/'.' '.$Method, 'A4-L','');
        }
		
			
	}

	function Cetak_rekap_voucher_byno(){
		$nodari = $this->input->get('nodari',TRUE);
		$nosampai = $this->input->get('nosampai',TRUE);
		$Method =$this->input->get('MethodVoucher',TRUE);

		
	   

		$data['periode'] =  array('nodari' => $nodari ,'nosampai' => $nosampai ,'Method'=> 0);
	    $data['header'] = $this->Cetak("Voucher-Rekap",  $this->session->userdata('Nama_userPms'). date("d-m-Y_H-i-s"), 1);
		$data['konten'] = $this->M_payment->load_rekap_voucher_byno($nodari,$nosampai, 0);
		if ($Method==0){ 
          $html=$this->load->view('payment/cetak/Voucher_rekap_byno',$data,TRUE); //print_r($html);exit();
  		  $this->create_pdf->load($html,'Payment_cash'.' '.$nodari.'-'.$nosampai.' '.'/'.' '.$Method, 'A4-L','I');
        }elseif ($Method==1) {
          $html=$this->load->view('payment/cetak/Voucher_rekap_byno',$data,TRUE);
  		  $this->create_pdf->load($html,'Payment_cash'.' '.$nodari.'-'.$nosampai.' '.'/'.' '.$Method, 'A4-P','I');
        }else{
          $html=$this->load->view('payment/cetak/Voucher_rekap_byno',$data,TRUE);
  		  $this->create_pdf->load($html,'Payment_cash'.' '.$nodari.'-'.$nosampai.' '.'/'.' '.$Method, 'A4-L','I');
        }
		
			
	}


}

<?php 

class M_purchase_requisition extends CI_Model{	



	function item_view() {   
     $query = $this->db->query(" SELECT tbitem.* ,tbsubcategory.subcategory as namabsubcategory FROM tbitem INNER JOIN `tbsubcategory`
                                ON tbitem.`idsubcategory` =  tbsubcategory.`idsubcategory` "); 
    return $query->result() ;   


   } 

       function get_kodepr($table) { 
        $tahun = date('y'); 
        $bulan = date('m'); 
        $q = $this->db->query("SELECT MAX(RIGHT(kodepr,4)) AS idmax FROM ".$table);
        $kd = ""; //kode awal
        if($q->num_rows()>0){ //jika data ada
            foreach($q->result() as $k){
                $tmp = ((int)$k->idmax)+1; //string kode diset ke integer dan ditambahkan 1 dari kode terakhir
                $kd = "0000".$tmp; //kode ambil 4 karakter terakhir
                $id = substr($kd, -5);
            }
        }else{ //jika data kosong diset ke kode awal
            $kd = $tahun.$bulan."00001";
        }
        $kar = "PR"; //karakter depan kodenya
        //gabungkan string dengan kode yang telah dibuat tadi
        return $kar.$tahun.$bulan.$id;
   }


   function Pr_add($data,$dataDetil) { 
       $this->db->trans_strict(FALSE);
       $this->db->trans_start();  

   		$this->db->insert( 'tbpr', $data);
 		$this->db->insert_batch( 'tbprdetil', $dataDetil);  

 		
 		$this->db->trans_complete();
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else{
            $this->db->trans_commit();
        }      
   } 

   function pr_view() {   
    $query = $this->db->query("SELECT kodepr,departement,date_format(dateadd,'%d-%m-%Y') as dateadd ,description,idoperator,nama_user FROM tbpr 
                            INNER JOIN tb_user ON tbpr.idoperator = tb_user.id_user where description <>'By System ' "); 
    return $query->result() ;   
       
   }

      function pr_view_detil($kodepr) {   
    $query = $this->db->query("SELECT tbprdetil.codeitem, subcategory,tbprdetil.`description`,tbprdetil.`unit`,tbprdetil.`price`,`qtyorder` FROM tbprdetil 
                                INNER JOIN `tbitem` ON tbprdetil.`codeitem` = tbitem.`codeitem`
                                INNER JOIN `tbsubcategory` ON tbitem.`idsubcategory`  = tbsubcategory.`idsubcategory`
                                WHERE `kodepr` = '$kodepr' "); 
    return $query->result() ;   
       
   } 

  function load_departement(){   
      $query = $this->db->query(" SELECT * from tbdepartement"); 
    return $query->result() ;       
   }


  
      function Load_PR_all() { 
        $query = $this->db->query("SELECT `kodepr` ,tbdepartement.`departement`,date_format(dateadd,'%d-%m-%Y') as dateadd ,`description` FROM tbpr INNER JOIN tbdepartement
                                  ON tbpr.`iddepartement` =  tbdepartement.iddepartement 
                                   "); 
         return $query->result() ;
        } 

         function Load_PR_bycode($kodepr) { 
        $query = $this->db->query("SELECT tbprdetil.`kodepr`, date_format(dateadd,'%d-%m-%Y') as dateadd,currstok,`unit`,tbprdetil.`description`,`qtyorder`,`price`,tbpr.`description` AS note,qtyorder*price as total,
                                  tbprdetil.note as notedetil  FROM tbpr
                                  INNER JOIN tbprdetil ON tbpr.`kodepr` =  tbprdetil.`kodepr` WHERE tbprdetil.`kodepr` = '$kodepr' "); 
         return $query->result() ;  

  }


      



}
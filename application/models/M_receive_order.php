
<?php 

class M_receive_order extends CI_Model{	



	function item_view() {   
		$this->db->select("*");
		$this->db->from('tbitem');	
		return $this->db->get()->result();     
       
   } 
  function load_price_item($codeitem) {   
    $this->db->select("*");
    $this->db->from('tbitem');
    $this->db->where('codeitem',$codeitem );  
    $data = $this->db->get()->row(); 
    return $data;        
   } 
    function load_departement(){   
      $query = $this->db->query(" SELECT * from tbdepartement"); 
    return $query->result() ;       
   }


    function get_kodero($table) { 
        $tahun = date('y'); 
        $bulan = date('m'); 
        $q = $this->db->query("SELECT MAX(RIGHT(kodero,4)) AS idmax FROM ".$table);
        $kd = ""; //kode awal
        if($q->num_rows()>0){ //jika data ada
            foreach($q->result() as $k){
                $tmp = ((int)$k->idmax)+1; //string kode diset ke integer dan ditambahkan 1 dari kode terakhir
                $kd = "0000".$tmp; //kode ambil 4 karakter terakhir
                $id = substr($kd, -5);
            }
        }else{ //jika data kosong diset ke kode awal
            $kd = $tahun.$bulan."00001";
        }
        $kar = "RO"; //karakter depan kodenya
        //gabungkan string dengan kode yang telah dibuat tadi
        return $kar.$tahun.$bulan.$id;
   }


   function Ro_add($data,$dataDetil,$kodePO,$dataHistoryPrice) { 
       // $this->db->trans_strict(FALSE);
       // $this->db->trans_start();

        $this->db->insert( 'tbro', $data);
        $this->db->insert_batch( 'tbrodetil', $dataDetil);  

       if (!empty($kodePO)) { 
        $this->db->set('ischecked', 1);
        $this->db->where('kodepo', $kodePO);
        $this->db->update('tbpo');
        foreach ($dataDetil as $detil ) {
          $itemcode = $detil['itemcode'];
          $this->db->query(" UPDATE tbpodetil set ischecked= 1 where kodepo = '$kodePO' and codeitem = '$itemcode' ");
          
        }
       }

       if (!empty($dataHistoryPrice)) {
         $this->db->insert_batch( 'tbhistoryitemprice', $dataHistoryPrice); 
            foreach ($dataHistoryPrice as $row) {
              $this->db->set('lastprice', $row['newprice']);
              $this->db->where('codeitem',$row['codeitem'] );
              $this->db->update('tbitem'); 

              $this->db->set('price', $row['pricehpp']);
              $this->db->where('codeitem',$row['codeitem'] );
              $this->db->update('tbitem'); 
         }
       }
 
 		// $this->db->trans_complete();
   //      if ($this->db->trans_status() === FALSE){
   //          $this->db->trans_rollback();
   //      }else{
   //          $this->db->trans_commit();
   //      }      
   } 


    function load_ro() {
    $data = $this->db->query("SELECT `kodero`,DATE_FORMAT(`datereceive`,'%d-%m-%Y') AS datereceive ,`vendor`,`noreceipt`,`total` FROM `tbro` order by kodero desc" );
    return $data->result();

   } 

   function load_MI($KodeRo) {
    $data = $this->db->query("SELECT tbro.`kodero`,tbro.`kodepo`,DATE_FORMAT(`datereceive`,'%d-%m-%Y') AS datereceive ,`vendor`,`noreceipt`,`itemcode`,`unit`,`description`,`price`,`subtotal`,tbrodetil.qty 
        FROM `tbro` INNER JOIN tbrodetil ON tbro.kodero = tbrodetil.kodero where tbro.kodero = '$KodeRo' ");
    return $data->result();

   } 
    function load_MI_byTgl($TglAwal, $TglAkhir) {
    $data = $this->db->query("SELECT tbro.`kodero`,DATE_FORMAT(`datereceive`,'%d-%m-%Y') AS datereceive ,`vendor`,`noreceipt`,`itemcode`,`unit`,`description`,`price`,`subtotal` ,tbrodetil.qty
                             FROM `tbro` INNER JOIN tbrodetil ON tbro.kodero = tbrodetil.kodero 
                             WHERE tbro.`datereceive` BETWEEN '$TglAwal' AND  '$TglAkhir' 
                             ORDER BY tbro.`kodero` ASC ");
    return $data->result();

   } 
   


   function load_receive_unpaid_by_vendor($kodevendor) {
     $data = $this->db->query("SELECT kodero,vendor,DATE_FORMAT(`datereceive`,'%d-%m-%Y') AS datereceive,totalafterdisc from tbro where ispaid=0 and idvendor='$kodevendor'  and ispaid='0' and isinvoicing='0' order by kodero desc");
     return $data->result();
    }
 






  

  

  


      



}
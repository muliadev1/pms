<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>

<style type="text/css">
  .modal.modal-wide .modal-dialog {
  width: 80%;
}

.modal-wide .modal-body {
  overflow-y: auto;
}
</style>
<section class="content-header">
    <div class="btn-group btn-breadcrumb">
            <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
            <a  class="btn btn-default  btn-xs active">Data Check</a>
        </div>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-xs-12">
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Data Check</h3> 
          
        </div>
    <!-- /.box-header -->
        <div class="box-body">
       

      <div class="row" style="background-color:#F2F2FF; margin-left:5px; margin-right:5px; margin-bottom:10px;">
         <div class="col-md-6">
                   <div class="form-group">
                      <label>Check No / Date </label> 
                    <select id="IdCheck" class="form-control select2"  style="width: 100%;" name="IdCheck">  
                    <option value="-" selected>--Pilih Check--</option>            
                     <?php
                          foreach($data as $u){
                      ?>
                        <option value="<?php echo $u->checkno; ?>" ><?php echo $u->checkno.' | '.'Bank :'.$u->namabank.' | '.' Date: '.$u->tglview; ?></option>
                        <?php } ?>
                    </select>
                    <input type="hidden" name="NamaDepartemen" id="NamaDepartemen" value="Store">
                  </div>
                </div>

              
                 <div class="col-md-3" style="margin-top:25px;">
                <a id="Proses" class="btn btn-info fa fa-eye"> Proses</a>
               <!--  <a id="Cetak" class="btn btn-success fa fa-print"> Cetak</a> -->
                </div>

            </div>
            <div id="tabelada" hidden="hidden">
           </div>

    <div class="modal modal-wide fade" id="myModalNew" role="dialog" style="display:none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" title="close" data-dismiss="modal">&times;</button>
                    <h4 align="center" class="modal-title">Detil MI</h4>
                <table class="table" style="background-color: #00acd6; border: 1px solid black; "  >
                <tr>
                  <td width="15%" style="border: 1px solid white; ">Voucher No</td>
                  <td  width="35%" id ="Voucherm" style="background-color: #00acd6; border: 1px solid white; " ><strong> <span></span> </strong> </td>
                  <td width="15%" style="border: 1px solid white; ">Vendor</td>
                  <td  width="35%" id ="Vendorm" style="background-color: #00acd6; border: 1px solid white; " ><strong> <span></span> </strong> </td>
                </tr>
              </table>
                </div>
              
                <div class="modal-body">   
                </div>
            </div>
        </div>     
    </div>
               


      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->



<?php
$this->load->view('template/Js');
$this->load->view('template/Foot');
?>

<script type="text/javascript">
  $(document).ready(function(){
     $("#Proses").click(function(e){ 
      var idcek = $('#IdCheck').val(); 
        $.LoadingOverlay("show");
        
           $.ajax({
        type: "POST",
        dataType: 'html',
        url: '<?php echo site_url('Payment/Check_view_data');?>',
        data: {idcek: idcek },
        success: function(data){
            e.preventDefault();
            // $('#tabelkosong').fadeOut(30);
             $('#tabelada').fadeIn(0,300);  
            $('#tabelada').html(data);
            $.LoadingOverlay("hide");
        }
      });
    });
  });
</script>




 <script>
  $(function () {
    $(".select2").select2();
     $(".select3").select2();

  });
  </script>

 

<script type="text/javascript">
  function File_Kosong() {
  $.alert({
    title: 'Caution!!',
    content: 'Transaction Is Invalid!',
    icon: 'fa fa-warning',
    type: 'orange',
});
}
</script>

<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
    
  });
</script>


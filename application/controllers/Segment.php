<?php

class Segment extends CI_Controller{

	function __construct(){
		parent::__construct();
		if($this->session->userdata('Kode_userPms')==NULL) {
           redirect('Login');
		}
		$this->load->library('session');
		$this->load->helper(array('form', 'url'));
		$this->load->model(array('M_segment'));
		date_default_timezone_set('Asia/Hong_Kong');
		
		if (!empty($this->session->userdata("active_db"))) {
			$this->load->database($this->session->userdata("active_db"), FALSE, TRUE);
		}
	}

	function index(){
		redirect('segment/Segment_view');
	}

	function Segment_view(){
		$data['data'] = $this->M_segment->Segment_view();
		$this->load->view('segment/segment',$data);
	}


	function Segment_add(){
		$this->load->view('segment/Segment_add');
	}

	function Segment_addDB(){
		$data = array(
			'segment' => $this->input->post('segment'),
			'isaktif' => '1'
		);
		$this->M_segment->Segment_addDB('tbsegment',$data) ;
		$this->session->set_flashdata('msg', 'Add Segment successfully ...');
		redirect('Segment/Segment_view');
	}

	function Segment_edit($id){
		$data['data'] = $this->M_segment->Segment_view_edit($id) ;
		$this->load->view('segment/segment_edit',$data);
	}

	function Segment_editDB(){
		$id = $this->input->post('Id');
		$data = array(
			'segment' => $this->input->post('segment'),
			'isaktif' => '1'
	);
		$this->M_segment->Segment_editDB('tbsegment',$data,$id) ;
		$this->session->set_flashdata('msg', 'Edit Segment successfully...');
		redirect('Segment/Segment_view');
	}


}
?>

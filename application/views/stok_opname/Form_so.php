<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>


<section class="content-header">
    <div class="btn-group btn-breadcrumb">
            <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
            <a  class="btn btn-default  btn-xs active">Stock Opname</a>
        </div>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-xs-12">
    <div class="col-xs-12">
      <div class="box box-success box-widget">
        <div class="box-header" >
          <h6 class="box-title" style="font-size: 15px;" >FORM STOCK OPNAME</h6>
        </div>
        <div class="box-body">
    <div class="row">
      <div class="col-md-3">
                   <div class="form-group">
                      <label>Departement</label> 
                    <select id="IdDepartement" class="form-control select2"  style="width: 100%;" name="IdDepartement">  
                    <option value="-" selected>--Pilih Departememnt--</option>
                     <option value="0" >Store</option>            
                     <?php
                          foreach($dataDepaetemen as $u){
                      ?>
                        <option value="<?php echo $u->iddepartement; ?>" ><?php echo $u->departement; ?></option>
                        <?php } ?>
                    </select>
                    <input type="hidden" name="NamaDepartemen" id="NamaDepartemen" value="Store">
                  </div>
                </div>

            <div class="col-md-3">
                   <div class="form-group" id="groupSubcategory">
                      <label>Subcategory</label> 
                    <select id="Subcategory" class="form-control select2"  style="width: 100%;" name="Subcategory">  
                        <option value="1" >--Pilih Subcategory--</option>
                       
                    </select>
                  </div>
                </div>
     
        <div class="col-sm-2">
          <div class="form-group">
             <a style="margin-top: 24px; " class="btn btn-default " id="CetakForm"><i class='glyphicon glyphicon-print'></i> Cetak</a>
          </div>
        </div> 
      </div>
    </div> 
   </div>
  </div>
    </div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->



<script type="text/javascript">
$("#Simpan").submit(function() {
    
        event.preventDefault();
        $.confirm({
          title: 'Confirmation',
          content: 'Are You Sure to Save?',
           type: 'blue',
          buttons: {
              Simpan: function () {
                  $.LoadingOverlay("show");
                  $("#Simpan").submit();
              },
              Batal: function () {
                
                  $.alert('Data Tidak Disimpan...');
              },
          }
      });
     

});
</script>
<?php
$this->load->view('template/Js');
$this->load->view('template/Foot');
?>
<script type="text/javascript">
   $(document).on('change', '#IdDepartement', function(){
    var NamaVen =$("#IdDepartement option:selected").text(); 
    var id = $("#IdDepartement").val();
    $("#NamaDepartemen").val('');  
      $("#NamaDepartemen").val(NamaVen);

       $.ajax({
            type: "POST",
            dataType:'html',
           url: '<?php echo site_url("Stok_opname/Select_subcategory_bydepartememnt");?>',
            data: {iddepartement: id},
            success: function(data){
              // $('#Subcategory').remove();
                 $('#groupSubcategory').html(data);  
                 $(".select2").select2();
            }
          });

    });
</script>

  <script>
  $(function () {
    $(".select2").select2();
     $(".select3").select2();

  });
  </script>
<script type="text/javascript">
   $(document).on('change', '#Subcategory', function(){
    var Namasub =$("#Subcategory option:selected").text(); 
    $("#Sub").val('');  
      $("#Sub").val(Namasub);
    });
</script>


<script type="text/javascript">
  function File_Kosong() {
  $.alert({
    title: 'Caution!!',
    content: 'Transaction Is Invalid!',
    icon: 'fa fa-warning',
    type: 'orange',
});
}
</script>
<script type="text/javascript">
  $(function () {  
  $("#CetakForm").click(function(){
     var Kode = $( "#Subcategory" ).val();
     var iddep = $( "#IdDepartement" ).val();
      var Sub = $("#Subcategory option:selected").text();
      var Dep = $("#IdDepartement option:selected").text();
      window.open("<?php echo base_url(). 'index.php/Stok_opname/CetakFormSO/';?>?kode="+ Kode +"&sub="+ Sub+"&dep="+ Dep+"&iddep="+ iddep ,"MyTargetWindowName")
   
  }); 
});
</script>
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>


<?php

class M_reservation extends CI_Model{

	 function Expected_arrival($tglAwal, $tglAkhir) {
		 $sql="SELECT  tbreservation.idreservation, tbreservationstatus.status, tbRoom.roomname AS Expr1, tbroomtype.desc, tbguest.saluation, tbguest.firstname,tbguest.lastname, DATE_FORMAT(tbreservation.checkin,'%m/%d/%Y') AS checkin,
					 DATE_FORMAT(tbreservation.checkout,'%m/%d/%Y') AS checkout, tbreservation.duration, tbguest.state AS Expr2,tbsegment.segment AS Expr3, tbcompany.name AS Expr4, tbreservation.notes, tbReservation.msg
					 FROM tbreservation INNER JOIN tbguest ON tbreservation.idguest = tbguest.idguest INNER JOIN tbroomtype ON tbreservation.idroomtype = tbroomtype.id INNER JOIN
					 tbroom ON tbreservation.idroom = tbroom.id INNER JOIN tbsegment ON tbreservation.idsegment = tbsegment.id INNER JOIN tbcompany ON tbreservation.idcompany = tbcompany.idcompany
					 INNER JOIN tbreservationstatus ON tbreservation.idreservationstatus = tbreservationstatus.id";

		if ($tglAwal=="") {
			$sql=$sql." WHERE Convert(checkin, date) = Convert (now(), date)  GROUP BY DATE_FORMAT(checkin, '%m/%d/%Y') and idreservationstatus<> 4";
		} else{
			$sql=$sql." WHERE Convert(checkin, date) BETWEEN '$tglAwal' AND '$tglAkhir' GROUP BY DATE_FORMAT(checkin, '%m/%d/%Y') and idreservationstatus<> 4";
		};
		$data = $this->db->query($sql);
		//print_r($this->db)
 		return $data->result();
    }

	 function Expected_departure($tglAwal) {
		 $sql = "SELECT  tbreservation.idreservation, tbreservationstatus.status, tbRoom.roomname AS Expr1, tbroomtype.desc, tbguest.saluation, tbguest.firstname,tbguest.lastname, DATE_FORMAT(tbreservation.checkin,'%m/%d/%Y') AS checkin,
  					 DATE_FORMAT(tbreservation.checkout,'%m/%d/%Y') AS checkout, tbreservation.duration, tbguest.state AS Expr2,tbsegment.segment AS Expr3, tbcompany.name AS Expr4, tbreservation.notes, tbReservation.msg
  					 FROM tbreservation INNER JOIN tbguest ON tbreservation.idguest = tbguest.idguest INNER JOIN tbroomtype ON tbreservation.idroomtype = tbroomtype.id INNER JOIN
  					 tbroom ON tbreservation.idroom = tbroom.id INNER JOIN tbsegment ON tbreservation.idsegment = tbsegment.id INNER JOIN tbcompany ON tbreservation.idcompany = tbcompany.idcompany
  					 INNER JOIN tbreservationstatus ON tbreservation.idreservationstatus = tbreservationstatus.id";
		if ($tglAwal=="") {
			$sql = $sql. " WHERE CONVERT(checkout, DATE) = CONVERT (NOW(), DATE)";
		} else {
			$sql = $sql. " WHERE DATE_FORMAT(tbreservation.checkout,'%Y-%m-%d') = '$tglAwal'";
		}
		$data = $this->db->query($sql);
		return $data->result();
  }


		function Guest_in_house() {
  		$data = $this->db->query("SELECT tbreservation.idreservation, tbroom.roomname, tbguest.saluation, tbguest.firstname, tbguest.lastname, tbroomtype.roomtype AS Expr1,
  															tbroomtype.desc, DATE_FORMAT(tbreservation.checkin,'%m/%d/%Y') AS checkin, DATE_FORMAT(tbreservation.checkout,'%m/%d/%Y') AS checkout, tbreservation.duration, tbreservation.adult, tbreservation.child,tbsegment.segment AS Expr2,
  															tbcompany.name AS Expr3, tbguest.state AS Expr4, tbreservation.notes, tbreservation.msg, tb_user.nama_user, tbroom.id
  															FROM tbreservation INNER JOIN tbroomtype ON tbreservation.idroomtype = tbroomtype.id INNER JOIN tbroom ON tbreservation.idroom = tbroom.id INNER JOIN tbguest ON tbreservation.idguest = tbguest.idguest
  															INNER JOIN tbsegment ON tbreservation.idsegment = tbsegment.id INNER JOIN tbcompany ON tbreservation.idcompany = tbcompany.idcompany INNER JOIN tb_user ON tbreservation.kodeop = tb_user.id_user
  															WHERE  tbreservation.statusreservation = '1' ORDER BY tbreservation.idroom ASC");
  		return $data->result();
     }

}
?>

<?php

class Source extends CI_Controller{

	function __construct(){
		parent::__construct();
		if($this->session->userdata('Kode_userPms')==NULL) {
           redirect('Login');
		}
		$this->load->library('session');
		$this->load->helper(array('form', 'url'));
		$this->load->model(array('M_source'));
		date_default_timezone_set('Asia/Hong_Kong');
		
		if (!empty($this->session->userdata("active_db"))) {
			$this->load->database($this->session->userdata("active_db"), FALSE, TRUE);
		}
	}

	function index(){
		redirect('Source/Source_view');
	}

	function Source_view(){
		$data['data'] = $this->M_source->Source_view();
		$this->load->view('source/source',$data);
	}


	function Source_add(){
		$this->load->view('source/Source_add');
	}

	function Source_addDB(){
		$data = array(
			'source' => $this->input->post('source'),
			'isaktif' => '1'
		);
		$this->M_source->Source_addDB('tbsource',$data) ;
		$this->session->set_flashdata('msg', 'Add Item Source successfully ...');
		redirect('Source/Source_view');
	}

	function Source_edit($id){
		$data['data'] = $this->M_source->Source_view_edit($id) ;
		$this->load->view('source/Source_edit',$data);
	}

	function Source_editDB(){
		$id = $this->input->post('Id');
		$data = array(
			'source' => $this->input->post('source'),
			'isaktif' => '1'
	);
		$this->M_source->Source_editDB('tbsource',$data,$id) ;
		$this->session->set_flashdata('msg', 'Edit Item Source successfully...');
		redirect('Source/Source_view');
	}

}
?>

<?php

class Article extends CI_Controller{

	function __construct(){
		parent::__construct();
		if($this->session->userdata('Kode_userPms')==NULL) {
           redirect('Login');
		}
		$this->load->library(array ('session','country'));
		$this->load->helper(array('form', 'url'));
		$this->load->model(array('M_article'));
		date_default_timezone_set('Asia/Hong_Kong');
		
		if (!empty($this->session->userdata("active_db"))) {
			$this->load->database($this->session->userdata("active_db"), FALSE, TRUE);
		}
	}

	function index(){
		redirect('Article/Article_sales_view');
	}

	function Article_sales_view(){
		$data['data'] = $this->M_article->Article_sales_view();
		$this->load->view('article/Article_sales',$data);
	}

	function Article_sales_add(){
		$data['departement'] = $this->M_article->get_departement();
		$data['category'] = $this->M_article->get_category();
		$this->load->view('Article/Article_sales_add',$data);
	}

	function Article_sales_addDB(){
		$kode = $this->M_article->getkode();
		$data = array(
			'idarticle' => $kode,
			'articlename' => $this->input->post('articlename'),
			'service' => $this->input->post('service'),
			'price' => $this->input->post('price'),
			'tax' => $this->input->post('tax'),
			'finalprice' => $this->input->post('finalprice'),
			'idarticlecategory' => $this->input->post('idarticlecategory'),
			'pricetype' => $this->input->post('pricetype'),
			'iddepartement' => $this->input->post('iddepartement'),
			'isaktif' => '1'
		);
		$this->M_article->Article_addDB('tbarticlesales',$data) ;
		$this->session->set_flashdata('msg', 'Add Article successfully ...');
		redirect('Article/Article_sales_view');
	}

	function Article_sales_edit($id){
		$data['edit'] = $this->M_article->Article_sales_edit($id) ;
		$data['departement'] = $this->M_article->get_departement();
		$data['category'] = $this->M_article->get_category();
		$this->load->view('Article/Article_sale_edit',$data);
	}

	function Article_sales_editDB(){
		$id = $this->input->post('idarticle');
		$data = array(
			'articlename' => $this->input->post('articlename'),
			'service' => $this->input->post('service'),
			'price' => $this->input->post('price'),
			'tax' => $this->input->post('tax'),
			'finalprice' => $this->input->post('finalprice'),
			'idarticlecategory' => $this->input->post('idarticlecategory'),
			'pricetype' => $this->input->post('pricetype'),
			'iddepartement' => $this->input->post('iddepartement'),
			);
		$this->M_article->Article_editDB('tbarticlesales',$data,$id);
		$this->session->set_flashdata('msg', 'Edit Article Sales successfully...');
		redirect('Article/Article_sales_view');
	}
	function Article_sales_delete($id){
		$data = array(
			'isaktif' => '0'
			);
		$this->M_article->Article_editDB('tbarticlesales',$data,$id);
		$this->session->set_flashdata('msg', 'Edit Article Sales successfully...');
		redirect('Article/Article_sales_view');
	}
	
	
	//ARTICLE PAYMENT
	function Article_payment_view(){
		$data['data'] = $this->M_article->Article_payment_view();
		$this->load->view('Article/Article_payment',$data);
	}
	function Article_payment_add(){
		$this->load->view('Article/Article_payment_add');
	}
	function Article_payment_addDB(){
		$kode = $this->M_article->getkode();
		$data = array(
			'articlepayment' => $this->input->post('articlepayment'),
			'status' => '1'
		);
		$this->M_article->Article_addDB('tbarticlepayment',$data) ;
		$this->session->set_flashdata('msg', 'Add Article successfully ...');
		redirect('Article/Article_payment_view');
	}
	function Article_payment_edit($id){
		$data['edit'] = $this->M_article->Article_payment_edit($id) ;
		$this->load->view('Article/Article_payment_edit',$data);
	}
	function Article_payment_editDB(){
		$id = $this->input->post('id');
		$data = array(
			'articlepayment' => $this->input->post('articlepayment')
			);
		$this->M_article->Articlepayment_editDB('tbarticlepayment',$data,$id);
		$this->session->set_flashdata('msg', 'Edit Article Sales successfully...');
		redirect('Article/Article_payment_view');
	}
	function Article_payment_delete($id){
		$data = array(
			'status' => '0'
			);
		$this->M_article->Articlepayment_editDB('tbarticlepayment',$data,$id);
		$this->session->set_flashdata('msg', 'Edit Article Sales successfully...');
		redirect('Article/Article_payment_view');
	}
}
?>

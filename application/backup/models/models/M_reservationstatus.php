<?php

class M_reservationstatus extends CI_Model{

	function ReservationStatus_view() {
		$data = $this->db->query("SELECT * FROM tbreservationstatus");
		return $data->result();
   }

	 function ReservationStatus_addDB($table,$data) {
			 $this->db->insert($table,$data);
	}

	function ReservationStatus_view_edit($id) {
	 $this->db->select("*");
	 $this->db->where('id',$id);
	 $this->db->from('tbreservationstatus');
	 return $this->db->get()->result();
	}

	function ReservationStatus_editDB($table,$data,$id) {
 			 $this->db->where('id',$id );
 			 $this->db->update($table,$data);
  }

}
?>

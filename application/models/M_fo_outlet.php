<?php

class M_fo_outlet extends CI_Model{

	// function FO_view() {
	// 	$data = $this->db->query("SELECT * FROM tbguest WHERE isaktif=1");
	// 	return $data->result();
 //   }
   function FO_view() {
    $data = $this->db->query("SELECT tbguest.*, tbreservation.*, CONCAT(`firstname`, ' '  ,lastname) AS nama FROM `tbreservation` INNER JOIN tbguest
                              ON tbreservation.`idguest` =  tbguest.`idguest`
                              WHERE tbreservation.`idreservationstatus` =4");
                              return $data->result();
   }

   function Reservation_list_view() {
    $data = $this->db->query("SELECT tbreservation.`idreservation`,STATUS, tbroom.`roomname`, tbroomtype.`roomtype`, CONCAT(`firstname`, ' '  ,lastname) AS nama ,
                              DATE_FORMAT(checkin,'%d-%m-%Y') AS checkin, DATE_FORMAT(checkout,'%d-%m-%Y') AS checkout, duration,tbcompany.`name`, notes
                              FROM `tbreservation` INNER JOIN tbguest
                              ON tbreservation.`idguest` =  tbguest.`idguest` INNER JOIN `tbreservationstatus`
                              ON tbreservation.`idreservationstatus` = tbreservationstatus.`id` INNER JOIN `tbroom`
                              ON tbreservation.`idroom` = tbroom.`id` INNER JOIN `tbroomtype`
                              ON tbreservation.`idroomtype` = tbroomtype.`id` INNER JOIN tbcompany
                              ON tbreservation.`idcompany` = tbcompany.`idcompany`
                              WHERE tbreservation.`statusreservation` <>4");
                              return $data->result();
   }

    function get_room() {
    $data = $this->db->query("SELECT id,idroomtype, roomname FROM tbroom");
    return $data->result();
   }
    function get_guest_bill() {
    $data = $this->db->query("SELECT tbreservation.`idguest` , CONCAT(`firstname`, ' '  ,lastname) AS nama FROM `tbreservation` INNER JOIN tbguest 
ON  tbreservation.`idguest` =  tbguest.`idguest` WHERE `tbreservation`.`idreservationstatus` =  5");
    return $data->result();
   }

   function get_checkout_bill($id_guest) {
    $data = $this->db->query("SELECT tbcheckout.id, DATE_FORMAT(datecheckout,'%d-%m-%Y') AS tanggal FROM `tbcheckout` INNER JOIN tbreservation
ON tbcheckout.`idreservation` = tbreservation.`idreservation`
WHERE tbreservation.`idguest`='$id_guest' ");
    return $data->result();
   }

   
   

   function CetakRR_byTgl($tgl1,$tgl2) {
    $data = $this->db->query("SELECT roomname, roomtype, tbreservation.`notes`, adult+child AS pax,tbreservationdetil.`rate`, articlerate, CONCAT(`firstname`, ' '  ,lastname) AS nama,
        DATE_FORMAT(tbreservation.`checkin`,'%d-%m-%Y') AS checkin , DATE_FORMAT(tbreservation.`checkout`,'%d-%m-%Y') AS checkout   FROM `tbreservation`  INNER JOIN `tbreservationdetil`
ON tbreservation.`idreservation` = tbreservationdetil.`idreservation` INNER JOIN `tbpackage`
ON tbreservationdetil.`idpackage` = tbpackage.`id` INNER JOIN tbroom
ON tbreservation.`idroom` = tbroom.`id` INNER JOIN tbroomtype
ON tbroom.`idroomtype` =  tbroomtype.`id` INNER JOIN tbguest
ON tbreservation.`idguest` =  tbguest.`idguest`
WHERE DATE(checkin) >='$tgl1' AND  DATE(checkin) <='$tgl2'");
                              return $data->result();
   }

   function checkout_master($idrsv) {
     $sql = "SELECT  tbreservation.idreservation, tbreservationstatus.status, tbroom.roomname AS roomname, tbroomtype.desc, tbguest.saluation, tbguest.firstname,tbguest.lastname, DATE_FORMAT(tbreservation.checkin,'%d/%m/%Y') AS checkin,
             DATE_FORMAT(tbreservation.checkout,'%d/%m/%Y') AS checkout, tbreservation.duration, tbguest.state AS state,tbsegment.segment AS Expr3, tbcompany.name  as company, tbreservation.notes, tbreservation.msg
             FROM tbreservation INNER JOIN tbguest ON tbreservation.idguest = tbguest.idguest INNER JOIN tbroomtype ON tbreservation.idroomtype = tbroomtype.id INNER JOIN
             tbroom ON tbreservation.idroom = tbroom.id INNER JOIN tbsegment ON tbreservation.idsegment = tbsegment.id INNER JOIN tbcompany ON tbreservation.idcompany = tbcompany.idcompany
             INNER JOIN tbreservationstatus ON tbreservation.idreservationstatus = tbreservationstatus.id
             WHERE tbreservation.idreservation = '$idrsv' ";
   
    $data = $this->db->query($sql);
    return $data->row();
  }


    function get_kodersv($table) { 
        $tahun = date('y'); 
        $bulan = date('m'); 
        $q = $this->db->query("SELECT MAX(RIGHT(idreservation,4)) AS idmax FROM ".$table);
        $kd = ""; //kode awal
        if($q->num_rows()>0){ //jika data ada
            foreach($q->result() as $k){
                $tmp = ((int)$k->idmax)+1; //string kode diset ke integer dan ditambahkan 1 dari kode terakhir
                $kd = "000".$tmp; //kode ambil 4 karakter terakhir
                $id = substr($kd, -4);
            }
        }else{ //jika data kosong diset ke kode awal
            $kd = $tahun.$bulan."0001";
        }
        $kar = "RS"; //karakter depan kodenya
        //gabungkan string dengan kode yang telah dibuat tadi
        return $kar.$tahun.$bulan.$id;
   }

   function get_fotrx($table) { 
        $tahun = date('y'); 
        $bulan = date('m'); 
        $q = $this->db->query("SELECT MAX(RIGHT(kodemaster,4)) AS idmax FROM ".$table);
        $kd = ""; //kode awal
        if($q->num_rows()>0){ //jika data ada
            foreach($q->result() as $k){
                $tmp = ((int)$k->idmax)+1; //string kode diset ke integer dan ditambahkan 1 dari kode terakhir
                $kd = "000".$tmp; //kode ambil 4 karakter terakhir
                $id = substr($kd, -4);
            }
        }else{ //jika data kosong diset ke kode awal
            $kd = $tahun.$bulan."0001";
        }
        $kar = "RS"; //karakter depan kodenya
        //gabungkan string dengan kode yang telah dibuat tadi
        return $kar.$tahun.$bulan.$id;
   }
    function get_kodebill($table) { 
        $tahun = date('y'); 
        $bulan = date('m'); 
        $q = $this->db->query("SELECT MAX(RIGHT(kodemasterbil,4)) AS idmax FROM ".$table);
        $kd = ""; //kode awal
        if($q->num_rows()>0){ //jika data ada
            foreach($q->result() as $k){
                $tmp = ((int)$k->idmax)+1; //string kode diset ke integer dan ditambahkan 1 dari kode terakhir
                $kd = "000".$tmp; //kode ambil 4 karakter terakhir
                $id = substr($kd, -4);
            }
        }else{ //jika data kosong diset ke kode awal
            $kd = $tahun.$bulan."0001";
        }
        $kar = "BL"; //karakter depan kodenya
        //gabungkan string dengan kode yang telah dibuat tadi
        return $kar.$tahun.$bulan.$id;
   }

       function get_kodecheckout() { 
        $tahun = date('y'); 
        $bulan = date('m'); 
        $q = $this->db->query("SELECT MAX(RIGHT(id,4)) AS idmax FROM tbcheckout ");
        $kd = ""; //kode awal
        if($q->num_rows()>0){ //jika data ada
            foreach($q->result() as $k){
                $tmp = ((int)$k->idmax)+1; //string kode diset ke integer dan ditambahkan 1 dari kode terakhir
                $kd = "000".$tmp; //kode ambil 4 karakter terakhir
                $id = substr($kd, -4);
            }
        }else{ //jika data kosong diset ke kode awal
            $kd = $tahun.$bulan."0001";
        }
        $kar = "CO"; //karakter depan kodenya
        //gabungkan string dengan kode yang telah dibuat tadi
        return $kar.$tahun.$bulan.$id;
   }
function get_room_staus($idroom) {
    $data = $this->db->query(" SELECT tbroomstatus.* FROM `tbroom` INNER JOIN `tbroomstatus`
ON tbroom.`idroomstatus` = tbroomstatus.`id` where tbroom.id = '$idroom' ");
    return $data->row();
   }

   function get_article_sales() {
    $data = $this->db->query("   SELECT `idarticle`,`articlename`,finalprice FROM `tbarticlesales`");
    return $data->result();
   }
   function get_fo_invoice($IdReservation) {
    $data = $this->db->query("SELECT tbfotrxdetil.*, date_format(tgl,'%d-%m-%Y') as tgl1, nama_user FROM `tbfotrxdetil`
      inner join tb_user on tbfotrxdetil.idoperator = tb_user.id_user WHERE kodemaster = '$IdReservation' AND (charge <> 0 OR payment <> 0)");
    return $data->result();
   }
    function checkin_guest($idreservation) {
    $data = $this->db->query("SELECT tbreservation.*, CONCAT(`firstname`, ' '  ,lastname) AS nama, phone, date_format(checkin,'%m/%d/%Y') as tglin, date_format(checkout,'%m/%d/%Y') as tglout, date_format(cod,'%m/%d/%Y') as tglcod FROM `tbreservation` INNER JOIN tbguest
                              ON tbreservation.`idguest` =  tbguest.`idguest`
                              WHERE tbreservation.`idreservation` = '$idreservation'  ");
                              return $data->row();
   }
    function checkin_guest_rsv_detil($idreservation) {
    $data = $this->db->query("SELECT * from tbreservationdetil
                              WHERE tbreservationdetil.`idreservation` = '$idreservation'  ");
                              return $data->row();
   }
   function checkin_guest_fo_detil($idreservation) {
    $data = $this->db->query("SELECT tbfotrxdetil.* FROM tbfotrx INNER JOIN tbfotrxdetil
                              ON tbfotrx.`kodemaster` = tbfotrxdetil.`kodemaster`
                                WHERE tbfotrx.`kodereservation` = '$idreservation'  ");
                              return $data->row();
   }
   

   function checkin_guest_detil($idreservation) {
    $data = $this->db->query(" SELECT tbguest.* FROM `tbreservationguestlist` INNER JOIN tbguest
                                ON tbreservationguestlist.`idguest` = tbguest.`idguest`
                              WHERE tbreservationguestlist.`idreservation` = '$idreservation'  ");
                              return $data->result();
   }
  
   
   function Reservation_list_view_bykode($idreservation) {
    $data = $this->db->query("SELECT tbreservation.*, CONCAT(`firstname`, ' '  ,lastname) AS nama,phone, tbreservation.`idroom` AS idroom1,
                              (SELECT roomname FROM tbroom WHERE id = idroom1 ) AS roomname
                              FROM `tbreservation` 
                              INNER JOIN tbguest
                              ON tbreservation.`idguest` =  tbguest.`idguest`
                              WHERE tbreservation.`statusreservation` <>4 AND idreservation='$idreservation'");
                              return $data->row();
   }
   function Reservation_list_view_bykode_detilguest($idreservation) {
    $data = $this->db->query(" 
                                SELECT tbreservationguestlist.*, CONCAT(firstname,' ', lastname) AS nama FROM `tbreservation` INNER JOIN `tbreservationguestlist`
                              ON tbreservation.`idreservation` = tbreservationguestlist.`idreservation`INNER JOIN tbguest 
                              ON tbreservationguestlist.`idguest` =  tbguest.`idguest` 
                              WHERE tbreservationguestlist.`idreservation` = '$idreservation' ");
                              return $data->result();
   }

   function Reservation_list_view_bykode_detil($idreservation) {
    $data = $this->db->query(" SELECT `tbreservationdetil`.*,tbpackage.`package`,rate FROM  tbreservationdetil  INNER JOIN tbpackage
                                ON tbreservationdetil.`idpackage` = tbpackage.`id`
                                WHERE `idreservation` = '$idreservation'");
                              return $data->row();
   }

   function get_roombill($idreservation) {
    $data = $this->db->query("  SELECT tbfotrxdetil.*, DATE_FORMAT(tgl,'%d-%m-%Y') AS tgl1 FROM `tbfotrxdetil`
                                WHERE `kodemaster` = '$idreservation' AND idarticle = 'CHG001'  ORDER BY kode DESC ");
                              return $data->result();
   }
   
   function get_comsumebill($idreservation) {
    $data = $this->db->query("SELECT trxid AS trxid ,CONCAT('#FB', 'table:'  ,tableid)  AS des, grandtotal AS grandtotal,'0' AS payment , DATE_FORMAT(trxdate,'%d-%m-%Y') AS tgl1 FROM `tbfbtrx` 
                                  WHERE idreservationcharge = '$idreservation'
                                  UNION
                                  SELECT kodemaster AS trxid, invoicemanual AS des, '0' AS grandtotal, payment AS payment, DATE_FORMAT(tgl,'%d-%m-%Y') AS tgl1 FROM `tbfotrxdetil`
                                  WHERE `kodemaster` = '$idreservation' AND idarticle <> 'CHG001'
                                  ORDER BY tgl1 DESC  ");
                              return $data->result();
   }
   
   function get_comsumebill_dee($idreservation) {
    $data = $this->db->query("SELECT trxid AS trxid ,CONCAT('#FB', 'table:'  ,tableid)  AS des, grandtotal AS grandtotal,'0' AS payment , DATE_FORMAT(trxdate,'%d-%m-%Y') AS tgl1 FROM `tbfbtrx` 
                                  WHERE idreservationcharge = '$idreservation'
                                  UNION
                                  SELECT kodemaster AS trxid, invoicemanual AS des, charge AS grandtotal, payment AS payment, DATE_FORMAT(tgl,'%d-%m-%Y') AS tgl1 FROM `tbfotrxdetil`
                                  WHERE `kodemaster` = 'RS19020006' AND idarticle <> 'CHG001' AND (charge > 0 OR payment > 0)
                                  ORDER BY tgl1 DESC  ");
                              return $data->result();
   }

   function get_masterbill_cetak($idmaster,$status) {
    $data = $this->db->query("  SELECT tbcheckoutdetil.*, DATE_FORMAT(date,'%d-%m-%Y') AS tgl1  FROM `tbcheckoutdetil` WHERE idmaster = '$idmaster' AND STATUS = '$status' ");
    return $data->result();
   }
   function get_idrsv_by_checkout($idmaster) {
    $data = $this->db->query("   SELECT tbcheckout.id,tbreservation.`idreservation`, DATE_FORMAT(datecheckout,'%d-%m-%Y') AS tanggal FROM `tbcheckout` INNER JOIN tbreservation
ON tbcheckout.`idreservation` = tbreservation.`idreservation`
WHERE tbcheckout.`id`='$idmaster'");
    return $data->row();
   }







   

   
   


function Operator_view() {
    $data = $this->db->query("SELECT * FROM tb_user ");
    return $data->result();
   }

   function Get_master_clossing_resto(){
    $data= $this->db->query("SELECT tbfbtrx.*, DATE_FORMAT(trxdate,'%d-%m-%Y') AS tanggaltrx , CONCAT(`firstname`, ' '  ,lastname) AS nama,roomname,tablenumber
  FROM `tbfbtrx` INNER JOIN tbguest
  ON tbfbtrx.`idguest` = tbguest.`idguest` LEFT JOIN tbroom
  ON tbfbtrx.`idroom` = tbroom.`id` INNER JOIN tbtable
  ON tbfbtrx.`tableid` = tbtable.`id` 
   WHERE  isclosing =0");
    return $data->result();
  }

  function Get_data_clossing_resto_na(){
    $data= $this->db->query("SELECT tbfbtrx.*, DATE_FORMAT(trxdate,'%d-%m-%Y') AS tanggaltrx , CONCAT(`firstname`, ' '  ,lastname) AS nama,roomname,tablenumber,
      (SELECT  nama_user FROM tb_user WHERE id_user = idoperatorprocess) as nama_user
  FROM `tbfbtrx` INNER JOIN tbguest
  ON tbfbtrx.`idguest` = tbguest.`idguest` LEFT JOIN tbroom
  ON tbfbtrx.`idroom` = tbroom.`id` INNER JOIN tbtable
  ON tbfbtrx.`tableid` = tbtable.`id` 
   WHERE  isclosing =1  and isna = 0");
    return $data->result();
  }





   function Op_closing_data() {
    $data = $this->db->query(" SELECT tbfotrxdetil.kode, `tbfotrx`.`kodemaster`, invoicemanual,roomname, roomtype, DATE_FORMAT(tbfotrxdetil.`tgl`,'%d-%m-%Y') AS trdate, tbarticlesales.`articlename`,
       tbarticlecategory.`articlecategory`, charge, nama_user  FROM  tbfotrx INNER JOIN tbfotrxdetil
      ON tbfotrx.`kodemaster`  = `tbfotrxdetil`.`kodemaster` INNER JOIN tbroom 
      ON tbfotrxdetil.`idroom` = tbroom.`id` INNER JOIN `tbroomtype` 
      ON tbroom.`idroomtype` = tbroomtype.`id` INNER JOIN `tbarticlesales`
      ON tbfotrxdetil.`idarticle` = tbarticlesales.`idarticle` INNER JOIN `tbarticlecategory`
      ON tbarticlesales.`idarticlecategory` = tbarticlecategory.`id` INNER JOIN tb_user
      ON tbfotrxdetil.`idoperator` = tb_user.`id_user`
      WHERE isclosing = 0 ");
    return $data->result();
   }

   function get_rsv_inhouse() {
    $data = $this->db->query(" SELECT  * FROM `tbreservation` WHERE idreservationstatus = 4 ");
    return $data->result();
   }
   function get_rsv_in_fotrx($idreservation) {
    $datafotrx = $this->db->query(" SELECT * FROM `tbfotrxdetil` WHERE kodemaster = '$idreservation' ORDER BY night DESC LIMIT 1 ");
    $night = $datafotrx->row()->night + 1;
    $datarsv = $this->db->query(" SELECT tbreservationdetil.*, roomname, roomtype, nightk, package,
      CONCAT(`firstname`, ' '  ,lastname) AS nama FROM `tbreservationdetil` INNER JOIN tbroom 
      ON tbreservationdetil.`idroom` = tbroom.`id` INNER JOIN `tbroomtype`
      ON tbreservationdetil.`idroomtype` = tbroomtype.`id` INNER JOIN `tbpackage`
      ON tbreservationdetil.`idpackage` = tbpackage.`id` INNER JOIN `tbreservation`
      ON tbreservationdetil.`idreservation` = tbreservation.`idreservation` INNER JOIN tbguest
      ON tbreservation.`idguest` = tbguest.`idguest`
      WHERE tbreservationdetil.idreservation  = '$idreservation' AND nightk =$night LIMIT 1 ");
     
    return $datarsv->row();
   }

   

   function Op_closing_data_cetak($jam) {
    $data = $this->db->query(" SELECT tbfotrxdetil.kode, `tbfotrx`.`kodemaster`, invoicemanual,roomname, roomtype,DATE_FORMAT(tbfotrxdetil.`dateclosing`,'%Y-%m-%d: %H') AS jam,
 DATE_FORMAT(tbfotrxdetil.`tgl`,'%d-%m-%Y') AS trdate, tbarticlesales.`articlename`,
       tbarticlecategory.`articlecategory`, charge, nama_user  FROM  tbfotrx INNER JOIN tbfotrxdetil
      ON tbfotrx.`kodemaster`  = `tbfotrxdetil`.`kodemaster` INNER JOIN tbroom 
      ON tbfotrxdetil.`idroom` = tbroom.`id` INNER JOIN `tbroomtype` 
      ON tbroom.`idroomtype` = tbroomtype.`id` INNER JOIN `tbarticlesales`
      ON tbfotrxdetil.`idarticle` = tbarticlesales.`idarticle` INNER JOIN `tbarticlecategory`
      ON tbarticlesales.`idarticlecategory` = tbarticlecategory.`id` INNER JOIN tb_user
      ON tbfotrxdetil.`idoperator` = tb_user.`id_user`
      WHERE isclosing = 1 AND DATE_FORMAT(tbfotrxdetil.`dateclosing`,'%Y-%m-%d:%H') = '$jam' ");
    return $data->result();
   }

   function Select_clossing_byop($idoperator) {
    $data = $this->db->query("SELECT tbfotrxdetil.kode, `tbfotrx`.`kodemaster`, invoicemanual,roomname, roomtype,DATE_FORMAT(tbfotrxdetil.`dateclosing`,'%Y-%m-%d: %H') AS tgl,
DATE_FORMAT(tbfotrxdetil.`dateclosing`,'%Y-%m-%d:%H') AS tgl1,
 DATE_FORMAT(tbfotrxdetil.`tgl`,'%d-%m-%Y') AS trdate, tbarticlesales.`articlename`,
       tbarticlecategory.`articlecategory`, charge, nama_user  FROM  tbfotrx INNER JOIN tbfotrxdetil
      ON tbfotrx.`kodemaster`  = `tbfotrxdetil`.`kodemaster` INNER JOIN tbroom 
      ON tbfotrxdetil.`idroom` = tbroom.`id` INNER JOIN `tbroomtype` 
      ON tbroom.`idroomtype` = tbroomtype.`id` INNER JOIN `tbarticlesales`
      ON tbfotrxdetil.`idarticle` = tbarticlesales.`idarticle` INNER JOIN `tbarticlecategory`
      ON tbarticlesales.`idarticlecategory` = tbarticlecategory.`id` INNER JOIN tb_user
      ON tbfotrxdetil.`idoperator` = tb_user.`id_user`
      WHERE isclosing = 1 AND tbfotrxdetil.`idoperator` = '$idoperator'
      GROUP BY tgl DESC ");
    return $data->result();
   }

   function Select_jam_closing($iddetil) {
    $data = $this->db->query("SELECT DATE_FORMAT(tbfotrxdetil.`dateclosing`,'%Y-%m-%d:%H') AS tgl from  tbfotrxdetil
     WHERE tbfotrxdetil.`kode` = '$iddetil' ");
    return $data->row()->tgl;
   }

   
  
   function get_rsvstatus() {
		$data = $this->db->query("SELECT * FROM tbreservationstatus");
		return $data->result();
   }
   function get_purpose() {
    $data = $this->db->query("SELECT * FROM `tbpurpose`");
    return $data->result();
   }
   function get_source() {
    $data = $this->db->query("SELECT * FROM `tbsource`");
    return $data->result();
   }
   function get_segment() {
    $data = $this->db->query("SELECT * FROM `tbsegment`");
    return $data->result();
   }
   function get_articlepayment() {
    $data = $this->db->query("SELECT * FROM `tbarticlepayment`");
    return $data->result();
   }
   function get_article_payment() {
    $data = $this->db->query("SELECT `id`,`articlepayment` FROM `tbarticlepayment`");
    return $data->result();
   }
   function get_roomtype() {
		$data = $this->db->query("SELECT id,roomtype, rate FROM `tbroomtype`");
		return $data->result();
   }
   function Guest_view() {
		$data = $this->db->query("Select * FROM tbguest WHERE isaktif=1");
		return $data->result();
   }
   function Company_view() {
		$data = $this->db->query("SELECT * FROM tbcompany WHERE isaktif=1");
		return $data->result();
   }

   function get_room_by_type($idtype) {
		$data = $this->db->query("SELECT id,idroomtype, roomname FROM tbroom where idroomtype = '$idtype' ");
		return $data->result();
   }
   function get_package() {
		$data = $this->db->query("SELECT * FROM `tbpackage` GROUP BY id ");
		return $data->result();
   }
   function get_package_by_code($id) {
		$data = $this->db->query("SELECT * FROM `tbpackage` where id = '$id' ");
		return $data->row();
   }

   function get_detil_rsv($id) {
    $data = $this->db->query("SELECT * FROM `tbreservationdetil` where id = '$id' ");
    return $data->row();
   }

    function add_rsv($datamaster,$datamasterdetilguest,$dataDetil) {
       $this->db->insert( 'tbreservation', $datamaster);
        $this->db->insert_batch( 'tbreservationguestlist', $datamasterdetilguest);
         $this->db->insert_batch( 'tbreservationdetil', $dataDetil);
      // return $this->db->insert_id();
   }

   function add_fotrx($datamaster,$datadetil) {
       $this->db->insert( 'tbfotrx', $datamaster);
        $this->db->insert_batch( 'tbfotrxdetil', $datadetil);
   }
   function add_fotrx_invoice($datadetil) {
        $this->db->insert_batch( 'tbfotrxdetil', $datadetil);
   }

   function Na_addDB($datadetilFO, $dataiddetilRsv,$dataFb,$dataiddetilfb,$updNAdetil) {
      $this->db->trans_strict(FALSE);
      $this->db->trans_start();

        $this->db->insert_batch( 'tbfotrxdetil', $datadetilFO);

        for ($j=0; $j <count($updNAdetil); $j++) {
            $this->db->where('kodemaster', $updNAdetil[$j]['kodemaster']);
            $this->db->update('tbfotrxdetil', $updNAdetil[$j]);
        }

        for ($i=0; $i <count($dataiddetilRsv) ; $i++) { 
            $this->db->set('isruning', '1');
            $this->db->where('id', $dataiddetilRsv[$i]['id']);
            $this->db->update('tbreservationdetil');
        }

        for ($x=0; $x <count($dataiddetilfb) ; $x++) { 
            $this->db->where('trxid',$dataiddetilfb[$x]['id'] );
            $this->db->update('tbfbtrx',$dataFb);
        }
        
      $this->db->trans_complete();
      return $this->db->trans_status();
   }

    function add_biltrx($datamaster,$datadetil) {
       $this->db->insert( 'tbmasterbill', $datamaster);
        $this->db->insert_batch( 'tbmasterbilldetil', $datadetil);
   }

   function add_guest($data)
   {
      $this->db->insert('tbguest',$data);
   }
   function checkin_add($data,$id) {
       $this->db->where('idreservation',$id );
       $this->db->update('tbreservation',$data);
  }
   function update_fotrx($data,$id) {
       $this->db->where('kodemaster',$id );
       $this->db->update('tbfotrxdetil',$data);
   }
   function Add_clossing($data,$id) {
       $this->db->where('kode',$id );
       $this->db->update('tbfotrxdetil',$data);
   }
   function update_biltrx($datamaster,$datadetil,$id) {
       $this->db->where('idreservation',$id );
       $this->db->update('tbmasterbill',$datamaster);

       $this->db->where('idreservation',$id );
       $this->db->update('tbmasterbilldetil',$datadetil);
   }

    function checkoutAddDB($idRsv,$datamaster,$dataRoom,$dataCon,$dataSum) {
        $this->db->set('idreservationstatus', '5');
        $this->db->where('idreservation', $idRsv);
        $this->db->update('tbreservation');

        if (!empty($datamaster)) {
          $this->db->insert( 'tbcheckout', $datamaster);
        }
         if (!empty($dataRoom)) {
          $this->db->insert_batch( 'tbcheckoutdetil', $dataRoom);
        }
         if (!empty($dataCon)) {
         $this->db->insert_batch( 'tbcheckoutdetil', $dataCon);
        }
        if (!empty($dataCon)) {
         $this->db->insert_batch( 'tbcheckoutdetil', $dataSum);
        }             
        
   }

  
   


	

}
?>

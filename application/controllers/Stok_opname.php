<?php

class Stok_opname extends CI_Controller{
	private $IdOperator;
	private $Operator;

	function __construct(){
		parent::__construct();
		if($this->session->userdata('Kode_userPms')==NULL) {
           redirect('Login');
		}
		$this->load->library(array('session','create_pdf','ciqrcode'));
		$this->load->helper(array('form', 'url'));
		$this->load->model(array('M_category','M_category','M_item','M_subcategory','M_stok_opname','M_rbac','M_store_requisition','M_purchase_order','M_aset'));
		$this->IdOperator = $this->session->userdata('Kode_userPms');
		$this->Operator = $this->session->userdata('Nama_userPms');
    
    if (!empty($this->session->userdata("active_db"))) {
      $this->load->database($this->session->userdata("active_db"), FALSE, TRUE);
    }

	}

	function index(){
		redirect('Item/Item_view');
	}

	function SO_view(){
		$data['dataDepaetemen'] = $this->M_stok_opname->loaddepartementall();
		$this->load->view('Stok_opname/Stok_opname',$data);
	}
	function SO_view_data(){
		$Departement = $this->input->post('id');
		$NamaDep = $this->input->post('NamaDep');
		$Subcategory = $this->input->post('Subcategory');
	  if ($Departement==1) { //```````````` 
        $set = 'stokBO';
        $Istok = 'isBO';
        $IsProses = 'isprosesopnameBO';
       }elseif ($Departement==2) {
        $set = 'stokHK';
        $Istok = 'isHK';
         $IsProses = 'isprosesopnameHK';
       }elseif ($Departement==3) {
         $set = 'stokFO';
         $Istok = 'isFO';
          $IsProses = 'isprosesopnameFO';
       }elseif ($Departement==4) {
          $set = 'stokFB';
          $Istok = 'isFB';
          $IsProses = 'isprosesopnameFB';
       }elseif ($Departement==5) {
        $set = 'stokBO';
        $Istok = 'isBO';
         $IsProses = 'isprosesopnameBO';
       }elseif ($Departement==6) {
        $set = 'stokBar';
        $Istok = 'isBar';
        $IsProses = 'isprosesopnameBar';
      }elseif ($Departement==7) {
        $set = 'stokEng';
        $Istok = 'isEng';
        $IsProses = 'isprosesopnameEng';
      }elseif ($Departement==8) {
        $set = 'stokPG';
        $Istok = 'isPG';
        $IsProses = 'isprosesopnamePG';
      }elseif ($Departement==9) {
        $set = 'stokSec';
        $Istok = 'isSec';
        $IsProses = 'isprosesopnameSec';
       }else{
         $set = 'stok';
          $IsProses = 'isprosesopname';
       }
       $data['departement'] =  array('set' =>$set ,'istok' =>$Istok,'iddep' =>$Departement,'dep' =>$NamaDep );
      // $this->M_stok_opname->stok_opname_view_form($idsub,$iddepartement);
		$data['data'] = $this->M_stok_opname->stok_opname_view($Istok, $IsProses,$set,$Subcategory,$Departement );
		$this->load->view('Stok_opname/data_stok_opname',$data);
	}
	function daftar_so(){
		$data['data'] = $this->M_stok_opname->stok_opname_view_list();
		$this->load->view('Stok_opname/Cetak/Daftar_so',$data);
	}
	function form_so(){
		$data['data'] = $this->M_stok_opname->load_subcategory();
		$data['dataDepaetemen'] = $this->M_stok_opname->loaddepartementall();
		$this->load->view('Stok_opname/Form_so',$data);
	}

	function SO_view_item(){
		$data = $this->M_stok_opname->stok_opname_view();
		echo json_encode($data);
	}

	function add_proses_opname(){
		$Total = 0;
		date_default_timezone_set('Asia/Hong_Kong');
		
		
		$itemcode = $this->input->post('codeitem');
		$Description = $this->input->post('description');
		$Unit = $this->input->post('unit');
		$Price = $this->input->post('price');
		$Stok = $this->input->post('Stok');
		$Inputstok = $this->input->post('CurStok');
		$kodeSr =$this->M_store_requisition->get_kodesr('tbsr');
		$set = $this->input->post('Set');
		$Departement = $this->input->post('Departement');
		$iddepartement = $this->input->post('iddepartement');   
		//print_r($set) ;exit(); //````````````````

		if ($iddepartement==1) { //````````````
        $IsProses = 'isprosesopnameBO';
       }elseif ($iddepartement==2) {
        $IsProses = 'isprosesopnameHK';
       }elseif ($iddepartement==3) {
         $IsProses = 'isprosesopnameFO';
       }elseif ($iddepartement==4) {
         $IsProses = 'isprosesopnameFB';
       }elseif ($iddepartement==5) {
        $set = 'stokBO';
        $IsProses = 'isprosesopnameFB';
       }elseif ($iddepartement==6) {
        $IsProses = 'isprosesopnameBar';
      }elseif ($iddepartement==7) {
         $IsProses = 'isprosesopnameEng';
      }elseif ($iddepartement==8) {
        $IsProses = 'isprosesopnamePG';
      }elseif ($iddepartement==9) {
        $IsProses = 'isprosesopnameSec';
       }else{
          $IsProses = 'isprosesopname';
       }

		$itemcode1 = array_combine(range(1, count($itemcode)), array_values($itemcode));
		$Description1 = array_combine(range(1, count($Description)), array_values($Description));
		$Unit1 = array_combine(range(1, count($Unit)), array_values($Unit));
		$Price1 = array_combine(range(1, count($Price)), array_values($Price));
		$Stok1 = array_combine(range(1, count($Stok)), array_values($Stok));
		$Inputstok1 = array_combine(range(1, count($Inputstok)), array_values($Inputstok));

				$Jumlah = count($Stok);
				for ($i=1; $i <= $Jumlah ; $i++) { 
			if ( $Inputstok1[$i]!='-') {
				
			
				if ($Inputstok1[$i] !='-') {				
					$diff = $Stok1[$i] -  $Inputstok1[$i];
					if ($diff>0) {
						$statusInOut=1;
						$kondisiakhir='Baik';
					}else{
						$statusInOut=0;
						$kondisiakhir='out';
					}
				 	 $dataDetil[] = array('itemcode' => $itemcode1[$i],
						 				'price' => $Price1[$i],
						 				'date' => date('Y-m-d H:i:s'),
						 				'description' => $Description1[$i],
						 				'unit' => $Unit1[$i],
						 				'stok' => $Stok1[$i],
						 				'inputstok' => $Inputstok1[$i],
						 				'diff' => $diff,
						 				'departement' => $Departement,
						 				'iddepartement' => $iddepartement
						 		 );
				 	 //````

				 	 $datamastersr = array('kodesr' => $kodeSr,
	 				 		'iddepartement' => 0,
	 				 		'departement' => 'Store',
	 						'dateadd' => date('Y-m-d H:i:s'),
	 						'idoperator' => $this->IdOperator,
	 						'description' => 'By Stock Opname',
	 						'statusInOut' => $statusInOut,
	 						'isSO' => 1	
	 		 			);
				 	$dataDetilsr[] = array('kodesr' => $kodeSr,
						 				 'iddepartement' => 0,
						 				 'departement' => 'Store',
						 				'codeitem' => $itemcode1[$i],
						 				'price' => $Price1[$i],
						 				'qtyorder' => abs($diff),
						 				'description' => $Description1[$i],
						 				'unit' => $Unit1[$i],
						 				'note' => 'By Stock Opname',
						 				'currstok' => $Stok1[$i],
						 				'idoperator' => $this->IdOperator,
						 				'statusInOut' => $statusInOut,
	 									'isSO' => 1	
						 		 );

				  } 

		 			$isaset = $this->M_purchase_order->Load_subcategory_byiditem($itemcode1[$i]);
		 			if ($isaset==1) {
		 				$codeaset = $this->M_purchase_order->Load_aset_byiditem($itemcode1[$i]); 
		 				if (!empty($codeaset) and $iddepartement!=0 ) {
		 				 $dataDetilaset[] = array('codeaset' => $codeaset,
						 				 'iddepartement' => $iddepartement,
						 				 'departement' => $Departement,
						 				 'kondisiawal' => 'Baik',
						 				 'kondisiakhir' => $kondisiakhir,
						 				 'qty' =>$Inputstok1[$i],
						 				 'note' => 'By Stock Opname',
						 				 'inputdate' => date('Y-m-d H:i:s'),
						 				 'idoperator' => $this->IdOperator,
						 				 'operator' => $this->Operator
						 		 );
		 				}
		 			}
				 	
				}
				 
			}
			//print_r( $dataDetilaset);exit();




		$this->M_stok_opname->SO_add($dataDetil, $datamastersr,$dataDetilsr,$set,$IsProses);
		if (!empty($dataDetilaset)) {
		 $this->M_aset->Add_detilAset('tbasetdetil',$dataDetilaset,$iddepartement);
		}
		$this->session->set_flashdata('msg', 'Stock Opname Process successfully... ');
		$this->session->set_flashdata('tglso', date('Y-m-d'));
		redirect('Stok_opname/SO_view');
	}

	 function Select_subcategory_bydepartememnt(){
    $Departement = $this->input->post('iddepartement');
   // print_r( $Departement);exit();
    if ($Departement==1) { //```````````` 
        $set = 'stokBO';
        $Istok = 'isBO';
        $IsProses = 'isprosesopnameBO';
       }elseif ($Departement==2) {
        $set = 'stokHK';
        $Istok = 'isHK';
         $IsProses = 'isprosesopnameHK';
       }elseif ($Departement==3) {
         $set = 'stokFO';
         $Istok = 'isFO';
          $IsProses = 'isprosesopnameFO';
       }elseif ($Departement==4) {
          $set = 'stokFB';
          $Istok = 'isFB';
          $IsProses = 'isprosesopnameFB';
       }elseif ($Departement==5) {
        $set = 'stokBO';
        $Istok = 'isBO';
         $IsProses = 'isprosesopnameBO';
       }elseif ($Departement==6) {
        $set = 'stokBar';
        $Istok = 'isBar';
        $IsProses = 'isprosesopnameBar';
      }elseif ($Departement==7) {
        $set = 'stokEng';
        $Istok = 'isEng';
        $IsProses = 'isprosesopnameEng';
      }elseif ($Departement==8) {
        $set = 'stokPG';
        $Istok = 'isPG';
        $IsProses = 'isprosesopnamePG';
      }elseif ($Departement==9) {
        $set = 'stokSec';
        $Istok = 'isSec';
        $IsProses = 'isprosesopnameSec';
       }else{
         $Istok = '';
         $set = 'stok';
         $IsProses = 'isprosesopname';
       }
    $sub = $this->M_stok_opname->Select_subcategory_bydepartememnt($Departement);
    echo ' <div class="form-group" id="groupSubcategory">';
    echo ' <label>Subcategory</label> ';
    echo  '<select id="Subcategory" class="form-control select2"  style="width: 100%;" name="Subcategory"> ';
    echo ' <option value="" >Semua Subcategory</option>';
    foreach ($sub as $pek ) {
                    echo ' <option value="'.$pek->idsubcategory.'" >'.$pek->subcategory.'</option>';
    }
    echo '</select>';
    echo '</div>';

  }
  function Select_subcategory_bydepartememntsr(){
    $Departement = $this->input->post('iddepartement');
   // print_r( $Departement);exit();
    if ($Departement==1) { //```````````` 
        $set = 'stokBO';
        $Istok = 'isBO';
        $IsProses = 'isprosesopnameBO';
       }elseif ($Departement==2) {
        $set = 'stokHK';
        $Istok = 'isHK';
         $IsProses = 'isprosesopnameHK';
       }elseif ($Departement==3) {
         $set = 'stokFO';
         $Istok = 'isFO';
          $IsProses = 'isprosesopnameFO';
       }elseif ($Departement==4) {
          $set = 'stokFB';
          $Istok = 'isFB';
          $IsProses = 'isprosesopnameFB';
       }elseif ($Departement==5) {
        $set = 'stokBO';
        $Istok = 'isBO';
         $IsProses = 'isprosesopnameBO';
       }elseif ($Departement==6) {
        $set = 'stokBar';
        $Istok = 'isBar';
        $IsProses = 'isprosesopnameBar';
      }elseif ($Departement==7) {
        $set = 'stokEng';
        $Istok = 'isEng';
        $IsProses = 'isprosesopnameEng';
      }elseif ($Departement==8) {
        $set = 'stokPG';
        $Istok = 'isPG';
        $IsProses = 'isprosesopnamePG';
      }elseif ($Departement==9) {
        $set = 'stokSec';
        $Istok = 'isSec';
        $IsProses = 'isprosesopnameSec';
       }else{
         $Istok = '';
         $set = 'stok';
         $IsProses = 'isprosesopname';
       }
    $sub = $this->M_stok_opname->Select_subcategory_bydepartememnt($Departement);
    echo ' <div class="form-group" id="groupSubcategory">';
    echo ' <label>Subcategory</label> ';
    echo  '<select id="Subcategorysr" class="form-control select2"  style="width: 100%;" name="Subcategorysr"> ';
    echo ' <option value="" >Semua Subcategory</option>';
    foreach ($sub as $pek ) {
                    echo ' <option value="'.$pek->idsubcategory.'" >'.$pek->subcategory.'</option>';
    }
    echo '</select>';
    echo '</div>';

  }

  function Select_subcategory_bydepartememntsrItem(){
    $Departement = $this->input->post('iddepartement');
   // print_r( $Departement);exit();
    if ($Departement==1) { //```````````` 
        $set = 'stokBO';
        $Istok = 'isBO';
        $IsProses = 'isprosesopnameBO';
       }elseif ($Departement==2) {
        $set = 'stokHK';
        $Istok = 'isHK';
         $IsProses = 'isprosesopnameHK';
       }elseif ($Departement==3) {
         $set = 'stokFO';
         $Istok = 'isFO';
          $IsProses = 'isprosesopnameFO';
       }elseif ($Departement==4) {
          $set = 'stokFB';
          $Istok = 'isFB';
          $IsProses = 'isprosesopnameFB';
       }elseif ($Departement==5) {
        $set = 'stokBO';
        $Istok = 'isBO';
         $IsProses = 'isprosesopnameBO';
       }elseif ($Departement==6) {
        $set = 'stokBar';
        $Istok = 'isBar';
        $IsProses = 'isprosesopnameBar';
      }elseif ($Departement==7) {
        $set = 'stokEng';
        $Istok = 'isEng';
        $IsProses = 'isprosesopnameEng';
      }elseif ($Departement==8) {
        $set = 'stokPG';
        $Istok = 'isPG';
        $IsProses = 'isprosesopnamePG';
      }elseif ($Departement==9) {
        $set = 'stokSec';
        $Istok = 'isSec';
        $IsProses = 'isprosesopnameSec';
       }else{
         $Istok = '';
         $set = 'stok';
         $IsProses = 'isprosesopname';
       }
    $sub = $this->M_stok_opname->Select_subcategory_bydepartememnt($Departement);
    echo ' <div class="form-group" id="groupSubcategory">';
    echo ' <label>Subcategory</label> ';
    echo  '<select id="SubcategorysrItem" class="form-control select2"  style="width: 100%;" name="SubcategorysrItem"> ';
    echo ' <option value="" >Semua Subcategory</option>';
    foreach ($sub as $pek ) {
                    echo ' <option value="'.$pek->idsubcategory.'" >'.$pek->subcategory.'</option>';
    }
    echo '</select>';
    echo '</div>';

  }


	function Cetak($JenisLaporan,$Text){
		$data['hotel'] = $this->M_rbac->getPerusahaan();
		$data['qr'] = $this->ciqrcode->generateQRcode($JenisLaporan, $Text,1.9);
		$data1= $this->load->view('template/Cetak_head',$data, TRUE);
		return $data1;
	}
	function CetakSO(){
		date_default_timezone_set('Asia/Hong_Kong');
		$idsub = $this->input->get('idsub', TRUE);
		$sub = $this->input->get('sub', TRUE);
		$departement = $this->input->get('dep', TRUE);
		$iddepartement = $this->input->get('iddep', TRUE);

		$tgl = $this->input->get('Tgl', TRUE);
		$tgl1 = DateTime::createFromFormat('m/d/Y',$tgl);
		$tgl2 = $tgl1->format("Y-m-d");
		$tgl3 = $tgl1->format("d-m-Y");//print_r($tgl2);exit();

	    $data['header'] = $this->Cetak("Stok_opname",  $this->session->userdata('Nama_userPms'). date("d-m-Y_H-i-s"), 1);
	    $data['head'] =  array('NamaDep' =>$departement,'NamaSub' =>$sub,'Tanggal' =>$tgl3);
	    $data['kontenSO'] = $this->M_stok_opname->load_so_forlaporan($tgl2,$idsub,$iddepartement); 
	   // print_r( $data['kontenSO']);exit();
	    $html=$this->load->view('stok_opname/Cetak/SO',$data, TRUE);
     	$this->create_pdf->load($html,'Stok_opname'.'-'.$data['kontenSO'][0]->tgl, 'A4-P');	
	}
	function CetakSODiret(){
		date_default_timezone_set('Asia/Hong_Kong');
		$tgl = $this->input->get('Tgl', TRUE);
	    $data['header'] = $this->Cetak("Stok_opname",  $this->session->userdata('Nama_userPms'). date("d-m-Y_H-i-s"), 1);
	    $data['kontenSO'] = $this->M_stok_opname->load_so_forlaporan($tgl);
	    $html=$this->load->view('stok_opname/Cetak/SO',$data, TRUE);
     	$this->create_pdf->load($html,'Stok_opname'.'-'.$data['kontenSO'][0]->tgl, 'A4-P');	
	}

	function CetakFormSO(){
		//date_default_timezone_set('Asia/Hong_Kong');
		$idsub = $this->input->get('kode', TRUE);
		$departement = $this->input->get('dep', TRUE);
		$iddepartement = $this->input->get('iddep', TRUE);

		$sub = $this->input->get('sub', TRUE);
	    $data['header'] = $this->Cetak("Form_Stok_opname",  $this->session->userdata('Nama_userPms'). date("d-m-Y_H-i-s"), 1);
	    $data['kontenSO'] = $data['data'] = $this->M_stok_opname->stok_opname_view_form($idsub,$iddepartement);
	    $data['departement'] =  array('nama' =>$departement);
	   // print_r( $data['kontenSO']);exit();
	    $html=$this->load->view('stok_opname/Cetak/Form_so',$data, TRUE);
     	$this->create_pdf->load($html,'Form_Stok_opname', 'A4-P');	
	}

	

	


}
?>

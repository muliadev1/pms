<?php

class Roomstatus extends CI_Controller{

	function __construct(){
		parent::__construct();
		if($this->session->userdata('Kode_userPms')==NULL) {
           redirect('Login');
		}
		$this->load->library('session');
		$this->load->helper(array('form', 'url'));
		$this->load->model(array('M_roomstatus'));
		date_default_timezone_set('Asia/Hong_Kong');
		
		if (!empty($this->session->userdata("active_db"))) {
			$this->load->database($this->session->userdata("active_db"), FALSE, TRUE);
		}
	}

	function index(){
		redirect('RoomStatus/RoomStatus_view');
	}

	function RoomStatus_view(){
		$data['data'] = $this->M_roomstatus->RoomStatus_view();
		$this->load->view('roomstatus/RoomStatus',$data);
	}

	function RoomStatus_add(){
		$this->load->view('roomstatus/RoomStatus_add');
	}

	function RoomStatus_addDB(){
		$data = array(
			'status' => $this->input->post('status'),
			'description' => $this->input->post('description'),
		);
		$this->M_roomstatus->RoomStatus_addDB('tbroomstatus',$data) ;
		$this->session->set_flashdata('msg', 'Add Item Room Status successfully ...');
		redirect('Roomstatus/RoomStatus_view');
	}

	function RoomStatus_edit($id){
		$data['data'] = $this->M_roomstatus->RoomStatus_view_edit($id) ;
		$this->load->view('roomstatus/RoomStatus_edit',$data);
	}

	function RoomStatus_editDB(){
		$id = $this->input->post('Id');
		$data = array(
			'status' => $this->input->post('status'),
			'description' => $this->input->post('description'),
	);

		$this->M_roomstatus->RoomStatus_editDB('tbroomstatus',$data,$id) ;
		$this->session->set_flashdata('msg', 'Edit Room Status successfully...');
		redirect('Roomstatus/RoomStatus_view');
	}

}
?>

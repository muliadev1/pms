
          <div class="table-responsive">
                    <table id="tpay" class="table table-bordered table-striped">
                                <thead>
                                    <tr >
                                        <th >Voucher NO</th>
                                        <th >Date</th>
                                        <th>Vendor</th>
                                        <th>Total</th>
                                        <th></th>  
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    foreach($data as $u){
                                    ?>
                                    <tr>
                                         
                                         <td class="Kode" ><?php echo $u->voucherno ?></td>
                                         <td ><?php echo $u->voucherdate ?></td>
                                         <td class="Vendor"><?php echo $u->vendor ?></td>
                                         <td align="right" class="Price"><?php echo number_format($u->total, 0, '.', '.') ?></td>
                                      <td align="center">
                                       <a class='btn btn-default Pilih' id="" data-toggle='modal'     >  <span class='fa fa-fw  fa-check-square-o' ></span> </a>
                                      </td>

                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
          </div>
          
<script type="text/javascript">
  $(document).ready(function(){
     $(".Pilih").click(function(e){       
            var Kode =$(this).closest('tr').children('td.Kode').text();
            var mymodal11 = $('#ModalVoucher');
       
        $('#NoPay').val(Kode);
        mymodal11.modal('hide');    
    });
  });
</script>

  <script type="text/javascript">
  $(function () {
     $("#tpay").DataTable();
  });
  </script>
        
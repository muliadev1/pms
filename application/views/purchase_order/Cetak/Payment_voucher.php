
<?php echo $header?>

<style type="text/css">
  #TabelKonten tr td {
    padding-right: 7px;
    padding-left:  7px;
    font-size: 12px;
  }
</style>

<div style="margin-buttom : 15px;" >
<table width="100%" border="0"  >
    <tr>
     <td colspan="2" align="center" style="font-size:14px;"> <strong> PAYMENT VOUCHER  </strong>  </td>
    </tr>
    
   
  </table>
<table width="100%" border="0" style="font-size:11px;"  >
    
    <tr>
     <td><strong>Voucher No</strong></td>
     <td align="left">: <?php echo $kontenVoucher[0]->voucherno;?></td>
    </tr>
    <tr>
     <td width="20%"><strong>Date</strong> </td>
     <td align="left">: <?php echo $kontenVoucher[0]->voucherdate;?></td>
    </tr>
    <tr>
     <td>&nbsp;</td>
     <td align="left">&nbsp;</td>
    </tr>
  </table>
</div>

<table id="TabelKonten"  border="1" style="border-collapse: collapse; border-color:#000000; margin-bottom : 130px; font-size:12px;"  width="100%"   >
 <thead>
    <tr>
      <th colspan="3" scope="col">Pay To</th>
      <th colspan="3" scope="col">Payment Method</th>
    </tr>
    <tr>
      <th colspan="3" rowspan="2" align="left"> <strong>Vendor </strong> :<?php echo $kontenVoucher[0]->vendor;?></th>
      <th>Cash</th>
      <th>Check</th>
      <th>Amount</th>
    </tr>
    <tr>
    <?php $Grandtotal = 0 ; foreach ($kontenVoucher as $pek ) {
    $Grandtotal+= $pek->amount; }?>

    <?php if ($kontenVoucher[0]->paymentmethod==0) { ?>
      <th><strong><?php echo number_format( $Grandtotal, 0, '.', '.') ?></strong></th>
      <th>-</th>
      <th><strong><?php echo number_format( $Grandtotal, 0, '.', '.') ?></strong></th>
   <?php  } else{ ?>
      <th>-</th>
      <th><strong><?php echo number_format( $Grandtotal, 0, '.', '.') ?></strong></th>
      <th><strong><?php echo number_format( $Grandtotal, 0, '.', '.') ?></strong></th>

    <?php }?>
      
    </tr>
</thead>
  <tbody>
    <tr>
      <td align="center" height="30px;" ><strong>Date</strong></td>
      <td align="center" align="center"><strong>Receive No</strong></td>
      <td align="center"><strong>Amount</strong> </td>
      <td colspan="3" align="center"><strong>Description</strong></td>
    </tr>
    
   
     <?php foreach ($kontenVoucher as $pek ) { ?>
         <tr>
        <td><?php echo $pek->rodate ?></td>
        <td><?php echo $pek->kodero ?></td>
        <td align="right"><?php echo number_format($pek->amount, 0, '.', '.') ?></td>
        <td colspan="3" align="center"><?php echo $pek->description ?></td>
          </tr>
     <?php } ?>

    <tr style="background-color:#E6EBE6;">
      <td colspan="2" align="center"> <strong> TOTAL</strong></td>
      <td align="right"> <strong><?php echo number_format( $Grandtotal, 0, '.', '.') ?></strong></td>
      <td colspan="3" >&nbsp;</td>
    </tr>
  </tbody>
</table>



<table width="100%" border="0" style="font-size:11px; "  >
    <tr>
      <td >&nbsp; </td>
      <td >&nbsp; </td>     
      <td >&nbsp; </td>
      <td >&nbsp; </td>
    </tr>
     <tr>
      <td >&nbsp; </td>
      <td >&nbsp; </td>     
      <td >&nbsp; </td>
      <td >&nbsp; </td>
    </tr>
  <tr>
     <td align="center" ><strong>APPROVED BY</strong> </td>
     <td align="center"><strong>CHECKED BY</strong></td>
     <td align="center"><strong>PREPARED BY</strong></td>
     <td align="center"><strong>RECEIVED BY</strong></td>
    </tr>
     <tr>
      <td >&nbsp; </td>
      <td >&nbsp; </td>     
      <td >&nbsp; </td>
      <td >&nbsp; </td>
    </tr>
     <tr>
      <td >&nbsp; </td>
      <td >&nbsp; </td>     
      <td >&nbsp; </td>
      <td >&nbsp; </td>
    </tr>
     <tr>
      <td >&nbsp; </td>
      <td >&nbsp; </td>     
      <td >&nbsp; </td>
      <td >&nbsp; </td>
    </tr>
     <tr>
      <td ><hr style="color:#000000"> </td>
      <td ><hr style="color:#000000"> </td>     
      <td ><hr style="color:#000000"> </td>
      <td ><hr style="color:#000000"> </td>
    </tr>
   
  </table>


  






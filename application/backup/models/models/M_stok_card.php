
<?php 

class M_stok_card extends CI_Model{	



 function get_stok_awal_byitem($tgl1,$tgl2,$itemcode){

        $query = $this->db->query("  SELECT SUM(Ins) as masuk,SUM(Outs) as keluar FROM (
                                        SELECT `datereceive` AS tgl ,`itemcode` AS codeitem,`qty` AS qty,`description`,'RO' AS keterangan,'IN' AS STATUS,'Store' AS departement
                                        ,qty AS INs , 0 AS Outs 
                                        FROM `tbrodetil` INNER JOIN `tbro`ON `tbrodetil`.`kodero` = `tbro`.`kodero`
                                        UNION
                                        SELECT `dateadd` AS tgl ,codeitem AS codeitem,qtyorder AS qty,tbsrdetil.description, 'SR' AS keterangan ,'OUT' AS STATUS,tbsrdetil.`departement`
                                        ,0 AS INs , qtyorder AS OUTs  
                                        FROM `tbsrdetil` INNER JOIN `tbsr` ON tbsrdetil.`kodesr` = tbsr.`kodesr` WHERE tbsrdetil.isSO=0
                                        UNION
                                        SELECT `dateadd` AS tgl ,codeitem AS codeitem,qtyorder AS qty,tbsrdetil.description, 'SO OUT' AS keterangan ,'OUT' AS STATUS,tbsrdetil.`departement`
                                        ,0 AS INs , qtyorder AS OUTs
                                        FROM `tbsrdetil` INNER JOIN `tbsr` ON tbsrdetil.`kodesr` = tbsr.`kodesr` WHERE tbsrdetil.isSO=1 AND tbsrdetil.statusInOut=1
                                        UNION
                                        SELECT `dateadd` AS tgl ,codeitem AS codeitem,qtyorder AS qty,tbsrdetil.description, 'SO IN' AS keterangan ,'IN' AS STATUS,tbsrdetil.`departement`
                                        ,qtyorder AS INs , 0 AS OUTs  
                                        FROM `tbsrdetil` INNER JOIN `tbsr` ON tbsrdetil.`kodesr` = tbsr.`kodesr` WHERE tbsrdetil.isSO=1 AND tbsrdetil.statusInOut=0
                                        UNION
                                        SELECT `dateadd` AS tgl ,codeitem AS codeitem,qty AS qty,tbbadstokdetil.description, 'BS' AS keterangan ,'OUT' AS STATUS,tbbadstokdetil.`departement`
                                        ,0 AS INs ,qty AS OUTs  
                                        FROM `tbbadstokdetil` INNER JOIN `tbbadstok` ON tbbadstokdetil.`kodebs` = tbbadstok.`kodebs` 
                                      ) AS a WHERE DATE(tgl) BETWEEN '$tgl1' AND '$tgl2' and codeitem='$itemcode'
                                    "); 
         $perjalananstok =$query->row();
          $query1 = $this->db->query("select * from tbitem where codeitem='00001'");
          $stokditb = $query1->row();
         $stokawal =$stokditb->stok - $perjalananstok->masuk + $perjalananstok->keluar ;
         return $stokawal;
  }
  function Load_SC_bytgl($tgl1,$tgl2,$itemcode) { 
        $query = $this->db->query("SELECT tgl,codeitem,qty,description,keterangan,departement, STATUS, DATE_FORMAT(tgl,'%d-%m-%Y') AS tgl1 FROM (
                                      SELECT `datereceive` AS tgl ,`itemcode` AS codeitem,`qty` AS qty,`description`,'RO' AS keterangan,'IN' AS STATUS,'Store' AS departement
                                      FROM `tbrodetil` INNER JOIN `tbro`ON `tbrodetil`.`kodero` = `tbro`.`kodero`
                                      UNION
                                      SELECT `dateadd` AS tgl ,codeitem AS codeitem,qtyorder AS qty,tbsrdetil.description, 'SR' AS keterangan ,'OUT' AS STATUS,tbsrdetil.`departement`
                                      FROM `tbsrdetil` INNER JOIN `tbsr` ON tbsrdetil.`kodesr` = tbsr.`kodesr` WHERE tbsrdetil.isSO=0
                                      UNION
                                      SELECT `dateadd` AS tgl ,codeitem AS codeitem,qtyorder AS qty,tbsrdetil.description, 'SO OUT' AS keterangan ,'OUT' AS STATUS,tbsrdetil.`departement`
                                      FROM `tbsrdetil` INNER JOIN `tbsr` ON tbsrdetil.`kodesr` = tbsr.`kodesr` WHERE tbsrdetil.isSO=1 AND tbsrdetil.statusInOut=1
                                      UNION
                                      SELECT `dateadd` AS tgl ,codeitem AS codeitem,qtyorder AS qty,tbsrdetil.description, 'SO IN' AS keterangan ,'IN' AS STATUS,tbsrdetil.`departement`
                                      FROM `tbsrdetil` INNER JOIN `tbsr` ON tbsrdetil.`kodesr` = tbsr.`kodesr` WHERE tbsrdetil.isSO=1 AND tbsrdetil.statusInOut=0
                                      UNION
                                      SELECT `dateadd` AS tgl ,codeitem AS codeitem,qty AS qty,tbbadstokdetil.description, 'BS' AS keterangan ,'OUT' AS STATUS,tbbadstokdetil.`departement`
                                      FROM `tbbadstokdetil` INNER JOIN `tbbadstok` ON tbbadstokdetil.`kodebs` = tbbadstok.`kodebs` 
                                  ) AS a WHERE DATE(tgl) BETWEEN '$tgl1' AND '$tgl2' and codeitem='$itemcode'
                                  ORDER BY tgl ASC"); 
         return $query->result() ;
  } 

  function item_view() {
   $query = $this->db->query("SELECT tbitem.*, tbsubcategory.`description` AS descsubcategory FROM tbitem INNER JOIN `tbsubcategory`
                                ON tbitem.`idsubcategory` =  tbsubcategory.`idsubcategory`  
                                 "); 
    return $query->result() ;     
            
   }

     



  

  


  


      



}
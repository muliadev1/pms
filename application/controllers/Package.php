<?php

class Package extends CI_Controller{

	function __construct(){
		parent::__construct();
		if($this->session->userdata('Kode_userPms')==NULL) {
           redirect('Login');
		}
		$this->load->library(array('session','country'));
		$this->load->helper(array('form', 'url'));
		$this->load->model(array('M_Company','M_package'));
		date_default_timezone_set('Asia/Hong_Kong');
		
		if (!empty($this->session->userdata("active_db"))) {
			$this->load->database($this->session->userdata("active_db"), FALSE, TRUE);
		}
	}

	function index(){
		redirect('Company/Company_view');
	}

	function Package_view(){
		$data['data']= $this->M_package->Package_view(); 
		$this->load->view('package/Package',$data);
	}
	function Package_add_view(){
		$this->load->view('package/Package_add');
	}

	function Package_view_edit($id){
		$data['data']= $this->M_package->package_view_edit($id); 
		$this->load->view('package/Package_edit',$data);
	}
	function Add_article_package(){
		$index = $this->input->post('index');
		$data = ' 
	<div style="background-color: #F2F2F7; margin-top:20px;" id="element'.$index.'" >
        <div class="row"   style="margin-left:10px;">
           <div class="col-md-6">
              <div class="form-group">
              <label>Article Name</label>
               <input type="text" name="ArticleName['.$index.']" id="ArticleName['.$index.']"  class="form-control remove"  placeholder="Atricle Name">
            </div>
          </div>
          <div class="col-md-6">
               <div class="form-group">
                 <a class="btn btn-danger btn-xs remove" idk = "'.$index.'" style="margin-top:30px;" >  <span class="fa fa-times " ></span> </a>
              </div>
            </div>
        </div>
        <div class="row" style="margin-left:10px;">
          <div class="col-md-6">
               <div class="form-group">
               <input type="text" name="ArticleRate['.$index.']" id="ArticleRate['.$index.']"  class="form-control"  placeholder="Atricle Rate">
              </div>
            </div>
        </div>
    </div>';
		echo $data;
	}

	function package_add(){
		$packagename = $this->input->post('PackageName');
		$packagerate = $this->input->post('PackageRate');
		$articlename = $this->input->post('ArticleName');
		$articlerate = $this->input->post('ArticleRate');
		$idpackagenew =$this->M_package->get_idpackage(); 

		$articlename1 = array_combine(range(1, count($articlename)), array_values($articlename));
		$articlerate1 = array_combine(range(1, count($articlerate)), array_values($articlerate));
//print_r($dataPackage);exit();
		$Jumlah = count($articlename1);
				for ($i=1; $i <= $Jumlah ; $i++) { 
				 	 $dataPackage[] = array('id' => $idpackagenew,
						 				 'package' => $packagename,
						 				 'packagerate' => $packagerate,
						 				'remark' =>'-',
						 				'article' => $articlename1[$i],
						 				'articlerate' => $articlerate1[$i]
						 		 );
				 }

		$this->M_package->package_addDB($dataPackage);
		$this->session->set_flashdata('msg', ' Add Package successfully... ');
		redirect('Package/Package_view');
		
	}


	
	

}
?>

<?php

class Menu_subcategory extends CI_Controller{

	function __construct(){
		parent::__construct();
		if($this->session->userdata('Kode_userPms')==NULL) {
           redirect('Login');
		}
		$this->load->library('session');
		$this->load->helper(array('form', 'url'));
		$this->load->model(array('M_menu_category'));
		$this->load->model(array('M_menu_subcategory'));
		
		if (!empty($this->session->userdata("active_db"))) {
			$this->load->database($this->session->userdata("active_db"), FALSE, TRUE);
		}
	}

	function index(){
		redirect('Menu_subcategory/MenuSubcategory_view');
	}

	function MenuSubcategory_view(){
		$data['data'] = $this->M_menu_subcategory->MenuSubcategory_view();
		$this->load->view('menu_subcategory/menu_subcategory',$data);
	}


	function MenuSubcategory_add(){
		$data['data'] = $this->M_menu_category->MenuCategory_view();
		$this->load->view('menu_subcategory/menu_subcategory_add',$data);
	}

	function MenuSubcategory_addDB(){
		$data = array(
			'idcategory' => $this->input->post('idcategory'),
			'subcategory' => $this->input->post('subcategory'),
			'description' => $this->input->post('description')
		);
		$this->M_menu_subcategory->MenuSubcategory_addDB('tbmenusubcategory',$data) ;
		$this->session->set_flashdata('msg', 'Add Menu Sub Category successfully ...');
		redirect('Menu_subcategory/MenuSubcategory_view');
	}
	function MenuSubcategory_edit($id){
		$data['data1'] = $this->M_menu_category->MenuCategory_view();
		$data['data2'] = $this->M_menu_subcategory->MenuSubcategory_view_edit($id);
		$this->load->view('menu_subcategory/menu_subcategory_edit',$data);
	}

	function MenuSubcategory_editDB(){
		$id = $this->input->post('Id');
		$data = array(
			'idcategory' => $this->input->post('idcategory'),
			'subcategory' => $this->input->post('subcategory'),
			'description' => $this->input->post('description')

	);
		$this->M_menu_subcategory->MenuSubcategory_editDB('tbmenusubcategory',$data,$id) ;
		$this->session->set_flashdata('msg', 'Edit Menu Sub Category successfully...');
		redirect('Menu_subcategory/MenuSubcategory_view');


	}


}
?>


<?php echo $header?>

<style type="text/css">
  #TabelKonten tr td {
    padding-right: 7px;
    padding-left:  7px;
    font-size: 12px;
  }
</style>

<div style="margin-buttom : 15px;" >
<table width="100%" border="0"  >
    <tr>
     <td colspan="2" align="center" style="font-size:14px;"> <strong> PURCHASE REPORT  </strong>  </td>
    </tr>
    
   
  </table>
<table width="100%" border="0" style="font-size:11px;"  >
   <tr>
     <td>&nbsp;</td>
     <td align="left">&nbsp;</td>
    </tr>
  
    <tr>
    <tr>
     <td width="10%"><strong>Date</strong> </td>
     <td align="left">: <?php echo $periode['TglAwal'];?> <strong>TO</strong> <?php echo $periode['TglAkhir'];?> </td>
    </tr>
     <tr>
     <td width="10%"><strong>Category</strong> </td>
     <td align="left">: <?php echo $periode['Cat'];?> </td>
    </tr>
    <tr>
     <td>&nbsp;</td>
     <td align="left">&nbsp;</td>
    </tr>
    

  </table>
</div>

<table id="TabelKonten"  border="1" style="border-collapse: collapse; border-color:#000000; margin-bottom : 130px;"  width="100%"   >
    <thead  >
        <tr align="center" class="header">
            <th width="5%" style="font-size: 11px;" >NO</th>
            <th width="10%" style="font-size: 11px;">DATE</th>
            <th width="10%" style="font-size: 11px;">MI No.</th>
            <th width="15%" style="font-size: 11px;">VENDOR</th>
            <th width="10%" style="font-size: 11px;">ALAT TULIS</th>
            <th width="10%" style="font-size: 11px;">BAHAN PEMBERSIH</th>
            <th width="10%" style="font-size: 11px;"> KEPERLUAN TAMU</th>
            <th width="10%" style="font-size: 11px;">BAHAN POOL</th>
            <th width="10%" style="font-size: 11px;">GAS</th>
            <th width="10%" style="font-size: 11px;">ALAT REKAYASA</th>
        </tr>
    </thead>
     <tbody>
      <?php $total=0;$no=1; 
      $subtotalmeat = 0; $subtotalvg = 0;  $subtotalfr = 0; $subtotalgro = 0; $subtotaldp=0; $subtotalhari=0;
      $totalmeat = 0; $totalvg = 0;$totalfr = 0; $totalgro = 0; $totaldp=0; $totalhari=0; 
      $tglhari = '-'; $last=count($konten);// print_r($last);exit();
       foreach ($konten as $row) { ?>
    <tr>
      <td><?php echo $no; ?></td>
      <td><?php echo $row->tgl; ?></td>
      <td><?php echo $row->kodero; ?></td>
      <td style="font-size: 10px;"><?php echo $row->vendor; ?></td>
          
          <?php if ($row->idsubcategory=='21') { $subtotalmeat+=$row->subtotal; ?>
           <td align="right"><?php echo number_format($row->subtotal , 0, '.', '.'); ?></td>
           <td>&nbsp;</td>
           <td>&nbsp;</td>
           <td>&nbsp;</td>
           <td>&nbsp;</td>
            <td  align="right"><?php echo number_format($row->subtotal , 0, '.', '.'); ?></td>

          <?php }elseif($row->idsubcategory=='1' ) { $subtotalfr+=$row->subtotal; ?>
           <td >&nbsp;</td>
           <td align="right"><?php echo number_format($row->subtotal , 0, '.', '.'); ?></td>
            <td>&nbsp;</td>
           <td>&nbsp;</td>
           <td>&nbsp;</td>
           <td  align="right"><?php echo number_format($row->subtotal , 0, '.', '.'); ?></td>

           <?php }elseif( $row->idsubcategory=='2') { $subtotalvg+=$row->subtotal; ?>
           <td>&nbsp;</td>
           <td>&nbsp;</td>
           <td  align="right"><?php echo number_format($row->subtotal , 0, '.', '.'); ?></td>
           <td>&nbsp;</td>
           <td>&nbsp;</td>
           <td  align="right"><?php echo number_format($row->subtotal , 0, '.', '.'); ?></td>

           <?php }elseif($row->idsubcategory=='22') { $subtotalgro+=$row->subtotal; ?>
           <td>&nbsp;</td>
           <td>&nbsp;</td>
            <td>&nbsp;</td>
           <td  align="right"><?php echo number_format($row->subtotal , 0, '.', '.'); ?></td>
           <td>&nbsp;</td>
           <td  align="right"><?php echo number_format($row->subtotal , 0, '.', '.'); ?></td>

           <?php }elseif($row->idsubcategory=='27') { $subtotaldp+=$row->subtotal ?>
           <td>&nbsp;</td>
           <td>&nbsp;</td>
           <td>&nbsp;</td>
            <td>&nbsp;</td>
           <td  align="right"><?php echo number_format($row->subtotal , 0, '.', '.'); ?></td>
           <td  align="right"><?php echo number_format($row->subtotal , 0, '.', '.'); ?></td>
           <?php }else{ ?>
            <td>&nbsp;</td>
           <td>&nbsp;</td>
           <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>

            <?php }?> 
       </tr>           

           <?php if ($tglhari!=$row->tgl and $no!=1 ) { ?>
        <tr style="background-color: #f0f0f0;">
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td  align="right"> <strong><?php echo number_format($subtotalmeat , 0, '.', '.'); ?></strong></td>
          <td  align="right"> <strong><?php echo number_format($subtotalfr , 0, '.', '.'); ?></strong></td>
          <td  align="right"> <strong><?php echo number_format($subtotalvg , 0, '.', '.'); ?></strong></td>
          <td  align="right"> <strong><?php echo number_format($subtotalgro , 0, '.', '.'); ?></strong></td>
          <td  align="right"> <strong><?php echo number_format($subtotaldp , 0, '.', '.'); ?></strong></td>
          <td  align="right"> <strong><?php echo number_format($subtotalmeat+$subtotalvg+$subtotalfr+$subtotalgro+$subtotaldp , 0, '.', '.'); ?></strong></td>
        </tr>
            <?php  $totalmeat+= $subtotalmeat; $totalvg+=$subtotalvg; $totalfr+=$subtotalfr; $totalgro+=$subtotalgro;$totaldp+=$subtotaldp;
              $subtotalmeat = 0; $subtotalvg = 0;$subtotalfr = 0; $subtotalgro = 0; $subtotaldp=0; $subtotalhari=0; } ?>
   
      
      <?php $no++; $totalhari+=$row->subtotal; $tglhari=$row->tgl;} ?>

         <tr style="background-color: #f0f0f0;">
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td  align="right"> <strong><?php echo number_format($subtotalmeat , 0, '.', '.'); ?></strong></td>
          <td  align="right"> <strong><?php echo number_format($subtotalfr , 0, '.', '.'); ?></strong></td>
          <td  align="right"> <strong><?php echo number_format($subtotalvg , 0, '.', '.'); ?></strong></td>
          <td  align="right"> <strong><?php echo number_format($subtotalgro , 0, '.', '.'); ?></strong></td>
          <td  align="right"> <strong><?php echo number_format($subtotaldp, 0, '.', '.'); ?></strong></td>
          <td  align="right"> <strong><?php echo number_format($subtotalmeat+$subtotalvg+$subtotalfr+$subtotalgro+$subtotaldp , 0, '.', '.'); ?></strong></td>
        </tr>

        <tr style="background-color: #f0f0f0;">
          <td colspan="9">&nbsp;</td>
        </tr>
        <tr style="background-color: #f0f0f0;">
          <td colspan="4" align="center"><strong>TOTAL</strong></td>
          <td  align="right"> <strong><?php echo number_format($totalmeat , 0, '.', '.'); ?></strong></td>
          <td  align="right"> <strong><?php echo number_format($totalfr , 0, '.', '.'); ?></strong></td>
          <td  align="right"> <strong><?php echo number_format($totalvg , 0, '.', '.'); ?></strong></td>
          <td  align="right"> <strong><?php echo number_format($totalgro , 0, '.', '.'); ?></strong></td>
          <td  align="right"> <strong><?php echo number_format($totaldp , 0, '.', '.'); ?></strong></td>
          <td  align="right"> <strong><?php echo number_format($totalmeat+$totalvg+$totalgro+$totaldp , 0, '.', '.'); ?></strong></td>
        </tr>
   
    </tbody>

    <tfoot>
      
    </tfoot>
     

  </table>



<!-- <table width="100%" border="0" style="font-size:11px; "  >
    <tr>
      <td >&nbsp; </td>
      <td >&nbsp; </td>     
      <td >&nbsp; </td>
    </tr>
     <tr>
      <td >&nbsp; </td>
      <td >&nbsp; </td>     
      <td >&nbsp; </td>
    </tr>
  <tr>
     <td ><strong>QUANTYTY AND QUALITY APPROVED</strong> </td>
     <td ><strong>PRICE APPROVED</strong></td>
     <td ><strong>RECEIVED</strong></td>
    </tr>
         <tr>
      <td >&nbsp; </td>     
      <td >&nbsp; </td>
      <td >&nbsp; </td>
    </tr>
     <tr>
      <td >&nbsp; </td>     
      <td >&nbsp; </td>
      <td >&nbsp; </td>
    </tr>
     <tr>
      <td >&nbsp; </td>     
      <td >&nbsp; </td>
      <td >&nbsp; </td>
    </tr>
     <tr>
      <td ><hr style="color:#000000"> </td>     
      <td ><hr style="color:#000000"> </td>
      <td ><hr style="color:#000000"> </td>
    </tr>
   
  </table> -->


  






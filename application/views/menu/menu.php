<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>

<section class="content-header">
    <div class="btn-group btn-breadcrumb">
            <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
            <a  class="btn btn-default  btn-xs active">Menu</a>
        </div>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Menu</h3>
          <a style="float:right"  href="<?php echo base_url('index.php/Menu/Menu_add');?>" class="btn btn-primary">
            <i class="glyphicon glyphicon-saved"></i>
            Add Item
          </a>
        </div>
    <!-- /.box-header -->
        <div class="box-body">
        <div class="table-responsive">
          <table id="example1" class="table table-bordered table-striped text-center">
                                <thead>
                                    <tr >
                                        <th>ID</th>
                                        <th>Sub Category</th>
                                        <th>Menu</th>
                                        <th>Description</th>
                                        <th>Price (IDR)</th>
                                        <th>Tax</th>
                                        <th>Service</th>
                                        <th>Final Price</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach($data as $u){

                                    ?>
                                    <tr  class='odd gradeX context'>
                                        <td><?php echo $u->id ?></td>
                                        <td><?php echo $u->subcategory?></td>
                                        <td><?php echo $u->menu?></td>
                                        <td><?php echo $u->description?></td>
                                        <td><?php echo number_format($u->price, 0, '.', '.') ?></td>
                                        <td><?php echo number_format($u->tax, 0, '.', '.')?></td>
                                        <td><?php echo number_format($u->service, 0, '.', '.')?></td>
                                        <td><?php echo number_format($u->finalprice, 0, '.', '.')?></td>
                                         <td align="right">
                                         <a class="btn btn-primary"   href="<?php echo base_url('index.php/Menu/Menu_edit/'.$u->id); ?>">  <span class="fa fa-fw fa-edit" ></span> </a>
                                        <!-- <input type="hidden" id="btnhapus" value="<?php //echo $u->description ?>" > -->
                                        </td>






                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
          </div>



        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->

</section><!-- /.content -->


<?php
$this->load->view('template/Js');
$this->load->view('template/Foot');
?>

<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>

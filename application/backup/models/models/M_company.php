<?php

class M_Company extends CI_Model{

	function Company_view() {
		$data = $this->db->query("SELECT * FROM tbcompany WHERE isaktif=1");
		return $data->result();
   }

	 function Company_addDB($table,$data) {
			 $this->db->insert($table,$data);
			// print_r($this->db->last_query());
	}

	function Company_view_edit($id) {
	 $this->db->select("*");
	 $this->db->where('idcompany',$id);
	 $this->db->from('tbcompany');
	 return $this->db->get()->result();
	}

	function Company_editDB($table,$data,$id) {
 			 $this->db->where('idcompany',$id );
 			 $this->db->update($table,$data);
  }

}
?>

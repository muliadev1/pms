
<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');

?>
<style type="text/css">
  .box-widget {
 box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
}
.modal.modal-wide .modal-dialog {
  width: 90%;
}

.modal-wide .modal-body {
  overflow-y: auto;
}

#tallModal .modal-body p { margin-bottom: 900px }
.pull-right {
    float: right;
}

.pull-left {
    float: left;
}
</style>

<!-- Content Header (Page header) -->
<section class="content-header" align="center" style="margin-bottom: 24px; ">
   
    <h3> 
        Cost Control Report
    </h3>
    
</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-xs-12">
    <?php $this->load->view('template/msg_sukses'); ?>


  <div class="col-xs-12">
      <div class="box box-success box-widget">
        <div class="box-header" >
          <h6 class="box-title" style="font-size: 15px;" >MEMORANDUM INVOICE</h6>
        </div>
        <div class="box-body">
    <div class="row">
      <div class="col-sm-4">
         <div class="form-group">
            <label>NO Receive Order</label>
            <div class="input-group date">
            <input type="text" name="NoRo" id="NoRo"  class="form-control input-2">
            <div class="input-group-addon " id="CariRo" >
                <i class="fa fa-search"></i>
              </div>
            </div>
          </div>
        </div>
     
        <div class="col-sm-2">
          <div class="form-group">
             <a style="margin-top: 24px; " class="btn btn-default MI"><i class='glyphicon glyphicon-print'></i> Cetak</a>
          </div>
        </div> 
      </div>
       <div class="row">
      <div class="col-sm-4">
         <div class="form-group">
            <label>Dari Tanggal</label>
            <div class="input-group date">
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
              <input type="text"  data-validation="date" data-validation-format="mm/dd/yyyy" data-validation-error-msg="Tanggal Lahir Belum Dipilih"  data-validation-require-leading-zero="false" class="form-control pull-right" name= "TglLahir" id="TglAwalMI">
            </div>
          </div>
        </div>
      <div class="col-sm-4">
          <div class="form-group">
            <label>Sampai Tanggal</label>
            <div class="input-group date">
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
              <input type="text"  data-validation="date" data-validation-format="mm/dd/yyyy" data-validation-error-msg="Tanggal Lahir Belum Dipilih"  data-validation-require-leading-zero="false" class="form-control pull-right" name= "TglLahir" id="TglAkhirMI">
            </div>
          </div>
        </div>
        <div class="col-sm-2">
          <div class="form-group">
             <a style="margin-top: 24px; " class="btn btn-default CetakTglMI"><i class='glyphicon glyphicon-print'></i> Cetak</a>
          </div>
        </div> 
      </div>
    </div> 
   </div>
  </div>





   <div class="col-xs-12">
      <div class="box box-success box-widget">
        <div class="box-header" >
          <h6 class="box-title" style="font-size: 15px;">STORE REQUISITION</h6>
        </div>
        <div class="box-body">
    <div class="row">
      <div class="col-sm-4">
         <div class="form-group">
            <label>NO Store Requisition</label>
            <div class="input-group date">
            <input type="text" name="NoSR" id="NoSR"  class="form-control input-2">
            <div class="input-group-addon " id="CariNoSR" >
                <i class="fa fa-search"></i>
              </div>
            </div>
          </div>
        </div>
     
        <div class="col-sm-2">
          <div class="form-group">
             <a style="margin-top: 24px; " class="btn btn-default SR"><i class='glyphicon glyphicon-print'></i> Cetak</a>
          </div>
        </div> 
      </div>
    </div> 
   </div>
  </div>

   <div class="col-xs-12">
      <div class="box box-success box-widget">
        <div class="box-header" >
          <h6 class="box-title" style="font-size: 15px;">STORE REQUISITION</h6>
        </div>
        <div class="box-body">
    <div class="row">
      <div class="col-sm-2">
          <div class="form-group">
            <label>Date From</label>
            <div class="input-group date">
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
              <input type="text"  data-validation="date" data-validation-format="mm/dd/yyyy" data-validation-error-msg="Tanggal Lahir Belum Dipilih"  data-validation-require-leading-zero="false" class="form-control pull-right" name= "TglSR" id="TglSR">
            </div>
          </div>
        </div>
        <div class="col-sm-2">
          <div class="form-group">
            <label>Date To</label>
            <div class="input-group date">
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
              <input type="text"  data-validation="date" data-validation-format="mm/dd/yyyy" data-validation-error-msg="Tanggal Lahir Belum Dipilih"  data-validation-require-leading-zero="false" class="form-control pull-right" name= "TglSR1" id="TglSR1">
            </div>
          </div>
        </div>

      <div class="col-md-3">
                   <div class="form-group">
                      <label>Departement</label> 
                    <select id="IdDepartementsr" class="form-control select2"  style="width: 100%;" name="IdDepartementsr">  
                    <option value="-" selected>--Pilih Departememnt--</option> 
                     <option value="0" >--Store--</option>            
                     <?php
                          foreach($departement as $udep){
                      ?>
                        <option value="<?php echo $udep->iddepartement; ?>" ><?php echo $udep->departement; ?></option>
                        <?php } ?>
                    </select>
                    <input type="hidden" name="NamaDepartemen" id="NamaDepartemen" value="Store">
                  </div>
                </div>

            <div class="col-md-3">
                   <div class="form-group" id="groupSubcategorysr">
                      <label>Subcategory</label> 
                    <select id="Subcategorysr" class="form-control select2"  style="width: 100%;" name="Subcategorysr">  
                        <option value="1" >--Pilih Subcategory--</option>
                       
                    </select>
                  </div>
                </div>
     
        <div class="col-sm-2">
          <div class="form-group">
             <a style="margin-top: 24px; " class="btn btn-default SRbytgl"><i class='glyphicon glyphicon-print'></i> Cetak</a>
          </div>
        </div> 
      </div>
    </div> 
   </div>
  </div>









     <div class="col-xs-12">
      <div class="box box-success box-widget">
        <div class="box-header" >
          <h6 class="box-title" style="font-size: 15px;">STORE REQUISITION PER ITEM</h6>
        </div>
        <div class="box-body">
    <div class="row">
      <div class="col-sm-2">
          <div class="form-group">
            <label>Date From</label>
            <div class="input-group date">
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
              <input type="text"  data-validation="date" data-validation-format="mm/dd/yyyy" data-validation-error-msg="Tanggal Lahir Belum Dipilih"  data-validation-require-leading-zero="false" class="form-control pull-right" name= "TglSRitem" id="TglSRitem">
            </div>
          </div>
        </div>
        <div class="col-sm-2">
          <div class="form-group">
            <label>Date To</label>
            <div class="input-group date">
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
              <input type="text"  data-validation="date" data-validation-format="mm/dd/yyyy" data-validation-error-msg="Tanggal Lahir Belum Dipilih"  data-validation-require-leading-zero="false" class="form-control pull-right" name= "TglSRitem1" id="TglSRitem1">
            </div>
          </div>
        </div>

      <div class="col-md-3">
                   <div class="form-group">
                      <label>Departement</label> 
                    <select id="IdDepartementsrItem" class="form-control select2"  style="width: 100%;" name="IdDepartementsr">  
                    <option value="-" selected>--Pilih Departememnt--</option> 
                     <option value="0" >--Store--</option>            
                     <?php
                          foreach($departement as $udep){
                      ?>
                        <option value="<?php echo $udep->iddepartement; ?>" ><?php echo $udep->departement; ?></option>
                        <?php } ?>
                    </select>
                    <input type="hidden" name="NamaDepartemen" id="NamaDepartemen" value="Store">
                  </div>
                </div>

            <div class="col-md-3">
                   <div class="form-group" id="groupSubcategorysrItem">
                      <label>Subcategory</label> 
                    <select id="SubcategorysrItem" class="form-control select2"  style="width: 100%;" name="Subcategorysr1">  
                        <option value="1" >--Pilih Subcategory--</option>
                       
                    </select>
                  </div>
                </div>
                <div class="col-md-3">
                   <div class="form-group" id="groupsrItem">
                      <label>Item</label> 
                    <select id="SrItem" class="form-control select2"  style="width: 100%;" name="SrItem">  
                        <option value="1" >--Pilih Item--</option>
                       
                    </select>
                  </div>
                </div>
     
        <div class="col-sm-2">
          <div class="form-group">
             <a style="margin-top: 24px; " class="btn btn-default SRbytglItem"><i class='glyphicon glyphicon-print'></i> Cetak</a>
          </div>
        </div> 
      </div>
    </div> 
   </div>
  </div>

  <div class="col-xs-12">
      <div class="box box-success box-widget">
        <div class="box-header" >
          <h6 class="box-title" style="font-size: 15px;">PEMBELIAN</h6>
        </div>
        <div class="box-body">
    <div class="row">
           <div class="col-md-3">
                   <div class="form-group">
                      <label>Category</label> 
                    <select id="idcategoryPembelian" class="form-control select2"  style="width: 100%;" name="idcategoryPembelian">  
                    <option value="-" selected>--Pilih Category--</option> 
                     <?php
                          foreach($category as $cat){
                      ?>
                        <option value="<?php echo $cat->idcategory; ?>" ><?php echo $cat->category; ?></option>
                        <?php } ?>
                    </select>
                  </div>
                </div>
                   <div class="col-md-3">
                   <div class="form-group">
                      <label>Departement</label> 
                    <select id="IdDepartementpem" class="form-control select2"  style="width: 100%;">  
                    <option value="-" selected>--Pilih Departememnt--</option> 
                     <option value="0" >--Store--</option>            
                     <?php
                          foreach($departement as $udep){
                      ?>
                        <option value="<?php echo $udep->iddepartement; ?>" ><?php echo $udep->departement; ?></option>
                        <?php } ?>
                    </select>
                  </div>
                </div>

                      <div class="col-sm-3">
          <div class="form-group">
            <label>Date From</label>
            <div class="input-group date">
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
              <input type="text"  data-validation="date" class="form-control pull-right"  name= "TglPem" id="TglPem">
            </div>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="form-group">
            <label>Date To</label>
            <div class="input-group date">
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
              <input type="text"  data-validation="date"  class="form-control pull-right" name= "TglPem1" id="TglPem1">
            </div>
          </div>
        </div>
     
        <div class="col-sm-2">
          <div class="form-group">
             <a style="margin-top: 24px; " class="btn btn-default Pem"><i class='glyphicon glyphicon-print'></i> Cetak</a>
          </div>
        </div> 
      </div>
    </div> 
   </div>
  </div>


      <div class="col-xs-12">
      <div class="box box-success box-widget">
        <div class="box-header" >
          <h6 class="box-title" style="font-size: 15px;">STOCK CARD</h6>
        </div>
        <div class="box-body">
        <div class="row">
          <div class="col-sm-3">
             <div class="form-group">
                    <label>Date From</label>
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text"   value="<?= date('m/d/Y') ?>"     class="form-control pull-right" name= "Tgl1BS" id="Tgl1BS">
                      </div>
                  </div>
            </div>
      
          <div class="col-sm-3">
             <div class="form-group">
                    <label>Date To</label>
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text"   value="<?= date('m/d/Y') ?>"     class="form-control pull-right" name= "Tgl2BS" id="Tgl2BS">
                      </div>
                  </div>
            </div>
        </div> 
         <div class="row">
            <div class="col-sm-6">
             <div class="form-group">
                    <label>Item</label>
                      <div class="form-group">
                         <input type="text" name="CariItemBS" id="CariItemBS"  class="form-control input-2">
                          <input type="hidden" name="NOItemBS" id="NOItemBS"  class="form-control input-2">
                      </div>
                  </div>
            </div>
        
        <div class="col-sm-2">
          <div class="form-group">
             <a style="margin-top: 24px; " class="btn btn-default CS"><i class='glyphicon glyphicon-print'></i> Cetak</a>
          </div>
        </div>
      </div>
      </div>
    </div> 
   </div>
  </div>


    



        
</section><!-- /.content -->
      <div class="modal modal-wide fade" id="ModalRo" role="dialog">
          <div class="modal-dialog modal-sm">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 align="center" class="modal-title">Receive Order</h4>
                  </div>
                  <div class="modal-body">
                      
                  </div>
                  
              </div>
          </div>
      </div>
            <div class="modal modal-wide fade" id="ModalCS" role="dialog">
          <div class="modal-dialog modal-sm">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 align="center" class="modal-title">Receive Order</h4>
                  </div>
                  <div class="modal-body">
                      
                  </div>
                  
              </div>
          </div>
      </div>

     



<script type="text/javascript">
  $(function () {  
  $(".MI").click(function(){
      var NoRO = $( "#NoRo" ).val();
      window.open("<?php echo base_url(). 'index.php/Purchase_order/CetakMI/';?>?NoRo="+NoRO ,"MyTargetWindowName")
  }); 
});
</script>
<script type="text/javascript">
  $(function () {  
  $(".CetakTglMI").click(function(){
      var TglAwal = $( "#TglAwalMI" ).val();
      var TglAkhir = $( "#TglAkhirMI" ).val();
      window.open("<?php echo base_url(). 'index.php/Purchase_order/CetakMI_byTgl/';?>?Status="+ 'Semua' +"&TglAwal="+ TglAwal+"&TglAkhir="+TglAkhir ,"MyTargetWindowName")
  }); 
});
</script>

<script type="text/javascript">
  $(function () {  
  $(".PaymentV").click(function(){
      var NoPay = $( "#NoPay" ).val();
      window.open("<?php echo base_url(). 'index.php/Purchase_order/CetakPV/';?>?NoPay="+NoPay ,"MyTargetWindowName")
  }); 
});
</script>

<script type="text/javascript">
  $(function () {  
  $(".Pem").click(function(){
      var idcat = $( "#idcategoryPembelian" ).val();
      var cat =$("#idcategoryPembelian option:selected").text(); 
      var Tglpem = $('#TglPem').val();
       var Tglpem1 = $('#TglPem1').val();
       var IdDepartementpem = $('#IdDepartementpem').val();
       var Departementpem = $("#IdDepartementpem option:selected").text(); 
       
      window.open("<?php echo base_url(). 'index.php/Purchase_order/Cetak_pembelian_cat/';?>?Tglpem="+ Tglpem +"&idcat="+ idcat +"&Tglpem1="+ Tglpem1+"&cat="+ cat+"&iddep="+ IdDepartementpem+"&dep="+ Departementpem,"MyTargetWindowName")
  }); 
});
</script>

<script type="text/javascript">
  $(function () {  
  $(".SR").click(function(){
      var kodesr = $( "#NoSR" ).val();
      window.open("<?php echo base_url(). 'index.php/Store_requisition/CetakSR/';?>?KodeSR="+kodesr ,"MyTargetWindowName")
  }); 
});
</script>

<script type="text/javascript">
  $(function () {  
  $(".PO").click(function(){
      var kodepo = $( "#NoPO" ).val();
      window.open("<?php echo base_url(). 'index.php/Purchase_order/CetakPO/';?>?NoPO="+kodepo ,"MyTargetWindowName")
  }); 
});
</script>

<script type="text/javascript">
  $(function () {   
    $(".SO").click(function(){
      var iddep = $('#IdDepartement').val();
      var NamaDep =$("#IdDepartement option:selected").text(); 
      var idsub = $('#Subcategory').val();
       var NamaSub =$("#Subcategory option:selected").text(); 
       var TglSO = $('#TglSO').val();
      window.open("<?php echo base_url(). 'index.php/Stok_opname/CetakSO/';?>?dep="+ NamaDep +"&sub="+ NamaSub+"&iddep="+ iddep+"&idsub="+ idsub+"&Tgl="+ TglSO,"MyTargetWindowName")
  });
});
</script>
<script type="text/javascript">
  $(function () {   
    $(".SRbytgl").click(function(){
      var iddep = $('#IdDepartementsr').val();
      var NamaDep =$("#IdDepartementsr option:selected").text(); 
      var idsub = $('#Subcategorysr').val();
     // alert(idsub);
       var NamaSub =$("#Subcategorysr option:selected").text(); 
       var TglSO = $('#TglSR').val();
       var TglSO1 = $('#TglSR1').val();
      window.open("<?php echo base_url(). 'index.php/Store_requisition/CetakSR_tgl/';?>?Tgl1="+ TglSO1 +"&dep="+ NamaDep +"&sub="+ NamaSub+"&iddep="+ iddep+"&idsub="+ idsub+"&Tgl="+ TglSO,"MyTargetWindowName")
  });
});
</script>

<script type="text/javascript">
  $(function () {   
    $(".SRbytglItem").click(function(){
      var iddep = $('#IdDepartementsrItem').val();
      var iditem =$('#SrItem').val();
      var idsub = $('#SubcategorysrItem').val();
       var NamaDep =$("#IdDepartementsrItem option:selected").text(); 
     // alert(idsub);
       var TglSO = $('#TglSRitem').val();
       var TglSO1 = $('#TglSRitem1').val();
      window.open("<?php echo base_url(). 'index.php/Store_requisition/CetakSR_tgl_item/';?>?Tgl1="+ TglSO1 +"&iditem="+ iditem +"&iddep="+ iddep+"&idsub="+ idsub+"&Tgl="+ TglSO+"&dep="+ NamaDep,"MyTargetWindowName")
  });
});
</script>


<script type="text/javascript">
  $(function () {  
  $(".BS").click(function(){
      var nobs = $( "#NoBS" ).val();
      window.open("<?php echo base_url(). 'index.php/Bad_stok/CetakBS/';?>?nobs="+nobs ,"MyTargetWindowName")
  }); 
});
</script>

<script type="text/javascript">
  $(function () {  
  $(".CS").click(function(){
      var TglAwal = $( "#Tgl1BS" ).val();
      var TglAkhir = $( "#Tgl2BS" ).val();
      var kode = $( "#NOItemBS" ).val();
       window.open("<?php echo base_url(). 'index.php/Stok_card/CetakSC/';?>?codeitem="+ kode+"&tgl1="+ TglAwal+"&tgl2="+TglAkhir ,"MyTargetWindowName")
          // window.open("<?php echo base_url(). 'index.php/Bad_stok/CetakSC/';?>?tgl1="+ TglAwal+"&tgl2="+TglAkhir ,"MyTargetWindowName")

  }); 
});
</script>








<?php
$this->load->view('template/Js');
$this->load->view('template/Foot');
?>
<script type="text/javascript">
   $(document).on('change', '#IdDepartement', function(){
    var NamaVen =$("#IdDepartement option:selected").text(); 
    var id = $("#IdDepartement").val();
    $("#NamaDepartemen").val('');  
      $("#NamaDepartemen").val(NamaVen);

       $.ajax({
            type: "POST",
            dataType:'html',
           url: '<?php echo site_url("Stok_opname/Select_subcategory_bydepartememnt");?>',
            data: {iddepartement: id},
            success: function(data){
              // $('#Subcategory').remove();
                 $('#groupSubcategory').html(data);  
                 $(".select2").select2();
            }
          });

    });
</script>

<script type="text/javascript">
   $(document).on('change', '#SubcategorysrItem', function(){
    var IdSub = $("#SubcategorysrItem").val();
     var IdDepartement = $("#IdDepartementsrItem").val();
    // alert(IdDepartement);
       $.ajax({
            type: "POST",
            dataType:'html',
           url: '<?php echo site_url("Store_requisition/load_item_bysubDep");?>',
            data: {iddepartement: IdDepartement, kodesubcategory:IdSub },
            success: function(data){
                 $('#groupsrItem').html(data);  
                 $(".select2").select2();
            }
          });

    });
</script>

<script type="text/javascript">
   $(document).on('change', '#IdDepartementsr', function(){
    var NamaVen =$("#IdDepartementsr option:selected").text(); 
    var id = $("#IdDepartementsr").val();
    $("#NamaDepartemen").val('');  
      $("#NamaDepartemen").val(NamaVen);

       $.ajax({
            type: "POST",
            dataType:'html',
           url: '<?php echo site_url("Stok_opname/Select_subcategory_bydepartememntsr");?>',
            data: {iddepartement: id},
            success: function(data){
              // $('#Subcategory').remove();
                 $('#groupSubcategorysr').html(data);  
                 $(".select2").select2();
            }
          });

    });
</script>


<script type="text/javascript">
   $(document).on('change', '#IdDepartementsrItem', function(){
    var NamaVen =$("#IdDepartementsrItem option:selected").text(); 
    var id = $("#IdDepartementsrItem").val();
    $("#NamaDepartemen").val('');  
      $("#NamaDepartemen").val(NamaVen);

       $.ajax({
            type: "POST",
            dataType:'html',
           url: '<?php echo site_url("Stok_opname/Select_subcategory_bydepartememntsrItem");?>',
            data: {iddepartement: id},
            success: function(data){
              // $('#Subcategory').remove();
                 $('#groupSubcategorysrItem').html(data);  
                 $(".select2").select2();
            }
          });

    });
</script>



<script type="text/javascript">
        $("#CariNoPO").click(function(e){

      $.ajax({
        type: "POST",
        url: '<?php echo site_url('Purchase_order/purchase_order_list');?>',
        data: {id: '1'},
        success: function(data){

             e.preventDefault();
             var mymodal = $('#ModalPO');
             var height = $(window).height() - 200;
            mymodal.find(".modal-body").css("max-height", height);
             mymodal.find('.modal-body').html(data);
            mymodal.modal('show');           
        }
      });
  });
</script> 
   
<script type="text/javascript">
        $("#CariRo").click(function(e){

      $.ajax({
        type: "POST",
        url: '<?php echo site_url('Purchase_order/Select_ro');?>',
        data: {id: '1'},
        success: function(data){

             e.preventDefault();
             var mymodal = $('#ModalRo');
             var height = $(window).height() - 200;
            mymodal.find(".modal-body").css("max-height", height);
             mymodal.find('.modal-body').html(data);
            mymodal.modal('show');           
        }
      });
  });
</script> 

<script type="text/javascript">
        $("#CariNoPay").click(function(e){

      $.ajax({
        type: "POST",
        url: '<?php echo site_url('Purchase_order/Select_payment');?>',
        data: {id: '1'},
        success: function(data){

             e.preventDefault();
             var mymodal = $('#ModalVoucher');
             var height = $(window).height() - 200;
            mymodal.find(".modal-body").css("max-height", height);
             mymodal.find('.modal-body').html(data);
            mymodal.modal('show');           
        }
      });
  });
</script> 

<script type="text/javascript">
        $("#CariNoPR").click(function(e){

      $.ajax({
        type: "POST",
        url: '<?php echo site_url('Purchase_requisition/daftar_pr');?>',
        data: {id: '1'},
        success: function(data){

             e.preventDefault();
             var mymodal = $('#ModalPR');
             var height = $(window).height() - 200;
            mymodal.find(".modal-body").css("max-height", height);
             mymodal.find('.modal-body').html(data);
            mymodal.modal('show');           
        }
      });
  });
</script> 

<script type="text/javascript">
        $("#CariNoSR").click(function(e){

      $.ajax({
        type: "POST",
        url: '<?php echo site_url('Store_requisition/daftar_sr');?>',
        data: {id: '1'},
        success: function(data){

             e.preventDefault();
             var mymodal = $('#ModalSR');
             var height = $(window).height() - 200;
            mymodal.find(".modal-body").css("max-height", height);
             mymodal.find('.modal-body').html(data);
            mymodal.modal('show');           
        }
      });
  });
</script>

<script type="text/javascript">
        $("#CariNoSO").click(function(e){

      $.ajax({
        type: "POST",
        url: '<?php echo site_url('Stok_opname/daftar_so');?>',
        data: {id: '1'},
        success: function(data){

             e.preventDefault();
             var mymodal = $('#ModalSO');
             var height = $(window).height() - 200;
            mymodal.find(".modal-body").css("max-height", height);
             mymodal.find('.modal-body').html(data);
            mymodal.modal('show');           
        }
      });
  });
</script> 


<script type="text/javascript">
    $("#CariNoBS").click(function(e){
      $.ajax({
        type: "POST",
        url: '<?php echo site_url('Bad_stok/daftar_bs');?>',
        data: {id: '1'},
        success: function(data){

             e.preventDefault();
             var mymodal = $('#ModalBS');
             var height = $(window).height() - 200;
            mymodal.find(".modal-body").css("max-height", height);
             mymodal.find('.modal-body').html(data);
            mymodal.modal('show');           
        }
      });
  });
</script> 


<script type="text/javascript">
    $("#CariItemBS").click(function(e){
      $.ajax({
        type: "POST",
        url: '<?php echo site_url('Stok_card/daftar_item');?>',
        data: {id: '1'},
        success: function(data){

             e.preventDefault();
             var mymodal = $('#ModalCS');
             var height = $(window).height() - 200;
            mymodal.find(".modal-body").css("max-height", height);
             mymodal.find('.modal-body').html(data);
            mymodal.modal('show');           
        }
      });
  });
</script> 




  <script>
  $( function() {
    $( "#TglAkhirMI" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });
    $( "#TglAwalMI" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });
    $( "#TglAwalPer" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });
    $( "#TglAkhirPer" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });
     $( "#PerTanggal" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });
     $( "#TglSO" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });
      $( "#TglSR" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });
      $( "#TglSR1" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });
      $( "#TglSRitem" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });
      $( "#TglSRitem1" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });
       $( "#TglPem" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });
       $( "#TglPem1" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });
    $(".select2").select2();


  });

</script>
      <script>
  $( function() {
    $( "#Tgl1BS" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });
    $( "#Tgl2BS" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });


  });
  </script>


<script type="text/javascript">
  $(function () {  
  $(".SemuaNasabah").click(function(){
      var TglAwal = $( "#TglAwalSemua" ).val();
      var TglAkhir = $( "#TglAkhirSemua" ).val();
      window.open("<?php echo base_url(). 'index.php/Tabungan/CetakSemuaNasabah/';?>?Status="+ 'Semua' +"&TglAwal="+ TglAwal+"&TglAkhir="+TglAkhir ,"MyTargetWindowName")
  }); 
});
</script>

<script type="text/javascript">
  $(function () {  
  $(".PerNasabah").click(function(){
      var TglAwal = $( "#TglAwalPer" ).val();
      var TglAkhir = $( "#TglAkhirPer" ).val();
       var Norek = $( "#Norek" ).val();
      window.open("<?php echo base_url(). 'index.php/Tabungan/CetakPerNasabah/';?>?Norek="+ Norek +"&TglAwal="+ TglAwal+"&TglAkhir="+TglAkhir ,"MyTargetWindowName")
  }); 
});
</script>
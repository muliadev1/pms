<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>

<section class="content-header">
    <div class="btn-group btn-breadcrumb">
      <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
      <a  class="btn btn-default  btn-xs active">Expectd Arrival</a>
    </div>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-xs-12">
      <div class="box box-primary">
       <div class="box-header">
        <h3 class="box-title">Expected Arrival</h3>
        <div class="form-group">
        <br>

           <div class="row" style="background-color:#F2F2FF; margin-left:5px; margin-right:5px; margin-bottom:10px;">
            <div class="row" class="row" >
              <div class="col-md-3">
                <label style="padding-left: 15px;">From</label>
              </div>
              <div class="col-md-3">
                <label style="padding-left: 15px;">Until</label>
              </div>
            </div>
            <div >
              <div class="col-md-3">
                <div class="form-group">
                  <div class="input-group date">
                    <div class="input-group-addon" style="background: #DDD;">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" value="<?= date('m/d/Y') ?>" class="form-control pull-right" name= "tglAwal" id="tglAwal">
                  </div>
                  <input type="hidden" value="<?php echo $tglAwal ?>" id="Tawal" name="Tawal">
                </div>
              </div>
             <div class="col-md-3">
                <div class="form-group">
                  <div class="input-group date">
                    <div class="input-group-addon" style="background: #DDD;">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" value="<?= date('m/d/Y') ?>" class="form-control pull-right" name= "tglAkhir" id="tglAkhir">
                  </div>
                  <input type="hidden" value="<?php echo $tglAkhir ?>" id="Takhir" name="Takhir">
                </div>
              </div>
              <div class="col-sm-1">
               <div class="form-group">
                 <a  class="btn btn-info Proses"><i class="fa fa-check-square"></i> Process</a>
               </div>
              </div>
               <div class="col-sm-2">
                <div class="form-group">
                 <a class="btn btn-default EA"><i class='glyphicon glyphicon-print'></i> Print</a>
                </div>
              </div>
         </div>
       </div>
      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body" >
        <div class="table-responsive" id="tabelkosong">
          <table id="example11" class="table table-bordered table-striped">
                                <thead>
                                    <tr >
                                        <th >No</th>
                                        <th >R. Status</th>
                                        <th >#Room</th>
                                        <th >R. Type</th>
                                        <th >Guest Name</th>
                                        <th >Arrival</th>
                                        <th >Departure</th>
                                        <th >Night</th>
                                        <th  style="display: none;">Nationality</th>
                                        <th >Segment</th>
                                        <th >Company</th>
                                        <th >Res. Notes</th>
                                        <th style="display: none;">Res. Message</th>  
                                    </tr>
                                </thead>
                                <tbody> 
                                   
                                </tbody>
                            </table>
          </div>
           <div id="tabelada" hidden="hidden">
           </div>



        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->

</section><!-- /.content -->


<?php
$this->load->view('template/Js');
$this->load->view('template/Foot');
?>


<script type="text/javascript">
    $(".Proses").click(function(e){
      var tglAwal = $( "#tglAwal" ).datepicker().val();
      var tglAkhir = $( "#tglAkhir" ).datepicker().val();
     //alert(tglAkhir);
        $.LoadingOverlay("show");
        
        $.ajax({
        type: "POST",
        dataType: 'html',
        url: '<?php echo site_url('Reservation/ExpectedArrival_view_data');?>',
        data: {tglAwal: tglAwal,tglAkhir:tglAkhir},
        success: function(data){
            e.preventDefault();
            $('#tabelkosong').fadeOut(30);
             $('#tabelada').html(data);
             $('#tabelada').fadeIn(0,300);  
              $("#example2").DataTable();
            $.LoadingOverlay("hide");
        }
      });
    });
</script>

<script type="text/javascript">
  $(function () {  
  $(".EA").click(function(){
      var TglAwal = $( "#tglAwal" ).val();
      var TglAkhir = $( "#tglAkhir" ).val();
      window.open("<?php echo base_url(). 'index.php/Reservation/CetakLapEA/';?>?Status="+ 'Semua' +"&TglAwal="+ TglAwal+"&TglAkhir="+TglAkhir ,"MyTargetWindowName")
  }); 
});
</script>

<script>
  $(function () {
    $("#example11").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>

<script>
$( function() {
$( "#tglAwal" ).datepicker({
  autoclose: true,
  dateFormat: 'yy/mm/dd'
  });
$( "#tglAkhir" ).datepicker({
  autoclose: true,
  dateFormat: 'yy/mm/dd'
  });
});
</script>

<script type="text/javascript">
  $(function () {
  $(".EA").click(function(){
      var TglAwal = $( "#Tawal" ).datepicker().val();
      var TglAkhir = $( "#Takhir" ).datepicker().val();
      $.ajax({
        type: "POST",
        url: '<?php echo site_url("Reservation/CetakLapEA"); ?>',
        dataType: 'html',
        data: {Tawal: TglAwal, Takhir : TglAkhir},
        success: function(respon){
        },
      });
  });
});
</script>

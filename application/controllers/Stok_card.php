<?php 

class Stok_card extends CI_Controller{
	private $IdOperator;

	function __construct(){
		parent::__construct();
		if($this->session->userdata('Kode_userPms')==NULL) {
           redirect('Login');
		}
		$this->load->library(array('session','create_pdf','ciqrcode'));
		$this->load->model(array('M_stok_card','M_rbac'));
		$this->IdOperator = $this->session->userdata('Kode_userPms');
		
		if (!empty($this->session->userdata("active_db"))) {
			$this->load->database($this->session->userdata("active_db"), FALSE, TRUE);
		}
	}


	function index(){
		$this->load->view('dashboard1');
	}

	function daftar_item(){
	// $dari = $this->input->post('dari');
	// if ($dari=='PO') {
	// 	$data['dari'] =  array('dari' => 'PO');
	// }elseif ($dari=='POD') {
	// 	$data['dari'] =  array('dari' => 'POD');
	// }
	$data['data'] = $this->M_stok_card->item_view();
	$this->load->view('stok_card/Cetak/Daftar_item',$data);
	}


	function Cetak($JenisLaporan,$Text){
		$data['hotel'] = $this->M_rbac->getPerusahaan();
		$data['qr'] = $this->ciqrcode->generateQRcode($JenisLaporan, $Text,1.9);
		$data1= $this->load->view('template/Cetak_head',$data, TRUE);
		return $data1;
	}

	function CetakSC(){
		$tgl1 =  $this->input->get('tgl1', TRUE);
		$tgl11 = DateTime::createFromFormat('m/d/Y',$tgl1);
		$tglfinal1= $tgl11->format("Y-m-d");
		$tglpakai1= $tgl11->format("d-m-Y");
		$tgl2 =  $this->input->get('tgl2', TRUE);
		$tgl22 = DateTime::createFromFormat('m/d/Y',$tgl2);
		$tglfinal2 = $tgl22->format("Y-m-d");
		$tglpakai2 = $tgl22->format("d-m-Y");
		$codeitem =  $this->input->get('codeitem', TRUE);

		$data['tgl'] = array('tgl1' => $tglpakai1 ,'tgl2' => $tglpakai2 );
		$data['kontenSC'] = $this->M_stok_card->Load_SC_bytgl($tglfinal1,$tglfinal2,$codeitem);
	    $data['stokawal'] = $this->M_stok_card->get_stok_awal_byitem($tglfinal1,$tglfinal2,$codeitem);
	   // print_r($tglfinal1.'  '.$tglfinal2);exit();
	    
	    $data['header'] = $this->Cetak("Stock_Card",  $this->session->userdata('Nama_userPms').'-'.$data['kontenBs'][0]->kodebs.'-'. date("d-m-Y_H-i-s"), 1);
	    
	    $html=$this->load->view('stok_card/Cetak/SC',$data, TRUE);
     	$this->create_pdf->load($html,'stok_card'.'-'.$tglpakai1.'-'.$tglpakai2, 'A4-L');	
	}
	


}
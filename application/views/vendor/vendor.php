<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>

<section class="content-header">
    <div class="btn-group btn-breadcrumb">
            <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
            <a  class="btn btn-default  btn-xs active">Vendor</a>
        </div>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Vendor</h3>
          <a style="float:right"  href="<?php echo base_url('index.php/Vendor/Vendor_add');?>" class="btn btn-primary">
            <i class="glyphicon glyphicon-saved"></i>
            Add Vendor
          </a>
        </div>
    <!-- /.box-header -->
        <div class="box-body">
        <div class="table-responsive">
          <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr >
                                        <th>ID</th>
                                        <th>Vendor</th>
                                        <th>Address</th>
                                        <th>Phone</th>
                                        <th>Email</th>
                                        <th>Contact Person</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach($data as $u){

                                    ?>
                                    <tr  class='odd gradeX context'>
                                        <td><?php echo $u->idvendor ?></td>
                                        <td><?php echo $u->vendor?></td>
                                        <td><?php echo $u->address?></td>
                                        <td><?php echo $u->phone?></td>
                                        <td><?php echo $u->email?></td>
                                        <td><?php echo $u->contact?></td>
                                         <td align="center">
                                         <a class="btn btn-warning btn-xs" title="Edit Data"  href="<?php echo base_url('index.php/Vendor/Vendor_edit/'.$u->idvendor); ?>">  <span class="fa fa-fw fa-edit" ></span> </a>
                                        <input type="hidden" id="btnhapus" value="<?php echo $u->vendor ?>" >


                                        </td>






                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
          </div>



        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->

</section><!-- /.content -->


<?php
$this->load->view('template/Js');
$this->load->view('template/Foot');
?>

<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>

<script>
function doconfirm()
{
    var nama = $('#btnhapus').val();
    job=confirm("Apakah Anda Yakin Menonaktifkan Bidang Studi "  + nama + " ?" );
    if(job!=true)
    {
        return false;
    }
}
</script>

<script>
function doconfirmaktif()
{
    var nama = $('#btnaktif').val();
    job=confirm("Apakah Anda Yakin Mengaktifkan Nasabah "  + nama + " ?" );
    if(job!=true)
    {
        return false;
    }
}
</script>

<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>
<script src="<?php echo base_url('assets/js/accounting.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/cleave.min.js') ?>" type="text/javascript"></script>

<style type="text/css">
.modal.modal-wide .modal-dialog {
  width: 80%;
}
.modal-wide .modal-body {
  overflow-y: auto;
}

td.No { display: none
 }


#tallModal .modal-body p { margin-bottom: 900px }
.pull-right {
    float: right;
}

.pull-left {
    float: left;
}

select {
    width: 300px;
}

</style>
    <script type="text/javascript">
      jQuery(document).ready(function($) {
        new Cleave('.input-1', {
           numeral: true,
           numeralDecimalMark: ',',
           delimiter: '.'
      });
        new Cleave('.input-2', {
           numeral: true,
           numeralDecimalMark: ',',
           delimiter: '.'
      });
         new Cleave('.input-3', {
           numeral: true,
           numeralDecimalMark: ',',
           delimiter: '.'
      });
          new Cleave('.input-4', {
           numeral: true,
           numeralDecimalMark: ',',
           delimiter: '.'
      });

          });
    </script>
    <script type="text/javascript">
      function doMath()
      {
          var qty = document.getElementById('Qty1').value;
          var qty2 = qty.replace(/\./g, "");
          document.getElementById('Qty').value =  qty2.replace(/\,/g, ".");

          var price = document.getElementById('Price1').value;
          var price2 = price.replace(/\./g, "");

          document.getElementById('Price').value =  price2.replace(/\,/g, ".");
      }
    </script>
        <script type="text/javascript">
      function doMathdisc()
      {
          var disc = document.getElementById('Disc1').value;
          document.getElementById('Disc').value =  disc.replace(/\./g, "");
          total = document.getElementById('Total').value;
         
          var discHitung = disc.replace(/\./g, "");
           var TotalSetDisc = parseFloat(total) - parseFloat(discHitung);
           document.getElementById('TotalSetDiskon').value = TotalSetDisc;
           document.getElementById('TotalSetDiskon1').value =  TotalSetDisc.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");


      }
    </script>

    <script type="text/javascript">
     function doMathdp()
      {

          var dp = document.getElementById('DP1').value;
          document.getElementById('DP').value =  dp.replace(/\./g, "");
          var disc = document.getElementById('Disc').value;

          dphitung = dp.replace(/\./g, "");
          total = document.getElementById('Total').value;
          var remain = parseFloat(total) -parseFloat(dphitung) - parseFloat(disc);
          document.getElementById('Remain').value =  remain;
          document.getElementById('Remain1').value =  remain.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
      }
    </script>



<!-- Content Header (Page header) -->
<section class="content-header">
   <div class="btn-group btn-breadcrumb">
            <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
             <a href="<?php echo base_url('index.php/Receive_item/Purchase_order_list');?>" class="btn btn-default  btn-xs">Receive Item</a>
            <a  class="btn btn-default  btn-xs active">Receive Item</a>
        </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
             <div class="box box-info">
              <form method="post" id="Simpan" action="<?php echo base_url(). 'index.php/Receive_item/add_Ro'; ?>" enctype="multipart/form-data">
                <div class="box-header">
          <div class="row" >
             <div class="col-md-3">
             </div>
             <div class="col-md-3">
                <div id="cssmenu"  >
                  <ul>
                     <li class="active"><a>Add Item</a></li>
                     <li id="AddPayment" style="cursor:pointer;"> <a >Payment Method</a></li> 
                     <li id="DropTo" style="cursor:pointer;" > <a>Drop  </a></li>
                     <li id="Save" style="cursor:pointer;" > <a> Save</a></li>
                  </ul>
                </div>
              </div>
              <div class="col-md-3" style="float:right; margin-right:50px;" align="right">
             </div>
            </div>
                  <hr style="border-top: 1px solid #8c8b8b; padding: 1px; margin-top: 5px; margin-bottom: 0px;">
                </div>
                <div class="box-body">


            <div class="row" style="margin-bottom:20px;">
             <div class="col-md-3">
             </div>
             <div class="col-md-3" >
                <a style="margin-top:5px; float:right; "  id="BackMati" class="btn btn-primary  btn-xs btn-flat disabled"  ><i class='glyphicon glyphicon-arrow-left'></i>  Back  </a>
                <a style="margin-top:5px; float:right; display:none; "   id="BackToItem" class="btn btn-primary  btn-xs btn-flat "  ><i class='glyphicon glyphicon-arrow-left'></i>  Back  </a>
                <a style="margin-top:5px; float:right; display:none; "  id="BackToPay" class="btn btn-primary  btn-xs btn-flat "  ><i class='glyphicon glyphicon-arrow-left'></i>  Back  </a>
                <a style="margin-top:5px; float:right; display:none; "  id="BackToDrop" class="btn btn-primary  btn-xs btn-flat "  ><i class='glyphicon glyphicon-arrow-left'></i>  Back  </a>
              </div>
              
              <div class="col-md-3" >
                <a style="margin-top:5px; float:left;"  id="NextToPay" class="btn btn-primary  btn-xs btn-flat "  ><i class='glyphicon glyphicon-arrow-right'></i>  Next  </a>
                <a style="margin-top:5px; float:left; display:none;"  id="NextToDrop" class="btn btn-primary  btn-xs btn-flat "  ><i class='glyphicon glyphicon-arrow-right'></i>  Next  </a>
                <a style="margin-top:5px; float:left; display:none;"  id="SaveFinal" class="btn btn-primary  btn-xs btn-flat "  ><i class='fa  fa-file'></i>  Save  </a>

             </div>
            </div>
            


            <div id="ElementAddItem">
               <div class="row" style=" padding-top:5px; margin:0px ;background-color:#ebebeb">
                 <div class="col-md-6">
                   <div class="form-group">
                      <label class="control-label">Vendor</label>
                      <input type="hidden" id="kodepo" name="kodepo" value="<?php echo $data[0]->kodepo ?>"  class="form-control">
                      <input type="hidden" name="idvendor" value="<?php echo $data[0]->idvendor ?>"  class="form-control">
                      <input type="text" readonly="readonly" name="vendor" value="<?php echo $data[0]->vendor ?>"  class="form-control">

                  </div>
                </div>
                <div class="col-md-3">
                   <div class="form-group">
                         <label class="control-label">Receipt No.</label>
                         <input type="text" required="required" name="receiptno" value=""  class="form-control">
                    </div>
               </div>
                <div class="col-md-3">
                  <div class="form-group">
                  <label>Receipt Date</label>
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text"   value="<?= date('m/d/Y') ?>"     class="form-control pull-right" name= "ExpetedDate" id="ExpetedDate">
                    </div>
                 </div>
               </div>
              </div>
              <br>

              <div class="row">
              </div>

               <div class="row">
                  <div class="col-md-2">
                    <div id ="divItemCode" class="form-group">
                      <label>Item Code</label>
                      <select class="form-control select2" style="width: 100%;" id="selectItemCode"name="iditem">
                        <option value=""></option>
                        <?php
                            foreach($dataitem as $u){
                        ?>
                          <option value="<?php echo $u->codeitem; ?>" ><?php echo $u->description; ?></option>
                          <?php } ?>

                      </select>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div id ="divDescription" class="form-group">
                          <label class="control-label">Description</label>
                          <input type="text" name="Description" id="Description"  class="form-control">
                     </div>
                  </div>
                  <div  class="col-md-1">
                    <div id ="divQtyOrder" class="form-group">
                          <label class="control-label">Order</label>
                          <input type="text" readonly="readonly" name="Qty1" id="QtyOrder"  class="form-control input-1"  >
                          <input type="hidden" name="Qtyodr" id="Qtyodr"  >
                     </div>
                  </div>
                  <div  class="col-md-1">
                    <div id ="divQty" class="form-group">
                          <label class="control-label">Receive</label>
                          <input type="text" name="Qty1" id="Qty1"  onkeyup="doMath()" class="form-control "  >
                          <input type="hidden" name="Qty" id="Qty"  onkeyup="doMath()">
                     </div>
                  </div>
                  <div  class="col-md-2">
                    <div id ="divPrice" class="form-group">
                          <label class="control-label">Price</label>
                          <input type="text" name="Price" id="Price1"  class="form-control input-2" onkeyup="doMath()">
                           <input type="hidden" name="Id" id="Id"  class="form-control">
                           <input type="hidden" name="Price" id="Price"  class="form-control input-2" >
                           <input type="hidden" name="Unit" id="Unit"  class="form-control" >
                           <input type="hidden" name="index" id="index" value="0"  class="form-control">
                           <input type="hidden" name="Total" id="Total" value="0"  class="form-control">
                           <input type="hidden" name="Stok" id="QtyStok" value="0"  class="form-control">
                           <input type="hidden" name="IsCash" id="IsCash" value="0"  class="form-control">
                          

                     </div>
                  </div>
                  <div  class="col-md-3">
                    <div class="form-group" >
                        <a style="margin-top:25px; margin-right:10px; " class="btn btn-primary btn-flat Add"  ><i class='glyphicon glyphicon-arrow-down'></i>  Add  </a>
                        <!-- <a style="margin-top:25px; margin-right:10px; " class="btn btn-warning btn-flat disabled " id="Batal"  ><i class='glyphicon glyphicon-remove'></i>  Batal  </a> -->
                     </div>
                  </div>
                </div>
                     <div class="table-responsive">
                         <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                   <th style="display:none">No</th>
                                    <th>Item Code</th>
                                    <th>Description</th>
                                    <th>Qty Order</th>
                                    <th>Qty Receive</th>
                                    <th>Unit</th>
                                    <th>Price</th>
                                    <th>Subtotal</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                      </div>
                  </div>

        <div id="ElementAddPayment" class="box-widget" hidden="hidden">
            <label class="control-label">Discount</label>
            <div class="row">
                <div class="col-md-2">
                  <div class="form-group">
                    <input type="text" name="Disc1" id="Disc1"  class="form-control input-4" onkeyup="doMathdisc()">
                    <input type="hidden" name="Disc" id="Disc"  class="form-control" onkeyup="doMathdisc()">
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <input type="text" name="TotalSetDiskon1" id="TotalSetDiskon1"  class="form-control input-4" readonly="readonly" onkeyup="doMathdisc()">
                    <input type="hidden" name="TotalSetDiskon" id="TotalSetDiskon"  class="form-control" onkeyup="doMathdisc()">
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-2">
                  <label class="control-label">Method</label>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4">
                <div class="form-group">
                <label style="margin-right:50px;">
                  <input type="radio" name="r3" class="flat-red" name="Cash" id="Cash" checked>
                  Cash
                </label>
                <label>
                  <input type="radio" name="r3" class="flat-red" name="Credit" id="Credit">
                  Credit
                </label>
              </div>          
                </div>
              </div>
           
              <div id="ElementCredit" hidden="hidden">
                <div class="row">
                  <div class="col-md-4">
                  <div class="form-group" id="DivDP">
                    <label class="control-label">Down Payment</label>
                         <input type="text" name="DP1" id="DP1"  class="form-control input-3" onkeyup="doMathdp()">
                         <input type="hidden" name="DP" id="DP"  class="form-control" onkeyup="doMathdp()">
                    </div>
                  </div>
                  <div class="col-md-4">
                   <div class="form-group" id="DivRemain" >
                    <label class="control-label">Remain</label>
                         <input type="text" name="Remain1" id="Remain1"  class="form-control" readonly="readonly" onkeyup="doMathdp()">
                          <input type="hidden" name="Remain" id="Remain"  class="form-control" onkeyup="doMathdp()">
                   </div>
                  </div>
                </div>

                 <div class="row">
                  <div class="col-md-4">
                  <div class="form-group" id="DivDueDate">
                    <label class="control-label">Due Date</label>
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text"   value="<?= date('m/d/Y') ?>"     class="form-control pull-right" name= "DueDate" id="DueDate">
                      </div>
                  </div>
                  </div>
                </div>
              </div> 
            </div>

          <div id="ElementDropTo" class="box-widget" hidden="hidden">
            <div class="row">
                 <div class="col-md-6">
                   <div class="form-group">
                      <label>Depatement</label>
                    <select id="IdDepartement" class="form-control select2"  style="width: 100%;" name="IdDepartement">                 
                      <option value="0" >Store</option>               
                    <!--  <?php
                          foreach($dataDepaetemen as $u){
                      ?>
                        <option value="<?php echo $u->iddepartement; ?>" ><?php echo $u->departement; ?></option>
                        <?php } ?> -->
                    </select>
                    <input type="hidden" name="NamaDepartemen" id="NamaDepartemen" value="Store">
                  </div>
                </div>
              </div>
            </div>
             <a   type="submit" hidden="hidden"   id="simpan1"  >  </a>   

        </form>
      </div>
     </div>
</div>

    <div class="modal modal-wide fade" id="myModal" role="dialog" style="display:none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" title="close" data-dismiss="modal">&times;</button>
                    <h4 align="center" class="modal-title">Purchase Requisition List</h4>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>



</section>



<?php
$this->load->view('template/Foot');
$this->load->view('template/Js');
?>


<script type="text/javascript">
  $(function () {
  $("#NextToPay").click(function(){ 
  var Total =$('#Total').val();
  if (parseFloat(Total) >0) {
        event.preventDefault();
                   $('#ElementAddItem').fadeOut(300); 
                   $('#BackMati').fadeOut(300); 
                   $('#NextToPay').fadeOut(300); 
                   $('#BackToItem').fadeTo(0,300);
                   $('#NextToDrop').fadeTo(0,300);
                   $('#ElementAddPayment').fadeTo(0,3000);
                   $('#TotalSetDiskon1').val(Total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."))
                   $('#TotalSetDiskon').val(Total)
                   $('#DP1').val('0')
                   $('#DP').val('0')
                   $('#Disc1').val('0')
                   $('#Disc').val('0')
                   $('#Remain1').val(Total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."))
                   $('#Remain').val(Total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."))
                   $('#AddPayment').addClass('active'); 
        }else{
          File_Kosong();
        }; 
     });  
  });
</script>
<script type="text/javascript">
  $(function () {
  $("#BackToPay").click(function(){  
        event.preventDefault();        
                    $('#ElementAddItem').fadeOut(300); 
                   $('#BackMati').fadeOut(300); 
                   $('#NextToPay').fadeOut(300); 
                   $('#BackToItem').fadeTo(0,300);
                   $('#NextToDrop').fadeTo(0,300);
                   $('#ElementAddPayment').fadeTo(0,3000);
                   $('#BackToPay').fadeOut(0,3000);
                   $('#ElementDropTo').fadeOut(300);
                   $('#SaveFinal').fadeOut(0,3000);
                   $('#DropTo').removeClass('active'); 
     });  
  });
</script>
<script type="text/javascript">
  $(function () {
  $("#BackToItem").click(function(){  
        event.preventDefault();
                  $('#ElementAddItem').fadeIn(300); 
                   $('#BackMati').fadeIn(300); 
                   $('#NextToPay').fadeIn(300); 
                   $('#BackToItem').fadeOut(0,300);
                   $('#NextToDrop').fadeOut(0,300);
                   $('#ElementAddPayment').fadeOut(0,3000);
                   $('#AddPayment').removeClass('active'); 
 

     });  
  });
</script>


<script type="text/javascript">
  $(function () {
  $("#NextToDrop").click(function(){  
        event.preventDefault();
          $('#ElementAddPayment').fadeOut(300);
           $('#ElementDropTo').fadeTo(0,3000);
           $('#BackToPay').fadeTo(0,3000);
           $('#SaveFinal').fadeTo(0,3000);
           $('#BackToItem').fadeOut(0,300);
           $('#NextToDrop').fadeOut(0,300);
           $('#DropTo').addClass('active');  

     });  
  });
</script>

<script type="text/javascript">
  $(function () {
  $("#SaveFinal").click(function(){  
    $('#simpan1').trigger('click');  
     });  
  });
</script>

<script type="text/javascript">
$("#simpan1").click(function() {
        event.preventDefault();
        $.confirm({
          title: 'Confirmation',
          content: 'Are You Sure to Save?',
          type:'blue',
          buttons: {
              Simpan: function () {
                  $.LoadingOverlay("show");
                  $("#Simpan").submit();
              },
              Batal: function () {
                
                  $.alert('Data Tidak Disimpan...');
              },
          }
      }); 

});
</script>

<script type="text/javascript">
  $('#Credit').on('ifChanged', function(event){
    $('#ElementCredit').fadeTo(0,3000);
    $('#DivRemain').addClass('has-success');
    $('#DivDP').addClass('has-success');
    $('#DivDueDate').addClass('has-success');
    $('#IsCash').val('1');
    $( '#Cash' ).prop( "checked", false );
});
  $('#Cash').on('ifChanged', function(event){
    $('#ElementCredit').fadeOut(0,3000);
    $('#IsCash').val('0');
    $('#Credit').prop("checked", false)
});
</script>


<script type="text/javascript">
$('#selectItemCode').on("select2:select", function(e) {
// what you would like to happen
  var itemcode = $('#selectItemCode').val();
 // alert(itemcode);
  var kodepo = $('#kodepo').val();
  $.ajax({
    type: "POST",
    dataType:'json',
   url: '<?php echo site_url('Receive_item/select_item_by_itemcode_kodepo');?>',
    data: {itemcode: itemcode,kodepo:kodepo},
    success: function(data){
         $('#Description').val(data[0].description);
         var odr = data[0].qtyorder.toString().replace(/\./g, ",");
         var odr2=odr.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
         $('#QtyOrder').val(odr2);
         $('#Qtyodr').val(data[0].qtyorder);
         var pr = data[0].price.toString().replace(/\./g, ",");
         var pr2=pr.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
         $('#Price1').val(pr2);
         $('#Price').val(data[0].price);
         $('#Unit').val(data[0].unit);
          $('#QtyStok').val(data[0].qtystok);
         
    }
  });
});

</script>



<script type="text/javascript">

  $('.Add').on('click',function(e){
    var iditem = '';
    var itemcode = '';
    var Description ='';
    var Qty = '';
    var Unit =  '';
    var Price = '';
    var Subtotal = '';
    i = $('#index').val();
    var Total =$('#Total').val(); 
    //var i = 1;

     var itemcode = $('#selectItemCode').val();
     var Description = $('#Description').val();

     var QtyOrder = $('#QtyOrder').val();
     var Qty= $('#Qty1').val();
     var QtyTampil= $('#Qty1').val();

     var Stok = $('#QtyStok').val();
     var Unit = $('#Unit').val();
     var Price = $('#Price').val();
     var PriceTampil = $('#Price1').val();

     var SubTotal = parseFloat(Qty) * parseFloat(Price);
         SubTotal = SubTotal.toString().replace(/\./g, ",");
         // alert(SubTotal);
     var SubTotalTampil = SubTotal.replace(/\B(?=(\d{3})+(?!\d))/g, ".");


     var btndelete =  "<a class='btn btn-danger btn-xs Hapus' title='Remove Item'>  <span class=' fa fa-minus-circle' ></span> </a> "
     var t = $('#example1').DataTable();

     // alert(itemcode);
     // alert(Qty);
    //
     if (itemcode=='' ||itemcode==null  ) {
      File_Kosong()
     }else if (Qty=='' || parseFloat(Qty)<=0) {
      File_Kosong()
    }else if (Price=='' || parseFloat(Price)<=0) {
     File_Kosong()
     }else{


     var row = t.row.add( [
         i,
         itemcode,
         Description,
         QtyOrder,
         QtyTampil,
         Unit,
         PriceTampil,
         SubTotalTampil,
         btndelete
      ] ).draw(false);
     t.row(row).column(0).nodes().to$().addClass('No');
      t.row(row).column(7).nodes().to$().addClass('Subtotal');

     $('<input>').attr({
     type: 'hidden',
     class: i,
     name: 'itemcode['+i+']',
     value: itemcode
     }).appendTo('form');

     $('<input>').attr({
     type: 'hidden',
     class: i,
     name: 'Description['+i+']',
     value: Description
     }).appendTo('form');

     $('<input>').attr({
     type: 'hidden',
     class: i,
     name: 'Qty['+i+']',
     value: Qty
     }).appendTo('form');

     $('<input>').attr({
     type: 'hidden',
     class: i,
     name: 'Price['+i+']',
     value: Price
     }).appendTo('form');

    $('<input>').attr({
     type: 'hidden',
     class: i,
     name: 'Unit['+i+']',
     value: Unit
     }).appendTo('form');

    $('<input>').attr({
     type: 'hidden',
     class: i,
     name: 'Stok['+i+']',
     value: Stok
     }).appendTo('form');

     $('<input>').attr({
     type: 'hidden',
     class: i,
     name: 'Subtotal['+i+']',
     value: SubTotal
     }).appendTo('form');
      //alert(SubTotal);

     $('#Total').val(parseFloat(Total) + parseFloat(SubTotal));

     $('#index').val('');
     $('#index').val(parseFloat(i)+1);
      $('#Description').val('');
      $('#QtyOrder').val('');
      $('#Qty').val('');
      $('#Qty1').val('');
      $('#Unit').val('');
      $('#Price').val('');
      $('#Price1').val('');

   }
  });
  </script>
  <script type="text/javascript">
   $('#example1 tbody').on('click', '.Hapus', function(e){
       var t = $('#example1').DataTable();
      iditem = $(this).closest('tr').children('td.No').text();
      $('.'+iditem).remove();
      t.row($(this).closest('tr')).remove().draw(false);
      $('#IndexDelete').val(iditem);

      var Total =$('#Total').val(); 
      var Subtotal = $(this).closest('tr').children('td.Subtotal').text().replace(/\./g, "");;
      $('#Total').val('');
        $('#Total').val(parseFloat(Total)-parseFloat(Subtotal));
 })
   </script>
  

  <script type="text/javascript">
$('#example1').dataTable({
    "paging": false,
    "ordering": false,
    "searching": false
});
  </script>






  <script>
  $(function () {
    $(".select2").select2();
     $(".select3").select2();

  });
  </script>
  <script type="text/javascript">
   $(document).on('change', '#IdVendor', function(){
    var NamaVen =$("#IdVendor option:selected").text(); 
    $("#Vendor").val('');  
      $("#Vendor").val(NamaVen);
      $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });

    });
</script>

<script type="text/javascript">
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });
</script>




   <script type="text/javascript">
  function File_Kosong() {
  $.alert({
    title: 'Caution!!',
    content: 'Invalid Data!',
    icon: 'fa fa-warning',
    type: 'orange',
});
}
</script>

</script>

<script>
  $( function() {
    $( "#DueDate" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });
    $( "#ExpetedDate" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });


  });
  </script>
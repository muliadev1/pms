<?php

class M_purpose extends CI_Model{

	function Purpose_view() {
		$data = $this->db->query("SELECT * FROM tbpurpose WHERE isaktif=1");
		return $data->result();
   }

	 function Purpose_addDB($table,$data) {
			 $this->db->insert($table,$data);
	}

	function Purpose_view_edit($id) {
	 $this->db->select("*");
	 $this->db->where('id',$id);
	 $this->db->from('tbpurpose');
	 return $this->db->get()->result();
	}

	function Purpose_editDB($table,$data,$id) {
 			 $this->db->where('id',$id );
 			 $this->db->update($table,$data);
  }

}
?>

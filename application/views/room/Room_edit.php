<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>

<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="btn-group btn-breadcrumb">
   <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
   <a href="<?php echo base_url('index.php/Room/Room_view');?>" class="btn btn-default  btn-xs">Room</a>
   <a  class="btn btn-default  btn-xs active">Edit Room</a>
  </div>
</section>

<!-- Main content -->
<section class="content">
 <div class="row">
  <div class="col-md-12">
   <div class="box box-info">
     <div class="box-header">
      <h3 class="box-title">Edit Room Type</h3>
     </div>
     <div class="box-body">
      <form method="post" id="Simpan" action="<?php echo base_url(). 'index.php/Room/Room_editDB'; ?>">

        <div class="form-group">
         <label>Room Type</label>
         <select id="roomtype" name="roomtype" class="form-control select2"  style="width: 100%;">
           <?php
            foreach($room_type as $u){
              //print_r($country);exit();
              if ($u->id == $data[0]->idroomtype) {
                ?>
                <option value="<?php echo $u->id; ?>"selected><?php echo $u->roomtype; ?></option>
                <?php
              } else {
                ?>
                <option value="<?php echo $u->id; ?>"><?php echo $u->roomtype; ?></option>
                  <?php
              }
            }
                ?>
         </select>
        </div>

        <div class="form-group">
         <label>Room Status</label>
         <select id="roomstatus" name="roomstatus" class="form-control select2"  style="width: 100%;">
           <?php
            foreach($room_status as $u){
              //print_r($country);exit();
              if ($u->id == $data[0]->idroomstatus) {
                ?>
                <option value="<?php echo $u->id; ?>"selected><?php echo $u->status; ?> - <?php echo $u->description; ?></option>
                <?php
              } else {
                ?>
                <option value="<?php echo $u->id; ?>"><?php echo $u->status; ?> - <?php echo $u->description; ?></option>
                <?php
              }
            }
                ?>
         </select>
        </div>

        <div class="form-group">
         <label class="control-label">Room Name</label>
         <input type="text" name="roomname" id="roomname" value="<?php echo $data[0]->roomname; ?>"  data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out firstname..."  class="form-control"  placeholder="">
         <input type="hidden" name="Id"  value="<?php echo $data[0]->id; ?>" class="form-control"  placeholder="">
        </div>

        <div class="form-group">
         <label class="control-label">Description</label>
        <textarea type="text" name="desc" id="desc" data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out firstname..."  class="form-control"><?php echo $data[0]->desc;?></textarea>
      </div>

       <div class="form-group">
        <button type="submit" value="Validate" class="btn btn-default"><i class='glyphicon glyphicon-ok'></i> Save</button>
       </div>
      </form>
     </div>
    </div>
   </div>
  </div>
</section>

<script type="text/javascript">
$("#Simpan").submit(function() {
     var roomtype = $('#roomtype').val();
     var roomstatus = $('#roomstatus').val();
     var roomname = $('#roomname').val();
     var description   = $('#desc').val();
        if (roomtype == ''|| roomstatus == ''|| roomname == ''|| description == ''){
            File_Kosong(); return false;
        }else{
        event.preventDefault();
        $.confirm({
          title: 'Confirmation',
          content: 'Are You Sure to Save?',
           type: 'blue',
          buttons: {
              Save: function () {
                  $.LoadingOverlay("show");
                  $("#Simpan").submit();
              },
              Cancel: function () {

                  $.alert('Data Not Saved...');
              },
          }
      });
    }

});
</script>

<?php
$this->load->view('template/Foot');
$this->load->view('template/Js');
?>


<script>

  $.validate({
    modules : 'location, date, file',
    onModulesLoaded : function() {
      $('#country').suggestCountry();
    }
  });

  // Restrict presentation length
  $('#presentation').restrictLength( $('#pres-max-length') );

</script>
<script type="text/javascript">
  function File_Kosong() {
  $.alert({
    title: 'Caution!!',
    content: 'Edit Data Invalid!',
    icon: 'fa fa-warning',
    type: 'orange',
});
}
</script>

<script>
$(function () {
  $(".select2").select2();
   $(".select3").select2();

});
</script>

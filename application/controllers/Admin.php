<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller{

	function __construct(){
		parent::__construct();
		//$this->load->library('auth');
		if($this->session->userdata('Kode_userKop')==NULL) {
           redirect('Login');
		}
	
		
		if (!empty($this->session->userdata("active_db"))) {
			$this->load->database($this->session->userdata("active_db"), FALSE, TRUE);
		}		
	}

	function index(){
		$this->load->view('dashboard1');
	}
}
<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>
<style type="text/css">
.modal.modal-wide .modal-dialog {
  width: 90%;
}

.modal-wide .modal-body {
  overflow-y: auto;
}

#tallModal .modal-body p { margin-bottom: 900px }
.pull-right {
    float: right;
}

.pull-left {
    float: left;
}

</style>

<section class="content-header">
    <div class="btn-group btn-breadcrumb">
            <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
            <a  class="btn btn-default  btn-xs active">Puschase Requisition</a>
        </div>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Category</h3>
          <a style="float:right"  href="<?php echo base_url('index.php/Purchase_requisition/purchase_requisition_add');?>" class="btn btn-primary">
            <i class="glyphicon glyphicon-saved"></i>
            Add Puschase Requisition
          </a>
        </div>
    <!-- /.box-header -->
        <div class="box-body">
        <div class="table-responsive">
          <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr >
                                        <th >NO</th>
                                        <th >Code</th>
                                        <th >Date</th>
                                        <th >Departemen</th>
                                        <th >Description</th>
                                        <th >User</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i=1;
                                    foreach($data as $u){ 

                                    ?>
                                    
                                    <tr  class='odd gradeX context'>
                                         <td><?php echo $i ?></td>
                                        <td class="Kodepr"><?php echo $u->kodepr ?></td>
                                        <td class="Date"><?php echo $u->dateadd?></td>
                                        <td><?php echo $u->departement?></td>
                                        <td><?php echo $u->description?></td>
                                        <td><?php echo $u->nama_user?></td>
                                         <td align="center">
                                         <a class="btn btn-default Detail"   href="#" title="View Detail">  <span class="fa fa-fw fa-list" ></span> </a>
                                        
                                        </td>






                                    </tr>
                                    <?php $i++;} ?>
                                </tbody>
                            </table>
          </div>



        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->

</section><!-- /.content -->


<div class="modal modal-wide fade" id="myModal" role="dialog">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 align="center" class="modal-title">Puschase Requisition Detail</h4>
                            </div>
                            
                            <div class="modal-body">
                            <table class="table box-widget" style=" border: 1px solid black; "  >
                              <tr>
                                <td width="15%" style="background-color: #3399FF; border: 1px solid black; "> Code</td>
                                <td  width="35%"id ="kodeprm" style=" background-color: #C2F2FF; border: 1px solid black; " ><strong> <span></span> </strong> </td>
                                <td width="15%" style="background-color: #3399FF; border: 1px solid black; ">Date</td>
                                <td  width="35%"id ="Datem" style="background-color: #C2F2FF; border: 1px solid black; " ><strong> <span></span> </strong> </td>
                              </tr>
                              </table>
                                 <div class="modal-append">
                                <p> </p>
                                </div> 
                            </div>
                            
                        </div>
                    </div>
                </div>


<?php
$this->load->view('template/Js');
$this->load->view('template/Foot');
?>


<script type="text/javascript">
  $(document).ready(function(){
     $(".Detail").click(function(e){
      var kodepr =$(this).closest('tr').children('td.Kodepr').text();
      var date =$(this).closest('tr').children('td.Date').text();         
      $.ajax({
        type: "POST",
       url: '<?php echo site_url('Purchase_requisition/purchase_requisition_view_detail');?>',
        data: {id: kodepr},
        success: function(data){
             e.preventDefault();
            var mymodal = $('#myModal');
            var height = $(window).height() - 200;
            mymodal.find(".modal-append").css("max-height", height);
            $("#kodeprm span").text(kodepr);
            $("#Datem span").text(date);
            mymodal.find('.modal-append').html(data);
            mymodal.modal('show');           
        }
      });
    });
  });
</script>


<script>
  $(function () {
    $("#example1").DataTable();
     $("#example11").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>


<?php

class M_stok_opname extends CI_Model{

	function stok_opname_view($Istok,$IsProses,$set ,$Subcategory,$Departement) {
		if($Departement!=0){
		$data = $this->db->query("SELECT codeitem,tbsubcategory.subcategory,tbitem.description,
															unit,price,stok
										FROM tbitem inner join tbsubcategory
										ON tbitem.idsubcategory = tbsubcategory.idsubcategory
										INNER JOIN tbsubcategorydepartement ON tbitem.`codeitem` = tbsubcategorydepartement.`itemcode` 
															Where tbitem.isaktif = 1 
															and tbsubcategorydepartement.kodesubcategory='$Subcategory'
															and tbsubcategorydepartement.iddepartement='$Departement'  ");
		}else{
			$data = $this->db->query("SELECT codeitem,tbsubcategory.subcategory,tbitem.description,unit,price, stok 
										FROM tbitem INNER JOIN tbsubcategory ON tbitem.`idsubcategory` =  tbsubcategory.`idsubcategory`
										 WHERE   tbsubcategory.idsubcategory='$Subcategory' 
										 AND  tbitem.isaktif = 1  ");

		}

		
		//print_r($this->db->last_query());exit();
		return $data->result();

   }
   function Select_subcategory_bydepartememnt($idDepartement) {
   	if ($idDepartement!=0) {
   		$data = $this->db->query("SELECT * FROM tbsubcategory WHERE idsubcategory IN 
								(SELECT kodesubcategory FROM tbsubcategorydepartement WHERE iddepartement = '$idDepartement')");
   	}else{
   		$data = $this->db->query("SELECT * FROM tbsubcategory ");
   	}
		
		return $data->result();

   }
		

   function stok_opname_view_form($idsub,$iddepartement) {
   	if ($iddepartement!=0) {
   		 $data = $this->db->query("SELECT tbitem.codeitem,tbitem.description,subcategory FROM tbitem 
							INNER JOIN tbsubcategorydepartement ON tbitem.`codeitem` = tbsubcategorydepartement.`itemcode` 
							INNER JOIN tbsubcategory ON tbsubcategorydepartement.`kodesubcategory` =  tbsubcategory.`idsubcategory`
										 WHERE tbitem.`isaktif` =1   and tbsubcategorydepartement.kodesubcategory='$idsub' and 
										 iddepartement='$iddepartement' ");
   	}else{
   		$data = $this->db->query("SELECT tbitem.codeitem,tbitem.description,subcategory FROM tbitem 
							INNER JOIN tbsubcategory ON tbitem.`idsubcategory` =  tbsubcategory.`idsubcategory`
										 WHERE tbitem.`isaktif` =1   and tbsubcategory.idsubcategory='$idsub' ");

   	}
		
		return $data->result();

   }
    function loaddepartementall() {
		$data = $this->db->query("Select * from tbdepartement where isaktif='1' order by iddepartement asc");
		return $data->result();

   }
   

   function stok_opname_view_list() {
		$data = $this->db->query("SELECT DATE_FORMAT(DATE,'%d-%m-%Y') AS tgl FROM tbstokopname  GROUP BY tgl ");
		return $data->result();
   }
    function load_subcategory() {
		$data = $this->db->query("select * from tbsubcategory order by idsubcategory asc");
		return $data->result();
   }
   function load_category() {
		$data = $this->db->query("select * from tbcategory order by idcategory asc");
		return $data->result();
   }

   function load_so_forlaporan($tgl,$idsub,$iddepartement) {
   	
   	if ($iddepartement!=0) {
   		$data = $this->db->query("SELECT DATE_FORMAT(DATE,'%d-%m-%Y') AS tgl,tbstokopname.`itemcode`,tbstokopname.`description`,
				tbitem.`unit`,tbstokopname.`price`,tbstokopname.`stok`,`inputstok`,`diff`  
				FROM tbstokopname INNER JOIN tbitem 
				ON tbstokopname.itemcode = tbitem.codeitem INNER JOIN tbsubcategory
				ON tbitem.idsubcategory = tbsubcategory.idsubcategory
				INNER JOIN tbsubcategorydepartement ON tbitem.`codeitem` = tbsubcategorydepartement.`itemcode` 
				WHERE tbitem.isaktif = 1 
				AND tbsubcategorydepartement.kodesubcategory='$idsub'
				AND tbsubcategorydepartement.iddepartement='$iddepartement'
				AND tbstokopname.`iddepartement` = '$iddepartement'
				AND  DATE_FORMAT(tbstokopname.date,'%Y-%m-%d') =  '$tgl' ");
   		
   	}else{
   		$data = $this->db->query("SELECT DATE_FORMAT(DATE,'%d-%m-%Y') AS tgl,tbstokopname.`itemcode`,tbstokopname.`description`,
				tbitem.`unit`,tbstokopname.`price`,tbstokopname.`stok`,`inputstok`,`diff` FROM tbstokopname INNER JOIN tbitem 
				ON tbstokopname.itemcode = tbitem.codeitem FROM tbstokopname 
				WHERE  DATE_FORMAT(tbstokopname.date,'%Y-%m-%d') =  '$tgl' and tbitem.idsubcategory='$idsub' ");

   	}
   	

		



		return $data->result();

   }

   

  function SO_add($dataDetil,$dataPr,$dataDetilPr,$set, $IsProses) { 
       $this->db->trans_begin(); 

 		$this->db->insert_batch( 'tbstokopname', $dataDetil);
 		$this->db->insert( 'tbsr', $dataPr);
      	$this->db->insert_batch( 'tbsrdetil', $dataDetilPr);  

 		foreach ($dataDetil as $data ) {
 			$this->db->set($set,$data['inputstok']);
		    $this->db->where('codeitem',$data['itemcode'] );
		    $this->db->update('tbitem'); 
				//`isprosesopname``isprosesopnameHK``isprosesopnameFO``isprosesopnameFB``isprosesopnameBO``isprosesopnameEng``isprosesopnamePG``isprosesopnameSec`
		    $this->db->set($IsProses,1);
		    $this->db->where('codeitem',$data['itemcode'] );
		    $this->db->update('tbitem'); 
 		 } 

 		
 		if ($this->db->trans_status() === FALSE){
			   $this->db->trans_rollback();
		}else{
			    $this->db->trans_commit();
			}    
   }
	

}
?>

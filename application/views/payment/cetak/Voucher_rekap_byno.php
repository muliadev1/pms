
<?php echo $header?>

<style type="text/css">
  #TabelKonten tr td {
    padding-right: 7px;
    padding-left:  7px;
    font-size: 12px;
  }
</style>

<div style="margin-buttom : 15px;" >
<table width="100%" border="0"  >
    <tr>
     <td colspan="2" align="center" style="font-size:14px;"> <strong> REKAP VOUCHER  </strong>  </td>
    </tr>
    
   
  </table>
<table width="100%" border="0" style="font-size:11px;"  >
 <?php  
    if ($periode['Method']==0){ 
              $Methodhead ='All Method' ;
            }elseif ($periode['Method']==1) {
             $Methodhead ='Cash' ;
            }else{
              $Methodhead ='Cheque';
            } ?>

     <tr>
      <td width="15%"><strong>Voucher No From </strong> </td>
     <td align="left">: <?php echo $periode['nodari'];?></td>
    </tr>
    <tr>
      <td width="15%"><strong>Voucher No To</strong> </td>
     <td align="left">: <?php echo $periode['nosampai'];?></td>
    </tr>
     <tr>
      <td width="15%"><strong>Method</strong> </td>
     <td align="left">: <?php echo  $Methodhead; ?></td>
    </tr>
    <tr>
     <td>&nbsp;</td>
     <td align="left">&nbsp;</td>
    </tr>

    

  </table>
</div>

<table id="TabelKonten"  border="1" style="border-collapse: collapse; border-color:#000000; margin-bottom : 130px;"  width="100%"   >
  <?php if ($periode['Method']==0){  ?>
    <thead  >
        <tr align="center" class="header">
          <th width="5%" >No</th>
          <th width="10%" >Voucher No</th>
          <th width="10%" >Voucher Date</th>
          <th width="10%" >Payment Method</th>
          <th width="15%" >Vendor</th>
          <th width="20%" >Note</th>
          <th width="10%" >Amount</th>
          <th width="10%" >Bank</th>
           <th width="10%" >Cheque No</th>
          <th width="10%" >Amount Check</th>
           
        </tr>
    </thead>
     <tbody> 
      <?php $total=0;$no=1; foreach ($konten as $row) { ?>
      <?php if ($row->paymentmethod==0){ 
              $Method ='Cash' ;
            }else{
               $Method ='Cheque';
            } ;
            $voucherno= $row->voucherno;
            ?>
            <?php 
       if ($voucherno != $voucherno1 and $no!=1) { ?>
        <tr style="background-color: #f0f0f0;">
          <td colspan="6"><strong>SUBTOTAL</strong></td>
          <td  align="right"><strong><?php echo number_format($totaldetil) ?></strong></td>
           <td colspan="2">&nbsp;</td>
           <td  align="right"><strong><?php echo number_format($totalcheckdetil) ?></strong></td>
        </tr>
      <?php $totaldetil=0; $totalcheckdetil=0;} ?>
    <tr>
       <td><?php echo $no; ?></td>
       <td><?php echo $row->voucherno; ?></td>
      <td><?php echo $row->voucherdate1; ?></td>
       <td><?php echo $Method; ?></td>
       <td><?php echo $row->vendor; ?></td>
       <td><?php echo $row->description; ?></td>
       <td align="right"><?php echo number_format($row->total, 0, '.', '.') ?> </td>
       <td><?php echo $row->namabank; ?></td>
        <td><?php echo $row->checkno; ?></td>
       <td align="right"><?php echo number_format($row->totalcheck, 0, '.', '.') ?></td>
     </tr>

       
    <?php  $no++; $total+=$row->total; $totaldetil+=$row->total; $totalcheck+=$row->totalcheck; $totalcheckdetil+=$row->totalcheck; $voucherno1= $row->voucherno;} ?>
    <tr style="background-color: #f0f0f0;" >
          <td colspan="6" ><strong>SUBTOTAL</strong></td>
          <td  align="right" ><strong><?php echo number_format($totaldetil) ?></strong></td>
           <td colspan="2">&nbsp;</td>
           <td  align="right" ><strong><?php echo number_format($totalcheckdetil) ?></strong></td>
        </tr>
    </tbody>
    <tfoot>
      <tr style="background-color: #f0f0f0;">
        <td colspan="6"> <strong> TOTAL</strong></td>
        <td align="right"><strong><?php echo number_format($total, 0, '.', '.') ?></strong></td>
        <td colspan="2">&nbsp;</td>
         <td align="right"><strong><?php echo number_format($totalcheck, 0, '.', '.') ?></strong></td>
      </tr>
    </tfoot>



<!--  cash ///////////////////////////////////////////////////////////////////////////////// -->
             
   <?php } elseif ($periode['Method']==1) { ?>

   <thead  >
        <tr align="center" class="header">
          <th width="5%" >No</th>
          <th width="15%" >Voucher no</th>
          <th width="15%" >Voucher Date</th>
          <th width="15%" >Vendor</th>
           <th width="15%" >Note</th>
          <th width="15%" >Amount</th>
           
        </tr>
    </thead>
     <tbody>
      <?php $total=0;$no=1; foreach ($konten as $row) { ?>
     
    <tr>
       <td><?php echo $no; ?></td>
       <td><?php echo $row->voucherno; ?></td>
       <td><?php echo $row->voucherdate1; ?></td>
       <td><?php echo $row->vendor; ?></td>
        <td><?php echo $row->description; ?></td>
       <td align="right"><?php echo number_format($row->total, 0, '.', '.') ?> </td>
        <?php  $no++; $total+=$row->total;} ?>
    </tr>
    </tbody>
    <tfoot>
      <tr style="background-color: #f0f0f0;">
        <td colspan="5"> <strong> Total</strong></td>
        <td align="right"><strong><?php echo number_format($total, 0, '.', '.') ?></strong></td>
      </tr>
    </tfoot>
          
          <!--  Check ///////////////////////////////////////////////////////////////////////////////// -->   
   <?php }else{ ?>
    <thead  >
        <tr align="center" class="header">
          <th width="5%" >No</th>
          <th width="10%" >Voucher No</th>
          <th width="10%" >Voucher Date</th>
          <th width="15%" >Vendor</th>
          <th width="15%" >Note</th>
          <th width="15%" >Amount</th>
          <th width="10%" >Bank</th>
          <th width="10%" >Cheque Date</th>
           <th width="10%" >Cheque No</th>
          <th width="10%" >Amount Check</th>
           
        </tr>
    </thead>
     <tbody> 
      <?php $total=0;$no=1; foreach ($konten as $row) { ?>
      
    <tr>
       <td><?php echo $no; ?></td>
       <td><?php echo $row->voucherno; ?></td>
      <td><?php echo $row->voucherdate1; ?></td>
       <td><?php echo $row->vendor; ?></td>
        <td><?php echo $row->description; ?></td>
       <td align="right"><?php echo number_format($row->total, 0, '.', '.') ?> </td>
       <td><?php echo $row->namabank; ?></td>
       <td><?php echo $row->tglinput1; ?></td>
        <td><?php echo $row->checkno; ?></td>
       <td align="right"><?php echo number_format($row->totalcheck, 0, '.', '.') ?></td>
        <?php  $no++; $total+=$row->total; $totalcheck+=$row->totalcheck;} ?>
    </tbody>
    <tfoot>
      <tr style="background-color: #f0f0f0;">
        <td colspan="5"> <strong> Total</strong></td>
        <td align="right"><strong><?php echo number_format($total, 0, '.', '.') ?></strong></td>
        <td colspan="3">&nbsp;</td>
         <td align="right" style="font-size: 11px;"><strong><?php echo number_format($totalcheck, 0, '.', '.') ?></strong></td>
      </tr>
    </tfoot>
         
     <?php   } ?>
             
   

  
     

  </table>



  






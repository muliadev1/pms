<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>
<script src="<?php echo base_url('assets/js/accounting.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/cleave.min.js') ?>" type="text/javascript"></script>
<style type="text/css">
 /* .No{
    display : none;
}

}*/

.StokEditTd{
    display : none;
}
.modal.modal-wide .modal-dialog {
  width: 80%;
}
 .box-widget {
border: #adadad solid 1px;
padding: 10px;
margin-top: 30px;
margin-bottom: 30px;
}
.modal-wide .modal-body {
  overflow-y: auto;
}

td.No { display: none
 }


#tallModal .modal-body p { margin-bottom: 900px }
.pull-right {
    float: right;
}

.pull-left {
    float: left;
}

select {
    width: 300px;
}
.breadcrumbs-two a{
  background: #ddd;
  padding: .7em 1em;
  float: left;
  text-decoration: none;
  color: #444;
  text-shadow: 0 1px 0 rgba(255,255,255,.5); 
  position: relative;
}


</style>
    <script type="text/javascript">
      jQuery(document).ready(function($) {
        new Cleave('.input-1', {
           numeral: true, 
           numeralDecimalMark: ',',
           delimiter: '.' 
      });
        new Cleave('.input-2', {
           numeral: true, 
           numeralDecimalMark: ',',
           delimiter: '.' 
      });
        new Cleave('.input-3', {
           numeral: true, 
           numeralDecimalMark: ',',
           delimiter: '.' 
      });
        new Cleave('.input-4', {
           numeral: true, 
           numeralDecimalMark: ',',
           delimiter: '.' 
      });
          });
    </script>
    <script type="text/javascript">
      function doMath()
      {
          var qty = document.getElementById('Qty1').value;
          document.getElementById('Qty').value =  qty.replace(/\./g, "");

          var price = document.getElementById('Price1').value;
          document.getElementById('Price').value =  price.replace(/\./g, "");

      }
    </script>
    <script type="text/javascript">
      function doMathdisc()
      {
          var disc = document.getElementById('Disc1').value;
          document.getElementById('Disc').value =  disc.replace(/\./g, "");
          total = document.getElementById('Total').value;
         
          var discHitung = disc.replace(/\./g, "");
           var TotalSetDisc = parseFloat(total) - parseFloat(discHitung);
           document.getElementById('TotalSetDiskon').value = TotalSetDisc;
           document.getElementById('TotalSetDiskon1').value =  TotalSetDisc.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");


      }
    </script>

    <script type="text/javascript">
     function doMathdp()
      {

          var dp = document.getElementById('DP1').value;
          document.getElementById('DP').value =  dp.replace(/\./g, "");
          var disc = document.getElementById('Disc').value;

          dphitung = dp.replace(/\./g, "");
          total = document.getElementById('Total').value;
          var remain = parseFloat(total) -parseFloat(dphitung) - parseFloat(disc);
          document.getElementById('Remain').value =  remain;
          document.getElementById('Remain1').value =  remain.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
      }
    </script>

<!-- Content Header (Page header) -->
<section class="content-header">
   <div class="btn-group btn-breadcrumb">
            <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
            <a  class="btn btn-default  btn-xs active">Add Direct Order</a>
        </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="row" >
        <div class="col-md-12">
             <div class="box box-info">
              <form method="post" id="Simpan" action="<?php echo base_url(). 'index.php/Purchase_order/add_po_direct'; ?>" enctype="multipart/form-data">
        <div class="box-header">
          <div class="row">
             <div class="col-md-3">
             </div>
             <div class="col-md-3">
                <div id="cssmenu"  >
                  <ul>
                     <li class="active"><a>Add Item</a></li>
                     <li id="AddPayment" style="cursor:pointer;"> <a >Payment Method</a></li> 
                     <li id="DropTo" style="cursor:pointer;" > <a>Drop  </a></li>
                     <li id="Save" style="cursor:pointer;" > <a> Save</a></li>
                  </ul>
                </div>
              </div>
              <div class="col-md-3" style="float:right; margin-right:50px;" align="right">
             </div>
            </div>                 
             <hr style="border-top: 1px solid #8c8b8b; padding: 1px; margin-top: 5px; margin-bottom: 0px;">
        </div>
        <div class="box-body">
            <div class="row" style="margin-bottom:5px;">
             <div class="col-md-3">
             </div>
             <div class="col-md-3" >
                <a style="margin-top:5px; float:right; "  id="BackMati" class="btn btn-primary  btn-xs btn-flat disabled"  ><i class='glyphicon glyphicon-arrow-left'></i>  Back  </a>
                <a style="margin-top:5px; float:right; display:none; "   id="BackToItem" class="btn btn-primary  btn-xs btn-flat "  ><i class='glyphicon glyphicon-arrow-left'></i>  Back  </a>
                <a style="margin-top:5px; float:right; display:none; "  id="BackToPay" class="btn btn-primary  btn-xs btn-flat "  ><i class='glyphicon glyphicon-arrow-left'></i>  Back  </a>
                <a style="margin-top:5px; float:right; display:none; "  id="BackToDrop" class="btn btn-primary  btn-xs btn-flat "  ><i class='glyphicon glyphicon-arrow-left'></i>  Back  </a>
              </div> 
              <div class="col-md-3" >
                <a style="margin-top:5px; float:left;"  id="NextToPay" class="btn btn-primary  btn-xs btn-flat "  ><i class='glyphicon glyphicon-arrow-right'></i>  Next  </a>
                <a style="margin-top:5px; float:left; display:none;"  id="NextToDrop" class="btn btn-primary  btn-xs btn-flat "  ><i class='glyphicon glyphicon-arrow-right'></i>  Next  </a>
                <a style="margin-top:5px; float:left; display:none;"  id="SaveFinal" class="btn btn-primary  btn-xs btn-flat "  ><i class='fa  fa-file'></i>  Save  </a>
             </div>
            </div>
        <div id="ElementAddItem"  >
        <div class="box-widget">
               <div class="row ">
                 <div class="col-md-6 ">
                   <div class="form-group">
                      <label>Vendor</label>
                    <select id="IdVendor" class="form-control select2"  style="width: 100%;" name="IdVendor">                 
                      <?php
                          foreach($data as $u){
                      ?>
                        <option value="<?php echo $u->idvendor; ?>" ><?php echo $u->vendor; ?></option>
                        <?php } ?>
                    </select>
                    <input type="hidden" name="Vendor" id="Vendor" value="<?php echo $data[0]->vendor; ?>">
                  </div>
                </div>
              </div>
              <div class="row">
                 <div class="col-md-6">
                    <div class="form-group">
                    <label>No Receipt</label>
                       <input type="text" name="NoReceipt" id="NoReceipt" autocomplete="off"  class="form-control"  >
                  </div>
                </div>
              </div>
               <div class="row">
                  <div class="col-md-2">
                    <div id ="divItemCode" class="form-group">
                          <label class="control-label">Item Code</label>
                          <input type="text" name="ItemCodeInput" id="ItemCodeInput"  class="form-control">
                     </div>
                  </div>
                  <div class="col-md-4">
                    <div id ="divDescription" class="form-group">
                          <label class="control-label">Description</label>
                          <input type="text" name="Description" id="Description" readonly="readonly" class="form-control">
                     </div>
                  </div>
                  <div  class="col-md-1">
                    <div id ="divQty" class="form-group">
                          <label class="control-label">Qty</label>
                          <input type="text" name="Qty1" id="Qty1" autocomplete="off"  class="form-control" onkeyup="doMath()" >
                          <input type="hidden" name="Qty" id="Qty"  onkeyup="doMath()">
                     </div>
                  </div>
                  <div  class="col-md-2">
                    <div id ="divPrice" class="form-group">
                          <label class="control-label">Price</label>
                          <input type="text" name="Price1" id="Price1"  class="form-control input-2" onkeyup="doMath()">
                           <input type="hidden" name="Price" id="Price"  class="form-control" onkeyup="doMath()">
                           <input type="hidden" name="IndexNew" id="IndexNew"  class="form-control">
                           <input type="hidden" name="UnitNew" id="UnitNew"  class="form-control">
                           <input type="hidden" name="index" id="index" value="0"  class="form-control">
                           <input type="hidden" name="Total" id="Total" value="0"  class="form-control">
                           <input type="hidden" name="StokAdd" id="StokAdd" value="0"  class="form-control">
                           <input type="hidden" name="IsCash" id="IsCash" value="0"  class="form-control">

                     </div>
                  </div>
                  <div  class="col-md-3">
                    <div class="form-group" >
                        <a style="margin-top:25px; margin-right:10px; " class="btn btn-primary btn-flat disabled Edit" id="Edit"  ><i class='glyphicon glyphicon-arrow-down'></i>  Proses  </a> 
                        <a style="margin-top:25px; margin-right:10px; " class="btn btn-warning btn-flat disabled " id="Batal"  ><i class='glyphicon glyphicon-remove'></i>  Batal  </a>  
                     </div>
                  </div>
                </div>
            </div>
          

                      <div class="table-responsive">
                         <table id="tbdetil" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th style="display:none">No</th>
                                    <th>Item Code</th>
                                    <th>Description</th>
                                    <th>Qty Order</th>
                                    <th>Unit</th>
                                    <th>Price</th>
                                    <th>Subtotal</th>
                                    <th>Action</th>
                                     <th style="display:none">Stok</th>
                                    
                                </tr>
                            </thead>
                            <tbody id="mytbody">
                          
                                
                            </tbody>
                        </table>
                      </div>
                    </div> 
            <div id="ElementAddPayment" class="box-widget" hidden="hidden">
            <label class="control-label">Discount</label>
            <div class="row">
                <div class="col-md-2">
                  <div class="form-group">
                    <input type="text" name="Disc1" id="Disc1"  class="form-control input-4" onkeyup="doMathdisc()">
                    <input type="hidden" name="Disc" id="Disc"  class="form-control" onkeyup="doMathdisc()">
                  </div>
                </div>
              <label class="control-label">Total</label>
                <div class="col-md-2">
                  <div class="form-group">
                    <input type="text" name="TotalSetDiskon1" id="TotalSetDiskon1"  class="form-control input-4" readonly="readonly" onkeyup="doMathdisc()">
                    <input type="hidden" name="TotalSetDiskon" id="TotalSetDiskon"  class="form-control" onkeyup="doMathdisc()">
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-2">
                  <label class="control-label">Method</label>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4">
                <div class="form-group">
                <label style="margin-right:50px;">
                  <input type="radio" name="r3" class="flat-red" name="Cash" id="Cash" checked>
                  Cash
                </label>
                <label>
                  <input type="radio" name="r3" class="flat-red" name="Credit" id="Credit">
                  Credit
                </label>
              </div>          
                </div>
              </div>
           
              <div id="ElementCredit" hidden="hidden">
                <div class="row">
                  <div class="col-md-4">
                  <div class="form-group" id="DivDP">
                    <label class="control-label">Down Payment</label>
                         <input type="text" name="DP1" id="DP1"  class="form-control input-3" onkeyup="doMathdp()">
                         <input type="hidden" name="DP" id="DP"  class="form-control" onkeyup="doMathdp()">
                    </div>
                  </div>
                  <div class="col-md-4">
                   <div class="form-group" id="DivRemain" >
                    <label class="control-label">Remain</label>
                         <input type="text" name="Remain1" id="Remain1"  class="form-control" readonly="readonly" onkeyup="doMathdp()">
                          <input type="hidden" name="Remain" id="Remain"  class="form-control" onkeyup="doMathdp()">
                   </div>
                  </div>
                </div>

                 <div class="row">
                  <div class="col-md-4">
                  <div class="form-group" id="DivDueDate">
                    <label class="control-label">Due Date</label>
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text"   value="<?= date('m/d/Y') ?>"     class="form-control pull-right" name= "DueDate" id="DueDate">
                      </div>
                  </div>
                  </div>
                </div>
              </div> 
            </div>

            <div id="ElementDropTo" class="box-widget" hidden="hidden">
            <div class="row">
                 <div class="col-md-6">
                   <div class="form-group" readonly="readonly" >
                      <label>Depatement</label> 
                    <select id="IdDepartement" class="form-control select2"  style="width: 100%;"  name="IdDepartement">  
                     <option value="0" >Store</option>               
                     <!-- <?php
                          foreach($dataDepaetemen as $u){
                      ?>
                        <option value="<?php echo $u->iddepartement; ?>" ><?php echo $u->departement; ?></option>
                        <?php } ?> -->
                    </select>
                    <input type="hidden" name="NamaDepartemen" id="NamaDepartemen" value="Store">
                  </div>
                </div>
              </div>
            </div>

                       
          <a   type="submit" hidden="hidden"   id="simpan1"  >  </a>       
        </form>  
      </div>
     </div>
</div>

    <div class="modal modal-wide fade" id="myModal" role="dialog" style="display:none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" title="close" data-dismiss="modal">&times;</button>
                    <h4 align="center" class="modal-title">Purchase Requisition List</h4>
                </div>
                <div class="modal-body">   
                </div>
            </div>
        </div>     
    </div>

      <div class="modal modal-wide fade" id="myModalNew" role="dialog" style="display:none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" title="close" data-dismiss="modal">&times;</button>
                    <h4 align="center" class="modal-title">Item List</h4>
                </div>
                <div class="modal-body">   
                </div>
            </div>
        </div>     
    </div>

 

</section>

  

<?php
$this->load->view('template/Js');
$this->load->view('template/Foot');
?>




 



<script type="text/javascript">
$(function () {
  $("#NextToPay").click(function(){ 
  var Total =$('#Total').val();
  if (parseFloat(Total) >0) {
        event.preventDefault();
                   $('#ElementAddItem').fadeOut(300); 
                   $('#BackMati').fadeOut(300); 
                   $('#NextToPay').fadeOut(300); 
                   $('#BackToItem').fadeTo(0,300);
                   $('#NextToDrop').fadeTo(0,300);
                   $('#ElementAddPayment').fadeTo(0,3000);
                   $('#TotalSetDiskon1').val(Total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."))
                   $('#TotalSetDiskon').val(Total)
                   $('#DP1').val('0')
                   $('#DP').val('0')
                   $('#Disc1').val('0')
                   $('#Disc').val('0')
                   $('#Remain1').val(Total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."))
                   $('#Remain').val(Total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."))
                   $('#AddPayment').addClass('active'); 
        }else{
          File_Kosong();
        }; 
     });  
  });
</script>

<script type="text/javascript">
  $(function () {
  $("#BackToPay").click(function(){  
        event.preventDefault();        
                    $('#ElementAddItem').fadeOut(300); 
                   $('#BackMati').fadeOut(300); 
                   $('#NextToPay').fadeOut(300); 
                   $('#BackToItem').fadeTo(0,300);
                   $('#NextToDrop').fadeTo(0,300);
                   $('#ElementAddPayment').fadeTo(0,3000);
                   $('#BackToPay').fadeOut(0,3000);
                   $('#ElementDropTo').fadeOut(300);
                   $('#SaveFinal').fadeOut(0,3000);
                   $('#DropTo').removeClass('active'); 
     });  
  });
</script>
<script type="text/javascript">
  $(function () {
  $("#BackToItem").click(function(){  
        event.preventDefault();
                  $('#ElementAddItem').fadeIn(300); 
                   $('#BackMati').fadeIn(300); 
                   $('#NextToPay').fadeIn(300); 
                   $('#BackToItem').fadeOut(0,300);
                   $('#NextToDrop').fadeOut(0,300);
                   $('#ElementAddPayment').fadeOut(0,3000);
                   $('#AddPayment').removeClass('active'); 
 

     });  
  });
</script>


<script type="text/javascript">
  $(function () {
  $("#NextToDrop").click(function(){  
        event.preventDefault();
          $('#ElementAddPayment').fadeOut(300);
           $('#ElementDropTo').fadeTo(0,3000);
           $('#BackToPay').fadeTo(0,3000);
           $('#SaveFinal').fadeTo(0,3000);
           $('#BackToItem').fadeOut(0,300);
           $('#NextToDrop').fadeOut(0,300);
           $('#DropTo').addClass('active');  

     });  
  });
</script>

<script type="text/javascript">
  $(function () {
  $("#SaveFinal").click(function(){  
    $('#simpan1').trigger('click');  
     });  
  });
</script>

<script type="text/javascript">
  $(function () {
  $("#Save").click(function(){  
    $('#simpan1').trigger('click');  
     });  
  });
</script>

<script type="text/javascript">
$("#simpan1").click(function() {
        event.preventDefault();
        $.confirm({
          title: 'Confirmation',
          content: 'Are You Sure to Save?',
           type: 'blue',
          buttons: {
              Simpan: function () {
                  $.LoadingOverlay("show");
                  $("#Simpan").submit();
              },
              Batal: function () {
                
                  $.alert('Data Tidak Disimpan...');
              },
          }
      }); 

});
</script>

<script type="text/javascript">
  $('#Credit').on('ifChanged', function(event){
    $('#ElementCredit').fadeTo(0,3000);
    $('#DivRemain').addClass('has-success');
    $('#DivDP').addClass('has-success');
    $('#DivDueDate').addClass('has-success');
    $('#IsCash').val('1');
    $( '#Cash' ).prop( "checked", false );
});
  $('#Cash').on('ifChanged', function(event){
    $('#ElementCredit').fadeOut(0,3000);
    $('#IsCash').val('0');
    $('#Credit').prop("checked", false)
});
</script>


 <script>
        var iditem = '';
        var itemcode = '';
        var Description ='';
        var Qty = '';
        var Unit =  '';
        var Price = '';
        var Subtotal = '';
        var Stok = '';
        var  Total = 0;

  $('table').on('click', '.PilihEdit', function(e){
         var t = $('#tbdetil').DataTable();
         iditem = $(this).closest('tr').children('td.No').text();
         itemcode = $(this).closest('tr').children('td.ItemCode').text();
         Description =$(this).closest('tr').children('td.Description').text();
         Qty = $(this).closest('tr').children('td.Qty').text();
         Unit =  $(this).closest('tr').children('td.Unit').text();
         Price = $(this).closest('tr').children('td.Price').text();
         Subtotal = $(this).closest('tr').children('td.Subtotal').text().replace(/\./g, "");
         Stok = $(this).closest('tr').children('td.StokEditTd').text();
         Total = $('#Total').val();

        $('#Id').val(iditem);
        $('#ItemCodeInput').val(itemcode);
        $('#Description').val(Description);
        $('#Qty').val(Qty);
        $('#Qty1').val(Qty);
        $('#Price').val(Price);
        $('#Price1').val(Price);
        $('#Total').val(parseFloat(Total)-parseFloat(Subtotal));
        styleEdit()
      
     $('.'+iditem).remove();
      t.row($(this).closest('tr')).remove().draw(false);

  })


  $('#Batal').on('click', function(e){ 
  var t = $('#tbdetil').DataTable();
   PriceHitung =Price.replace(/\./g, "");
   QtyHitung = Qty;//.replace(/\./g, "");
   iditemNew = $('#IndexNew').val();
    Total = $('#Total').val();
    $('#Total').val('');
    $('#Total').val(parseFloat(Total)+parseFloat(Subtotal));

  var btnedit =  "<a class='btn btn-warning btn-xs PilihEdit' title='Edit Item'>  <span class=' fa fa-edit' ></span></a> <a class='btn btn-danger btn-xs Hapus' title='Remove Item'>  <span class=' fa fa-close' ></span> </a>" 

  if (iditemNew == '') {
        var row = t.row.add( [
            iditem,
            itemcode,
            Description,
            Qty,
            Unit,
            Price,
            Subtotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."),
            btnedit,
            Stok
        ] ).draw(false);
        t.row(row).column(0).nodes().to$().addClass('No');
        t.row(row).column(1).nodes().to$().addClass('ItemCode');
        t.row(row).column(2).nodes().to$().addClass('Description');
        t.row(row).column(3).nodes().to$().addClass('Qty');
        t.row(row).column(4).nodes().to$().addClass('Unit');
        t.row(row).column(5).nodes().to$().addClass('Price');
        t.row(row).column(6).nodes().to$().addClass('Subtotal');
        t.row(row).column(8).nodes().to$().addClass('StokEditTd');


        $('<input>').attr({
        type: 'hidden',
        class: iditem,
        name: 'index', 
        value: iditem
        }).appendTo('form');

        $('<input>').attr({
        type: 'hidden',
        class: iditem,
        name: 'itemcode['+iditem+']', 
        value: itemcode
        }).appendTo('form');

        $('<input>').attr({
        type: 'hidden',
        class: iditem,
        name: 'Description['+iditem+']', 
        value: Description
        }).appendTo('form');

        $('<input>').attr({
        type: 'text',
        class: iditem,
        name: 'Qty['+iditem+']', 
        value: Qty//.replace(/\./g, "")
        }).appendTo('form');


         $('<input>').attr({
        type: 'hidden',
        class: iditem,
        name: 'Unit['+iditem+']', 
        value: Unit
        }).appendTo('form');

        $('<input>').attr({
        type: 'hidden',
        class: iditem,
        name: 'Price['+iditem+']', 
        value: Price.replace(/\./g, "")
        }).appendTo('form');

        $('<input>').attr({
        type: 'hidden',
        class: iditem,
        name: 'Subtotal['+iditem+']', 
        value: Subtotal.replace(/\./g, "")
        }).appendTo('form');

        $('<input>').attr({
        type: 'hidden',
        class: iditem,
        name: 'Stok['+iditem+']', 
        value: Stok
        }).appendTo('form');

        styleEditProses();
    }else{
      styleEditProses();
    }
  })

  $('#Edit').on('click', function(e){ 
    var t = $('#tbdetil').DataTable();
  var btnedit =  "<a class='btn btn-warning btn-xs PilihEdit' title='Edit Item'>  <span class=' fa fa-edit' ></span> </a> <a class='btn btn-danger btn-xs Hapus' title='Remove Item'>  <span class=' fa fa-close' ></span> </a>" 

       itemcode = $('#ItemCodeInput').val();
        iditemNew = $('#IndexNew').val();
        UnitNew = $('#UnitNew').val();
       Description = $('#Description').val();
       Qty = $('#Qty1').val();
       Price =$('#Price1').val();
       PriceHitung =Price.replace(/\./g, "");
       QtyHitung = Qty;//.replace(/\./g, "");
       Subtotal = PriceHitung*QtyHitung;
       Total = $('#Total').val();
       Stok = $('#StokAdd').val();
       $('#Total').val('');
        $('#Total').val(parseFloat(Total)+parseFloat(Subtotal));

       if (iditemNew != '') {
        var index = $('#index').val();
        iditem = parseInt(index)+1;;
        Unit = UnitNew;
        $('#index').val(iditem);
       };

        if (itemcode=='' ||itemcode==null  ) {
         File_Kosong()
        }else if (Qty=='' || parseFloat(Qty)<=0) {
         File_Kosong()
        }else{

        var row = t.row.add( [
            iditem,
            itemcode,
            Description,
            Qty,
            Unit,
            Price,
            Subtotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."),
            btnedit,
            Stok
        ] ).draw(false);
        t.row(row).column(0).nodes().to$().addClass('No');
        t.row(row).column(1).nodes().to$().addClass('ItemCode');
        t.row(row).column(2).nodes().to$().addClass('Description');
        t.row(row).column(3).nodes().to$().addClass('Qty');
        t.row(row).column(4).nodes().to$().addClass('Unit');
        t.row(row).column(5).nodes().to$().addClass('Price');
        t.row(row).column(6).nodes().to$().addClass('Subtotal');
        t.row(row).column(8).nodes().to$().addClass('StokEditTd');


        $('<input>').attr({
        type: 'hidden',
        class: iditem,
        name: 'index', 
        value: iditem
        }).appendTo('form');

        $('<input>').attr({
        type: 'hidden',
        class: iditem,
        name: 'itemcode['+iditem+']', 
        value: itemcode
        }).appendTo('form');

        $('<input>').attr({
        type: 'hidden',
        class: iditem,
        name: 'Description['+iditem+']', 
        value: Description
        }).appendTo('form');

        $('<input>').attr({
        type: 'hidden',
        class: iditem,
        name: 'Qty['+iditem+']', 
        value: Qty//.replace(/\./g, "")
        }).appendTo('form');


         $('<input>').attr({
        type: 'hidden',
        class: iditem,
        name: 'Unit['+iditem+']', 
        value: Unit
        }).appendTo('form');

        $('<input>').attr({
        type: 'hidden',
        class: iditem,
        name: 'Price['+iditem+']', 
        value: Price.replace(/\./g, "")
        }).appendTo('form');

        $('<input>').attr({
        type: 'hidden',
        class: iditem,
        name: 'Subtotal['+iditem+']', 
        value: Subtotal
        }).appendTo('form');

        $('<input>').attr({
        type: 'hidden',
        class: iditem,
        name: 'Stok['+iditem+']', 
        value: Stok
        }).appendTo('form');
        styleEditProses();
      }
    })

 $('#tbdetil tbody').on('click', '.Hapus', function(e){
       var t = $('#tbdetil').DataTable();
      iditem = $(this).closest('tr').children('td.No').text();
      $('.'+iditem).remove();
      t.row($(this).closest('tr')).remove().draw(false);
      $('#IndexDelete').val(iditem);
      Total = $('#Total').val();
      Subtotal = $(this).closest('tr').children('td.Subtotal').text().replace(/\./g, "");;
      $('#Total').val('');
        $('#Total').val(parseFloat(Total)-parseFloat(Subtotal));
 })



  </script>







<script type="text/javascript">
  $(document).ready(function(){
     $("#View").click(function(e){ 
     var idvendor = $('#IdVendor').val();        
      $.ajax({
        type: "POST",
       url: '<?php echo site_url('Purchase_order/select_pr_byvendor');?>',
        data: {idvendor: idvendor  },
        success: function(data){
            e.preventDefault();
            var mymodal = $('#myModal');
            mymodal.modal({backdrop: 'static', keyboard: false}) 
            var height = $(window).height() - 200;
            mymodal.find(".modal-body").css("max-height", height);
            mymodal.find('.modal-body').html(data);
            mymodal.modal('show');            
        }
      });
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function(){
     $("#ItemCodeInput").click(function(e){ 
     var idvendor = $('#IdVendor').val();        
      $.ajax({
        type: "POST",
       url: '<?php echo site_url('Purchase_order/daftar_item');?>',
        data: {idvendor: idvendor,dari:'POD'  },
        success: function(data){
            e.preventDefault();
            var mymodal1 = $('#myModalNew');
            mymodal1.modal({backdrop: 'static', keyboard: false}) 
            var height = $(window).height() - 200;
            mymodal1.find(".modal-body").css("max-height", height);
            mymodal1.find('.modal-body').html(data);
            mymodal1.modal('show');            
        }
      });
    });
  });
</script>

  <script type="text/javascript">
function styleEdit() {
        $('#View').addClass('disabled')
        $('#simpan1').addClass('disabled');
        $('#Batal').removeClass('disabled');
        $('#Edit').removeClass('disabled');
        $('.PilihEdit').addClass('disabled');
        $('#divItemCode').addClass('has-success');
        $('#divDescription').addClass('has-success');
        $('#divQty').addClass('has-success');
        $('#divPrice').addClass('has-success');
        $('#IndexNew').val('');
}
function styleEditProses() {
        $('#View').removeClass('disabled');;
        $('#simpan1').removeClass('disabled');
        $('#Batal').addClass('disabled');
        $('#Edit').addClass('disabled');
        $('.PilihEdit').removeClass('disabled');
        $('#divItemCode').removeClass('has-success');
        $('#divDescription').removeClass('has-success');
        $('#divQty').removeClass('has-success');
        $('#divPrice').removeClass('has-success');
        
        $('#Id').val('');
        $('#ItemCodeInput').val('');
        $('#Description').val('');
        $('#Qty').val('');
        $('#Qty1').val('');
        $('#Price').val('');
        $('#Price1').val('');
        $('#IndexNew').val('');
}
  </script>






  <script>
  $(function () {
    $(".select2").select2();
     $(".select3").select2();

  });
  </script>
  <script type="text/javascript">
$('#tbdetil').dataTable({
    "paging": false,
    "ordering": false,
    "searching": false
});
  </script>



   <script type="text/javascript">
  function File_Kosong() {
  $.alert({
    title: 'Caution!!',
    content: 'Invalid Data!',
    icon: 'fa fa-warning',
    type: 'orange',
});
}
</script>

<script>
  $( function() {
    $( "#DueDate" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });
    $( "#ExpetedDate" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });


  });
  </script>

<script type="text/javascript">
   $(document).on('change', '#IdVendor', function(){
    var NamaVen =$("#IdVendor option:selected").text(); 
    $("#Vendor").val('');  
      $("#Vendor").val(NamaVen);
      $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });

    });
</script>
<script type="text/javascript">
   $(document).on('change', '#IdDepartement', function(){
    var NamaVen =$("#IdDepartement option:selected").text(); 
    $("#NamaDepartemen").val('');  
      $("#NamaDepartemen").val(NamaVen);
    });
</script>

<script type="text/javascript">
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });
</script>


  <?php if($this->session->flashdata('kodero')): ?>
<?php $koderocetak =  $this->session->flashdata('kodero');?> 
<script type="text/javascript">
  $(document).ready(function() {
    var kodero = "<?php echo $koderocetak;?>"
    
          window.open("<?php echo base_url(). 'index.php/Purchase_order/CetakMI/';?>?NoRo="+kodero ,"MyTargetWindowName")
  });
  </script>
  <?php endif; ?>



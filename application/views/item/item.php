<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>

<style type="text/css">
 /* .No{
    display : none;
}

}*/

.StokEditTd{
    display : none;
}
.modal.modal-wide .modal-dialog {
  width: 80%;

.modal-wide .modal-body {
  overflow-y: auto;
}


#tallModal .modal-body p { margin-bottom: 900px }
.pull-right {
    float: right;
}

.pull-left {
    float: left;
}

</style>

<section class="content-header">
    <div class="btn-group btn-breadcrumb">
            <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
            <a  class="btn btn-default  btn-xs active">Item</a>
        </div>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Item</h3>
          <a style="float:right"  href="<?php echo base_url('index.php/Item/Item_add');?>" class="btn btn-primary">
            <i class="glyphicon glyphicon-saved"></i>
            Add Item
          </a>
        </div>
    <!-- /.box-header -->
        <div class="box-body">
        <div class="table-responsive">
          <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr >
                                        <th>Item Code</th>
                                        <th>Sub Category</th>
                                        <th>Description</th>
                                        <th>Unit</th>
                                        <th>Price (IDR)</th>
                                        <th>Stock</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach($data as $u){

                                    ?>
                                    <tr  class='odd gradeX context'>
                                        <td class="codeitem"><?php echo $u->codeitem ?></td>
                                        <td><?php echo $u->subcategory?></td>
                                        <td><?php echo $u->description?></td>
                                        <td><?php echo $u->unit?></td>
                                        <td><?php echo number_format($u->price, 0, '.', '.') ?></td>
                                        <td><?php echo $u->stok?></td>
                                         <td align="center">
                                         <a class="btn btn-warning btn-xs" title="Edit Data"  href="<?php echo base_url('index.php/Item/Item_edit/'.$u->codeitem); ?>">  <span class="fa fa-fw fa-edit" ></span> </a>
                                        <a class="btn btn-info btn-xs ViewDetail" title="View History Price"   style="cursor:pointer" >  <span class="fa fa-fw fa-eye " ></span> </a>
                                        <a class="btn btn-danger btn-xs" title="Edit Data"  href="<?php echo base_url('index.php/Item/Item_edit_akun/'.$u->codeitem); ?>">  <span class="fa fa-fw fa-edit" ></span> </a>

                                         <a class="btn btn-success btn-xs" title="Edit Data"  href="<?php echo base_url('index.php/Item/Item_edit_aset/'.$u->codeitem); ?>">  <span class="fa fa-fw fa-edit" ></span> </a>


                                        </td>






                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
          </div>

           <div class="modal modal-wide fade" id="myModal" role="dialog" style="display:none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" title="close" data-dismiss="modal">&times;</button>
                    <h4 align="center" class="modal-title">History Item Price</h4>
                </div>
                <div class="modal-body">   
                </div>
            </div>
        </div>     
    </div>



        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
  </div><!-- /.row -->

</section><!-- /.content -->


<?php
$this->load->view('template/Js');
$this->load->view('template/Foot');
?>

<script type="text/javascript">
  $(document).ready(function(){
     $(".ViewDetail").click(function(e){ 
     var codeitem =  $(this).closest('tr').children('td.codeitem').text();      
      $.ajax({
        type: "POST",
       url: '<?php echo site_url('Item/view_history_priceitem');?>',
        data: {codeitem: codeitem  },
        success: function(data){
            e.preventDefault();
            var mymodal = $('#myModal');
            mymodal.modal({backdrop: 'static', keyboard: false}) 
            var height = $(window).height() - 200;
            mymodal.find(".modal-body").css("max-height", height);
            mymodal.find('.modal-body').html(data);
            mymodal.modal('show');            
        }
      });
    });
  });
</script>

<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>


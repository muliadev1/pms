

 <style type="text/css">
    @media print {
       body {
        -webkit-print-color-adjust: exact;
      }
      #warna{
        -webkit-print-color-adjust: exact;
      }
      @page {
        size: 300px 442px; 
        margin:0px;
        }
    }
  </style>

 
  
  <body onload="printDiv('printableArea')" id="printableArea">
        <table  style="margin: auto;" border="0" width="100%" align="center">
          <tbody>
            <tr>
              <th  style="padding-right: 10px;" width="10%" rowspan="4" ><img height="30px" src="<?php echo base_url().'assets/img-sistem/logo-laporan.png'; ?>"> </th> 
              <th width="60%" align="center" style="font-size:16px" ><strong>RESTAURANT</strong> <br> 
                <font style="font-size: 15px;"></font> SHANTI </th>
              
         </tr>
            <tr>
             <td align="center" style="font-size:10px;"> Phone : 288060-289146 </td>
            </tr>
            <tr>
              <td align="center" style="font-size:10px;"> </td>
            </tr>
            <tr>
              <td align="center" style="font-size:10px;"> </td>
            </tr>
            <tr>
              <td colspan="3" > <hr style="color: '#000066'; ">  </td>
            </tr>
          </tbody>
          </table>  


           <table  border="0" style="border-collapse: collapse; border-color:#000000; font-size: 9px;" width="100%" align="center">
            <tr>
              <th align="center" colspan="2">RESTAURANT ORDER </th>
            </tr>
             <tr>
              <th align="center" colspan="2">&nbsp; </th>
            </tr>
             <tr>
              <th align="left">ORDER NO</th>
              <th align="left">: <?php echo $detilmenu[0]->ordernomanual ?></th>
            </tr>
            <tr>
              <th align="left">DATE</th>
              <th align="left">: <?php echo $mastermenu->tanggaltrx ?></th>
            </tr>
            <tr>
              <th align="left">ROOM NO</th>
              <th align="left">: <?php echo $mastermenu->roomname ?></th>
            </tr>
            <tr>
              <th align="left">PERSONS</th>
              <th align="left">: <?php echo $mastermenu->nama ?></th>
            </tr>
            <tr>
              <th align="left">TABLE NO</th>
              <th align="left">: <?php echo $mastermenu->tablenumber ?></th>
            </tr>
            <tr>
              <th align="left">WAITER NO</th>
              <th align="left">: <?php echo $detilmenu[0]->idwaiter ?></th>
            </tr>
             <tr>
               <td align="center" colspan="2"> <hr style="color: '#000000'; "></td>
            </tr>
           </table>

           <table  border="0" style="border-collapse: collapse; border-color:#000000; font-size: 8px;" width="100%" align="center">
            <tr>
              <td> <strong> FOOD</strong></td>
              <td>&nbsp;</td>
            </tr>
            <?php $dataFood = $this->M_resto->Get_trx_menu_detail_category_cetak($detilmenu[0]->trxid,'FOOD') ?>
            <?php foreach ($dataFood as $food ){ ?>
              <tr>
                <td><?php echo $food->menu; ?></td>
                <td ><?php echo number_format($food->qty); ?></td>
                 <td ><?php echo number_format($food->subtotal); ?></td>
              </tr>
            <?php } ?>

            <tr>
              <td><strong>BEVERAGE</strong></td>
              <td>&nbsp;</td>
            </tr>
             <?php $dataBev = $this->M_resto->Get_trx_menu_detail_category_cetak($detilmenu[0]->trxid,'BEVERAGE') ?>
            <?php foreach ($dataBev as $bev ){ ?>
              <tr>
                <td><?php echo $bev->menu; ?></td>
                <td ><?php echo number_format($bev->qty); ?></td>
                 <td ><?php echo number_format($bev->subtotal); ?></td>
              </tr>
            <?php } ?>
          <tr>
            <tr>
              <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
              <td  colspan="3"> <hr style="color: '#000066'; "></td>
            </tr>
           </table>


            <table  border="0" style="border-collapse: collapse; border-color:#000000; font-size: 9px;" width="100%" align="center">
            <tr>
              <th align="left">NO BILL</th>
              <th align="left">CASHIER SIGNATURE</th>
            </tr>
             <tr>
            <tr>
              <th colspan="2">&nbsp;</th>
            </tr>
            <tr>
              <th colspan="2">&nbsp;</th>
            </tr>
            <tr>
              <th colspan="2">&nbsp;</th>
            </tr>
            <tr>
              <th align="left">-------------------------------</th>
              <th align="left">--------------------------------</th>
            </tr>
            
           </table>



    
</body>


<script>
function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}
</script>

 



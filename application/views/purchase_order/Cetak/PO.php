
<?php echo $header?>

<style type="text/css">
  #TabelKonten tr td {
    padding-right: 7px;
    padding-left:  7px;
    font-size: 12px;
  }
</style>

<div style="margin-buttom : 15px;" >
<table width="100%" border="0"  >
    <tr>
     <td colspan="2" align="center" style="font-size:14px;"> <strong> PURCHASE ORDER  </strong>  </td>
    </tr>
    
   
  </table>
<table width="100%" border="0" style="font-size:11px;"  >
  <tr>
     <td width="10%"><strong>NO</strong> </td>
     <td align="left">: <?php echo $kontenPO[0]->kodepo;?></td>
     <td align="left">&nbsp;</td>
    </tr>
    <tr>
     <td><strong>Date</strong></td>
     <td align="left">: <?php echo $kontenPO[0]->dateorder;?></td>
    <td align="right"><strong> Vendor : <?php echo $kontenPO[0]->vendor;?></strong></td>
    </tr>
    <tr>
     <td>&nbsp;</td>
     <td align="left">&nbsp;</td>
      <td align="right">&nbsp;</td>
    </tr>
  </table>
</div>

<table id="TabelKonten"  border="1" style="border-collapse: collapse; border-color:#000000; margin-bottom : 130px;"  width="100%"   >
  <tbody>
    <tr>
      <th rowspan="2" width="5%" scope="col">NO</th>
      <th rowspan="2" width="30%" scope="col">Description</th>
      <th rowspan="2"  width="10%" scope="col">Qty</th>
      <th height="35" width="20%" colspan="2" scope="col">Price</th>
      <th rowspan="2" width="25%" scope="col">Remark</th>
    </tr>
    <tr>
      <td align="center" width="15%"><strong>Unit</strong> </td>
      <td align="center" width="15%"><strong>Total</strong></td>
    </tr>
    <?php $i=1;$GrandTotal=0; foreach ($kontenPO as $pek ) { ?>
    <tr>
      <td><?php echo $i?></td>
      <td><?php echo $pek->description ?></td>
       <td align="right"><?php echo number_format($pek->qtyorder, 2, '.', '.') ?></td>
      <td align="right"><?php echo number_format($pek->price, 0, '.', '.')?></td>
      <td align="right"><?php echo number_format($pek->qtyorder*$pek->price  , 0, '.', '.') ?></td>
      <td align="center"><?php echo $pek->notedetil ?></td>
    </tr>
   <?php $i++; $GrandTotal+=$pek->qtyorder*$pek->price; } ?>
  </tbody>
  <tfoot>
    <tr style="background-color:#E6EBE6;">
      <td colspan="4"><strong>TOTAL</strong></td>
      <td align="right"> <strong ><?php echo number_format($GrandTotal, 0, '.', '.') ?></strong></td>
       <td>&nbsp;</td>
    </tr>
  </tfoot>
</table>



<table width="100%" border="0" style="font-size:11px; "  >
    <tr>
      <td >&nbsp; </td>
      <td >&nbsp; </td>     
      <td >&nbsp; </td>
    </tr>
     <tr>
      <td >&nbsp; </td>
      <td >&nbsp; </td>     
      <td >&nbsp; </td>
    </tr>
    
    <tr>
     <td align="center"><strong>PURCHASING</strong> </td>
     <td  align="center"><strong>APPROVED</strong></td>
     <td  align="center"><strong>DEPARTEMENT</strong></td>
    </tr>
    <tr>
       <td >&nbsp; </td>
      <td >&nbsp; </td>     
      <td >&nbsp; </td>
    </tr>
    <tr>
      <td >&nbsp; </td>
      <td >&nbsp; </td>     
      <td >&nbsp; </td>
    </tr>
    <tr>
       <td >&nbsp; </td>
      <td >&nbsp; </td>     
      <td >&nbsp; </td>
    </tr>
     <tr>
      <td >&nbsp; </td>
      <td >&nbsp; </td>     
      <td >&nbsp; </td>
    </tr>
    <tr>
      <td >&nbsp; </td>
      <td >&nbsp; </td>     
      <td >&nbsp; </td>
    </tr>
     <tr>
     <td align="center">&nbsp; <hr style="color:#000000"> </td>
     <td align="center"><?php echo $kontenPO[0]->namaveri ?> <hr style="color:#000000"></td>
     <td align="center">&nbsp; <hr style="color:#000000"></td>
    </tr>
   
   
  </table>


  






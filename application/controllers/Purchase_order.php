<?php 

class Purchase_order extends CI_Controller{
	private $IdOperator;
	private $Operator;
	private $IdAset;

	function __construct(){
		parent::__construct();
		if($this->session->userdata('Kode_userPms')==NULL) {
           redirect('Login'); 		 

		}
		$this->load->library(array('session','akuntan_function','create_pdf','ciqrcode'));
		$this->load->model(array('M_purchase_order','M_aset','M_purchase_requisition','M_receive_order','M_store_requisition','M_rbac','M_payment','M_stok_opname'));
		$this->IdOperator = $this->session->userdata('Kode_userPms');
		$this->Operator = $this->session->userdata('Nama_userPms');
		$this->IdAset = $this->session->userdata('KodeKategoriAset');
		
		if (!empty($this->session->userdata("active_db"))) {
			$this->load->database($this->session->userdata("active_db"), FALSE, TRUE);
		}
	}

	function index(){
		$this->load->view('dashboard1');
	}

	function purchase_order_add(){
		$data['data'] = $this->M_purchase_order->load_vendor_all();
		$this->load->view('purchase_order/Purchase_order',$data);
	}
	function realisasi_po_view(){
		$data['datavendor'] = $this->M_purchase_order->load_vendor_all();
		$this->load->view('purchase_order/Realisasi_po',$data);
	}
	function realisasi_po_view_data(){
		$IdVendor = $this->input->post('IdVendor');
		$DateFromRpo = $this->input->post('DateFromRpo');
		$DateToRpo = $this->input->post('DateToRpo');

	   $DateFromRpo1 = DateTime::createFromFormat('m/d/Y',$DateFromRpo);
	   $DateFromRpo2 = $DateFromRpo1->format("Y-m-d");
	   $DateToRpo1 = DateTime::createFromFormat('m/d/Y',$DateToRpo);
	   $DateToRpo2 = $DateToRpo1->format("Y-m-d");

		$data['periode'] =  array('TglAwal' => $DateFromRpo2 , 'TglAkhir' => $DateToRpo2,'IdVendor' => $IdVendor);
		$data['konten'] = $this->M_purchase_order->load_realisasi_po($IdVendor, $DateFromRpo2,$DateToRpo2);
		$this->load->view('purchase_order/Realisasi_po_data',$data);
	}
	function realisasi_po_view_data_detil(){
		$kodepo = $this->input->post('kodepo');
	 // print_r($kodepo);exit();

		$data['periode'] =  array('TglAwal' => $DateFromRpo2 , 'TglAkhir' => $DateToRpo2,'IdVendor' => $IdVendor);
		$data['konten'] = $this->M_purchase_order->load_realisasi_po_detil($kodepo);
		$this->load->view('purchase_order/Realisasi_po_data_detil',$data);
	}
	function purchase_order_edit($kodepo){
		$data['master'] = $this->M_purchase_order->Load_po_master($kodepo); 
		$data['detil'] = $this->M_purchase_order->Load_po_detil($kodepo); 
		$data['data'] = $this->M_purchase_order->load_vendor_all();
		$this->load->view('purchase_order/Purchase_order_edit',$data);
	}
	function purchase_order_list(){
		$data['data'] = $this->M_purchase_order->load_po_all();
		$this->load->view('purchase_order/Cetak/Daftar_po',$data);
	}

	function purchase_order_list_veri(){
		$data['data'] = $this->M_purchase_order->load_po_all_veri();
		$this->load->view('purchase_order/Purchase_order_veri',$data);
	}
	function purchase_order_list_aktifkan(){
		$this->load->view('purchase_order/Purchase_order_aktifkan');
	}
	function purchase_order_list_hapus(){
		$this->load->view('purchase_order/Purchase_order_hapus');
	}
	function purchase_order_list_aktifkan_data(){
		$TglAwal = $this->input->post('TglAwal');
		$TglAkhir = $this->input->post('TglAkhir');

	    $TglAwal1 = DateTime::createFromFormat('m/d/Y',$TglAwal);
	    $TglAwal2 = $TglAwal1->format("Y-m-d");
	    $TglAkhir1 = DateTime::createFromFormat('m/d/Y',$TglAkhir);
	    $TglAkhir2 = $TglAkhir1->format("Y-m-d");
		
		$data['data'] = $this->M_purchase_order->load_po_cheked($TglAwal2, $TglAkhir2);
		$this->load->view('purchase_order/Purchase_order_aktifkan_data',$data);
	}

	function purchase_order_list_hapus_data(){
		$TglAwal = $this->input->post('TglAwal');
		$TglAkhir = $this->input->post('TglAkhir');

	    $TglAwal1 = DateTime::createFromFormat('m/d/Y',$TglAwal);
	    $TglAwal2 = $TglAwal1->format("Y-m-d");
	    $TglAkhir1 = DateTime::createFromFormat('m/d/Y',$TglAkhir);
	    $TglAkhir2 = $TglAkhir1->format("Y-m-d");
		
		$data['data'] = $this->M_purchase_order->load_po_delete($TglAwal2, $TglAkhir2);
		$this->load->view('purchase_order/Purchase_order_hapus_data',$data);
	}
	
	
	function purchase_order_list_veri_after(){
		$data['data'] = $this->M_purchase_order->load_po_all_veri();
		$this->load->view('purchase_order/Purchase_order_veri_after',$data);
	}

	function purchase_order_list_veri_detil(){
		$kode =$this->input->post('kode');
		$data['data'] = $this->M_purchase_order->load_po_all_veri_detil($kode);
		$this->load->view('purchase_order/Purchase_order_list_detil',$data);
	}

	function purchase_order_add_setujui(){
		$kode = $this->input->post('kode');
		$data = array(
			'status' =>1,
			'verifikatorby' => $this->Operator
	);
		$this->M_purchase_order->purchase_order_add_setujui($data,$kode);
		$this->session->set_flashdata('msg', 'Data Berhasil Disimpan...');
		$link =  site_url('Purchase_order/purchase_order_list_veri'); 		
		echo $link;
	}

	function purchase_order_add_aktifkan(){
		$kode = $this->input->post('kode');
		$data = array('ischecked' =>0);
		$this->M_purchase_order->purchase_order_add_aktifkan($data,$kode);
		$this->session->set_flashdata('msg', 'Data Berhasil Disimpan...');
	}

	function purchase_order_add_hapus(){
		$kode = $this->input->post('kode');
		$this->M_purchase_order->purchase_order_add_delete($kode);
		$this->session->set_flashdata('msg', 'Data Berhasil Disimpan...');
	}


	function purchase_order_add_revisi(){
		$kode = $this->input->post('kode');
		$catatan = $this->input->post('catatan');
		$data = array(
			'status' =>2,
			'verifikatorby' => $this->Operator,
			'catatan' => $catatan
	);
		$this->M_purchase_order->purchase_order_add_setujui($data,$kode);
		$this->session->set_flashdata('msg', 'Data Berhasil Disimpan...');
		$link =  site_url('Purchase_order/purchase_order_list_veri'); 		
		echo $link;
	}

	function purchase_order_direct_add(){
		$data['data'] = $this->M_purchase_order->load_vendor_all();
		$data['dataDepaetemen'] = $this->M_purchase_order->load_departement();
		$this->load->view('purchase_order/Purchase_order_direct',$data);
	}

	function purchase_requisition_view(){
		$data['data'] = $this->M_purchase_order->pr_view();
		$this->load->view('purchase_requisition/Purchase_requisition_list',$data);
	}

	function purchase_requisition_view_detail(){
		$id = $this->input->post('id');
		$data['data'] = $this->M_purchase_order->pr_view_detil($id);
		$this->load->view('purchase_requisition/Purchase_requisition_detail',$data);
	}

	function add_po(){
		$Total = 0;
		date_default_timezone_set('Asia/Hong_Kong');
		$dateexpected = $this->input->post('ExpetedDate');
		$dateexpected1 = DateTime::createFromFormat('m/d/Y',$dateexpected);
		$dateexpected2 = $dateexpected1->format("Y-m-d");

		$itemcode = $this->input->post('itemcode');
		$Qty = $this->input->post('Qty');
		$Description = $this->input->post('Description');
		$Note = $this->input->post('Note');
		$Unit = $this->input->post('Unit');
		$idvendor = $this->input->post('IdVendor'); 
		$vendor = $this->input->post('Vendor');
		$Price = $this->input->post('Price');
		$Subtotal = $this->input->post('Subtotal');
		$Stok = $this->input->post('Stok');
		$kode =$this->M_purchase_order->get_kodepo('tbpo');
		$NotePo = $this->input->post('NotePo');

		$itemcode1 = array_combine(range(1, count($itemcode)), array_values($itemcode));
		$Price1 = array_combine(range(1, count($Price)), array_values($Price));
		$Qty1 = array_combine(range(1, count($Qty)), array_values($Qty));
		$Description1 = array_combine(range(1, count($Description)), array_values($Description));
		$Unit1 = array_combine(range(1, count($Unit)), array_values($Unit));
		$Stok1 = array_combine(range(1, count($Stok)), array_values($Stok));
		$Subtotal1 = array_combine(range(1, count($Subtotal)), array_values($Subtotal));
		$Note1 = array_combine(range(1, count($Note)), array_values($Note));


				$Jumlah = count($Stok);
				for ($i=1; $i <= $Jumlah ; $i++) { 
				 	 $dataDetil[] = array('kodepo' => $kode,
						 				'codeitem' => $itemcode1[$i],
						 				'price' => $Price1[$i],
						 				'qtyorder' => $Qty1[$i],
						 				'description' => $Description1[$i],
						 				'unit' => $Unit1[$i],
						 				'qtystok' => $Stok1[$i],
						 				'note' => $Note1[$i],
						 				'subtotal' => $Subtotal1[$i]
						 		 );
				 	 	$Total+= $Subtotal1[$i];
				 } 

				// print_r( $dataDetil);exit();

		 		$data = array('kodepo' => $kode,
				 		'idvendor' => $idvendor,
				 		'vendor' => $vendor,
						'dateorder' => date('Y-m-d H:i:s'),
						'idoperator' => $this->IdOperator,
						'description' => $NotePo,
						'total' => $Total,
						'dateexpected' => $dateexpected2,
						'method' => 'CASH'	
		 			);
		 

		$this->M_purchase_order->Po_add($data,$dataDetil);
		$this->session->set_flashdata('msg', ' Add Purchase Order successfully... ');
		$this->session->set_flashdata('kodepo', $kode);
		//$this->CetakPODirect($kode);
		redirect('Purchase_order/purchase_order_add');
	}



	function add_po_edit(){
		$Total = 0;
		date_default_timezone_set('Asia/Hong_Kong');
		$dateexpected = $this->input->post('ExpetedDate');
		$dateexpected1 = DateTime::createFromFormat('m/d/Y',$dateexpected);
		$dateexpected2 = $dateexpected1->format("Y-m-d");

		$itemcode = $this->input->post('itemcode');
		$Qty = $this->input->post('Qty');
		$Description = $this->input->post('Description');
		$Note = $this->input->post('Note');
		$Unit = $this->input->post('Unit');
		$idvendor = $this->input->post('IdVendor'); 
		$vendor = $this->input->post('Vendor');
		$Price = $this->input->post('Price');
		$Subtotal = $this->input->post('Subtotal');
		$Stok = $this->input->post('Stok');
		$NotePo = $this->input->post('NotePo');//print_r($NotePo);exit();
		$kode = $this->input->post('kodepoEdit');

		$itemcode1 = array_combine(range(1, count($itemcode)), array_values($itemcode));
		$Price1 = array_combine(range(1, count($Price)), array_values($Price));
		$Qty1 = array_combine(range(1, count($Qty)), array_values($Qty));
		$Description1 = array_combine(range(1, count($Description)), array_values($Description));
		$Unit1 = array_combine(range(1, count($Unit)), array_values($Unit));
		$Stok1 = array_combine(range(1, count($Stok)), array_values($Stok));
		$Subtotal1 = array_combine(range(1, count($Subtotal)), array_values($Subtotal));
		$Note1 = array_combine(range(1, count($Note)), array_values($Note));


				$Jumlah = count($Stok);
				for ($i=1; $i <= $Jumlah ; $i++) { 
				 	 $dataDetil[] = array('kodepo' => $kode,
						 				'codeitem' => $itemcode1[$i],
						 				'price' => $Price1[$i],
						 				'qtyorder' => $Qty1[$i],
						 				'description' => $Description1[$i],
						 				'unit' => $Unit1[$i],
						 				'qtystok' => $Stok1[$i],
						 				'note' => $Note1[$i],
						 				'subtotal' => $Subtotal1[$i]
						 		 );
				 	 	$Total+= $Subtotal1[$i];
				 } 

				// print_r( $dataDetil);exit();

		 		$data = array(
				 		'idvendor' => $idvendor,
				 		'vendor' => $vendor,
						'dateorder' => date('Y-m-d H:i:s'),
						'idoperator' => $this->IdOperator,
						'description' => $NotePo,
						'total' => $Total,
						'dateexpected' => $dateexpected2,
						'method' => 'CASH'	
		 			);
		 		//print_r($data);exit();
		 

		$this->M_purchase_order->Po_edit($data,$dataDetil,$kode);
		$this->session->set_flashdata('msg', ' Add Purchase Order successfully... ');
		$this->session->set_flashdata('kodepo', $kode);
		//$this->CetakPODirect($kode);
		redirect('Purchase_order/purchase_order_add');
	}

	function CetakPODirect($NoPO){
	    $data['header'] = $this->Cetak("Purchase_order",  $this->session->userdata('Nama_userPms'). date("d-m-Y_H-i-s"), 1);
	    $data['kontenPO'] = $this->M_purchase_order->load_po_forlaporan($NoPO);
	    $html=$this->load->view('purchase_order/Cetak/PO',$data, TRUE);
	    //print_r($html);exit();
     	$this->create_pdf->load($html,'Purchase_order'.'-'.$data['kontenPO'][0]->kodepo, 'A4-P');	
	}

	function add_po_direct(){
		$Total = 0;
		date_default_timezone_set('Asia/Hong_Kong');
		$DueDate = $this->input->post('DueDate');
		$DueDate1 = DateTime::createFromFormat('m/d/Y',$DueDate);
		$DueDate2 = $DueDate1->format("Y-m-d");
		$itemcode = $this->input->post('itemcode');
		$Qty = $this->input->post('Qty');
		$Description = $this->input->post('Description');
		$Note = $this->input->post('Note');
		$Unit = $this->input->post('Unit');
		$idvendor = $this->input->post('IdVendor'); 
		$vendor = $this->input->post('Vendor');
		$Price = $this->input->post('Price');
		$Subtotal = $this->input->post('Subtotal');
		$Stok = $this->input->post('Stok');
		$kode =$this->M_purchase_order->get_kodepo('tbpo');
		$NotePo = 'By System ';//$this->input->post('NotePo');
		$noreceipt =$this->input->post('NoReceipt');
		$discount =$this->input->post('Disc');
		$iscredit =$this->input->post('IsCash');
		$dp =$this->input->post('DP');
		$remain =$this->input->post('Remain');
		$IdDepartement = $this->input->post('IdDepartement'); 
		$NamaDepartemen = $this->input->post('NamaDepartemen');
		//0 cash ,1 cek
		if ($iscredit=='1') {
			$isinvoicing ='1';
			$ispaid = '0';
			$method = 1;
		}else{
			$isinvoicing ='0';
			$ispaid = '0';
			$method = 0;
		}

		 

		$kodePr =$this->M_purchase_requisition->get_kodepr('tbpr');
		$kodePo =$this->M_purchase_order->get_kodepo('tbpo');
		$kodeRo =$this->M_receive_order->get_kodero('tbro');
		$kodeVoucher =$this->M_payment->get_kodevoucher('tbpayment');
		$kodeSr =$this->M_store_requisition->get_kodesr('tbsr');


		$itemcode1 = array_combine(range(1, count($itemcode)), array_values($itemcode));
		$Price1 = array_combine(range(1, count($Price)), array_values($Price));
		$Qty1 = array_combine(range(1, count($Qty)), array_values($Qty));
		$Description1 = array_combine(range(1, count($Description)), array_values($Description));
		$Unit1 = array_combine(range(1, count($Unit)), array_values($Unit));
		$Stok1 = array_combine(range(1, count($Stok)), array_values($Stok));
		$Subtotal1 = array_combine(range(1, count($Subtotal)), array_values($Subtotal));



		 		//PR
		 		$dataPr = array('kodepr' => $kodePr,
	 				 		'iddepartement' => $IdDepartement,
	 				 		'departement' => $NamaDepartemen,
	 						'dateadd' => date('Y-m-d H:i:s'),
	 						'idoperator' => $this->IdOperator,
	 						'description' => $NotePo	
	 		 	);
				$Jumlah = count($Stok);
				for ($i=1; $i <= $Jumlah ; $i++) { 
				 	 $dataDetilPr[] = array('kodepr' => $kodePr,
						 				 'iddepartement' => $IdDepartement,
						 				 'departement' => $NamaDepartemen,
						 				'codeitem' => $itemcode1[$i],
						 				'price' => $Price1[$i],
						 				'qtyorder' => $Qty1[$i],
						 				'description' => $Description1[$i],
						 				'unit' => $Unit1[$i],
						 				'currstok' => $Stok1[$i],
						 				'idoperator' => $this->IdOperator
						 		 );
				 }

			//PO
				$Jumlah = count($Stok);
				for ($i=1; $i <= $Jumlah ; $i++) { 
				 	 $dataDetil[] = array('kodepo' => $kodePo,
						 				'codeitem' => $itemcode1[$i],
						 				'price' => $Price1[$i],
						 				'qtyorder' => $Qty1[$i],
						 				'description' => $Description1[$i],
						 				'unit' => $Unit1[$i],
						 				'qtystok' => $Stok1[$i],
						 				'note' => $NotePo,
						 				'subtotal' => $Subtotal1[$i]
						 		 );
				 	 	$Total+= $Subtotal1[$i];
				 } 

		 		$data = array('kodepo' => $kodePo,
				 		'idvendor' => $idvendor,
				 		'vendor' => $vendor,
						'dateorder' => date('Y-m-d H:i:s'),
						'idoperator' => $this->IdOperator,
						'description' => $NotePo,
						'total' => $Total,
						'dateexpected' => date('Y-m-d H:i:s'),
						'method' => $iscredit,
						'ischecked' => 1	
		 			);

				//RO
				 $totalafterdisc = $Total - $discount;
				$Jumlah = count($Stok);
				for ($i=1; $i <= $Jumlah ; $i++) { 
				 	 $dataDetilRO[] = array('kodero' => $kodeRo,
						 				'itemcode' => $itemcode1[$i],
						 				'price' => $Price1[$i],
						 				'qtyorder' => $Qty1[$i],
						 				'qty' => $Qty1[$i],
						 				'description' => $Description1[$i],
						 				'unit' => $Unit1[$i],
						 				'stok' => $Stok1[$i],
						 				'subtotal' => $Subtotal1[$i]

						 		 );
				 } 

		 		$dataRO = array('kodero' => $kodeRo,
				 		'kodepo' => $kodePo,
				 		'vendor' => $vendor,
						'datereceive' => date('Y-m-d H:i:s'),
						'idvendor' => $idvendor,
						'vendor' => $vendor,
						'idoperator' => $this->IdOperator,
						'operator' => $this->Operator,
						'noreceipt' => $noreceipt,
						'isinvoicing'=> $isinvoicing,
						'ispaid'=> $ispaid,
						'total' => $Total,
						'disc' => $discount,
						'totalafterdisc' => $totalafterdisc,	
						'iscredit' => $iscredit,
						'duedate' => $DueDate2,
						'dp' => $dp,
						'remain' => $remain	,
						'iddepartement' => $IdDepartement	
		 			);
		 		//SR
	 			$datamastersr = array('kodesr' => $kodeSr,
	 				 		'iddepartement' => $IdDepartement,
	 				 		'departement' => $NamaDepartemen,
	 						'dateadd' => date('Y-m-d H:i:s'),
	 						'idoperator' => $this->IdOperator,
	 						'description' => 'Transfer Direct By System'	
	 		 			);



		
				$Jumlah = count($Stok);
				for ($i=1; $i <= $Jumlah ; $i++) { 
				 	 $dataDetilsr[] = array('kodesr' => $kodeSr,
						 				 'iddepartement' => $IdDepartement,
						 				 'departement' => $NamaDepartemen,
						 				'codeitem' => $itemcode1[$i],
						 				'price' => $Price1[$i],
						 				'qtyorder' => $Qty1[$i],
						 				'description' => $Description1[$i],
						 				'unit' => $Unit1[$i],
						 				'note' => 'Transfer Direct By System',
						 				'currstok' => $Stok1[$i],
						 				'idoperator' => $this->IdOperator
						 		 );
				 } 

		 		
		 		//History Price  
		 		$Jumlah = count($Stok);
				for ($i=1; $i <= $Jumlah ; $i++) { 
					$dataHPP = $this->M_purchase_order->load_price_item($itemcode1[$i]);
					$StokLamaHPP = $dataHPP->stok;

					$PriceLamaHPP= $dataHPP->price;
					$StokBaruHPP = $dataHPP->stok + $Qty1[$i];
					$PriceBaruHPP=  $Price1[$i];
					$HargaBaruHPP = (($StokLamaHPP*$PriceLamaHPP) + ($StokBaruHPP*$PriceBaruHPP)) /($StokLamaHPP+$StokBaruHPP);
						$dataHistoryPrice[] = array('codeitem' => $itemcode1[$i],
						 				'price' => $priceold,
						 				'newprice' => $Price1[$i],
						 				'date' => date('Y-m-d H:i:s'),
						 				'idvendor' => $idvendor,
						 				'pricehpp' => $HargaBaruHPP,
						 				'kodero' => $kodeRo
						 		 );	
				 } 
		 		
		 		//Payment
		 		$dataPay = array('voucherno' => $kodeVoucher,
				 		'voucherdate' => date('Y-m-d H:i:s'),
				 		'vendor' => $idvendor,
						'address' => '-',
						'paymentmethod' => 'CASH' ,
						'bank' => '-',
						'checkno' => '-',
						'total' => $Total,
						'idoperator'=> $this->IdOperator		
		 			);
		 	
		 		$dataPayDetil = array('voucherno' => $kodeVoucher,
				 		'amount' => $Total,
						'description' => '-',
						'rodate' => date('Y-m-d H:i:s'),
						'kodero' => $kodeRo	
		 			);

		 		//Aset
		 		for ($i=1; $i <= $Jumlah ; $i++) { 
		 			$isaset = $this->M_purchase_order->Load_subcategory_byiditem($itemcode1[$i]);
		 			if ($isaset==1) {
		 				$codeaset = $this->M_purchase_order->Load_aset_byiditem($itemcode1[$i]); 
		 				 $dataDetilaset[] = array('codeaset' => $codeaset,
						 				 'iddepartement' => $IdDepartement,
						 				 'departement' => $NamaDepartemen,
						 				 'kondisiawal' => 'Baik',
						 				 'kondisiakhir' => 'Baik',
						 				 'qty' => $Qty1[$i],
						 				 'note' => 'IN',
						 				 'inputdate' => date('Y-m-d H:i:s'),
						 				 'idoperator' => $this->IdOperator,
						 				 'operator' => $this->Operator
						 		 );
		 				
		 			}
				 	
				 } 

		 //Modul Akuntansi Start
		$Nominal = $Total;
		$NominalWithDP = $Total-$dp;
		$BuktiTransaksi= $kodeRo;
		$KodeOperator= $this->session->userdata('Kode_userPms');
		//print_r($iscredit);exit();
		if ($iscredit=='1') {
			if ($dp>0) {
				$dataJurnal = $this->akuntan_function->generate_jurnal('1',$BuktiTransaksi,$NominalWithDP,$KodeOperator);
				$dataJurnal1 = $this->akuntan_function->generate_jurnal('3',$BuktiTransaksi,$dp,$KodeOperator);
				$dataDetilJurnal=  array($dataJurnal[1],$dataJurnal[2],$dataJurnal1[2]);
			}else{
				// $dataJurnal = $this->akuntan_function->generate_jurnal('1',$BuktiTransaksi,$Nominal,$KodeOperator);
				// $dataDetilJurnal=  array($dataJurnal[1],$dataJurnal[2]);
			}
		}else{
			// $dataJurnal = $this->akuntan_function->generate_jurnal('2',$BuktiTransaksi,$Nominal,$KodeOperator);
			// $dataDetilJurnal=  array($dataJurnal[1],$dataJurnal[2]);	
		}
		//Modul Akuntansi End

		$dataMasterJurnal = $dataJurnal[0];
		
		$this->M_purchase_requisition->Pr_add($dataPr,$dataDetilPr);
		$this->M_purchase_order->Po_add($data,$dataDetil);
		$this->M_receive_order->Ro_add($dataRO,$dataDetilRO,$kodePo,$dataHistoryPrice);

		// $this->M_payment->Payment_add('tbmasterjurnal',$dataMasterJurnal,'tbdetiljurnal',$dataDetilJurnal,
		// 							  'tbpayment',$dataPay,'tbpaymentdetil',$dataPayDetil,$iscredit);

		if ($IdDepartement!=0) {
			$this->M_store_requisition->add_transfer_stok_direct($IdDepartement,$dataDetilRO,$datamastersr,$dataDetilsr);
		}
		if (!empty($dataDetilaset)) {
			$this->M_aset->Add_detilAset_byorder('tbasetdetil',$dataDetilaset,$IdDepartement); 
		} 
		
		$this->session->set_flashdata('msg', ' Add Purchase Order successfully... ');
		$this->session->set_flashdata('kodepr', $kodePr);
		$this->session->set_flashdata('kodepo', $kodePo);
		$this->session->set_flashdata('kodero', $kodeRo);
		redirect('Purchase_order/purchase_order_direct_add');
	}


	function CetakDirect($NoRoPrint){	
	   $data['header'] = $this->Cetak("Tabungan",  $this->session->userdata('Nama_userPms'). date("d-m-Y_H-i-s"), 1);
	   $data['konten'] = $this->M_receive_order->load_MI($NoRoPrint);
	   $html=$this->load->view('purchase_order/Cetak/MI',$data, TRUE);
	    $this->create_pdf->load($html,'Memorandum_invoice'.'-'.$data['konten'][0]->kodero, 'A4-P','Print');
	
	}


	function select_pr_byvendor(){
		$idvendor = $this->input->post('idvendor');
		$data['data'] = $this->M_purchase_order->load_pr_byvendor($idvendor);
		$this->load->view('purchase_order/Purchase_requisition_list',$data);
	}

	function daftar_item(){
	$dari = $this->input->post('dari');
	if ($dari=='PO') {
		$data['dari'] =  array('dari' => 'PO');
	}elseif ($dari=='POD') {
		$data['dari'] =  array('dari' => 'POD');
	}
	$data['data'] = $this->M_purchase_order->item_view();
	$this->load->view('purchase_order/Daftar_item',$data);
	}
	function LaporanInventoryHome(){
		$data['subcategory'] = $this->M_stok_opname->load_subcategory();
		$data['departement'] = $this->M_stok_opname->loaddepartementall();
		$this->load->view('purchase_order/Cetak/Home',$data);
	}
	function LaporanccHome(){
		$data['subcategory'] = $this->M_stok_opname->load_subcategory();
		$data['category'] = $this->M_stok_opname->load_category();
		$data['departement'] = $this->M_stok_opname->loaddepartementall();
		$this->load->view('purchase_order/Cetak/Home_cc',$data);
	}

	function Select_ro(){
		$data['data'] = $this->M_receive_order->load_ro();
		$data1 = $this->load->view('purchase_order/Cetak/Daftar_ro',$data);
	}

	function Select_payment(){
		$data['data'] = $this->M_payment->load_voucher_all();
		$data1 = $this->load->view('purchase_order/Cetak/Daftar_payment',$data);
		

	}

	function Cetak($JenisLaporan,$Text){
		$data['hotel'] = $this->M_rbac->getPerusahaan();
		$data['qr'] = $this->ciqrcode->generateQRcode($JenisLaporan, $Text,1.9);
		$data1= $this->load->view('template/Cetak_head',$data, TRUE);
		return $data1;
	}


	function CetakMI($NoRoPrint){
		if (empty($NoRoPrint)) {
			$NoRo = $this->input->get('NoRo', TRUE);
		}else{
			$NoRo = $NoRoPrint;
		}
		
	   $data['header'] = $this->Cetak("Memorandum_invoice",  $this->session->userdata('Nama_userPms'). date("d-m-Y_H-i-s"), 1);
	   $data['konten'] = $this->M_receive_order->load_MI($NoRo);
	   $html=$this->load->view('purchase_order/Cetak/MI',$data, TRUE);
	    if (empty($NoRoPrint)) {
			$this->create_pdf->load($html,'Memorandum_invoice'.'-'.$data['konten'][0]->kodero, 'A4-P','');
		}else{
			$this->create_pdf->load($html,'Memorandum_invoice'.'-'.$data['konten'][0]->kodero, 'A4-P','Print');
		}
		
	}
	function CetakMI_byTgl(){
	   $TglAwal = $this->input->get('TglAwal', TRUE);
	   $TglAkhir = $this->input->get('TglAkhir', TRUE);

	   $TglAwal1 = DateTime::createFromFormat('m/d/Y',$TglAwal);
	   $TglAwal2 = $TglAwal1->format("Y-m-d");
	   $TglAwal3 = $TglAwal1->format("d-m-Y");
	   $TglAkhir1 = DateTime::createFromFormat('m/d/Y',$TglAkhir);
	   $TglAkhir2 = $TglAkhir1->format("Y-m-d");
	    $TglAkhir3 = $TglAkhir1->format("d-m-Y");

	   $data['header'] = $this->Cetak("Memorandum_invoice",  $this->session->userdata('Nama_userPms'). date("d-m-Y_H-i-s"), 1);
	   $data['konten'] = $this->M_receive_order->load_MI_byTgl($TglAwal2, $TglAkhir2);
	   $data['periode'] =  array('TglAwal' => $TglAwal3 , 'TglAkhir' => $TglAkhir3);
	   $html=$this->load->view('purchase_order/Cetak/MI_bytgl',$data, TRUE);
	  // print_r($html);exit();
	  $this->create_pdf->load($html,'Memorandum_invoice'.' '.$TglAwal3.' '.'-'.' '.$TglAkhir3, 'A4-P','');
		
		
	}

	function CetakPV(){
		$NoPay = $this->input->get('NoPay', TRUE);
	    $data['header'] = $this->Cetak("Payment_voucher",  $this->session->userdata('Nama_userPms'). date("d-m-Y_H-i-s"), 1);
	    $data['kontenVoucher'] = $this->M_payment->load_voucher($NoPay);
	    $html=$this->load->view('purchase_order/Cetak/Payment_voucher',$data, TRUE);
     	$this->create_pdf->load($html,'Payment_voucher'.'-'.$data['kontenVoucher'][0]->voucherno, 'A4-P');	
	}

	function CetakPO(){
		$NoPO = $this->input->get('NoPO', TRUE);
	    $data['header'] = $this->Cetak("Purchase_order",  $this->session->userdata('Nama_userPms'). date("d-m-Y_H-i-s"), 1);
	    $data['kontenPO'] = $this->M_purchase_order->load_po_forlaporan($NoPO);
	    $html=$this->load->view('purchase_order/Cetak/PO',$data, TRUE);
     	$this->create_pdf->load($html,'Purchase_order'.'-'.$data['kontenPO'][0]->kodepo, 'A4-P');	
	}

		function Cetak_pembelian_cat(){
	   $TglAwal = $this->input->get('Tglpem', TRUE);
	   $TglAkhir = $this->input->get('Tglpem1', TRUE);
	   $idcat = $this->input->get('idcat', TRUE);
	    $cat = $this->input->get('cat', TRUE);
	     $iddepartement = $this->input->get('iddep', TRUE);
	      $departement = $this->input->get('dep', TRUE);
	  //  print_r($departement);exit();

	   $TglAwal1 = DateTime::createFromFormat('m/d/Y',$TglAwal);
	   $TglAwal2 = $TglAwal1->format("Y-m-d");
	   $TglAwal3 = $TglAwal1->format("d-m-Y");
	   $TglAkhir1 = DateTime::createFromFormat('m/d/Y',$TglAkhir);
	   $TglAkhir2 = $TglAkhir1->format("Y-m-d");
	    $TglAkhir3 = $TglAkhir1->format("d-m-Y");

	   
	   if ( $idcat=='6') {
	   	  $data['header'] = $this->Cetak("Pembelian",  $this->session->userdata('Nama_userPms'). date("d-m-Y_H-i-s"), 1);
	     $data['konten'] = $this->M_purchase_order->Cetak_pembelian_cat($TglAwal2, $TglAkhir2,$idcat, $iddepartement);
	     $data['periode'] =  array('TglAwal' => $TglAwal3 , 'TglAkhir' => $TglAkhir3, 'Cat' => $cat,'Dep' => $departement);
	      $html=$this->load->view('purchase_order/Cetak/Pembelian_bycat_bev',$data, TRUE);
	   }elseif ( $idcat=='2') {
	     $data['header'] = $this->Cetak("Pembelian",  $this->session->userdata('Nama_userPms'). date("d-m-Y_H-i-s"), 1);
	     $data['konten'] = $this->M_purchase_order->Cetak_pembelian_cat_food($TglAwal2, $TglAkhir2,$idcat, $iddepartement);
	     $data['periode'] =  array('TglAwal' => $TglAwal3 , 'TglAkhir' => $TglAkhir3, 'Cat' => $cat,'Dep' => $departement);
	   // print_r( $data['konten']);exit();
	   	 $html=$this->load->view('purchase_order/Cetak/Pembelian_bycat_food',$data, TRUE);
	   }elseif ( $idcat=='3') {
	     $data['header'] = $this->Cetak("Pembelian",  $this->session->userdata('Nama_userPms'). date("d-m-Y_H-i-s"), 1);
	     $data['konten'] = $this->M_purchase_order->Cetak_pembelian_cat_material($TglAwal2, $TglAkhir2,$idcat);
	     $data['periode'] =  array('TglAwal' => $TglAwal3 , 'TglAkhir' => $TglAkhir3, 'Cat' => $cat,'Dep' => $departement);
	   //  print_r( $data['konten']);exit();
	   	 $html=$this->load->view('purchase_order/Cetak/Pembelian_bycat_material',$data, TRUE);
	   }


	  
	 // print_r($html);exit();
	  $this->create_pdf->load($html,'Pembelian '.$cat.' '.$TglAwal3.' '.'-'.' '.$TglAkhir3, 'A4-L','F');
		
		
	}

	






}
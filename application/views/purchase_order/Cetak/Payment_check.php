
<?php echo $header?>

<style type="text/css">
  #TabelKonten tr td {
    padding-right: 7px;
    padding-left:  7px;
    font-size: 12px;
  }
</style>

<div style="margin-buttom : 15px;" >
<table width="100%" border="0"  >
    <tr>
     <td colspan="2" align="center" style="font-size:14px;"> <strong> PAYMENT CHECK  </strong>  </td>
    </tr>
    
   
  </table>
<table width="100%" border="0" style="font-size:11px;"  >
   <tr>
     <td>&nbsp;</td>
     <td align="left">&nbsp;</td>
    </tr>
  <tr>
    <?php if ($periode['Bank']!=0){ ?>
     <td width="10%"><strong>Bank</strong> </td>
     <td align="left">: <?php echo $konten[0]->namabank;?></td>
    <?php }else{ ?>
      <td width="10%"><strong>Bank</strong> </td>
      <td align="left">: All Bank</td>
    <?php   } ?>
    </tr>
    <tr>
    <?php if ($periode['vendor']!=0){ ?>
      <td width="10%"><strong>Vendor</strong> </td>
     <td align="left">: <?php echo $konten[0]->vendor;?></td>
    <?php }else{ ?>
      <td width="10%"><strong>Vendor</strong> </td>
      <td align="left">: All Vendor</td>
    <?php   } ?>
    </tr>

    
    <?php if ($periode['format']!='-'){ ?>
     <tr>
     <td width="10%"><strong>Date From</strong> </td>
     <td align="left">: <?php echo $periode['TglDariCek'];?></td>
   </tr>
  <tr>
     <td width="10%"><strong>Date To</strong> </td>
     <td align="left">: <?php echo $periode['TglSampaiCek'];?></td>
   </tr>
    <?php }else{ ?>
    <tr>
      <td width="10%"><strong>Date To</strong> </td>
     <td align="left">: <?php echo $periode['TglSampaiCek'];?></td>
    </tr>
    <?php   } ?>
    </tr>


  
    <tr>
     <td>&nbsp;</td>
     <td align="left">&nbsp;</td>
    </tr>
    

  </table>
</div>

<table id="TabelKonten"  border="1" style="border-collapse: collapse; border-color:#000000; margin-bottom : 130px;"  width="100%"   >
    <thead  >
        <tr align="center" class="header">
          <th width="5%" >No</th>
            <th width="15%" >Date</th>
            <th width="15%">Check NO</th>
          <?php if ($periode['vendor']==0){ ?>
            <th width="35%" >Vendor</th>
          <?php } ?>

             <?php if ($periode['Bank']==0){ ?>
             <th width="15%" >Bank</th>
              <?php } ?>
            <th width="15%">Amount</th>
        </tr>
    </thead>
     <tbody>
      <?php $total=0;$no=1; foreach ($konten as $row) { ?>
    <tr>
       <td><?php echo $no; ?></td>
      <td><?php echo $row->tglinput; ?></td>
      <td><?php echo $row->checkno; ?></td>

         <?php if ($periode['vendor']==0){ ?>
          <td><?php echo $row->vendor; ?></td>
          <?php   }?>

          <?php if ($periode['Bank']==0){ ?>
          <td><?php echo $row->namabank; ?></td>
          <?php   }?>

      <td align="right"><?php echo number_format($row->amount, 0, '.', '.'); ?></td>
      <?php $total+=$row->amount;$no++;} ?>
    </tr>
    </tbody>

    <tfoot>
      <?php if ($periode['Bank']==0 and  $periode['vendor']==0 ){ ?>
       <tr style="background-color:#E6EBE6;">
         <td colspan="5" align="left"><strong>TOTAL</strong></td>
         <td align="right"><strong><?php echo number_format($total, 0, '.', '.'); ?></strong></td>
       </tr>
     <?php  }elseif($periode['Bank']!=0 and  $periode['vendor']==0 ){ ?>
       <tr style="background-color:#E6EBE6;">
         <td colspan="4" align="left"><strong>TOTAL</strong></td>
         <td align="right"><strong><?php echo number_format($total, 0, '.', '.'); ?></strong></td>
       </tr>
     <?php   }elseif($periode['Bank']==0 and  $periode['vendor']!=0 ){ ?>
       <tr style="background-color:#E6EBE6;">
         <td colspan="4" align="left"><strong>TOTAL</strong></td>
         <td align="right"><strong><?php echo number_format($total, 0, '.', '.'); ?></strong></td>
       </tr>
     <?php   }else{ ?>
       <tr style="background-color:#E6EBE6;">
         <td colspan="3" align="left"><strong>TOTAL</strong></td>
         <td align="right"><strong><?php echo number_format($total, 0, '.', '.'); ?></strong></td>
       </tr>
     <?php   }?>
    </tfoot>
     

  </table>



<table width="100%" border="0" style="font-size:11px; "  >
    <tr>
      <td >&nbsp; </td>
      <td >&nbsp; </td>     
      <td >&nbsp; </td>
    </tr>
     <tr>
      <td >&nbsp; </td>
      <td >&nbsp; </td>     
      <td >&nbsp; </td>
    </tr>
  <tr>
     <td align="center"><strong>APPROVED </strong> </td>
     <td align="center"><strong>RECEIVED </strong></td>
    </tr>
         <tr>
      <td >&nbsp; </td>     
      <td >&nbsp; </td>
    </tr>
     <tr>
      <td >&nbsp; </td>     
      <td >&nbsp; </td>
    </tr>
    <tr>
      <td >&nbsp; </td>     
      <td >&nbsp; </td>
    </tr>
     <tr>
      <td >&nbsp; </td>     
      <td >&nbsp; </td>
    </tr>
     
     <tr>
      <td align="center"><hr style="color:#000000"> </td>     
      <td align="center"><hr style="color:#000000"> </td>
    </tr>
   
  </table>


  






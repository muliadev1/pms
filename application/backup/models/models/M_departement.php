
<?php

class M_departement extends CI_Model{



	function Departement_view() {   
		$data = $this->db->query("SELECT `tb_mahasiswa`.* , tb_bidangstudi.`nama_bidstudi` FROM `tb_mahasiswa`
                      INNER JOIN `tb_bidangstudi` ON `tb_mahasiswa`.`id_bidstudi` = `tb_bidangstudi`.`id_bidstudi`");

		return $data->result();

   }

    function Mahasiswa_addDB($table,$data) {
    $this->db->trans_start();

      $this->db->insert($table,$data);
      $NIM = $this->db->insert_id();

    $dataUser = array(
      'nama_user' => $data['namaLengkap'],
      'username' => $NIM,
      'password' => $NIM,
      'parent' => 'Mahasiswa',
      'id_parent' => $NIM
      );
      $this->db->insert('tb_user',$dataUser);
      $IdUser = $this->db->insert_id();


      $dataUserRole = array(
      'id_user' => $IdUser,
      'id_role' => '2'
      );
      $this->db->insert('rbac_user_role',$dataUserRole);


         $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else{
            $this->db->trans_commit();
        }

   }

   function Mahasiswa_NonAktifDB($table,$data,$id) {
         $this->db->where('id_Mahasiswa',$id );
         $this->db->update($table,$data);
   }

   function Mahasiswa_view_edit($id) {
		$data = $this->db->query("SELECT `tb_mahasiswa`.* , tb_bidangstudi.`nama_bidstudi` FROM `tb_mahasiswa`
                      INNER JOIN `tb_bidangstudi` ON `tb_mahasiswa`.`id_bidstudi` = `tb_bidangstudi`.`id_bidstudi`
                      where NIM = '$id' ");
    return $data->result();
   }

   function Mahasiswa_editDB($table,$data,$id) {
         $this->db->where('NIM',$id );
         $this->db->update($table,$data);
   }

    function Mahasiswa_import($table,$data) {
      $this->db->trans_start();

      $insert = $this->db->insert("tb_mahasiswa",$data);
      $NIM = $data['NIM'];

    $dataUser = array(
      'nama_user' => $data['namaLengkap'],
      'username' => $NIM,
      'password' => $NIM,
      'parent' => 'Mahasiswa',
      'id_parent' => $NIM
      );
      $this->db->insert('tb_user',$dataUser);
      $IdUser = $this->db->insert_id();

      $dataUserRole = array(
      'id_user' => $IdUser,
      'id_role' => '2'
      );
      $this->db->insert('rbac_user_role',$dataUserRole);


       $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else{
            $this->db->trans_commit();
        }
   }











}

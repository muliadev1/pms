<?php

class Vendor extends CI_Controller{

	function __construct(){
		parent::__construct();
		if($this->session->userdata('Kode_userPms')==NULL) {
           redirect('Login');
		}
		$this->load->library('session');
		$this->load->helper(array('form', 'url'));

		$this->load->model(array('M_vendor'));
		
		if (!empty($this->session->userdata("active_db"))) {
			$this->load->database($this->session->userdata("active_db"), FALSE, TRUE);
		}

	}

	function index(){
		redirect('Vendor/Vendor_view');
	}

	function Vendor_view(){
		$data['data'] = $this->M_vendor->Vendor_view();
		$this->load->view('vendor/vendor',$data);
	}


	function Vendor_add(){
	//	$data['data'] = $this->M_subcategory->Subcategory_view();
		$this->load->view('Vendor/Vendor_add');
	}

	function Vendor_addDB(){
		$data = array(
			'vendor' => $this->input->post('vendor'),
			'address' => $this->input->post('address'),
			'phone' => $this->input->post('phone'),
			'email' => $this->input->post('email'),
			'contact' => $this->input->post('contact')
		);
		$this->M_vendor->Vendor_addDB('tbvendor',$data);
		$this->session->set_flashdata('msg', 'Add Vendor successfully ...');
		redirect('Vendor/Vendor_view');
	}

	function Vendor_edit($id){
	//	$data['data'] = $this->M_subcategory->Subcategory_view();
		$data['data2'] = $this->M_vendor->Vendor_view_edit($id) ;
		// print_r($data['data']);
		// exit();
		$this->load->view('vendor/vendor_edit',$data);
	}

	function Vendor_editDB(){
		$id = $this->input->post('Id');
		$data = array(

			'vendor' => $this->input->post('vendor'),
			'address' => $this->input->post('address'),
			'phone' => $this->input->post('phone'),
			'email' => $this->input->post('email'),
			'contact' => $this->input->post('contact')
	);
		$this->M_vendor->Vendor_editDB('tbvendor',$data,$id) ;
		$this->session->set_flashdata('msg', 'Edit Vendor successfully...');
		redirect('Vendor/Vendor_view');


	}


}
?>




<!-- Main content -->

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    <h3 class="modal-title" align="center" id="myModalLabel" style="font-weight:bold;">TABLE <?php echo $data->tableid;?> </h3>
  </div>
   
<section class="content">
  <div class="row">
   <div class="col-md-12">

  <div class="row">
   <div class="col-md-4"> 
      <form method="post" id="Simpan"  action="<?php echo base_url(). 'index.php/Company/Company_addDB'; ?>">
       <div class="form-group">
        <label class="control-label">Total</label>
        <input type="text" name="Total" id="Total" readonly="readonly"   class="form-control"  value="<?php echo number_format($data->total, 0, '.', '.');?>">
         <input type="hidden" name="Total1" id="Total1" value="<?php echo $data->total;?>">
       </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
        <label class="control-label">Discount</label>
        <input type="text" name="Discount" id="Discount" readonly="readonly"   class="form-control"   value="<?php echo number_format($data->disc, 0, '.', '.');?>">
        <input type="hidden" name="Discount1" id="Discount1" value="<?php echo $data->disc;?>">
       </div>
       </div>
       <div class="col-md-4">
        <div class="form-group has-success">
        <label class="control-label">After Discount</label>
        <input type="text" name="AfterDiscount" id="AfterDiscount" readonly="readonly"   class="form-control"   value="<?php echo number_format($data->total2, 0, '.', '.');?>">
        <input type="hidden" name="AfterDiscount1" id="AfterDiscount1"  value="<?php echo $data->total2;?>">
       </div>
      </div>
    </div>

 <div class="row" >
   <div class="col-md-4">
    <div class="form-group"> 
        <label class="control-label">Service</label>
        <input type="text" name="Service" id="Service"   class="form-control input-ser" onkeyup="doMathGrand()"  value="0">
         <input type="text" name="Service1" id="Service1" value="0">
       </div>
       </div>
       <div class="col-md-4">
        <div class="form-group has-success">
        <label class="control-label">Tax</label>
        <input type="text" name="Tax" id="Tax"  class="form-control input-tax"  onkeyup="doMathGrand()" value="0">
        <input type="text" name="Tax1" id="Tax1"  value="0">
       </div>
      </div>
      <div class="col-md-4">
        <div class="form-group has-success">
        <label class="control-label">Grand Total</label>
        <input type="text" name="GrandTotal" id="GrandTotal" readonly="readonly"   class="form-control"  onkeyup="doMathGrand()"  value="<?php echo number_format($data->total2, 0, '.', '.');?>">
        <input type="text" name="GrandTotal1" id="GrandTotal1" >
       </div>
      </div>
    </div>


       <div class="form-group"  >
        <div class="row" >
         <div class="col-md-4">
          <label class="control-label">Payment Status</label>
         </div>
        </div>
        <div class="row" >
         <div class="col-md-4">
          <input type="radio" name="PayMethod" id="type" class="flat-red" value="Walk" id="walk" checked> WALK IN
         </div>
         <div class="col-md-4">
          <input type="radio" name="PayMethod" id="type1" class="flat-red" value="Room" id="room"> ROOM CHARGE
         </div>
         <div class="col-md-4">
          <input type="radio" name="PayMethod" id="type2" class="flat-red" value="Compliment" id="compliment"> COMPLIMENT BILL
         </div>
        </div>
       </div>

      <div class="form-group" hidden="hidden" id="ElementRoom">
        <label class="control-label">Room</label>
         <input type="text" name="Room" id="Room"  readonly="readonly"  class="form-control"  value="0" >
       </div>
 

        <div class="form-group">
        <label>Payment Metgod</label>
        <select id="PayMethod" class="form-control select2"  style="width: 100%;" name="PayMethod">      
             <option value="1">CASH</option>
             <option value="2">DEBT</option>
        </select>
       </div>

        <div class="form-group"> 
        <label class="control-label">Payment Amount</label>
        <input type="text" name="PayAmount" id="PayAmount"  value="0"   class="form-control input-21111" onkeyup="doMathPay()" >
        <input type="hidden" name="PayAmount1" id="PayAmount1" value="0" onkeyup="doMathPay()" >
       </div>
        <div class="form-group has-success">
        <label class="control-label">Remain</label>
        <input type="text" name="Remain" id="Remain"  readonly="readonly" value="0"  class="form-control"  value="0" onkeyup="doMathPay()">
        <input type="hidden" name="Remain1" id="Remain1"  readonly="readonly" value="0"  onkeyup="doMathPay()">
       </div>
       <div class="form-group ">
        <label class="control-label">Note</label>
        <textarea type="text" name="Note" id="Note"    class="form-control"></textarea>
       </div>

          <div class="form-group">
            <button type="submit" value="Validate" class="btn btn-default"><i class='glyphicon glyphicon-ok'></i> Save</button>
          </div>
        </form>
       </div>

</section>


<script type="text/javascript">
   $(function () {
    new Cleave('.input-21111', {
      numeral: true,
      numeralDecimalMark: ',',
      delimiter: '.'
    });
    new Cleave('.input-ser', {
      numeral: true,
      numeralDecimalMark: ',',
      delimiter: '.'
    });
    new Cleave('.input-tax', {
      numeral: true,
      numeralDecimalMark: ',',
      delimiter: '.'
    });
  });
</script>
 <script type="text/javascript">
      function doMathGrand()
      {
          var service = document.getElementById('Service').value;
          document.getElementById('Service1').value =  service.replace(/\./g, "");
          var tax = document.getElementById('Tax').value;
          document.getElementById('Tax1').value =  tax.replace(/\./g, "");

          var AfterDiscount = document.getElementById('AfterDiscount1').value;
          var service1 = document.getElementById('Service1').value;
          var tax1 = document.getElementById('Tax1').value;
          var Grandtotal1 =  parseFloat(service1) +parseFloat(tax1)+parseFloat(AfterDiscount);

          alert(Grandtotal1);

          //document.getElementById('Grandtotal').value = Grandtotal1.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
           document.getElementById('Grandtotal1').value = Grandtotal1;

      }
    </script>

 <script type="text/javascript">
      function doMathPay()
      {
          var PayAmount = document.getElementById('PayAmount').value;
          document.getElementById('PayAmount1').value =  PayAmount.replace(/\./g, "");

          var AfterDiscount = document.getElementById('AfterDiscount1').value;
          var PayAmount1 = document.getElementById('PayAmount1').value;
          var Grandtotal =  AfterDiscount-PayAmount1;
          document.getElementById('Remain').value = Grandtotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
           document.getElementById('Remain1').value = Grandtotal;

      }
    </script>


<script type="text/javascript">
$("#Simpan").submit(function() {
    var name = $('#name').val();
    var description = $('#description').val();
    var state = $('#state').val();
    var phone = $('#phone').val();
    var zipcode = $('#zipcode').val();
    var email = $('#email').val();
    var address = $('#address').val();
    var type = $('#type').val();

        if (name == ''|| description==''|| state == ''|| phone==''|| zipcode == ''|| email==''|| address == ''|| type==''){
            File_Kosong(); return false;
        }else{
        event.preventDefault();
        $.confirm({
          title: 'Confirmation',
          content: 'Are You Sure to Save?',
           type: 'blue',
          buttons: {
              Save: function () {
                  $.LoadingOverlay("show");
                  $("#Simpan").submit();
              },
              Cancel: function () {

                  $.alert('Data Not Saved...');
              },
          }
      });
    }

});
</script>





<script type="text/javascript">
  function File_Kosong() {
  $.alert({
    title: 'Caution!!',
    content: 'Add Company Invalid!',
    icon: 'fa fa-warning',
    type: 'orange',
});
}
</script>

<script>
$(function () {
  $(".select2").select2();
   $(".select3").select2();

});
</script>
<script type="text/javascript">
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });
</script>

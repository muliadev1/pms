
<?php 

class M_purchase_order extends CI_Model{	



	function item_view() {
   $query = $this->db->query("SELECT tbitem.*, tbsubcategory.`description` AS descsubcategory FROM tbitem INNER JOIN `tbsubcategory`
                                ON tbitem.`idsubcategory` =  tbsubcategory.`idsubcategory`  
                                 "); 
    return $query->result() ;     
		        
   }
   function Load_aset_byiditem($codeitem) {
   $query = $this->db->query("SELECT codeaset from tbaset where codeitem = '$codeitem'  
                                 "); 
    $data =  $query->row() ; 
   return  $data->codeaset  ; 
            
   }
   function Load_subcategory_byiditem($codeitem) {
   $query = $this->db->query("SELECT idcategory FROM tbitem INNER JOIN tbsubcategory 
                                  ON  tbitem.`idsubcategory` = tbsubcategory.`idsubcategory` where codeitem = '$codeitem'  
                                 "); 
    $data =  $query->row() ; 
   return  $data->idcategory  ; 
            
   }

    function load_price_item($codeitem) {   
    $this->db->select("*");
    $this->db->from('tbitem');
    $this->db->where('codeitem',$codeitem );  
    $data = $this->db->get()->row(); 
    return $data;        
   } 

       function get_kodepo($table) { 
        $tahun = date('y'); 
        $bulan = date('m'); 
        $q = $this->db->query("SELECT MAX(RIGHT(kodepo,4)) AS idmax FROM ".$table);
        $kd = ""; //kode awal
        if($q->num_rows()>0){ //jika data ada
            foreach($q->result() as $k){
                $tmp = ((int)$k->idmax)+1; //string kode diset ke integer dan ditambahkan 1 dari kode terakhir
                $kd = "0000".$tmp; //kode ambil 4 karakter terakhir
                $id = substr($kd, -5);
            }
        }else{ //jika data kosong diset ke kode awal
            $kd = $tahun.$bulan."00001";
        }
        $kar = "PO"; //karakter depan kodenya
        //gabungkan string dengan kode yang telah dibuat tadi
        return $kar.$tahun.$bulan.$id;
   }


   function Po_add($data,$dataDetil) { 
   // print_r($data);exit();
       $this->db->trans_strict(FALSE);
       $this->db->trans_start();  

   		 $this->db->insert( 'tbpo', $data);
 		   $this->db->insert_batch( 'tbpodetil', $dataDetil);

    $this->db->trans_complete();
      if ($this->db->trans_status() === FALSE){
          $this->db->trans_rollback();
      }else{
          $this->db->trans_commit();
      }      
   } 


    function Po_edit($data,$dataDetil,$kodepo) { 
       $this->db->trans_strict(FALSE);
       $this->db->trans_start(); 

       $this->db->where('kodepo', $kodepo);
       $this->db->update('tbpo', $data);

        $this->db->where('kodepo', $kodepo);
        $this->db->delete('tbpodetil');

       $this->db->insert_batch( 'tbpodetil', $dataDetil);

    $this->db->trans_complete();
      if ($this->db->trans_status() === FALSE){
          $this->db->trans_rollback();
      }else{
          $this->db->trans_commit();
      }      
   } 

    function purchase_order_add_setujui($data,$kode) { 
      $this->db->where('kodepo', $kode);
       $this->db->update( 'tbpo', $data);
     
   } 
   function purchase_order_add_aktifkan($data,$kode) { 
      $this->db->where('kodepo', $kode);
       $this->db->update( 'tbpo', $data);  
   }
    function purchase_order_add_delete($kode) { 
      $this->db->where('kodepo', $kode);
      $this->db->delete('tbpo');

       $this->db->where('kodepo', $kode);
      $this->db->delete('tbpodetil');
   } 

   function pr_view() {   
    $query = $this->db->query("SELECT kodepr,departement,date_format(dateadd,'%d-%m-%Y') as dateadd ,description,idoperator,nama_user FROM tbpr 
                            INNER JOIN tb_user ON tbpr.idoperator = tb_user.id_user"); 
    return $query->result() ;   
       
   }

  function load_pr_byvendor($id_vendor) {   
    $query = $this->db->query("SELECT tbprdetil.`codeitem`,subcategory,tbprdetil.`description`,tbprdetil.`unit`,currstok,tbprdetil.`price`,`qtyorder`,
                              (tbprdetil.price*tbprdetil.qtyorder) AS Subtotal,tbpr.`departement` FROM `tbpr` 
                              INNER JOIN tbprdetil ON tbpr.`kodepr` = tbprdetil.`kodepr` 
                              INNER JOIN tbitem ON tbprdetil.`codeitem` = tbitem.`codeitem` 
                              INNER JOIN tbsubcategory ON tbitem.idsubcategory = tbsubcategory.idsubcategory 
                                 "); 
    return $query->result() ;   
       
   } 

    function load_vendor_pr() {   
    $query = $this->db->query("SELECT tbvendor.`idvendor`,tbvendor.`vendor` FROM `tbvendor` WHERE idvendor IN (
                              SELECT tbvendor.`idvendor` FROM `tbvendor` INNER JOIN tbitem
                              ON tbvendor.`idvendor` =  tbitem.`idvendor` INNER JOIN tbprdetil
                              ON tbprdetil.`codeitem` = tbitem.`codeitem` )
                                 "); 
    return $query->result() ;   
       
   } 

  function load_vendor_all() {   
    $query = $this->db->query("SELECT tbvendor.`idvendor`,tbvendor.`vendor` FROM `tbvendor` "); 
    return $query->result() ;   
       
   } 
   function load_po_all() {   
    $query = $this->db->query("SELECT `kodepo`,`vendor`,DATE_FORMAT(`dateorder`,'%d-%m-%Y') AS dateorder, `total`,status,catatan,description FROM tbpo order by kodepo desc  "); 
    return $query->result() ;   
       
   } 

   function load_po_all_veri() {   
    $query = $this->db->query("SELECT `kodepo`,`vendor`,DATE_FORMAT(`dateorder`,'%d-%m-%Y') AS dateorder, `total`,status,catatan,description FROM tbpo  where description<>'By System'  order by kodepo desc  "); 
    return $query->result() ;   
       
   }


   function load_po_all_veri_detil($kode) {   
    $query = $this->db->query("SELECT tbpodetil.*, tbpo.description AS descmaster, STATUS,catatan FROM `tbpodetil`  INNER JOIN tbpo 
                                ON tbpodetil.`kodepo` =  tbpo.`kodepo`
                                WHERE tbpodetil.kodepo = '$kode'  "); 
    return $query->result() ;   
       
   } 

    function load_realisasi_po($IdVendor, $DateFromRpo2,$DateToRpo2) {
         if ($IdVendor!=0) {
             $where  = "WHERE datereceive BETWEEN '".$DateFromRpo2."' and '".$DateToRpo2."' and  tbro.idvendor ='$IdVendor' ";
         }else{
            $where  = "WHERE datereceive BETWEEN '".$DateFromRpo2."' and '".$DateToRpo2."' ";
         }
           $data = $this->db->query("SELECT DATE_FORMAT(datereceive,'%d-%m-%Y') AS datereceive , tbro.`kodepo` ,tbro.vendor, 
            tbpo.`total` AS totalpo,tbro.`kodero` ,tbro.`total` AS totalro FROM `tbro` INNER JOIN tbpo 
              ON tbro.`kodepo` = tbpo.`kodepo` ".$where."
             " );


    return $data->result();
 
}
 function load_realisasi_po_detil($kodepo) {   
    $query = $this->db->query("SELECT * FROM tbrodetil INNER JOIN tbro
                              ON tbrodetil.`kodero` = tbro.`kodero`
                              WHERE tbro.`kodepo` = '$kodepo'  "); 
    return $query->result() ;      
   }


  function Load_po_master($kode) {   
    $query = $this->db->query("SELECT * FROM tbpo WHERE kodepo = '$kode'  "); 
    return $query->row() ;      
   }
   function Load_po_detil($kode) {   
    $query = $this->db->query("SELECT * FROM tbpodetil WHERE kodepo = '$kode'  "); 
    return $query->result() ;      
   } 

   function load_po(){
   $query = $this->db->query("Select * from tbpo where ischecked=0  and status=1 order by kodepo desc");
   return $query->result() ;
  }
   function load_po_cheked($tglawal, $tglakhir){
   $query = $this->db->query("SELECT tbpo.*,date_format(dateorder,'%d-%m-%Y') as dateorder1 from tbpo where ischecked=1   AND  date(dateorder) >='$tglawal' AND date(dateorder) <= '$tglakhir' order by kodepo desc");
   return $query->result() ;
  }
   function load_po_delete($tglawal, $tglakhir){
   $query = $this->db->query("SELECT tbpo.*,date_format(dateorder,'%d-%m-%Y') as dateorder1 from tbpo where ischecked=0   AND  date(dateorder) >='$tglawal' AND date(dateorder) <= '$tglakhir' order by kodepo desc");
   return $query->result() ;
  }

  function load_po_by_kode($kodepo){
   $query = $this->db->query("Select * from tbpo where kodepo='$kodepo'");
   return $query->result() ;
  }
  function load_po_item_by_kode($kodepo){
   $query = $this->db->query("Select * from tbpodetil where kodepo='$kodepo'");
   return $query->result() ;
  }
  function select_item_by_itemcode_kodepo($itemcode,$kodepo) {
    $query = $this->db->query("select * from tbpodetil
                                WHERE codeitem = '$itemcode' and kodepo = '$kodepo'
                                 ");
    return $query->result() ;

   }

    function load_departement(){   
      $query = $this->db->query(" SELECT * from tbdepartement"); 
    return $query->result() ;       
   }
    function load_po_forlaporan($kodePo){   
      $query = $this->db->query(" SELECT tbpodetil.`kodepo`,`vendor`,DATE_FORMAT(`dateorder`,'%d-%m-%Y') AS dateorder, tbpodetil.description, qtyorder,price,tbpodetil.`note` as notedetil,(SELECT nama_user FROM tb_user WHERE id = verifikatorby) AS namaveri
           FROM tbpo 
                            INNER JOIN tbpodetil ON tbpo.`kodepo` =  tbpodetil.`kodepo` WHERE tbpodetil.`kodepo` = '$kodePo'
                            "); 
    return $query->result() ;       
   }
   function add_detilaset($data){   
      $this->db->insert_batch('tbasetdetil',$data);      
   }

  

  

  


  


      



}
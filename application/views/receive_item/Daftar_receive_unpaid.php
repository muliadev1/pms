

          <div class="table-responsive">
                    <table id="example11" class="table table-bordered table-striped">
                                <thead>
                                    <tr >
                                        <th>Invoice No.</th>
                                        <th>Invoice Date</th>
                                        <th>Amount</th>
                                        <th style="display:none">Amount</th>
                                         <th>Action</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach($data as $u){
                                    ?>
                                    <tr>
                                         <td class="invoiceno" ><?php echo $u->kodero ?></td>
                                         <td class="invoicedate"><?php echo $u->datereceive ?></td>
                                         <td align="right" class="amount"><?php echo number_format($u->totalafterdisc) ?></td>
                                        <td class="amount1" style="display:none"><?php echo $u->totalafterdisc ?></td>
                                      <td align="center">
                                       <a class='btn btn-default Pilih' id="" data-toggle='modal'> <span class='fa fa-fw  fa-check-square-o' ></span> </a>
                                      </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
          </div>

<script type="text/javascript">

     $(".Pilih").click(function(){
       //alert('Halo');
            var invoiceno =$(this).closest('tr').children('td.invoiceno').text();
            var invoicedate =$(this).closest('tr').children('td.invoicedate').text();
            var amount =$(this).closest('tr').children('td.amount').text();
            var amount1 =$(this).closest('tr').children('td.amount1').text();


            var mymodal = $('#myModal');

            $('#invoiceno').val(invoiceno);
            $('#invoicedate').val(invoicedate);
            $('#amount').val(amount);
            $('#amount1').val(amount1);
            // alert(amount1);
             mymodal.modal('hide');
    });

</script>

  <script type="text/javascript">
  $(function () {
     $('#example11').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": true
    });
  });
  </script>

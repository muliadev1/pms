
<?php echo $header?>

<style type="text/css">
  #TabelKonten tr td {
    padding-right: 7px;
    padding-left:  7px;
    font-size: 11px;
  }
</style>

<div style="margin-buttom : 15px;" >
<table width="100%" border="0"  >
    <tr>
     <td colspan="2" align="center" style="font-size:14px;"> <strong> PURCHASE REPORT  </strong>  </td>
    </tr>
    
   
  </table>
<table width="100%" border="0" style="font-size:11px;"  >
   <tr>
     <td>&nbsp;</td>
     <td align="left">&nbsp;</td>
    </tr>
  
    <tr>
    <tr>
     <td width="10%"><strong>Date</strong> </td>
     <td align="left">: <?php echo $periode['TglAwal'];?> <strong>TO</strong> <?php echo $periode['TglAkhir'];?> </td>
    </tr>
     <tr>
     <td width="10%"><strong>Category</strong> </td>
     <td align="left">: <?php echo $periode['Cat'];?> </td>
    </tr>
    <!--  <tr>
     <td width="10%"><strong>Departement</strong> </td>
     <td align="left">: <?php echo $periode['Dep'];?> </td>
    </tr> -->
    <tr>
     <td>&nbsp;</td>
     <td align="left">&nbsp;</td>
    </tr>
    

  </table>
</div>

<table id="TabelKonten"  border="1" style="border-collapse: collapse; border-color:#000000; margin-bottom : 130px;"  width="100%"   >
    <thead  >
        <tr align="center" class="header">
            <th width="5%" style="font-size: 11px;" >NO</th>
            <th width="8%" style="font-size: 11px;">DATE</th>
            <th width="7%" style="font-size: 11px;">MI No.</th>
            <th width="15%" style="font-size: 11px;">VENDOR</th>
            <th width="10%" style="font-size: 11px;">BEER</th>
            <th width="10%" style="font-size: 11px;">ALCOHOL</th>
            <th width="10%" style="font-size: 11px;">WINE</th>
            <th width="10%" style="font-size: 11px;">SOFT DRINK</th>
            <th width="10%" style="font-size: 11px;">OTHER</th>
            <th width="15%" style="font-size: 11px;">SUBTOTAL</th>
        </tr>
    </thead>
     <tbody>
      <?php $total=0;$no=1; 
      $subtotalbeer = 0; $subtotalalc = 0; $subtotalwine = 0; $subtotalsd=0; $subtotalhari=0;
      $totalbeer = 0; $totalalc = 0; $totalwine = 0; $totalsd=0; $totalhari=0; $totalot=0; 
      $tglhari = '-'; $last=count($konten);// print_r($last);exit();
       foreach ($konten as $row) { ?>

        <?php if ($tglhari!=$row->tgl and $no!=1 ) { ?>
        <tr style="background-color: #f0f0f0;">
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
      <td  align="right" style="font-size: 10px;"> <strong><?php echo number_format($subtotalbeer , 0, '.', '.'); ?></strong></td>
      <td  align="right" style="font-size: 10px;"> <strong><?php echo number_format($subtotalalc , 0, '.', '.'); ?></strong></td>
      <td  align="right" style="font-size: 10px;"> <strong><?php echo number_format($subtotalwine , 0, '.', '.'); ?></strong></td>
      <td  align="right" style="font-size: 10px;"> <strong><?php echo number_format($subtotalsd , 0, '.', '.'); ?></strong></td>
      <td  align="right" style="font-size: 10px;"> <strong><?php echo number_format($subtotalot , 0, '.', '.'); ?></strong></td>
      <td  align="right" style="font-size: 10px;"> <strong><?php echo number_format($subtotalbeer+$subtotalalc+$subtotalwine+$subtotalsd , 0, '.', '.'); ?></strong></td>
        </tr>
            <?php  $totalbeer+= $subtotalbeer; $totalalc+=$subtotalalc; $totalwine+=$subtotalwine;$totalsd+=$subtotalsd;
            $totalot+=$subtotalot;
             $subtotalbeer=0;$subtotalalc=0;$subtotalwine=0;$subtotalsd=0;$subtotalot=0;$subtotalhari=0; } ?>


    <tr>
      <td style="font-size: 10px;"><?php echo $no; ?></td>
      <td style="font-size: 10px;"><?php echo $row->tgl; ?></td>
      <td style="font-size: 10px;"><?php echo $row->kodero; ?></td>
      <td style="font-size: 10px;"><?php echo $row->vendor; ?></td>

          <?php if ($row->idsubcategory=='25') { $subtotalbeer+=$row->subtotal; ?>
           <td align="right" style="font-size: 10px;"><?php echo number_format($row->subtotal , 0, '.', '.'); ?></td>
           <td>&nbsp;</td>
           <td>&nbsp;</td>
           <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td  align="right" style="font-size: 10px;"><?php echo number_format($row->subtotal , 0, '.', '.'); ?></td>

          <?php }elseif($row->idsubcategory=='23') { $subtotalalc+=$row->subtotal; ?>
           <td>&nbsp;</td>
           <td  align="right" style="font-size: 10px;"><?php echo number_format($row->subtotal , 0, '.', '.'); ?></td>
           <td>&nbsp;</td>
           <td>&nbsp;</td>
           <td>&nbsp;</td>
           <td  align="right" style="font-size: 10px;"><?php echo number_format($row->subtotal , 0, '.', '.'); ?></td>

           <?php }elseif($row->idsubcategory=='24') { $subtotalwine+=$row->subtotal; ?>
           <td>&nbsp;</td>
           <td>&nbsp;</td>
           <td  align="right" style="font-size: 10px;"><?php echo number_format($row->subtotal , 0, '.', '.'); ?></td>
           <td>&nbsp;</td>
           <td>&nbsp;</td>
           <td  align="right" style="font-size: 10px;"><?php echo number_format($row->subtotal , 0, '.', '.'); ?></td>

           <?php }elseif($row->idsubcategory=='26') { $subtotalsd+=$row->subtotal ?>
           <td>&nbsp;</td>
           <td>&nbsp;</td>
           <td>&nbsp;</td>
           <td  align="right" style="font-size: 10px;"><?php echo number_format($row->subtotal , 0, '.', '.'); ?></td>
            <td>&nbsp;</td>
           <td  align="right" style="font-size: 10px;"><?php echo number_format($row->subtotal , 0, '.', '.'); ?></td>

           <?php }else { $subtotalot+=$row->subtotal ?>
           <td>&nbsp;</td>
           <td>&nbsp;</td>
           <td>&nbsp;</td>
            <td>&nbsp;</td>
           <td  align="right" style="font-size: 10px;"><?php echo number_format($row->subtotal , 0, '.', '.'); ?></td>
           <td  align="right" style="font-size: 10px;"><?php echo number_format($row->subtotal , 0, '.', '.'); ?></td>
           <?php } ?> 
       </tr>      
       </tr>           

          
   
      
      <?php $no++; $totalhari+=$row->subtotal; $tglhari=$row->tgl;} ?>

         <tr style="background-color: #f0f0f0;">
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
      <td  align="right" style="font-size: 10px;"> <strong><?php echo number_format($subtotalbeer , 0, '.', '.'); ?></strong></td>
      <td  align="right" style="font-size: 10px;"> <strong><?php echo number_format($subtotalalc , 0, '.', '.'); ?></strong></td>
      <td  align="right" style="font-size: 10px;"> <strong><?php echo number_format($subtotalwine , 0, '.', '.'); ?></strong></td>
      <td  align="right" style="font-size: 10px;"> <strong><?php echo number_format($subtotalsd , 0, '.', '.'); ?></strong></td>
      <td  align="right" style="font-size: 10px;"> <strong><?php echo number_format($subtotalot , 0, '.', '.'); ?></strong></td>
      <td  align="right" style="font-size: 10px;"> <strong><?php echo number_format($subtotalbeer+$subtotalalc+$subtotalwine+$subtotalsd+$subtotalot , 0, '.', '.'); ?></strong></td>
        </tr>

        <tr style="background-color: #f0f0f0;">
          <td colspan="10">&nbsp;</td>
        </tr>
        <tr style="background-color: #f0f0f0;">
          <td colspan="4" align="center"><strong>TOTAL</strong></td>
          <td  align="right" style="font-size: 10px;"> <strong><?php echo number_format($totalbeer , 0, '.', '.'); ?></strong></td>
          <td  align="right" style="font-size: 10px;"> <strong><?php echo number_format($totalalc , 0, '.', '.'); ?></strong></td>
          <td  align="right" style="font-size: 10px;"> <strong><?php echo number_format($totalwine , 0, '.', '.'); ?></strong></td>
          <td  align="right" style="font-size: 10px;"> <strong><?php echo number_format($totalsd , 0, '.', '.'); ?></strong></td>
           <td  align="right" style="font-size: 10px;"> <strong><?php echo number_format($totalot , 0, '.', '.'); ?></strong></td>
          <td  align="right" style="font-size: 10px;"> <strong><?php echo number_format($totalbeer+$totalalc+$totalwine+$totalsd+$totalot , 0, '.', '.'); ?></strong></td>
        </tr>
   
    </tbody>

    <tfoot>
      
    </tfoot>
     

  </table>



<!-- <table width="100%" border="0" style="font-size:11px; "  >
    <tr>
      <td >&nbsp; </td>
      <td >&nbsp; </td>     
      <td >&nbsp; </td>
    </tr>
     <tr>
      <td >&nbsp; </td>
      <td >&nbsp; </td>     
      <td >&nbsp; </td>
    </tr>
  <tr>
     <td ><strong>QUANTYTY AND QUALITY APPROVED</strong> </td>
     <td ><strong>PRICE APPROVED</strong></td>
     <td ><strong>RECEIVED</strong></td>
    </tr>
         <tr>
      <td >&nbsp; </td>     
      <td >&nbsp; </td>
      <td >&nbsp; </td>
    </tr>
     <tr>
      <td >&nbsp; </td>     
      <td >&nbsp; </td>
      <td >&nbsp; </td>
    </tr>
     <tr>
      <td >&nbsp; </td>     
      <td >&nbsp; </td>
      <td >&nbsp; </td>
    </tr>
     <tr>
      <td ><hr style="color:#000000"> </td>     
      <td ><hr style="color:#000000"> </td>
      <td ><hr style="color:#000000"> </td>
    </tr>
   
  </table> -->


  






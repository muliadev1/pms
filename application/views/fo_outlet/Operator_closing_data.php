

         <div class="table-responsive">
          <table id="exampleada" class="table table-bordered table-striped">
                                <thead>
                                     <tr >
                                        <th >NO</th>
                                        <th >Invoice No</th>
                                        <th >Manual Invoice No</th>
                                        <th >Room</th>
                                        <th >Date</th>                         
                                        <th >Article</th>
                                        <th >Room Type</th>
                                        <th >Amount</th>
                                        <th >User</th>
                                        <th style="display: none;">IdDetil</th>
                                </thead>
                                <tbody>
                                  <?php $no=1;$totalamount=0;   foreach ($dataclosing as $clo){  ?>
                                    <tr>
                                      <td><?php echo $no ?></td>
                                      <td><?php echo $clo->kodemaster ?></td>
                                      <td><?php echo $clo->invoicemanual ?></td>
                                      <td><?php echo $clo->roomname ?></td>
                                      <td><?php echo $clo->trdate ?></td>
                                      <td><?php echo $clo->articlename ?></td>
                                      <td><?php echo $clo->articlecategory ?></td>
                                      <td align="right"><?php echo number_format($clo->charge, 2, ',', '.') ?></td>
                                      <td><?php echo $clo->nama_user ?></td>
                                      <td style="display: none;"><?php echo $clo->kode ?></td> 
                                    </tr>
                                  
                                    
                                  <?php $no++;$totalamount+=$clo->charge;} ?>
                                    
                                </tbody>
                                <tfoot>
                                  <tr style="background-color: #f0f0f0; font-weight: bold;">
                                    <td colspan="7">TOTAL</td>
                                     <td align="right"><?php echo number_format($totalamount, 2, ',', '.') ?></td>
                                      <td>&nbsp;</td>
                                  </tr>
                                </tfoot>
                            </table>
          </div>

         

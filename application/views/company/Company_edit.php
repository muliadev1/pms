<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>

<!-- Content Header (Page header) -->
<section class="content-header">
   <div class="btn-group btn-breadcrumb">
            <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
             <a href="<?php echo base_url('index.php/Company/Company_view');?>" class="btn btn-default  btn-xs">Company</a>
            <a  class="btn btn-default  btn-xs active">Edit Company</a>
        </div>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
   <div class="col-md-12">
    <div class="box box-info">
     <div class="box-header">
      <h3 class="box-title">Edit Company</h3>
     </div>
     <div class="box-body">
     <form method="post" id="Simpan" action="<?php echo base_url(). 'index.php/Company/Company_editDB'; ?>">

     <div class="form-group">
      <label class="control-label">Company Name</label>
      <input type="text" name="name" id="name"  value="<?php echo $data[0]->name; ?>"  data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out category name..."  class="form-control"  placeholder="">
      <input type="hidden" name="IdCompany"  value="<?php echo $data[0]->idcompany; ?>" class="form-control"  placeholder="">
     </div>

     <div class="form-group">
      <label class="control-label">Description</label>
      <textarea type="text" name="description" id="description" data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out firstname..."  class="form-control"><?php echo $data[0]->description;?></textarea>
     </div>

     <div class="form-group">
      <label>Country</label>
      <select id="state" name="state" class="form-control select2"  style="width: 100%;">
        <?php
         foreach($country as $u){
           //print_r($country);exit();
           if ($u == $data[0]->state) {
             ?>
             <option value="<?php echo $u; ?>"selected><?php echo $u; ?></option>
             <?php
           } else {
             ?>
             <option value="<?php echo $u; ?>"><?php echo $u; ?></option>
               <?php
           }
         }
             ?>
      </select>
     </div>

     <div class="form-group">
      <label class="control-label">Phone</label>
      <input type="text" name="phone" id="phone" value="<?php echo $data[0]->phone; ?>"  data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out firstname..."  class="form-control"  placeholder="">
     </div>

     <div class="form-group">
      <label class="control-label">Zip Code</label>
      <input type="text" name="zipcode" id="phone" value="<?php echo $data[0]->zipcode; ?>"  data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out firstname..."  class="form-control"  placeholder="">
     </div>

     <div class="form-group">
      <label class="control-label">E-Mail</label>
      <input type="text" name="email" id="email" value="<?php echo $data[0]->email; ?>"  data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out firstname..."  class="form-control"  placeholder="">
     </div>

     <div class="form-group">
      <label class="control-label">Address</label>
      <textarea type="text" name="address" id="address" data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out firstname..."  class="form-control"><?php echo $data[0]->address;?></textarea>
     </div>

     <div class="form-group">
      <div class="row">
       <div class="col-md-1">
        <label class="control-label">Type</label>
       </div>
      </div>
      <div class="row">
       <div class="col-md-2">
        <input type="radio" name="type" <?php if (isset($data[0]->type) && $data[0]->type=="Company") echo "checked";?> class="flat-red" value="Company"> Company
       </div>
       <div class="col-md-2">
        <input type="radio" name="type" <?php if (isset($data[0]->type) && $data[0]->type=="Agens") echo "checked";?> class="flat-red"value="Agens"> Agens
       </div>
       <div class="col-md-2">
        <input type="radio" name="type" <?php if (isset($data[0]->type) && $data[0]->type=="Pului Agens") echo "checked";?> class="flat-red" value="Pului Agens"> Pului Agens
       </div>
      </div>
     </div>

      <div class="form-group">
       <button type="submit" value="Validate" class="btn btn-default"><i class='glyphicon glyphicon-ok'></i> Save</button>
      </div>
     </form>
    </div>
   </div>
  </div>
 </div>
</section>

<script type="text/javascript">
$("#Simpan").submit(function() {
  var name = $('#name').val();
  var description = $('#description').val();
  var state = $('#state').val();
  var phone = $('#phone').val();
  var zipcode = $('#zipcode').val();
  var email = $('#email').val();
  var address = $('#address').val();
  var type = $('#type').val();
      if (name == ''|| description==''|| state == ''|| phone==''|| zipcode == ''|| email==''|| address == ''|| type==''){
          File_Kosong(); return false;
        }else{
        event.preventDefault();
        $.confirm({
          title: 'Confirmation',
          content: 'Are You Sure to Save?',
           type: 'blue',
          buttons: {
              Save: function () {
                  $.LoadingOverlay("show");
                  $("#Simpan").submit();
              },
              Cancel: function () {

                  $.alert('Data Not Saved...');
              },
          }
      });
    }

});
</script>

<?php
$this->load->view('template/Foot');
$this->load->view('template/Js');
?>


<script>

  $.validate({
    modules : 'location, date, file',
    onModulesLoaded : function() {
      $('#country').suggestCountry();
    }
  });

  // Restrict presentation length
  $('#presentation').restrictLength( $('#pres-max-length') );

</script>
<script type="text/javascript">
  function File_Kosong() {
  $.alert({
    title: 'Caution!!',
    content: 'Edit Data Invalid!',
    icon: 'fa fa-warning',
    type: 'orange',
});
}
</script>

<script>
$(function () {
  $(".select2").select2();
   $(".select3").select2();

});
</script>
<script type="text/javascript">
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });
</script>

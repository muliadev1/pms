<?php
/**
 *
 */
class Menu_category extends CI_Controller{

  function __construct(){
		parent::__construct();
		if($this->session->userdata('Kode_userPms')==NULL) {
           redirect('Login');
		}
		$this->load->library('session');
		$this->load->helper(array('form', 'url'));
		$this->load->model(array('M_menu_category'));
		
		if (!empty($this->session->userdata("active_db"))) {
			$this->load->database($this->session->userdata("active_db"), FALSE, TRUE);
		}
	}
   function index(){
    redirect('Menu_category/MenuCategory_view');
  }
   function MenuCategory_view(){
    $data['data'] = $this->M_menu_category->MenuCategory_view();
		$this->load->view('menu_category/menu_category',$data);
  }

  function MenuCategory_add(){
		$this->load->view('menu_category/menu_category_add');
	}

	function MenuCategory_addDB(){
		$data = array(
			'category' => $this->input->post('category'),
			'description' => $this->input->post('description')
		);
		$this->M_menu_category->MenuCategory_addDB('tbmenucategory',$data);
		$this->session->set_flashdata('msg', 'Add Menu Category successfully ...');
		redirect('Menu_category/MenuCategory_view');
	}

	function MenuCategory_edit($id){
		$data['data'] = $this->M_menu_category->MenuCategory_view_edit($id) ;
		$this->load->view('menu_category/menu_category_edit',$data);
	}

	function MenuCategory_editDB(){
		$id = $this->input->post('Id');
		$data = array(
			'category' => $this->input->post('category'),
			'description' => $this->input->post('description')

	);
		$this->M_menu_category->MenuCategory_editDB('tbmenucategory',$data,$id) ;
		$this->session->set_flashdata('msg', 'Edit Menu Category successfully...');
		redirect('Menu_category/MenuCategory_view');
	}
}

?>

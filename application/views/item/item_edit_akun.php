<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>
<script src="<?php echo base_url('assets/js/accounting.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/cleave.min.js') ?>" type="text/javascript"></script>

    <script type="text/javascript">
      function doMath()
      {
          var price = document.getElementById('price1').value;
          document.getElementById('price').value =  price.replace(/\./g, "");
      }
    </script>
    <script type="text/javascript">
      jQuery(document).ready(function($) {
        new Cleave('.input-1', {
           numeral: true, 
           numeralDecimalMark: ',',
           delimiter: '.' 
      });
          });
    </script>

<!-- Content Header (Page header) -->
<section class="content-header">
   <div class="btn-group btn-breadcrumb">
            <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
             <a href="<?php echo base_url('index.php/Item/Item_view');?>" class="btn btn-default  btn-xs">Item</a>
            <a  class="btn btn-default  btn-xs active">Edit Item</a>
        </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
             <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Edit Item </h3>
                </div>
                <div class="box-body">
                    <form method="post" id="Simpan" action="<?php echo base_url(). 'index.php/Item/Item_editDB_akun'; ?>">
                    <div class="form-group">
                      <div class="form-group">
                              <label class="control-label">Item Code</label>
                              <input type="text" readonly="readonly" value="<?php echo $data2[0]->codeitem; ?>"  name="Id"  data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out item code..."  class="form-control"  placeholder="">
                      </div>
                     
                    </div>
                      <div class="form-group">
                            <label class="control-label">Description</label>
                            <input type="text" value="<?php echo $data2[0]->description; ?>"  name="description"  data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out description..."  class="form-control"  placeholder="">
                    </div>

                   <div class="form-group">
                      <label>Akun HK</label>
                    <select id="akunHK" class="form-control select2"  style="width: 100%;" name="akunHK">
                      <option value="1101"></option>
                      <?php foreach($dataakun as $akun){  ?>
                        <?php if ($akun->NoAkun==$data2[0]->akunHK) { ?>
                         <option value="<?php echo $akun->NoAkun; ?>" selected><?php echo $akun->nama; ?></option>
                        <?php } ?>
                      <option value="<?php echo $akun->NoAkun; ?>" ><?php echo $akun->nama; ?></option>
                      <?php } ?>
                    </select>
                  </div>

                  <div class="form-group">
                      <label>Akun FO</label>
                    <select id="akunFO" class="form-control select2"  style="width: 100%;" name="akunFO">
                      <option value="1101"></option>
                      <?php foreach($dataakun as $akunFO){  ?>
                        <?php if ($akunFO->NoAkun==$data2[0]->akunFO) { ?>
                         <option value="<?php echo $akun->NoAkun; ?>" selected><?php echo $akun->nama; ?></option>
                        <?php } ?>
                      <option value="<?php echo $akun->NoAkun; ?>" ><?php echo $akun->nama; ?></option>
                      <?php } ?>
                    </select>
                  </div>

                  <div class="form-group">
                      <label>Akun FB</label>
                    <select id="akunFB" class="form-control select2"  style="width: 100%;" name="akunFB">
                      <option value="1101"></option>
                      <?php foreach($dataakun as $akunFB){  ?>
                        <?php if ($akunFB->NoAkun==$data2[0]->akunFB) { ?>
                         <option value="<?php echo $akun->NoAkun; ?>" selected><?php echo $akun->nama; ?></option>
                        <?php } ?>
                      <option value="<?php echo $akun->NoAkun; ?>" ><?php echo $akun->nama; ?></option>
                      <?php } ?>
                    </select>
                  </div>

                   <div class="form-group">
                      <label>Akun BO</label>
                    <select id="akunBO" class="form-control select2"  style="width: 100%;" name="akunBO">
                      <option value="1101"></option>
                      <?php foreach($dataakun as $akunBO){  ?>
                        <?php if ($akunBO->NoAkun==$data2[0]->akunBO) { ?>
                         <option value="<?php echo $akun->NoAkun; ?>" selected><?php echo $akun->nama; ?></option>
                        <?php } ?>
                      <option value="<?php echo $akun->NoAkun; ?>" ><?php echo $akun->nama; ?></option>
                      <?php } ?>
                    </select>
                  </div>

                  <div class="form-group">
                      <label>Akun BAR</label>
                    <select id="akunBar" class="form-control select2"  style="width: 100%;" name="akunBar">
                      <option value="1101"></option>
                      <?php foreach($dataakun as $akunBar){  ?>
                        <?php if ($akunBar->NoAkun==$data2[0]->akunBar) { ?>
                         <option value="<?php echo $akun->NoAkun; ?>" selected><?php echo $akun->nama; ?></option>
                        <?php } ?>
                      <option value="<?php echo $akun->NoAkun; ?>" ><?php echo $akun->nama; ?></option>
                      <?php } ?>
                    </select>
                  </div>

                   <div class="form-group">
                      <label>Akun Security</label>
                    <select id="akunSec" class="form-control select2"  style="width: 100%;" name="akunSec">
                      <option value="1101"></option>
                      <?php foreach($dataakun as $akunSec){  ?>
                        <?php if ($akunSec->NoAkun==$data2[0]->akunSec) { ?>
                         <option value="<?php echo $akun->NoAkun; ?>" selected><?php echo $akun->nama; ?></option>
                        <?php } ?>
                      <option value="<?php echo $akun->NoAkun; ?>" ><?php echo $akun->nama; ?></option>
                      <?php } ?>
                    </select>
                  </div>

                  <div class="form-group">
                      <label>Akun Pool & Garden</label>
                    <select id="akunPG" class="form-control select2"  style="width: 100%;" name="akunPG">
                      <option value="1101"></option>
                      <?php foreach($dataakun as $akunPG){  ?>
                        <?php if ($akunPG->NoAkun==$data2[0]->akunPG) { ?>
                         <option value="<?php echo $akun->NoAkun; ?>" selected><?php echo $akun->nama; ?></option>
                        <?php } ?>
                      <option value="<?php echo $akun->NoAkun; ?>" ><?php echo $akun->nama; ?></option>
                      <?php } ?>
                    </select>
                  </div>

                  <div class="form-group">
                      <label>Akun Enginnering</label>
                    <select id="akunEng" class="form-control select2"  style="width: 100%;" name="akunEng">
                      <option value="1101"></option>
                      <?php foreach($dataakun as $akunEng){  ?>
                        <?php if ($akunEng->NoAkun==$data2[0]->akunEng) { ?>
                         <option value="<?php echo $akun->NoAkun; ?>" selected><?php echo $akun->nama; ?></option>
                        <?php } ?>
                      <option value="<?php echo $akun->NoAkun; ?>" ><?php echo $akun->nama; ?></option>
                      <?php } ?>
                    </select>
                  </div>

                   <div class="form-group">
                      <button type="submit" value="Validate" class="btn btn-default"><i class='glyphicon glyphicon-ok'></i> Save</button>
                    </div>
                    </form>
                </div>
               </div>
         </div>
    </div>


</section>



<script type="text/javascript">
$("#Simpan").submit(function() {
    var price = $('#price').val();
     var description = $('#description').val();
        if (description == ''|| price==''){
            File_Kosong(); return false; 
        }else{
        event.preventDefault();
        $.confirm({
          title: 'Confirmation',
          content: 'Are You Sure to Save?',
           type: 'blue',
          buttons: {
              Simpan: function () {
                  $.LoadingOverlay("show");
                  $("#Simpan").submit();
              },
              Batal: function () {
                
                  $.alert('Data Tidak Disimpan...');
              },
          }
      });
    } 

});
</script>

<?php
$this->load->view('template/Js');
$this->load->view('template/Foot');
?>



<script type="text/javascript">
  function File_Kosong() {
  $.alert({
    title: 'Caution!!',
    content: 'Transaction Is Invalid!',
    icon: 'fa fa-warning',
    type: 'orange',
});
}
</script>

<script>
  $(function () {
    $(".select2").select2();
  });
  </script>

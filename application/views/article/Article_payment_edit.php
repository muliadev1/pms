<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>

<!-- Content Header (Page header) -->
<section class="content-header">
   <div class="btn-group btn-breadcrumb">
            <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
             <a href="<?php echo base_url('index.php/Article/Article_payment_view');?>" class="btn btn-default  btn-xs">Article Payment</a>
            <a  class="btn btn-default  btn-xs active">Edit Article Payment</a>
        </div>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
   <div class="col-md-12">
    <div class="box box-info">
     <div class="box-header">
      <h3 class="box-title">Edit Article Payment</h3>
     </div>

     <div class="box-body">
      <form method="post" id="Simpan"  action="<?php echo base_url(). 'index.php/Article/Article_payment_editDB'; ?>">
      	<input type="hidden" name="id" value="<?php echo $edit->id?>">
       <div class="form-group">
        <label class="control-label">Article Payment Name</label>
        <input type="text" name="articlepayment" id="articlepayment"  data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out article payment name..."  class="form-control"  placeholder="article name" value="<?php echo $edit->articlepayment?>">
       </div>
          <div class="form-group">
            <button type="submit" value="Validate" class="btn btn-default"><i class='glyphicon glyphicon-ok'></i> Save</button>
          </div>
        </form>
       </div>
      </div>
     </div>
    </div>
</section>

<script type="text/javascript">
$("#Simpan").submit(function() {
    var saluation = $('#saluation').val();
    var firstname = $('#firstname').val();
    var lastname = $('#lastname').val();
    var gender = $('#gender').val();
    var birthday = $('#birthday').val();
    var idtype  = $('#idtype').val();
    var idnumber = $('#idnumber').val();
    var description = $('#description').val();
    var country = $('#state').val();
    var phone = $('#phone').val();
    var email = $('#email').val();
    var zipcode = $('#zipcode').val();
    var address = $('#address').val();
        if (saluation == ''|| firstname=='' || lastname=='' || gender=='' || birthday=='' || idtype==''
             || idnumber==''|| description==''|| country==''|| phone==''|| email==''|| zipcode==''|| address==''){
            File_Kosong(); return false;
        }else{
        event.preventDefault();
        $.confirm({
          title: 'Confirmation',
          content: 'Are You Sure to Save?',
           type: 'blue',
          buttons: {
              Save: function () {
                  $.LoadingOverlay("show");
                  $("#Simpan").submit();
              },
              Cancel: function () {

                  $.alert('Data Not Saved...');
              },
          }
      });
    }

});
</script>

<?php
$this->load->view('template/Js');
$this->load->view('template/Foot');
?>



<script type="text/javascript">
  function File_Kosong() {
  $.alert({
    title: 'Caution!!',
    content: 'Add Data Invalid!',
    icon: 'fa fa-warning',
    type: 'orange',
});
}
</script>

<script>
$(function () {
  $(".select2").select2();
   $(".select3").select2();

});
</script>

<script>
$( function() {
$( "#birthday" ).datepicker({
autoclose: true,
dateFormat: 'yy/mm/dd'
});
});
</script>
<script type="text/javascript">
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });
</script>

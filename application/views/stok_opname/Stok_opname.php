<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>


<section class="content-header">
    <div class="btn-group btn-breadcrumb">
            <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
            <a  class="btn btn-default  btn-xs active">Stock Opname</a>
        </div>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-xs-12">
      <div class="box box-warning">
        <div class="box-header">
          <h3 class="box-title">Stock Opname</h3> 
          
        </div>
    <!-- /.box-header -->
        <div class="box-body">
      <div class="row" style="background-color:#F2F2FF; margin-left:5px; margin-right:5px; margin-bottom:10px;">
         <div class="col-md-3">
                   <div class="form-group">
                      <label>Departement</label> 
                    <select id="IdDepartement" class="form-control select2"  style="width: 100%;" name="IdDepartement">  
                    <option value="-" selected>--Pilih Departememnt--</option> 
                     <option value="0" >Store</option>            
                     <?php
                          foreach($dataDepaetemen as $u){
                      ?>
                        <option value="<?php echo $u->iddepartement; ?>" ><?php echo $u->departement; ?></option>
                        <?php } ?>
                    </select>
                    <input type="hidden" name="NamaDepartemen" id="NamaDepartemen" value="Store">
                  </div>
                </div>
                <div class="col-md-3">
                   <div class="form-group" id="groupSubcategory">
                      <label>Subcategory</label> 
                    <select id="Subcategory" class="form-control select2"  style="width: 100%;" name="Subcategory">  
                        <option value="1" >--Pilih Subcategory--</option>
                       
                    </select>
                  </div>
                </div>
                 <div class="col-md-5" style="margin-top:25px;">
                <a id="Proses" class="btn btn-info fa fa-eye"> Proses</a>
                </div>
            </div>
            <div id="tabelada" hidden="hidden">
           </div>
               


      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->



<?php
$this->load->view('template/Js');
$this->load->view('template/Foot');
?>

<script type="text/javascript">
  $(document).ready(function(){
     $("#Proses").click(function(e){ 
      var iddep = $('#IdDepartement').val();
      var namadep = $('#NamaDepartemen').val();
      var Subcategory =$('#Subcategory').val();
      $.LoadingOverlay("show");
      $.ajax({
        type: "POST",
        dataType: 'html',
        url: '<?php echo site_url('Stok_opname/SO_view_data');?>',
        data: {id: iddep,NamaDep:namadep,Subcategory:Subcategory },
        success: function(data){
            e.preventDefault();
            // $('#tabelkosong').fadeOut(30);
             $('#tabelada').fadeIn(0,300);  
            $('#tabelada').html(data);
            $.LoadingOverlay("hide");
        }
      });
    });
  });
</script>

 <script>
  $(function () {
    $(".select2").select2();
     $(".select3").select2();

  });
  </script>
<script type="text/javascript">
   $(document).on('change', '#IdDepartement', function(){
    var NamaVen =$("#IdDepartement option:selected").text(); 
    $("#NamaDepartemen").val('');  
      $("#NamaDepartemen").val(NamaVen);
    });
</script>

<script type="text/javascript">
  function File_Kosong() {
  $.alert({
    title: 'Caution!!',
    content: 'Transaction Is Invalid!',
    icon: 'fa fa-warning',
    type: 'orange',
});
}
</script>

<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>

<script type="text/javascript">
   $(document).on('change', '#IdDepartement', function(){
    var NamaVen =$("#IdDepartement option:selected").text(); 
    var id = $("#IdDepartement").val();
    $("#NamaDepartemen").val('');  
      $("#NamaDepartemen").val(NamaVen);

       $.ajax({
            type: "POST",
            dataType:'html',
           url: '<?php echo site_url("Stok_opname/Select_subcategory_bydepartememnt");?>',
            data: {iddepartement: id},
            success: function(data){
              // $('#Subcategory').remove();
                 $('#groupSubcategory').html(data);  
                 $(".select2").select2();
            }
          });

    });
</script>


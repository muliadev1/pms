<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>
<style type="text/css">
  #TabelKonten tr td {
    padding-right: 3px;
    font-size: 14px;
  }
</style>
<style type="text/css">

.modal.modal-wide .modal-dialog {
  width: 50%;
}

.modal-wide .modal-body {
  overflow-y: auto;
}




#tallModal .modal-body p { margin-bottom: 900px }
.pull-right {
    float: right;
}

.pull-left {
    float: left;
}

select {
    width: 300px;
}

</style>

<section class="content-header">
    <div class="btn-group btn-breadcrumb">
            <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
             <a href="<?php echo base_url('index.php/Fo_outlet/Fo_outlet_view');?>" class="btn btn-default  btn-xs">Guest List</a>
            <a  class="btn btn-default  btn-xs active">Edit Item</a>
        </div>
</section>
<!-- Main content -->
<section class="content">

<div class="row">
    <div class="col-xs-8">
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Guest Detail</h3>
        </div>
    <!-- /.box-header -->
  <div class="box-body">
    <div class="row" > 
      <div class="col-xs-12"  >
        <table border="0" id="TabelKonten"  style="width:100%; background-color:#7adddd ; margin-bottom:30px; ">
          <tr>
            <td width="15%"><strong> Guest Folio</strong></td>
            <td width="28%">: <?php echo $IdReservation; ?></td>
            <td width="7%"><strong>Room</strong></td>
            <td width="15%">: <?php echo $room[0]->roomname; ?></td>
            <td width="10%"><strong>Check In</strong></td>
            <td width="15%">: <?php echo $guest->tglin; ?></td>
           </tr>
          <tr>
            <td ><strong>Guest</strong></td>
            <td >: <?php echo $guest->nama; ?></td>
            <td ><strong>Type</strong></td>
            <td >: <?php echo $roomtype[0]->roomtype; ?></td>
            <td ><strong>Check Out</strong></td>
            <td >: <?php echo $guest->tglout; ?></td>
          </tr>
           <tr>
            <td ><strong>Reservation Note</strong></td>
            <td >: <?php echo $guest->notes; ?></td>
          </tr>
          </tr>
        </table>
    </div>
</div>

        <div class="table-responsive">
          <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr >
                                        <th>NO</th>
                                        <th>Date</th>
                                        <th>Description</th>
                                        <th>Charge</th>
                                        <th>Payment</th>
                                        <th>User</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <?php $no=1; $charge = 0; $payment = 0;  foreach ($invoice as $inv ){ ?>
                                  <?php //if ($inv->charge > 0 and $inv->payment > 0): ?>
                                   <tr >
                                        <td><?php echo $no ?></td>
                                        <td><?php echo $inv->tgl1 ?></td>
                                        <td><?php echo $inv->desc ?></td>
                                        <td><?php echo number_format($inv->charge, 0, '.', '.') ?></td>
                                        <td><?php echo number_format($inv->payment, 0, '.', '.') ?></td>
                                       <td><?php echo $inv->nama_user ?></td>
                                    </tr>
                                  <?php //endif ?>
                                    
                                  <?php $no++; $charge +=$inv->charge ; $payment += $inv->payment; } ?>
                                    
                                </tbody>
                                <tfoot>
                                  <tr style="background-color: #f0f0f0; ">
                                    <td colspan="3"> <strong>Total</strong> </td>
                                    <td align="right" > <strong><?php echo  number_format($charge, 0, '.', '.') ?></strong> </td>
                                    <td align="right"> <strong><?php echo  number_format($payment, 0, '.', '.') ?></strong> </td>
                                    <td >&nbsp;</td>
                                  </tr>
                               
                                 <tr style="background-color: #FF9900; ">
                                    <td colspan="3"> <strong>Balance</strong> </td>
                                    
                            <td colspan="2" align="right" > <strong><?php echo  number_format($payment-$charge, 0, '.', '.') ?></strong> </td>
                                  
                                    <td >&nbsp;</td>
                                  </tr>
                                </tfoot>
                            </table>
          </div>



        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
 <div class="col-xs-4">
      <div class="box box-success">
        <div class="box-header">
          <h3 class="box-title">Article</h3>
        </div>
    <!-- /.box-header -->
        <div class="box-body">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#fa-icons" data-toggle="tab">Article Sales</a></li>
              <li><a href="#glyphicons" data-toggle="tab">Article Payment</a></li>
            </ul>
            <div class="tab-content">
              <!-- Font Awesome Icons -->
              <div class="tab-pane active" id="fa-icons">
                  <div class="table-responsive">
                        <table id="example4" class="table table-bordered table-striped">
                              <thead>
                                  <tr >
                                      <th>ID</th>
                                      <th>Article Name</th>
                                      <th style="display: none;">&nbsp;</th>
                                      <th>&nbsp;</th>
                                  </tr>
                              </thead>
                              <tbody>
                              <?php
                                    foreach($article_sales as $pek){
                                    ?>
                                    <tr  class='odd gradeX context'>
                                        <td class="idAs"><?php echo $pek->idarticle ?></td>
                                        <td class="NamaAs"><?php echo $pek->articlename?></td>
                                        <td style="display: none;" class="PriceAs"><?php echo number_format($pek->finalprice, 0, '.', '.')?></td>

                                        
                                        <td align="center">
                                          <a class="btn btn-warning btn-xs AddAs" >  <span class="fa fa-fw fa-edit" ></span> </a>
                                        </td>
                                    </tr>
                                    <?php } ?>

                                  
                              </tbody>
                         </table>
                  </div>
              </div>
               <div class="tab-pane" id="glyphicons">
               <div class="table-responsive">
                        <table id="example4" class="table table-bordered table-striped">
                              <thead>
                                  <tr >
                                      <th>ID</th>
                                      <th>Payment</th>
                                  </tr>
                              </thead>
                              <tbody>
                              <?php
                                    foreach($article_payment as $pek1){
                                    ?>
                                    <tr  class='odd gradeX context'>
                                        <td class="idpay"><?php echo $pek1->id ?></td>
                                        <td class="namapay"><?php echo $pek1->articlepayment?></td>
                                        
                                        <td align="center">
                                          <a class="btn btn-warning btn-xs AddPay" >  <span class="fa fa-fw fa-edit" ></span> </a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                  
                              </tbody>
                         </table>
                  </div>
        </div>

      </div>
    </div>

     <div class="modal modal-wide fade" id="myModalAS" role="dialog" style="display:none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" title="close" data-dismiss="modal">&times;</button>
                    <h4 align="center" class="modal-title">Add New Article Sales</h4>
                </div>
                <div class="modal-body"> 
                <!-- <form method="post"  id="Simpan" action="<?php echo base_url(). 'index.php/Fo_outlet/Fo_invoice_add'; ?>">  -->
                  <input type="hidden" name="idRsv" id="idRsv"  value="<?php echo  $invoice[0]->kodemaster; ?>"   class="form-control"  >
                   <input type="hidden" name="idRoom"  id="idRoom"  value="<?php echo  $invoice[0]->idroom; ?>"   class="form-control"  >
                <input type="hidden" name="idRoomType" id="idRoomType"  value="<?php echo  $invoice[0]->tipe; ?>"   class="form-control"  >
                  <input type="hidden" name="idAs" id="idAs"   class="form-control"  >
                 
                 <div class="form-group">
                            <label class="control-label">Article Name</label>
                            <input type="text" name="NamaAs" id="NamaAs"  readonly="readonly"  class="form-control"  >
                    </div> 
              <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                            <label class="control-label">Invoice</label>
                            <input type="text" name="Invoice" id="Invoice"   class="form-control"  >
                    </div> 
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                            <label class="control-label">Charge</label>
                            <input type="text" name="Charge" id="Charge"   class="form-control"  >
                    </div> 
                  </div>
                </div>
                  <div class="form-group">
                      <a id="SimpanAs"  class="btn btn-primary"><i class='glyphicon glyphicon-ok'></i> Save</a>
                    </div>
         <!--  </form> -->
                </div>
            </div>
        </div>     
    </div>


    <div class="modal modal-wide fade" id="myModalPay" role="dialog" style="display:none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" title="close" data-dismiss="modal">&times;</button>
                    <h4 align="center" class="modal-title">Add New Article Payment</h4>
                </div>
                <div class="modal-body"> 
                <!-- <form method="post"  id="Simpan" action="<?php echo base_url(). 'index.php/Fo_outlet/Fo_invoice_add'; ?>">  -->
                  <input type="hidden" name="idRsv" id="idRsvPay"  value="<?php echo  $invoice[0]->kodemaster; ?>"   class="form-control"  >
                   <input type="hidden" name="idRoom"  id="idRoomPay"  value="<?php echo  $invoice[0]->idroom; ?>"   class="form-control"  >
                <input type="hidden" name="idRoomType" id="idRoomTypePay"  value="<?php echo  $invoice[0]->tipe; ?>"   class="form-control"  >
                  <input type="hidden" name="idPay" id="idPay"   class="form-control"  >
                 
                 <div class="form-group">
                            <label class="control-label">Article Payment</label>
                            <input type="text" name="NamaPay" id="NamaPay"  readonly="readonly"  class="form-control"  >
                    </div> 
              <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                            <label class="control-label">Invoice</label>
                            <input type="text" name="InvoicePay" id="InvoicePay"   class="form-control"  >
                    </div> 
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                            <label class="control-label">Nominal</label>
                            <input type="text" name="Nominal" id="Nominal"   class="form-control"  >
                    </div> 
                  </div>
                </div>
                 <div class="form-group">
                            <label class="control-label">Description</label>
                            <input type="text" name="DescriptionPay" id="DescriptionPay"   class="form-control"  >
                    </div>
                  <div class="form-group">
                      <a id="SimpanPay"  class="btn btn-primary"><i class='glyphicon glyphicon-ok'></i> Save</a>
                    </div>
         <!--  </form> -->
                </div>
            </div>
        </div>     
    </div>




        </div><!-- /.box-body -->
      </div><!-- /.box -->
</div>
  </div><!-- /.row -->

</section><!-- /.content -->




<?php
$this->load->view('template/Js');
$this->load->view('template/Foot');
?>

<script type="text/javascript">
  $(document).ready(function(){
     $(".AddAs").click(function(e){ 
     var idas = $(this).closest('tr').children('td.idAs').text();
     var NamaAs = $(this).closest('tr').children('td.NamaAs').text();
     var PriceAs = $(this).closest('tr').children('td.PriceAs').text();
             e.preventDefault();
            var mymodal1 = $('#myModalAS');
            $('#idAs').val(idas);
            $('#NamaAs').val(NamaAs);
             $('#Charge').val(PriceAs);
            mymodal1.modal({backdrop: 'static', keyboard: false}) 
            var height = $(window).height() - 200;
            mymodal1.find(".modal-body").css("max-height", height);
            mymodal1.modal('show');        

    });
  });
</script>
<script type="text/javascript">
$("#SimpanAs").click(function() {
    var charge = $('#Charge').val();
     var kodemaster = $('#idRsv').val();
     var invoicemanual  = $('#Invoice').val();
     var idroom  = $('#idRoom').val();
     var tipe = $('#idRoomType').val();
     var idarticle  = $('#idAs').val(); 
     var articlename   = $('#NamaAs').val(); 
     var desc  = $('#NamaAs').val();
      var charge  = $('#Charge').val();

        if (charge == ''|| charge==''){
            File_Kosong(); return false; 
        }else{
        event.preventDefault();
        $.confirm({
          title: 'Confirmation',
          content: 'Are You Sure to Save?',
           type: 'blue',
          buttons: {
              Simpan: function () {
        $.LoadingOverlay("show");
         $.ajax({
                      type: "POST",
                      url: '<?php echo site_url('Fo_outlet/Fo_invoice_add_sales');?>',
                      data: {Charge: charge,idRsv:kodemaster, Invoice:invoicemanual,idRoom:idroom,idRoomType:tipe,idAs:idarticle,  desc:desc,articlename:articlename,NamaAs:articlename },
                        success: function(data){
                          window.location = "<?php echo base_url(); ?>index.php/Fo_outlet/Fo_invoice_view/"+kodemaster
                                      
                        }
                      });
              },
              Batal: function () {
                
                  $.alert('Data Tidak Disimpan...');
              },
          }
      });
    } 

});
</script>

<script type="text/javascript">
  $(document).ready(function(){
     $(".AddPay").click(function(e){ 
     var idpay = $(this).closest('tr').children('td.idpay').text();
     var namapay = $(this).closest('tr').children('td.namapay').text();
             e.preventDefault();
            var mymodal1 = $('#myModalPay');
            $('#idPay').val(idpay);
            $('#NamaPay').val(namapay);
            mymodal1.modal({backdrop: 'static', keyboard: false}) 
            var height = $(window).height() - 200;
            mymodal1.find(".modal-body").css("max-height", height);
            mymodal1.modal('show');        

    });
  });
</script>

<script type="text/javascript">
$("#SimpanPay").click(function() {
    var payment = $('#Nominal').val();
     var kodemaster = $('#idRsvPay').val();
     var invoicemanual  = $('#InvoicePay').val();
     var idroom  = $('#idRoomPay').val();
     var tipe = $('#idRoomTypePay').val();
     var idarticle  = $('#idPay').val(); 
     var articlename   = $('#NamaPay').val(); 
     var desc  = $('#DescriptionPay').val();

        if (payment == ''|| payment==''){
            File_Kosong(); return false; 
        }else{
        event.preventDefault();
        $.confirm({
          title: 'Confirmation',
          content: 'Are You Sure to Save?',
           type: 'blue',
          buttons: {
              Simpan: function () {
        $.LoadingOverlay("show");
         $.ajax({
                      type: "POST",
                      url: '<?php echo site_url('Fo_outlet/Fo_invoice_add_payment');?>',
                      data: {Payment: payment,idRsv:kodemaster, Invoice:invoicemanual,idRoom:idroom,idRoomType:tipe,idarticle:idarticle,  DescriptionPay:desc,articlename:articlename,articlename:articlename },
                        success: function(data){
                          window.location = "<?php echo base_url(); ?>index.php/Fo_outlet/Fo_invoice_view/"+kodemaster
                                      
                        }
                      });
              },
              Batal: function () {
                
                  $.alert('Data Tidak Disimpan...');
              },
          }
      });
    } 

});
</script>





<script type="text/javascript">
  function File_Kosong() {
  $.alert({
    title: 'Caution!!',
    content: 'Transaction Is Invalid!',
    icon: 'fa fa-warning',
    type: 'orange',
});
}
</script>

<script>
  $(function () {
     $('#example1').DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });

$('#example2').DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
   
  });
</script>

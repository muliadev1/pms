

          <div class="table-responsive">
                    <table id="example11" class="table table-bordered table-striped">
                                <thead>
                                    <tr >
                                        <th style="dislpay:none;" >ID</th>
                                        <th style="dislpay:none;" >Stok</th>
                                        <th >Code Item</th>
                                        <th >Sub Category</th>
                                        <th>Description</th>
                                        <th>Unit</th>
                                        <th>Price</th>
                                        <th>Qty Order</th>
                                        <th>Subtotal</th>
                                        <th>Departement</th>
                                        <th>Action</th>
                                       
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $no = 1;
                                    foreach($data as $u){
                                    ?>
                                    <tr>
                                         <td class="Id" style="dislpay:none;" ><?php echo $no ?></td>
                                         <td class="Stok" style="dislpay:none;" ><?php echo $u->currstok ?></td>
                                         <td class="ItemCode" ><?php echo $u->codeitem ?></td>
                                         <td class="Stok" ><?php echo $u->subcategory ?></td>
                                         <td class="Description" ><?php echo $u->description ?></td>
                                         <td class="Unit"><?php echo $u->unit ?></td>
                                         <td align="right" class="Price"><?php echo number_format($u->price, 3, '.', '.') ?></td>
                                         <td class="Qty"><?php echo number_format($u->qtyorder, 0, '.', '.') ?></td>
                                        <td align="right" class="Subtotal"><?php echo number_format($u->Subtotal, 3, '.', '.') ?></td>
                                         <td class="Depaetement"><?php echo $u->departement ?></td>
                                         <td>
                                            <a  class="btn btn-default Add" id="btnAdd<?php echo $no ; ?>"><i class='fa fa-fw  fa-check-square-o'></i>  Add  </a>  
                                         </td>
                                      

                                    </tr>
                                <?php $no++;} ?>
                                </tbody>
                            </table>
          </div>

<script type="text/javascript">
  $(document).ready(function() {
    var t = $('#tbdetil').DataTable();
    var i = 1;
    var iditem = 0;
      

 
    $('.Add').on( 'click', function () {

      var id =  $(this).closest('tr').children('td.Id').text();
        var itemcode = $(this).closest('tr').children('td.ItemCode').text();
        var Description =$(this).closest('tr').children('td.Description').text();
        var Qty = $(this).closest('tr').children('td.Qty').text();
        var Unit =  $(this).closest('tr').children('td.Unit').text();
        var Price = $(this).closest('tr').children('td.Price').text();
        var Subtotal = $(this).closest('tr').children('td.Subtotal').text();
        var Stok = $(this).closest('tr').children('td.Stok').text();
         var Note ='By Purchase Requisition';
         var index = $('#index').val();
        var btnedit =  "<a class='btn btn-warning btn-xs PilihEdit' title='Edit Item'>  <span class=' fa fa-edit' ></span> </a> <a class='btn btn-danger btn-xs Hapus' title='Remove Item'>  <span class=' fa fa-close' ></span> </a> "
         $('#btnAdd'+id).addClass('disabled');
         $('#btnAdd'+id).removeClass('btn-default').addClass('btn-danger');
          

           iditem = parseInt(index)+1;
          
       var row = t.row.add([
            iditem,
            itemcode,
            Description,
            Qty,
            Unit,
            Price,
            Subtotal,
            Note,
            btnedit,
            Stok
        ] ).draw(false);
        t.row(row).column(0).nodes().to$().addClass('No');
         t.row(row).column(1).nodes().to$().addClass('ItemCode');
        t.row(row).column(2).nodes().to$().addClass('Description');
        t.row(row).column(3).nodes().to$().addClass('Qty');
        t.row(row).column(4).nodes().to$().addClass('Unit');
        t.row(row).column(5).nodes().to$().addClass('Price');
        t.row(row).column(6).nodes().to$().addClass('Subtotal');
         t.row(row).column(8).nodes().to$().addClass('StokEditTd');
  

          $('<input>').attr({
        type: 'hidden',
        class: iditem,
        name: 'index', 
        value: iditem
        }).appendTo('form');

        $('<input>').attr({
        type: 'hidden',
        class: iditem,
        name: 'itemcode['+iditem+']', 
        value: itemcode
        }).appendTo('form');

        $('<input>').attr({
        type: 'hidden',
        class: iditem,
        name: 'Description['+iditem+']', 
        value: Description
        }).appendTo('form');

        $('<input>').attr({
        type: 'hidden',
        class: iditem,
        name: 'Qty['+iditem+']', 
        value: Qty.replace(/\./g, "")
        }).appendTo('form');


         $('<input>').attr({
        type: 'hidden',
        class: iditem,
        name: 'Unit['+iditem+']', 
        value: Unit
        }).appendTo('form');

        $('<input>').attr({
        type: 'hidden',
        class: iditem,
        name: 'Price['+iditem+']', 
        value: Price.replace(/\./g, "")
        }).appendTo('form');

        $('<input>').attr({
        type: 'hidden',
        class: iditem,
        name: 'Subtotal['+iditem+']', 
        value: Subtotal.replace(/\./g, "")
        }).appendTo('form');


        $('<input>').attr({
          type: 'text',
          class: iditem,
          name: 'Note['+iditem+']', 
          value: Note
          }).appendTo('form');

         $('<input>').attr({
        type: 'hidden',
        class: iditem,
        name: 'Stok['+iditem+']', 
        value: Stok
        }).appendTo('form');


         $('#ItemCode').val('');
         $('#Description').val('');
         $('#Qty').val('');
         $('#Qty1').val('');
         $('#Price').val('');
         $('#Unit').val('');
         $('#index').val(iditem);
    

} );
} );
</script>

<script type="text/javascript">
  $(function () {
    $("#example11").DataTable();
  });
  </script>
        
          



        

      <form method="post" id="Simpan" action="<?php echo base_url(). 'index.php/Stok_opname/add_proses_opname'; ?>">
      <input type="hidden" name="Istok"  value="<?php echo $departement['istok'] ;?>">
      <input type="hidden" name="Set"  value="<?php echo $departement['set'] ;?>">
      <input type="hidden" name="Departement"  value="<?php echo $departement['dep'] ;?>">
      <input type="hidden" name="iddepartement"  value="<?php echo $departement['iddep'] ;?>">
        <div class="table-responsive">
          <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr >
                                       <th >NO</th>
                                        <th >Voucher NO</th>
                                        <th >Vendor</th>        
                                        <th >Total</th>
                                        <th >View Detail</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i=1;
                                    foreach($data as $u){
                                    ?>
                                    <tr  class='odd gradeX context'>
                                      <td ><?php echo $i ?></td> 
                                        <td class="vocherno"><?php echo $u->voucherno ?></td>
                                         <td class="vendor"><?php echo $u->vendor ?></td> 
                                        <td><?php echo number_format($u->total, 0, '.', '.')?></td>
                                        <td align="center"> 
                                        <a class="btn btn-info btn-xs ViewDetail" title="View History Price"   style="cursor:pointer" >  <span class="fa fa-fw fa-list " ></span> </a>
                                        </td>

                                    </tr>
                                    <?php $i++;} ?>
                                </tbody>
                            </table>
          </div>



        </div><!-- /.box-body -->
        </form>
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->


<script type="text/javascript">
  $(document).ready(function(){
     $(".ViewDetail").click(function(e){ 
      var idv = $(this).closest('tr').children('td.vocherno').text();  
      var vendor = $(this).closest('tr').children('td.vendor').text();      
      $.ajax({
        type: "POST",
        dataType:"html",
       url: '<?php echo site_url('Payment/Check_view_data_detil');?>',
        data: {idv: idv},
        success: function(data){
            e.preventDefault();
            var mymodal1 = $('#myModalNew');
            mymodal1.modal({backdrop: 'static', keyboard: false}) 
            var height = $(window).height() - 200;
            mymodal1.find(".modal-body").css("max-height", height);
             $("#Voucherm span").text(idv);
              $("#Vendorm span").text(vendor);
            mymodal1.find('.modal-body').html(data);
            mymodal1.modal('show');            
        }
      });
    });
  });
</script>


<script type="text/javascript">
  function File_Kosong() {
  $.alert({
    title: 'Caution!!',
    content: 'Transaction Is Invalid!',
    icon: 'fa fa-warning',
    type: 'orange',
});
}
</script>

<script>
  $(function () {
    
    $('#example1').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
  });
</script>

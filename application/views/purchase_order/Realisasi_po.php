<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>

<style type="text/css">
  .modal.modal-wide .modal-dialog {
  width: 80%;
}

.modal-wide .modal-body {
  overflow-y: auto;
}
</style>
<section class="content-header">
    <div class="btn-group btn-breadcrumb">
            <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
            <a  class="btn btn-default  btn-xs active">Data Realisai PO</a>
        </div>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-xs-12">
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Data Realisai PO</h3> 
          
        </div>
    <!-- /.box-header -->
        <div class="box-body">
        <input type="hidden" name="IsFormat" id="IsFormat" value="1"  class="form-control">
        <!-- <div class="row" > -->

               <!--  <div class="col-md-4" style="margin-left:6px;">
                <div class="form-group">
                <label style="margin-right:50px;">
                  <input type="radio" name="r3" class="flat-red" name="Cash" id="Format1" checked>
                  Format 1
                </label>
                <label>
                  <input type="radio" name="r3" class="flat-red" name="Credit" id="Format2">
                  Format 2
                </label>
              </div>          
                </div>
              </div> -->

      <div class="row" style="background-color:#F2F2FF; margin-left:5px; margin-right:5px; margin-bottom:10px;">
         <div class="col-md-3">
                   <div class="form-group">
                      <label>Vendor</label> 
                    <select id="IdVendor" class="form-control select2"  style="width: 100%;" name="IdVendor">
                      <option value="0" >ALL VENDOR</option>
                      <?php foreach($datavendor as $vendor){?>
                        <option value="<?php echo $vendor->idvendor; ?>" ><?php echo $vendor->vendor; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

              <div class="col-md-2"> 
                    <div class="form-group">
                          <label class="control-label">Date From</label>
                          <input type="text" name="DateFromRpo" id="DateFromRpo" value="<?= date('d/m/Y') ?>" class="form-control">
                     </div>
                  </div>
                <div class="col-md-2">
                    <div class="form-group">
                          <label class="control-label">Date To</label>
                          <input type="text" name="DateToRpo" id="DateToRpo" value="<?= date('d/m/Y') ?>" class="form-control">
                     </div>
                  </div>
                 <div class="col-md-3" style="margin-top:25px;">
                <a id="Proses" class="btn btn-info fa fa-eye"> Proses</a>
                <a id="Cetak" class="btn btn-success fa fa-print"> Cetak</a>
                </div>

            </div>
            <div id="tabelada" hidden="hidden">
           </div>

    <div class="modal modal-wide fade" id="myModalNew" role="dialog" style="display:none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" title="close" data-dismiss="modal">&times;</button>
                    <h4 align="center" class="modal-title">History Realisai PO</h4>
                <table class="table" style="background-color: #00acd6; border: 1px solid black; "  >
                <!-- <tr>
                  <td width="15%" style="border: 1px solid white; ">Asset</td>
                  <td  width="35%"id ="CodeAsetm" style="background-color: #00acd6; border: 1px solid white; " ><strong> <span></span> </strong> </td>
                  <td width="15%" style="border: 1px solid white; ">Departement</td>
                  <td  width="35%"id ="Departementm" style="background-color: #00acd6; border: 1px solid white; " ><strong> <span></span> </strong> </td>
                </tr> -->
              </table>
                </div>
              
                <div class="modal-body">   
                </div>
            </div>
        </div>     
    </div>
               


      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->



<?php
$this->load->view('template/Js');
$this->load->view('template/Foot');
?>

<script type="text/javascript">
  $(document).ready(function(){
     $("#Proses").click(function(e){ 
        

      var IdVendor = $('#IdVendor').val();
      var DateFromRpo = $('#DateFromRpo').val();
      var DateToRpo = $('#DateToRpo').val();
    
        $.LoadingOverlay("show");
        
           $.ajax({
        type: "POST",
        dataType: 'html',
        url: '<?php echo site_url('Purchase_order/realisasi_po_view_data');?>',
        data: {IdVendor: IdVendor,DateFromRpo:DateFromRpo,DateToRpo:DateToRpo },
        success: function(data){
            e.preventDefault();
            // $('#tabelkosong').fadeOut(30);
             $('#tabelada').fadeIn(0,300);  
            $('#tabelada').html(data);
            $.LoadingOverlay("hide");
        }
      });
    });
  });
</script>

<script type="text/javascript">
  $(function () {  
  $("#Cetak").click(function(){
      var iddep = $('#IdDepartement').val();
      var namadep = $('#NamaDepartemen').val();
      var idsub = $('#Subcategory').val();
      var format = $('#IsFormat').val();
       var NamaSub =$("#Subcategory option:selected").text(); 
      window.open("<?php echo base_url(). 'index.php/Aset/CetakAset_bydepartement/';?>?id="+ iddep +"&namadep="+ namadep+"&namasub="+ NamaSub+"&idsub="+ idsub+"&format="+ format,"MyTargetWindowName")
  }); 
});
</script>



 <script>
  $(function () {
    $(".select2").select2();
     $(".select3").select2();
     $( "#DateFromRpo" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });
           $( "#DateToRpo" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });

  });
  </script>

  <script type="text/javascript">
  $('#Format1').on('ifChanged', function(event){ 
    $('#IsFormat').val('1');
    $('#Format2').prop("checked", false)
});
  $('#Format2').on('ifChanged', function(event){
    $('#IsFormat').val('2');
    $('#Format1').prop("checked", false)
});
</script>

<script type="text/javascript">
   $(document).on('change', '#IdDepartement', function(){
    var NamaVen =$("#IdDepartement option:selected").text(); 
    var id = $("#IdDepartement").val();
    $("#NamaDepartemen").val('');  
      $("#NamaDepartemen").val(NamaVen);

       $.ajax({
            type: "POST",
            dataType:'html',
           url: '<?php echo site_url("Aset/Select_subcategory_bydepartememnt");?>',
            data: {iddepartement: id},
            success: function(data){
              // $('#Subcategory').remove();
                 $('#groupSubcategory').html(data);  
                 $(".select2").select2();
            }
          });

    });
</script>



<script type="text/javascript">
  function File_Kosong() {
  $.alert({
    title: 'Caution!!',
    content: 'Transaction Is Invalid!',
    icon: 'fa fa-warning',
    type: 'orange',
});
}
</script>

<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });
  });
</script>


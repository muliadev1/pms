<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>

<!-- Content Header (Page header) -->
<section class="content-header">
   <div class="btn-group btn-breadcrumb">
            <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
             <a  class="btn btn-default  btn-xs" href="<?php echo base_url('index.php/Package/Package_view');?>">Package</a>
            <a  class="btn btn-default  btn-xs active">Add Package</a>
        </div>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
   <div class="col-md-12">
    <div class="box box-info">
     <div class="box-header">
      <h3 class="box-title">Add New Package</h3>
     </div>

     <div class="box-body">
             <input type="hidden" name="Index" id="Index" value="0" >
      <form method="post" id="Simpan"  action="<?php echo base_url(). 'index.php/Package/package_add'; ?>">

       <div class="form-group">
        <label class="control-label">Package Name</label>
        <input type="text" name="PackageName" id="PackageName" data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out category description..."  class="form-control"  placeholder="Package Name">
       </div> 
       <div class="form-group">
        <label class="control-label">Package Rate</label>
        <input type="text" name="PackageRate" id="PackageRate"  data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out category description..."  class="form-control"  placeholder="Package Rate">
       </div> 
       <div class="form-group">
            <a type="submit" value="Validate" class="btn btn-success btn-xs" id="AddArticle"><i class='glyphicon glyphicon-plus'></i> Add Article</a>
        </div>

      <p id="ArticleList"></p>


           

          <div class="form-group">
            <button type="submit" value="Validate" class="btn btn-default"><i class='glyphicon glyphicon-ok'></i> Save</button>
          </div>
        </form>
       </div>
      </div>
     </div>
    </div>
</section>

<script type="text/javascript">
  $(document).ready(function(){
     $("#AddArticle").click(function(){ 
     var index = $('#Index').val(); 
        $.ajax({
        type: "POST",
        data :"html",
       url: '<?php echo site_url('Package/Add_article_package');?>',
        data: {index: index  },
        success: function(data){
            $('#ArticleList').append(data); 
            var indexnew = parseInt(index) +1;
            $('#Index').val(indexnew); 
        }
      });
           
    });
  });
</script>

<script type="text/javascript">
  $(document).on('click', '.remove' , function() {
    index= $(this).attr("idk")
    $('#element'+index).remove(); 
  });
</script>

<script type="text/javascript">
$("#Simpan").submit(function() {
    var name = $('#name').val();
    var description = $('#description').val();
    var state = $('#state').val();
    var phone = $('#phone').val();
    var zipcode = $('#zipcode').val();
    var email = $('#email').val();
    var address = $('#address').val();
    var type = $('#type').val();

        if (name == ''|| description==''|| state == ''|| phone==''|| zipcode == ''|| email==''|| address == ''|| type==''){
            File_Kosong(); return false;
        }else{
        event.preventDefault();
        $.confirm({
          title: 'Confirmation',
          content: 'Are You Sure to Save?',
           type: 'blue',
          buttons: {
              Save: function () {
                  $.LoadingOverlay("show");
                  $("#Simpan").submit();
              },
              Cancel: function () {

                  $.alert('Data Not Saved...');
              },
          }
      });
    }

});
</script>

<?php
$this->load->view('template/Js');
$this->load->view('template/Foot');
?>



<script type="text/javascript">
  function File_Kosong() {
  $.alert({
    title: 'Caution!!',
    content: 'Add Company Invalid!',
    icon: 'fa fa-warning',
    type: 'orange',
});
}
</script>

<script>
$(function () {
  $(".select2").select2();
   $(".select3").select2();

});
</script>
<script type="text/javascript">
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });
</script>

<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>
<script src="<?php echo base_url('assets/js/accounting.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/cleave.min.js') ?>" type="text/javascript"></script>
<style type="text/css">
.modal.modal-wide .modal-dialog {
  width: 60%;
}
.modal-wide .modal-body {
  overflow-y: auto;
}


 


#tallModal .modal-body p { margin-bottom: 900px }
.pull-right {
    float: right;
}

.pull-left {
    float: left;
}

select {
    width: 300px;
}

</style>
    <script type="text/javascript">
      jQuery(document).ready(function($) {
        new Cleave('.input-1', {
           numeral: true,
           numeralDecimalMark: ',',
           delimiter: '.'
      });
          });
    </script>
    <script type="text/javascript">
      function doMath()
      {
          var qty = document.getElementById('Qty1').value;
          document.getElementById('Qty').value =  qty.replace(/\./g, "");
      }
    </script>

<!-- Content Header (Page header) -->
<section class="content-header">
   <div class="btn-group btn-breadcrumb">
            <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
             <a href="<?php echo base_url('index.php/Purchase_requisition/purchase_requisition_view');?>" class="btn btn-default  btn-xs">Payment</a>
            <a  class="btn btn-default  btn-xs active">Add check</a>
        </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
             <div class="box box-info">
              <form method="post" >
                <div class="box-header">
                  <h3 class="box-title" ><strong>Add check</strong> </h3>
                  <hr style="border-top: 1px solid #8c8b8b; padding: 1px; margin-top: 5px; margin-bottom: 0px;">
                </div>
                <div class="box-body">
             

              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                     <label>Check No.</label>
                   <input  type="text" name="check" id="check"  class="form-control"> </input>
                 </div>
               </div>
               <div class="col-md-6" >
                   <div class="form-group">
                      <label>Bank</label>
                    <select id="Bank" class="form-control select2"  style="width: 100%;" name="Bank">
                      <?php
                          foreach($bank as $bank1){
                      ?>
                        <option value="<?php echo $bank1->id; ?>" ><?php echo $bank1->namabank; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
              </div>
               <!-- hidden akun untuk pocer -->
            <div class="row">
              <div class="col-md-6" >
                   <div class="form-group">
                      <label>Vendor</label>
                    <select id="IdVendor" class="form-control select2"  style="width: 100%;" name="IdVendor">
                      <?php
                          foreach($datavendor as $vendor){
                      ?>
                        <option value="<?php echo $vendor->idvendor; ?>" ><?php echo $vendor->vendor; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

              </div>
              <!-- hidden akun untuk pocer -->

              
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                     <label></label>
                  </div>
               </div>
             </div>

               <div class="row">
                 
                    <div class="col-md-4">
                    <div class="form-group">
                          <label class="control-label">Voucher NO</label>
                          <input type="text" name="VoucherNo" id="VoucherNo"  class="form-control">
                     </div>
                  </div>

                  <div class="col-md-4">
                    <div class="form-group">
                          <label class="control-label">Date Voucher</label>
                          <input type="text" name="DateVoucher" id="DateVoucher"  class="form-control">
                     </div>
                  </div>

                  <div class="col-md-3">
                    <div class="form-group">
                          <label class="control-label">Amount</label>
                          <input  type="text" name="amount" id="amount"  class="form-control input-1"  >

                        </div>
                  </div>

                  <div class="col-md-1">
                    <div class="form-group" >
                        <a style="margin-top:25px; margin-right:10px; " class="btn btn-primary btn-flat btn-xs " id="AddBawah"><i class='glyphicon glyphicon-arrow-down'></i>  Proses  </a>
                     </div>
                  </div>
                </div>
                <div class="row">
                <div class="col-md-6" >


                </div>


                </div>



                      <div class="table-responsive">

                         <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Voucher No.</th>
                                    <th>Voucher Date</th>
                                    <th>Amount</th>
                                    <th >Action </th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                      </div>

                       <div class="col-md-12" align="left" style="margin-top :30px;">
                          <div class="form-group" >
                              <a    class="btn btn-success btn-flat"  id="simpan1"  ><i class='fa fa-check-square-o'></i>  Simpan  </a>
                          </div>
                      </div>
        </form>
      </div>
     </div>
</div>

    <div class="modal modal-wide fade" id="myModal" role="dialog" style="display:none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" title="close" data-dismiss="modal">&times;</button>
                    <h4 align="center" class="modal-title">Voucher List</h4>
                    <h5 align="center" id="namavendormodal" style="background-color: #0bc3c3;"></h5>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>



</section>



<?php
$this->load->view('template/Js');
$this->load->view('template/Foot');
?>
<script type="text/javascript">
 $("#simpan1").click(function(e){

        
        event.preventDefault();
        $.confirm({
          title: 'Konfirmasi',
          content: 'Anda yakin untuk menyimpan data ?',
           type: 'blue',
          buttons: {
              Simpan: function () { 
                var datadetil= $("#example1").DataTable();
                var datadetils = datadetil.rows().data().toArray();
                var check = $("#check").val();
                var bank = $("#Bank").val();
                var idvendor = $("#IdVendor").val();
                alert(idvendor);
                 $.ajax({
                    type: "POST",
                    url: '<?php echo site_url('Payment/payment_add_voucher_addDB');?>',
                    data:  {datadetil:datadetils,check:check,bank:bank,idvendor:idvendor },
                    success: function(data){
                      window.location = '<?php echo site_url('Payment/payment_add_voucher');?>';
                    }
                  }); 
              },
              Batal: function () {
                
                  $.alert('Data tidak disimpan...');
              },
          }
      }); 

});
</script>

<script type="text/javascript">
     $("#VoucherNo").click(function(e){
       var IdVendor=$('#IdVendor').val();
       var namaven = $("#IdVendor option:selected").text();
      $.ajax({
        type: "POST",
       url: '<?php echo site_url('Payment/view_voucher_byvendor');?>',
        data: {idvendor: IdVendor},
        success: function(data){
         $('#namavendormodal').text(namaven); 
            e.preventDefault();
            var mymodal = $('#myModal');
            var height = $(window).height() - 200;
            mymodal.find(".modal-body").css("max-height", height);
            mymodal.find('.modal-body').html(data);
            mymodal.modal('show');
              //  alert(data);
        }
      });
    });
</script>

<script type="text/javascript">
    $('#AddBawah').on( 'click', function () {
      var t = $('#example1').DataTable();
      var i = 1;
         
        var  VoucherNo   = $('#VoucherNo').val(); 
        var  DateVoucher = $('#DateVoucher').val(); 
        var amount = $('#amount').val();
        var btndelete =  "<a class='btn btn-danger btn-xs' title='Remove Item'>  <span class=' fa fa-minus-circle' ></span> </a> "
        // if (VoucherNo=='' ||VoucherNo==null  ) {
        //  File_Kosong();return false;
        // }
       var row = t.row.add( [
            VoucherNo,
            DateVoucher,
            amount,
            btndelete
        ] ).draw(false);
        t.row(row).column(0).nodes().to$().addClass('No');
       // t.columns([0,5]).visible(false); 
         $('#VoucherNo').val(''); 
        $('#DateVoucher').val(''); 
        $('#amount').val('');
      });
</script>

 <script>
  $('table').on('click', 'a', function(e){
    var t = $('#example1').DataTable();
    t.row($(this).closest('tr')).remove().draw( false );

})
  </script>

  <script>
  $(function () {
    $(".select2").select2();
     $(".select3").select2();

  });
  </script>

    <script type="text/javascript">
$('#example1').dataTable({
    "paging": false,
    "ordering": false,
    "searching": false
});
  </script>



   <script type="text/javascript">
  function File_Kosong() {
  $.alert({
    title: 'Caution!!',
    content: 'Invalid Data!',
    icon: 'fa fa-warning',
    type: 'orange',
});
}
</script>

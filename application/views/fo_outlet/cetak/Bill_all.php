
<?php echo $header?>

<style type="text/css">
  .TabelKonten tr td {
    padding-right: 7px;
    padding-left:  7px;
    font-size: 11px;
  }
</style>

<table class="TabelKonten"  border="0" style="border-collapse: collapse; border-color:#000000; padding-bottom : 50px ;"  width="100%" >
            <tr>
                <td width="15%"><strong> Id Rsv</strong></td>
                <td>: <?php echo $master->idreservation ?></td>
                <td width="15%"><strong>Room</strong></td>
                <td>: <?php echo $master->roomname ?></td>
              </tr>
              <tr>
                <td width="15%"><strong>Guest</strong></td>
                <td>: <?php echo $master->firstname.' '.$master->lastname ?></td>
                <td width="15%"><strong>Room Type</strong></td>
                <td>: <?php echo $master->desc ?></td>
              </tr>
              <tr>
                <td width="15%"><strong>Company</strong></td>
                <td>: <?php echo $master->company ?></td>
                <td width="15%"><strong>Arrival</strong></td>
                <td>: <?php echo $master->checkin ?></td>
              </tr> 
              <tr>
                <td width="15%"><strong>Nationality</strong></td>
                <td>: <?php echo $master->state ?></td>
                <td width="15%"><strong>Departure</strong></td>
                <td>: <?php echo $master->checkout ?></td>
              </tr>                  
          </table>
<br>

<table class="TabelKonten"  border="1" style="border-collapse: collapse; border-color:#000000; padding-top : 50px; margin-bottom: 20px;"  width="100%" >         
                               <thead>
                                <tr style="font-weight: bold; background-color: #f0f0f0; "><th colspan="6">ROOM BILL</th></tr>
                                    <tr >
                                        <th>NO</th>
                                        <th>Date</th>
                                        <th>Invoice</th>
                                        <th>Description</th>
                                        <th>Charge (IDR)</th>
                                        <th>Payment (IDR)</th>
                                        <th style="display: none;">id trx</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $noroom=1; $totalchargeroom=0; $totalpaymentroom=0; 
                                    foreach($room as $room){ ?>
                                    <tr>
                                      <td><?php echo $noroom ?></td>
                                      <td><?php echo $room->tgl1 ?></td>
                                      <td><?php echo $room->invoicemanual ?></td>
                                      <td><?php echo $room->description ?></td>
                                      <td align="right"><?php echo number_format($room->charge, 2, ',', '.') ?></td>
                                      <td align="right"><?php echo number_format($room->payment, 2, ',', '.') ?></td>
                                       <td style="display: none;"><?php echo $room->kode ?></td>
                                    </tr>
                                    <?php $noroom++; $totalchargeroom+=$room->charge; $totalpaymentroom+=$room->payment;} ?>
                                </tbody>
                                <tfoot>
                                  <tr style="font-weight: bold; background-color: #f0f0f0; ">
                                    <td colspan="4">SUMMARY</td>
                                    <td  align="right"><?php echo number_format($totalchargeroom, 2, ',', '.') ?></td>
                                    <td  align="right"><?php echo number_format($totalpaymentroom, 2, ',', '.') ?></td>
                                  </tr>
                                  <tr style="font-weight: bold; background-color: #f0f0f0;">
                                    <td colspan="5">BALANCE</td>
                                    <td  align="right"><?php echo number_format($totalpaymentroom-$totalchargeroom, 2, ',', '.') ?></td>
                                  </tr>
                                </tfoot>
                            </table>






      <table class="TabelKonten"  border="1" style="border-collapse: collapse; border-color:#000000; padding-top : 50px; margin-bottom: 20px;"  width="100%" > 
                                <thead>
                                  <tr style="font-weight: bold; background-color: #f0f0f0; "><th colspan="6">CONSUME BILL</th></tr>
                                    <tr>
                                        <th>NO</th>
                                        <th>Date</th>
                                        <th>Invoice</th>
                                        <th>Description</th>
                                        <th>Charge (IDR)</th>
                                        <th>Payment (IDR)</th>
                                        <th style="display: none;">id trx</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $nocom=1; $totalchargecom=0; $totalpaymentcom=0; 
                                    foreach($comsume as $com){ ?>
                                    <tr>
                                      <td><?php echo $nocom ?></td>
                                      <td><?php echo $com->tgl1 ?></td>
                                      <td><?php echo $com->trxid ?></td>
                                      <td> <?php echo $com->description ?></td>
                                      <td align="right"><?php echo number_format($com->charge, 2, ',', '.') ?></td>
                                      <td align="right"><?php echo number_format($com->payment, 2, ',', '.') ?></td>
                                      <td style="display: none;"> <?php echo $com->trxid ?></td>
                                    </tr>
                                    <?php $nocom++; $totalchargecom+=$com->charge; $totalpaymentcom+=$com->payment;} ?>
                                </tbody>
                                <tfoot>
                                  <tr style="font-weight: bold; background-color: #f0f0f0;">
                                    <td colspan="4">TOTAL</td>
                                    <td  align="right"><?php echo number_format($totalchargecom, 2, ',', '.') ?></td>
                                    <td  align="right"><?php echo number_format($totalpaymentcom, 2, ',', '.') ?></td>
                                  </tr>
                                  <tr style="font-weight: bold; background-color: #f0f0f0;">
                                    <td colspan="5">BALANCE</td>
                                    <td  align="right"><?php echo number_format($totalpaymentcom-$totalchargecom, 2, ',', '.') ?></td>
                                  </tr>
                                </tfoot>
                            </table>






    <table class="TabelKonten"  border="1" style="border-collapse: collapse; border-color:#000000; padding-top : 50px; margin-bottom: 20px;"  width="100%" > 
                                <thead>
                                  <tr style="font-weight: bold; background-color: #f0f0f0; "><th colspan="6">SUMMARY  BILL</th></tr>
                                    <tr >
                                        <th>NO</th>
                                        <th>Date</th>
                                        <th>Invoice</th>
                                        <th>Description</th>
                                        <th>Charge (IDR)</th>
                                        <th>Payment (IDR)</th>
                                        <th style="display: none;">id trx</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $nosum=1; $totalchargesum=0; $totalpaymentsum=0; 
                                    foreach($summary as $sum){ ?>
                                    <tr>
                                      <td><?php echo $nosum ?></td>
                                      <td><?php echo $sum->tgl1 ?></td>
                                      <td><?php echo $sum->invoicemanual ?></td>
                                      <td> <?php echo $sum->description ?></td>
                                      <td align="right"><?php echo number_format($sum->charge, 2, ',', '.') ?></td>
                                      <td align="right"><?php echo number_format($sum->payment, 2, ',', '.') ?></td>
                                      <td style="display: none;"> <?php echo $com->trxid ?></td>
                                    </tr>
                                    <?php $nosum++; $totalchargesum+=$sum->charge; $totalpaymentsum+=$com->payment;} ?>
                                </tbody>
                                <tfoot>
                                  <tr style="font-weight: bold; background-color: #f0f0f0;">
                                    <td colspan="4">TOTAL</td>
                                    <td  align="right"><?php echo number_format($totalchargesum, 2, ',', '.') ?></td>
                                    <td  align="right"><?php echo number_format($totalpaymentsum, 2, ',', '.') ?></td>
                                  </tr>
                                  <tr style="font-weight: bold; background-color: #f0f0f0;">
                                    <td colspan="5">BALANCE</td>
                                    <td  align="right"><?php echo number_format($totalchargesum-$totalpaymentsum, 2, ',', '.') ?></td>
                                  </tr>
                                </tfoot>
                            </table>
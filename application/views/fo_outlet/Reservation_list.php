<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>

<section class="content-header">
    <div class="btn-group btn-breadcrumb">
            <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
            <a  class="btn btn-default  btn-xs active">Reservation List</a>
        </div>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-xs-12">
      <div class="box box-info">
        <div class="box-header">
          <h3 class="box-title">Reservation List</h3>
         
        </div>
    <!-- /.box-header -->
        <div class="box-body">
        <div class="table-responsive">
          <table id="example1" class="table table-bordered table-striped">
          <!--   SELECT tbreservation.`idreservation`,STATUS, tbroom.`roomname`, tbroomtype.`roomtype`, CONCAT(`firstname`, ' '  ,lastname) AS nama ,
DATE_FORMAT(checkin,'%d-%m-%y') AS checkin, DATE_FORMAT(checkout,'%d-%m-%y') AS checkout, duration,tbcompany.`name`, notes -->
                                <thead>
                                    <tr >
                                        <th >Reservation ID</th>
                                        <th >Status</th>
                                        <th >Room</th>
                                        <th>Room Type</th>
                                        <th>Guest Name</th>
                                        <th>Arrival</th>
                                        <th>Departure</th>
                                        <th>Duration</th>
                                         <th>Company</th>
                                          <th>Notes</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach($data as $u){
                                    ?>
                                    <tr  class='odd gradeX context'>
                                        <td><?php echo $u->idreservation ?></td>
                                        <td><?php echo $u->STATUS?></td>
                                        <td><?php echo $u->roomname?></td>
                                        <td><?php echo $u->roomtype?></td>
                                        <td><?php echo $u->nama?></td>
                                        <td><?php echo $u->checkin?></td>
                                        <td><?php echo $u->checkout?></td>
                                        <td><?php echo $u->duration?></td>
                                        <td><?php echo $u->name?></td>
                                         <td><?php echo $u->notes?></td>
                                       






                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
          </div>



        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->

</section><!-- /.content -->


<?php
$this->load->view('template/Js');
$this->load->view('template/Foot');
?>
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>

<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>

<style type="text/css">
  .modal.modal-wide .modal-dialog {
  width: 80%;
}

.modal-wide .modal-body {
  overflow-y: auto;
}
</style>
<section class="content-header">
    <div class="btn-group btn-breadcrumb">
            <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
            <a  class="btn btn-default  btn-xs active">PO Received</a>
        </div>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-xs-12">
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">PO Received</h3> 
          
        </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div class="row" style="background-color:#F2F2FF; margin-left:5px; margin-right:5px; margin-bottom:10px;">
        
        <div class="col-sm-3" id="divTglAwalHutang" style=" margin-top: 10px;">
                 <div class="form-group">
                    <label>Dari Tanggal</label>
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text"  data-validation="date" data-validation-format="mm/dd/yyyy" data-validation-error-msg="Tanggal Lahir Belum Dipilih"  data-validation-require-leading-zero="false" class="form-control pull-right" name= "TglLahir" id="TglAwalHutang">
                    </div>
                  </div>
                </div>
              <div class="col-sm-3" id="divTglAkhirHutang" style=" margin-top: 10px;">
                  <div class="form-group">
                    <label>Sampai Tanggal</label>
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text"  data-validation="date" data-validation-format="mm/dd/yyyy" data-validation-error-msg="Tanggal Lahir Belum Dipilih"  data-validation-require-leading-zero="false" class="form-control pull-right" name= "TglLahir" id="TglAkhirHutang">
                    </div>
                  </div>
                </div>
                 <div class="col-sm-3" style="margin-top:33px;">
                <a id="Proses" class="btn btn-info fa fa-eye"> Proses</a>
                </div>
    </div>


         <div class="row" style="background-color:#F2F2FF; margin-left:5px; margin-right:5px; margin-bottom:10px;">
                
                 
          </div>

  






            <div id="tabelada" hidden="hidden">
           </div>



   
               


      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->



<?php
$this->load->view('template/Js');
$this->load->view('template/Foot');
?>

<script type="text/javascript">
  $(document).ready(function(){
     $("#Proses").click(function(e){ 
      var TglAwal = $('#TglAwalHutang').val();
      var TglAkhir = $('#TglAkhirHutang').val();
        $.LoadingOverlay("show");
           $.ajax({
        type: "POST",
        dataType: 'html',
        url: '<?php echo site_url('Purchase_order/purchase_order_list_aktifkan_data');?>',
        data: {TglAwal: TglAwal, TglAkhir:TglAkhir },
        success: function(data){
            e.preventDefault();
            // $('#tabelkosong').fadeOut(30);
             $('#tabelada').fadeIn(0,300);  
            $('#tabelada').html(data);
             $("#example1").DataTable();
            $.LoadingOverlay("hide");
        }
      });
    });
  });
</script>

<script type="text/javascript">
  $(function () {  
  $("#Cetak").click(function(){
      var format = $('#IsFormat').val();
      var IdVendor = $('#IdVendor').val();
      var TglAwalHutang = $('#TglAwalHutang').val();
      var TglAkhirHutang = $('#TglAkhirHutang').val();
      var TglPerHutang = $('#TglPerHutang').val();
      
      window.open("<?php echo base_url(). 'index.php/Payment/Cetak_Hutang_byvendor/';?>?IdVendor="+ IdVendor +"&TglAwalHutang="+ TglAwalHutang+"&TglAkhirHutang="+ TglAkhirHutang+"&TglPerHutang="+ TglPerHutang+"&format="+ format,"MyTargetWindowName")
  }); 
});
</script>



 <script>
  $(function () {
    $(".select2").select2();
     $(".select3").select2();

  });
  </script>

  <script type="text/javascript">
  $('#RentangTanggal').on('ifChanged', function(event){ 
    $('#IsFormat').val('1');
    $('#PerTanggal').prop("checked", false)
    $('#divTglPerHutang').fadeOut(0,200)
     $('#divTglAwalHutang').fadeIn(0,200)
     $('#divTglAkhirHutang').fadeIn(0,200)

});
  $('#PerTanggal').on('ifChanged', function(event){
    $('#IsFormat').val('2');
    $('#RentangTanggal').prop("checked", false)
     $('#divTglAwalHutang').fadeOut(0,200)
     $('#divTglAkhirHutang').fadeOut(0,200)
    $('#divTglPerHutang').fadeIn(0,200)
});
</script>

 <script>
  $( function() {
    $( "#TglAwalHutang" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });
    $( "#TglAkhirHutang" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });
    $( "#TglPerHutang" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });
    

  });

</script>




<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });
  });
</script>


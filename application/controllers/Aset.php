<?php

class Aset extends CI_Controller{
	private $IdOperator;
	private $Operator;
  private $IdAset;

	function __construct(){
		parent::__construct();
		if($this->session->userdata('Kode_userPms')==NULL) {
           redirect('Login');
		}
		$this->load->library(array('session','user_check','akuntan_function','create_pdf','ciqrcode'));
		$this->load->helper(array('form', 'url'));
		$this->load->model(array('M_category','M_category','M_aset','M_subcategory','M_bad_stok'));
		$this->IdOperator = $this->session->userdata('Kode_userPms');
		$this->Operator = $this->session->userdata('Nama_userPms');
    $this->IdAset = $this->session->userdata('KodeKategoriAset');

    
    if (!empty($this->session->userdata("active_db"))) {
      $this->load->database($this->session->userdata("active_db"), FALSE, TRUE);
    }
	}

	function index(){
		redirect('Aset/Aset_view');
	}

	function Aset_view(){
		$data['dataDepaetemen'] = $this->M_aset->loaddepartementall();
		$this->load->view('aset/Aset',$data);
	}
		function Aset_view_data(){
		$Departement = $this->input->post('id');
		$NamaDep = $this->input->post('NamaDep');
    $IdSub = $this->input->post('idsub');
    $Format = $this->input->post('format'); 
	  if ($Departement==1) { //```````````` 
        $set = 'stokBO';
        $Istok = 'isBO';
        $IsProses = 'isprosesopnameBO';
        $baik = 'baikBO';
        $kurangbaik = 'kurangbaikBO';
        $out = 'outBO';
        $used = 'usedBO';
        $lose = 'loseBO';
       }elseif ($Departement==2) {
        $set = 'stokHK';
        $Istok = 'isHK';
        $IsProses = 'isprosesopnameHK';
        $baik = 'baikHK';
        $kurangbaik = 'kurangbaikHK';
        $out = 'outHK';
        $used = 'usedHK';
        $lose = 'loseHK';
       }elseif ($Departement==3) {
         $set = 'stokFO';
         $Istok = 'isFO';
         $IsProses = 'isprosesopnameFO';
         $baik = 'baikFO';
        $kurangbaik = 'kurangbaikFO';
        $out = 'outFO';
        $used = 'usedFO';
        $lose = 'loseFO';
       }elseif ($Departement==4) {
          $set = 'stokFB';
          $Istok = 'isFB';
          $IsProses = 'isprosesopnameFB';
           $baik = 'baikFB';
        $kurangbaik = 'kurangbaikFB';
        $out = 'outFB';
        $used = 'usedFB';
        $lose = 'loseFB';
       }elseif ($Departement==5) {
        $set = 'stokBO';
        $Istok = 'isBO';
        $IsProses = 'isprosesopnameBO';
        $baik = 'baikBO';
        $kurangbaik = 'kurangbaikBO';
        $out = 'outBO';
        $used = 'usedBO';
        $lose = 'loseBO';
       }elseif ($Departement==6) {
        $set = 'stokBar';
        $Istok = 'isBar';
        $IsProses = 'isprosesopnameBar';
        $baik = 'baikBar';
        $kurangbaik = 'kurangbaikBar';
        $out = 'outBar';
        $used = 'usedBar';
        $lose = 'loseBar';
      }elseif ($Departement==7) {
        $set = 'stokEng';
        $Istok = 'isEng';
        $IsProses = 'isprosesopnameEng';
         $baik = 'baikEng';
        $kurangbaik = 'kurangbaikEng';
        $out = 'outEng';
        $used = 'usedEng';
        $lose = 'loseEng';
      }elseif ($Departement==8) {
        $set = 'stokPG';
        $Istok = 'isPG';
        $IsProses = 'isprosesopnamePG';
         $baik = 'baikPG';
        $kurangbaik = 'kurangbaikPG';
        $out = 'outPG';
        $used = 'usedPG';
        $lose = 'losePG';
      }elseif ($Departement==9) {
        $set = 'stokSec';
        $Istok = 'isSec';
        $IsProses = 'isprosesopnameSec';
        $baik = 'baikSec';
        $kurangbaik = 'kurangbaikSec';
        $out = 'outSec';
        $used = 'usedSec';
        $lose = 'loseSec';
       }else{
        $Istok = '';
        $set = 'stok';
        $IsProses = 'isprosesopname';
        $baik = 'baik';
        $kurangbaik = 'kurangbaik';
        $out = 'out';
        $used = 'used';
        $lose = 'lose';
       }
    $data['departement'] =  array('set' =>$set ,'istok' =>$Istok,'iddep' =>$Departement,'dep' =>$NamaDep,'idsub' =>$IdSub );
		$data['data'] = $this->M_aset->Aset_view($Istok, $set,$baik, $kurangbaik,$out,$used,$lose,$IdSub, $this->IdAset,$Departement );
    //print_r($Format);exit();
    if ($Format==1) {
      $this->load->view('aset/Data_aset',$data);
    }else{
       $this->load->view('aset/Data_aset_2',$data);
    }
		
	}

    function status_aset_add(){
		$data['dataDepaetemen'] =  $this->M_aset->loaddepartementall();
		$this->load->view('aset/Change_status_aset',$data);
	}
	 function Aset_view_detil(){
	 	$idepartement = $this->input->post('dep');
	 	$codeaset = $this->input->post('codeaset');
		$data['data'] =  $this->M_aset->Aset_view_detil($codeaset,$idepartement);
		$this->load->view('aset/Data_aset_detil',$data);
	}

	function daftar_item(){
		$Departement = $this->input->post('iddepartement');
	  if ($Departement==1) { //```````````` 
        $set = 'stokBO';
        $Istok = 'isBO';
        $IsProses = 'isprosesopnameBO';
       }elseif ($Departement==2) {
        $set = 'stokHK';
        $Istok = 'isHK';
         $IsProses = 'isprosesopnameHK';
       }elseif ($Departement==3) {
         $set = 'stokFO';
         $Istok = 'isFO';
          $IsProses = 'isprosesopnameFO';
       }elseif ($Departement==4) {
          $set = 'stokFB';
          $Istok = 'isFB';
          $IsProses = 'isprosesopnameFB';
       }elseif ($Departement==5) {
        $set = 'stokBO';
        $Istok = 'isBO';
         $IsProses = 'isprosesopnameBO';
       }elseif ($Departement==6) {
        $set = 'stokBar';
        $Istok = 'isBar';
        $IsProses = 'isprosesopnameBar';
      }elseif ($Departement==7) {
        $set = 'stokEng';
        $Istok = 'isEng';
        $IsProses = 'isprosesopnameEng';
      }elseif ($Departement==8) {
        $set = 'stokPG';
        $Istok = 'isPG';
        $IsProses = 'isprosesopnamePG';
      }elseif ($Departement==9) {
        $set = 'stokSec';
        $Istok = 'isSec';
        $IsProses = 'isprosesopnameSec';
       }else{
       	 $Istok = '';
         $set = 'stok';
         $IsProses = 'isprosesopname';
       }
		$data['data'] = $this->M_aset->item_view($Istok,$this->session->userdata('KodeKategoriAset'));
    // print_r($data['data']);exit();
		$this->load->view('aset/Daftar_item',$data);
	}


		function status_aset_addDB(){
		date_default_timezone_set('Asia/Hong_Kong');
		$itemcode = $this->input->post('itemcode');
		$asetcode = $this->input->post('codeaset');
		$kondisi = $this->input->post('Kondisi');
		$kondisiakhir = $this->input->post('Kondisiakhir');
		//print_r($itemcode);exit();
		$Qty = $this->input->post('Qty');
		$Description = $this->input->post('Description');
		$Note = $this->input->post('Note');
		$Unit = $this->input->post('Unit');
		$IdDepartement = $this->input->post('IdDepartement'); 
		$NamaDepartemen = $this->input->post('NamaDepartemen');
		$Price = $this->input->post('Price');
		$Stok = $this->input->post('Stok');
		$kode =$this->M_bad_stok->get_kodebs('tbbadstok');
		$NotePr = $this->input->post('NotePr');

		$itemcode1 = array_combine(range(1, count($itemcode)), array_values($itemcode));
		$asetcode1 = array_combine(range(1, count($asetcode)), array_values($asetcode));
		$kondisi1 = array_combine(range(1, count($kondisi)), array_values($kondisi));
		$kondisiakhir1 = array_combine(range(1, count($kondisiakhir)), array_values($kondisiakhir));
		$Price1 = array_combine(range(1, count($Price)), array_values($Price));
		$Qty1 = array_combine(range(1, count($Qty)), array_values($Qty));
		$Description1 = array_combine(range(1, count($Description)), array_values($Description));
		$Unit1 = array_combine(range(1, count($Unit)), array_values($Unit));
		$Stok1 = array_combine(range(1, count($Stok)), array_values($Stok));
		$Note1 = array_combine(range(1, count($Note)), array_values($Note));

			$data = array('kodebs' => $kode,
	 				 		'iddepartement' => $IdDepartement,
	 				 		'departement' => $NamaDepartemen,
	 						'dateadd' => date('Y-m-d H:i:s'),
	 						'idoperator' => $this->IdOperator,
	 						'description' => $NotePr	
	 		 			);

				$Jumlah = count($Stok);
				for ($i=1; $i <= $Jumlah ; $i++) { 
					if ($kondisiakhir1[$i]=='Out') {
				 	 $dataDetil[] = array('kodebs' => $kode,
						 				 'iddepartement' => $IdDepartement,
						 				 'departement' => $NamaDepartemen,
						 				'codeitem' => $itemcode1[$i],
						 				'price' => $Price1[$i],
						 				'qty' => $Qty1[$i],
						 				'description' => $Description1[$i],
						 				'unit' => $Unit1[$i],
						 				'note' => $Note[$i],
						 				'currstok' => $Stok1[$i],
						 				'idoperator' => $this->IdOperator
						 		 );
				 	}
				 } 

				 for ($i=1; $i <= $Jumlah ; $i++) { 
				 	 $dataDetilaset[] = array('codeaset' => $asetcode1[$i],
						 				 'iddepartement' => $IdDepartement,
						 				 'departement' => $NamaDepartemen,
						 				 'kondisiawal' => $kondisi1[$i],
						 				 'kondisiakhir' => $kondisiakhir1[$i],
						 				 'qty' => $Qty1[$i],
						 				 'note' => $Note[$i],
						 				 'inputdate' => date('Y-m-d H:i:s'),
						 				 'idoperator' => $this->IdOperator,
						 				 'operator' => $this->Operator
						 		 );
				 } 
//print_r($dataDetilaset);exit();
				if ( !empty( $dataDetil)) {
					$this->M_bad_stok->add_bad_stok($IdDepartement,$data,$dataDetil);	
				}
		 $this->M_aset->Add_detilAset('tbasetdetil',$dataDetilaset,$IdDepartement);
		 $this->session->set_flashdata('msg', 'Store Requisition Add successfully... ');
		 redirect('Aset/status_aset_add');
	}

  function Select_subcategory_bydepartememnt(){
    $Departement = $this->input->post('iddepartement');
   // print_r( $Departement);exit();
    if ($Departement==1) { //```````````` 
        $set = 'stokBO';
        $Istok = 'isBO';
        $IsProses = 'isprosesopnameBO';
       }elseif ($Departement==2) {
        $set = 'stokHK';
        $Istok = 'isHK';
         $IsProses = 'isprosesopnameHK';
       }elseif ($Departement==3) {
         $set = 'stokFO';
         $Istok = 'isFO';
          $IsProses = 'isprosesopnameFO';
       }elseif ($Departement==4) {
          $set = 'stokFB';
          $Istok = 'isFB';
          $IsProses = 'isprosesopnameFB';
       }elseif ($Departement==5) {
        $set = 'stokBO';
        $Istok = 'isBO';
         $IsProses = 'isprosesopnameBO';
       }elseif ($Departement==6) {
        $set = 'stokBar';
        $Istok = 'isBar';
        $IsProses = 'isprosesopnameBar';
      }elseif ($Departement==7) {
        $set = 'stokEng';
        $Istok = 'isEng';
        $IsProses = 'isprosesopnameEng';
      }elseif ($Departement==8) {
        $set = 'stokPG';
        $Istok = 'isPG';
        $IsProses = 'isprosesopnamePG';
      }elseif ($Departement==9) {
        $set = 'stokSec';
        $Istok = 'isSec';
        $IsProses = 'isprosesopnameSec';
       }else{
         $Istok = '';
         $set = 'stok';
         $IsProses = 'isprosesopname';
       }
    $sub = $this->M_aset->Select_subcategory_bydepartememnt($Departement);
    echo ' <div class="form-group" id="groupSubcategory">';
    echo ' <label>Subcategory</label> ';
    echo  '<select id="Subcategory" class="form-control select2"  style="width: 100%;" name="Subcategory"> ';
    echo ' <option value="" >Semua Subcategory</option>';
    foreach ($sub as $pek ) {
                    echo ' <option value="'.$pek->idsubcategory.'" >'.$pek->subcategory.'</option>';
    }
    echo '</select>';
    echo '</div>';

  }


	function Cetak($JenisLaporan,$Text){
		$data['hotel'] = $this->M_rbac->getPerusahaan();
		$data['qr'] = $this->ciqrcode->generateQRcode($JenisLaporan, $Text,1.9);
		$data1= $this->load->view('template/Cetak_head',$data, TRUE);
		return $data1;
	}


	function CetakAset_bydepartement(){
		$NamaDep = $this->input->get('namadep', TRUE);
		$Departement = $this->input->get('id', TRUE);
    $NamaSub = $this->input->get('namasub', TRUE);
     $IdSub = $this->input->get('idsub', TRUE); 
    $Format =$this->input->get('format', TRUE);  

	  if ($Departement==1) { //```````````` 
        $set = 'stokBO';
        $Istok = 'isBO';
        $IsProses = 'isprosesopnameBO';
        $baik = 'baikBO';
        $kurangbaik = 'kurangbaikBO';
        $out = 'outBO';
        $used = 'usedBO';
        $lose = 'loseBO';
       }elseif ($Departement==2) {
        $set = 'stokHK';
        $Istok = 'isHK';
        $IsProses = 'isprosesopnameHK';
        $baik = 'baikHK';
        $kurangbaik = 'kurangbaikHK';
        $out = 'outHK';
        $used = 'usedHK';
        $lose = 'loseHK';
       }elseif ($Departement==3) {
         $set = 'stokFO';
         $Istok = 'isFO';
         $IsProses = 'isprosesopnameFO';
         $baik = 'baikFO';
        $kurangbaik = 'kurangbaikFO';
        $out = 'outFO';
        $used = 'usedFO';
        $lose = 'loseFO';
       }elseif ($Departement==4) {
          $set = 'stokFB';
          $Istok = 'isFB';
          $IsProses = 'isprosesopnameFB';
           $baik = 'baikFB';
        $kurangbaik = 'kurangbaikFB';
        $out = 'outFB';
        $used = 'usedFB';
        $lose = 'loseFB';
       }elseif ($Departement==5) {
        $set = 'stokBO';
        $Istok = 'isBO';
        $IsProses = 'isprosesopnameBO';
        $baik = 'baikBO';
        $kurangbaik = 'kurangbaikBO';
        $out = 'outBO';
        $used = 'usedBO';
        $lose = 'loseBO';
       }elseif ($Departement==6) {
        $set = 'stokBar';
        $Istok = 'isBar';
        $IsProses = 'isprosesopnameBar';
        $baik = 'baikBar';
        $kurangbaik = 'kurangbaikBar';
        $out = 'outBar';
        $used = 'usedBar';
        $lose = 'loseBar';
      }elseif ($Departement==7) {
        $set = 'stokEng';
        $Istok = 'isEng';
        $IsProses = 'isprosesopnameEng';
         $baik = 'baikEng';
        $kurangbaik = 'kurangbaikEng';
        $out = 'outEng';
        $used = 'usedEng';
        $lose = 'loseEng';
      }elseif ($Departement==8) {
        $set = 'stokPG';
        $Istok = 'isPG';
        $IsProses = 'isprosesopnamePG';
         $baik = 'baikPG';
        $kurangbaik = 'kurangbaikPG';
        $out = 'outPG';
        $used = 'usedPG';
        $lose = 'losePG';
      }elseif ($Departement==9) {
        $set = 'stokSec';
        $Istok = 'isSec';
        $IsProses = 'isprosesopnameSec';
        $baik = 'baikSec';
        $kurangbaik = 'kurangbaikSec';
        $out = 'outSec';
        $used = 'usedSec';
        $lose = 'loseSec';
       }else{
       	$Istok = '';
        $set = 'stok';
        $IsProses = 'isprosesopname';
        $baik = 'baik';
        $kurangbaik = 'kurangbaik';
        $out = 'out';
        $used = 'used';
        $lose = 'lose';
       }

      $data['departement'] =  array('set' =>$set ,'istok' =>$Istok,'iddep' =>$Departement,'dep' =>$NamaDep,'namasub' =>$NamaSub,'idsub' =>$IdSub );
	   $data['header'] = $this->Cetak("Inventory-report-".$data['departement']['dep'],  $this->session->userdata('Nama_userPms'). date("d-m-Y_H-i-s"), 1);
	   $data['konten'] = $this->M_aset->Aset_view($Istok, $set,$baik, $kurangbaik,$out,$used,$lose, $IdSub,$this->IdAset );
     
     if ($Format==1) {
           $html=$this->load->view('aset/cetak/aset_bydepartement',$data, TRUE);
     }else{
       $html=$this->load->view('aset/cetak/aset_format_2',$data, TRUE);
     }
	   $this->create_pdf->load($html,"Inventory-report-".$data['departement']['dep'], 'A4-L','');
		
	}
	


}
?>

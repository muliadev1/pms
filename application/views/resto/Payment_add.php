


<!-- Main content -->


   
<section class="content" id="PaymentKonten" hidden="hidden">
  <div class="row">
  <div class="col-md-3">
    <a  value="Validate" id="BackToSplit" class="btn btn-lg btn-warning"><i class='fa fa-caret-square-o-left'></i> Back</a>
  </div>
   <div class="col-md-6" style=" border: 5px solid #00a65a;">
  <div class="row">
   <div class="col-md-4">
       <div class="form-group">
        <label class="control-label">Total</label>
        <input type="text" name="Total" id="Total" readonly="readonly"   class="form-control"  >
         <input type="hidden" name="Total1" id="Total1" >
       </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
        <label class="control-label">Discount</label>
        <input type="text" name="Discount" id="Discount" readonly="readonly"   class="form-control"   >
        <input type="hidden" name="Discount1" id="Discount1" >
       </div>
       </div>
       <div class="col-md-4">
        <div class="form-group has-success">
        <label class="control-label">After Discount</label>
        <input type="text" name="AfterDiscount" id="AfterDiscount" readonly="readonly"   class="form-control" >
        <input type="hidden" name="AfterDiscount1" id="AfterDiscount1"  >
       </div>
      </div>
    </div>

 <div class="row" >
   <div class="col-md-4">
    <div class="form-group">
        <label class="control-label">Service</label> 
        <input type="text" name="Service" id="Service"   class="form-control input-ser" onkeyup="doMathGrand()" autocomplete="off" >
         <input type="hidden" name="Service1" id="Service1"  onkeyup="doMathGrand()">
       </div>
       </div>
       <div class="col-md-4">
        <div class="form-group has-success">
        <label class="control-label">Tax</label>
        <input type="text" name="Tax" id="Tax"  class="form-control input-tax"  onkeyup="doMathGrand()"  autocomplete="off" >
        <input type="hidden" name="Tax1" id="Tax1"   onkeyup="doMathGrand()">
       </div>
      </div>
      <div class="col-md-4">
        <div class="form-group has-success">
        <label class="control-label">Grand Total</label>
        <input type="text" name="GrandTotal" id="GrandTotal" readonly="readonly"   class="form-control"  onkeyup="doMathGrand()" >
        <input type="hidden" name="GrandTotal1" id="GrandTotal1"  onkeyup="doMathGrand()" >
       </div>
      </div>
    </div>


       <div class="form-group"  >
        <div class="row" >
         <div class="col-md-4">
          <label class="control-label">Payment Status</label>
         </div>
        </div>
        <div class="row"  >
         <div class="col-md-4">
          <input type="radio" name="PayMethod" id="rbWalk" class="flat-red" value="1" id="walk" checked> WALK IN
         </div>
         <div class="col-md-4">
          <input type="radio" name="PayMethod" id="rbRoom" class="flat-red" value="2" id="room"> ROOM CHARGE
         </div>
         <div class="col-md-4">
          <input type="radio" name="PayMethod" id="rbComp" class="flat-red" value="3" id="compliment"> COMPLIMENT BILL
         </div>
        </div>
       </div>


      <div class="form-group" hidden="hidden" id="ElementRoom">
        <label class="control-label">Room</label>
         <input type="text" name="Room" id="Room"  readonly="readonly"  class="form-control"  value="0" >
       </div>

<div  class="row" id="rowpay" style="background-color: #baffd1;">
  <div  class="col-md-12">
  <div  class="row" id="rowpay" >
  <div  class="col-md-4">
      <div class="form-group">
        <label>Payment Method</label>
        <select id="PayMethod" class="form-control select2"  style="width: 100%;" >  
        <?php foreach ($payment as $pay ){ ?>
              <option value="<?php echo $pay->id ?>"><?php echo $pay->articlepayment; ?></option>
            <?php } ?>    
             
        </select>
       </div>
     </div>
    <div  class="col-md-4">
     <div class="form-group">
        <label class="control-label">Payment Amount</label>
        <input type="text" name="PayAmount" id="PayAmount"    class="form-control input-21111"  autocomplete="off"  >
        <input type="hidden" name="PayAmount1" id="PayAmount1"  >
       </div>
      </div>

           <div  class="col-md-3">
                    <div class="form-group" >
                        <a style="margin-top:25px; margin-right:10px; " class="btn btn-primary btn-flat Edit" id="Addpayment"  ><i class='glyphicon glyphicon-arrow-down'></i>  Proses  </a>   
                     </div>
                  </div>
                </div>


    <div class="form-group">
            <table id="paymenttabel" class="table"  >
                                <thead>
                                    <tr >
                                       <th >ID</th>
                                        <th >Payment Method</th>
                                        <th >Amount</th>
                                        <th style="display: none;">idMethod</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="mytbody">
                                   
                                </tbody>
                                </table>
                      </div>
      </div>
       </div>

                      

        
        <div class="form-group has-success">
        <label class="control-label">Remain</label>
        <input type="text" name="Remain" id="Remain"  readonly="readonly"  class="form-control"   onkeyup="doMathPay()">
        <input type="hidden" name="Remain1" id="Remain1"  readonly="readonly"  onkeyup="doMathPay()">
       </div>
       <div class="form-group ">
        <label class="control-label">Note</label>
        <textarea type="text" name="Note" id="Note"    class="form-control"></textarea>
       </div>

          <div class="form-group">
            <button type="submit" id="simpan1"  class="btn btn-primary"><i class='glyphicon glyphicon-ok'></i> Save</button>
          </div>
       </div>
       <div class="col-md-3 "></div>
       <input type="hidden"  id="IdPay" value="0" readonly="readonly"  ">

</section>


      <script type="text/javascript">
   $('#rbRoom').on('ifChanged', function(event){  
     $("#rowpay").fadeOut(0,1000);
    $("#Remain").val(0); 
    $("#Remain1").val(0); 
    });

    $('#rbWalk').on('ifChanged', function(event){  
     $("#rowpay").fadeIn(0,1000);
    $("#Remain").val(0); 
    $("#Remain1").val(0); 
    });

    $('#rbComp').on('ifChanged', function(event){  
     $("#rowpay").fadeOut(0,1000);
    $("#Remain").val(0); 
    $("#Remain1").val(0); 
    });
</script>


<script type="text/javascript">
   $('#Addpayment').on('click', function(e){ 
    var idpay = parseInt($('#IdPay').val()) +1;
     var tbpay = $('#paymenttabel').DataTable();
    
  var PayMethod = $('#PayMethod').val(); 
  var PayAmount = $('#PayAmount').val();
  var PayMethod1 = $("#PayMethod option:selected").text();
  
   var btnedit =  "<a class='btn btn-danger btn-xs delPay'>  <span class='fa fa-fw  fa-minus-square' ></span> </a>";
   var row = tbpay.row.add( [
            idpay,
            PayMethod1,
            PayAmount,
            PayMethod,
            btnedit
        ]).draw(false);
        tbpay.row(row).column(0).nodes().to$().addClass('PayMethod bold');
        tbpay.row(row).column(1).nodes().to$().addClass('PayAmount bold');
        tbpay.row(row).column(2).nodes().to$().addClass('PayMethod1 bold');
        tbpay.columns([3]).visible(false);
        HitungPay();
        GenetateElementPay(idpay,PayAmount,PayMethod)
        $('#IdPay').val(idpay);
  });
</script>
<script type="text/javascript">
  function GenetateElementPay(id,PayAmount,PayMethod) {
 $('<input>').attr({
        type: 'text',
        id: id+'idPay',
        class: id+'Pay',
        name: 'idPay['+id+']', 
        value: id
        }).appendTo('p');
 $('<input>').attr({
        type: 'text',
        id: id+'PayAmountPay',
        class: id+'Pay',
        name: 'PayAmountPay['+id+']', 
        value: PayAmount
        }).appendTo('p');
 $('<input>').attr({
        type: 'text',
        id: id+'PayMethodPay',
        class: id+'Pay',
        name: 'PayMethodPay['+id+']', 
        value: PayMethod.replace(/\./g, "")   
        }).appendTo('p');

  }
</script>


<script type="text/javascript">
  function HitungPay() {
    var tbpay = $('#paymenttabel').DataTable();
    var GrandTotal1=$('#GrandTotal1').val();
   
    var infotbpay =tbpay.page.info();

    var TotalPay = 0 ;
        if (infotbpay.end>0) {
         for (var i = infotbpay.start; i < infotbpay.end; i++) {
           TotalPay+= parseFloat(tbpay.row(i).data()[2].replace(/\./g, ""));
         }
        };
    TotalPay = TotalPay-GrandTotal1;
    $('#Remain').val('');
    $('#Remain').val(TotalPay.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."),);
    
  }
</script>

<script type="text/javascript">
$(function () {
   $('#paymenttabel tbody').on('click', '.delPay', function(e){
     var tbpay = $('#paymenttabel').DataTable();
     var id= tbpay.row($(this).closest('tr')).data()[0]; 
      tbpay.row($(this).closest('tr')).remove().draw(false);
       $('.'+id+'Pay').remove();
      HitungPay();  
  });
});
</script>


<script type="text/javascript">
   $(function () {

     $('#paymenttabel').DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": false,
      "autoWidth": false
    });
    new Cleave('.input-21111', {
      numeral: true,
      numeralDecimalMark: ',',
      delimiter: '.'
    });
    new Cleave('.input-ser', {
      numeral: true,
      numeralDecimalMark: ',',
      delimiter: '.'
    });
    new Cleave('.input-tax', {
      numeral: true,
      numeralDecimalMark: ',',
      delimiter: '.'
    });
  });
</script>
 <script type="text/javascript">
      function doMathGrand(){
        //AfterDiscount  Tax GrandTotal Service
        var AfterDiscount = document.getElementById('AfterDiscount1').value;
        var Tax = document.getElementById('Tax').value;
        document.getElementById('Tax1').value =  Tax.replace(/\./g, "");
        var Tax1 = document.getElementById('Tax1').value;

        var Service = document.getElementById('Service').value;
        document.getElementById('Service1').value =  Service.replace(/\./g, ""); 
        var Service1 = document.getElementById('Service1').value;

        var Grandtotal = parseFloat(AfterDiscount)+parseFloat(Tax1)+parseFloat(Service1);     

        document.getElementById('GrandTotal').value = Grandtotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");;
         document.getElementById('GrandTotal1').value = Grandtotal;
        

      }
    </script>

 <script type="text/javascript">
      function doMathPay(){
          var PayAmount = document.getElementById('PayAmount').value;
          document.getElementById('PayAmount1').value =  PayAmount.replace(/\./g, "");
          var PayAmount1 = document.getElementById('PayAmount1').value;

          var Grandtotal = document.getElementById('GrandTotal1').value;
          var Remain = parseFloat(PayAmount1)-parseFloat(Grandtotal);
          document.getElementById('Remain').value = Remain.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
          document.getElementById('Remain1').value = Remain;
      }
    </script>


<script type="text/javascript">
$("#simpan1").click(function() {
    var payment = $('#PayAmount1').val();
    var remain = $('#Remain1').val();
        if (remain<0 ) {InvalidTransaction();return false};
        event.preventDefault();
        $.confirm({
          title: 'Confirmation',
          content: 'Are You Sure to Save?',
           type: 'blue',
          buttons: {
              Save: function () {
                  $.LoadingOverlay("show");
                  $("#Simpan").submit();
              },
              Cancel: function () {

                  $.alert('Data Not Saved...');
              },
          }
      });

});
</script>





<script>
$(function () {
  $(".select2").select2();
  $(".select3").select2();

});
</script>
<script type="text/javascript">
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });
</script>

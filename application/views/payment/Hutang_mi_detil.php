
        <div class="table-responsive">
          <table id="example111" class="table table-bordered table-striped">
                                <thead>
                                    <tr >
                                        <th>Item Code</th>
                                        <th>Description</th>
                                        <th>Unit</th>
                                         <th>Price</th>
                                         <th>Qty</th>
                                         <th>Subtotal</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i=1; $TotalHutang = 0;
                                    foreach($mi as $u){
                                    ?>
                                    <tr  class='odd gradeX context'>
                                        <td class="code"><?php echo $u->itemcode ?></td>
                                        <td class="desc"><?php echo $u->description?></td>
                                        <td class="desc"><?php echo $u->unit?></td>
                                        <td class="total" align="right"><?php echo number_format($u->price, 0, '.', '.')?></td>
                                        <td class="total" align="right"><?php echo $u->qty ?></td>
                                        <td class="total" align="right"><?php echo number_format($u->subtotal, 0, '.', '.')?></td> 
                                        

                                    </tr>
                                    <?php $i++; $TotalHutang += $u->tosubtotaltal; } ?>
                                </tbody>
                                
                            </table>
          </div>



        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->


<script type="text/javascript">
  $(document).ready(function(){
     $(".ViewDetail").click(function(e){ 
     var nomi = $('#IdDepartement').val();  
      var code = $(this).closest('tr').children('td.code').text(); 
       var total = $(this).closest('tr').children('td.total').text();     
      $.ajax({
        type: "POST",
       url: '<?php echo site_url('Payment/viwedetilmi');?>',
        data: {nomi:nomi  },
        success: function(data){
            e.preventDefault();
            var mymodal1 = $('#myModalNew');
            mymodal1.modal({backdrop: 'static', keyboard: false}) 
            var height = $(window).height() - 200;
            mymodal1.find(".modal-body").css("max-height", height);
             $("#NO span").text(nomi);
              $("#Code span").text(total);
            mymodal1.find('.modal-body').html(data);
            mymodal1.modal('show');            
        }
      });
    });
  });
</script>


<script type="text/javascript">
  function File_Kosong() {
  $.alert({
    title: 'Caution!!',
    content: 'Transaction Is Invalid!',
    icon: 'fa fa-warning',
    type: 'orange',
});
}
</script>

<script>
  $(function () {
    
    $('#example111').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
  });
</script>

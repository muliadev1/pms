<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>
<script src="<?php echo base_url('assets/js/cleave.min.js') ?>" type="text/javascript"></script>
<style type="text/css">
/* .modal-fullscreen */

.modal-fullscreen {
  background: transparent;
}
.modal-fullscreen .modal-content {
  background: solid white;
  border: 0;
  -webkit-box-shadow: none;
  box-shadow: none;
}
.modal-fullscreen .modal-dialog {
  background: solid white;
  border: 0;
  -webkit-box-shadow: none;
  box-shadow: none;
}
.modal-backdrop.modal-backdrop-fullscreen {
  background: #ffffff;
}
.modal-backdrop.modal-backdrop-fullscreen.in {
  opacity: .98;
  filter: alpha(opacity=98);
}

/* .modal-fullscreen size: we use Bootstrap media query breakpoints */
.modal-wide .modal-body {
  overflow-y: auto;
}
.modal.modal-wide .modal-dialog {
  width: 80%;
}

.modal-fullscreen .modal-dialog {
  margin: 0;
  margin-right: auto;
  margin-left: auto;
  width: 100%;
}
@media (min-width: 768px) {
  .modal-fullscreen .modal-dialog {
    width: 100%;
  }
}
@media (min-width: 992px) {
  .modal-fullscreen .modal-dialog {
    width: 100%;
  }
}
@media (min-width: 1200px) {
  .modal-fullscreen .modal-dialog {
     width: 100%;
  }
}
#myModalPayment .modal-body p { margin-bottom: 900px }
.pull-right {
    float: right;
}
.modal.modal-wide .modal-dialog {
  width: 80%;
}
.modal.modal-wide1 .modal-dialog1 {
   width: 300px;
   height: 300px;
   position: absolute;
   left: 50%;
   top: 50%; 
   margin-left: -150px;
   margin-top: -150px;
}
</style>

<section class="content-header">
    <div class="btn-group btn-breadcrumb">
            <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
            <a  class="btn btn-default  btn-xs active">Table List</a>
        </div>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
 <?php foreach ($data as $row ) { ?>
<?php if (!empty($row->trxid)) { ?>
  <div class="col-lg-3 col-xs-6 AddPaymentItem" Idr="<?php echo $row->tablenumber ?>" idtx="<?php echo $row->trxid ?>" style="cursor:pointer;">
        <div class="small-box bg-red">
          <div class="inner">
            <h3 id="table<?php echo $row->tablenumber ?>"> <?php echo $row->tablenumber ?></h3>
                
                <p> <b class="clsGuestName"> <?php echo $row->firstname ?> <?php echo $row->lastname ?></b></p>
          </div>
          <div class="icon">
            <i class="fa fa-minus-circle"></i>
          </div>
          <a href="javascript:void(0)" class="small-box-footer">Action <i class="fa fa-hand-o-up"></i></a>
        </div>
      </div>
    <?php }else{ ?>
      <div class="col-lg-3 col-xs-6" >
        <div class="small-box bg-green AddGuest" Idr="<?php echo $row->tablenumber ?>" style="cursor:pointer;"  >
          <div class="inner">
            <h3 > <?php echo $row->tablenumber ?> </h3>

            <p><em>Empty</em></p>
          </div>
          <div class="icon">
            <i class="fa fa-user-plus"></i>
          </div>
          <a href="javascript:void(0)" class="small-box-footer">Add Guest <i class="fa fa-hand-o-up"></i></a>
        </div>
      </div>

       <?php }?>
<?php }?>


</div>
<!-- Modal fullscreen -->
<div class="modal modal-fullscreen fade" id="modalAddGuest" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">

    </div>
  </div>
</div>

<div class="modal modal-fullscreen fade" id="modalSplitBill" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">

    </div>
  </div>
</div>
<div class="modal modal-fullscreen fade" id="modalMoveTable"role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">

    </div>
  </div>
</div>

</section><!-- /.content -->
<div class="modal fade" id="addMenuQtyReq" tabindex="-2" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 style="font-weight:bold;" id="qtyHeader"></h4>
      </div>
      <div class="modal-header">
        <input type="hidden" id="qMenuId" value="">
        <input type="hidden" id="qMenu" value="">
        <div class="form-group">
          <label>Quantity</label>
          <input type="text" placeholder="Qty" class="qty form-control input-1" value="1"/>
        </div>
        <div class="form-group">
          <label>Request</label>
          <textarea placeholder="Request" class="req form-control" rows="3" autocomplete="off"></textarea>
        </div>
      </div>
      <div class="modal-footer">
        <div class="pull-right">
          <button type="button" class="btn btn-primary" id="okay_qty">Submit</button>
          <button type="button" class="btn btn-default"  class="close" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>
</div>

 <div class="modal modal-wide fade" id="myModalGuest" role="dialog" style="display:none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" title="close" data-dismiss="modal">&times;</button>
                    <h4 align="center" class="modal-title">Guest List</h4>
                </div>
                <div class="modal-body">   
                </div>
            </div>
        </div>     
    </div>

     <div class="modal modal-wide fade" id="myModalWaiter" role="dialog" style="display:none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" title="close" data-dismiss="modal">&times;</button>
                    <h4 align="center" class="modal-title">Waiter List</h4>
                </div>
                <div class="modal-body">   
                </div>
            </div>
        </div>     
    </div>

     <div class="modal modal-wide fade" id="myModalPayment" role="dialog" style="display:none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" title="close" data-dismiss="modal">&times;</button>
                    <h4 align="center" class="modal-title">Payment Process</h4>
                </div>
                <div class="modal-body">   
                </div>
            </div>
        </div>     
    </div>
     <div class="modal modal-wide1 fade1" id="myDisc" role="dialog" style="display:none;">
        <div class="modal-dialog1 modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                <!--  <a type="button" class="btn btn-danger btn-xs" id="CloseDisc" title="close" style="float:right;" >&times;</a> -->
                    <h4 align="center" class="modal-title">Discount </h4>
                    <h6 align="center" class="modal-title" id="MenuModal" style="font-weight: bold;"></h6>
                </div>
                <div class="modal-body">
                <div class="form-group">
                <input type="hidden" name="QtyDisc" id="QtyDisc"  class="form-control " >
                <input type="hidden" name="PriceDisc" id="PriceDisc"  class="form-control " >
                <input type="hidden" name="IdMenuModalDisc" id="IdMenuModalDisc"  class="form-control">
                <input type="hidden" name="SubtotalDiscModal" id="SubtotalDiscModal"  class="form-control">
                <input type="hidden" name="IdMenuDiscModal" id="IdMenuDiscModal"  class="form-control " >
                <input type="hidden" name="DiscInputBayangan" id="DiscInputBayangan"  class="form-control " >
                  <input type="text" name="DiscInput" id="DiscInput"  class="form-control input-disc" style="font-weight: bold;" >
                    </div> 
                    <div class="form-group">
                    <a type="button" id="DiscInputOK"  class="btn btn-primary btn-sm"><i class="fa fa-check fa-lg"></i> Process</a>
                   <a type="button"  id="CloseDisc" class="btn btn-warning btn-sm"><i class="fa fa-times fa-lg"></i>Cancel</a>
                    </div>  
                </div>
            </div>
        </div>     
    </div>



<?php
$this->load->view('template/Js');
$this->load->view('template/Foot');
?>




<script type="text/javascript">
    $(".AddGuest").click(function(e){
      var modalAddGuest = $('#modalAddGuest');
      $('#modalAddGuest').modal({backdrop: 'static', keyboard: false}) ;
          $.ajax({
        type: "POST",
       url: '<?php echo site_url('Resto/Add_guest');?>',
       cache: false,
        data: {status: $(this).attr("Idr")},
        success: function(data){
            e.preventDefault();
            modalAddGuest.find('.modal-content').html(data);
            modalAddGuest.modal('show');
        }
      });
  });
</script>
<script type="text/javascript"> 
  function AddPayment(status,idtrx,guestName) {
      var modalSplitBill = $('#modalSplitBill');
      $('#modalSplitBill').modal({backdrop: 'static', keyboard: false}) ;
        $.ajax({
        type: "POST",
        url: '<?php echo site_url('Resto/split_bil');?>',
        cache: false,
        data: {status: status, idtrx: idtrx, guestName :guestName },
        success: function(data){
          //  e.preventDefault();
            modalSplitBill.find('.modal-content').html(data);
            modalSplitBill.modal('show');
        }
      });
    };
</script>
<script type="text/javascript"> 
  function MoveTable(status,idtrx,guestName) {
      var modalMoveTable = $('#modalMoveTable');
      $('#modalMoveTable').modal({backdrop: 'static', keyboard: false}) ;
        $.ajax({
        type: "POST",
        url: '<?php echo site_url('Resto/move_table');?>',
        cache: false,
        data: {status: status, idtrx: idtrx, guestName :guestName },
        success: function(data){
          //  e.preventDefault();
            modalMoveTable.find('.modal-content').html(data);
            modalMoveTable.modal('show');
        }
      });
    };
</script>
<script type="text/javascript">
  // function AddPayment(status,idtrx,guestName) {
  //    //var idvendor = $('#IdVendor').val();        
  //     $.ajax({
  //       type: "POST",
  //      url: '<?php echo site_url('Resto/payment_process');?>',
       
  //       data: {status: status, idtrx: idtrx, guestName :guestName },
  //       success: function(data){
  //           e.preventDefault();
  //           var mymodal1 = $('#myModalGuest');
  //           mymodal1.modal({backdrop: 'static', keyboard: false}) 
  //           //var height = $(window).height() - 200;
  //          // mymodal1.find(".modal-body").css("max-height", height);
  //           mymodal1.find('.modal-body').html(data);
  //           mymodal1.modal('show');            
  //       }
  //     });
  //  };
</script>

<script type="text/javascript">
  function OrderMore(status,idtrx,guestName) {
      var modalAddGuest = $('#modalAddGuest');
      $('#modalAddGuest').modal({backdrop: 'static', keyboard: false}) ;
        $.ajax({
        type: "POST",
        url: '<?php echo site_url('Resto/Add_guest');?>',
        cache: false,
        data: {status: status, idtrx: idtrx, guestName :guestName },
        success: function(data){
          //  e.preventDefault();
            modalAddGuest.find('.modal-content').html(data);
            modalAddGuest.modal('show');
        }
      });
    };
</script>

<script type="text/javascript">
   $(".AddPaymentItem").click(function(e){
    var table =  $(this).attr("Idr");
    var id =$(this).attr("Idtx");
    var guestName = $(this).find('.clsGuestName').html();
     event.preventDefault();
        $.confirm({
          title: 'Table'+' '+table,
          content: 'select options',
           closeIcon: true,
          buttons: {
            Order: {
            text: 'Order',
            btnClass: 'btn-blue',
            action: function(){
                OrderMore(table,id,guestName);
            }
          },
          Payment: {
            text: 'Payment',
            btnClass: 'btn-green',
            action: function(){
                AddPayment(table,id,guestName);
            }
          },
           MoveTable: {
            text: 'Move Table',
            btnClass: 'btn-warning',
            action: function(){
                MoveTable(table,id,guestName);
            }
          },
          }
      });
   });
  </script>












<script type="text/javascript">
  $(".modal-transparent").on('show.bs.modal', function () {
  setTimeout( function() {
    $(".modal-backdrop").addClass("modal-backdrop-transparent");
  }, 0);
});
$(".modal-transparent").on('hidden.bs.modal', function () {
  $(".modal-backdrop").addClass("modal-backdrop-transparent");
});

$(".modal-fullscreen").on('show.bs.modal', function () {
  setTimeout( function() {
    $(".modal-backdrop").addClass("modal-backdrop-fullscreen");
  }, 0);
});
$(".modal-fullscreen").on('hidden.bs.modal', function () {
  $(".modal-backdrop").addClass("modal-backdrop-fullscreen");
});
</script>

<script>

  $(function () {
    new Cleave('.input-1', {
      numeral: true,
      numeralDecimalMark: ',',
      delimiter: '.'
    });
    new Cleave('.input-disc', {
      numeral: true,
      numeralDecimalMark: ',',
      delimiter: '.'
    });

     $("#menuListKambing").DataTable();
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
     $('#sidebar').addClass('sidebar-collapse');
     
  });

  function newMenu(id_menu, menu, qty, request) {
    var sama = false;
    var index_sama = "";
    var length = pV.tb.rows().data().length;
    if (request == "") {
      request = "-";
    }

    for (var i = 0; i < length; i++) {
      if (pV.tb.cell(i, 0).data() == id_menu && pV.tb.cell(i, 3).data() == request) {
        sama = true;
        index_sama = i;
      }
    }

    if (sama == true) {
      menu_req_sama(index_sama, qty);
    }else if(sama == false){
      addNewRow(id_menu, menu, qty, request);
    }
  }

  $(document).on("click", "#menu_okay", function() {
    var datas = pV.tb.rows().data().toArray();
    if (datas == "") {
      customAlert("Please fill menu table ...");
    }else {
      confrimAlert();
    }
  });

  $(document).on("click", "#okay_qty", function() {
    var qty = $('#addMenuQtyReq').find(".qty").val();
    if (!qty) {
      customAlert('Please input Quantity field ...');
      return false;
    }
    var id_menu = $('#qMenuId').val();
    var menu = $('#qMenu').val();
    var req = $('#addMenuQtyReq').find('.req').val();
    newMenu(id_menu, menu, qty, req);
    $('#addMenuQtyReq').modal("hide");
  });

  $("#modalAddGuest").on("hidden.bs.modal", function () {
    location.reload();
  });
    $("#modalSplitBill").on("hidden.bs.modal", function () {
    location.reload();
  });
     $("#modalMoveTable").on("hidden.bs.modal", function () {
    location.reload();
  });
</script>


<script type="text/javascript">
$(function () {
   $('#SplitTable tbody').on('click', '.PilihPay', function(e){
    alert('tes');
     var t = $('#SplitTable').DataTable();



   id = t.row($(this).closest('tr')).data()[0]; //$(this).closest('tr').children('td.No').text();
   menu = t.row($(this).closest('tr')).data()[1]; //$(this).closest('tr').children('td.ItemCode').text();
   price = t.row($(this).closest('tr')).data()[2];//$(this).closest('tr').children('td.Description').text();
   idsm = t.row($(this).closest('tr')).data()[3];//$(this).closest('tr').children('td.Qty').text();
   price1 =  t.row($(this).closest('tr')).data()[4];//$(this).closest('tr').children('td.Unit').text();
   qty = t.row($(this).closest('tr')).data()[5]; //$(this).closest('tr').children('td.Price').text();
   subtotal = t.row($(this).closest('tr')).data()[6];//$(this).closest('tr').children('td.Subtotal').text();
   namauser = t.row($(this).closest('tr')).data()[9];//$(this).closest('tr').children('td.StokEditTd').text();
   Note = t.row($(this).closest('tr')).data()[7];//$(this).closest('tr').children('td.Note').text();
   var btnedit =  "<a class='btn btn-warning btn-xs PilihEdit' title='Edit Item'>  <span class=' fa fa-edit' ></span></a> <a class='btn btn-danger btn-xs Hapus' title='Remove Item'>  <span class=' fa fa-close' ></span> </a>" 

   var row = t.row.add( [
            id,
            menu,
            price,
            idsm,
            price1,
            qty,
            subtotal,
            namauser,
            btnedit
        ] ).draw(false);
        t.row(row).column(0).nodes().to$().addClass('No');
        t.row(row).column(1).nodes().to$().addClass('ItemCode');
        t.row(row).column(2).nodes().to$().addClass('Description');
        t.row(row).column(3).nodes().to$().addClass('Qty');
        t.row(row).column(4).nodes().to$().addClass('Unit');
        t.row(row).column(5).nodes().to$().addClass('Price');
        t.row(row).column(6).nodes().to$().addClass('Subtotal');
        t.row(row).column(7).nodes().to$().addClass('Note');
        t.row(row).column(8).nodes().to$().addClass('Edit');
       
  });
});
</script>

<?php if($this->session->flashdata('cetakbill2')): ?>
<?php ?> 
<script type="text/javascript">
  $(document).ready(function() {
    
       //  window.open("<?php echo base_url(). 'index.php/Resto/Cetak_Bill2/';?>?Bill="+'2' ,"MyTargetWindowName")
       
    window.open("<?php echo base_url(). 'index.php/Resto/Cetak_Bill2/';?>", '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes')



  });
  </script>

  <?php endif; ?>



<?php $lasttrx =  $table->tableid;?> 
<script type="text/javascript">
  // $(document).ready(function() {
  //   var lasttrx = "<?php echo $lasttrx;?>"
  //   //alert(lasttrx);

  //   $("#table"+lasttrx).notify("Guest Successfully Add","success",
  //      { position:"left" });


  // });
  </script>
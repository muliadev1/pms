<?php

class M_stok_opname extends CI_Model{

	function stok_opname_view($Istok,$IsProses,$set ) {
		if(empty($Istok)){
		$data = $this->db->query("select codeitem,tbsubcategory.subcategory,tbitem.description,
															unit,price,stok
															FROM tbitem inner join tbsubcategory
															ON tbitem.idsubcategory = tbsubcategory.idsubcategory
															Where tbitem.isaktif = 1 and isprosesopname=0 and  $IsProses=0 ");
		}else{
			$data = $this->db->query("SELECT codeitem,tbsubcategory.subcategory,tbitem.description,unit,price,$set  as stok 
										FROM tbitem INNER JOIN tbsubcategory ON tbitem.idsubcategory = tbsubcategory.idsubcategory
										 WHERE $Istok='1' AND tbitem.isaktif = 1 AND  $IsProses=0");

		}
		return $data->result();

   }
   function stok_opname_view_form($idsub) {
		$data = $this->db->query("select codeitem,tbsubcategory.subcategory,tbitem.description,
															unit,price,stok
															FROM tbitem inner join tbsubcategory
															ON tbitem.idsubcategory = tbsubcategory.idsubcategory
															Where tbitem.isaktif = 1 and tbitem.idsubcategory='$idsub'");
		return $data->result();

   }
    function loaddepartementall() {
		$data = $this->db->query("Select * from tbdepartement where isaktif='1' order by iddepartement asc");
		return $data->result();

   }
   

   function stok_opname_view_list() {
		$data = $this->db->query("SELECT DATE_FORMAT(DATE,'%d-%m-%Y') AS tgl FROM tbstokopname  GROUP BY tgl ");
		return $data->result();
   }
    function load_subcategory() {
		$data = $this->db->query("select * from tbsubcategory order by idsubcategory asc");
		return $data->result();
   }

   function load_so_forlaporan($tgl) {
		$data = $this->db->query("SELECT DATE_FORMAT(DATE,'%d-%m-%Y') AS tgl,`itemcode`,`description`,`unit`,`price`,`stok`,`inputstok`,`diff` ,departement,iddepartement 
									FROM tbstokopname  WHERE DATE_FORMAT(DATE,'%Y-%m-%d') =  '$tgl' ");
		return $data->result();

   }

   

  function SO_add($dataDetil,$dataPr,$dataDetilPr,$set, $IsProses) { 
       $this->db->trans_begin(); 

 		$this->db->insert_batch( 'tbstokopname', $dataDetil);
 		$this->db->insert( 'tbsr', $dataPr);
      	$this->db->insert_batch( 'tbsrdetil', $dataDetilPr);  

 		foreach ($dataDetil as $data ) {
 			$this->db->set($set,$data['inputstok']);
		    $this->db->where('codeitem',$data['itemcode'] );
		    $this->db->update('tbitem'); 
				//`isprosesopname``isprosesopnameHK``isprosesopnameFO``isprosesopnameFB``isprosesopnameBO``isprosesopnameEng``isprosesopnamePG``isprosesopnameSec`
		    $this->db->set($IsProses,1);
		    $this->db->where('codeitem',$data['itemcode'] );
		    $this->db->update('tbitem'); 
 		 } 

 		
 		if ($this->db->trans_status() === FALSE){
			   $this->db->trans_rollback();
		}else{
			    $this->db->trans_commit();
			}    
   }
	

}
?>

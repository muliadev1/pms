<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>
<script src="<?php echo base_url('assets/js/accounting.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/cleave.min.js') ?>" type="text/javascript"></script>

    <script type="text/javascript">
      function doMath()
      {
          var price = document.getElementById('price1').value;
          document.getElementById('price').value =  price.replace(/\./g, "");
      }
    </script>
    <script type="text/javascript">
      jQuery(document).ready(function($) {
        new Cleave('.input-1', {
           numeral: true, 
           numeralDecimalMark: ',',
           delimiter: '.' 
      });
          });
    </script>

<!-- Content Header (Page header) -->
<section class="content-header">
   <div class="btn-group btn-breadcrumb">
            <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
             <a href="<?php echo base_url('index.php/Item/Item_view');?>" class="btn btn-default  btn-xs">Item</a>
            <a  class="btn btn-default  btn-xs active">Edit Item</a>
        </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
             <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Edit Item </h3>
                </div>
                <div class="box-body">
                    <form method="post" id="Simpan" action="<?php echo base_url(). 'index.php/Item/Item_editDB'; ?>">
                    <div class="form-group">
                      <div class="form-group">
                              <label class="control-label">Item Code</label>
                              <input type="text" readonly="readonly" value="<?php echo $data2[0]->codeitem; ?>"  name="Id"  data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out item code..."  class="form-control"  placeholder="">
                      </div>
                      <label>Sub Category</label>
                      <select class="form-control select2" style="width: 100%;" name="idsubcategory">
                      <?php
                          foreach($data as $u){
                            if ($data2[0]->idsubcategory == $u->idsubcategory) {
                      ?>
                              <option selected="selected"  value="<?php echo $u->idsubcategory; ?>" ><?php echo $u->subcategory; ?></option>
                      <?php
                              # code...
                            }else{
                      ?>
                              <option  value="<?php echo $u->idsubcategory; ?>" ><?php echo $u->subcategory; ?></option>
                      <?php
                            }
                      ?>
                        <?php } ?>
                      </select>
                    </div>
                    <div class="form-group">
                            <label class="control-label">Description</label>
                            <input type="text" value="<?php echo $data2[0]->description; ?>"  name="description"  data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out description..."  class="form-control"  placeholder="">
                    </div>
                    <div class="form-group">
                            <label class="control-label">Unit</label>
                            <input type="text" name="unit" value="<?php echo $data2[0]->unit; ?>" data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out unit..."  class="form-control"  placeholder="">
                    </div>
                    <div class="form-group">
                            <label class="control-label">Price</label>
                            <input type="text" name="price1" id="price1" value="<?php echo $data2[0]->price; ?>" autocomplete="off" data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out price..."  onkeyup="doMath()" class="form-control input-1"  placeholder="">
                            <input type="hidden" name="price" id="price" value="<?php echo $data2[0]->price; ?>"  id="price"  onkeyup="doMath()">
                    </div>

                   <div class="form-group">
                      <button type="submit" value="Validate" class="btn btn-default"><i class='glyphicon glyphicon-ok'></i> Save</button>
                    </div>
                    </form>
                </div>
               </div>
         </div>
    </div>


</section>



<script type="text/javascript">
$("#Simpan").submit(function() {
    var price = $('#price').val();
     var description = $('#description').val();
        if (description == ''|| price==''){
            File_Kosong(); return false; 
        }else{
        event.preventDefault();
        $.confirm({
          title: 'Confirmation',
          content: 'Are You Sure to Save?',
           type: 'blue',
          buttons: {
              Simpan: function () {
                  $.LoadingOverlay("show");
                  $("#Simpan").submit();
              },
              Batal: function () {
                
                  $.alert('Data Tidak Disimpan...');
              },
          }
      });
    } 

});
</script>

<?php
$this->load->view('template/Js');
$this->load->view('template/Foot');
?>



<script type="text/javascript">
  function File_Kosong() {
  $.alert({
    title: 'Caution!!',
    content: 'Transaction Is Invalid!',
    icon: 'fa fa-warning',
    type: 'orange',
});
}
</script>

<script>
  $(function () {
    $(".select2").select2();
  });
  </script>

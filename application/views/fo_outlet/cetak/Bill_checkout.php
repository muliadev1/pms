
<?php echo $header?>

<style type="text/css">
  .TabelKonten tr td {
    padding-right: 7px;
    padding-left:  7px;
    font-size: 11px;
  }
</style>

<table class="TabelKonten"  border="0" style="border-collapse: collapse; border-color:#000000; padding-bottom : 50px ;"  width="100%" >
            <tr>
                <td width="15%"><strong> Id Rsv</strong></td>
                <td>: <?php echo $master->idreservation ?></td>
                <td width="15%"><strong>Room</strong></td>
                <td>: <?php echo $master->roomname ?></td>
              </tr>
              <tr>
                <td width="15%"><strong>Guest</strong></td>
                <td>: <?php echo $master->firstname.' '.$master->lastname ?></td>
                <td width="15%"><strong>Room Type</strong></td>
                <td>: <?php echo $master->desc ?></td>
              </tr>
              <tr>
                <td width="15%"><strong>Company</strong></td>
                <td>: <?php echo $master->company ?></td>
                <td width="15%"><strong>Arrival</strong></td>
                <td>: <?php echo $master->checkin ?></td>
              </tr> 
              <tr>
                <td width="15%"><strong>Nationality</strong></td>
                <td>: <?php echo $master->state ?></td>
                <td width="15%"><strong>Departure</strong></td>
                <td>: <?php echo $master->checkout ?></td>
              </tr>                  
          </table>
<br>

<?php   if ($periode['jenisbill']=='1') { ?>
          <table class="TabelKonten"  border="1" style="border-collapse: collapse; border-color:#000000; padding-top : 50px; margin-bottom: 20px;"  width="100%" >         
                                <thead>
                                    <tr >
                                        <th>NO</th>
                                        <th>Date</th>
                                        <th>Invoice</th>
                                        <th>Description</th>
                                        <th>Charge (IDR)</th>
                                        <th>Payment (IDR)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $noroom=1; $totalchargeroom=0; $totalpaymentroom=0; 
                                   // print_r($Dataroom);exit();
                                    foreach($Dataroom as $room){ ?>
                                    <tr style="font-weight: bold; background-color: #f0f0f0; "><th colspan="6">ROOM BILL</th></tr>
                                    <tr>
                                      <td><?php echo $noroom ?></td>
                                      <td><?php echo $room->tgl1 ?></td>
                                      <td><?php echo $room->invoicemanual ?></td>
                                      <td><?php echo $room->desc ?></td>
                                      <td align="right"><?php echo number_format($room->charge, 2, ',', '.') ?></td>
                                      <td align="right"><?php echo number_format($room->payment, 2, ',', '.') ?></td>
                                    </tr>
                                    <?php $noroom++; $totalchargeroom+=$room->charge; $totalpaymentroom+=$room->payment;} ?>
                                </tbody>

                                 <tfoot>
        <tr style=" background-color: #f0f0f0; ">
              <td colspan="4"><strong>SUMMARY</strong></td>
              <td  align="right"><strong><?php echo number_format($totalchargeroom, 2, ',', '.') ?></strong></td>
              <td  align="right"><strong><?php echo number_format($totalpaymentroom, 2, ',', '.') ?></strong></td>
         </tr>
         <tr style=" background-color: #f0f0f0; ">
              <td colspan="6">&nbsp;</td>
         </tr>
        <tr style=" background-color: #f0f0f0;">
            <td colspan="5"><strong><strong> BALANCE</td>
             <td align="right"><strong><?php echo number_format($totalpaymentroom-$totalchargeroom, 2, ',', '.') ?></strong></td>
        </tr>
    </tfoot>
                            </table>



<?php }elseif ($periode['jenisbill']=='1') { ?>
    <table class="TabelKonten"  border="1" style="border-collapse: collapse; border-color:#000000; padding-top : 50px; margin-bottom: 20px;"  width="100%" > 
                                <thead>
                                    <tr >
                                  <tr style="font-weight: bold; background-color: #f0f0f0; "><th colspan="6">CONSUME BILL</th></tr>
                                        <th>NO</th>
                                        <th>Date</th>
                                        <th>Invoice</th>
                                        <th>Description</th>
                                        <th>Charge (IDR)</th>
                                        <th>Payment (IDR)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $nocom=1; $totalchargecom=0; $totalpaymentcom=0; 
                                    foreach($Datacomsume as $com){ ?>
                                    <tr>
                                      <td><?php echo $nocom ?></td>
                                      <td><?php echo $com->tgl1 ?></td>
                                      <td><?php echo $com->trxid ?></td>
                                      <td> <?php echo $com->des ?></td>
                                      <td align="right"><?php echo number_format($com->grandtotal, 2, ',', '.') ?></td>
                                      <td align="right"><?php echo number_format($com->payment, 2, ',', '.') ?></td>
                                    </tr>
                                    <?php $nocom++; $totalchargecom+=$com->grandtotal; $totalpaymentcom+=$com->payment;} ?>
                                </tbody>
      <tfoot>
        <tr style=" background-color: #f0f0f0; ">
              <td colspan="4"><strong>SUMMARY</strong></td>
              <td  align="right"><strong><?php echo number_format($totalchargecom, 2, ',', '.') ?></strong></td>
              <td  align="right"><strong><?php echo number_format($totalpaymentcom, 2, ',', '.') ?></strong></td>
         </tr>
         <tr style=" background-color: #f0f0f0; ">
              <td colspan="6">&nbsp;</td>
         </tr>
        <tr style=" background-color: #f0f0f0;">
            <td colspan="5"><strong><strong> BALANCE</td>
             <td align="right"><strong><?php echo number_format($totalpaymentcom-$totalchargecom, 2, ',', '.') ?></strong></td>
        </tr>
    </tfoot>

                            </table>

<?php }else{ ?>


    <table class="TabelKonten"  border="1" style="border-collapse: collapse; border-color:#000000; padding-top : 50px; margin-bottom: 20px;"  width="100%" > 
                                <thead>
                              <tr style="font-weight: bold; background-color: #f0f0f0; "><th colspan="6">SUMMARY BILL</th></tr>
                                    <tr >
                                        <th>NO</th>
                                        <th>Date</th>
                                        <th>Invoice</th>
                                        <th>Description</th>
                                        <th>Charge (IDR)</th>
                                        <th>Payment (IDR)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $nosum=1; $totalchargeroom=0;$totalpaymentroom=0;$totalchargecom=0; $totalpaymentcom=0;
                                    foreach($Dataroom as $room){ ?>
                                    <tr>
                                      <td><?php echo $nosum ?></td>
                                      <td><?php echo $room->tgl1 ?></td>
                                      <td><?php echo $room->invoicemanual ?></td>
                                      <td><?php echo $room->desc ?></td>
                                      <td align="right"><?php echo number_format($room->charge, 2, ',', '.') ?></td>
                                      <td align="right"><?php echo number_format($room->payment, 2, ',', '.') ?></td>
                                    </tr>
                                    <?php $nosum++; $totalchargeroom+=$room->charge; $totalpaymentroom+=$room->payment;} ?>

                                    <?php foreach($Datacomsume as $com){ ?>
                                    <tr>
                                      <td><?php echo $nosum ?></td>
                                      <td><?php echo $com->tgl1 ?></td>
                                      <td><?php echo $com->trxid ?></td>
                                      <td> <?php echo $com->des ?></td>
                                      <td align="right"><?php echo number_format($com->grandtotal, 2, ',', '.') ?></td>
                                      <td align="right"><?php echo number_format($com->payment, 2, ',', '.') ?></td>
                                    </tr>
                                    <?php $nosum++; $totalchargecom+=$com->grandtotal; $totalpaymentcom+=$com->payment;} ?>
                                </tbody>
                     <tfoot>
        <tr style=" background-color: #f0f0f0; ">
              <td colspan="4"><strong>SUMMARY</strong></td>
              <td  align="right"><strong><?php echo number_format($totalchargeroom + $totalchargecom, 2, ',', '.') ?></strong></td>
              <td  align="right"><strong><?php echo number_format($totalpaymentroom + $totalpaymentcom, 2, ',', '.') ?></strong></td>
         </tr>
         <tr style=" background-color: #f0f0f0; ">
              <td colspan="6">&nbsp;</td>
         </tr>
        <tr style=" background-color: #f0f0f0;">
            <td colspan="5"><strong> BALANCE</td>
            <td  align="right"><strong><?php echo number_format( ($totalpaymentroom + $totalpaymentcom)-($totalchargeroom+$totalchargecom), 2, ',', '.') ?></strong></td>
        </tr>
                                </tfoot>
                            </table>
        <?php } ?>
<?php

class M_item extends CI_Model{

	function getkodeitem() { 
        $q = $this->db->query("SELECT MAX(RIGHT(codeitem,5)) AS idmax FROM tbitem");
        $kd = ""; //kode awal
        if($q->num_rows()>0){ //jika data ada
            foreach($q->result() as $k){
                $tmp = ((int)$k->idmax)+1; //string kode diset ke integer dan ditambahkan 1 dari kode terakhir
                $kd = "0000".$tmp; //kode ambil 4 karakter terakhir
                $id = substr($kd, -5);
            }
        }else{ //jika data kosong diset ke kode awal
            $kd = "00001";
        }
        //$kar = "I"; //karakter depan kodenya
        //gabungkan string dengan kode yang telah dibuat tadi
        return $id;
   } 

	function Item_view() {
		$data = $this->db->query("
															select codeitem,tbsubcategory.subcategory,tbitem.description,
																	unit,price,stok
															FROM tbitem inner join tbsubcategory
															ON tbitem.idsubcategory = tbsubcategory.idsubcategory
															Where tbitem.isaktif = 1");
		return $data->result();

   }
   
   function load_stok_bydepartement($Departement,$idsub) {
   	 if ($Departement==1) {
        $set = 'stokBO';
         $setaset = 'isBO';
       }elseif ($Departement==2) {
        $set = 'stokHK';
        $setaset = 'isHK';
       }elseif ($Departement==3) {
         $set = 'stokFO';
          $setaset = 'isFO';
       }elseif ($Departement==4) {
          $set = 'stokFB';
           $setaset = 'isFB';
       }elseif ($Departement==5) {
        $set = 'stokBO';
         $setaset = 'isBO';
      }elseif ($Departement==6) {
        $set = 'stokBar';
        $setaset = 'isBar';
      }elseif ($Departement==7) {
        $set = 'stokEng';
        $setaset = 'isEng';
      }elseif ($Departement==8) {
        $set = 'stokPG';
        $setaset = 'isPG';
      }elseif ($Departement==9) {
        $set = 'stokSec';
        $setaset = 'isSec';
       }else{
         $set = 'stok';
        $setaset = '-';
       }

   if ($Departement!=0) {
      $data = $this->db->query("SELECT tbitem.*,subcategory,$set AS stok 
                    FROM tbitem inner join tbsubcategory
                    ON tbitem.idsubcategory = tbsubcategory.idsubcategory
                    INNER JOIN tbsubcategorydepartement ON tbitem.`codeitem` = tbsubcategorydepartement.`itemcode` 
                              Where tbitem.isaktif = 1 
                              and tbsubcategorydepartement.kodesubcategory='$idsub'
                              and tbsubcategorydepartement.iddepartement='$Departement' ");
   }else{
     $data = $this->db->query("SELECT tbitem.*,subcategory 
                    FROM tbitem inner join tbsubcategory
                    ON tbitem.idsubcategory = tbsubcategory.idsubcategory
                     Where  tbsubcategory.idsubcategory='$idsub' and tbitem.isaktif = 1  
                               ");

   }  
		return $data->result();

   }


   function load_item_bydepartement_rekap($Departement,$idsub) {
   if ($Departement!=0) {
      $data = $this->db->query("SELECT tbitem.*,subcategory,tbsubcategorydepartement.kodesubcategory
                    FROM tbitem inner join tbsubcategory
                    ON tbitem.idsubcategory = tbsubcategory.idsubcategory
                    INNER JOIN tbsubcategorydepartement ON tbitem.`codeitem` = tbsubcategorydepartement.`itemcode` 
                              Where tbitem.isaktif = 1 
                              and tbsubcategorydepartement.kodesubcategory='$idsub'
                              and tbsubcategorydepartement.iddepartement='$Departement' group by tbitem.codeitem ");
   }else{
     $data = $this->db->query("SELECT tbitem.*,subcategory 
                    FROM tbitem inner join tbsubcategory
                    ON tbitem.idsubcategory = tbsubcategory.idsubcategory
                     Where  tbsubcategory.idsubcategory='$idsub' and tbitem.isaktif = 1  
                               ");

   }  
    return $data->result();

   }
   function load_item_bydepartement($Departement,$idsub) {
   if ($Departement!=0) {
      $data = $this->db->query("SELECT tbitem.*,subcategory,tbsubcategorydepartement.kodesubcategory
                    FROM tbitem inner join tbsubcategory
                    ON tbitem.idsubcategory = tbsubcategory.idsubcategory
                    INNER JOIN tbsubcategorydepartement ON tbitem.`codeitem` = tbsubcategorydepartement.`itemcode` 
                              Where tbitem.isaktif = 1 
                              and tbsubcategorydepartement.kodesubcategory='$idsub'
                              and tbsubcategorydepartement.iddepartement='$Departement' group by tbitem.codeitem ");
   }else{
     $data = $this->db->query("SELECT tbitem.*,subcategory 
                    FROM tbitem inner join tbsubcategory
                    ON tbitem.idsubcategory = tbsubcategory.idsubcategory
                     Where  tbsubcategory.idsubcategory='$idsub' and tbitem.isaktif = 1  
                               ");

   }  
    return $data->result();

   }
  

  function load_stokIN_by_item($Departement,$codeitem,$tgl1,$tgl2) {
   if ($Departement!=0) {
      $data = $this->db->query("SELECT tbsrdetil.id,tbitem.`description`,SUM(tbsrdetil.`qtyorder`)  AS stok FROM tbsr
                    INNER JOIN`tbsrdetil` ON tbsr.`kodesr` = tbsrdetil.`kodesr`
                   INNER JOIN `tbitem` ON tbsrdetil.`codeitem` =  tbitem.`codeitem`
                    INNER JOIN tbsubcategory
                    ON tbitem.idsubcategory = tbsubcategory.idsubcategory
                    INNER JOIN tbsubcategorydepartement ON tbitem.`codeitem` = tbsubcategorydepartement.`itemcode` 
                
                    WHERE tbitem.isaktif = 1 
                    AND tbsrdetil.codeitem='$codeitem'
                    AND tbsrdetil.`iddepartement`='$Departement'
                    AND tbsubcategorydepartement.itemcode='$codeitem'
                      AND tbsubcategorydepartement.`iddepartement`='$Departement'                              
                    AND DATE(tbsr.`dateadd`) >= '$tgl1'   AND DATE(tbsr.`dateadd`) <= '$tgl2' ");
   }else{
     $data = $this->db->query("SELECT tbitem.*,subcategory 
                    FROM tbitem inner join tbsubcategory
                    ON tbitem.idsubcategory = tbsubcategory.idsubcategory
                     Where  tbsubcategory.idsubcategory='$idsub' and tbitem.isaktif = 1  
                               ");

   }  
    return $data->row();

   }

   function load_stok_awal($Departement,$idsubcategory,$codeitem,$tgl1) {

   $datamaster = $this->db->query(" SELECT * FROM `tbbackupstokmaster` 
WHERE  tbbackupstokmaster.`iddepartement` = '$Departement' AND idsubcategory='$idsubcategory' AND tbbackupstokmaster.date <='$tgl1' ORDER BY tbbackupstokmaster.id DESC LIMIT 1 ")->row();
  
   if (!empty($datamaster)) {
    $idmaster = $datamaster->id;
     $datadetil = $this->db->query("SELECT * FROM `tbbackupstokdetil` where idmaster='$idmaster' and itemcode= '$codeitem' ");
     
     // print_r($datadetil->row());exit();
     if (!empty($datadetil)) {
        return $datadetil->row();
     }else{
        return '-';
     }
   
   }else{
    return '-';
   }

}

    function load_stokOut_by_item($Departement,$codeitem,$tgl1,$tgl2) {
   if ($Departement!=0) {
      $data = $this->db->query("SELECT tbstokopname.id,tbitem.`description`,SUM(`tbstokopname`.`diff`)  AS stok FROM tbstokopname
                   INNER JOIN `tbitem` ON tbstokopname.`itemcode` =  tbitem.`codeitem`
                    INNER JOIN tbsubcategory
                    ON tbitem.idsubcategory = tbsubcategory.idsubcategory
                    INNER JOIN tbsubcategorydepartement ON tbitem.`codeitem` = tbsubcategorydepartement.`itemcode` 
                
                    WHERE tbitem.isaktif = 1 
                    AND tbstokopname.itemcode='$codeitem'
                    AND tbstokopname.`iddepartement`='$Departement'
                    AND tbstokopname.diff>0
                    AND tbsubcategorydepartement.itemcode='$codeitem'
                      AND tbsubcategorydepartement.`iddepartement`='$Departement'                              
                    AND DATE(tbstokopname.`date`) >= '$tgl1'  AND DATE(tbstokopname.`date`) <= '$tgl2' ");
   }else{
     $data = $this->db->query("SELECT tbitem.*,subcategory 
                    FROM tbitem inner join tbsubcategory
                    ON tbitem.idsubcategory = tbsubcategory.idsubcategory
                     Where  tbsubcategory.idsubcategory='$idsub' and tbitem.isaktif = 1  
                               ");

   }  
    return $data->row();

   }


   function loaddepartementall() {
		$data = $this->db->query("Select * from tbdepartement where isaktif='1' order by iddepartement asc");
		return $data->result();

   }
    function load_akun() { 
         $data = $this->db->query("SELECT `NoAkun`,CONCAT(`NamaSub`, ' - '  ,tbakun.`NamaAkun`) AS nama FROM tbakun INNER JOIN `tbsubklasifikasi`
                    ON tbakun.`IdSub`  =  tbsubklasifikasi.`IdSub` ORDER BY tbsubklasifikasi.IdSub ASC");
         return $data->result();

    }

	 function Item_addDB($table,$data) {
			 $this->db->insert($table,$data);
			// print_r($this->db->last_query());
	}

  function backup_stok($data,$datadetil,$iddepartement,$idsubcategory) {
       $this->db->insert('tbbackupstokmaster',$data);
        $id = $this->db->insert_id();

  for ($i=0; $i <count($datadetil) ; $i++) { 
   $datadetil1[] = array(
        'idmaster'=> $id,
        'itemcode'=> $datadetil[$i][0],
        'price'=> $datadetil[$i][4],
        'stock'=> $datadetil[$i][5]
     );
  }
 // print_r($datadetil);exit();
  $this->db->insert_batch('tbbackupstokdetil', $datadetil1);
}

	function Item_view_edit($id) {
		$data = $this->db->query("	select codeitem,tbsubcategory.subcategory,tbitem.description,
							`akunHK`,`akunFO`,`akunFB`,`akunBO`,`akunBar`,`akunSec`,`akunPG`,`akunEng`,
																		unit,price,stok,tbsubcategory.idsubcategory
																FROM tbitem inner join tbsubcategory
																ON tbitem.idsubcategory = tbsubcategory.idsubcategory
																Where tbitem.isaktif = 1 and codeitem = $id ");
	 return $data->result();
	}

	function Load_history_priceitem($codeitem) {
		$data = $this->db->query("SELECT tbitem.`codeitem`,description,unit,tbhistoryitemprice.price, tbhistoryitemprice.`newprice`, DATE_FORMAT(tbhistoryitemprice.date,'%d-%m-%Y') AS tgl  FROM `tbhistoryitemprice` INNER JOIN tbitem
								  ON tbhistoryitemprice.`codeitem`  = tbitem.codeitem where tbitem.codeitem='$codeitem' 
								  order by tbhistoryitemprice.id desc ");
	 return $data->result();
	}

	function Item_editDB($table,$data,$id) {
 			 $this->db->where('codeitem',$id );
 			 $this->db->update($table,$data);
  }

}
?>

<?php

class M_source extends CI_Model{

	function Source_view() {
		$data = $this->db->query("SELECT * FROM tbsource WHERE isaktif=1");
		return $data->result();
   }

	 function Source_addDB($table,$data) {
			 $this->db->insert($table,$data);
	}

	function Source_view_edit($id) {
	 $this->db->select("*");
	 $this->db->where('id',$id);
	 $this->db->from('tbsource');
	 return $this->db->get()->result();
	}

	function Source_editDB($table,$data,$id) {
 			 $this->db->where('id',$id );
 			 $this->db->update($table,$data);
  }

}
?>

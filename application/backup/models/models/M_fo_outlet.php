<?php

class M_fo_outlet extends CI_Model{

	// function FO_view() {
	// 	$data = $this->db->query("SELECT * FROM tbguest WHERE isaktif=1");
	// 	return $data->result();
 //   }
   function FO_view() {
    $data = $this->db->query("SELECT tbguest.*, tbreservation.*, CONCAT(`firstname`, ' '  ,lastname) AS nama FROM `tbreservation` INNER JOIN tbguest
                              ON tbreservation.`idguest` =  tbguest.`idguest`
                              WHERE tbreservation.`idreservationstatus` =4");
                              return $data->result();
   }

   function Reservation_list_view() {
    $data = $this->db->query("SELECT tbreservation.`idreservation`,STATUS, tbroom.`roomname`, tbroomtype.`roomtype`, CONCAT(`firstname`, ' '  ,lastname) AS nama ,
                              DATE_FORMAT(checkin,'%d-%m-%Y') AS checkin, DATE_FORMAT(checkout,'%d-%m-%Y') AS checkout, duration,tbcompany.`name`, notes
                              FROM `tbreservation` INNER JOIN tbguest
                              ON tbreservation.`idguest` =  tbguest.`idguest` INNER JOIN `tbreservationstatus`
                              ON tbreservation.`idreservationstatus` = tbreservationstatus.`id` INNER JOIN `tbroom`
                              ON tbreservation.`idroom` = tbroom.`id` INNER JOIN `tbroomtype`
                              ON tbreservation.`idroomtype` = tbroomtype.`id` INNER JOIN tbcompany
                              ON tbreservation.`idcompany` = tbcompany.`idcompany`
                              WHERE tbreservation.`statusreservation` <>4");
                              return $data->result();
   }

   function checkout_master($idrsv) {
     $sql = "SELECT  tbreservation.idreservation, tbreservationstatus.status, tbRoom.roomname AS Expr1, tbroomtype.desc, tbguest.saluation, tbguest.firstname,tbguest.lastname, DATE_FORMAT(tbreservation.checkin,'%m/%d/%Y') AS checkin,
             DATE_FORMAT(tbreservation.checkout,'%m/%d/%Y') AS checkout, tbreservation.duration, tbguest.state AS Expr2,tbsegment.segment AS Expr3, tbcompany.name AS Expr4, tbreservation.notes, tbReservation.msg
             FROM tbreservation INNER JOIN tbguest ON tbreservation.idguest = tbguest.idguest INNER JOIN tbroomtype ON tbreservation.idroomtype = tbroomtype.id INNER JOIN
             tbroom ON tbreservation.idroom = tbroom.id INNER JOIN tbsegment ON tbreservation.idsegment = tbsegment.id INNER JOIN tbcompany ON tbreservation.idcompany = tbcompany.idcompany
             INNER JOIN tbreservationstatus ON tbreservation.idreservationstatus = tbreservationstatus.id
             WHERE tbreservation.idreservation = '$idrsv' ";
   
    $data = $this->db->query($sql);
    return $data->result();
  }

    function checkin_guest($idreservation) {
    $data = $this->db->query("SELECT tbreservation.*, CONCAT(`firstname`, ' '  ,lastname) AS nama, phone, date_format(checkin,'%m/%d/%Y') as tglin, date_format(checkout,'%m/%d/%Y') as tglout, date_format(cod,'%m/%d/%Y') as tglcod FROM `tbreservation` INNER JOIN tbguest
                              ON tbreservation.`idguest` =  tbguest.`idguest`
                              WHERE tbreservation.`idreservation` = '$idreservation'  ");
                              return $data->row();
   }
    function checkin_guest_rsv_detil($idreservation) {
    $data = $this->db->query("SELECT * from tbreservationdetil
                              WHERE tbreservationdetil.`idreservation` = '$idreservation'  ");
                              return $data->row();
   }
   function checkin_guest_fo_detil($idreservation) {
    $data = $this->db->query("SELECT tbfotrxdetil.* FROM tbfotrx INNER JOIN tbfotrxdetil
                              ON tbfotrx.`kodemaster` = tbfotrxdetil.`kodemaster`
                                WHERE tbfotrx.`kodereservation` = '$idreservation'  ");
                              return $data->row();
   }
   

   function checkin_guest_detil($idreservation) {
    $data = $this->db->query(" SELECT tbguest.* FROM `tbreservationguestlist` INNER JOIN tbguest
                                ON tbreservationguestlist.`idguest` = tbguest.`idguest`
                              WHERE tbreservationguestlist.`idreservation` = '$idreservation'  ");
                              return $data->result();
   }
  
   
   function Reservation_list_view_bykode($idreservation) {
    $data = $this->db->query("SELECT tbreservation.*, CONCAT(`firstname`, ' '  ,lastname) AS nama,phone, tbreservation.`idroom` AS idroom1,
                              (SELECT roomname FROM tbroom WHERE id = idroom1 ) AS roomname
                              FROM `tbreservation` 
                              INNER JOIN tbguest
                              ON tbreservation.`idguest` =  tbguest.`idguest`
                              WHERE tbreservation.`statusreservation` <>4 AND idreservation='$idreservation'");
                              return $data->row();
   }
   function Reservation_list_view_bykode_detilguest($idreservation) {
    $data = $this->db->query(" 
                                SELECT tbreservationguestlist.*, CONCAT(firstname,' ', lastname) AS nama FROM `tbreservation` INNER JOIN `tbreservationguestlist`
                              ON tbreservation.`idreservation` = tbreservationguestlist.`idreservation`INNER JOIN tbguest 
                              ON tbreservationguestlist.`idguest` =  tbguest.`idguest` 
                              WHERE tbreservationguestlist.`idreservation` = '$idreservation' ");
                              return $data->result();
   }

   function Reservation_list_view_bykode_detil($idreservation) {
    $data = $this->db->query(" SELECT `tbreservationdetil`.*,tbpackage.`package`,rate FROM  tbreservationdetil  INNER JOIN tbpackage
                                ON tbreservationdetil.`idpackage` = tbpackage.`id`
                                WHERE `idreservation` = '$idreservation'");
                              return $data->row();
   }

  
    function get_kodersv($table) { 
        $tahun = date('y'); 
        $bulan = date('m'); 
        $q = $this->db->query("SELECT MAX(RIGHT(idreservation,4)) AS idmax FROM ".$table);
        $kd = ""; //kode awal
        if($q->num_rows()>0){ //jika data ada
            foreach($q->result() as $k){
                $tmp = ((int)$k->idmax)+1; //string kode diset ke integer dan ditambahkan 1 dari kode terakhir
                $kd = "000".$tmp; //kode ambil 4 karakter terakhir
                $id = substr($kd, -4);
            }
        }else{ //jika data kosong diset ke kode awal
            $kd = $tahun.$bulan."0001";
        }
        $kar = "RS"; //karakter depan kodenya
        //gabungkan string dengan kode yang telah dibuat tadi
        return $kar.$tahun.$bulan.$id;
   }

      function get_fotrx($table) { 
        $tahun = date('y'); 
        $bulan = date('m'); 
        $q = $this->db->query("SELECT MAX(RIGHT(kodemaster,4)) AS idmax FROM ".$table);
        $kd = ""; //kode awal
        if($q->num_rows()>0){ //jika data ada
            foreach($q->result() as $k){
                $tmp = ((int)$k->idmax)+1; //string kode diset ke integer dan ditambahkan 1 dari kode terakhir
                $kd = "000".$tmp; //kode ambil 4 karakter terakhir
                $id = substr($kd, -4);
            }
        }else{ //jika data kosong diset ke kode awal
            $kd = $tahun.$bulan."0001";
        }
        $kar = "RS"; //karakter depan kodenya
        //gabungkan string dengan kode yang telah dibuat tadi
        return $kar.$tahun.$bulan.$id;
   }
    function get_kodebill($table) { 
        $tahun = date('y'); 
        $bulan = date('m'); 
        $q = $this->db->query("SELECT MAX(RIGHT(kodemasterbil,4)) AS idmax FROM ".$table);
        $kd = ""; //kode awal
        if($q->num_rows()>0){ //jika data ada
            foreach($q->result() as $k){
                $tmp = ((int)$k->idmax)+1; //string kode diset ke integer dan ditambahkan 1 dari kode terakhir
                $kd = "000".$tmp; //kode ambil 4 karakter terakhir
                $id = substr($kd, -4);
            }
        }else{ //jika data kosong diset ke kode awal
            $kd = $tahun.$bulan."0001";
        }
        $kar = "BL"; //karakter depan kodenya
        //gabungkan string dengan kode yang telah dibuat tadi
        return $kar.$tahun.$bulan.$id;
   }

   function get_article_sales() {
		$data = $this->db->query("   SELECT `idarticle`,`articlename`,finalprice FROM `tbarticlesales`");
		return $data->result();
   }
   function get_segment() {
		$data = $this->db->query("SELECT * FROM `tbsegment`");
		return $data->result();
   }
   function get_articlepayment() {
    $data = $this->db->query("SELECT * FROM `tbarticlepayment`");
    return $data->result();
   }
   function get_article_payment() {
		$data = $this->db->query("SELECT `id`,`articlepayment` FROM `tbarticlepayment`");
		return $data->result();
   }
    function get_fo_invoice($IdReservation) {
    $data = $this->db->query("SELECT tbfotrxdetil.*, date_format(tgl,'%d-%m-%Y') as tgl1, nama_user FROM `tbfotrxdetil`
      inner join tb_user on tbfotrxdetil.idoperator = tb_user.id_user ");
    return $data->result();
   }
   function get_purpose() {
		$data = $this->db->query("SELECT * FROM `tbpurpose`");
		return $data->result();
   }
   function get_source() {
		$data = $this->db->query("SELECT * FROM `tbsource`");
		return $data->result();
   }
   function get_room() {
		$data = $this->db->query("SELECT id,idroomtype, roomname FROM tbroom");
		return $data->result();
   }
   function get_room_staus($idroom) {
    $data = $this->db->query(" SELECT tbroomstatus.* FROM `tbroom` INNER JOIN `tbroomstatus`
ON tbroom.`idroomstatus` = tbroomstatus.`id` where tbroom.id = '$idroom' ");
    return $data->row();
   }
  
   function get_rsvstatus() {
		$data = $this->db->query("SELECT * FROM tbreservationstatus");
		return $data->result();
   }
   function get_roomtype() {
		$data = $this->db->query("SELECT id,roomtype, rate FROM `tbroomtype`");
		return $data->result();
   }
   function Guest_view() {
		$data = $this->db->query("Select * FROM tbguest WHERE isaktif=1");
		return $data->result();
   }
   function Company_view() {
		$data = $this->db->query("SELECT * FROM tbcompany WHERE isaktif=1");
		return $data->result();
   }

   function get_room_by_type($idtype) {
		$data = $this->db->query("SELECT id,idroomtype, roomname FROM tbroom where idroomtype = '$idtype' ");
		return $data->result();
   }
   function get_package() {
		$data = $this->db->query("SELECT * FROM `tbpackage` GROUP BY id ");
		return $data->result();
   }
   function get_package_by_code($id) {
		$data = $this->db->query("SELECT * FROM `tbpackage` where id = '$id' ");
		return $data->row();
   }

    function add_rsv($datamaster,$datamasterdetilguest,$dataDetil) {
       $this->db->insert( 'tbreservation', $datamaster);
        $this->db->insert_batch( 'tbreservationguestlist', $datamasterdetilguest);
         $this->db->insert_batch( 'tbreservationdetil', $dataDetil);
      // return $this->db->insert_id();
   }

   function add_fotrx($datamaster,$datadetil) {
       $this->db->insert( 'tbfotrx', $datamaster);
        $this->db->insert_batch( 'tbfotrxdetil', $datadetil);
   }
   function add_fotrx_invoice($datadetil) {
        $this->db->insert_batch( 'tbfotrxdetil', $datadetil);
   }

    function add_biltrx($datamaster,$datadetil) {
       $this->db->insert( 'tbmasterbill', $datamaster);
        $this->db->insert_batch( 'tbmasterbilldetil', $datadetil);
   }

   function add_guest($data)
   {
      $this->db->insert('tbguest',$data);
   }
   function checkin_add($data,$id) {
       $this->db->where('idreservation',$id );
       $this->db->update('tbreservation',$data);
  }
   function update_fotrx($data,$id) {
       $this->db->where('kodemaster',$id );
       $this->db->update('tbfotrxdetil',$data);
   }
   function update_biltrx($datamaster,$datadetil,$id) {
       $this->db->where('idreservation',$id );
       $this->db->update('tbmasterbill',$datamaster);

       $this->db->where('idreservation',$id );
       $this->db->update('tbmasterbilldetil',$datadetil);
   }

  
   


	

}
?>

<?php

class M_segment extends CI_Model{

	function Segment_view() {
		$data = $this->db->query("SELECT * FROM tbsegment WHERE isaktif=1");
		return $data->result();
   }

	 function Segment_addDB($table,$data) {
			 $this->db->insert($table,$data);
	}

	function Segment_view_edit($id) {
	 $this->db->select("*");
	 $this->db->where('id',$id);
	 $this->db->from('tbsegment');
	 return $this->db->get()->result();
	}

	function Segment_editDB($table,$data,$id) {
 			 $this->db->where('id',$id );
 			 $this->db->update($table,$data);
  }

}
?>

<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>

<!-- Content Header (Page header) -->
<section class="content-header">
   <div class="btn-group btn-breadcrumb">
            <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
             <a href="<?php echo base_url('index.php/Article/Article_sales_view');?>" class="btn btn-default  btn-xs">Article Sales</a>
            <a  class="btn btn-default  btn-xs active">Edit Article Sales</a>
        </div>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
   <div class="col-md-12">
    <div class="box box-info">
     <div class="box-header">
      <h3 class="box-title">Edit Article Sales</h3>
     </div>

     <div class="box-body">
      <form method="post" id="Simpan"  action="<?php echo base_url(). 'index.php/Article/Article_sales_editDB'; ?>">
      	<input type="hidden" name="idarticle" value="<?php echo $edit->idarticle; ?>">
       <div class="form-group">
        <label class="control-label">Article Sales Name</label>
        <input type="text" name="articlename" id="articlename"  data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out article sales name..."  class="form-control"  placeholder="articlename" value="<?php echo $edit->articlename ?>">
       </div>

       <div class="form-group">
        <label class="control-label">Price</label>
        <input type="number" name="price" id="price"  data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out price..."  class="form-control"  placeholder="price" value="<?php echo $edit->price ?>">
       </div>
       <div class="form-group">
        <label class="control-label">Service price</label>
        <input type="number" name="service" id="service"  data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out service price..."  class="form-control"  placeholder="service" value="<?php echo $edit->service ?>">
       </div>
       <div class="form-group">
        <label class="control-label">Tax price</label>
        <input type="number" name="tax" id="tax"  data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out tax price..."  class="form-control"  placeholder="tax" value="<?php echo $edit->tax ?>">
       </div>
       <div class="form-group">
        <label class="control-label">Final Price</label>
        <input type="number" name="finalprice" id="finalprice"  data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out final price..."  class="form-control"  placeholder="final price" value="<?php echo $edit->finalprice ?>">
       </div>
      <div class="form-group">
       <label>Article Sales Category</label>
       <select id="idarticlecategory" name="idarticlecategory" class="form-control select2"  style="width: 100%;">
        	<option value="<?php echo $edit->idarticlecategory; ?>"><?php echo $edit->articlecategory; ?></option>
         <?php
          foreach($category as $u){
              ?>
            <option value="<?php echo $u->id; ?>"><?php echo $u->articlecategory; ?></option>
              <?php } ?>
       </select>
      </div>
      <div class="form-group">
       <label>Price Type</label>
       <select id="pricetype" name="pricetype" class="form-control select2"  style="width: 100%;">
        	<option value="<?php echo $edit->pricetype; ?>"><?php echo $edit->pricetype; ?></option>
            <option value="Tax&Service">Tax & Service</option>
            <option value="Tax">Tax</option>
       </select>
      </div>
      <div class="form-group">
       <label>Departement</label>
       <select id="iddepartement" name="iddepartement" class="form-control select2"  style="width: 100%;">
        	<option value="<?php echo $edit->iddepartement; ?>"><?php echo $edit->departement; ?></option>
         <?php
          foreach($departement as $a){
              ?>
            <option value="<?php echo $a->iddepartement; ?>"><?php echo $a->departement; ?></option>
              <?php } ?>
       </select>
      </div>
          <div class="form-group">
            <button type="submit" value="Validate" class="btn btn-default"><i class='glyphicon glyphicon-ok'></i> Save</button>
          </div>
        </form>
       </div>
      </div>
     </div>
    </div>
</section>

<script type="text/javascript">
$("#Simpan").submit(function() {

    var saluation = $('#saluation').val();
    var firstname = $('#firstname').val();
    var lastname = $('#lastname').val();
    var gender = $('#gender').val();
    var birthday = $('#birthday').val();
    var idtype  = $('#idtype').val();
    var idnumber = $('#idnumber').val();
    var description = $('#description').val();
    var country = $('#state').val();
    var phone = $('#phone').val();
    var email = $('#email').val();
    var zipcode = $('#zipcode').val();
    var address = $('#address').val();
        if (saluation == ''|| firstname=='' || lastname=='' || gender=='' || birthday=='' || idtype==''
             || idnumber==''|| description==''|| country==''|| phone==''|| email==''|| zipcode==''|| address==''){
            File_Kosong(); return false;
        }else{
        event.preventDefault();
        $.confirm({
          title: 'Confirmation',
          content: 'Are You Sure to Save?',
           type: 'blue',
          buttons: {
              Save: function () {
                  $.LoadingOverlay("show");
                  $("#Simpan").submit();
              },
              Cancel: function () {

                  $.alert('Data Not Saved...');
              },
          }
      });
    }

});
</script>

<?php
$this->load->view('template/Js');
$this->load->view('template/Foot');
?>



<script type="text/javascript">
  function File_Kosong() {
  $.alert({
    title: 'Caution!!',
    content: 'Add Data Invalid!',
    icon: 'fa fa-warning',
    type: 'orange',
});
}
</script>

<script>
$(function () {
  $(".select2").select2();
   $(".select3").select2();

});
</script>

<script>
$( function() {
$( "#birthday" ).datepicker({
autoclose: true,
dateFormat: 'yy/mm/dd'
});
});
</script>
<script type="text/javascript">
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });
</script>


      <form method="post" id="Simpan" action="<?php echo base_url(). 'index.php/Stok_opname/add_proses_opname'; ?>">
      <input type="hidden" name="Istok"  value="<?php echo $departement['istok'] ;?>">
      <input type="hidden" name="Set"  value="<?php echo $departement['set'] ;?>">
      <input type="hidden" name="Departement"  value="<?php echo $departement['dep'] ;?>">
      <input type="hidden" name="iddepartement"  value="<?php echo $departement['iddep'] ;?>">
        <div class="table-responsive">
          <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr >
                                        <th >Code Aset</th>
                                        <th >Description</th>
                                        <?php if (empty($departement['idsub'])) { ?>
                                         <th >Subcategory</th>
                                        <?php }?>
                                        <th >Stock Bigin</th>
                                        <th >Store Departement</th>
                                        <th >Stock Used</th>                         
                                        <th >Lose & BRKT</th>
                                        <th >Out</th>
                                        <th >Total Stock</th>
                                         <th >Req In</th>
                                        <th >View History</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i=1;
                                    foreach($data as $u){
                                      $qtyreq = $this->M_aset->load_request_in($departement['iddep'], $u->codeitem) ;
                                    ?>
                                    <tr  class='odd gradeX context'>
                                        <td class="codeaset"><?php echo $u->codeaset ?></td>
                                        <td class="desc"><?php echo $u->description?></td>
                                         <?php if (empty($departement['idsub'])) { ?>
                                        <td><?php echo $u->subcategory?></td>
                                        <?php }?>
                                        <td><?php echo number_format($u->masuk+$u->keluar, 0, '.', '.')?></td>
                                         <td><?php echo number_format($u->masuk-$u->used, 0, '.', '.')?></td>
                                        <td><?php echo number_format($u->used, 0, '.', '.')?></td>
                                        <td><?php echo number_format($u->lose, 0, '.', '.')?></td>
                                        <td><?php echo number_format($u->keluar, 0, '.', '.')?></td>
                                        <td><?php echo number_format($u->masuk+$u->keluar-$u->keluar, 0, '.', '.')?></td>
                                        <td>0</td>
                                        <td align="center"> 
                                        <a class="btn btn-info btn-xs ViewDetail" title="View History Price"   style="cursor:pointer" >  <span class="fa fa-fw fa-list " ></span> </a>
                                        </td>

                                    </tr>
                                    <?php $i++;} ?>
                                </tbody>
                            </table>
          </div>



        </div><!-- /.box-body -->
        </form>
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->


<script type="text/javascript">
  $(document).ready(function(){
     $(".ViewDetail").click(function(e){ 
     var dep = $('#IdDepartement').val();  
      var codeaset = $(this).closest('tr').children('td.codeaset').text(); 
      var namadep =  $("#IdDepartement option:selected").text();
       var namaaset = $(this).closest('tr').children('td.desc').text();     
      $.ajax({
        type: "POST",
       url: '<?php echo site_url('Aset/Aset_view_detil');?>',
        data: {dep: dep, codeaset:codeaset  },
        success: function(data){
            e.preventDefault();
            var mymodal1 = $('#myModalNew');
            mymodal1.modal({backdrop: 'static', keyboard: false}) 
            var height = $(window).height() - 200;
            mymodal1.find(".modal-body").css("max-height", height);
             $("#CodeAsetm span").text(namaaset);
              $("#Departementm span").text(namadep);
            mymodal1.find('.modal-body').html(data);
            mymodal1.modal('show');            
        }
      });
    });
  });
</script>


<script type="text/javascript">
  function File_Kosong() {
  $.alert({
    title: 'Caution!!',
    content: 'Transaction Is Invalid!',
    icon: 'fa fa-warning',
    type: 'orange',
});
}
</script>

<script>
  $(function () {
    
    $('#example1').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
  });
</script>

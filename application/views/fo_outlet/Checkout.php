<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>



<section class="content-header">
    <div class="btn-group btn-breadcrumb">
      <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
             <a href="<?php echo base_url('index.php/Reservation/ExpectedDeparture_view');?>" class="btn btn-default  btn-xs">Expected Departure</a>
            <a  class="btn btn-default  btn-xs active">Checkout</a>
        </div>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-xs-12">
      <div class="box box-solid bg-teal-gradient collapsed-box">
        <div class="box-header">
           <div class="box-tools pull-right">
                <button type="button" class="btn bg-teal btn-sm" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
          <i class="fa fa-user"></i>
          <h3 class="box-title"> <?php echo $master->firstname.' '.$master->lastname ?> </h3>
        </div>
    <!-- /.box-header -->
        <div class="box-body">
        <div class="table-responsive">
          <table width="100%">
            <tr>
                <td width="15%"><strong> Id Rsv</strong></td>
                <td>: <?php echo $master->idreservation ?></td>
                <td width="15%"><strong>Room</strong></td>
                <td>: <?php echo $master->roomname ?></td>
              </tr>
              <tr>
                <td width="15%"><strong>Guest</strong></td>
                <td>: <?php echo $master->firstname.' '.$master->lastname ?></td>
                <td width="15%"><strong>Room Type</strong></td>
                <td>: <?php echo $master->desc ?></td>
              </tr>
              <tr>
                <td width="15%"><strong>Company</strong></td>
                <td>: <?php echo $master->company ?></td>
                <td width="15%"><strong>Arrival</strong></td>
                <td>: <?php echo $master->checkin ?></td>
              </tr> 
              <tr>
                <td width="15%"><strong>Nationality</strong></td>
                <td>: <?php echo $master->state ?></td>
                <td width="15%"><strong>Departure</strong></td>
                <td>: <?php echo $master->checkout ?></td>
              </tr>                  
          </table>
          </div>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div>

  <div class="row">
    <div class="col-xs-12">
      <div class="box box-info">
        <div class="box-header">
          <input type="hidden" id="idreservation" value="<?php echo $master->idreservation ?>">
          <input type="hidden" id="company" value="<?php echo $master->company ?>">
          
          <i class="fa fa-money"></i>
          <h3 class="box-title"><strong>Master Bill </h3>
        </div>
        <div class="box-body">
          <div class="col-xs-12">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li id="tab-Roomcss" class="active"><a href="#tab-Room" data-toggle="tab" aria-expanded="true">Room Bill</a></li>
              <li id="tab-concss" ><a href="#tab-con" data-toggle="tab" aria-expanded="false">Consume</a></li>
              <li id="tab-sumcss" ><a href="#tab-sum" id="tab-sumklik" data-toggle="tab" aria-expanded="false">Summary</a></li>
            </ul>
            <div class="tab-content">
               <div class="tab-pane active" id="tab-Room">
            <div class="row">
               <a style="float:right"  class="btn btn-default Room"> <i class="glyphicon glyphicon-print"></i> Print Room Bill</a>
            </div>
             <div class="row">
                <?php  $Dataroom = $this->M_fo_outlet->get_roombill($master->idreservation);  ?>
                      <div class="table-responsive">
                    <table id="tabelroom" class="table table-bordered table-striped">
                                <thead>
                                    <tr >
                                        <th>NO</th>
                                        <th>Date</th>
                                        <th>Invoice</th>
                                        <th>Description</th>
                                        <th>Charge (IDR)</th>
                                        <th>Payment (IDR)</th>
                                        <th style="display: none;">id trx</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $noroom=1; $totalchargeroom=0; $totalpaymentroom=0; 
                                   // print_r($Dataroom);exit();
                                    foreach($Dataroom as $room){ ?>
                                    <tr>
                                      <td><?php echo $noroom ?></td>
                                      <td><?php echo $room->tgl1 ?></td>
                                      <td><?php echo $room->invoicemanual ?></td>
                                      <td><?php echo $room->desc ?></td>
                                      <td align="right"><?php echo number_format($room->charge, 2, ',', '.') ?></td>
                                      <td align="right"><?php echo number_format($room->payment, 2, ',', '.') ?></td>
                                       <td style="display: none;"><?php echo $room->kode ?></td>
                                    </tr>
                                    <?php $noroom++; $totalchargeroom+=$room->charge; $totalpaymentroom+=$room->payment;} ?>
                                </tbody>
                                <tfoot>
                                  <tr style="font-weight: bold; background-color: #f0f0f0; border: 1px;">
                                    <td colspan="4">SUMMARY</td>
                                    <td  align="right"><?php echo number_format($totalchargeroom, 2, ',', '.') ?></td>
                                    <td  align="right"><?php echo number_format($totalpaymentroom, 2, ',', '.') ?></td>
                                  </tr>
                                  <tr style="font-weight: bold; background-color: #f0f0f0;">
                                    <td colspan="5">BALANCE</td>
                                    <td  align="right"><?php echo number_format($totalpaymentroom-$totalchargeroom, 2, ',', '.') ?></td>
                                  </tr>
                                </tfoot>
                            </table>
                    </div>
                  </div>
          </div>




               <div class="tab-pane" id="tab-con">
             <div class="row">
               <a style="float:right"  class="btn btn-default Consume"> <i class="glyphicon glyphicon-print"></i> Print Consume Bill</a>
            </div>
             <div class="row">
                <?php $Datacomsume = $this->M_fo_outlet->get_comsumebill_dee($master->idreservation); //print_r($this->db->last_query());exit(); ?>
                      <div class="table-responsive">
                    <table id="tabelcom" class="table table-bordered table-striped">
                                <thead>
                                    <tr >
                                        <th>NO</th>
                                        <th>Date</th>
                                        <th>Invoice</th>
                                        <th>Description</th>
                                        <th>Charge (IDR)</th>
                                        <th>Payment (IDR)</th>
                                        <th style="display: none;">id trx</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $nocom=1; $totalchargecom=0; $totalpaymentcom=0; 
                                    foreach($Datacomsume as $com){ ?>
                                    <tr>
                                      <td><?php echo $nocom ?></td>
                                      <td><?php echo $com->tgl1 ?></td>
                                      <td><?php echo $com->trxid ?></td>
                                      <td> <?php echo $com->des ?></td>
                                      <td align="right"><?php echo number_format($com->grandtotal, 2, ',', '.') ?></td>
                                      <td align="right"><?php echo number_format($com->payment, 2, ',', '.') ?></td>
                                      <td style="display: none;"> <?php echo $com->trxid ?></td>
                                    </tr>
                                    <?php $nocom++; $totalchargecom+=$com->grandtotal; $totalpaymentcom+=$com->payment;} ?>
                                </tbody>
                                <tfoot>
                                  <tr style="font-weight: bold; background-color: #f0f0f0;">
                                    <td colspan="4">TOTAL</td>
                                    <td  align="right"><?php echo number_format($totalchargecom, 2, ',', '.') ?></td>
                                    <td  align="right"><?php echo number_format($totalpaymentcom, 2, ',', '.') ?></td>
                                  </tr>
                                  <tr style="font-weight: bold; background-color: #f0f0f0;">
                                    <td colspan="5">BALANCE</td>
                                    <td  align="right"><?php echo number_format($totalpaymentcom-$totalchargecom, 2, ',', '.') ?></td>
                                  </tr>
                                </tfoot>
                            </table>
                    </div>
                  </div>
               </div>
             




               <div class="tab-pane" id="tab-sum">
                <div class="row">
          <a style="float:right"  class="btn btn-default Summary"> <i class="glyphicon glyphicon-print"></i> Print Summary Bill</a>
            </div>
             <div class="row">
                      <div class="table-responsive">
                    <table id="tabelsum" class="table table-bordered table-striped">
                                <thead>
                                    <tr >
                                        <th>NO</th>
                                        <th>Date</th>
                                        <th>Invoice</th>
                                        <th>Description</th>
                                        <th>Charge (IDR)</th>
                                        <th>Payment (IDR)</th>
                                        <th style="display: none;">id trx</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $nosum=1; 
                                    foreach($Dataroom as $room){  ?>
                                    <tr>
                                      <td><?php echo $nosum ?></td>
                                      <td><?php echo $room->tgl1 ?></td>
                                      <td><?php echo $room->invoicemanual ?></td>
                                      <td><?php echo $room->desc ?></td>
                                      <td align="right"><?php echo number_format($room->charge, 2, ',', '.') ?></td>
                                      <td align="right"><?php echo number_format($room->payment, 2, ',', '.') ?></td>
                                      <td style="display: none;"><?php echo $room->kode ?></td>
                                    </tr>
                                    <?php $nosum++; } ?>

                                    <?php foreach($Datacomsume as $com){ ?>
                                    <tr>
                                      <td><?php echo $nosum ?></td>
                                      <td><?php echo $com->tgl1 ?></td>
                                      <td><?php echo $com->trxid ?></td>
                                      <td> <?php echo $com->des ?></td>
                                      <td align="right"><?php echo number_format($com->grandtotal, 2, ',', '.') ?></td>
                                      <td align="right"><?php echo number_format($com->payment, 2, ',', '.') ?></td>
                                      <td style="display: none;"><?php echo $com->trxid ?></td>
                                    </tr>
                                    <?php $nosum++; } ?>
                                </tbody>
                                <tfoot>
                                  <tr style="font-weight: bold; background-color: #f0f0f0; border: 1px;">
                                    <td colspan="4">SUMMARY</td>
                                    <td  align="right"><?php echo number_format($totalchargeroom + $totalchargecom, 2, ',', '.') ?></td>
                                    <td  align="right"><?php echo number_format($totalpaymentroom + $totalpaymentcom, 2, ',', '.') ?></td>
                                  </tr>
                                  <tr style="font-weight: bold; background-color: #f0f0f0;">
                                    <td colspan="5">BALANCE</td>
                <td  align="right"><?php echo number_format( ($totalpaymentroom + $totalpaymentcom)-($totalchargeroom+$totalchargecom), 2, ',', '.') ?></td>
                                  </tr>
                                </tfoot>
                            </table>
                    </div>
                  </div>
               </div>


            </div>
            <!-- /.tab-content -->
          </div>
          <div class="row">
                <div class="form-group">
                      <a  id="Simpan" class="btn btn-warning"><i class='glyphicon glyphicon-ok'></i> Checkout</a>
                    </div>
                 </div>
<input type="hidden" id="balancelast" value="<?php echo ($totalpaymentroom + $totalpaymentcom)-($totalchargeroom+$totalchargecom) ?>">
        
          <!-- /.nav-tabs-custom -->
        </div>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
   </div>

 


</section><!-- /.content -->


<?php
$this->load->view('template/Js');
$this->load->view('template/Foot');
?>

<script>
  $(function () {
    var company=$("#company").val();
      if (company=='Personal Account') {
       $('#tab-Roomcss').hide();
       $('#tab-concss').hide();
      $('#tab-sumklik').trigger('click');
      }
  });
   

</script>

<script type="text/javascript">
$("#Simpan").click(function() {
  var balance  = $("#balancelast").val();
        var dataroom= $("#tabelroom").DataTable();
        var datarooms = dataroom.rows().data().toArray();
        var datacom= $("#tabelcom").DataTable();
        var datacoms = datacom.rows().data().toArray();
        var datasum= $("#tabelsum").DataTable();
        var datasums = datasum.rows().data().toArray();
        var idreservation = $("#idreservation").val();
        event.preventDefault();
        if (parseInt(balance)<0) { invalid_data(); return false;}
       
        $.confirm({
          title: 'Confirmation',
          content: 'Are You Sure to Save?',
          type:'blue',
          buttons: {
              Simpan: function () {
                $.ajax({
                  type: "POST",
                 url: '<?php echo site_url('Fo_outlet/checkoutAddDB');?>',
                  data: {idreservation:idreservation, datarooms:datarooms, datacoms:datacoms, datasums:datasums },
                  success: function(data){
                     window.location = '<?php echo site_url('Reservation/ExpectedDeparture_view');?>';
                                 
                  }
                });     
              },
              Batal: function () {
                
                  $.alert('Data Tidak Disimpan...');
              },
          }
      }); 

});
</script>

<script type="text/javascript">
  $(function () {  
  $(".Summary").click(function(){
      var idreservation = $("#idreservation").val();
      window.open("<?php echo base_url(). 'index.php/Fo_outlet/CetakBill_guest_checkout/';?>?jenisbill="+ '3' +"&idreservation="+ idreservation ,"MyTargetWindowName")
  }); 
});
</script>
<script type="text/javascript">
  $(function () {  
  $(".Consume").click(function(){
      var idreservation = $("#idreservation").val();
      window.open("<?php echo base_url(). 'index.php/Fo_outlet/CetakBill_guest_checkout/';?>?jenisbill="+ '2' +"&idreservation="+ idreservation ,"MyTargetWindowName")
  }); 
});
</script>
<script type="text/javascript">
  $(function () {  
  $(".Room").click(function(){
      var idreservation = $("#idreservation").val();
      window.open("<?php echo base_url(). 'index.php/Fo_outlet/CetakBill_guest_checkout/';?>?jenisbill="+ '1' +"&idreservation="+ idreservation ,"MyTargetWindowName")
  }); 
});
</script>


<script>
  $(function () {
    $("#example1").DataTable();
    $('#tabelroom').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
    $('#tabelcom').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
    $('#tabelsum').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>

   <script type="text/javascript">
  function invalid_data() {
  $.alert({
    title: 'Caution!!',
    content: 'Transaction Not Balance!',
    icon: 'fa fa-warning',
    type: 'orange',
});
}
</script>


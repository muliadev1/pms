
<?php 

class M_stok_card extends CI_Model{	



 function get_stok_awal_byitem($tgl1,$tgl2,$itemcode){

        $query = $this->db->query("  SELECT SUM(Ins) as masuk,SUM(Outs) as keluar FROM (
                                        SELECT `datereceive` AS tgl ,`itemcode` AS codeitem,`qty` AS qty,`description`,'RO' AS keterangan,'IN' AS STATUS,'Store' AS departement
                                        ,qty AS INs , 0 AS Outs 
                                        FROM `tbrodetil` INNER JOIN `tbro`ON `tbrodetil`.`kodero` = `tbro`.`kodero`
                                        UNION
                                        SELECT `dateadd` AS tgl ,codeitem AS codeitem,qtyorder AS qty,tbsrdetil.description, 'SR' AS keterangan ,'OUT' AS STATUS,tbsrdetil.`departement`
                                        ,0 AS INs , qtyorder AS OUTs 
                                        FROM `tbsrdetil` INNER JOIN `tbsr` ON tbsrdetil.`kodesr` = tbsr.`kodesr` WHERE tbsrdetil.isSO=0
                                      AND tbsrdetil.iddepartement<>0 
                                        UNION
                                        SELECT `dateadd` AS tgl ,codeitem AS codeitem,qtyorder AS qty,tbsrdetil.description, 'SO OUT' AS keterangan ,'OUT' AS STATUS,tbsrdetil.`departement`
                                        ,0 AS INs , qtyorder AS OUTs
                                        FROM `tbsrdetil` INNER JOIN `tbsr` ON tbsrdetil.`kodesr` = tbsr.`kodesr` WHERE tbsrdetil.isSO=1 AND tbsrdetil.statusInOut=1
                                        UNION
                                        SELECT `dateadd` AS tgl ,codeitem AS codeitem,qtyorder AS qty,tbsrdetil.description, 'SO IN' AS keterangan ,'IN' AS STATUS,tbsrdetil.`departement`
                                        ,qtyorder AS INs , 0 AS OUTs  
                                        FROM `tbsrdetil` INNER JOIN `tbsr` ON tbsrdetil.`kodesr` = tbsr.`kodesr` WHERE tbsrdetil.isSO=1 AND tbsrdetil.statusInOut=0
                                        UNION
                                        SELECT `dateadd` AS tgl ,codeitem AS codeitem,qty AS qty,tbbadstokdetil.description, 'BS' AS keterangan ,'OUT' AS STATUS,tbbadstokdetil.`departement`
                                        ,0 AS INs ,qty AS OUTs  
                                        FROM `tbbadstokdetil` INNER JOIN `tbbadstok` ON tbbadstokdetil.`kodebs` = tbbadstok.`kodebs` 
                                      ) AS a WHERE DATE(tgl) BETWEEN '$tgl1' AND '$tgl2' and codeitem='$itemcode'
                                    "); 
         $perjalananstok =$query->row();
          $query1 = $this->db->query("SELECT * FROM tbitem where codeitem='$itemcode'");
          $stokditb = $query1->row();
         $stokawal =$stokditb->stok - $perjalananstok->masuk + $perjalananstok->keluar ;
         return $stokawal;
  }
  function Load_SC_bytgl($tgl1,$tgl2,$itemcode) { 
        $query = $this->db->query("SELECT nourut, tgl,codeitem,qty,description,keterangan,departement,price, STATUS, DATE_FORMAT(tgl,'%d-%m-%Y') AS tgl1 FROM (
                                      SELECT `datereceive` AS tgl ,`itemcode` AS codeitem,`qty` AS qty,`description`,'RO' AS keterangan,'IN' AS STATUS,'Store' AS departement, price as price ,'1' nourut
                                      FROM `tbrodetil` INNER JOIN `tbro`ON `tbrodetil`.`kodero` = `tbro`.`kodero`
                                      UNION
                                      SELECT `dateadd` AS tgl ,codeitem AS codeitem,qtyorder AS qty,tbsrdetil.description, 'SR' AS keterangan ,'OUT' AS STATUS,tbsrdetil.`departement`, price as price, '2' nourut
                                      FROM `tbsrdetil` INNER JOIN `tbsr` ON tbsrdetil.`kodesr` = tbsr.`kodesr` WHERE tbsrdetil.isSO=0 
                                      AND tbsrdetil.iddepartement<>0 
                                      UNION
                                      SELECT `dateadd` AS tgl ,codeitem AS codeitem,qtyorder AS qty,tbsrdetil.description, 'SO OUT' AS keterangan ,'OUT' AS STATUS,tbsrdetil.`departement`,price as price,'3' nourut
                                      FROM `tbsrdetil` INNER JOIN `tbsr` ON tbsrdetil.`kodesr` = tbsr.`kodesr` WHERE tbsrdetil.isSO=1 AND tbsrdetil.statusInOut=1
                                      UNION
                                      SELECT `dateadd` AS tgl ,codeitem AS codeitem,qtyorder AS qty,tbsrdetil.description, 'SO IN' AS keterangan ,'IN' AS STATUS,tbsrdetil.`departement`,price as price,'4' nourut
                                      FROM `tbsrdetil` INNER JOIN `tbsr` ON tbsrdetil.`kodesr` = tbsr.`kodesr` WHERE tbsrdetil.isSO=1 AND tbsrdetil.statusInOut=0
                                      UNION
                                      SELECT `dateadd` AS tgl ,codeitem AS codeitem,qty AS qty,tbbadstokdetil.description, 'BS' AS keterangan ,'OUT' AS STATUS,tbbadstokdetil.`departement`,price as price,'5' nourut
                                      FROM `tbbadstokdetil` INNER JOIN `tbbadstok` ON tbbadstokdetil.`kodebs` = tbbadstok.`kodebs` 
                                  ) AS a WHERE DATE(tgl) BETWEEN '$tgl1' AND '$tgl2' and codeitem='$itemcode'
                                  ORDER BY tgl1 ASC, nourut ASC"); 
         return $query->result() ;
  } 

  function item_view() {
   $query = $this->db->query("SELECT tbitem.*, tbsubcategory.`description` AS descsubcategory FROM tbitem INNER JOIN `tbsubcategory`
                                ON tbitem.`idsubcategory` =  tbsubcategory.`idsubcategory`  
                                 "); 
    return $query->result() ;     
            
   }

     



  

  


  


      



}
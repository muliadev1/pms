<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>

<!-- Content Header (Page header) -->
<section class="content-header">
   <div class="btn-group btn-breadcrumb">
            <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
             <a href="<?php echo base_url('index.php/Guest/Guest_view');?>" class="btn btn-default  btn-xs">Guest</a>
            <a  class="btn btn-default  btn-xs active">Add Guest</a>
        </div>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
   <div class="col-md-12">
    <div class="box box-info">
     <div class="box-header">
      <h3 class="box-title">Add New Item Guest</h3>
     </div>

     <div class="box-body">
      <form method="post" id="Simpan"  action="<?php echo base_url(). 'index.php/Guest/Guest_addDB'; ?>">
       <div class="form-group">
        <label>Saluation</label>
        <select id="saluation" class="form-control"  style="width: 100%;" name="saluation">
         <option value="Mr." >Mr.</option>
         <option value="Mrs." >Mrs.</option>
         <option value="Ms." >Ms.</option>
         <option value="Miss." >Miss.</option>
        </select>
       </div>

       <div class="form-group">
        <label class="control-label">Firstname</label>
        <input type="text" name="firstname" id="firstname"  data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out category description..."  class="form-control"  placeholder="Firstname">
       </div>

       <div class="form-group">
        <label class="control-label">Lastname</label>
        <input type="text" name="lastname" id="lastname"  data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out category description..."  class="form-control"  placeholder="Lastname">
       </div>

       <div class="form-group">
        <div class="row">
         <div class="col-md-1">
          <label class="control-label">Gender</label>
         </div>
        </div>
       <div class="row">
         <div class="col-md-1">
          <input type="radio" id="gender" name="gender" class="flat-red" value="Male" checked> Male
         </div>
         <div class="col-md-2">
          <input type="radio" id="gender" name="gender" class="flat-red" value="Female"> Female
         </div>
        </div>
       </div>

       <div class="form-group">
        <label>Birthday</label>
       <div class="input-group date">
        <div class="input-group-addon">
         <i class="fa fa-calendar"></i>
        </div>
         <input type="text" value="<?= date('m/d/Y') ?>"class="form-control pull-right" name= "birthday" id="birthday">
       </div>
      </div>

      <div class="form-group">
       <label>Type ID</label>
       <select id="idtype" class="form-control select2"  style="width: 100%;" name="idtype">
        <option value="Passport"  >Passport</option>
        <option value="ID Card"  >ID Card</option>
        <option value="Driving Licensed"  >Driving Licensed</option>
       </select>
      </div>

      <div class="form-group">
       <label class="control-label">ID Number</label>
       <input type="text" name="idnumber" id="idnumber"  data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out category description..."  class="form-control"  placeholder="ID Number">
      </div>

      <div class="form-group">
       <label>Description</label>
       <textarea type="text" name="description" id="description"  class="form-control"></textarea>
      </div>

      <div class="form-group">
       <label>Country</label>
       <select id="state" name="state" class="form-control select2"  style="width: 100%;">
         <?php
          foreach($country as $u){
            //print_r($country);exit();
              ?>
            <option value="<?php echo $u; ?>"><?php echo $u; ?></option>
              <?php } ?>
       </select>
      </div>

       <div class="form-group">
        <label class="control-label">Phone</label>
        <input type="text" name="phone" id="phone"  data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out category description..."  class="form-control"  placeholder="Phone">
       </div>

       <div class="form-group">
        <label class="control-label">E-Mail</label>
        <input type="text" name="email" id="email"  data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out category description..."  class="form-control"  placeholder="Email">
       </div>

       <div class="form-group">
        <label class="control-label">Zip Code</label>
        <input type="text" name="zipcode" id="zipcode"  data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out category description..."  class="form-control"  placeholder="Zip Code">
       </div>

       <div class="form-group">
        <label>Address</label>
        <textarea type="text" name="address" id="address"  class="form-control"></textarea>
       </div>

          <div class="form-group">
            <button type="submit" value="Validate" class="btn btn-default"><i class='glyphicon glyphicon-ok'></i> Save</button>
          </div>
        </form>
       </div>
      </div>
     </div>
    </div>
</section>

<script type="text/javascript">
$("#Simpan").submit(function() {
    var saluation = $('#saluation').val();
    var firstname = $('#firstname').val();
    var lastname = $('#lastname').val();
    var gender = $('#gender').val();
    var birthday = $('#birthday').val();
    var idtype  = $('#idtype').val();
    var idnumber = $('#idnumber').val();
    var description = $('#description').val();
    var country = $('#state').val();
    var phone = $('#phone').val();
    var email = $('#email').val();
    var zipcode = $('#zipcode').val();
    var address = $('#address').val();
        if (saluation == ''|| firstname=='' || lastname=='' || gender=='' || birthday=='' || idtype==''
             || idnumber==''|| description==''|| country==''|| phone==''|| email==''|| zipcode==''|| address==''){
            File_Kosong(); return false;
        }else{
        event.preventDefault();
        $.confirm({
          title: 'Confirmation',
          content: 'Are You Sure to Save?',
           type: 'blue',
          buttons: {
              Save: function () {
                  $.LoadingOverlay("show");
                  $("#Simpan").submit();
              },
              Cancel: function () {

                  $.alert('Data Not Saved...');
              },
          }
      });
    }

});
</script>

<?php
$this->load->view('template/Js');
$this->load->view('template/Foot');
?>



<script type="text/javascript">
  function File_Kosong() {
  $.alert({
    title: 'Caution!!',
    content: 'Add Data Invalid!',
    icon: 'fa fa-warning',
    type: 'orange',
});
}
</script>

<script>
$(function () {
  $(".select2").select2();
   $(".select3").select2();

});
</script>

<script>
$( function() {
$( "#birthday" ).datepicker({
autoclose: true,
dateFormat: 'yy/mm/dd'
});
});
</script>
<script type="text/javascript">
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });
</script>


<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');

?>
<style type="text/css">
  .box-widget {
 box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
}
.modal.modal-wide .modal-dialog {
  width: 90%;
}

.modal-wide .modal-body {
  overflow-y: auto;
}

#tallModal .modal-body p { margin-bottom: 900px }
.pull-right {
    float: right;
}

.pull-left {
    float: left;
}
</style>

<!-- Content Header (Page header) -->
<section class="content-header" align="center" style="margin-bottom: 24px; ">
   
    <h3> 
        Resto Report
    </h3>
    
</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-xs-12">
    <?php $this->load->view('template/msg_sukses'); ?>


  <div class="col-xs-12">
      <div class="box box-success box-widget">
        <div class="box-header" >
          <h6 class="box-title" style="font-size: 15px;" >SUMMARY OF SALES</h6>
        </div>
        <div class="box-body">
    <div class="row">
      <div class="col-sm-4">
         <div class="form-group">
            <label>Operator</label>
              <select id="Operator" class="form-control select2"  style="width: 100%;" name="Operator">  
                    <option value="-" selected>--Select Operator--</option>            
                     <?php
                          foreach($operator as $op){
                      ?>
                        <option value="<?php echo $op->id_user; ?>" ><?php echo $op->nama_user; ?></option>
                        <?php } ?>
                    </select>
          </div>
        </div>
        <div class="col-sm-4">
         <div class="form-group"  id="groupClos">
            <label>Date Clossing</label>
              <select id="DateClosing" class="form-control select2"  style="width: 100%;" >  
                    <option value="-" selected>--Select Date--</option>            
                     
                        <option value="<?php echo $op->id_user; ?>" ><?php echo $op->nama_user; ?></option>
                    </select>
          </div>
        </div>

     
        <div class="col-sm-2">
          <div class="form-group">
             <a style="margin-top: 24px; " class="btn btn-default SOL"><i class='glyphicon glyphicon-print'></i> Cetak</a>
          </div>
        </div> 
      </div>

    </div> 
   </div>
  </div>





        
</section><!-- /.content -->
      
       <div class="modal modal-wide fade" id="ModalCS" role="dialog">
          <div class="modal-dialog modal-sm">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 align="center" class="modal-title">Item List</h4>
                  </div>
                  <div class="modal-body">
                      
                  </div>
                  
              </div>
          </div>
      </div>








<?php
$this->load->view('template/Js');
$this->load->view('template/Foot');
?>
<script type="text/javascript">
   $(document).on('change', '#Operator', function(){
    var idop = $("#Operator").val();
       $.ajax({
            type: "POST",
            dataType:'html',
           url: '<?php echo site_url("Resto/Select_clossing_byop");?>',
            data: {idop: idop},
            success: function(data){
              // $('#Subcategory').remove();
                 $('#groupClos').html(data);  
                 $(".select2").select2();
            }
          });

    });
</script>



<script type="text/javascript">
  $(function () {  
  $(".SOL").click(function(){
      var idmaster = $( "#DateClosing" ).val();   
          window.open("<?php echo base_url(). 'index.php/Resto/Cetak_closing/';?>?idmaster="+idmaster ,"MyTargetWindowName")
  }); 
});
</script>

 <script>
  $(function () {
    $(".select2").select2();
     $(".select3").select2();

  });
  </script>



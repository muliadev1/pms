
<?php// echo $header?>

<style type="text/css">
  .TabelKonten tr td {
    padding-right: 7px;
    padding-left:  7px;
    font-size: 12px;
  }
</style>
<table class="TabelKonten"  border="0" style="border-collapse: collapse; border-color:#000000; padding-bottom : 50px ;"  width="100%" >
<tr>
  <td colspan="2" align="center" style="font-size: 14px;"> <strong> FO CLOSSING REPORT</strong></td>
</tr>
<tr>
  <td width="10%"> <strong>Date Clossing</strong></td>
  <td>:<?php echo $dataclosing[0]->trdate ?></td>
</tr>
<tr>
  <td width="10%"> <strong>Operator</strong></td>
  <td>:<?php echo $dataclosing[0]->nama_user ?></td>
</tr>
</table>
<br>

<table class="TabelKonten"  border="1" style="border-collapse: collapse; border-color:#000000; padding-top : 50px;"  width="100%" >         
                                <thead>
                                     <tr >
                                        <th >NO</th>
                                        <th >Invoice No</th>
                                        <th >Manual Invoice No</th>
                                        <th >Room</th>
                                        <th >Date</th>                         
                                        <th >Article</th>
                                        <th >Room Type</th>
                                        <th >Amount</th>
                                        <th >User</th>
                                </thead>
                                <tbody>
                                  <?php $no=1;$totalamount=0;   foreach ($dataclosing as $clo){  ?>
                                    <tr>
                                      <td><?php echo $no ?></td>
                                      <td><?php echo $clo->kodemaster ?></td>
                                      <td><?php echo $clo->invoicemanual ?></td>
                                      <td><?php echo $clo->roomname ?></td>
                                      <td><?php echo $clo->trdate ?></td>
                                      <td><?php echo $clo->articlename ?></td>
                                      <td><?php echo $clo->articlecategory ?></td>
                                      <td align="right"><?php echo number_format($clo->charge, 2, ',', '.') ?></td>
                                      <td><?php echo $clo->nama_user ?></td>
                                    </tr>
                                  
                                    
                                  <?php $no++;$totalamount+=$clo->charge;} ?>
                                    
                                </tbody>
                                <tfoot>
                                  <tr style="background-color: #f0f0f0; font-weight: bold;">
                                    <td colspan="7"><strong>TOTAL</strong></td>
                                     <td align="right"><strong><?php echo number_format($totalamount, 2, ',', '.') ?></strong></td>
                                      <td>&nbsp;</td>
                                  </tr>
                                </tfoot>
                            </table>


<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/jQuery/jquery-2.2.3.min.js') ?>"></script> 
<script type="text/javascript">
  $(document).ready(function(){
    window.print();
    window.close();
  });
</script>
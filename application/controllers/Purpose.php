<?php

class Purpose extends CI_Controller{

	function __construct(){
		parent::__construct();
		if($this->session->userdata('Kode_userPms')==NULL) {
           redirect('Login');
		}
		$this->load->library('session');
		$this->load->helper(array('form', 'url'));
		$this->load->model(array('M_purpose'));
		date_default_timezone_set('Asia/Hong_Kong');
		
		if (!empty($this->session->userdata("active_db"))) {
			$this->load->database($this->session->userdata("active_db"), FALSE, TRUE);
		}
	}

	function index(){
		redirect('Purpose/Purpose_view');
	}

	function Purpose_view(){
		$data['data'] = $this->M_purpose->Purpose_view();
		$this->load->view('purpose/Purpose',$data);
	}

	function Purpose_add(){
		$this->load->view('purpose/Purpose_add');
	}

	function Purpose_addDB(){
		$data = array(
			'purpose' => $this->input->post('purpose'),
			'isaktif' => '1'
		);
		$this->M_purpose->Purpose_addDB('tbpurpose',$data) ;
		$this->session->set_flashdata('msg', 'Add Purpose successfully ...');
		redirect('Purpose/Purpose_view');
	}

	function Purpose_edit($id){
		$data['data'] = $this->M_purpose->Purpose_view_edit($id) ;
		$this->load->view('purpose/Purpose_edit',$data);
	}

	function Purpose_editDB(){
		$id = $this->input->post('Id');
		$data = array(
			'purpose' => $this->input->post('purpose'),
			'isaktif' => '1'
	);
		$this->M_purpose->Purpose_editDB('tbpurpose',$data,$id) ;
		$this->session->set_flashdata('msg', 'Edit Purpose successfully...');
		redirect('Purpose/Purpose_view');
	}


}
?>

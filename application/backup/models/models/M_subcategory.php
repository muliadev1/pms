<?php

class M_subcategory extends CI_Model{

	function Subcategory_view() {
		$data = $this->db->query("Select idsubcategory,tbcategory.category
																		 ,subcategory,tbsubcategory.description
															from tbsubcategory INNER JOIN tbcategory
															 ON tbsubcategory.idcategory=tbcategory.idcategory
															where tbsubcategory.isaktif=1");
		return $data->result();

   }

	 function Subcategory_addDB($table,$data) {
			 $this->db->insert($table,$data);
			// print_r($this->db->last_query());
	}

	function Subcategory_view_edit($id) {
		$data = $this->db->query("Select idsubcategory,tbcategory.category
 																		,subcategory,tbsubcategory.description,tbsubcategory.idcategory
 														 from tbsubcategory INNER JOIN tbcategory
 															ON tbsubcategory.idcategory=tbcategory.idcategory
 														 where tbsubcategory.isaktif=1 and idsubcategory = $id");
	 return $data->result();
	}

	function SubCategory_editDB($table,$data,$id) {
 			 $this->db->where('idsubcategory',$id );
 			 $this->db->update($table,$data);
  }

}
?>

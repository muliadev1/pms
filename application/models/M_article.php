<?php

class M_article extends CI_Model{

	function Article_sales_view() {
		$data = $this->db->query("SELECT `tbarticlesales`.*, tbarticlecategory.`articlecategory` FROM tbarticlesales 
INNER JOIN tbarticlecategory ON tbarticlesales.`idarticlecategory` = tbarticlecategory.`id`
WHERE tbarticlesales.`isaktif` = 1");
		return $data->result();
   }
	function Article_payment_view() {
		$data = $this->db->query("SELECT * from tbarticlepayment where status = 1");
		return $data->result();
   }
	function get_departement() {
		$data = $this->db->query("SELECT * FROM tbdepartement WHERE IsAktif = 1");
		return $data->result();
   }
	function get_category() {
		$data = $this->db->query("SELECT * FROM `tbarticlecategory` WHERE status = 1");
		return $data->result();
   }

	 function Article_addDB($table,$data) {
			 $this->db->insert($table,$data);
	}

	function Article_sales_edit($id) {
	 $data = $this->db->query("SELECT tbarticlesales.*, `tbarticlecategory`.`articlecategory`, tbdepartement.`departement` FROM `tbarticlesales`
INNER JOIN tbarticlecategory ON tbarticlesales.`idarticlecategory` = tbarticlecategory.`id`
INNER JOIN tbdepartement ON tbarticlesales.`iddepartement` = tbdepartement.`iddepartement`
WHERE tbarticlesales.`idarticle` = '".$id."'");
	 return $data->row();
	}
	function Article_payment_edit($id) {
	 $data = $this->db->query("SELECT * from tbarticlepayment where id = '".$id."'");
	 return $data->row();
	}

	function Article_editDB($table,$data,$id) {
 			 $this->db->where('idarticle',$id );
 			 $this->db->update($table,$data);

  	}
	function Articlepayment_editDB($table,$data,$id) {
 			 $this->db->where('id',$id );
 			 $this->db->update($table,$data);

  	}
	function getkode() { 
        $q = $this->db->query("SELECT MAX(RIGHT(`idarticle`,3)) AS idmax FROM `tbarticlesales`");
        $kd = ""; //kode awal
        if($q->num_rows()>0){ //jika data ada
            foreach($q->result() as $k){
                $tmp = ((int)$k->idmax)+1; //string kode diset ke integer dan ditambahkan 1 dari kode terakhir
                $kd = "0000".$tmp; //kode ambil 4 karakter terakhir
                $kd = substr($kd, -3);
            }
        }else{ //jika data kosong diset ke kode awal
            $kd = "CHG001";
        }
        //gabungkan string dengan kode yang telah dibuat tadi
        return "CHG".$kd;
  	} 
}
?>

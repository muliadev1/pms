<?php 

class Bad_stok extends CI_Controller{
	private $IdOperator;

	function __construct(){
		parent::__construct();
		if($this->session->userdata('Kode_userPms')==NULL) {
           redirect('Login');
		}
		$this->load->library(array('session','create_pdf','ciqrcode'));
		$this->load->model(array('M_purchase_requisition','M_bad_stok','M_rbac'));
		$this->IdOperator = $this->session->userdata('Kode_userPms');
		
		if (!empty($this->session->userdata("active_db"))) {
			$this->load->database($this->session->userdata("active_db"), FALSE, TRUE);
		}
	}


	function index(){
		$this->load->view('dashboard1');
	}

	function bad_stok_add(){
		$data['data'] = $this->M_bad_stok->item_view();
		$data['dataDepaetemen'] = $this->M_bad_stok->load_departement();
		$this->load->view('bad_stok/Bad_stok',$data);
	}

	function daftar_item(){
		$data['data'] = $this->M_bad_stok->item_view();
		$this->load->view('bad_stok/Daftar_item',$data);
	}

	function add_bad_stock(){

		$itemcode = $this->input->post('itemcode');
		//print_r($itemcode);exit();
		$Qty = $this->input->post('Qty');
		$Description = $this->input->post('Description');
		$Note = $this->input->post('Note');
		$Unit = $this->input->post('Unit');
		$IdDepartement = $this->input->post('IdDepartement'); 
		$NamaDepartemen = $this->input->post('NamaDepartemen');
		$Price = $this->input->post('Price');
		$Stok = $this->input->post('Stok');
		$kode =$this->M_bad_stok->get_kodebs('tbbadstok');
		$NotePr = $this->input->post('NotePr');

		$itemcode1 = array_combine(range(1, count($itemcode)), array_values($itemcode));
		$Price1 = array_combine(range(1, count($Price)), array_values($Price));
		$Qty1 = array_combine(range(1, count($Qty)), array_values($Qty));
		$Description1 = array_combine(range(1, count($Description)), array_values($Description));
		$Unit1 = array_combine(range(1, count($Unit)), array_values($Unit));
		$Stok1 = array_combine(range(1, count($Stok)), array_values($Stok));
		$Note1 = array_combine(range(1, count($Note)), array_values($Note));

			$data = array('kodebs' => $kode,
	 				 		'iddepartement' => $IdDepartement,
	 				 		'departement' => $NamaDepartemen,
	 						'dateadd' => date('Y-m-d H:i:s'),
	 						'idoperator' => $this->IdOperator,
	 						'description' => $NotePr	
	 		 			);

				$Jumlah = count($Stok);
				for ($i=1; $i <= $Jumlah ; $i++) { 
				 	 $dataDetil[] = array('kodebs' => $kode,
						 				 'iddepartement' => $IdDepartement,
						 				 'departement' => $NamaDepartemen,
						 				'codeitem' => $itemcode1[$i],
						 				'price' => $Price1[$i],
						 				'qty' => $Qty1[$i],
						 				'description' => $Description1[$i],
						 				'unit' => $Unit1[$i],
						 				'note' => $Note[$i],
						 				'currstok' => $Stok1[$i],
						 				'idoperator' => $this->IdOperator
						 		 );
				 } 
		 
		
		$this->M_bad_stok->add_bad_stok($IdDepartement,$data,$dataDetil);
		$this->session->set_flashdata('msg', 'Store Requisition Add successfully... ');
		$this->session->set_flashdata('kodebs', $kode);
		redirect('Bad_stok/bad_stok_add');
	}


	function daftar_bs(){
		$data['data'] = $this->M_bad_stok->Load_BS_all();
		$this->load->view('bad_stok/Cetak/Daftar_bs',$data);
	}

	function Cetak($JenisLaporan,$Text){
		$data['hotel'] = $this->M_rbac->getPerusahaan();
		$data['qr'] = $this->ciqrcode->generateQRcode($JenisLaporan, $Text,1.9);
		$data1= $this->load->view('template/Cetak_head',$data, TRUE);
		return $data1;
	}

	function CetakBS(){
		$KodeBS = $this->input->get('nobs', TRUE);
		$data['kontenBs'] = $this->M_bad_stok->Load_BS_bycode($KodeBS);
	    $data['header'] = $this->Cetak("Payment_voucher",  $this->session->userdata('Nama_userPms').'-'.$data['kontenBs'][0]->kodebs.'-'. date("d-m-Y_H-i-s"), 1);
	    $html=$this->load->view('bad_stok/Cetak/BS',$data, TRUE);
     	$this->create_pdf->load($html,'bad_stok'.'-'.$data['kontenBs'][0]->kodesr, 'A4-P');	
	}
	


}
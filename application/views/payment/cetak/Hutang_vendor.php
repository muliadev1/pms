<?php echo $header?>

<style type="text/css">
  #TabelKonten tr td {
    padding-right: 7px;
    padding-left:  7px;
    font-size: 12px;
  }
</style>
<div style="margin-buttom : 15px;" >
<table width="100%" border="0"  >
    <tr>
     <td colspan="2" align="center" style="font-size:14px;"> <strong> DATA HUTANG  </strong>  </td>
    </tr>
    
   
  </table>
<table width="100%" border="0" style="font-size:11px;"  >
   <tr>
     <td>&nbsp;</td>
     <td align="left">&nbsp;</td>
      <td>&nbsp;</td>
     <td align="left">&nbsp;</td>
    </tr>
  <tr>
     <td width="10%"><strong>Vendor</strong> </td>
     <?php if ($idvendor==0){ ?>
      <td align="left">: All Vendor</td>
     <?php }else{ ?>
      <td align="left">: <?php echo $konten[0]->vendor;?></td>
     <?php } ?>

     
     <td width="10%" >&nbsp;</td>
     <td align="left">&nbsp;</td>
    </tr>
    <?php if ($periode['format']==1) { ?>
    <tr>
     <td width="10%"><strong>Tanggal</strong> </td>
     <td align="left">: <?php echo $periode['TglAwal'];?> <strong> S/d </strong> <?php echo $periode['TglAkhir'];?></td>
      <td width="10%">&nbsp;</td>
     <td align="left">&nbsp; </td>
    </tr>
    <?php }else{?>
         <tr>
         <td width="10%"><strong>Per</strong> </td>
         <td align="left">: <?php echo $periode['TglPer'];?></td>
          <td width="10%">&nbsp;</td>
         <td align="left">&nbsp; </td>
        </tr>
      <?php }?>
  
    <tr>
     <td>&nbsp;</td>
     <td align="left">&nbsp;</td>
      <td>&nbsp;</td>
     <td align="left">&nbsp;</td>
    </tr>
  </table>
</div>

    <table id="TabelKonten"  border="1" style="border-collapse: collapse; border-color:#000000; margin-bottom : 130px;"  width="100%"   >
                                <thead>
                                    <tr >
                                        <th >Tanggal</th>
                                          <?php if ($idvendor==0){ ?>
                                            <th>Vendor</th>
                                          <?php } ?>
                                        <th>NO MI</th>
                                        <th width="10%"> Jatuh Tempo</th>
                                         <th>Jumlah</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i=1; $TotalHutang = 0;
                                    foreach($konten as $u){
                                    ?>
                                    <tr  class='odd gradeX context'>
                                        <td class="date"><?php echo $u->datereceive ?></td>
                                         <?php if ($idvendor==0){ ?>
                                            <td class="vendor"><?php echo $u->vendor?></td>
                                          <?php } ?>
                                        <td class="code"><?php echo $u->kodero?></td>
                                        <td class="desc"><?php echo $u->duedate?></td>
                                        <td class="total" align="right"><?php echo number_format($u->total, 0, '.', '.')?></td> 


                                    </tr>
                                    <?php $i++; $TotalHutang += $u->total; } ?>
                                </tbody>
                                <tfoot>
                                  <tr style="background-color: #f0f0f0;">
                                     <?php if ($idvendor==0){ ?>
                                      <td colspan="4"><strong>TOTAL HUTANG</strong></td>
                                     <?php }else{ ?>
                                      <td colspan="3"><strong>TOTAL HUTANG</strong></td>
                                     <?php } ?>
                                     <td align="right"><strong><?php echo number_format($TotalHutang, 0, '.', '.')?></strong></td>
                                  </tr>
                                </tfoot>
                            </table>
          


<script type="text/javascript">
  $(document).ready(function(){
     $(".ViewDetail").click(function(e){ 
     //var nomi = $('#IdDepartement').val();  
      var nomi = $(this).closest('tr').children('td.code').text(); 
       var total = $(this).closest('tr').children('td.total').text();     
      $.ajax({
        type: "POST",
       url: '<?php echo site_url('Payment/viwedetilmi');?>',
        data: {nomi:nomi  },
        success: function(data){
            e.preventDefault();
            var mymodal1 = $('#myModalNew');
            mymodal1.modal({backdrop: 'static', keyboard: false}) 
            var height = $(window).height() - 200;
            mymodal1.find(".modal-body").css("max-height", height);
             $("#NO span").text(nomi);
              $("#Total span").text(total);
            mymodal1.find('.modal-body').html(data);
            mymodal1.modal('show');            
        }
      });
    });
  });
</script>

<script type="text/javascript">
  $(document).ready(function(){
     $(".Bayar").click(function(e){ 
       var kodero = $(this).closest('tr').children('td.code').text(); 

      $.confirm({
          title: 'Confirmation',
          content: 'Are You Sure to Save?',
          buttons: {
              Simpan: function () {
                  $.ajax({
                    type: "POST",
                   url: '<?php echo site_url('Payment/acc_payment');?>',
                    data: {kodero:kodero  },
                    success: function(data){
                       $("#Proses").trigger('click');
                       Berhasil_bayar()          
                    }
                  });
              },
              Batal: function () {

                  $.alert('Data Tidak Disimpan...');
              },
          }
      }); 
    });
  });
</script>


<script type="text/javascript">
  function File_Kosong() {
  $.alert({
    title: 'Caution!!',
    content: 'Transaction Is Invalid!',
    icon: 'fa fa-warning',
    type: 'orange',
});
}
</script>

<script type="text/javascript">
  function Berhasil_bayar() {
  $.alert({
    title: 'Success!!',
    content: 'Data Berhasil Diproses...',
    icon: 'fa fa-check-circle',
    type: 'green',
});
}
</script>

<script>
  $(function () {
    
    $('#example1').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
  });
</script>

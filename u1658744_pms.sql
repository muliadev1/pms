-- phpMyAdmin SQL Dump
-- version 4.9.11
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 29, 2023 at 03:25 PM
-- Server version: 10.5.19-MariaDB-cll-lve
-- PHP Version: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u1658744_pms`
--

-- --------------------------------------------------------

--
-- Table structure for table `rbac_menu`
--

CREATE TABLE `rbac_menu` (
  `id_menu` int(10) NOT NULL,
  `nama` text DEFAULT NULL,
  `text` text DEFAULT NULL,
  `icon` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `rbac_menu`
--

INSERT INTO `rbac_menu` (`id_menu`, `nama`, `text`, `icon`) VALUES
(1, 'Master Data', 'Untuk Admin', 'fa fa-th'),
(2, 'Purchase Requisition', 'Purchase Requisition', 'fa fa-list-alt'),
(3, 'Purchase Order', 'Purchase Order', 'fa fa-send'),
(4, 'Stok Opname', 'SO', 'fa fa-edit'),
(5, 'Outlet', 'resto, dll', 'fa fa-home'),
(6, 'Receive Item', 'RO', 'fa fa-download'),
(7, 'Menu', 'Resto Menu', 'fa fa-list-alt'),
(8, 'Report', 'laporan inventori', 'fa fa-book'),
(9, 'Stock', 'Store mens', 'fa fa-cube'),
(10, 'Master Data', 'Untuk FO', 'fa fa-th'),
(11, 'Account Payable', 'Untuk BO', 'fa fa-money'),
(12, 'Front Office', 'Fo', 'fa fa-home'),
(13, 'Aset', 'Aset', 'fa fa-home'),
(14, 'Accounting Report', NULL, 'fa fa-book'),
(15, 'Hutang', NULL, 'fa fa-list-alt'),
(16, 'Verifikator', NULL, 'fa fa-key'),
(17, 'Data Akun', NULL, 'fa fa-list-alt');

-- --------------------------------------------------------

--
-- Table structure for table `rbac_menu_role`
--

CREATE TABLE `rbac_menu_role` (
  `id_role` int(10) DEFAULT NULL,
  `id_menu` int(10) DEFAULT NULL,
  `sort` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `rbac_menu_role`
--

INSERT INTO `rbac_menu_role` (`id_role`, `id_menu`, `sort`) VALUES
(2, 1, 1),
(2, 2, 2),
(2, 3, 3),
(2, 4, 6),
(2, 6, 4),
(2, 9, 5),
(2, 8, 7),
(3, 5, 2),
(3, 7, 1),
(4, 10, 1),
(5, 11, 1),
(4, 12, 1),
(6, 13, 1),
(5, 14, 2),
(5, 15, 3),
(7, 16, 1),
(5, 17, 3);

-- --------------------------------------------------------

--
-- Table structure for table `rbac_role`
--

CREATE TABLE `rbac_role` (
  `id_role` int(10) NOT NULL,
  `nama_role` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `rbac_role`
--

INSERT INTO `rbac_role` (`id_role`, `nama_role`) VALUES
(2, 'Backoffice'),
(3, 'Resto'),
(4, 'Front Office'),
(5, 'Accounting');

-- --------------------------------------------------------

--
-- Table structure for table `rbac_submenu`
--

CREATE TABLE `rbac_submenu` (
  `id_submenu` int(10) NOT NULL,
  `nama_submenu` text DEFAULT NULL,
  `link` text DEFAULT NULL,
  `parent_menu` int(11) DEFAULT NULL,
  `icon` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `rbac_submenu`
--

INSERT INTO `rbac_submenu` (`id_submenu`, `nama_submenu`, `link`, `parent_menu`, `icon`) VALUES
(1, 'Categor List', 'index.php/Category/Category_view', 1, 'fa fa-tags'),
(2, 'Sub Category', 'index.php/Subcategory/Subcategory_view', 1, 'fa fa-tags'),
(3, 'Purchase Requisiton', 'index.php/Purchase_requisition/purchase_requisition_view', 2, 'fa fa-tags'),
(4, 'Item', 'index.php/Item/Item_view', 1, 'fa fa-file-text-o'),
(5, 'Vendor', 'index.php/Vendor/Vendor_view', 1, 'fa fa-users'),
(6, 'Purchase Order', 'index.php/Purchase_order/purchase_order_list_veri_after', 3, 'fa fa-file-archive-o'),
(7, 'Direct Order', 'index.php/Purchase_order/purchase_order_direct_add', 3, 'fa fa-file-o'),
(8, 'Stok Opname', 'index.php/Stok_opname/SO_view', 4, 'fa fa-file'),
(9, 'Resto', 'index.php/Resto/Table_view', 5, 'fa fa-coffee'),
(10, 'Receive Item', 'index.php/Receive_item/Purchase_order_list', 6, 'fa fa-file-o'),
(11, 'Menu List', 'index.php/Menu/Menu_view', 7, 'fa fa-list-alt'),
(12, 'Subcategory Menu', 'index.php/Menu_subcategory/MenuSubcategory_view', 7, 'fa fa-columns'),
(13, 'Category Menu', 'index.php/Menu_category/MenuCategory_view', 7, 'fa fa-files-o'),
(14, 'Inventory Report', 'index.php/Purchase_order/LaporanInventoryHome', 8, 'fa fa-book'),
(15, 'Store Requisiton', 'index.php/Store_requisition/store_requisition_add', 9, 'fa fa-external-link-square'),
(16, 'Guest', 'index.php/Guest/Guest_view', 10, 'fa fa-book'),
(17, 'Company', 'index.php/Company/Company_view', 10, 'fa fa-building-o'),
(18, 'Segment', 'index.php/Segment/Segment_view', 10, 'fa fa-clipboard'),
(19, 'Source', 'index.php/Source/Source_view', 10, 'fa fa-archive'),
(20, 'Room Type', 'index.php/Roomtype/RoomType_view', 10, 'fa fa-building-o'),
(21, 'Room Status', 'index.php/Roomstatus/RoomStatus_view', 10, 'fa fa-building-o'),
(22, 'Room', 'index.php/Room/Room_view', 10, 'fa fa-building-o'),
(23, 'Reservation Status', 'index.php/Reservation_status/ReservationStatus_view', 10, 'fa fa-book'),
(24, 'Purpose', 'index.php/Purpose/Purpose_view', 10, 'fa fa-send'),
(25, 'Account Payable', 'index.php/Payment/payment_entry', 11, 'fa fa-pencil-square-o'),
(26, 'Fo Invoice', 'index.php/Fo_outlet/Fo_outlet_view', 12, 'fa fa-edit'),
(27, 'Add Reservation', 'index.php/Fo_outlet/reservation', 12, 'fa fa-bed'),
(28, 'Package', 'index.php/Package/Package_view', 10, 'fa fa-file-o'),
(29, 'Bad Stock', 'index.php/Bad_stok/bad_stok_add', 9, 'fa fa-trash-o'),
(30, 'Form SO', 'index.php/Stok_opname/form_so', 4, 'fa fa-file-o'),
(31, 'Data Aset', 'index.php/Aset/Aset_view', 13, 'fa fa-file-o'),
(32, 'Bad Asset', 'index.php/Aset/status_aset_add', 13, 'fa fa-trash-o'),
(33, 'Payment Report', 'index.php/Payment/LaporanAkuntingHome', 14, 'fa fa-book'),
(34, 'Payment Direct', 'index.php/Payment/payment_entry_direct', 11, ' fa fa-pencil-square-o'),
(35, 'Data Hutang', 'index.php/Payment/hutang', 15, 'fa fa-file-o'),
(36, 'Verifikasi PO', 'index.php/Purchase_order/purchase_order_list_veri', 16, 'fa fa-list-alt'),
(37, 'Data Akun', 'index.php/Akun/DataAkun', 17, 'fa fa-list-ol'),
(38, 'Data Subklas', 'index.php/Akun/index', 17, 'fa fa-list-alt'),
(39, 'Add Voucher', 'index.php/Payment/payment_add_voucher', 11, 'fa fa-list-alt'),
(40, 'Realisasi PO', 'index.php/Purchase_order/realisasi_po_view', 11, 'fa fa-list-alt'),
(41, 'Check List', 'index.php/Payment/check_view', 11, 'fa fa-money'),
(43, 'Expected Arrival', 'index.php/Reservation/ExpectedArrival_view', 12, 'fa fa-list-alt'),
(44, 'Expected Departure', 'index.php/Reservation/ExpectedDeparture_view', 12, 'fa fa-list-alt'),
(45, 'Guest In House', 'index.php/Reservation/GuestInHouse_view', 12, 'fa fa-list-alt'),
(46, 'Reservation List', 'index.php/Fo_outlet/Reservation_list_view', 12, 'fa fa-list-alt'),
(47, 'Stock', 'index.php/Item/Stok_view', 1, 'fa fa-list-alt'),
(48, 'Received PO', 'index.php/Purchase_order/purchase_order_list_aktifkan', 3, 'fa fa-check-square'),
(49, 'Delete PO', 'index.php/Purchase_order/purchase_order_list_hapus', 3, 'fa  fa-times-circle'),
(50, 'Closing', 'index.php/Fo_outlet/Op_closing', 12, 'fa  fa-times-circle'),
(51, 'Report', 'index.php/Fo_outlet/Cetak_home', 12, 'fa fa-book'),
(52, 'Night Audit', 'index.php/Fo_outlet/Na_view', 12, 'fa  fa-times-circle'),
(53, 'Article Sales', 'index.php/Article/Article_sales_view', 10, 'fa fa-book'),
(54, 'Article Payment', 'index.php/Article/Article_payment_view', 10, 'fa fa-money');

-- --------------------------------------------------------

--
-- Table structure for table `rbac_submenu2`
--

CREATE TABLE `rbac_submenu2` (
  `id_submenu` int(10) NOT NULL,
  `nama_submenu` text DEFAULT NULL,
  `link` text DEFAULT NULL,
  `parent_menu` int(11) DEFAULT NULL,
  `icon` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `rbac_submenu2`
--

INSERT INTO `rbac_submenu2` (`id_submenu`, `nama_submenu`, `link`, `parent_menu`, `icon`) VALUES
(1, 'Categor List', 'index.php/Category/Category_view', 1, 'fa fa-tags'),
(2, 'Sub Category', 'index.php/Subcategory/Subcategory_view', 1, 'fa fa-tags'),
(3, 'Purchase Requisiton', 'index.php/Purchase_requisition/purchase_requisition_view', 2, 'fa fa-tags'),
(4, 'Item', 'index.php/Item/Item_view', 1, 'fa fa-file-text-o'),
(5, 'Vendor', 'index.php/Vendor/Vendor_view', 1, 'fa fa-users'),
(6, 'Purchase Order', 'index.php/Purchase_order/purchase_order_list_veri_after', 3, 'fa fa-file-archive-o'),
(7, 'Direct Order', 'index.php/Purchase_order/purchase_order_direct_add', 3, 'fa fa-file-o'),
(8, 'Stok Opname', 'index.php/Stok_opname/SO_view', 4, 'fa fa-file'),
(9, 'Resto', 'index.php/Resto/Table_view', 5, 'fa fa-coffee'),
(10, 'Receive Item', 'index.php/Receive_item/Purchase_order_list', 6, 'fa fa-file-o'),
(11, 'Menu List', 'index.php/Menu/Menu_view', 7, 'fa fa-list-alt'),
(12, 'Subcategory Menu', 'index.php/Menu_subcategory/MenuSubcategory_view', 7, 'fa fa-columns'),
(13, 'Category Menu', 'index.php/Menu_category/MenuCategory_view', 7, 'fa fa-files-o'),
(14, 'Inventory Report', 'index.php/Purchase_order/LaporanInventoryHome', 8, 'fa fa-book'),
(15, 'Store Requisiton', 'index.php/Store_requisition/store_requisition_add', 9, 'fa fa-external-link-square'),
(16, 'Guest', 'index.php/Guest/Guest_view', 10, 'fa fa-book'),
(17, 'Company', 'index.php/Company/Company_view', 10, 'fa fa-building-o'),
(18, 'Segment', 'index.php/Segment/Segment_view', 10, 'fa fa-clipboard'),
(19, 'Source', 'index.php/Source/Source_view', 10, 'fa fa-archive'),
(20, 'Room Type', 'index.php/Roomtype/RoomType_view', 10, 'fa fa-building-o'),
(21, 'Room Status', 'index.php/Roomstatus/RoomStatus_view', 10, 'fa fa-building-o'),
(22, 'Room', 'index.php/Room/Room_view', 10, 'fa fa-building-o'),
(23, 'Reservation Status', 'index.php/Reservation_status/ReservationStatus_view', 10, 'fa fa-book'),
(24, 'Purpose', 'index.php/Purpose/Purpose_view', 10, 'fa fa-send'),
(25, 'Account Payable', 'index.php/Payment/payment_entry', 11, 'fa fa-pencil-square-o'),
(26, 'Fo Invoice', 'index.php/Fo_outlet/Fo_outlet_view', 12, 'fa fa-edit'),
(27, 'Add Reservation', 'index.php/Fo_outlet/reservation', 12, 'fa fa-bed'),
(28, 'Package', 'index.php/Package/Package_view', 10, 'fa fa-file-o'),
(29, 'Bad Stock', 'index.php/Bad_stok/bad_stok_add', 9, 'fa fa-trash-o'),
(30, 'Form SO', 'index.php/Stok_opname/form_so', 4, 'fa fa-file-o'),
(31, 'Data Aset', 'index.php/Aset/Aset_view', 13, 'fa fa-file-o'),
(32, 'Bad Asset', 'index.php/Aset/status_aset_add', 13, 'fa fa-trash-o'),
(33, 'Payment Report', 'index.php/Payment/LaporanAkuntingHome', 14, 'fa fa-book'),
(34, 'Payment Direct', 'index.php/Payment/payment_entry_direct', 11, ' fa fa-pencil-square-o'),
(35, 'Data Hutang', 'index.php/Payment/hutang', 15, 'fa fa-file-o'),
(36, 'Verifikasi PO', 'index.php/Purchase_order/purchase_order_list_veri', 16, 'fa fa-list-alt'),
(37, 'Data Akun', 'index.php/Akun/DataAkun', 17, 'fa fa-list-ol'),
(38, 'Data Subklas', 'index.php/Akun/index', 17, 'fa fa-list-alt'),
(39, 'Add Voucher', 'index.php/Payment/payment_add_voucher', 11, 'fa fa-list-alt'),
(40, 'Realisasi PO', 'index.php/Purchase_order/realisasi_po_view', 11, 'fa fa-list-alt'),
(41, 'Check List', 'index.php/Payment/check_view', 11, 'fa fa-money'),
(43, 'Expected Arrival', 'index.php/Reservation/ExpectedArrival_view', 12, 'fa fa-list-alt'),
(44, 'Expected Departure', 'index.php/Reservation/ExpectedDeparture_view', 12, 'fa fa-list-alt'),
(45, 'Guest In House', 'index.php/Reservation/GuestInHouse_view', 12, 'fa fa-list-alt'),
(46, 'Reservation List', 'index.php/Fo_outlet/Reservation_list_view', 12, 'fa fa-list-alt'),
(47, 'Stock', 'index.php/Item/Stok_view', 1, 'fa fa-list-alt'),
(48, 'Received PO', 'index.php/Purchase_order/purchase_order_list_aktifkan', 3, 'fa fa-check-square'),
(49, 'Delete PO', 'index.php/Purchase_order/purchase_order_list_hapus', 3, 'fa  fa-times-circle'),
(50, 'Closing', 'index.php/Fo_outlet/Op_closing', 12, 'fa  fa-times-circle'),
(51, 'Report', 'index.php/Fo_outlet/Cetak_home', 12, 'fa fa-book'),
(52, 'Night Audit', 'index.php/Fo_outlet/Na_view', 12, 'fa  fa-times-circle');

-- --------------------------------------------------------

--
-- Table structure for table `rbac_user_role`
--

CREATE TABLE `rbac_user_role` (
  `id_user` int(10) DEFAULT NULL,
  `id_role` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `rbac_user_role`
--

INSERT INTO `rbac_user_role` (`id_user`, `id_role`) VALUES
(5, 4),
(7, 4),
(8, 4),
(9, 4),
(1, 4),
(1, 2),
(1, 3),
(1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `table 55`
--

CREATE TABLE `table 55` (
  `COL 1` varchar(5) DEFAULT NULL,
  `COL 2` varchar(1) DEFAULT NULL,
  `COL 3` varchar(17) DEFAULT NULL,
  `COL 4` varchar(3) DEFAULT NULL,
  `COL 5` varchar(1) DEFAULT NULL,
  `COL 6` varchar(1) DEFAULT NULL,
  `COL 7` varchar(1) DEFAULT NULL,
  `COL 8` varchar(1) DEFAULT NULL,
  `COL 9` varchar(1) DEFAULT NULL,
  `COL 10` varchar(1) DEFAULT NULL,
  `COL 11` varchar(1) DEFAULT NULL,
  `COL 12` varchar(1) DEFAULT NULL,
  `COL 13` varchar(1) DEFAULT NULL,
  `COL 14` varchar(1) DEFAULT NULL,
  `COL 15` varchar(1) DEFAULT NULL,
  `COL 16` varchar(1) DEFAULT NULL,
  `COL 17` varchar(1) DEFAULT NULL,
  `COL 18` varchar(1) DEFAULT NULL,
  `COL 19` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `table 55`
--

INSERT INTO `table 55` (`COL 1`, `COL 2`, `COL 3`, `COL 4`, `COL 5`, `COL 6`, `COL 7`, `COL 8`, `COL 9`, `COL 10`, `COL 11`, `COL 12`, `COL 13`, `COL 14`, `COL 15`, `COL 16`, `COL 17`, `COL 18`, `COL 19`) VALUES
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('00482', '8', 'MEJA KANTOR', 'PCS', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
('00483', '8', 'MEJA KECIL', 'PCS', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
('00484', '8', 'KURSI KANTOR', 'PCS', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
('00485', '8', 'KURSI PJNG TUNGGU', 'PCS', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
('00486', '8', 'COMPUTER', 'PCS', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
('00487', '8', 'PRINTER ', 'PCS', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
('00488', '8', 'PRINTER ', 'PCS', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
('00489', '8', 'SCANNER', 'PCS', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
('00490', '8', 'MESIN TIK ', 'PCS', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
('00491', '8', 'AC', 'PCS', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
('00492', '8', 'KIPAS ANGIN ', 'PCS', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
('00493', '8', 'TV CCTV + RECORD', 'PCS', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
('00494', '8', 'TELPON ', 'PCS', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
('00495', '8', 'ALMARI BESI', 'PCS', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
('00496', '8', 'ALMARI PLASTIK', 'PCS', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
('00497', '8', 'CALCULATOR', 'PCS', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
('00498', '8', 'STERPLES', 'PCS', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
('00499', '8', 'JAM DINDING', 'PCS', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
('00500', '8', 'TEMPAT SAMPAH', 'PCS', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
('00501', '8', 'STAVOL ', 'PCS', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbakun`
--

CREATE TABLE `tbakun` (
  `NoAkun` varchar(10) NOT NULL,
  `IdSub` varchar(10) DEFAULT NULL,
  `NamaAkun` varchar(100) DEFAULT NULL,
  `Saldo` double DEFAULT NULL,
  `IsKontra` varchar(1) DEFAULT NULL,
  `IsAktif` varchar(1) DEFAULT NULL,
  `OpeningBalance` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `tbakun`
--

INSERT INTO `tbakun` (`NoAkun`, `IdSub`, `NamaAkun`, `Saldo`, `IsKontra`, `IsAktif`, `OpeningBalance`) VALUES
('11001', '11', 'Kas', -8278720, '0', '1', 0),
('12001', '51', 'Pesediaan edit', -381311.18299999996, '0', '1', 0),
('50001', '50', 'Biaya', 386234.88899999997, '0', '1', 1),
('51001', '51', 'Transfer MI Out', 1000000, '0', '0', 0),
('51002', '51', 'Komisi', 0, '0', '0', 0),
('51003', '51', 'Alat Tulis', 0, '0', '0', 0),
('51004', '51', 'PCC', 0, '0', '0', 0),
('52001', '52', 'Bahan Pembersih', 0, '0', '0', 0),
('52002', '52', 'Dekorasi', 0, '0', '0', 0),
('52003', '52', 'Operasional Lain', 0, '0', '0', 0),
('53001', '53', 'Food', 76077.75, '0', '0', 0),
('53002', '53', 'Baverage', 0, '0', '0', 0),
('54001', '53', 'Admin Cetakan', 35000, '0', '0', 0),
('54002', '54', 'Hiburan', 0, '0', '0', 0),
('54003', '54', 'Operasional Lain', 0, '0', '0', 0),
('55001', '55', 'Biaya Telepon', 0, '0', '0', 0),
('56001', '56', 'Obat Pool', 0, '0', '0', 0),
('56002', '56', 'Operasional Lain', 0, '0', '0', 0),
('57001', '57', 'Gaji Karyawan', 0, '0', '0', 0),
('57002', '57', 'Kas Karyawan', 0, '0', '0', 0),
('58001', '58', 'Biaya Satpam', 0, '0', '1', 0),
('58002', '58', 'Biaya Manajer', 0, '0', '1', 0),
('58003', '58', 'Biaya Upacara adat', 0, '0', '1', 0),
('58004', '58', 'Sumbangan', 0, '0', '1', 0),
('58005', '58', 'Alat Tulis', 0, '0', '1', 0),
('59001', '59', 'Biaya Pemasaran', 0, '0', '1', 0),
('61001', '61', 'Service Bangunan', 156116.80800000002, '0', '1', 0),
('61002', '61', 'Kebun', 0, '0', '1', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbarticlecategory`
--

CREATE TABLE `tbarticlecategory` (
  `id` int(11) NOT NULL,
  `articlecategory` varchar(100) DEFAULT NULL,
  `status` int(11) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `tbarticlecategory`
--

INSERT INTO `tbarticlecategory` (`id`, `articlecategory`, `status`) VALUES
(1, 'Room Revenue', 1),
(2, 'Other Room Revenue', 1),
(3, 'Other Income', 1),
(4, 'Extra Bed', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbarticlepayment`
--

CREATE TABLE `tbarticlepayment` (
  `id` int(11) NOT NULL,
  `articlepayment` varchar(100) DEFAULT NULL,
  `status` int(11) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `tbarticlepayment`
--

INSERT INTO `tbarticlepayment` (`id`, `articlepayment`, `status`) VALUES
(1, 'Cash Settlement', 1),
(2, 'Bank Transfer', 1),
(3, 'Debit BCA', 1),
(4, 'Debit Mandiri', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbarticlesales`
--

CREATE TABLE `tbarticlesales` (
  `idarticle` varchar(10) NOT NULL,
  `articlename` varchar(100) DEFAULT NULL,
  `price` double(20,2) DEFAULT NULL,
  `service` double(20,2) DEFAULT NULL,
  `tax` double(20,2) DEFAULT NULL,
  `finalprice` double(20,2) DEFAULT NULL,
  `idarticlecategory` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  `iddepartement` int(11) DEFAULT NULL,
  `pricetype` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `tbarticlesales`
--

INSERT INTO `tbarticlesales` (`idarticle`, `articlename`, `price`, `service`, `tax`, `finalprice`, `idarticlecategory`, `status`, `iddepartement`, `pricetype`) VALUES
('CHG001', 'Room', 400000.00, 40000.00, 44000.00, 484000.00, 1, 1, 1, 'Tax&Service'),
('CHG002', 'Extra Bed', 10000.00, 1000.00, 1100.00, 12100.00, 2, 1, 1, 'Tax'),
('CHG003', 'Kepitu Restaurant', 10000.00, 1000.00, 1100.00, 12100.00, 3, 1, 1, 'Tax'),
('CHG004', 'Serayu Spa', 10000.00, 1000.00, 1100.00, 12100.00, 3, 1, 1, 'Tax'),
('CHG005', 'Day Use', 10000.00, 1000.00, 1100.00, 12100.00, 3, 1, 1, 'Tax'),
('CHG006', 'Transport', 10000.00, 1000.00, 1100.00, 12100.00, 3, 1, 1, 'Tax'),
('CHG007', 'Tour', 10000.00, 1000.00, 1100.00, 12100.00, 3, 1, 1, 'Tax');

-- --------------------------------------------------------

--
-- Table structure for table `tbaset`
--

CREATE TABLE `tbaset` (
  `idaset` int(10) NOT NULL,
  `codeaset` varchar(20) DEFAULT NULL,
  `codeitem` varchar(20) DEFAULT NULL,
  `baik` int(10) DEFAULT 0,
  `kurangbaik` int(10) DEFAULT 0,
  `used` int(10) DEFAULT 0,
  `lose` int(10) DEFAULT 0,
  `out` int(10) DEFAULT 0,
  `jumlah` int(10) DEFAULT 0,
  `baikHK` int(10) DEFAULT 0,
  `kurangbaikHK` int(10) DEFAULT 0,
  `usedHK` int(10) DEFAULT 0,
  `loseHK` int(10) DEFAULT 0,
  `outHK` int(10) DEFAULT 0,
  `jumlahHK` int(10) DEFAULT 0,
  `baikFO` int(10) DEFAULT 0,
  `kurangbaikFO` int(10) DEFAULT 0,
  `usedFO` int(10) DEFAULT 0,
  `loseFO` int(10) DEFAULT 0,
  `outFO` int(10) DEFAULT 0,
  `jumlahFO` int(10) DEFAULT 0,
  `baikFB` int(10) DEFAULT 0,
  `kurangbaikFB` int(10) DEFAULT 0,
  `usedFB` int(10) DEFAULT 0,
  `loseFB` int(10) DEFAULT 0,
  `outFB` int(10) DEFAULT 0,
  `jumlahFB` int(10) DEFAULT 0,
  `baikBO` int(10) DEFAULT 0,
  `kurangbaikBO` int(10) DEFAULT 0,
  `usedBO` int(10) DEFAULT 0,
  `loseBO` int(10) DEFAULT 0,
  `outBO` int(10) DEFAULT 0,
  `jumlahBO` int(10) DEFAULT 0,
  `baikEng` int(10) DEFAULT 0,
  `kurangbaikEng` int(10) DEFAULT 0,
  `usedEng` int(10) DEFAULT 0,
  `loseEng` int(10) DEFAULT 0,
  `outEng` int(10) DEFAULT 0,
  `jumlahEng` int(10) DEFAULT 0,
  `baikPG` int(10) DEFAULT 0,
  `kurangbaikPG` int(10) DEFAULT 0,
  `usedPG` int(10) DEFAULT 0,
  `losePG` int(10) DEFAULT 0,
  `outPG` int(10) DEFAULT 0,
  `jumlahPG` int(10) DEFAULT 0,
  `baikSec` int(10) DEFAULT 0,
  `kurangbaikSec` int(10) DEFAULT 0,
  `usedSec` int(10) DEFAULT 0,
  `loseSec` int(10) DEFAULT 0,
  `outSec` int(10) DEFAULT 0,
  `jumlahSec` int(10) DEFAULT 0,
  `baikBar` int(10) DEFAULT 0,
  `kurangbaikBar` int(10) DEFAULT 0,
  `usedBar` int(10) DEFAULT 0,
  `loseBar` int(10) DEFAULT 0,
  `outBar` int(10) DEFAULT 0,
  `jumlahBar` int(10) DEFAULT 0,
  `isaktif` int(1) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbasetdetil`
--

CREATE TABLE `tbasetdetil` (
  `id` int(10) NOT NULL,
  `codeaset` varchar(20) DEFAULT NULL,
  `qty` int(10) DEFAULT 0,
  `kondisiawal` varchar(20) DEFAULT NULL,
  `kondisiakhir` varchar(20) DEFAULT NULL,
  `iddepartement` int(10) DEFAULT NULL,
  `departement` text DEFAULT NULL,
  `note` text DEFAULT NULL,
  `inputdate` datetime DEFAULT NULL,
  `idoperator` varchar(20) DEFAULT NULL,
  `operator` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbbadstok`
--

CREATE TABLE `tbbadstok` (
  `kodebs` varchar(20) NOT NULL,
  `iddepartement` int(11) DEFAULT NULL,
  `departement` varchar(50) DEFAULT NULL,
  `dateadd` datetime DEFAULT NULL,
  `idoperator` int(11) DEFAULT NULL,
  `description` tinytext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbbadstokdetil`
--

CREATE TABLE `tbbadstokdetil` (
  `id` int(11) NOT NULL,
  `kodebs` varchar(20) DEFAULT NULL,
  `codeitem` varchar(11) DEFAULT NULL,
  `description` tinytext DEFAULT NULL,
  `unit` varchar(45) DEFAULT NULL,
  `price` double(20,3) DEFAULT NULL,
  `qty` double(10,3) DEFAULT NULL,
  `currstok` double(10,3) DEFAULT NULL,
  `note` text DEFAULT NULL,
  `iddepartement` int(11) DEFAULT NULL,
  `departement` varchar(50) DEFAULT NULL,
  `idoperator` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbbank`
--

CREATE TABLE `tbbank` (
  `id` int(10) NOT NULL,
  `namabank` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `tbbank`
--

INSERT INTO `tbbank` (`id`, `namabank`) VALUES
(1, 'Danamon'),
(2, 'BNI'),
(3, 'BRI'),
(4, 'BCA'),
(5, 'MANDIRI');

-- --------------------------------------------------------

--
-- Table structure for table `tbcategory`
--

CREATE TABLE `tbcategory` (
  `idcategory` int(11) NOT NULL,
  `category` varchar(100) DEFAULT NULL,
  `isaktif` int(11) DEFAULT 1,
  `description` tinytext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `tbcategory`
--

INSERT INTO `tbcategory` (`idcategory`, `category`, `isaktif`, `description`) VALUES
(1, 'FRUIT AND VAGETABLE', 1, 'FRUIT AND VAGETABLE'),
(2, 'FOOD', 1, 'FOOD'),
(3, 'MATERIAL', 1, 'MATERIAL'),
(4, 'KORAN', 1, 'KORAN'),
(5, 'ASET', 1, 'ASET'),
(6, 'BEVERAGE ', 1, 'BEVERAGE');

-- --------------------------------------------------------

--
-- Table structure for table `tbcheck`
--

CREATE TABLE `tbcheck` (
  `id` int(10) NOT NULL,
  `checkno` varchar(50) DEFAULT NULL,
  `bank` int(10) DEFAULT NULL,
  `total` decimal(15,2) DEFAULT NULL,
  `idvendor` int(10) DEFAULT NULL,
  `idoperator` int(10) DEFAULT NULL,
  `tglinput` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbcheckdetil`
--

CREATE TABLE `tbcheckdetil` (
  `id` int(10) NOT NULL,
  `idmaster` int(10) DEFAULT NULL,
  `idpayment` varchar(20) DEFAULT NULL,
  `amount` decimal(15,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbcheckout`
--

CREATE TABLE `tbcheckout` (
  `id` varchar(50) NOT NULL,
  `idreservation` varchar(50) DEFAULT NULL,
  `datecheckout` datetime DEFAULT NULL,
  `idoperator` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbcheckoutdetil`
--

CREATE TABLE `tbcheckoutdetil` (
  `id` int(10) NOT NULL,
  `idmaster` varchar(50) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `invoicemanual` text DEFAULT NULL,
  `description` text DEFAULT NULL,
  `charge` double(12,2) DEFAULT NULL,
  `payment` double(12,2) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `trxid` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbcompany`
--

CREATE TABLE `tbcompany` (
  `idcompany` int(20) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `idstate` varchar(50) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `zipcode` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `description` tinytext DEFAULT NULL,
  `currtimestamp` datetime DEFAULT NULL,
  `isaktif` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `tbcompany`
--

INSERT INTO `tbcompany` (`idcompany`, `name`, `address`, `idstate`, `state`, `zipcode`, `email`, `phone`, `type`, `description`, `currtimestamp`, `isaktif`) VALUES
(1, 'BANK ABC', '123', 'Afrika Selatan', 'Afrika Selatan', '010101', 'dek.sandhiyasa990@gmail.com', '09090', 'Company', '', '2018-11-14 23:08:15', 1),
(2, 'Personal Account', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbdepartement`
--

CREATE TABLE `tbdepartement` (
  `iddepartement` int(11) NOT NULL,
  `departement` varchar(45) DEFAULT NULL,
  `isaktif` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `tbdepartement`
--

INSERT INTO `tbdepartement` (`iddepartement`, `departement`, `isaktif`) VALUES
(1, 'Accounting', 1),
(2, 'House Keeping', 1),
(3, 'Front Office', 1),
(4, 'Food & Baverage', 1),
(5, 'Back Office', 1),
(6, 'Bar', 1),
(7, 'Engineering', 1),
(8, 'Pool & Gardener', 1),
(9, 'Security', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbdetiljurnal`
--

CREATE TABLE `tbdetiljurnal` (
  `NoTxn` int(100) NOT NULL,
  `IdJurnal` varchar(50) DEFAULT NULL,
  `NoAkun` varchar(20) DEFAULT NULL,
  `Debet` double DEFAULT NULL,
  `Kredit` double DEFAULT NULL,
  `TanggalInput` datetime DEFAULT NULL,
  `TanggalSistem` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbfbtrx`
--

CREATE TABLE `tbfbtrx` (
  `trxid` varchar(20) NOT NULL,
  `trxdate` datetime DEFAULT NULL,
  `tableid` int(11) DEFAULT 1,
  `idguest` varchar(20) DEFAULT NULL,
  `idreservation` varchar(20) DEFAULT NULL,
  `idroom` int(11) DEFAULT NULL,
  `total` double(20,2) DEFAULT NULL,
  `disc` double(20,2) DEFAULT NULL,
  `total2` double(20,2) DEFAULT NULL,
  `service` double(20,2) DEFAULT NULL,
  `tax` double(20,2) DEFAULT NULL,
  `grandtotal` double(20,2) DEFAULT NULL,
  `idarticle` int(11) DEFAULT 0,
  `totalpayment` double(20,2) DEFAULT NULL,
  `remain` double(20,2) DEFAULT NULL,
  `statuspayment` int(11) DEFAULT NULL,
  `idreservationcharge` varchar(20) DEFAULT NULL,
  `idroomcharge` varchar(20) DEFAULT NULL,
  `notes` tinytext DEFAULT NULL,
  `idoperatoropenorder` int(11) DEFAULT NULL,
  `idoperatorprocess` int(11) DEFAULT NULL,
  `dateclosingNA` datetime DEFAULT NULL,
  `dateclosing` datetime DEFAULT NULL,
  `isclosing` int(11) DEFAULT 0,
  `isNA` int(11) DEFAULT 0,
  `waiterid` int(10) DEFAULT NULL,
  `waitername` text DEFAULT NULL,
  `paymentstatus` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `tbfbtrx`
--

INSERT INTO `tbfbtrx` (`trxid`, `trxdate`, `tableid`, `idguest`, `idreservation`, `idroom`, `total`, `disc`, `total2`, `service`, `tax`, `grandtotal`, `idarticle`, `totalpayment`, `remain`, `statuspayment`, `idreservationcharge`, `idroomcharge`, `notes`, `idoperatoropenorder`, `idoperatorprocess`, `dateclosingNA`, `dateclosing`, `isclosing`, `isNA`, `waiterid`, `waitername`, `paymentstatus`) VALUES
('FB220600001', '2022-06-04 13:14:38', 1, '1', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 0, 0, 0, '-', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbfbtrxdetil`
--

CREATE TABLE `tbfbtrxdetil` (
  `id` int(10) NOT NULL,
  `trxid` varchar(15) DEFAULT NULL,
  `trxdate` datetime DEFAULT NULL,
  `menuid` varchar(10) DEFAULT NULL,
  `menu` varchar(200) DEFAULT NULL,
  `price` double(20,2) DEFAULT NULL,
  `disp` double(5,2) DEFAULT NULL,
  `dism` double(20,2) DEFAULT NULL,
  `qty` int(10) DEFAULT NULL,
  `subtotal` double(20,2) DEFAULT NULL,
  `addrequest` text DEFAULT NULL,
  `idoperator` int(10) DEFAULT NULL,
  `statustrx` int(1) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `datena` datetime DEFAULT NULL,
  `waiterid` int(1) DEFAULT NULL,
  `waitername` text DEFAULT NULL,
  `ordernomanual` varchar(50) DEFAULT NULL,
  `price1` double(20,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `tbfbtrxdetil`
--

INSERT INTO `tbfbtrxdetil` (`id`, `trxid`, `trxdate`, `menuid`, `menu`, `price`, `disp`, `dism`, `qty`, `subtotal`, `addrequest`, `idoperator`, `statustrx`, `description`, `datena`, `waiterid`, `waitername`, `ordernomanual`, `price1`) VALUES
(1, 'FB220600001', '2022-06-04 13:14:38', '13', 'BEEF BURGER', 55000.00, 0.00, 0.00, 1, 55000.00, '-', 1, NULL, NULL, NULL, 0, '-', '-', NULL),
(2, 'FB220600001', '2022-06-04 13:14:38', '12', 'CHICKEN BURGER', 50000.00, 0.00, 0.00, 1, 50000.00, '-', 1, NULL, NULL, NULL, 0, '-', '-', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbfotrx`
--

CREATE TABLE `tbfotrx` (
  `kodemaster` varchar(50) NOT NULL,
  `tgl` datetime DEFAULT NULL,
  `kodereservation` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbfotrxdetil`
--

CREATE TABLE `tbfotrxdetil` (
  `kode` int(10) NOT NULL,
  `kodemaster` varchar(50) DEFAULT NULL,
  `invoicemanual` varchar(50) DEFAULT NULL,
  `idroom` varchar(50) DEFAULT NULL,
  `tgl` datetime DEFAULT NULL,
  `tipe` varchar(50) DEFAULT NULL,
  `idarticle` varchar(50) DEFAULT NULL,
  `articlename` text DEFAULT NULL,
  `desc` varchar(50) DEFAULT NULL,
  `charge` bigint(20) DEFAULT NULL,
  `payment` bigint(20) DEFAULT NULL,
  `idoperator` varchar(50) DEFAULT NULL,
  `isclosing` int(1) DEFAULT 0,
  `isna` int(1) DEFAULT 0,
  `tglna` datetime DEFAULT NULL,
  `dateclosing` datetime DEFAULT NULL,
  `night` int(10) DEFAULT NULL,
  `kodemasterbill` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `tbfotrxdetil`
--

INSERT INTO `tbfotrxdetil` (`kode`, `kodemaster`, `invoicemanual`, `idroom`, `tgl`, `tipe`, `idarticle`, `articlename`, `desc`, `charge`, `payment`, `idoperator`, `isclosing`, `isna`, `tglna`, `dateclosing`, `night`, `kodemasterbill`) VALUES
(1, 'RS22050001', '0009', '', '2022-05-27 00:00:00', '', 'CHG002', 'Extra Bed', 'Extra Bed', 12100, 0, '1', 0, 0, NULL, NULL, NULL, NULL),
(2, 'RS22050001', '0888', '', '2022-05-27 00:00:00', '', 'CHG006', 'Transport', 'Transport', 200000, 0, '1', 0, 0, NULL, NULL, NULL, NULL),
(3, 'RS22050001', '0888', '', '2022-05-27 00:00:00', '', '1', 'Cash Settlement', 'Cash Settlement : Cash', 0, 212100, '1', 0, 0, NULL, NULL, NULL, NULL),
(4, 'RS22050002', 'Room Charge (NA)', '6', '2022-05-28 13:12:48', '11', 'CHG001', 'Room', 'Room Charge (NA)', 0, 0, '1', 1, 1, '2022-05-27 10:55:34', '2022-05-28 13:12:48', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbguest`
--

CREATE TABLE `tbguest` (
  `idguest` int(20) NOT NULL,
  `saluation` varchar(10) DEFAULT NULL,
  `firstname` varchar(100) DEFAULT NULL,
  `lastname` varchar(100) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `idstate` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `zipcode` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `idtype` varchar(100) DEFAULT NULL,
  `idnumber` varchar(100) DEFAULT NULL,
  `description` tinytext DEFAULT NULL,
  `currtimestamp` datetime DEFAULT NULL,
  `isaktif` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `tbguest`
--

INSERT INTO `tbguest` (`idguest`, `saluation`, `firstname`, `lastname`, `address`, `idstate`, `state`, `zipcode`, `email`, `phone`, `gender`, `birthday`, `idtype`, `idnumber`, `description`, `currtimestamp`, `isaktif`) VALUES
(1, 'Mr.', 'ERIK', 'TOHIR', 'BERTEH\nPEREAN TENGAH', 'Afghanistan', 'Afghanistan', '82191', 'et@mail.com', '0812345678', 'Male', '2022-05-28', 'Passport', '123', '-', '2022-05-28 18:28:06', 1),
(2, 'Mr.', 'Bayu', 'Irawan', 'Perean\r\nBaturiti', 'Indonesia', 'Indonesia', '82191', 'hadee.bayu@gmail.com', '081350908676', 'Male', '2022-05-28', 'Passport', '123', '-', '2022-05-28 21:38:20', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbhistoryhutang`
--

CREATE TABLE `tbhistoryhutang` (
  `id` int(10) NOT NULL,
  `kodero` varchar(100) DEFAULT NULL,
  `operator` int(11) DEFAULT NULL,
  `dateadd` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbhistoryitemprice`
--

CREATE TABLE `tbhistoryitemprice` (
  `id` int(10) NOT NULL,
  `codeitem` varchar(100) DEFAULT NULL,
  `price` double(20,3) DEFAULT NULL,
  `newprice` double(20,3) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `idvendor` int(11) DEFAULT NULL,
  `pricehpp` double(20,3) DEFAULT NULL,
  `kodero` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbitem`
--

CREATE TABLE `tbitem` (
  `iditem` int(11) NOT NULL,
  `codeitem` varchar(100) DEFAULT NULL,
  `idsubcategory` int(11) DEFAULT NULL,
  `description` varchar(225) DEFAULT NULL,
  `unit` varchar(45) DEFAULT NULL,
  `price` double(20,3) DEFAULT NULL,
  `stok` double(15,3) DEFAULT 0.000,
  `isaktif` char(1) DEFAULT '1',
  `stokHK` double(15,3) DEFAULT 0.000,
  `stokFO` double(15,3) DEFAULT 0.000,
  `stokFB` double(15,3) DEFAULT 0.000,
  `stokBO` double(15,3) DEFAULT 0.000,
  `idvendor` int(10) DEFAULT NULL,
  `stokBar` double(15,3) DEFAULT 0.000,
  `stokSec` double(15,3) DEFAULT 0.000,
  `stokPG` double(15,3) DEFAULT 0.000,
  `stokEng` double(15,3) DEFAULT 0.000,
  `isHK` int(11) DEFAULT 0,
  `isFO` int(11) DEFAULT 0,
  `isFB` int(11) NOT NULL DEFAULT 0,
  `isBO` int(11) DEFAULT 0,
  `isBar` int(11) DEFAULT 0,
  `isSec` int(11) DEFAULT 0,
  `isPG` int(11) DEFAULT 0,
  `isEng` int(11) DEFAULT 0,
  `lastprice` double(15,3) DEFAULT 0.000,
  `akunHK` int(10) DEFAULT 12001,
  `akunFO` int(10) DEFAULT 12001,
  `akunBar` int(10) DEFAULT 12001,
  `akunSec` int(10) DEFAULT 12001,
  `akunPG` int(10) DEFAULT 12001,
  `akunEng` int(10) DEFAULT 12001,
  `akunFB` int(10) DEFAULT 12001,
  `akunBO` int(10) DEFAULT 12001,
  `isprosesopname` int(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `tbitem`
--

INSERT INTO `tbitem` (`iditem`, `codeitem`, `idsubcategory`, `description`, `unit`, `price`, `stok`, `isaktif`, `stokHK`, `stokFO`, `stokFB`, `stokBO`, `idvendor`, `stokBar`, `stokSec`, `stokPG`, `stokEng`, `isHK`, `isFO`, `isFB`, `isBO`, `isBar`, `isSec`, `isPG`, `isEng`, `lastprice`, `akunHK`, `akunFO`, `akunBar`, `akunSec`, `akunPG`, `akunEng`, `akunFB`, `akunBO`, `isprosesopname`) VALUES
(10, '00003', 3, 'COCO CRUNCH', 'PAK', 47291.667, -9.000, '0', 0.000, 0.000, 30.000, 3.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 50000.000, 52003, 1101, 1101, 1101, 1101, 1101, 53001, 1101, 0),
(11, '00004', 3, 'CORN FLAKES', 'PAK', 33500.000, 13.000, '1', 0.000, 0.000, 44.000, 3.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 33500.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(12, '00005', 3, 'KRUPUK UDANG', 'PAK', 26500.000, 15.000, '1', 0.000, 0.000, 45.000, 2.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 26500.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(13, '00006', 3, 'SPAGHETTI', 'PAK', 25000.000, -5.000, '1', 0.000, 0.000, 22.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 25000.000, 12001, 61001, 61001, 61001, 61001, 61001, 53001, 61001, 0),
(14, '00007', 3, 'MUSTARD', 'PCS', 35000.000, 4.000, '1', 0.000, 0.000, 19.000, 1.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 35000.000, 12001, 61001, 61001, 61001, 61001, 61001, 54001, 61001, 0),
(15, '00008', 3, 'FRESH MILK/SUSU ULTRA ', 'PAK', 15500.000, -310.000, '1', 0.000, 0.000, 420.000, 0.000, NULL, 37.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 185000.000, 12001, 61001, 61001, 61001, 61001, 61001, 53001, 61001, 0),
(16, '00009', 3, 'MUSHROOM', 'CAN', 16500.000, 22.000, '1', 0.000, 0.000, 107.000, 4.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 16500.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(17, '00010', 3, 'PERMIFAN YEAST', 'PCS', 2930.000, -24.000, '1', 0.000, 0.000, 49.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 4250.000, 12001, 61001, 61001, 61001, 61001, 61001, 53001, 61001, 0),
(18, '00011', 3, 'SANTAN KARA', 'PCS', 10000.000, 20.000, '1', 0.000, 0.000, 75.000, 4.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 10000.000, 12001, 61001, 61001, 61001, 61001, 61001, 53001, 61001, 0),
(19, '00012', 3, 'BUMBU PECEL', 'PCS', 13500.000, -80.000, '1', 0.000, 0.000, 200.000, 15.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 13500.000, 12001, 61001, 61001, 61001, 61001, 61001, 53002, 61001, 0),
(20, '00013', 4, 'OLD FASHION GLASS', 'PCS', 12000.000, 1.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 12000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(21, '00014', 3, 'CHEESE SLICE', 'PCS', 10666.000, -155.000, '1', 0.000, 0.000, 169.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 6301.000, 12001, 61001, 61001, 61001, 61001, 61001, 53001, 61001, 0),
(22, '00015', 3, 'CHEESE CEDAR', 'PCS', 12263.866, 2.000, '1', 0.000, 0.000, 20.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 12240.000, 12001, 61001, 61001, 61001, 61001, 61001, 53001, 61001, 0),
(23, '00016', 3, 'RED CERRY', 'BTL', 60000.000, 9.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 4.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 60000.000, 12001, 61001, 61001, 61001, 61001, 61001, 53001, 61001, 0),
(24, '00017', 3, 'TOMATO ABC BOTOL', 'PCS', 11003.448, -19.000, '1', 0.000, 0.000, 61.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 10923.000, 12001, 61001, 61001, 61001, 61001, 61001, 53001, 61001, 0),
(25, '00018', 3, 'TOMATO SASET', 'PCS', 5360.000, 4.000, '1', 0.000, 0.000, 8.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 5270.000, 12001, 61001, 61001, 61001, 61001, 61001, 53001, 61001, 0),
(26, '00019', 3, 'SAMBAL SASET', 'PCS', 6670.000, 2.000, '1', 0.000, 0.000, 8.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 6690.000, 12001, 61001, 61001, 61001, 61001, 61001, 53001, 61001, 0),
(27, '00020', 3, 'AJINOMOTO', 'PCS', 37500.000, 8.000, '1', 0.000, 0.000, 10.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 37500.000, 12001, 61001, 61001, 61001, 61001, 61001, 53001, 61001, 0),
(28, '00021', 3, 'SAOS RAJA RASA', 'BTL', 37500.000, -5.000, '1', 0.000, 0.000, 22.000, 2.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 37500.000, 12001, 61001, 61001, 61001, 61001, 61001, 53001, 61001, 0),
(29, '00022', 3, 'SAOS BAHAGIA', 'PCS', 18500.000, -8.000, '1', 0.000, 0.000, 22.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 18500.000, 12001, 61001, 61001, 61001, 61001, 61001, 53001, 61001, 0),
(30, '00023', 3, 'SAMBAL ABC BOTOL', 'PCS', 12813.675, 5.000, '1', 0.000, 0.000, 46.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 12172.000, 12001, 61001, 61001, 61001, 61001, 61001, 53001, 61001, 0),
(31, '00024', 3, 'LEA PERIN', 'PCS', 50000.000, -8.000, '1', 0.000, 0.000, 19.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 50000.000, 12001, 61001, 61001, 61001, 61001, 61001, 53001, 61001, 0),
(32, '00025', 3, 'MINYAK WIJEN', 'PCS', 33050.000, 2.000, '1', 0.000, 0.000, 4.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 33050.000, 12001, 61001, 61001, 61001, 61001, 61001, 53001, 61001, 0),
(33, '00026', 3, 'SAOS TIRAM', 'BTL', 40000.000, 0.000, '1', 0.000, 0.000, 24.000, 2.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 40000.000, 12001, 61001, 61001, 61001, 61001, 61001, 53001, 61001, 0),
(34, '00027', 3, 'KECAP MANIS', 'PCS', 27500.000, 1.000, '1', 0.000, 0.000, 49.000, 2.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 27500.000, 12001, 61001, 61001, 61001, 61001, 61001, 53001, 61001, 0),
(35, '00028', 3, 'CUKA DIXI', 'PCS', 20000.000, 12.000, '1', 0.000, 0.000, 2.000, 1.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 20000.000, 12001, 61001, 61001, 61001, 61001, 61001, 53001, 61001, 0),
(36, '00029', 3, 'SAOS TAPE', 'PCS', 35000.000, 3.000, '1', 0.000, 0.000, 2.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 35000.000, 12001, 61001, 61001, 61001, 61001, 61001, 53001, 61001, 0),
(37, '00030', 3, 'SAOS TIGA TOMAT', 'PCS', 20000.000, 10.000, '1', 0.000, 0.000, 5.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 20000.000, 12001, 61001, 61001, 61001, 61001, 61001, 53001, 61001, 0),
(38, '00031', 3, 'KECAP INGGRIS', 'PCS', 30000.000, 1.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 30000.000, 12001, 61001, 61001, 61001, 61001, 61001, 53001, 61001, 0),
(39, '00032', 3, 'MAYONAIS 3 LTR', 'GALON', 151250.000, -2.000, '1', 0.000, 0.000, 13.000, 1.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 155000.000, 12001, 61001, 61001, 61001, 61001, 61001, 53001, 61001, 0),
(40, '00033', 3, 'YOGHURT', 'PCS', 34000.000, -191.000, '1', 0.000, 0.000, 227.000, 4.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 34000.000, 12001, 61002, 61002, 61002, 61002, 61002, 53001, 61002, 0),
(41, '00034', 3, 'GARAM', 'PCS', 14906.250, -1.000, '1', 0.000, 0.000, 22.000, 5.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 15000.000, 12001, 61002, 61002, 61002, 61002, 61002, 53001, 61002, 0),
(42, '00035', 3, 'FISH GRAVI', 'PCS', 20000.000, 1.000, '1', 0.000, 0.000, 3.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 20000.000, 12001, 61002, 61002, 61002, 61002, 61002, 53001, 61002, 0),
(43, '00036', 3, 'TOMATO KETCHUP GLN', 'GALON', 79493.775, 2.000, '1', 0.000, 0.000, 7.000, 1.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 79500.000, 12001, 61002, 61002, 61002, 61002, 61002, 53001, 61002, 0),
(44, '00037', 3, 'MINYAK SALAD', 'GALON', 197913.195, 5.000, '1', 0.000, 0.000, 11.000, 1.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 198000.000, 12001, 61002, 61002, 61002, 61002, 61002, 53001, 61002, 0),
(45, '00038', 3, 'MINYAK BIMOLI', 'GALON', 228000.000, -3.000, '1', 0.000, 0.000, 18.000, 1.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 228000.000, 12001, 61002, 61002, 61002, 61002, 61002, 53001, 61002, 0),
(46, '00039', 3, 'TEPUNG BERAS', 'PAK', 7500.000, 2.000, '1', 0.000, 0.000, 44.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 7500.000, 12001, 61002, 61002, 61002, 61002, 61002, 53001, 61002, 0),
(47, '00040', 3, 'TEPUNG TERIGU', 'PAK', 12500.000, -2.000, '1', 0.000, 0.000, 151.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 12500.000, 12001, 61002, 61002, 61002, 61002, 61002, 53001, 61002, 0),
(48, '00041', 3, 'TEPUNG KANJI', 'PCS', 2580.000, -2.000, '1', 0.000, 0.000, 8.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 6000.000, 12001, 61002, 61002, 61002, 61002, 61002, 53001, 61002, 0),
(49, '00042', 3, 'TEPUNG TAPIOKA', 'PAK', 4800.000, -6.000, '1', 0.000, 0.000, 15.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 4800.000, 12001, 61002, 61002, 61002, 61002, 61002, 53001, 61002, 0),
(50, '00043', 3, 'MIE BURUNG DARA', 'PCS', 5100.000, -60.000, '1', 0.000, 0.000, 85.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 5100.000, 12001, 61002, 61002, 61002, 61002, 61002, 53001, 61002, 0),
(51, '00044', 3, 'MENTEGA', 'PAK', 155000.000, 1.000, '1', 0.000, 0.000, 33.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 155000.000, 12001, 61002, 61002, 61002, 61002, 61002, 53001, 61002, 0),
(52, '00045', 3, 'KNOR', 'CAN', 0.000, 2.000, '1', 0.000, 0.000, 15.000, 1.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 85000.000, 12001, 61002, 61002, 61002, 61002, 61002, 53001, 61002, 0),
(53, '00046', 3, 'TOMATO PASTA', 'CAN', 125000.000, -1.000, '1', 0.000, 0.000, 7.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 125000.000, 12001, 61002, 61002, 61002, 61002, 61002, 53001, 61002, 0),
(54, '00047', 3, 'TELOR', 'KRAT', 42000.000, -169.000, '1', 0.000, 0.000, 391.000, 10.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 42000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(55, '00048', 3, 'BERAS 25KG', 'PAK', 297916.667, -8.000, '1', 0.000, 0.000, 9.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 295000.000, 12001, 61002, 61002, 61002, 61002, 61002, 53001, 61002, 0),
(56, '00049', 3, 'BERAS 10KG', 'PAK', 127846.154, -7.000, '1', 0.000, 0.000, 25.500, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 130000.000, 12001, 61002, 61002, 61002, 61002, 61002, 53001, 61002, 0),
(57, '00050', 3, 'GULA PASIR', 'KG', 13800.000, -111.000, '1', 0.000, 0.000, 173.000, 4.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 13800.000, 12001, 61001, 61001, 61001, 61001, 61001, 53001, 61001, 0),
(58, '00051', 3, 'KOPI BALI 250gr', 'PAK', 20000.000, 82.000, '1', 0.000, 0.000, 81.000, 3.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 20000.000, 12001, 61002, 61002, 61002, 61002, 61002, 53001, 61002, 0),
(59, '00052', 3, 'TEH SARIWANGI', 'PAK', 6000.000, 44.000, '1', 0.000, 0.000, 81.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 6000.000, 12001, 61002, 61002, 61002, 61002, 61002, 53002, 61002, 0),
(60, '00053', 3, 'KOPI BREAKFAST', 'PAK', 25000.000, -47.000, '1', 0.000, 0.000, 143.000, 8.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 25000.000, 12001, 61002, 61002, 61002, 61002, 61002, 53002, 61002, 0),
(61, '00054', 3, 'KOPI NESCAFE', 'BTL', 80000.000, 7.000, '1', 0.000, 0.000, 3.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 80000.000, 12001, 61002, 61002, 61002, 61002, 61002, 53002, 61002, 0),
(62, '00055', 3, 'GULA SASET', 'PCS', 300.000, -893.500, '1', 1000.000, 0.000, 2761.000, 150.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 300.000, 52003, 61002, 61002, 61002, 61002, 61002, 53001, 61002, 0),
(63, '00056', 3, 'KOPI SASET', 'PAK', 160000.000, -6.000, '1', 9.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 160000.000, 12001, 61002, 61002, 61002, 61002, 61002, 53002, 61002, 0),
(64, '00057', 3, 'TEH SASET', 'PAK', 0.000, -1.000, '1', 3.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 95000.000, 12001, 61002, 61002, 61002, 61002, 61002, 53002, 61002, 0),
(65, '00058', 3, 'CREAM SASET', 'PAK', 75000.000, -2.000, '1', 4.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 75000.000, 12001, 61002, 61002, 61002, 61002, 61002, 53001, 61002, 0),
(66, '00059', 3, 'KOPI BEAN LARISHA', 'PCS', 62500.000, -2.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 18.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 62500.000, 12001, 61002, 61002, 61002, 61002, 61002, 53001, 61002, 0),
(67, '00060', 3, 'JAM HONEY', 'PAK', 197363.707, 4.000, '1', 0.000, 0.000, 21.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 197376.000, 12001, 61002, 61002, 61002, 61002, 61002, 53001, 61002, 0),
(68, '00061', 3, 'JAM STRAWBERRY', 'PAK', 115750.000, -5.000, '1', 0.000, 0.000, 22.000, 2.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 115750.000, 12001, 61002, 61002, 61002, 61002, 61002, 61002, 61002, 0),
(69, '00062', 3, 'JAM ORANGE', 'PAK', 109125.000, 3.000, '1', 0.000, 0.000, 7.000, 1.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 109125.000, 12001, 61002, 61002, 61002, 61002, 61002, 53001, 61002, 0),
(70, '00063', 3, 'JAM PINEPLE', 'PAK', 109125.000, 6.000, '1', 0.000, 0.000, 4.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 109125.000, 12001, 61002, 61002, 61002, 61002, 61002, 53001, 61002, 0),
(71, '00064', 3, 'GULA TROPICANA SLIM', 'PAK', 27500.000, -30.000, '1', 0.000, 0.000, 26.000, 4.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 27500.000, 12001, 61002, 61002, 61002, 61002, 61002, 53001, 61002, 0),
(72, '00065', 3, 'ANCHOVIES', 'PCS', 65000.000, 0.000, '1', 0.000, 0.000, 2.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 60000.000, 12001, 61002, 61002, 61002, 61002, 61002, 53001, 61002, 0),
(73, '00066', 3, 'BEAK BEAN', 'PCS', 19500.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 19500.000, 12001, 61002, 61002, 61002, 61002, 61002, 53001, 61002, 0),
(74, '00067', 3, 'BLACK OLIVE', 'BTL', 35000.000, 3.000, '1', 0.000, 0.000, 3.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 35000.000, 12001, 61002, 61002, 61002, 61002, 61002, 53001, 61002, 0),
(75, '00068', 3, 'JAGUNG KALENG', 'CAN', 15500.000, 21.000, '1', 0.000, 0.000, 22.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 15590.000, 12001, 61002, 61002, 61002, 61002, 61002, 53001, 61002, 0),
(76, '00069', 3, 'SUSU DIAMOND', 'PAK', 15345.354, -17.000, '1', 0.000, 0.000, 20.000, 2.000, NULL, 16.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 1, 0, 0, 0, 15640.000, 12001, 61002, 61002, 61002, 61002, 61002, 53001, 61002, 0),
(77, '00070', 3, 'MIHUN JAGUNG', 'PCS', 6000.000, 38.000, '1', 0.000, 0.000, 51.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 6000.000, 12001, 61002, 61002, 61002, 61002, 61002, 53001, 61002, 0),
(78, '00071', 3, 'KWETIAU', 'PCS', 16500.000, 23.000, '1', 0.000, 0.000, 2.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 16500.000, 12001, 61002, 61002, 61002, 61002, 61002, 53001, 61002, 0),
(79, '00072', 4, 'BUKU TULIS BESAR', 'PCS', 11200.000, 11.000, '1', 1.000, 0.000, 0.000, 2.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 11200.000, 12001, 61002, 61002, 61002, 61002, 61002, 61002, 58005, 0),
(80, '00073', 4, 'BUKU TULIS KECIL', 'PCS', 6200.000, 13.000, '1', 0.000, 0.000, 0.000, 1.000, NULL, 1.000, 0.000, 0.000, 1.000, 0, 0, 0, 0, 0, 0, 0, 0, 6200.000, 12001, 61005, 61005, 61005, 61005, 61005, 61005, 61005, 0),
(81, '00074', 4, 'KERTAS HVS', 'RIM', 59500.000, -2.000, '1', 0.000, 4.000, 0.000, 4.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 59500.000, 12001, 61005, 61005, 61005, 61005, 61005, 61005, 61005, 0),
(82, '00075', 4, 'KERTAS  HVS  A4', 'RIM', 41400.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 41400.000, 12001, 61002, 61002, 61002, 61002, 61002, 61002, 58005, 0),
(83, '00076', 4, 'STEPLER', 'PCS', 12000.000, 2.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 12000.000, 12001, 51003, 61002, 61002, 61002, 61002, 54003, 58005, 0),
(84, '00077', 4, 'ISI STEPLES', 'PAK', 2450.000, 1.000, '1', 0.000, 3.000, 0.000, 26.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 2450.000, 12001, 51003, 61002, 61002, 61002, 61002, 54003, 58005, 0),
(85, '00078', 4, 'KLIP', 'PAK', 4000.000, -2.000, '1', 0.000, 2.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 51003, 61002, 61002, 61002, 61002, 54003, 58005, 0),
(86, '00079', 4, 'TIP X', 'PCS', 7800.000, 6.000, '1', 0.000, 0.000, 0.000, 3.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 7800.000, 12001, 51003, 61002, 61002, 61002, 61002, 54003, 58005, 0),
(87, '00080', 4, 'KWITANSI', 'PAD', 7500.000, 41.000, '1', 0.000, 5.000, 0.000, 1.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 7500.000, 12001, 51003, 61001, 61001, 61001, 61001, 61001, 58005, 0),
(88, '00081', 4, 'MEMO', 'PAD', 10000.000, 19.000, '1', 1.000, 8.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 10000.000, 12001, 51003, 61001, 61001, 61001, 61001, 61001, 58005, 0),
(89, '00082', 4, 'BILL RESTAURANT', 'PAD', 12000.000, 115.000, '1', 0.000, 0.000, 0.000, 5.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 12000.000, 12001, 61001, 61001, 61001, 61001, 61001, 61001, 61001, 0),
(90, '00083', 4, 'RESTAURANT ORDER', 'PAD', 12000.000, -17.000, '1', 0.000, 0.000, 0.000, 17.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 12000.000, 12001, 61005, 61005, 61005, 61005, 61005, 54001, 61005, 0),
(91, '00084', 4, 'SUMMARY', 'PAD', 20000.000, 1.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 22000.000, 12001, 61001, 61001, 61001, 61001, 61001, 54003, 61001, 0),
(92, '00085', 4, 'REQ BARANG MATERIAL', 'PAD', 12000.000, 8.000, '1', -1.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 12000.000, 12001, 61001, 61001, 61001, 61001, 61001, 61001, 58005, 0),
(93, '00086', 4, 'REQ. BARANG FOOD', 'PAD', 12000.000, 16.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 12000.000, 12001, 61001, 61001, 61001, 61001, 61001, 61001, 58005, 0),
(94, '00087', 4, 'REQ. BARANG MINUMAN', 'PAD', 12000.000, 9.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 12000.000, 12001, 61001, 61001, 61001, 61001, 61001, 61001, 58005, 0),
(95, '00088', 4, 'FORMULIR A', 'PAD', 17500.000, 20.000, '1', 0.000, 9.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 17500.000, 52001, 61001, 61001, 61001, 61001, 61001, 61001, 61001, 0),
(96, '00089', 4, 'TISSUE NAPKIN', 'PCS', 101040.172, -312.000, '1', 0.000, 0.000, 446.000, 10.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 200000.000, 52001, 61001, 61001, 61001, 61001, 61001, 61001, 61001, 0),
(97, '00090', 4, 'TISSUE TOILET', 'BOX', 275000.000, -2.000, '1', 23.000, 0.000, 1.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 285000.000, 52001, 61001, 61001, 61001, 61001, 61001, 61001, 61001, 0),
(98, '00091', 4, 'TISSUE KOTAK', 'PCS', 4500.000, 72.000, '1', 72.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 4500.000, 52001, 61001, 61001, 61001, 61001, 61001, 61001, 61001, 0),
(99, '00092', 4, 'TISSUE DINNER', 'PCS', 10666.000, -61.000, '1', 10.000, 0.000, 0.000, 3.000, NULL, 86.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 320000.000, 12001, 61001, 61001, 61001, 61001, 61001, 54003, 61001, 0),
(100, '00093', 4, 'SHOWER CAP', 'PAK', 90000.000, 5.000, '1', 4.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 90000.000, 52001, 61005, 61005, 61005, 61005, 61005, 61005, 61005, 0),
(101, '00094', 4, 'GLASS BAG', 'PAK', 30000.000, 2.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 30000.000, 52001, 61005, 61005, 61005, 61005, 61005, 61005, 61005, 0),
(102, '00095', 4, 'LOUNDRY BAG(200pcs)', 'PCS', 1000.000, -101.000, '1', 101.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52001, 61005, 61005, 61005, 61005, 61005, 61005, 61005, 0),
(103, '00096', 4, 'SABUN', 'PCS', -11957.447, -297.000, '1', 300.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 7000.000, 52001, 61001, 61001, 61001, 61001, 61001, 61001, 61001, 0),
(104, '00097', 4, 'STERNO', 'PCS', 4300.000, -208.000, '1', 0.000, 0.000, 625.000, 15.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 430000.000, 12001, 61001, 61001, 61001, 61001, 61001, 61001, 54003, 0),
(105, '00098', 4, 'KATIK SATE', 'PAK', 16000.000, 2.000, '1', 0.000, 0.000, 23.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 16000.000, 12001, 61001, 61001, 61001, 61001, 61001, 61001, 54003, 0),
(106, '00099', 4, 'KOREK', 'PCS', 1500.000, -100.000, '1', 0.000, 0.000, 300.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 1500.000, 52003, 61001, 61001, 61001, 61001, 61001, 61001, 54003, 0),
(107, '00100', 4, 'BAY FRESH', 'PCS', 33166.000, -20.000, '1', 68.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 33166.000, 52001, 61001, 61001, 61001, 61001, 61001, 54003, 61001, 0),
(108, '00101', 4, 'BAYGON', 'PCS', 39000.000, 2.000, '1', 66.000, 0.000, 1.000, 0.000, NULL, 2.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 39000.000, 52003, 61001, 61001, 61001, 61001, 61001, 61001, 54003, 0),
(109, '00102', 4, 'SHINE WOOD', 'GLN', 115000.000, 3.000, '1', 5.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 115000.000, 52001, 61001, 61001, 61001, 61001, 61001, 61001, 61001, 0),
(110, '00103', 4, 'MEMORANDUM INVOICE', 'PAD', 12500.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 12500.000, 12001, 61001, 61001, 61001, 61001, 61001, 61001, 58005, 0),
(111, '00104', 4, 'AMPLOP LOGO', 'PAK', 75000.000, -2.000, '1', 0.000, 2.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 75000.000, 12001, 51003, 61001, 61001, 61001, 61001, 61001, 61001, 0),
(112, '00105', 4, 'AMPLOP BIASA', 'PAK', 15700.000, 2.000, '1', 0.000, 1.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 15700.000, 12001, 58005, 61001, 61001, 61001, 61001, 61001, 61001, 0),
(113, '00106', 4, 'LILIN', 'PAK', 15400.000, -36.000, '1', 0.000, 0.000, 0.000, 3.000, NULL, 40.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 14000.000, 12001, 61001, 61001, 61001, 61001, 61001, 61001, 54003, 0),
(114, '00107', 4, 'PIPET', 'PAK', 38500.000, -7.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 9.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 38500.000, 12001, 61001, 61001, 61001, 61001, 61001, 61001, 54003, 0),
(115, '00108', 4, 'PLASTIK SAMPAH', 'PAK', 37500.000, -379.000, '1', 52.000, 0.000, 323.000, 23.000, NULL, 0.000, 0.000, 7.000, 0.000, 0, 0, 0, 1, 0, 0, 0, 0, 150000.000, 52003, 61001, 61001, 61001, 61001, 61001, 54003, 61001, 0),
(116, '00109', 4, 'PLASTIK WRAP', 'ROOL', 188000.000, -5.000, '1', 0.000, 0.000, 6.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 188000.000, 12001, 61001, 61001, 61001, 61001, 61001, 61001, 54003, 0),
(117, '00110', 4, 'SUNLIGHT', 'PCS', 44770.202, -149.000, '1', 19.000, 0.000, 207.000, 4.000, NULL, 0.000, 0.000, 0.000, 0.000, 1, 0, 1, 0, 0, 0, 0, 0, 76000.000, 52001, 61001, 61001, 61001, 61001, 61001, 61001, 54003, 0),
(118, '00111', 4, 'SAPU LANTAI', 'PCS', 30000.000, 0.000, '1', 3.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 30000.000, 52001, 61001, 61001, 61001, 61001, 61001, 61001, 54003, 0),
(119, '00112', 4, 'SIKAT LANTAI', 'PCS', 75000.000, 2.000, '1', 1.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 75000.000, 52001, 61001, 61001, 61001, 61001, 61001, 61001, 54003, 0),
(120, '00113', 4, 'TOILET BOLL', 'PCS', 30000.000, 9.000, '1', 2.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 30000.000, 52001, 61001, 61001, 61001, 61001, 61001, 61001, 61001, 0),
(121, '00114', 4, 'DUSH PAN', 'PCS', 30000.000, 0.000, '1', 2.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 30000.000, 52001, 61001, 61001, 61001, 61001, 61001, 61001, 54003, 0),
(122, '00115', 4, 'EMBER KECIL', 'PCS', 30000.000, 13.000, '1', 1.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 30000.000, 52001, 61001, 61001, 61001, 61001, 61001, 61001, 61001, 0),
(123, '00116', 4, 'FANTASTIK', 'PAIL', 272000.000, -1.000, '1', 5.000, 0.000, 4.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 272000.000, 52001, 61001, 61001, 61001, 61001, 61001, 61001, 54003, 0),
(124, '00117', 4, 'FLOOR CLEAN', 'GLN', 330000.000, 0.000, '1', 1.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 330000.000, 52001, 61001, 61001, 61001, 61001, 61001, 61001, 54003, 0),
(125, '00118', 4, 'M P C', 'GLN', 46250.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 52001, 61001, 61001, 61001, 61001, 61001, 61001, 61001, 0),
(126, '00119', 4, 'EXXED', 'CAN', 125000.000, 2.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 61001, 61001, 61001, 61001, 61001, 61001, 61001, 0),
(127, '00120', 4, 'BATERAI AAA', 'PCS', 11760.000, -10.000, '1', 0.000, 1.000, 4.000, 0.000, NULL, 0.000, 0.000, 0.000, 9.000, 0, 0, 1, 0, 0, 0, 0, 0, 2840.000, 12001, 61001, 61001, 58001, 61001, 61005, 61001, 61001, 0),
(128, '00121', 4, 'BATERAI AA', 'PCS', 9679.500, -11.000, '1', 0.000, 0.000, 8.000, 0.000, NULL, 0.000, 0.000, 0.000, 11.000, 0, 0, 1, 0, 0, 0, 0, 0, 1930.000, 12001, 61001, 61001, 58001, 61001, 61001, 61001, 61001, 0),
(129, '00122', 4, 'BATERAI BESAR', 'PCS', 9000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 61001, 61001, 58001, 61001, 61001, 61001, 61001, 0),
(130, '00123', 4, 'ELEMENT 400W', 'PCS', 275000.000, 10.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 275000.000, 12001, 61001, 61001, 61001, 61001, 61005, 61001, 61001, 0),
(131, '00124', 4, 'ELEMENT 500W', 'PCS', 275000.000, -10.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 10.000, 0, 0, 0, 0, 0, 0, 0, 0, 275000.000, 12001, 61001, 61001, 61005, 61001, 61001, 61001, 61001, 0),
(132, '00125', 4, 'BALON CLEAR 15 W', 'PCS', 7000.000, 50.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 7000.000, 12001, 61001, 61001, 61001, 61001, 61005, 61001, 61001, 0),
(133, '00126', 4, 'BALON CLEAR 25 W', 'PCS', 7000.000, 50.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 7000.000, 12001, 61001, 61001, 61001, 61001, 61005, 61001, 61001, 0),
(134, '00127', 4, 'BALON CLEAR 40 W', 'PCS', 15090.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 15090.000, 12001, 61001, 61001, 61001, 61001, 61005, 61001, 61001, 0),
(135, '00128', 4, 'BALON CLEAR 80 W', 'PCS', 0.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 61001, 61001, 61001, 61001, 61005, 61001, 61001, 0),
(136, '00129', 4, 'PHILIPS 150 W', 'PCS', 22800.000, 10.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 22800.000, 12001, 61001, 61001, 61001, 61001, 61005, 61001, 61001, 0),
(137, '00130', 4, 'PHILIPS 300 W', 'PCS', 27000.000, 7.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 27000.000, 12001, 61001, 61001, 61001, 61001, 61005, 61001, 61001, 0),
(138, '00131', 4, 'NEON 18 W', 'PCS', 11550.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 11550.000, 12001, 61001, 61001, 61001, 61001, 61005, 61001, 61001, 0),
(139, '00132', 4, 'TORNADO 5 W', 'PCS', 41000.000, 9.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 14.000, 0, 0, 0, 0, 0, 0, 0, 0, 41000.000, 12001, 61001, 61001, 61001, 61001, 61005, 61001, 61001, 0),
(140, '00133', 4, 'TORNADO 15 W', 'PCS', 45000.000, 16.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 32.000, 0, 0, 0, 0, 0, 0, 0, 0, 45000.000, 12001, 61001, 61001, 61001, 61001, 61005, 61001, 61001, 0),
(141, '00134', 4, 'SEQUIS LANTAI', 'PCS', 75000.000, 7.000, '1', 1.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 75000.000, 52001, 61001, 61001, 61001, 61001, 61001, 61001, 61001, 0),
(142, '00135', 4, 'AMPLOP', 'PAK', 15700.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 15700.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(143, '00136', 4, 'AMPLOP LOGO', 'PAK', 75000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 75000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(144, '00137', 4, 'PHILIPS 80 W', 'PCS', 136700.000, 3.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 136700.000, 12001, 61001, 61001, 61001, 61001, 61005, 61001, 61001, 0),
(145, '00138', 4, 'PHILIPS 40 W', 'PCS', 17100.000, 12.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 12.000, 0, 0, 0, 0, 0, 0, 0, 0, 17100.000, 12001, 61001, 61001, 61001, 61001, 61005, 61001, 61001, 0),
(146, '00139', 3, 'TABASCO', 'BTL', 87790.000, 25.000, '1', 0.000, 0.000, 1.000, 0.000, NULL, 1.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 87790.000, 12001, 61001, 61001, 61001, 61001, 61001, 53001, 61001, 0),
(147, '00140', 3, 'JAM PINEAPLE', 'PAK', 101875.000, 2.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 101875.000, 12001, 61001, 61001, 61001, 61001, 61001, 53001, 61001, 0),
(148, '00141', 3, 'JAM ORANGE', 'PAK', 109125.000, 2.000, '1', 0.000, 0.000, 1.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 101875.000, 12001, 61001, 61001, 61001, 61001, 61001, 53001, 61001, 0),
(149, '00142', 3, 'BUTTER MINI', 'PAK', 185000.000, -6.000, '1', 0.000, 0.000, 18.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 185000.000, 12001, 61001, 61001, 61001, 61001, 61001, 53001, 61001, 0),
(150, '00143', 1, 'SEMANGKA', 'KG', 7000.000, 19.000, '1', 0.000, 0.000, 3094.000, 8.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 7000.000, 12001, 61001, 61001, 61001, 61001, 61001, 53001, 61001, 0),
(151, '00144', 4, 'PURCHASING ORDER', 'PAD', 15000.000, 8.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 15000.000, 12001, 61001, 61001, 61001, 61001, 61001, 61001, 58005, 0),
(152, '00145', 4, 'MEMORANDUM INVOICE', 'PAD', 12500.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 12500.000, 12001, 61001, 61001, 61001, 61001, 61001, 61001, 58005, 0),
(153, '00146', 4, 'REQ MATERIAL', 'PAD', 12000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 12000.000, 12001, 61001, 61001, 61001, 61001, 61001, 61001, 58005, 0),
(154, '00147', 4, 'LAUNDRY BAG', 'PCS', 900.000, -200.000, '1', 200.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 900.000, 52001, 61001, 61001, 61001, 61001, 61001, 61001, 61001, 0),
(155, '00148', 4, 'SABUN', 'PCS', 1000.000, -900.000, '1', 900.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52001, 61001, 61001, 61001, 61001, 61001, 61001, 61001, 0),
(156, '00149', 4, 'GLASS CLEANER', 'GLN', 46250.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 46250.000, 52001, 61001, 61001, 61001, 61001, 61001, 61001, 61001, 0),
(157, '00150', 5, 'LARGE BINTANG', 'KRAT', 378424.930, 9.000, '1', 0.000, 0.000, 1.000, 0.000, NULL, 2.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 1, 0, 0, 0, 380200.000, 12001, 61001, 61001, 61001, 61001, 61001, 53002, 61001, 0),
(158, '00151', 5, 'SMALL BINTANG', 'KRT', 320650.000, 15.000, '1', 0.000, 0.000, 24.000, 0.000, NULL, 2.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 1, 0, 0, 0, 318200.000, 12001, 61001, 61001, 61001, 61001, 61001, 53002, 61001, 0),
(159, '00152', 5, 'SMALL HENEIKEN', 'KRT', 440200.000, -1.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 2.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 440200.000, 12001, 61001, 61001, 61001, 61001, 61001, 53002, 61001, 0),
(160, '00153', 5, 'COCA COLA', 'DZ', 110000.000, -18.000, '1', 0.000, 0.000, 0.000, 1.000, NULL, 23.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 110000.000, 12001, 61005, 61005, 61005, 61005, 61005, 53002, 61005, 0),
(161, '00154', 5, 'FANTA', 'DZ', 110000.000, -1.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 3.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 110000.000, 12001, 61005, 61005, 61005, 61005, 61005, 53002, 61005, 0),
(162, '00155', 5, 'SPRITE', 'DZ', 110000.000, -6.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 7.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 110000.000, 12001, 61005, 61005, 61005, 61005, 61005, 53002, 61005, 0),
(163, '00156', 5, 'DIET COKE', 'DZ', 110000.000, -5.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 7.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 110000.000, 12001, 61005, 61005, 61005, 61005, 61005, 53002, 61005, 0),
(164, '00157', 5, 'SODA WATER', 'DZ', 110000.000, -7.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 9.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 110000.000, 12001, 61005, 61005, 61005, 61005, 61005, 53002, 61005, 0),
(165, '00158', 5, 'TONIC WATER', 'DZ', 110000.000, -1.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 4.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 110000.000, 12001, 61005, 61005, 61005, 61005, 61005, 53002, 61005, 0),
(166, '00159', 5, 'SMALL AQUA', 'DZ', 40350.000, -47.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 47.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 40350.000, 12001, 61005, 61005, 61005, 61005, 61005, 53002, 61005, 0),
(167, '00160', 5, 'SUNQUICK', 'GLN', 145000.000, 0.000, '1', 0.000, 0.000, 5.000, 0.000, NULL, 10.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 145000.000, 12001, 61005, 61005, 61005, 61005, 61005, 53002, 61005, 0),
(168, '00161', 5, 'AGA RED BOTOL', 'BTL', 132000.000, 15.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 5.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 132000.000, 12001, 61005, 61005, 61005, 61005, 61005, 53003, 61005, 0),
(169, '00162', 5, 'AGA RED CASK', 'CASK', 254100.000, 1.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 7.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 254100.000, 12001, 61005, 61005, 61005, 61005, 61005, 53003, 61005, 0),
(170, '00163', 5, 'AGA WHITE BTL', 'BTL', 132000.000, -4.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 30.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 132000.000, 12001, 61005, 61005, 61005, 61005, 61005, 53003, 61005, 0),
(171, '00164', 5, 'AGA WHITE CASK', 'CASK', 254100.000, 5.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 17.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 254100.000, 12001, 61005, 61005, 61005, 61005, 61005, 53003, 61005, 0),
(172, '00165', 5, 'ROSE BOTOL', 'BTL', 126500.000, 6.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 14.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 126500.000, 12001, 61005, 61005, 61005, 61005, 61005, 53003, 61005, 0),
(173, '00166', 5, 'ROSE CASK', 'CASK', 248600.000, 3.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 15.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 248600.000, 12001, 61005, 61005, 61005, 61005, 61005, 53003, 61005, 0),
(174, '00167', 5, 'CARBENET MERLOT', 'BTL', 165000.000, 10.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 2.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 165000.000, 12001, 61005, 61005, 61005, 61005, 61005, 53003, 61005, 0),
(175, '00168', 5, 'TWO ISLAND  SHIRAZ', 'BTL', 189750.000, -4.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 4.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 189750.000, 12001, 61005, 61005, 61005, 61005, 61005, 53003, 61005, 0),
(176, '00169', 5, 'TWO ISLAND CHARDONAY', 'BTL', 189750.000, 9.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 21.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 189750.000, 12001, 61005, 61005, 61005, 61005, 61005, 53003, 61005, 0),
(177, '00170', 5, 'WINE TUNJUNG', 'BTL', 200200.000, 7.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 200200.000, 12001, 61005, 61005, 61005, 61005, 61005, 53003, 61005, 0),
(178, '00171', 5, 'WINE JEPUN', 'BTL', 177100.000, 7.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 177100.000, 12001, 61005, 61005, 61005, 61005, 61005, 53003, 61005, 0),
(179, '00172', 5, 'RESLING WINE', 'BTL', 189750.000, 5.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 1.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 189750.000, 12001, 61005, 61005, 61005, 61005, 61005, 53003, 61005, 0),
(180, '00173', 5, 'BLM TRIPLE SEX', 'BTL', 165000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 1.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 165000.000, 12001, 61005, 61005, 61005, 61005, 61005, 53003, 61005, 0),
(181, '00174', 5, 'BLM BANANA', 'BTL', 165000.000, 5.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 165000.000, 12001, 61005, 61005, 61005, 61005, 61005, 53003, 61005, 0),
(182, '00175', 5, 'BLM MELON', 'BTL', 165000.000, 2.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 3.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 165000.000, 12001, 61005, 61005, 61005, 61005, 61005, 53003, 61005, 0),
(183, '00176', 5, 'BLM PINEAPLE', 'BTL', 165000.000, 2.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 1.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 165000.000, 12001, 61005, 61005, 61005, 61005, 61005, 53003, 61005, 0),
(184, '00177', 5, 'BLM COCONUT', 'BTL', 165000.000, 6.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 2.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 165000.000, 12001, 61005, 61005, 61005, 61005, 61005, 53003, 61005, 0),
(185, '00178', 5, 'BLM VODCA', 'BTL', 165000.000, -1.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 12.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 165000.000, 12001, 61005, 61005, 61005, 61005, 61005, 53003, 61005, 0),
(186, '00179', 5, 'BLM BLUE CURACAU', 'BTL', 165000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 165000.000, 12001, 61005, 61005, 61005, 61005, 61005, 53003, 61005, 0),
(187, '00180', 5, 'ARAK', 'BTL', 50000.000, -6.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 9.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 50000.000, 12001, 61005, 61005, 61005, 61005, 61005, 53003, 61005, 0),
(188, '00181', 5, 'SYRUP COCOPANDAN', 'BTL', 25910.868, 9.000, '1', 0.000, 0.000, 5.000, 0.000, NULL, 11.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 27500.000, 12001, 61005, 61005, 61005, 61005, 61005, 53002, 61005, 0),
(189, '00182', 5, 'BARCA RUM', 'BTL', 160000.000, 12.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 12.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 160000.000, 12001, 61005, 61005, 61005, 61005, 61005, 53003, 61005, 0),
(190, '00183', 5, 'DRY GIN', 'BTL', 160000.000, 3.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 7.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 160000.000, 12001, 61005, 61005, 61005, 61005, 61005, 53003, 61005, 0),
(191, '00184', 5, 'PINEAPLE CONCT. 1 LTR', 'BTL', 50000.000, 1.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 9.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 50000.000, 12001, 61005, 61005, 61005, 61005, 61005, 53003, 61005, 0),
(192, '00185', 5, 'LEMEN CONCTRAT', 'BTL', 43000.000, 2.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 43000.000, 12001, 61005, 61005, 61005, 61005, 61005, 53003, 61005, 0),
(193, '00186', 4, 'KIT BLACK', 'BTL', 24900.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 24900.000, 12001, 51003, 61005, 61005, 61005, 61005, 61005, 61005, 0),
(194, '00187', 4, 'MEMORANDUM INVOICE', 'PAD', 12000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 12000.000, 12001, 58005, 61005, 61005, 61005, 61005, 61005, 61005, 0),
(195, '00188', 4, 'REQ MINUMAN', ' PAD', 12000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 12000.000, 12001, 61005, 61005, 61005, 61005, 61005, 61005, 58005, 0),
(196, '00189', 4, 'REQ FOOD', 'PAD', 12000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 12000.000, 12001, 61005, 61005, 61005, 61005, 61005, 61005, 58005, 0),
(197, '00190', 4, 'REQ MATERIAL', ' PAD', 12000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 12000.000, 12001, 61005, 61005, 61005, 61005, 61005, 61005, 58005, 0),
(198, '00191', 4, 'SUMMARY', ' PAD', 20000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 20000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(199, '00192', 4, 'REST ORDER', ' PAD', 10000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 10000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(200, '00193', 1, 'NANAS', 'PCS', 7000.000, 85.000, '1', 0.000, 0.000, 2060.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 7000.000, 12001, 61005, 61005, 61005, 61005, 61005, 53001, 61005, 0),
(201, '00194', 1, 'PISANG RANTI', 'SISIR', 20000.000, 8.000, '1', 0.000, 0.000, 647.000, 6.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 20000.000, 12001, 61005, 61005, 61005, 61005, 61005, 53001, 61005, 0),
(202, '00195', 1, 'PISANG TANDUK', 'KG', 9995.457, 90.000, '1', 0.000, 0.000, 928.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 10000.000, 12001, 61005, 61005, 61005, 61005, 61005, 53001, 61005, 0),
(203, '00196', 1, 'PEPAYA', 'KG', 7000.000, 74.000, '1', 0.000, 0.000, 2073.000, 8.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 7000.000, 12001, 61005, 61005, 61005, 61005, 61005, 53001, 61005, 0),
(204, '00197', 1, 'LIME', 'KG', 25000.000, -10.000, '1', 0.000, 0.000, 257.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 25000.000, 12001, 61005, 61005, 61005, 61005, 61005, 53001, 61005, 0),
(205, '00198', 2, 'WORTEL', 'KG', 35000.000, 15.000, '1', 0.000, 0.000, 511.244, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 35000.000, 12001, 61005, 61005, 61005, 61005, 61005, 53001, 61005, 0),
(206, '00199', 2, 'TOMAT', 'KG', 12000.000, 30.000, '1', 0.000, 0.000, 921.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 12000.000, 12001, 61005, 61005, 61005, 61005, 61005, 53001, 61005, 0),
(207, '00200', 2, 'BAYAM', 'IKAT', 12000.000, 2.000, '1', 0.000, 0.000, 18.500, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 12000.000, 12001, 61005, 61005, 61005, 61005, 61005, 53001, 61005, 0),
(208, '00201', 2, 'BUNGA KOL', 'KG', 35000.000, 3.000, '1', 0.000, 0.000, 158.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 35000.000, 12001, 61005, 61005, 61005, 61005, 61005, 53001, 61005, 0),
(209, '00202', 2, 'SELADA BULAT', 'KG', 30000.000, 4.000, '1', 0.000, 0.000, 221.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 30000.000, 12001, 61005, 61005, 61005, 61005, 61005, 53001, 61005, 0),
(210, '00203', 2, 'SELADA KERITING', 'KG', 30000.000, -2.000, '1', 0.000, 0.000, 84.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 30000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(211, '00204', 2, 'TOMAT CHERY', 'KG', 40000.000, -4.500, '1', 0.000, 0.000, 24.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 40000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(212, '00205', 3, 'KENTANG KULIT', 'KG', 20000.000, 35.000, '1', 0.000, 0.000, 599.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 20000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(213, '00206', 2, 'KOL', 'KG', 10000.000, 27.000, '1', 0.000, 0.000, 481.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 10000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(214, '00207', 2, 'RUMANA', 'KG', 30000.000, -1.000, '1', 0.000, 0.000, 93.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 30000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(215, '00208', 2, 'BUNCIS', 'KG', 12000.000, 10.000, '1', 0.000, 0.000, 213.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 12000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(216, '00209', 2, 'RADISIO', 'KG', 30000.000, -11.000, '1', 0.000, 0.000, 39.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 30000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(217, '00210', 2, 'SAWI HIJAU', 'KG', 12000.000, -22.000, '1', 0.000, 0.000, 265.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 12000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(218, '00211', 1, 'AVOCADO', 'KG', 25000.000, 17.000, '1', 0.000, 0.000, 86.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 25000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(219, '00212', 2, 'LOBAK', 'KG', 12000.000, 14.000, '1', 0.000, 0.000, 185.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 12000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(220, '00213', 2, 'KACANG PANJANG', 'KG', 12000.000, 1.000, '1', 0.000, 0.000, 37.500, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 12000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(221, '00214', 2, 'TIMUN JEPANG', 'KG', 12000.000, 5.000, '1', 0.000, 0.000, 262.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 12000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(222, '00215', 1, 'MANGGA', 'KG', 45000.000, -5.000, '1', 0.000, 0.000, 116.038, 0.000, NULL, 6.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 45000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(223, '00216', 2, 'PARSLEY', 'KG', 30000.000, -76.500, '1', 0.000, 0.000, 94.500, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 30000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(224, '00217', 2, 'BASIL', 'KG', 60000.000, -52.250, '1', 0.000, 0.000, 61.750, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 60000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(225, '00218', 2, 'CORIANDER', 'KG', 60000.000, -52.000, '1', 0.000, 0.000, 65.750, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 60000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(226, '00219', 2, 'UTIK-UTIK', 'KG', 20000.000, -6.000, '1', 0.000, 0.000, 24.750, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 20000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(227, '00220', 2, 'JAGUNG MANIS', 'BKS', 10000.000, 0.000, '1', 0.000, 0.000, 54.000, 6.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 1, 0, 0, 0, 0, 10000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(228, '00221', 2, 'PAPRIKA MERAH', 'KG', 50000.000, -1.000, '1', 0.000, 0.000, 77.500, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 50000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(229, '00222', 2, 'PAPRIKA KUNING', 'KG', 50000.000, -2.000, '1', 0.000, 0.000, 78.500, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 50000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(230, '00223', 2, 'PAPRIKA HIJAU', 'KG', 30000.000, 0.000, '1', 0.000, 0.000, 130.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 30000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(231, '00224', 3, 'BAWANG MERAH', 'KG', 30000.000, 2.000, '1', 0.000, 0.000, 28.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 30000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(232, '00225', 3, 'BAWANG PUTIH', 'KG', 30000.000, 6.000, '1', 0.000, 0.000, 91.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 30000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(233, '00226', 3, 'BAWANG GORENG', 'KG', 150000.000, -3.500, '1', 0.000, 0.000, 36.500, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 150000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(234, '00227', 3, 'BOMBAY', 'KG', 20000.000, 13.000, '1', 0.000, 0.000, 210.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 20000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(235, '00228', 2, 'SELEDRI', 'KG', 30000.000, -100.000, '1', 0.000, 0.000, 114.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 30000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0);
INSERT INTO `tbitem` (`iditem`, `codeitem`, `idsubcategory`, `description`, `unit`, `price`, `stok`, `isaktif`, `stokHK`, `stokFO`, `stokFB`, `stokBO`, `idvendor`, `stokBar`, `stokSec`, `stokPG`, `stokEng`, `isHK`, `isFO`, `isFB`, `isBO`, `isBar`, `isSec`, `isPG`, `isEng`, `lastprice`, `akunHK`, `akunFO`, `akunBar`, `akunSec`, `akunPG`, `akunEng`, `akunFB`, `akunBO`, `isprosesopname`) VALUES
(236, '00229', 3, 'LOMBOK KECIL', 'KG', 49550.010, -54.250, '1', 0.000, 0.000, 84.250, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 50000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(237, '00230', 3, 'LOMBOK BESAR', 'KG', 20000.000, 5.000, '1', 0.000, 0.000, 122.500, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 20000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(238, '00231', 3, 'TEMPE', 'PAK', 3000.000, 8.000, '1', 0.000, 0.000, 413.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 3000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(239, '00232', 3, 'TAHU', 'PCS', 1000.000, 20.000, '1', 0.000, 0.000, 810.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(240, '00233', 3, 'KENTANG CRINKLE', 'KG', 37950.000, -3.000, '1', 0.000, 0.000, 18.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 37950.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(241, '00234', 1, 'STRAWBERY FRUIT', 'KG', 53333.333, 0.000, '1', 0.000, 0.000, 15.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 55000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(242, '00235', 3, 'PERE', 'KG', 30000.000, 21.000, '1', 0.000, 0.000, 79.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 30000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(243, '00236', 3, 'SEREH', 'IKAT', 10000.000, 1.000, '1', 0.000, 0.000, 69.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 10000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(244, '00237', 3, 'MERICA BUBUK', 'KG', 90000.000, -100.000, '1', 0.000, 0.000, 112.250, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 90000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(245, '00238', 3, 'GULA MERAH', 'KG', 20000.000, 0.000, '1', 0.000, 0.000, 17.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 20000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(246, '00239', 3, 'TERASI', 'PAK', 5000.000, 3.000, '1', 0.000, 0.000, 44.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 5000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(247, '00240', 2, 'DAUN MINT', 'PAK', 5938.000, -8.000, '1', 0.000, 0.000, 22.300, 0.000, NULL, 6.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 7000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(248, '00241', 3, 'KUNYIT', 'KG', 7500.000, 1.500, '1', 0.000, 0.000, 11.750, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 10000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(249, '00242', 3, 'ISEN', 'KG', 10000.000, -0.500, '1', 0.000, 0.000, 12.500, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 10000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(250, '00243', 3, 'JAHE', 'KG', 20000.000, 1.000, '1', 0.000, 0.000, 16.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 20000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(251, '00244', 3, 'KETAN', 'KG', 25000.000, 1.000, '1', 0.000, 0.000, 12.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 25000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(252, '00245', 2, 'LEEK', 'KG', 30000.000, -2.000, '1', 0.000, 0.000, 46.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 30000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(253, '00246', 1, 'MELON', 'KG', 15000.000, 0.000, '1', 0.000, 0.000, 240.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 15000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(254, '00247', 3, 'DAGING BABI', 'KG', 65000.000, 6.000, '1', 0.000, 0.000, 355.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 65000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(255, '00248', 3, 'DAGING SAPI', 'KG', 150000.000, 27.000, '1', 0.000, 0.000, 411.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 150000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(256, '00249', 3, 'DAGING AYAM FILLET', 'KG', 56000.000, -90.000, '1', 0.000, 0.000, 272.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 56000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(257, '00250', 3, 'DAGING CINCANG', 'KG', 85500.000, -5.000, '1', 0.000, 0.000, 30.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 85500.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(258, '00251', 3, 'STREAKY BACON', 'KG', 71250.000, -10.000, '1', 0.000, 0.000, 150.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 71250.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(259, '00252', 3, 'SMOKE HAM', 'KG', 79230.000, 6.000, '1', 0.000, 0.000, 37.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 79230.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(260, '00253', 4, 'AC LG 1PK', 'PCS', 4250000.000, -4.000, '1', 0.000, 0.000, 20.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 66120.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(261, '00254', 3, 'CHICK.NUGGET', 'KG', 71500.000, 8.000, '1', 0.000, 0.000, 31.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 71500.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(262, '00255', 3, 'UDANG KUPAS', 'KG', 150000.000, 5.000, '1', 0.000, 0.000, 181.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 150000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(263, '00256', 3, 'PRAWN', 'KG', 150033.664, 20.500, '1', 0.000, 0.000, 35.800, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 150000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(264, '00257', 3, 'RED SNAPER WHOLE', 'KG', 75000.000, 0.000, '1', 0.000, 0.000, 22.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 75000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(265, '00258', 3, 'MAHI WHOLE', 'KG', 75000.000, 0.000, '1', 0.000, 0.000, 31.500, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 75000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(266, '00259', 3, 'MAHI FILLET', 'KG', 85000.000, 15.000, '1', 0.000, 0.000, 214.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 85000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(267, '00260', 3, 'CUMI', 'KG', 75000.000, -5.000, '1', 0.000, 0.000, 114.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 75000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(268, '00261', 3, 'TUNA WHOLE', 'KG', 45000.000, -0.200, '1', 0.000, 0.000, 42.300, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 45000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(269, '00262', 3, 'TUNA FILLET', 'KG', 84961.362, 12.000, '1', 0.000, 0.000, 195.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 85000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(270, '00263', 3, 'BAWAL FILLET', 'KG', 85000.000, 6.500, '1', 0.000, 0.000, 210.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 85000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(271, '00264', 3, 'PORK RIBS', 'KG', 125400.000, -539.500, '1', 0.000, 0.000, 542.950, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 125400.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(272, '00265', 3, 'PORK SAUSAGES', 'KG', 77520.000, 0.000, '1', 0.000, 0.000, 22.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 77520.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(273, '00266', 3, 'TENGGIRI FILEET', 'KG', 85000.000, 0.000, '1', 0.000, 0.000, 59.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 85000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(274, '00267', 3, 'MACAREL FILLET', 'KG', 85000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 85000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(275, '00268', 3, 'TELOR', 'KRAT', 40000.000, -21.000, '1', 0.000, 0.000, 21.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 40000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(276, '00269', 3, 'BEEF PEPERONI', 'KG', 96900.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 96900.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(277, '00270', 3, 'TOPPING CREAM', 'BOTOL', 60800.000, 0.000, '1', 0.000, 0.000, 4.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 60800.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(278, '00271', 3, 'BERTOLI OLIVE OIL', 'BOTOL', 83000.000, 0.000, '1', 0.000, 0.000, 1.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 83000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(279, '00272', 3, 'ICE CREAM VANILA', 'GALON', 130000.000, -2.000, '1', 0.000, 0.000, 8.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 130000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(282, '00273', 3, 'JUICE SIRSAK 5 Ltr ', 'GLON', 49000.000, 2.000, '1', 0.000, 0.000, 22.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 49000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53002, 61004, 0),
(283, '00274', 5, 'PERNAT GRIGIO', 'BTL', 189750.000, 8.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 189750.000, 12001, 61004, 61004, 61004, 61004, 61004, 53003, 61004, 0),
(284, '00275', 5, 'SAUVIGNOM BLANC', 'BTL', 189750.000, 6.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 10.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 189750.000, 12001, 61004, 53003, 61004, 61004, 61004, 61004, 61004, 0),
(285, '00276', 5, 'AQUA MINI', 'DZ', 30000.000, -218.000, '1', 216.000, 0.000, 0.000, 0.000, NULL, 7.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 30000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(286, '00277', 5, 'JAMAICA RUM', 'BTL', 160000.000, 6.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 2.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 160000.000, 12001, 61004, 53003, 61004, 61004, 61004, 61004, 61004, 0),
(287, '00278', 5, 'WHISKY', 'BTL', 160000.000, 7.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 1.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 160000.000, 12001, 61004, 53003, 61004, 61004, 61004, 61004, 61004, 0),
(288, '00279', 5, 'GRENADINE SYRUP', 'BTL', 35000.000, 8.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 3.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 35000.000, 12001, 61004, 53002, 61004, 61004, 61004, 61004, 61004, 0),
(289, '00280', 5, 'MADU', 'BTL', 25805.556, 4.000, '1', 0.000, 0.000, 10.000, 0.000, NULL, 1.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 25000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(290, '00281', 5, 'ALEXANDRIA', 'BTL', 132000.000, 9.000, '1', 0.000, 0.000, 2.000, 0.000, NULL, 9.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 132000.000, 12001, 61004, 53003, 61004, 61004, 61004, 61004, 61004, 0),
(291, '00282', 5, 'MACATO DRAGON', 'BTL', 189750.000, 1.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 189750.000, 12001, 61004, 53003, 61004, 61004, 61004, 61004, 61004, 0),
(292, '00283', 3, 'PARMESAN CHEESE', 'CAN', 110000.000, 4.000, '1', 0.000, 0.000, 4.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 110000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(293, '00284', 3, 'ANCHOVIES', 'pcs', 0.000, 2.000, '1', 0.000, 0.000, 2.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 75000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(294, '00285', 4, 'COVER DUVE SINGLE', 'PC', 360000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 360000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(295, '00286', 4, 'COVER DUVE DOUBLE', 'PC', 400000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 400000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(296, '00287', 4, 'DENTAL KIT', 'pcs', 3850.000, 0.000, '1', 180.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 3850.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(297, '00288', 4, 'SHOWER CUP', 'pcs', 954.762, -80.000, '1', 180.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 950.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(298, '00289', 4, 'SANITARY BAG', 'PCS', 660.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 660.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(299, '00290', 4, 'LAUNDRY BAG', 'PC', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(300, '00291', 4, 'SOAP', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(301, '00292', 4, 'SHOWER GEL', 'PCS', 2600.000, -200.000, '1', 200.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 2600.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(302, '00293', 3, 'TERIYAKI', 'BTL', 52201.000, -3.000, '1', 0.000, 0.000, 9.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 52201.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(303, '00294', 3, 'COOKING CREAM', 'BTL', 33000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 33000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(304, '00295', 3, 'SHRIMP', 'KG', 150000.000, 5.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 150000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(305, '00296', 3, 'PAHA AYAM', 'KG', 37000.000, -8.500, '1', 0.000, 0.000, 28.500, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 37000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(306, '00297', 3, 'SALAME', 'KG', 124260.000, 0.000, '1', 0.000, 0.000, 2.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 124260.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(307, '00298', 3, 'MOZARELLA CHEESE', 'PAK', 250700.000, -2.000, '1', 0.000, 0.000, 17.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 250700.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(308, '00299', 3, 'ICE CREAM COKLAT', 'GLN', 130000.000, 0.000, '1', 0.000, 0.000, 3.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 130000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(309, '00300', 3, 'ICE CREAM  STRAWBERY', 'GLN', 130000.000, 0.000, '1', 0.000, 0.000, 3.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 130000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(310, '00301', 3, 'BURGER ROOL', 'PCS', 4500.000, -85.000, '1', 0.000, 0.000, 316.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 4500.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(311, '00302', 3, 'ROTI PANJANG', 'PCS', 15000.000, -19.000, '1', 0.000, 0.000, 69.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 15000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(312, '00303', 3, 'ROTI TAWAR KULIT', 'PAK', 9500.000, -280.000, '1', 0.000, 0.000, 761.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 9500.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(313, '00304', 3, 'ROTI GANDUM KULIT', 'PAK', 10000.000, -280.000, '1', 0.000, 0.000, 765.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 10000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(314, '00305', 3, 'KULIT LUMPIA', 'PAK', 23260.000, -33.000, '1', 0.000, 0.000, 74.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 23260.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(315, '00306', 3, 'SAYUR HIJAU', 'KG', 12000.000, 32.000, '1', 0.000, 0.000, 58.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 12000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(316, '00307', 1, 'LEMEN', 'KG', 20000.000, 25.000, '1', 0.000, 0.000, 225.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 20000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(317, '00308', 3, 'SELADA UNGU', 'KG', 30000.000, 8.000, '1', 0.000, 0.000, 8.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 30000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(318, '00309', 3, 'PREE', 'KG', 30000.000, -19.000, '1', 0.000, 0.000, 76.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 30000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(319, '00310', 3, 'KENCUR', 'KG', 25000.000, 0.000, '1', 0.000, 0.000, 2.500, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 25000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(320, '00311', 3, 'KEMIRI', 'KG', 30000.000, -5.500, '1', 0.000, 0.000, 9.750, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 30000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(321, '00312', 3, 'KETUMBAR', 'KG', 30000.000, 0.500, '1', 0.000, 0.000, 0.750, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 30000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(322, '00313', 3, 'DAUN SALAM', 'IKAT', 5000.000, -1.000, '1', 0.000, 0.000, 8.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 5000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(323, '00314', 3, 'MACARONI', 'PAK', 16000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 16000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(324, '00315', 3, 'BALSAMIK', 'BTL', 65500.000, 0.000, '1', 0.000, 0.000, 1.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 65500.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 54003, 0),
(325, '00316', 4, 'ALUMINIUM FOIL', 'pcs', 33420.000, 0.000, '1', 0.000, 0.000, 8.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 33420.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 54003, 0),
(326, '00317', 4, 'SABUT KAWAT', 'PC', 5000.000, -6.000, '1', 0.000, 0.000, 6.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 5000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 54003, 0),
(327, '00318', 3, 'BUMBU SOTO', 'PC', 5360.488, -51.000, '1', 0.000, 0.000, 105.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 5310.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(328, '00319', 3, 'BUMBU RENDANG', 'PAK', 5154.583, -34.000, '1', 0.000, 0.000, 46.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 4780.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(329, '00320', 3, 'TOM YUM PASTA', 'PC', 9240.000, -5.000, '1', 0.000, 0.000, 7.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 9240.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(330, '00321', 4, 'TAS KRESEK', 'PAK', 11000.000, -7.000, '1', 1.000, 0.000, 32.000, 2.000, NULL, 0.000, 0.000, 0.000, 0.000, 1, 0, 1, 0, 0, 0, 0, 0, 11000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 54003, 0),
(331, '00322', 4, 'PLASTIK 2 KG', 'PAK', 3855.000, -10.000, '1', 0.000, 0.000, 23.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 4100.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 54003, 0),
(332, '00323', 4, 'PLASTIK 1 KG', 'PAK', 2260.000, -4.000, '1', 2.000, 0.000, 5.000, 0.000, NULL, 5.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 1, 0, 0, 0, 2260.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 54003, 0),
(333, '00324', 4, 'STEROFOAM', 'PAK', 500.000, 0.000, '1', 0.000, 0.000, 101.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 500.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 54003, 0),
(334, '00325', 4, 'TEPUNG KANJI', 'KG', 4000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 4000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(335, '00326', 3, 'OREGANO', 'BTL', 23000.000, 0.000, '1', 0.000, 0.000, 1.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 23000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(336, '00327', 3, 'THYME', 'BTL', 21200.000, -1.000, '1', 0.000, 0.000, 1.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 21200.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(337, '00328', 3, 'BAY LEAVE', 'BTL', 13490.000, -1.000, '1', 0.000, 0.000, 5.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 13490.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(338, '00329', 5, 'SMALL BINTANG', 'BTL', 13258.000, -33.000, '1', 12.000, 0.000, 0.000, 0.000, NULL, 21.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 13258.000, 12001, 61004, 61004, 61004, 61004, 61004, 53002, 61004, 0),
(339, '00330', 4, 'SHAMPOO', 'PCS', 2600.000, -100.000, '1', 100.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 2600.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(340, '00331', 4, 'CONDITIONER', 'PC', 1485.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 1485.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(341, '00332', 4, 'COTTON BUD', 'pcs', 605.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 605.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(342, '00333', 4, 'HAIR DRAYER', 'PCS', 450000.000, -2.000, '1', 2.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 150000.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(343, '00334', 4, 'METAL POLISH', 'BTL', 180000.000, -1.000, '1', 0.000, 0.000, 1.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 180000.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(344, '00335', 18, 'BAY CLEAN', 'BTL', 8649.000, -7.000, '1', 2.000, 0.000, 8.000, 0.000, NULL, 2.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 8649.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(345, '00336', 5, 'TEQUILLA LOKAL', 'BTL', 160000.000, 3.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 7.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 160000.000, 12001, 61004, 53003, 61004, 61004, 61004, 61004, 61004, 0),
(346, '00337', 5, 'MYERS RUM', 'BTL', 25499.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 25499.000, 12001, 61004, 53003, 61004, 61004, 61004, 61004, 61004, 0),
(347, '00338', 5, 'RED LABEL', 'BTL', 550000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 550000.000, 12001, 61004, 53003, 61004, 61004, 61004, 61004, 61004, 0),
(348, '00339', 5, 'BLACK LABEL', 'BTL', 550000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 550000.000, 12001, 61004, 53003, 61004, 61004, 61004, 61004, 61004, 0),
(349, '00340', 5, 'BUSMILL', 'BTL', 375000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 375000.000, 12001, 61004, 53003, 61004, 61004, 61004, 61004, 61004, 0),
(350, '00341', 5, 'JACK DANIEL', 'BTL', 450000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 450000.000, 12001, 61004, 53003, 61004, 61004, 61004, 61004, 61004, 0),
(351, '00342', 5, 'JIM BEAM', 'BTL', 585000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 585000.000, 12001, 61004, 53003, 61004, 61004, 61004, 61004, 61004, 0),
(352, '00343', 5, 'CHIVAS REGAL', 'BTL', 750000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 750000.000, 12001, 61004, 53003, 61004, 61004, 61004, 61004, 61004, 0),
(353, '00344', 5, 'APRICOT BRANDY', 'BTL', 325000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 325000.000, 12001, 61004, 53003, 61004, 61004, 61004, 61004, 61004, 0),
(354, '00345', 5, 'BAILEYS', 'BTL', 650000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 650000.000, 12001, 61004, 53003, 61004, 61004, 61004, 61004, 61004, 0),
(355, '00346', 5, 'KAHLUA', 'BTL', 375000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 375000.000, 12001, 61004, 53003, 61004, 61004, 61004, 61004, 61004, 0),
(356, '00347', 5, 'COENTRUE', 'BTL', 450000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 450000.000, 12001, 61004, 53003, 61004, 61004, 61004, 61004, 61004, 0),
(357, '00348', 5, 'BENEDICTINE DOM', 'BTL', 600000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 600000.000, 12001, 61004, 53003, 61004, 61004, 61004, 61004, 61004, 0),
(358, '00349', 5, 'GALLIANO', 'BTL', 450000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 450000.000, 12001, 61004, 53003, 61004, 61004, 61004, 61004, 61004, 0),
(359, '00350', 5, 'BLM PINEAPLE', 'BTL', 165000.000, 2.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 165000.000, 12001, 61004, 53003, 61004, 61004, 61004, 61004, 61004, 0),
(360, '00351', 5, 'BLM COFFE', 'BTL', 165000.000, -1.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 1.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 165000.000, 12001, 61004, 53003, 61004, 61004, 61004, 61004, 61004, 0),
(361, '00352', 5, 'CAMPARI', 'BTL', 450000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 450000.000, 12001, 61004, 53003, 61004, 61004, 61004, 61004, 61004, 0),
(362, '00353', 5, 'PERNOD', 'BTL', 375000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 375000.000, 12001, 61004, 53003, 61004, 61004, 61004, 61004, 61004, 0),
(363, '00354', 5, 'MARTEL VSOP', 'BTL', 850000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 850000.000, 12001, 61004, 53003, 61004, 61004, 61004, 61004, 61004, 0),
(364, '00355', 1, 'APOKADO', 'KG', 25000.000, -14.000, '1', 0.000, 0.000, 184.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 25000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(365, '00356', 3, 'DAUN PISANG', 'PCS', 5000.000, 0.000, '1', 0.000, 0.000, 60.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 5000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(366, '00357', 3, 'INJIN', 'KG', 25000.000, -4.000, '1', 0.000, 0.000, 12.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 25000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(367, '00358', 3, 'KACANG TANAH', 'KG', 30000.000, 2.000, '1', 0.000, 0.000, 17.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 30000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(368, '00359', 3, 'KING PRAWN', 'KG', 150000.000, -26.000, '1', 0.000, 0.000, 194.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 150000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(369, '00360', 4, 'CHLORIME GRANULAR', 'KG', 45000.000, 0.000, '1', 30.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 120.000, 0.000, 1, 0, 0, 0, 0, 0, 1, 0, 45000.000, 12001, 61004, 61004, 61004, 56001, 61004, 61004, 61004, 0),
(370, '00361', 4, 'SODA ASH', 'KG', 15000.000, 0.000, '1', 10.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 15000.000, 12001, 61004, 61004, 61004, 56001, 61004, 61004, 61004, 0),
(371, '00362', 4, 'PAC', 'KG', 20000.000, 0.000, '1', 5.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 20000.000, 12001, 61004, 61004, 61004, 56001, 61004, 61004, 61004, 0),
(372, '00363', 4, 'HCL 25 LITER', 'Pail', 180000.000, 0.000, '1', 3.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 180000.000, 12001, 61004, 61004, 61004, 56001, 61004, 61004, 61004, 0),
(373, '00364', 4, 'SODIUM 25 LITER', 'Pail', 180000.000, 0.000, '1', 1.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 180000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(374, '00365', 4, 'VOUCHER WELCOME DRINK', 'PC', 500.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 500.000, 12001, 51003, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(375, '00366', 5, 'RADLER BIR', 'KRAT', 318200.000, 19.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 2.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 318200.000, 12001, 61004, 53002, 61004, 61004, 61004, 61004, 61004, 0),
(376, '00367', 5, 'BACARDI LIGHT', 'BOTOL', 295000.000, 1.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 295000.000, 12001, 61004, 53003, 61004, 61004, 61004, 61004, 61004, 0),
(377, '00368', 5, 'TIA MARIA', 'BOTOL', 475000.000, 1.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 475000.000, 12001, 61004, 53003, 61004, 61004, 61004, 61004, 61004, 0),
(378, '00369', 5, 'LYECHE', 'BTL', 325000.000, 1.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 325000.000, 12001, 61004, 53003, 61004, 61004, 61004, 61004, 61004, 0),
(379, '00370', 5, 'HAZELNUT', 'BOTOL', 250000.000, 1.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 250000.000, 12001, 61004, 53003, 61004, 61004, 61004, 61004, 61004, 0),
(380, '00371', 5, 'BUTER SCOT', 'BOTOL', 275000.000, 1.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 275000.000, 12001, 61004, 53003, 61004, 61004, 61004, 61004, 61004, 0),
(381, '00372', 3, 'PRAWN', 'KG', 150000.000, 0.000, '1', 0.000, 0.000, 9.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 150000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(382, '00373', 3, 'COKLAT SAOS', 'BTL', 9783.333, 2.000, '1', 0.000, 0.000, 4.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 10175.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(383, '00374', 3, 'ASTOR', 'KLG', 16100.000, -1.000, '1', 0.000, 0.000, 5.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 16100.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(384, '00375', 3, 'CERES', 'pak', 23092.018, 1.000, '1', 0.000, 0.000, 6.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 23000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(385, '00376', 3, 'MERICA HITAM', 'KG', 153333.333, -0.250, '1', 0.000, 0.000, 2.500, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 150000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(386, '00377', 4, 'RANGKAIAN SEPORA', 'BUAH', 35000.000, 0.000, '1', 0.000, 0.000, 0.000, 1.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 1, 0, 0, 0, 0, 35000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 54003, 0),
(387, '00378', 4, 'SERVIS BATH MAT', 'PCS', 5000.000, 0.000, '1', 8.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 5000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(388, '00379', 3, 'LONTONG', 'PCS', 2000.000, -80.000, '1', 0.000, 0.000, 105.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 2000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(389, '00380', 4, 'PERLENGKAPAN UPACARA', 'PCS', 106000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 106000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 58003, 0),
(390, '00381', 4, 'PREMIUM', 'LITER', 200000.000, 0.000, '1', 0.000, 0.000, 0.000, 1.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 1, 0, 0, 0, 0, 200000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 61002, 0),
(391, '00382', 5, 'BUAHVITA', 'PCS', 6250.000, 0.000, '1', 0.000, 0.000, 69.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 6250.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(392, '00383', 3, 'OLIVE OIL', 'BTL', 71250.000, 0.000, '1', 0.000, 0.000, 1.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 71250.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(393, '00384', 3, 'COOKING CREAM', 'KOTAK', 37197.000, -1.000, '1', 0.000, 0.000, 6.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 37197.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(394, '00385', 4, 'PARKIR', '1X', 1000.000, 0.000, '1', 0.000, 0.000, 10.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 61002, 0),
(395, '00386', 4, 'FOTO COPY', 'LBR', 200.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 53200.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 58005, 0),
(396, '00387', 4, 'GLADE', 'PCS', 33500.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 33500.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(397, '00388', 4, 'TAS PLASTIK', 'PCS', 200.000, -17.000, '1', 2.000, 0.000, 49.000, 1.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 200.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 54003, 0),
(398, '00389', 4, 'CIF CREM', 'BH', 18550.000, -8.000, '1', 4.000, 0.000, 12.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 18550.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 54003, 0),
(399, '00390', 4, 'WIPOL', 'BH', 20700.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 20700.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 54003, 0),
(400, '00391', 4, 'SENDOK TEH', 'PCS', 7500.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 6750.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 54003, 0),
(401, '00392', 4, 'GARPU', 'PCS', 6450.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 6450.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 54003, 0),
(402, '00393', 3, 'FERMIPAN', 'PCS', 14650.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 14650.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(403, '00394', 3, 'COCO CHIPS', 'PAK', 50000.000, 4.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 50000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(404, '00395', 3, 'LOMBOK MERAH', 'KG', 30000.000, -6.000, '1', 0.000, 0.000, 9.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 30000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(405, '00396', 5, 'BINTANG BREMER 620ML PL16', 'KRT', 380200.000, -28.000, '1', 0.000, 0.000, 0.000, 2.000, NULL, 39.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 380200.000, 12001, 61004, 61004, 61004, 61004, 61004, 53002, 61004, 0),
(406, '00397', 5, 'BINTANG PINT 330ML PL24', 'KRT', 318200.000, -42.000, '1', 0.000, 0.000, 0.000, 1.000, NULL, 57.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 318200.000, 12001, 61004, 61004, 61004, 61004, 61004, 53002, 61004, 0),
(407, '00398', 5, 'REDLER PINT 330 ML PL24', 'KRAT', 318200.000, -9.000, '1', 0.000, 0.000, 0.000, 1.000, NULL, 8.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 335000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53002, 61004, 0),
(408, '00399', 3, 'TROPICANA SLIM', 'PAK', 73550.000, 0.000, '1', 0.000, 0.000, 10.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 73550.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(409, '00400', 3, 'SELA', 'BKS', 8000.000, 0.000, '1', 0.000, 0.000, 8.000, 4.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 1, 0, 0, 0, 0, 8000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(410, '00401', 3, 'NASI SELA', 'BKS', 10000.000, 0.000, '1', 0.000, 0.000, 395.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 10000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 57002, 0),
(411, '00402', 4, 'CUCI MOBIL', '', 60000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 60000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 61002, 0),
(412, '00403', 4, 'BUKA TUTUP TANGKI', '', 30000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 30000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 61005, 0),
(413, '00404', 4, 'LAMPU POOL12X 100WT', 'BH', 100000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 100000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 61005, 0),
(414, '00405', 4, 'FITING HELOGEN', 'BH', 12500.000, 0.000, '1', 0.000, 0.000, 0.000, 1.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 1, 0, 0, 0, 0, 12500.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 61005, 0),
(415, '00406', 4, 'SILICON', 'BH', 15000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 15000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 61005, 0),
(416, '00407', 4, 'ALAT UPAKARA', '', 111000.000, -1.000, '1', 0.000, 0.000, 0.000, 1.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 111000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 58003, 0),
(417, '00408', 4, 'COMPLEMENT FORM', 'PC', 6000.000, 0.000, '1', 0.000, 0.000, 3.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1750.000, 12001, 61004, 61004, 61004, 61004, 53002, 53002, 61004, 0),
(418, '00409', 4, 'CERONCONG', 'PCS', 5000.000, -16.000, '1', 16.000, 0.000, 5.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 5000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(419, '00410', 5, 'ES CUBA', 'pak', 6000.000, 0.000, '1', 0.000, 0.000, 10.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 6000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53002, 61004, 0),
(420, '00411', 4, 'ARANG BATOK', 'KG', 10000.000, -125.000, '1', 0.000, 0.000, 320.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 10000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 54003, 0),
(421, '00412', 3, 'PROCHES SLICE', 'crt', 255928.000, 2.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 255928.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(422, '00413', 3, 'CHICKEN BREAKFAST ', 'KG', 66120.000, -4.000, '1', 0.000, 0.000, 24.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 66120.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(423, '00414', 3, 'MACAREL WHOLE', 'KG', 80000.000, 0.000, '1', 0.000, 0.000, 18.300, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 80000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(424, '00415', 3, 'SNAPER', 'KG', 70000.000, -8.000, '1', 0.000, 0.000, 24.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 70000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(425, '00416', 3, 'CUMI-CUMI', 'KG', 80000.000, 0.500, '1', 0.000, 0.000, 184.800, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 80000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(426, '00417', 6, 'LAUNDRY', 'kg', 601000.000, 0.000, '1', 1.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 601000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 2101, 0),
(427, '00418', 4, 'TERMOSTAR', 'PCS', 130000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 130000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 54003, 0),
(428, '00419', 5, 'SYRUP MARJAN STRAWBERRY', 'BTL', 21330.000, -1.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 1.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 21330.000, 12001, 61004, 61004, 61004, 61003, 61004, 61004, 61004, 0),
(429, '00420', 5, 'MARISA STROWBERY', 'BTL', 21000.000, 0.000, '1', 0.000, 0.000, 9.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 21000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(430, '00421', 3, 'SPRING ROLL', 'PAK', 22400.000, 0.000, '1', 0.000, 0.000, 5.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 22400.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(431, '00422', 3, 'BIHUN JAGUNG', 'PAK', 6000.000, -19.000, '1', 0.000, 0.000, 44.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 2950.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(432, '00423', 7, 'MUSIK PER REGULER EVENT', 'SEKALI MAIN ', 400000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 400000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 54002, 0),
(433, '00424', 7, 'transport sapam', 'SEBULAN ', 200000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 200000.000, 12001, 61004, 61004, 58001, 61004, 61004, 61004, 61004, 0),
(434, '00425', 7, 'tambahan', '1 KALI', 250000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 250000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 57002, 0),
(435, '00426', 4, 'PASANG INDOOR DI ACC', 'SEKALI DATANG', 100000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 100000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 61001, 0),
(436, '00427', 4, 'PORCELIEN CLENER', 'PAIL', 272000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 272000.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(437, '00428', 4, 'PLASTIK SAMPAH', 'PCS', 1400.000, -156.000, '1', 50.000, 0.000, 106.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 1400.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 54003, 0),
(438, '00429', 3, 'DAUN JERUK', 'kg', 3000.000, 1.000, '1', 0.000, 0.000, 16.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 3000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(439, '00430', 4, 'LT  1/2', 'BH', 8500.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 8500.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 61005, 0),
(440, '00431', 4, 'LP 1/2', 'BH', 8500.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 8500.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61005, 0),
(441, '00432', 4, 'TEE', 'BH', 10000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 10000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 61005, 0),
(442, '00433', 4, 'SEMEN PUTIH', 'KG', 4000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 4000.000, 61001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(443, '00434', 4, 'pipe', 'PC', 81000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 81000.000, 12001, 61004, 61004, 61004, 61004, 61005, 61004, 61004, 0),
(444, '00435', 5, 'ICE TUBE', 'PAK', 6000.000, -80.000, '1', 0.000, 0.000, 30.000, 0.000, NULL, 172.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 1, 0, 0, 0, 6000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53002, 61004, 0),
(445, '00436', 1, 'KELAPA MUDA', 'BH', 10000.000, 0.000, '1', 0.000, 0.000, 80.000, 0.000, NULL, 40.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 10000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53002, 61004, 0),
(446, '00437', 3, 'BUTTER MINI', 'PAK', 205000.000, 12.000, '1', 0.000, 0.000, 24.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 205000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(447, '00438', 3, 'BERAS PUTRI', 'KG', 64000.000, 0.000, '1', 0.000, 0.000, 2.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 64000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(448, '00439', 5, 'MARJAN VANILA', 'BTL', 21330.000, -1.000, '1', 0.000, 1.000, 0.000, 0.000, NULL, 1.000, 0.000, 0.000, 0.000, 0, 1, 0, 0, 0, 0, 0, 0, 21330.000, 12001, 51002, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(449, '00440', 7, 'RINDIK', 'SEBULAN ', 1040000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 1040000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 54002, 0),
(450, '00441', 3, 'ROTI TAWAR KUPAS', 'SEBUNGKUS ', 10000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 9000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(451, '00442', 7, 'BALI POS', 'pcs', 190000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 190000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 58002, 0),
(452, '00443', 4, 'DLINK WIEELESS', 'PC', 350000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 350000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 59001, 0),
(453, '00444', 3, 'MAHI - MAHI', 'KG', 110000.000, -7.000, '1', 0.000, 0.000, 23.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 110000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(454, '00445', 4, 'LPG 12 KG', 'CYLINDER', 138000.000, -19.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 55.000, 0, 0, 0, 0, 0, 0, 0, 1, 138000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(455, '00446', 4, 'VOCHER WELCAME DRINK', 'pcs', 500.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 500.000, 12001, 51003, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(456, '00447', 5, 'ICE TUBE', 'PAK', 6000.000, 0.000, '1', 0.000, 0.000, 30.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 6000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53002, 61004, 0),
(457, '00448', 6, 'LAUNDRY BLN SEPTEMBER 2017', 'KG', 17335800.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 17335800.000, 2101, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(458, '00449', 4, 'C/L HOMELINE', 'BOX', 234850.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 234850.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 58005, 0),
(459, '00450', 4, 'KUNCI', 'SET', 100000.000, -1.000, '1', 0.000, 0.000, 0.000, 1.000, NULL, 0.000, 0.000, 0.000, 1.000, 0, 0, 0, 1, 0, 0, 0, 0, 100000.000, 61001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(460, '00451', 4, 'ENGSEL', 'PCS', 10000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 10000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 61001, 0),
(461, '00452', 5, 'AIR ISI ULANG', 'PCS', 414000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 414000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 57002, 0),
(462, '00453', 4, 'KEY TAG', 'PCS', 60000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 60000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 61001, 0),
(463, '00454', 3, 'GULA', 'PCS', 250.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 250.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0);
INSERT INTO `tbitem` (`iditem`, `codeitem`, `idsubcategory`, `description`, `unit`, `price`, `stok`, `isaktif`, `stokHK`, `stokFO`, `stokFB`, `stokBO`, `idvendor`, `stokBar`, `stokSec`, `stokPG`, `stokEng`, `isHK`, `isFO`, `isFB`, `isBO`, `isBar`, `isSec`, `isPG`, `isEng`, `lastprice`, `akunHK`, `akunFO`, `akunBar`, `akunSec`, `akunPG`, `akunEng`, `akunFB`, `akunBO`, `isprosesopname`) VALUES
(464, '00455', 4, 'ML. SENG', 'PCS', 5000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 5000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 61001, 0),
(465, '00456', 4, 'SELTIP', 'PCS', 3500.000, -1.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 1.000, 0, 0, 0, 0, 0, 0, 0, 0, 3500.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 61001, 0),
(466, '00457', 4, 'KRAN BRASCO', 'PCS', 40000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 40000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 61001, 0),
(467, '00458', 4, 'SELANG', 'PCS', 8000.000, 0.000, '1', 0.000, 0.000, 0.000, 2.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 1, 0, 0, 0, 0, 8000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 61001, 0),
(468, '00459', 3, 'FRENSTIC', 'PCS', 15000.000, 0.000, '1', 0.000, 0.000, 5.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 15000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(469, '00460', 3, 'TOPPING AEROSOL', 'BTL', 63500.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 63500.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(470, '00461', 4, 'AC PANASONIC 1PK', 'PCS', 3600000.000, 0.000, '1', 0.000, 0.000, 4.000, 0.000, NULL, 2.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 25000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(471, '00462', 3, 'PORK SPARE RIBS', 'KG', 125400.000, 0.000, '1', 0.000, 0.000, 4.800, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 125400.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(472, '00463', 4, 'BALING BALING KIPAS', 'BH', 40000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 40000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 61005, 0),
(473, '00464', 4, 'ALAT ALAT TULIS', 'pcs', 224800.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 224800.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 58005, 0),
(474, '00465', 3, 'JUICE ORANGE RTD 5 LTR', 'GLN', 49000.000, -12.000, '1', 0.000, 0.000, 72.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 49000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(475, '00466', 3, 'JUICE MANGO RTD 5 LTR', 'GLN', 49000.000, -1.000, '1', 0.000, 0.000, 37.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 49000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(476, '00467', 3, 'JUICE GUAVA RTD 5LTR', 'GLN', 49000.000, -6.000, '1', 0.000, 0.000, 67.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 49000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(477, '00468', 5, 'PPN ', 'PCS', 40000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 40000.000, 12001, 61004, 61004, 61004, 61004, 61004, 58005, 61004, 0),
(478, '00469', 1, 'PISANG RANTI', 'SISIR', 20000.000, 0.000, '1', 0.000, 0.000, 24.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 20000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(479, '00470', 1, 'PISANG TANDUK', 'KG', 10000.000, -40.000, '1', 0.000, 0.000, 40.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 10000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(480, '00471', 3, 'WHIPING CREAM ', 'BH', 71500.000, -2.000, '1', 0.000, 0.000, 5.000, 0.000, NULL, 2.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 76500.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(481, '00472', 3, 'ORIGANO', 'BH', 21330.000, -5.000, '1', 0.000, 0.000, 10.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 17990.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(482, '00473', 3, 'ROSE MERY', 'BH', 22400.000, -5.000, '1', 0.000, 0.000, 10.100, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 21200.000, 12001, 61004, 61004, 61004, 61004, 61004, 53003, 61004, 0),
(483, '00474', 3, 'LEMO', 'KG', 5000.000, 0.000, '1', 0.000, 0.000, 11.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 5000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(484, '00475', 3, 'TOM YAM PASTA', 'pak', 10000.000, -10.000, '1', 0.000, 0.000, 25.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 10000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(485, '00476', 3, 'BAYAM AKAR', 'IKAT', 12000.000, -1.000, '1', 0.000, 0.000, 32.500, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 12000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(486, '00477', 3, 'KACANG IJO', 'KG', 25000.000, -1.000, '1', 0.000, 0.000, 15.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 25000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(487, '00478', 4, 'PLASTIK FILE', 'PCS', 11350.000, 0.000, '1', 0.000, 0.000, 1.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 11350.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 54003, 0),
(488, '00479', 5, 'ARAK BREM', 'BTL', 5390.000, 0.000, '1', 0.000, 0.000, 6.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 5390.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 58003, 0),
(489, '00480', 3, 'CHICK. BREAST', 'KG', 56000.000, 10.000, '1', 0.000, 0.000, 190.500, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 56000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(490, '00481', 1, 'PISANG RAJA', 'KG', 20000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 20000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(491, '00482', 8, 'MEJA KANTOR', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 12.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 1, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 2608, 0),
(492, '00483', 8, 'MEJA KECIL', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 3.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 1, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 2608, 0),
(493, '00484', 8, 'KURSI KANTOR', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 17.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 1, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 2608, 0),
(494, '00485', 8, 'KURSI PJNG TUNGGU', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 2.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 1, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 2608, 0),
(495, '00486', 8, 'COMPUTER LG', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 5.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 1, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 2608, 0),
(496, '00487', 8, 'PRINTER EPSON', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 3.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 1, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 2608, 0),
(497, '00488', 8, 'PRINTER CANON', 'PCS', 0.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 2608, 0),
(498, '00489', 8, 'SCANNER CANON', 'PCS', 0.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 2608, 0),
(499, '00490', 8, 'MESIN TIK ', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 1.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 1, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 2608, 0),
(500, '00491', 8, 'AC PANASONIK 1PK', 'UNIT', 3700000.000, 0.000, '1', 0.000, 0.000, 0.000, 1.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 1, 0, 0, 0, 0, 1000.000, 2604, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(501, '00492', 8, 'KIPAS ANGIN LG', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 3.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 1, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 2604, 0),
(502, '00493', 8, 'TV CCTV + RECORD', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 1.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 1, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 2608, 0),
(503, '00494', 8, 'TELPON LG', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 3.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 1, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 2608, 0),
(504, '00495', 8, 'ALMARI BESI', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 4.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 1, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 2608, 0),
(505, '00496', 8, 'ALMARI PLASTIK', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 6.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 1, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 2608, 0),
(506, '00497', 8, 'CALCULATOR CITIZEN', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 9.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 1, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 58005, 0),
(507, '00498', 8, 'STAPPLER', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 9.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 1, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 58005, 0),
(508, '00499', 8, 'JAM DINDING', 'PCS', 170000.000, 0.000, '1', 0.000, 1.000, 0.000, 3.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 1, 0, 1, 0, 0, 0, 0, 170000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 51005, 0),
(509, '00500', 8, 'TEMPAT SAMPAH', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 5.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 1, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 61003, 0),
(510, '00501', 8, 'STAVOL ', 'PCS', 1000.000, -1.000, '1', 0.000, 0.000, 0.000, 3.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 1, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 58005, 0),
(511, '00502', 9, 'CALCULATOR CASIO', 'PCS', 1000.000, 0.000, '1', 0.000, 1.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 1, 0, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 58005, 0),
(512, '00503', 9, 'CALCULATOR CITIZEN', 'PCS', 1000.000, 0.000, '1', 0.000, 1.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 1, 0, 0, 0, 0, 0, 0, 1000.000, 12001, 51003, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(513, '00504', 9, 'PENGGARIS', 'PCS', 1000.000, 0.000, '1', 0.000, 1.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 1, 0, 0, 0, 0, 0, 0, 1000.000, 12001, 51003, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(514, '00505', 9, 'JAM BEKER SEIKO', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 1, 0, 0, 0, 0, 0, 0, 1000.000, 12001, 51005, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(515, '00506', 9, 'JAM DIDING SEIKO', 'PCS', 1000.000, 0.000, '1', 0.000, 7.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 1, 0, 0, 0, 0, 0, 0, 1000.000, 12001, 51005, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(516, '00507', 9, 'MEJA TULIS', 'PCS', 1000.000, 0.000, '1', 0.000, 3.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 1, 0, 0, 0, 0, 0, 0, 1000.000, 12001, 2608, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(517, '00508', 9, 'KURSI', 'PCS', 1000.000, 0.000, '1', 0.000, 6.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 1, 0, 0, 0, 0, 0, 0, 1000.000, 12001, 2608, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(518, '00509', 9, 'COMPUTER TOSHIBA', 'PCS', 1000.000, 0.000, '1', 0.000, 3.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 1, 0, 0, 0, 0, 0, 0, 1000.000, 12001, 2608, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(519, '00510', 9, 'TELEPON PANASONIK', 'PCS', 1000.000, 0.000, '1', 0.000, 2.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 1, 0, 0, 0, 0, 0, 0, 1000.000, 12001, 2608, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(520, '00511', 9, 'FAXCIMALE PANASONIK', 'PCS', 1000.000, 0.000, '1', 0.000, 1.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 1, 0, 0, 0, 0, 0, 0, 1000.000, 12001, 2608, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(521, '00512', 9, 'FILING CABINET ELITE', 'PCS', 1000.000, 0.000, '1', 0.000, 1.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 1, 0, 0, 0, 0, 0, 0, 1000.000, 12001, 2605, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(522, '00513', 9, 'SAFETY BOX CHUBB', 'PCS', 1000.000, 0.000, '1', 0.000, 2.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 1, 0, 0, 0, 0, 0, 0, 1000.000, 12001, 2605, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(523, '00514', 9, 'TBG PEMADAM', 'PCS', 1000.000, 0.000, '1', 0.000, 1.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 1, 0, 0, 0, 0, 0, 0, 1000.000, 12001, 2603, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(524, '00515', 9, 'TIMBANGAN', 'PCS', 1000.000, 0.000, '1', 0.000, 1.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 1, 0, 0, 0, 0, 0, 0, 1000.000, 12001, 2605, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(525, '00516', 9, 'PLASTER STND', 'PCS', 1000.000, 0.000, '1', 0.000, 1.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 1, 0, 0, 0, 0, 0, 0, 1000.000, 12001, 51003, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(526, '00517', 9, 'TMPT SAMPAH', 'PCS', 1000.000, 0.000, '1', 0.000, 2.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 1, 0, 0, 0, 0, 0, 0, 1000.000, 12001, 51005, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(527, '00518', 9, 'STEPLES', 'PCS', 1000.000, 0.000, '1', 0.000, 1.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 1, 0, 0, 0, 0, 0, 0, 1000.000, 12001, 51003, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(528, '00519', 9, 'TIP EX', 'PCS', 1000.000, 0.000, '1', 0.000, 1.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 1, 0, 0, 0, 0, 0, 0, 1000.000, 12001, 51003, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(529, '00520', 9, 'PAPAN TULIS ', 'PCS', 1000.000, 0.000, '1', 0.000, 2.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 1, 0, 0, 0, 0, 0, 0, 1000.000, 12001, 51003, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(530, '00521', 9, 'STAVOL', 'PCS', 1000.000, 0.000, '1', 0.000, 2.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 1, 0, 0, 0, 0, 0, 0, 1000.000, 12001, 51003, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(531, '00522', 9, 'PESAWAT HT FISTCOM', 'PCS', 1000.000, 0.000, '1', 0.000, 1.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 1, 0, 0, 0, 0, 0, 0, 1000.000, 12001, 51005, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(532, '00523', 9, 'BELL COUNTER', 'PCS', 1000.000, 0.000, '1', 0.000, 1.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 1, 0, 0, 0, 0, 0, 0, 1000.000, 12001, 2608, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(533, '00524', 9, 'TEMPAT KORAN', 'PCS', 1000.000, 0.000, '1', 0.000, 1.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 1, 0, 0, 0, 0, 0, 0, 1000.000, 12001, 51005, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(534, '00525', 9, 'GONG ', 'PCS', 1000.000, 0.000, '1', 0.000, 1.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 1, 0, 0, 0, 0, 0, 0, 1000.000, 12001, 51005, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(535, '00526', 9, 'LUKISAN DDG', 'PCS', 1000.000, 0.000, '1', 0.000, 1.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 1, 0, 0, 0, 0, 0, 0, 1000.000, 12001, 51005, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(536, '00527', 9, 'PTNG GARUDA', 'PCS', 1000.000, 0.000, '1', 0.000, 1.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 1, 0, 0, 0, 0, 0, 0, 1000.000, 12001, 2608, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(537, '00528', 9, 'TEMP BROSUR', 'PCS', 0.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 51005, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(538, '00529', 9, 'GUEST SLIP RAK', 'PCS', 1000.000, 0.000, '1', 0.000, 1.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 1, 0, 0, 0, 0, 0, 0, 1000.000, 12001, 51005, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(539, '00530', 9, 'PETA P BALI ', 'PCS', 1000.000, 0.000, '1', 0.000, 1.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 1, 0, 0, 0, 0, 0, 0, 1000.000, 12001, 51005, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(540, '00531', 9, 'LAPTOP ACER', 'PCS', 0.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 2608, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(541, '00532', 9, 'KOMP ALL IN ONE LENOVO', 'PCS', 1000.000, 0.000, '1', 0.000, 1.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 1, 0, 0, 0, 0, 0, 0, 1000.000, 12001, 2608, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(542, '00533', 9, 'PRINTER / KASIR EPSON', 'PCS', 1000.000, 0.000, '1', 0.000, 1.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 1, 0, 0, 0, 0, 0, 0, 1000.000, 12001, 51003, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(543, '00534', 9, 'PRINTER / RSVN HI JET', 'PCS', 1000.000, 0.000, '1', 0.000, 1.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 1, 0, 0, 0, 0, 0, 0, 1000.000, 12001, 51003, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(544, '00535', 9, ' FOTO COPY ', 'PCS', 200.000, 0.000, '1', 0.000, 1.000, 0.000, 0.000, 0, 44.000, 0.000, 0.000, 0.000, 0, 1, 0, 0, 1, 0, 0, 0, 200.000, 12001, 2608, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(545, '00536', 9, 'KULKAS TOSHIBA', 'PCS', 1000.000, 0.000, '1', 0.000, 1.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 1, 0, 0, 0, 0, 0, 0, 1000.000, 12001, 2604, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(546, '00537', 10, 'MATRAS BED', 'PCS', 1000.000, 0.000, '1', 11.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 2609, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(547, '00538', 10, 'EX BED KAYU&STENTIC', 'PCS', 1000.000, 0.000, '1', 10.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 2609, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(548, '00539', 10, 'MEJA KAMAR', 'PCS', 1000.000, 0.000, '1', 6.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 2610, 61001, 61001, 61001, 61001, 61001, 61001, 61001, 0),
(549, '00540', 10, 'KURSI', 'PCS', 1000.000, 0.000, '1', 6.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(550, '00541', 10, 'TMPT SAMPAH KAYU', 'PCS', 0.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 2610, 61001, 61001, 61001, 61001, 61001, 61001, 61001, 0),
(551, '00542', 10, 'TMPT SAMPAH PLASTIK', 'PCS', 1000.000, 0.000, '1', 3.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52001, 61001, 61001, 61001, 61001, 61001, 61001, 61001, 0),
(552, '00543', 10, 'BABY COURT', 'PCS', 40000.000, 0.000, '1', 3.000, 0.000, 0.500, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 1, 0, 0, 0, 0, 0, 40000.000, 2610, 61006, 61006, 61006, 61006, 61006, 61006, 61006, 0),
(553, '00544', 10, 'TREY KAMAR', 'PCS', 1000.000, 0.000, '1', 2.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52003, 61006, 61006, 61006, 61006, 61006, 61006, 61006, 0),
(554, '00545', 10, 'LAMPU TABLE', 'PCS', 1000.000, 0.000, '1', 1.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 2610, 61006, 61006, 61006, 61006, 61006, 61006, 61006, 0),
(555, '00546', 10, 'TANGGA BESI ', 'PCS', 1000.000, 0.000, '1', 1.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 2602, 61006, 61006, 61006, 61006, 61006, 61006, 61006, 0),
(556, '00547', 10, 'KURSI PLASTIK', 'PCS', 1000.000, 0.000, '1', 1.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52003, 61006, 61006, 61006, 61006, 61006, 61006, 61006, 0),
(557, '00548', 10, 'KIPAS ANGIN KANTOR', 'PCS', 1000.000, 0.000, '1', 2.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52003, 61006, 61006, 61006, 61006, 61006, 61006, 61006, 0),
(558, '00549', 10, 'KURSI PANJANG ', 'PCS', 1000.000, 0.000, '1', 2.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 2610, 61006, 61006, 61006, 61006, 61006, 61006, 61006, 0),
(559, '00550', 10, 'EMBER BESAR', 'PCS', 30000.000, 0.000, '1', 1.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 30000.000, 52003, 61006, 61006, 61006, 61006, 61006, 61006, 61006, 0),
(560, '00551', 10, 'RAK', 'PCS', 1000.000, 0.000, '1', 6.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 2610, 61006, 61006, 61006, 61006, 61006, 61006, 61006, 0),
(561, '00552', 10, 'SHEET', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 2611, 61006, 61006, 61006, 61006, 61006, 61006, 61006, 0),
(562, '00553', 10, 'PILLOW CASE', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52003, 61006, 61006, 61006, 61006, 61006, 61006, 61006, 0),
(563, '00554', 10, 'BLENGKET', 'PCS', 1000.000, 0.000, '1', 15.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 2611, 61006, 61006, 61006, 61006, 61006, 61006, 61006, 0),
(564, '00555', 10, 'BED COVER', 'PCS', 0.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 2611, 61006, 61006, 61006, 61006, 61006, 61006, 61006, 0),
(565, '00556', 10, 'KESET', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52003, 61006, 61006, 61006, 61006, 61006, 61006, 61006, 0),
(566, '00557', 10, 'BANTAL', 'PCS', 85000.000, 0.000, '1', 15.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 85000.000, 2611, 61006, 61006, 61006, 61006, 61006, 61006, 61006, 0),
(567, '00558', 10, 'KIPAS ANGIN STAND UNTUK TAMU ', 'PCS', 1000.000, 0.000, '1', 2.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52003, 61006, 61006, 61006, 61006, 61006, 61006, 61006, 0),
(568, '00559', 10, 'RAK KAYU', 'PCS', 1000.000, 0.000, '1', 2.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 2610, 61006, 61006, 61006, 61006, 61006, 61006, 61006, 0),
(569, '00560', 10, 'MEJA KERJA ', 'PCS', 1000.000, 0.000, '1', 2.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 2610, 61006, 61006, 61006, 61006, 61006, 61006, 61006, 0),
(600, '00561', 11, 'BEER GLASS', 'PCS', 19000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 40.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 1, 0, 0, 0, 1000.000, 52003, 61006, 61006, 61006, 61006, 61006, 61006, 61006, 0),
(601, '00562', 11, 'COCKTAIL GLAS', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 3.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 1, 0, 0, 0, 1000.000, 52003, 61006, 61006, 61006, 61006, 61006, 61006, 61006, 0),
(602, '00563', 11, 'LONG GLAS', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 20.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 1, 0, 0, 0, 1000.000, 12001, 61006, 61006, 61006, 61006, 61006, 54003, 61006, 0),
(603, '00564', 11, 'CAMPAGE GLAS', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 26.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 1, 0, 0, 0, 1000.000, 12001, 61006, 61006, 61006, 61006, 61006, 54003, 61006, 0),
(604, '00565', 11, 'HIGHBALL GLAS', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 60.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 1, 0, 0, 0, 1000.000, 12001, 61006, 61006, 61006, 61006, 61006, 54002, 61006, 0),
(605, '00566', 11, 'COGNAC GLAS', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 27.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 1, 0, 0, 0, 1000.000, 12001, 61006, 54003, 61006, 61006, 61006, 61006, 61006, 0),
(606, '00567', 11, 'WINE GLAS', 'PCS', 1000.000, -2.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 20.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 1, 0, 0, 0, 1000.000, 12001, 61006, 54003, 61006, 61006, 61006, 61006, 61006, 0),
(607, '00568', 11, 'JUICE GLAS ', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 13.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 1, 0, 0, 0, 1000.000, 12001, 61006, 54003, 61006, 61006, 61006, 61006, 61006, 0),
(608, '00569', 11, 'JIGGER', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 2.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 1, 0, 0, 0, 1000.000, 12001, 61006, 54003, 61006, 61006, 61006, 61006, 61006, 0),
(609, '00570', 11, 'SHAKER ', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 2.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 1, 0, 0, 0, 1000.000, 12001, 61006, 54003, 61006, 61006, 61006, 61006, 61006, 0),
(610, '00571', 11, 'BLANDER', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 2.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 1, 0, 0, 0, 1000.000, 12001, 61006, 54003, 61006, 61006, 61006, 61006, 61006, 0),
(611, '00572', 11, 'TERMOS ICE', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 1.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 1, 0, 0, 0, 1000.000, 12001, 61006, 54003, 61006, 61006, 61006, 61006, 61006, 0),
(612, '00573', 11, 'SHORT CANDLE', 'PCS', 1000.000, -1.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 24.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 1, 0, 0, 0, 1000.000, 12001, 61005, 54003, 61005, 61005, 61005, 61005, 61005, 0),
(613, '00574', 11, 'SMALL TRAE', 'PCS', 13285.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 7.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 1, 0, 0, 0, 1000.000, 12001, 61005, 61005, 61005, 61005, 61005, 61005, 54003, 0),
(614, '00575', 11, 'LARGE TRAE', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 10.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 1, 0, 0, 0, 1000.000, 12001, 61005, 54003, 61005, 61005, 61005, 61005, 61005, 0),
(615, '00576', 11, 'FREEZER', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 3.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 1, 0, 0, 0, 1000.000, 12001, 61005, 61005, 61005, 61005, 61005, 61005, 2604, 0),
(616, '00577', 11, 'SOFT DRINK GLAS', 'PCS', 1000.000, -3.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 33.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 1, 0, 0, 0, 1000.000, 12001, 61005, 54003, 61005, 61005, 61005, 61005, 61005, 0),
(617, '00578', 11, 'VAS GLAS ', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 14.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 1, 0, 0, 0, 1000.000, 12001, 61005, 54003, 61005, 61005, 61005, 61005, 61005, 0),
(618, '00579', 11, 'WATER GOBLET', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 13.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 1, 0, 0, 0, 1000.000, 12001, 61005, 54003, 61005, 61005, 61005, 61005, 61005, 0),
(619, '00580', 11, 'MARGARITTA GLAS', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 16.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 1, 0, 0, 0, 1000.000, 12001, 61005, 54003, 61005, 61005, 61005, 61005, 61005, 0),
(620, '00581', 11, 'FRUIT PUNCH GLAS', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 27.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 1, 0, 0, 0, 1000.000, 12001, 61005, 54003, 61005, 61005, 61005, 61005, 61005, 0),
(621, '00582', 11, 'ICE COFFEE GLAS', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 6.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 1, 0, 0, 0, 1000.000, 12001, 61005, 54003, 61005, 61005, 61005, 61005, 61005, 0),
(622, '00583', 11, 'CUTTING BOARD', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 1.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 1, 0, 0, 0, 1000.000, 12001, 61005, 61005, 61005, 61005, 61005, 61005, 54003, 0),
(623, '00584', 11, 'BAR KNIFE ', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 2.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 1, 0, 0, 0, 1000.000, 12001, 61005, 54003, 61005, 61005, 61005, 61005, 61005, 0),
(624, '00585', 11, 'WINE COLLER ', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 6.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 1, 0, 0, 0, 1000.000, 12001, 61005, 54003, 61005, 61005, 61005, 61005, 61005, 0),
(625, '00586', 12, 'SENTER', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 2.000, 0.000, 0.000, 0, 0, 0, 0, 0, 1, 0, 0, 1000.000, 12001, 61005, 61005, 58001, 61005, 61005, 61005, 61005, 0),
(626, '00587', 12, 'HAND TOLK', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 2.000, 0.000, 0.000, 0, 0, 0, 0, 0, 1, 0, 0, 1000.000, 12001, 61005, 61005, 58001, 61005, 61005, 61005, 61005, 0),
(627, '00588', 12, 'PENTUNGAN', 'PCS', 150000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 1.000, 0.000, 0.000, 0, 0, 0, 0, 0, 1, 0, 0, 1000.000, 12001, 61005, 61005, 58001, 61005, 61005, 61005, 61005, 0),
(628, '00589', 12, 'BORGOL', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 2.000, 0.000, 0.000, 0, 0, 0, 0, 0, 1, 0, 0, 1000.000, 12001, 61005, 61005, 58002, 61005, 61005, 61005, 61005, 0),
(629, '00590', 12, 'TV CCTV', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 1.000, 0.000, 0.000, 0, 0, 0, 0, 0, 1, 0, 0, 1000.000, 12001, 61005, 61005, 58001, 61005, 61005, 61005, 61005, 0),
(630, '00591', 13, 'MATRAS', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 55.000, 0.000, 0, 0, 0, 0, 0, 0, 1, 0, 1000.000, 12001, 61005, 61005, 61005, 2611, 61005, 61005, 61005, 0),
(631, '00592', 13, 'LONG CHAIR', 'PCS', 1800000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 50.000, 0.000, 0, 0, 0, 0, 0, 0, 1, 0, 1000.000, 12001, 61005, 61005, 61005, 2610, 61005, 61005, 61005, 0),
(632, '00593', 13, 'UMBRELLA', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 18.000, 0.000, 0, 0, 0, 0, 0, 0, 1, 0, 1000.000, 12001, 61005, 61005, 61005, 52003, 61005, 61005, 61005, 0),
(633, '00594', 13, 'STAND UMBRELLA', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 18.000, 0.000, 0, 0, 0, 0, 0, 0, 1, 0, 1000.000, 12001, 61005, 61005, 61005, 2610, 61005, 61005, 61005, 0),
(634, '00595', 13, 'TOWEL POOL', 'PCS', 1000.000, -3.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 120.000, 0.000, 0, 0, 0, 0, 0, 0, 1, 0, 1000.000, 12001, 61005, 61005, 61005, 2611, 61005, 61005, 61005, 0),
(635, '00596', 13, 'SELANG VACUM', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 1.000, 0.000, 0, 0, 0, 0, 0, 0, 1, 0, 1000.000, 12001, 61005, 61005, 61005, 56001, 61005, 61005, 61005, 0),
(636, '00597', 13, 'SQUIS LANTAI', 'PCS', 75000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 1.000, 0.000, 0, 0, 0, 0, 0, 0, 1, 0, 1000.000, 12001, 61005, 61005, 61005, 56002, 61005, 61005, 61005, 0),
(637, '00598', 13, 'SIKAT PLASTIK', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 1.000, 0.000, 0, 0, 0, 0, 0, 0, 1, 0, 1000.000, 12001, 61005, 61005, 61005, 56001, 61005, 61005, 61005, 0),
(638, '00599', 13, 'SIKAT BAJA', 'PCS', 0.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 1.000, 0.000, 0, 0, 0, 0, 0, 0, 1, 0, 0.000, 12001, 61005, 61005, 61005, 56002, 61005, 61005, 61005, 0),
(639, '00600', 13, 'VACUM', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 1.000, 0.000, 0, 0, 0, 0, 0, 0, 1, 0, 1000.000, 12001, 61005, 61005, 61005, 56002, 61005, 61005, 61005, 0),
(640, '00601', 13, 'STIK VACUM', 'PCS', 0.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 1.000, 0.000, 0, 0, 0, 0, 0, 0, 1, 0, 0.000, 12001, 61005, 61005, 61005, 56002, 61005, 61005, 61005, 0),
(641, '00602', 13, 'PESAWAT H T ', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 1.000, 0.000, 0, 0, 0, 0, 0, 0, 1, 0, 1000.000, 12001, 61005, 61005, 61005, 56002, 61005, 61005, 61005, 0),
(642, '00603', 13, 'MESIN POOL', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 2.000, 0.000, 0, 0, 0, 0, 0, 0, 1, 0, 1000.000, 12001, 61005, 61005, 61005, 2604, 61005, 61005, 61005, 0),
(643, '00604', 13, 'SABIT', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 3.000, 0.000, 0, 0, 0, 0, 0, 0, 1, 0, 1000.000, 12001, 61005, 61005, 61005, 61003, 61005, 61005, 61005, 0),
(644, '00605', 13, 'CANGKUL BESAR', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 2.000, 0.000, 0, 0, 0, 0, 0, 0, 1, 0, 1000.000, 12001, 61005, 61005, 61005, 61003, 61005, 61005, 61005, 0),
(645, '00606', 13, 'CANGKUL KECIL', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 1.000, 0.000, 0, 0, 0, 0, 0, 0, 1, 0, 1000.000, 12001, 61005, 61005, 61005, 61003, 61005, 61005, 61005, 0),
(646, '00607', 13, 'GUNTING RUMPUT', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 3.000, 0.000, 0, 0, 0, 0, 0, 0, 1, 0, 1000.000, 12001, 61005, 61005, 61005, 61003, 61005, 61005, 61005, 0),
(647, '00608', 13, 'GUNTING RANTING', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 1.000, 0.000, 0, 0, 0, 0, 0, 0, 1, 0, 1000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(648, '00609', 13, 'EMBER SAMPAH', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 2.000, 0.000, 0, 0, 0, 0, 0, 0, 1, 0, 1000.000, 12001, 61005, 61005, 61005, 61003, 61005, 61005, 61005, 0),
(649, '00610', 13, 'MESIN GENDONG', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 1.000, 0.000, 0, 0, 0, 0, 0, 0, 1, 0, 1000.000, 12001, 61005, 61005, 61005, 61003, 61005, 61005, 61005, 0),
(650, '00611', 13, 'MESIN DORONG', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 1.000, 0.000, 0, 0, 0, 0, 0, 0, 1, 0, 1000.000, 12001, 61005, 61005, 61005, 61003, 61005, 61005, 61005, 0),
(651, '00612', 13, 'GERGAJI', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 1.000, 0.000, 0, 0, 0, 0, 0, 0, 1, 0, 1000.000, 12001, 61005, 61005, 61005, 61003, 61005, 61005, 61005, 0),
(652, '00613', 13, 'KAPAK', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 1.000, 0.000, 0, 0, 0, 0, 0, 0, 1, 0, 1000.000, 12001, 61005, 61005, 61005, 61003, 61005, 61005, 61005, 0),
(653, '00614', 13, 'LINGGIS', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 1.000, 0.000, 0, 0, 0, 0, 0, 0, 1, 0, 1000.000, 12001, 61005, 61005, 61005, 61003, 61005, 61005, 61005, 0),
(654, '00615', 13, 'BLAKAS', 'PCS', 60000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 1.000, 0.000, 0, 0, 0, 0, 0, 0, 1, 0, 60000.000, 12001, 61005, 61005, 61005, 61003, 61005, 61005, 61005, 0),
(655, '00616', 13, 'PINCER AIR', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 4.000, 0.000, 0, 0, 0, 0, 0, 0, 1, 0, 1000.000, 12001, 61005, 61005, 61005, 61003, 61005, 61005, 61005, 0),
(656, '00617', 13, 'TONG SAMPAH RODA', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 2.000, 0.000, 0, 0, 0, 0, 0, 0, 1, 0, 1000.000, 12001, 61005, 61005, 61005, 61003, 61005, 61005, 61005, 0),
(657, '00618', 14, 'WATER HEATER RHEEM', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 10.000, 0, 0, 0, 0, 0, 0, 0, 1, 1000.000, 12001, 61005, 61005, 61005, 61005, 61005, 61005, 61005, 0),
(658, '00619', 14, 'WATER HEATER ARISTON', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 2.000, 0, 0, 0, 0, 0, 0, 0, 1, 1000.000, 2603, 61005, 61005, 61005, 61005, 61005, 61005, 61005, 0),
(659, '00620', 14, 'TV LED 32 LG', 'PCS', 2600000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 35.000, 0, 0, 0, 0, 0, 0, 0, 1, 2600000.000, 2615, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(660, '00621', 14, 'TV LED 32 SHARP', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 14.000, 0, 0, 0, 0, 0, 0, 0, 1, 1000.000, 2615, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(661, '00622', 14, 'TV LED 42 LG', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 1.000, 0, 0, 0, 0, 0, 0, 0, 1, 1000.000, 2615, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(662, '00623', 14, 'TV LG SLIM', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 4.000, 0, 0, 0, 0, 0, 0, 0, 1, 1000.000, 2615, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(663, '00624', 14, 'TV 21 IN TOSHIBA', 'PCS', 0.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 61004, 61004, 2615, 61004, 61004, 61004, 61004, 0),
(664, '00625', 14, 'TABUNG GAS 12 KG', 'CYLINDER', 138000.000, 1.000, '1', 0.000, 0.000, 0.000, 21.000, 0, 0.000, 0.000, 0.000, 9.000, 0, 0, 0, 1, 0, 0, 0, 1, 138000.000, 2603, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(665, '00626', 14, 'TABUNG GAS 50 KG', 'CYLINDER', 566500.000, 0.000, '1', 0.000, 0.000, 0.000, 4.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 1, 0, 0, 0, 1, 566500.000, 12001, 61004, 61004, 61004, 61004, 2605, 61004, 61004, 0),
(666, '00627', 14, 'AC 1 PK TOSHIBA', 'PCS', 1000.000, -1.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 11.000, 0, 0, 0, 0, 0, 0, 0, 1, 1000.000, 12001, 61004, 61004, 61004, 61004, 2604, 61004, 61004, 0),
(667, '00628', 14, 'AC 1 PK PANSNIK', 'PCS', 1000.000, -1.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 23.000, 0, 0, 0, 0, 0, 0, 0, 1, 1000.000, 12001, 61004, 61004, 61004, 61004, 2604, 61004, 61004, 0),
(668, '00629', 14, 'AC 1 PK SHARP', 'PCS', 1000.000, -24.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 1.000, 0, 0, 0, 0, 0, 0, 0, 1, 1000.000, 12001, 61004, 61004, 61004, 61004, 2604, 61004, 61004, 0),
(669, '00630', 14, 'AC 1,5 PK TOSHIBA', 'PCS', 0.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 61004, 61004, 61004, 61004, 2604, 61004, 61004, 0),
(670, '00631', 14, 'AC 2 PK TOSHIBA', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 1.000, 0, 0, 0, 0, 0, 0, 0, 1, 1000.000, 12001, 61004, 61004, 61004, 61004, 2604, 61004, 61004, 0),
(671, '00632', 14, 'AC 1,5 PK PANSNIK', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 2.000, 0, 0, 0, 0, 0, 0, 0, 1, 1000.000, 12001, 61004, 61004, 61004, 61004, 2604, 61004, 61004, 0),
(672, '00633', 14, 'AC 3,4 PK SAMSUNG', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 2.000, 0, 0, 0, 0, 0, 0, 0, 1, 1000.000, 12001, 61004, 61004, 61004, 61004, 2604, 61004, 61004, 0),
(673, '00634', 14, 'AC 1 PK DAIKIN', 'PCS', 3700000.000, -2.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 18.000, 0, 0, 0, 0, 0, 0, 0, 1, 1000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(674, '00635', 14, 'KULKAS FOR TABLE DAEWO', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 21.000, 0, 0, 0, 0, 0, 0, 0, 1, 1000.000, 12001, 61004, 61004, 61004, 61004, 2604, 61004, 61004, 0),
(675, '00636', 14, 'KULKAS FOR TABLE SAMSUNG', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 19.000, 0, 0, 0, 0, 0, 0, 0, 1, 1000.000, 12001, 61004, 61004, 61004, 61004, 2604, 61004, 61004, 0),
(676, '00637', 14, 'KULKAS FOR TABLE SHARP', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 12.000, 0, 0, 0, 0, 0, 0, 0, 1, 1000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(677, '00638', 14, 'KULKAS FOR TABLE TOSHIBA', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 1.000, 0, 0, 0, 0, 0, 0, 0, 1, 1000.000, 12001, 61004, 61004, 61004, 61004, 2604, 61004, 61004, 0),
(678, '00639', 14, 'KULKAS SHARP', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 1.000, 0, 0, 0, 0, 0, 0, 0, 1, 1000.000, 12001, 61004, 61004, 61004, 61004, 2604, 61004, 61004, 0),
(679, '00640', 14, 'TABUNG PEMADAM MESSAFE', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 12.000, 0, 0, 0, 0, 0, 0, 0, 1, 1000.000, 12001, 61004, 61004, 61004, 61004, 2610, 61004, 61004, 0),
(680, '00641', 14, 'TABUNG PEMADAM KRAKATE', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 14.000, 0, 0, 0, 0, 0, 0, 0, 1, 1000.000, 12001, 61004, 61004, 61004, 61004, 2610, 61004, 61004, 0),
(681, '00642', 14, 'BOR LISTRIK BOS', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 1.000, 0, 0, 0, 0, 0, 0, 0, 1, 1000.000, 12001, 61004, 61004, 61004, 61004, 2603, 61004, 61004, 0),
(682, '00643', 14, 'TANGGA ALUMINIUM', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 2.000, 0, 0, 0, 0, 0, 0, 0, 1, 1000.000, 12001, 61004, 61004, 61004, 61004, 2602, 61004, 61004, 0),
(683, '00644', 14, 'SENTER PANSNIK', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 1.000, 0, 0, 0, 0, 0, 0, 0, 1, 1000.000, 12001, 61004, 61004, 61004, 61004, 61005, 61004, 61004, 0),
(684, '00645', 14, 'KOMPESOR ANGIN SWAN', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 1.000, 0, 0, 0, 0, 0, 0, 0, 1, 1000.000, 12001, 61004, 61004, 61004, 61004, 61005, 61004, 61004, 0),
(685, '00646', 14, 'HT ( HAND TALK ) FIST COM', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 1.000, 0, 0, 0, 0, 0, 0, 0, 1, 1000.000, 12001, 61004, 61004, 61004, 61004, 61005, 61004, 61004, 0),
(686, '00647', 14, 'AC 1,5 PK DAIKIN', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 1.000, 0, 0, 0, 0, 0, 0, 0, 1, 1000.000, 12001, 61004, 61004, 61004, 61004, 2604, 61004, 61004, 0),
(687, '00648', 15, 'BATH TOWEL', 'PCS', 70000.000, 0.000, '1', 235.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 70000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(688, '00649', 15, 'HAND TOWEL', 'PCS', 24000.000, -60.000, '1', 316.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 24000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(689, '00650', 15, 'BATH MAT', 'PCS', 70000.000, 0.000, '1', 219.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 70000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(690, '00651', 15, 'FACE TOWEL', 'PCS', 9000.000, -60.000, '1', 363.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 9000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(691, '00652', 15, 'SHEET DOUBLE', 'PCS', 1000.000, 0.000, '1', 117.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(692, '00653', 15, 'SHEET SINGLE', 'PCS', 1000.000, 0.000, '1', 93.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(693, '00654', 15, 'PILLOW CASE', 'PCS', 1000.000, 0.000, '1', 182.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(694, '00655', 15, 'PILLOW CASE DECRT', 'PCS', 1000.000, 0.000, '1', 108.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(695, '00656', 15, 'DOVET DOUBLE INER DOVE', 'PCS', 1000.000, 0.000, '1', 45.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(696, '00657', 15, 'DOVET DOUBLE COVER DOVE', 'PCS', 380000.000, -6.000, '1', 48.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 380000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(697, '00658', 15, 'DOVET TWIN INER DOVE', 'PCS', 1000.000, 0.000, '1', 42.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(698, '00659', 15, 'DOVET TWIN COVER DOVE', 'PCS', 340000.000, -6.000, '1', 57.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 340000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(699, '00660', 15, 'BLENGKIT B , DOUBLE', 'PCS', 0.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 2611, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(700, '00661', 15, 'BLENGKIT B , TWIN', 'PCS', 1000.000, 0.000, '1', 6.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 2611, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(701, '00662', 15, 'PILLOW BIASA ', 'PCS', 1000.000, 0.000, '1', 128.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(702, '00663', 15, 'PILLOW DECRT', 'PCS', 1000.000, 0.000, '1', 108.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(703, '00664', 15, 'SLEMPANG  DOUBLE', 'PCS', 1000.000, 0.000, '1', 38.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(704, '00665', 15, 'SLEMPANG  TWIN', 'PCS', 1000.000, 0.000, '1', 32.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(705, '00666', 15, 'COVER CHAIR SPON', 'PCS', 125000.000, 0.000, '1', 180.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 125000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(706, '00667', 15, 'JOK CHAIR ', 'PCS', 1000.000, 0.000, '1', 166.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(707, '00668', 15, 'SHOWER CURTAIN', 'PCS', 1000.000, 0.000, '1', 55.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(708, '00669', 15, 'KORDEN', 'PCS', 1000.000, 0.000, '1', 45.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(709, '00670', 15, 'DOVE DOUBLE INER DOVE 200X200', 'PCS', 1000.000, 0.000, '1', 2.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(710, '00671', 15, 'DOVE DOUBLE COVER DOVE 200X200', 'PCS', 1000.000, 0.000, '1', 2.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(711, '00672', 15, 'BED PED DOUBLE', 'PCS', 1000.000, 0.000, '1', 36.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 2610, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(712, '00673', 15, 'BED PED TWIN', 'PCS', 1000.000, 0.000, '1', 44.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 2610, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(713, '00674', 15, 'UMBRELLA', 'PCS', 1000.000, 0.000, '1', 44.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(714, '00675', 16, 'GLASS SQUEEZE', 'PCS', 1000.000, 0.000, '1', 3.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(715, '00676', 16, 'FLOOR SQUEEZE', 'PCS', 1000.000, 0.000, '1', 5.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(716, '00677', 16, 'DUST PAN ', 'PCS', 30000.000, 9.000, '1', 5.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 30000.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(717, '00678', 16, 'HAND BRUSH', 'PCS', 1000.000, 0.000, '1', 5.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(718, '00679', 16, 'STICK MOP', 'PCS', 1000.000, 0.000, '1', 9.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(719, '00680', 16, 'TOILET BOWL BRSH', 'PCS', 1000.000, 0.000, '1', 6.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(720, '00681', 16, 'BROOM ( sapu )', 'PCS', 1000.000, -1.000, '1', 6.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(721, '00682', 16, 'BUCKET ( ember )', 'PCS', 1000.000, 0.000, '1', 6.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(722, '00683', 16, 'HANDCADDY', 'PCS', 1000.000, 0.000, '1', 6.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0);
INSERT INTO `tbitem` (`iditem`, `codeitem`, `idsubcategory`, `description`, `unit`, `price`, `stok`, `isaktif`, `stokHK`, `stokFO`, `stokFB`, `stokBO`, `idvendor`, `stokBar`, `stokSec`, `stokPG`, `stokEng`, `isHK`, `isFO`, `isFB`, `isBO`, `isBar`, `isSec`, `isPG`, `isEng`, `lastprice`, `akunHK`, `akunFO`, `akunBar`, `akunSec`, `akunPG`, `akunEng`, `akunFB`, `akunBO`, `isprosesopname`) VALUES
(723, '00684', 16, 'MOPP CLOTH', 'PCS', 1000.000, -10.000, '1', 12.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(724, '00685', 16, 'DUSTING CLOTH', 'PCS', 1000.000, 0.000, '1', 39.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(725, '00686', 16, 'KANEBO', 'PCS', 89000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 89000.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(726, '00687', 16, 'WET&DRY VACUUM', 'PCS', 0.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(727, '00688', 16, 'STEEL WOOL', 'PCS', 0.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(728, '00689', 16, 'BOTTLE SPRAYER', 'PCS', 17000.000, 0.000, '1', 12.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 17000.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(729, '00690', 16, 'SCOTCH BRIGTH', 'PCS', 1000.000, -10.000, '1', 10.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(730, '00691', 16, 'LINEN TROLLEY', 'PCS', 1000.000, 0.000, '1', 2.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 2610, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(731, '00692', 16, 'ROOMBOY TROLLEY', 'PCS', 1000.000, 0.000, '1', 4.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 2610, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(732, '00693', 16, 'BELLBOY TROLLEY', 'PCS', 1000.000, 0.000, '1', 1.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 2610, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(733, '00694', 16, 'VACUUM CLEANER  ', 'PCS', 0.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(734, '00695', 16, 'PESAWAT H T ', 'PCS', 1000.000, 0.000, '1', 2.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(735, '00696', 17, 'DENTAL KIT', 'PCS', 1000.000, -180.000, '1', 150.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(736, '00697', 17, 'SHOWER CUP ', 'PCS', 1000.000, -200.000, '1', 30.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(737, '00698', 17, 'SANITARY BAG', 'PCS', 1000.000, 0.000, '1', 199.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(738, '00699', 17, 'LOUNDRY BAG', 'PCS', 1000.000, -200.000, '1', 26.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(739, '00700', 17, 'SOAP', 'PCS', 1000.000, -300.000, '1', 15.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(740, '00701', 17, 'SHOWER GEL', 'PCS', 2600.000, -100.000, '1', 5.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(741, '00702', 17, 'SHAMPOO', 'PCS', 2600.000, -100.000, '1', 7.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(742, '00703', 17, 'CONDITIONER', 'PCS', 1485.000, 0.000, '1', 45.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(743, '00704', 17, 'COTTON BUD ', 'PCS', 605.000, 0.000, '1', 100.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(744, '00705', 17, 'FACIAL TISSUE ', 'PCS', 1000.000, -100.000, '1', 48.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(745, '00706', 17, 'TOILET PEPPER ', 'PCS', 1000.000, -800.000, '1', 188.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(746, '00707', 17, 'AQUA 330 ml', 'BTL', 1250.000, -151.344, '1', 290.344, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(747, '00708', 17, 'HAIR DRYER', 'PCS', 1000.000, 0.000, '1', 2.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(748, '00709', 17, 'HOT WATER MAKER ', 'PCS', 1000.000, 0.000, '1', 53.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(749, '00710', 17, 'COFFEE SACHET', 'PCS', 1000.000, -500.000, '1', 5.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(750, '00711', 17, 'TEA SACHET', 'PCS', 1000.000, -500.000, '1', 47.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(751, '00712', 17, 'CREAMER SACHET', 'PCS', 1000.000, -500.000, '1', 51.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(752, '00713', 17, 'SUGAR SACHET ', 'PCS', 1000.000, -500.000, '1', 46.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(753, '00714', 17, 'SMALL BIR', 'PCS', 1000.000, -2.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53002, 61004, 0),
(754, '00715', 17, 'COCA COLA ', 'PCS', 1000.000, -6.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 53002, 61004, 61004, 61004, 61004, 61004, 0),
(755, '00716', 17, 'SPRITE ', 'PCS', 4584.000, -14.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 53002, 61004, 61004, 61004, 61004, 61004, 0),
(756, '00717', 17, 'FANTA', 'PCS', 0.000, -20.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 61004, 53002, 61004, 61004, 61004, 61004, 61004, 0),
(757, '00718', 17, 'SODA WATER', 'PCS', 110000.000, 1.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 110000.000, 12001, 61004, 53002, 61004, 61004, 61004, 61004, 61004, 0),
(758, '00719', 16, 'TEA SPOON', 'PCS', 7500.000, 0.000, '1', 111.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 7500.000, 12001, 61004, 54003, 61004, 61004, 61004, 61004, 61004, 0),
(759, '00720', 16, 'CUP ', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(760, '00721', 17, 'SOUCER CUP', 'PCS', 0.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(761, '00722', 18, 'PORSELIN CLEANER', 'liter', 1000.000, -20.000, '1', 10.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 54003, 61004, 61004, 61004, 61004, 61004, 0),
(762, '00723', 18, 'GLASS CLEANER', 'liter', 1000.000, -5.000, '1', 4.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(763, '00724', 18, 'FURNITURE POLISH ( LTR )', 'LTR', 1000.000, -10.000, '1', 5.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(764, '00725', 18, 'FURNITURE POLISH ( BTL )', 'PCS', 0.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(765, '00726', 18, 'METAL POLISH', 'btl', 1000.000, -1.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(766, '00727', 18, 'BAYGON SPRAY', 'btl', 1000.000, -13.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(767, '00728', 18, 'BAY FRESH', 'btl', 1000.000, -19.000, '1', 3.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(768, '00729', 18, 'FLOOR CLEANER', 'liter', 1000.000, -20.000, '1', 8.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(769, '00730', 18, 'BY CLEAN', 'BOTOL', 1000.000, -2.000, '1', 1.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(770, '00731', 18, 'SUN LIGHT', 'PCS', 13750.000, -8.000, '1', 2.000, 0.000, 6.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(771, '00732', 19, 'BREAKFAST KNIFE', 'PCS', 986.111, 1.000, '1', 0.000, 0.000, 65.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54004, 61004, 0),
(772, '00733', 19, 'BREAKFAST FORK', 'PCS', 983.607, 1.000, '1', 0.000, 0.000, 59.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(773, '00734', 19, 'BREAKFAST TEA SPOON', 'PCS', 989.796, 1.000, '1', 0.000, 0.000, 66.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(774, '00735', 19, 'LUNCH KNIFE', 'PCS', 984.615, 1.000, '1', 0.000, 0.000, 63.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 54003, 0),
(775, '00736', 19, 'LUNCH FORK', 'PCS', 984.127, 1.000, '1', 0.000, 0.000, 61.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 54003, 0),
(776, '00737', 19, 'LUNCH SPOON', 'PCS', 998.317, 1.000, '1', 0.000, 0.000, 88.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(777, '00738', 19, 'SOUP SPOON', 'PCS', 985.714, 1.000, '1', 0.000, 0.000, 55.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 54003, 0),
(778, '00739', 19, 'STEAK KNIFE', 'PCS', 944.444, 1.000, '1', 0.000, 0.000, 14.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 54003, 0),
(779, '00740', 19, 'ASTRAY', 'PCS', 941.176, 1.000, '1', 0.000, 0.000, 15.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 54003, 0),
(780, '00741', 19, 'TEMPAT GULA ', 'PCS', 933.333, 1.000, '1', 0.000, 0.000, 13.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 54003, 0),
(781, '00742', 19, ' TABLE CLOTCH PUTIH', 'PCS', 986.111, 1.000, '1', 0.000, 0.000, 70.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 54003, 0),
(782, '00743', 19, ' TABLE CLOTCH MERAH', 'PCS', 977.778, 1.000, '1', 0.000, 0.000, 43.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(783, '00744', 19, ' TABLE CLOTCH HIJAU', 'PCS', 958.333, 1.000, '1', 0.000, 0.000, 22.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 54003, 0),
(784, '00745', 19, ' NAFKIN PUTIH', 'PCS', 995.392, 1.000, '1', 0.000, 0.000, 215.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 54003, 0),
(785, '00746', 19, ' NAFKIN MERAH MAROON', 'PCS', 990.196, 1.000, '1', 0.000, 0.000, 97.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 54003, 0),
(786, '00747', 19, ' SKERTING P0LENG', 'PCS', 857.143, 1.000, '1', 0.000, 0.000, 5.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 54003, 0),
(787, '00748', 19, ' SKERTING HIJAU', 'PCS', 750.000, 1.000, '1', 0.000, 0.000, 2.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 54003, 0),
(788, '00749', 19, ' SKERTING ENDEK', 'PCS', 666.667, 1.000, '1', 0.000, 0.000, 1.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 54003, 0),
(789, '00750', 19, ' SKERTING BIRU ', 'PCS', 750.000, 1.000, '1', 0.000, 0.000, 2.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 54003, 0),
(790, '00751', 19, 'LELONTEK', 'PCS', 916.667, 1.000, '1', 0.000, 0.000, 10.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 54003, 0),
(791, '00752', 19, 'SAPUT SAKE', 'PCS', 937.500, 1.000, '1', 0.000, 0.000, 14.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 54003, 0),
(792, '00753', 19, 'TABLE NOMER', 'PCS', 962.963, 1.000, '1', 0.000, 0.000, 25.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 54003, 0),
(793, '00754', 19, 'TMPT GARAM/MERICA ', 'PCS', 987.805, 1.000, '1', 0.000, 0.000, 80.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 54003, 0),
(794, '00755', 19, 'MEJA REST', 'PCS', 944.444, 1.000, '1', 0.000, 0.000, 16.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 2610, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(795, '00756', 19, 'KURSI REST', 'PCS', 982.759, 1.000, '1', 0.000, 0.000, 56.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(796, '00757', 19, 'MEJA PANTAI', 'PCS', 875.000, 1.000, '1', 0.000, 0.000, 6.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 2610, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(797, '00758', 19, 'KURSI PANTAI', 'PCS', 961.538, 1.000, '1', 0.000, 0.000, 24.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 2610, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(798, '00759', 19, 'SKERTING HITAM ', 'PCS', 800.000, 1.000, '1', 0.000, 0.000, 3.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 54003, 0),
(799, '00760', 19, 'SKERTING PUTIH ', 'PCS', 900.000, 1.000, '1', 0.000, 0.000, 8.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 54003, 0),
(800, '00761', 19, 'KOLONG TIING ', 'PCS', 875.000, 1.000, '1', 0.000, 0.000, 6.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 54003, 0),
(801, '00762', 19, 'KIPAS ANGIN STAND', 'PCS', 750.000, 1.000, '1', 0.000, 0.000, 2.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 54003, 0),
(802, '00763', 19, 'TABLE PLASMET', 'PCS', 983.871, 1.000, '1', 0.000, 0.000, 60.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 54003, 0),
(803, '00764', 20, 'PIRING BREAKFAST', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 84.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(804, '00765', 20, 'PIRING DINE PUTIH', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 154.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 54003, 0),
(805, '00766', 20, 'PIRING DINE CREAM', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 31.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 54003, 0),
(806, '00767', 20, 'PIRING SALAD KOTAK', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 29.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 54003, 0),
(807, '00768', 20, 'PIRING NASI ', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 43.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 54003, 0),
(808, '00769', 20, 'PIRING OVAL', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 11.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(809, '00770', 20, 'OVAL ICE CREAM', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 43.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(810, '00771', 20, 'MANGKOK BREAKFAST', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 47.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(811, '00772', 20, 'OVAL BNN SPLIT', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 17.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(812, '00773', 20, 'TEMPAT TELOR', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 10.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(813, '00774', 20, 'TEMPAT SAOS BUNDAR', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 20.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(814, '00775', 20, 'BOWL SOUP', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 85.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(815, '00776', 20, 'SOUCHER BOWL SOUP', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 80.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(816, '00777', 20, 'CUP', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 64.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(817, '00778', 20, 'SOUCHER CUP', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 172.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(818, '00779', 20, 'TEMPAT SUSU STAINS', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 40.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(819, '00780', 20, 'KOPI/TEA, POT KRAMIK', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 11.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(820, '00781', 20, 'KRAMIK TMPT GULA ', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 20.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(821, '00782', 20, 'PITCHER SUSU', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 2.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(822, '00783', 20, 'PITCHER TEH/ KOPI KACA', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 6.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(823, '00784', 20, 'OVAL SAUS KOTAK', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 4.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(824, '00785', 20, 'OVAL SAUS ', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 5.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(825, '00786', 20, 'SOUCHER SAUS ', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 6.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(826, '00787', 20, 'BOWL KRAMIK PUTIH', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 2.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(827, '00788', 20, 'SOUCHER ROTI BESAR', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 3.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(828, '00789', 20, 'TMPT SEREAL', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 2.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(829, '00790', 20, 'OVAL STAINLESS', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 6.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(830, '00791', 20, 'BOWL KRAMIK BUAH', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 3.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(831, '00792', 20, 'MANGKOK KACA', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 1.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(832, '00793', 20, 'BOWL TMPT TELOR', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 1.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(833, '00794', 20, 'TOASTER', 'PCS', 1800000.000, 0.000, '1', 0.000, 0.000, 3.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1800000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(834, '00795', 20, 'MAGIC COM', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 2.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(835, '00796', 20, 'MAGIC COM BESAR', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 1.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(836, '00797', 20, 'MICROWAVE', 'PCS', 0.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 61004, 61004, 61004, 61004, 61004, 2608, 61004, 0),
(837, '00798', 20, 'TREMOS AIR PANAS', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 1.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(838, '00799', 20, 'OVEN PIZZA', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 1.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 2605, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(839, '00800', 20, 'TMPT AIR MINUM', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 1.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 2605, 61004, 0),
(840, '00801', 20, 'PANCI BESAR', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 2.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(841, '00802', 20, 'PANCI SEDANG', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 3.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(842, '00803', 20, 'TMPT SAOS STAINLESS', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 7.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(843, '00804', 20, 'JAG KOPI KECIL', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 1.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(844, '00805', 20, 'BASKOM STAINLESS', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 7.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(845, '00806', 20, 'TIMBANGAN KECIL', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 1.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(846, '00807', 20, 'PISAU KECIL', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(847, '00808', 20, 'PISAU SAYUR', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 8.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(848, '00809', 20, 'PISAU DAGING', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 3.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(849, '00810', 20, 'PISAU ROTI', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 1.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(850, '00811', 20, 'PILLER', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 1.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(851, '00812', 20, 'PISAU PIZZA', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 1.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(852, '00813', 20, 'PIZZA CUTTER', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 1.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(853, '00814', 20, 'TALENAN PIZZA', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 4.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(854, '00815', 20, 'LOYANG PIZZA', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 6.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(855, '00816', 20, 'PENGGORENGAN BSR', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 5.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(856, '00817', 20, 'PENGGORENGAN KCL', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 1.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(857, '00818', 20, 'KOMPOR CHINESE', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 2.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 2605, 61004, 0),
(858, '00819', 20, 'KOMPOR BIASA', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 3.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(859, '00820', 20, 'TALENAN ', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 2.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(860, '00821', 20, 'CAVING DISH', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 4.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 2605, 61004, 0),
(861, '00822', 20, 'TEPLON TELOR 26 CM', 'PCS', 365000.000, 0.000, '1', 0.000, 0.000, 2.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 365000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(862, '00823', 20, 'TEPLON SAOS', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 6.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(863, '00824', 20, 'SAOS POT', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 3.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 54003, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(864, '00825', 20, 'PANCI BACON', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 1.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 52003, 61004, 0),
(865, '00826', 20, 'TMPT KRUPUK', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 1.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(866, '00827', 20, 'PANCI SUP BIASA', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 1.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(867, '00828', 20, 'PANCI SUP LISTRIK', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 1.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(868, '00829', 20, 'SHOW CASE FREZER', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 1.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 2604, 61004, 0),
(869, '00830', 20, 'FREZER DAGING 6 PINTU', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 1.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 2604, 61004, 0),
(870, '00831', 20, 'KULKAS', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 1.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 2604, 61004, 0),
(871, '00832', 20, 'FREZER TABLE ', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 2.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 2604, 61004, 0),
(872, '00833', 20, 'KIPAS ANGIN ', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 2.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(873, '00834', 20, 'RAK PIRING ', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 1.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(874, '00835', 20, 'RAK STAINLES ', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 4.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 2605, 61004, 0),
(875, '00836', 20, 'MEJA STAINLESS', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 3.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 2605, 61004, 0),
(876, '00837', 20, 'WASTAFEL ', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 3.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 2605, 61004, 0),
(877, '00838', 20, 'OPENER CAN', 'PCS', 0.000, 0.000, '1', 0.000, 0.000, 1.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 0.000, 12001, 61004, 61004, 61004, 61004, 61004, 2605, 61004, 0),
(878, '00839', 20, 'SPATULA', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 7.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(879, '00840', 20, 'SUTIL PENGGORENGAN', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 6.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(880, '00841', 20, 'CILLER 4 PINTU', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 1.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 2604, 61004, 0),
(881, '00842', 20, 'BLENDER', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 1.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(882, '00843', 20, 'PANCI SUP KECIL', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 3.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(883, '00844', 20, 'SENDOK PENYARINGAN', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 4.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(884, '00845', 20, 'TEPLON PANCAKE ', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 2.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(885, '00846', 20, 'JEPITAN ', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 4.000, 0.000, 0, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(886, '00847', 4, 'BATAKO', 'PC', 3000.000, -10.000, '1', 0.000, 0.000, 0.000, 25.000, NULL, 0.000, 0.000, 0.000, 10.000, 0, 0, 0, 1, 0, 0, 0, 0, 3000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61001, 61004, 0),
(887, '00848', 4, 'SEMEN', 'SAK', 50000.000, 0.000, '1', 0.000, 0.000, 0.000, 2.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 1, 0, 0, 0, 0, 50000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 61001, 0),
(888, '00849', 4, 'KOLONG A', 'BTG', 40000.000, 0.000, '1', 0.000, 0.000, 0.000, 1.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 1, 0, 0, 0, 0, 40000.000, 12001, 61004, 61004, 61004, 61004, 61001, 61004, 61004, 0),
(889, '00850', 4, 'WEKANIS 3/5', 'BTG', 40000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 61004, 61004, 61004, 61004, 61001, 61004, 61004, 0),
(890, '00851', 4, 'KAMFER RENG ', 'BTG', 17000.000, 0.000, '1', 0.000, 0.000, 0.000, 2.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 1, 0, 0, 0, 0, 17000.000, 12001, 61004, 61004, 61004, 61004, 61001, 61004, 61004, 0),
(891, '00852', 4, 'VINILEK', 'GL', 110000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(892, '00853', 4, 'KABEL TIS', 'PC', 12500.000, -1.000, '1', 0.000, 0.000, 0.000, 2.000, NULL, 0.000, 0.000, 0.000, 1.000, 0, 0, 0, 1, 0, 0, 0, 0, 12500.000, 12001, 61004, 61004, 61004, 61004, 61005, 61004, 61004, 0),
(893, '00854', 4, 'MEKANIS 3/5', 'btg', 40000.000, 0.000, '1', 0.000, 0.000, 0.000, 1.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 1, 0, 0, 0, 0, 40000.000, 12001, 61004, 61004, 61004, 61004, 61005, 61004, 61004, 0),
(894, '00855', 4, 'VINILEK', 'GL', 110000.000, 0.000, '1', 0.000, 0.000, 0.000, 1.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 1, 0, 0, 0, 0, 110000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 61001, 0),
(895, '00856', 3, 'PERMESAN CHEESE', 'BTL', 110000.000, -6.000, '1', 0.000, 0.000, 6.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 95000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(896, '00857', 5, 'BARCARUM', 'btl', 180000.000, -2.000, '1', 0.000, 0.000, 1.000, 0.000, NULL, 2.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 180000.000, 12001, 61004, 53003, 61004, 61004, 61004, 61004, 61004, 0),
(897, '00858', 3, 'FRISIAN FLAG', 'BH', 10500.000, 0.000, '1', 0.000, 0.000, 1.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 10500.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(898, '00859', 4, 'floor squeeze', '2', 25000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(899, '00860', 3, 'PROCHIZ SLICE', 'crt', 255928.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(900, '00861', 3, 'TROPICANA SLIM', 'PAK', 27500.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(901, '00862', 4, 'JET SHOWER', 'PC', 230000.000, -1.000, '1', 0.000, 0.000, 0.000, 1.000, NULL, 0.000, 0.000, 0.000, 2.000, 0, 0, 0, 1, 0, 0, 0, 1, 230000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(902, '00863', 4, 'KRAN 1/2', 'PC', 25000.000, -3.000, '1', 0.000, 0.000, 0.000, 1.000, NULL, 0.000, 0.000, 0.000, 3.000, 0, 0, 0, 1, 0, 0, 0, 0, 25000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 61001, 0),
(903, '00864', 4, 'SELTIP', 'PC', 5000.000, 0.000, '1', 0.000, 0.000, 0.000, 3.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 1, 0, 0, 0, 0, 5000.000, 12001, 61004, 61004, 61004, 61004, 61005, 61004, 61004, 0),
(904, '00865', 4, 'KLEM KABEL', 'PC', 18000.000, -1.000, '1', 0.000, 0.000, 0.000, 2.000, NULL, 0.000, 0.000, 0.000, 1.000, 0, 0, 0, 1, 0, 0, 0, 0, 18000.000, 12001, 61004, 61004, 61004, 61004, 61005, 61004, 61004, 0),
(905, '00866', 4, 'WAJAN', 'PC', 395000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(906, '00867', 5, 'JIM BEAM', 'BTL', 0.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 61004, 53003, 61004, 61004, 61004, 61004, 61004, 0),
(907, '00868', 4, 'KERTAS CONTINUS FORM 3FLAY', 'PAK', 351700.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 351700.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 58005, 0),
(908, '00869', 4, 'HVS A4', 'RIM', 32500.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 32500.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 58005, 0),
(909, '00870', 4, 'TEA SPOON ROOM', 'pcs', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(910, '00871', 4, 'MEJA KACA ', 'UNIT', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 2.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 1, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 2608, 0),
(911, '00872', 8, 'MEJA KACA', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 3.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 1, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 2605, 0),
(912, '00873', 4, 'JASA LOUNDRY GLAN', 'BLN', 13836000.000, 0.000, '1', 1.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 13836000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(913, '00874', 16, 'TEA SPOON ROOM', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(914, '00875', 16, 'CUP + SOUCER ROOM', 'PCS', 1000.000, 0.000, '1', 158.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(915, '00876', 16, 'GLASS ', 'PCS', 1000.000, 0.000, '1', 215.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 1000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(916, '00877', 20, 'SAUCER BOWL SOUP', 'PCS ', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(917, '00878', 20, 'CUP', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(918, '00879', 17, 'HIGHBALL GLASS/WELLCOME', 'PCS', 9500.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 9500.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(919, '00880', 4, 'SAPU LIDI', 'IKAT', 5000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 5000.000, 12001, 61004, 61004, 61004, 61003, 61004, 61004, 61004, 0),
(920, '00881', 4, 'WAJAN', 'BH', 450000.000, 0.000, '1', 0.000, 0.000, 0.000, 1.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 1, 0, 0, 0, 0, 450000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(921, '00882', 4, 'OPENER CAN', 'BH', 135000.000, 0.000, '1', 0.000, 0.000, 0.000, 1.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 1, 0, 0, 0, 0, 135000.000, 12001, 61004, 61004, 61004, 61004, 61004, 2608, 61004, 0),
(922, '00883', 9, 'TMPT BRUSUR ', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 1000.000, 12001, 51005, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(923, '00884', 9, 'TMPT BRUSUR', 'BUAH', 1000.000, 0.000, '1', 0.000, 1.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 1, 0, 0, 0, 0, 0, 0, 1000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(924, '00885', 9, 'LAKTOP ACER ', 'UNIT', 1000.000, 0.000, '1', 0.000, 1.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 1, 0, 0, 0, 0, 0, 0, 1000.000, 12001, 2608, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(925, '00886', 4, 'TEST KIT', 'psg', 100000.000, 1.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 100000.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(926, '00887', 4, 'DUPA', 'PAK', 19000.000, -7.000, '1', 0.000, 0.000, 2.000, 7.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 19000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 58003, 0),
(927, '00888', 4, 'SPON', 'PC', 8500.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 8500.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(928, '00889', 4, 'SABUT KAWAT', 'PC', 6064.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(929, '00890', 4, 'POLYTEX SPON', 'PC', 9600.000, 0.000, '1', 0.000, 0.000, 8.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 9600.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(930, '00891', 4, 'POLITEX SILVER STAR', 'PC', 2749.000, 0.000, '1', 0.000, 0.000, 15.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 2749.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(931, '00892', 4, 'POL SAB STNLS', 'PC', 10000.000, 0.000, '1', 0.000, 0.000, 1.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 10000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(932, '00893', 3, 'ROTI MANIS', 'PCS', 2500.000, -1150.000, '1', 0.000, 0.000, 2960.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 2500.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(933, '00894', 4, 'SHOWER GEL', 'PC', 2600.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 2600.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(934, '00895', 4, 'SHAMPOO', 'PCS', 2600.000, -200.000, '1', 200.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(935, '00896', 3, 'SUSU COKLAT', 'KL', 12480.000, -4.000, '1', 0.000, 0.000, 10.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 15450.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(936, '00897', 3, 'TEPUNG ROTI', 'PAK', 31450.000, -6.000, '1', 0.000, 0.000, 12.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 31450.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(937, '00898', 3, 'GERKIN', 'BTL', 31500.000, -1.000, '1', 0.000, 0.000, 3.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 33000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(938, '00899', 4, 'STOP KONTAK', 'PC', 27500.000, 0.000, '1', 0.000, 0.000, 0.000, 2.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 1, 0, 0, 0, 0, 27500.000, 12001, 61004, 61004, 61004, 61004, 61005, 61004, 61004, 0),
(939, '00900', 4, 'EMBODUS', 'PC', 12500.000, 0.000, '1', 0.000, 0.000, 0.000, 3.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 1, 0, 0, 0, 0, 12500.000, 12001, 61004, 61004, 61004, 61004, 61005, 61004, 61004, 0),
(940, '00901', 4, 'KABEL NYY 3X 2.5', 'M', 10000.000, 0.000, '1', 0.000, 0.000, 0.000, 6.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 1, 0, 0, 0, 0, 10000.000, 12001, 61004, 61004, 61004, 61004, 61005, 61004, 61004, 0),
(941, '00902', 4, 'ISOLASI LISTRIK', 'PC', 15000.000, 0.000, '1', 0.000, 0.000, 0.000, 1.000, NULL, 0.000, 0.000, 0.000, 1.000, 0, 0, 0, 1, 0, 0, 0, 1, 15000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 61005, 0),
(942, '00903', 4, 'SAKLAR', 'PC', 20000.000, 0.000, '1', 0.000, 0.000, 0.000, 1.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 1, 0, 0, 0, 0, 20000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(943, '00904', 4, 'JAM DINDING', 'PC', 170000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 51005, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(944, '00905', 4, 'MASKER', 'PAK', 50000.000, 0.000, '1', 0.000, 1.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 1, 0, 0, 0, 0, 0, 0, 50000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(945, '00906', 4, 'SUMMARY OF SALES', 'BK', 22000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 22000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54004, 61004, 0),
(946, '00907', 4, 'DAY USE FORM', 'BK', 6000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 6000.000, 12001, 51003, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(947, '00908', 4, 'BUKTI PENYETORAN', 'RIM', 100000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 100000.000, 12001, 51003, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(948, '00909', 4, 'BINCARD', 'PC', 750.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 750.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 58005, 0),
(949, '00910', 4, 'GUEST CARD', 'PC', 2000.000, 0.000, '1', 0.000, 500.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 2000.000, 12001, 51003, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(950, '00911', 4, 'PHILIPS LED', 'PC', 25000.000, -4.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 4.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 61004, 61004, 61004, 61004, 61005, 61004, 61004, 0),
(951, '00912', 4, 'PHILIPS LED 35W', 'PC', 25000.000, 11.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 1.000, 0, 0, 0, 0, 0, 0, 0, 0, 25000.000, 12001, 61004, 61004, 61004, 61004, 61005, 61004, 61004, 0);
INSERT INTO `tbitem` (`iditem`, `codeitem`, `idsubcategory`, `description`, `unit`, `price`, `stok`, `isaktif`, `stokHK`, `stokFO`, `stokFB`, `stokBO`, `idvendor`, `stokBar`, `stokSec`, `stokPG`, `stokEng`, `isHK`, `isFO`, `isFB`, `isBO`, `isBar`, `isSec`, `isPG`, `isEng`, `lastprice`, `akunHK`, `akunFO`, `akunBar`, `akunSec`, `akunPG`, `akunEng`, `akunFB`, `akunBO`, `isprosesopname`) VALUES
(952, '00913', 4, 'SANDAL', 'PCS', 12050.000, -7.000, '1', 17.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 12050.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(953, '00914', 4, 'KALENDER', 'PC', 10000.000, 0.000, '1', 0.000, 0.000, 0.000, 11.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 1, 0, 0, 0, 0, 10000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 58005, 0),
(954, '00915', 4, 'ASEP', 'PAK', 17000.000, 0.000, '1', 0.000, 0.000, 0.000, 4.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 1, 0, 0, 0, 0, 17000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 58003, 0),
(955, '00916', 4, 'KUNCI SOLID', 'SET', 160000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 1.000, 0, 0, 0, 0, 0, 0, 0, 1, 160000.000, 61001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(956, '00917', 4, 'BIKIN KUNCI', 'BH', 40000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 2.000, 0, 0, 0, 0, 0, 0, 0, 1, 40000.000, 61001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(957, '00918', 8, 'MEJA KACA', 'PCS', 1000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 2608, 0),
(958, '00919', 3, 'ROTI TAWAR KULIT ', 'SEBUNGKUS ', 10000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(959, '00920', 3, 'ROTI  GANDUM KULIT', 'BUNGKUS ', 10500.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(960, '00921', 3, 'KOPI SETIA BALI', 'PAK', 13912.000, 0.000, '1', 0.000, 0.000, 4.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 13912.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(961, '00922', 3, 'ZUCHINI', 'kg', 30000.000, 0.000, '1', 0.000, 0.000, 3.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 30000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(962, '00923', 4, 'JANUR', 'PC', 30000.000, 0.000, '1', 0.000, 0.000, 5.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 30000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 58003, 0),
(963, '00924', 3, 'TOMYUM PASTA', 'PC', 12000.000, -15.000, '1', 0.000, 0.000, 21.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 12000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(964, '00925', 3, 'COKLAT CAKE', 'PC', 120000.000, 0.000, '1', 0.000, 0.000, 2.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 120000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(965, '00926', 4, 'TUSUK GIGI', 'PAK', 37000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 3.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 1, 0, 0, 0, 37000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(966, '00927', 4, 'WIPOL', 'BTL', 21750.000, -4.000, '1', 2.000, 0.000, 6.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 21750.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(967, '00928', 3, 'KANGKUNG', 'IKAT', 2200.000, 0.000, '1', 0.000, 0.000, 52.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 2200.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(968, '00929', 18, 'WIPOL', 'btl', 20690.000, -2.000, '1', 2.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 20690.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(969, '00930', 4, 'STEROFOAM', 'PCS', 400.000, -100.000, '1', 0.000, 0.000, 100.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 400.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(970, '00931', 5, 'BAILEYS', 'BTL', 650000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 650000.000, 12001, 61004, 53003, 61004, 61004, 61004, 61004, 61004, 0),
(971, '00932', 5, 'CHIVAS REGAL', 'BTL', 750000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 61004, 53003, 61004, 61004, 61004, 61004, 61004, 0),
(972, '00933', 4, 'KERTAS MINYAK', 'PAK', 30990.000, 0.000, '1', 0.000, 0.000, 1.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 30990.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(973, '00934', 5, 'KOPI SACHET', 'PAK', 160000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(974, '00935', 5, 'TEH SACHET', 'PAK', 90000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(975, '00936', 5, 'CREAM SACHET', 'PAK', 75000.000, -1.000, '1', 1.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 75000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(976, '00937', 4, 'REFILL INK EPSON L100 BK ORI', 'BH', 90000.000, 0.000, '1', 0.000, 0.000, 0.000, 2.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 1, 0, 0, 0, 0, 90000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 58005, 0),
(977, '00938', 4, 'ISI CUTTER BESARKENKO L 150', 'PAK', 4650.000, 0.000, '1', 0.000, 0.000, 0.000, 2.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 1, 0, 0, 0, 0, 4650.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 58005, 0),
(978, '00939', 4, 'CUTTER BESAR JOYKO L500', 'BH', 7800.000, 0.000, '1', 0.000, 0.000, 0.000, 1.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 1, 0, 0, 0, 0, 7800.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 58005, 0),
(979, '00940', 5, 'ARAK', 'BTL', 50000.000, -1.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 1.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 61004, 53003, 61004, 61004, 61004, 61004, 61004, 0),
(980, '00941', 4, 'SAPU LIDI', 'IKAT', 5000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 61004, 61004, 61004, 61003, 61004, 61004, 61004, 0),
(981, '00942', 5, 'CHERY RED', 'BTL', 60000.000, -9.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 9.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 60000.000, 12001, 61004, 53001, 61004, 61004, 61004, 61004, 61004, 0),
(982, '00943', 3, 'MIE TELUR ATOM BULAN', 'PAK', 5000.000, -20.000, '1', 0.000, 0.000, 140.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 5000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(983, '00944', 4, 'JAS HUJAN', 'PC', 8000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 5.000, 0.000, 0, 0, 0, 0, 0, 0, 1, 0, 8000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(984, '00945', 3, 'SNAPER WHOLE', 'kg', 75000.000, 0.000, '1', 0.000, 0.000, 14.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 75000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(985, '00946', 3, 'TUNA WHOLE', 'kg', 42000.000, -8.000, '1', 0.000, 0.000, 8.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 42000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(986, '00947', 3, 'TENGGIRI WHOLE', 'KG', 75000.000, -5.000, '1', 0.000, 0.000, 20.600, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 75000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(987, '00948', 3, 'MAHI MAHI WHOLE', 'KG', 65000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(988, '00949', 3, 'JAMUR KUPING', 'KG', 90000.000, 1.000, '1', 0.000, 0.000, 1.750, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 90000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(989, '00950', 3, 'TERASI', 'PC', 3000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(990, '00951', 3, 'RRUNG/ TERONG', 'KG', 10000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 10000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(991, '00952', 3, 'DAUN MELINJO', 'KG', 80000.000, 0.000, '1', 0.000, 0.000, 1.500, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 80000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(992, '00953', 3, 'BUMBU LODEH', 'PC', 5000.000, 0.000, '1', 0.000, 0.000, 13.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 5000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(993, '00954', 3, 'TERONG', 'KG', 10000.000, 0.000, '1', 0.000, 0.000, 4.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 10000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(994, '00955', 3, 'AGAR AGAR', 'PC', 4490.000, 0.000, '1', 0.000, 0.000, 3.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 4490.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(995, '00956', 3, 'POK CAY', 'KG', 13000.000, 0.000, '1', 0.000, 0.000, 6.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 13000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(996, '00957', 3, 'BROTOLI', 'KG', 45000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(997, '00958', 3, 'ACAR', 'KG', 30000.000, 0.000, '1', 0.000, 0.000, 0.250, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 30000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(998, '00959', 3, 'BROCOLI', 'KG', 40000.000, 0.000, '1', 0.000, 0.000, 3.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 40000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(999, '00960', 5, 'CLUB', 'DZ', 30000.000, -47.000, '1', 47.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 30000.000, 12001, 61004, 53002, 61004, 61004, 61004, 61004, 61004, 0),
(1000, '00961', 3, 'JEPANG', 'BJ', 5000.000, 0.000, '1', 0.000, 0.000, 2.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 5000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(1001, '00962', 3, 'KEMANGI', 'IKAT', 3000.000, 0.000, '1', 0.000, 0.000, 1.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 3000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(1002, '00963', 3, 'NANGKA SAYUR', 'PC', 12000.000, 0.000, '1', 0.000, 0.000, 2.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 12000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(1003, '00964', 3, 'TAHU KUNING SAYUR', 'BKS', 15000.000, 0.000, '1', 0.000, 0.000, 1.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 15000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(1004, '00965', 5, 'PINEAPLE CONCENTRATE  1ltr', 'BTL', 1000.000, -3.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 3.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 50000.000, 12001, 61004, 53002, 61004, 61004, 61004, 61004, 61004, 0),
(1005, '00966', 5, 'LEMEN CONCENTRATE  1ltr', 'BTL', 43000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 61004, 53002, 61004, 61004, 61004, 61004, 61004, 0),
(1006, '00967', 4, 'HIT SPRAY', 'BTL', 39000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 39000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(1007, '00968', 4, 'SIKAT TOILET', 'PC', 30000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(1008, '00969', 4, 'TOOTH BRUSH/SIKAT GIGI', 'PCS', 2600.000, -760.000, '1', 760.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 2600.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(1009, '00970', 3, 'RAMBUTAN', 'KG', 15000.000, -2.000, '1', 0.000, 0.000, 128.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 15000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(1010, '00971', 4, 'MAP PLASTIK', 'PCS', 10890.000, 0.000, '1', 0.000, 10.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 1, 0, 0, 0, 0, 0, 0, 10890.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 58005, 0),
(1011, '00972', 4, 'PHILIPS  15W', 'PCS', 7000.000, -30.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 30.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 61004, 61004, 61004, 61004, 61005, 61004, 61004, 0),
(1012, '00973', 4, 'PHILIPS 25W', 'PCS', 7000.000, -20.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 20.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 61004, 61004, 61004, 61004, 61005, 61004, 61004, 0),
(1013, '00974', 3, 'SPAGHETTI 1KG', 'PAK', 25000.000, -3.000, '1', 0.000, 0.000, 0.000, 3.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(1014, '00975', 20, 'JAM DINDING SEIKO ', '1', 170000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 51005, 0),
(1015, '00976', 4, 'GELAS JUICE 12 PL', 'PAK', 16500.000, 0.000, '1', 0.000, 0.000, 3.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 16500.000, 12001, 61004, 54003, 61004, 61004, 61004, 61004, 61004, 0),
(1016, '00977', 4, 'BATERAI  EVEREDY  1222-9V', 'PCS', 10460.714, -5.000, '1', 0.000, 0.000, 5.000, 0.000, NULL, 0.000, 0.000, 0.000, 5.000, 0, 0, 1, 0, 0, 0, 0, 0, 7300.000, 12001, 61004, 61004, 61004, 61004, 61005, 61004, 61004, 0),
(1017, '00978', 4, 'LAMINATING', 'PCS', 15833.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 6.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 1, 0, 0, 0, 15833.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 58005, 0),
(1018, '00979', 4, 'PULPEN', 'KT', 14500.000, 0.000, '1', 0.000, 0.000, 0.000, 2.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 1, 0, 0, 0, 0, 14500.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 58005, 0),
(1019, '00980', 5, 'SPRITE', 'BTL', 4583.000, -62.000, '1', 62.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 61004, 53002, 61004, 61004, 61004, 61004, 61004, 0),
(1020, '00981', 5, 'FANTA', 'BTL', 4583.000, -26.000, '1', 26.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 61004, 53002, 61004, 61004, 61004, 61004, 61004, 0),
(1021, '00982', 3, 'KELAPA PARUT', 'KG', 6666.667, 4.000, '1', 0.000, 0.000, 12.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 10000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(1022, '00983', 5, 'DIET COKE', 'KL', 4584.000, -25.000, '1', 25.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 61004, 53002, 61004, 61004, 61004, 61004, 61004, 0),
(1023, '00984', 3, 'MADRAS KARE', 'BTL', 100000.000, 0.000, '1', 0.000, 0.000, 2.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 100000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(1024, '00985', 3, 'SUMPING', 'BJ', 1500.000, 0.000, '1', 0.000, 0.000, 200.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1500.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(1025, '00986', 3, 'LAPIS', 'BJ', 1500.000, 0.000, '1', 0.000, 0.000, 50.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 1500.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(1026, '00987', 4, 'TUNGKU SATE TANAH', 'BH', 90000.000, 0.000, '1', 0.000, 0.000, 4.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 90000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(1027, '00988', 4, 'BOWL', 'PCS', 110000.000, 0.000, '1', 0.000, 0.000, 1.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 110000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(1028, '00989', 4, 'MEMO KECIL', 'PAD', 1500.000, 100.000, '1', 100.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 1500.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 58005, 0),
(1029, '00990', 4, 'HAUSE KEEPING REPORT', 'RIM', 100000.000, 2.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 100000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(1030, '00991', 4, 'FORMULIR A', 'PAD', 17500.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 51003, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(1031, '00992', 3, 'TABU', 'BJ', 25000.000, 0.000, '1', 0.000, 0.000, 1.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 25000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(1032, '00993', 3, 'BUAH PALA', 'KG', 10000.000, 0.000, '1', 0.000, 0.000, 1.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 10000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(1033, '00994', 4, 'SERVICE MATRAS', 'PC', 250000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(1034, '00995', 4, 'GLASS CLOTH', 'PC', 26000.000, -36.000, '1', 72.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 26000.000, 12001, 61004, 61004, 61004, 61004, 61004, 54003, 61004, 0),
(1035, '00996', 5, 'DIET COKE', ' KL', 4583.000, -8.000, '1', 8.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 61004, 53002, 61004, 61004, 61004, 61004, 61004, 0),
(1036, '00997', 1, 'PARSEL', 'BH', 11350.000, 0.000, '1', 21.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 11350.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(1037, '00998', 4, 'KASUR', 'PCS', 3800000.000, 0.000, '1', 1.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 3800000.000, 2611, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(1038, '00999', 4, 'ANGGREK BULAN', 'BTG', 95000.000, -15.000, '1', 0.000, 46.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 1, 0, 0, 0, 0, 0, 0, 95000.000, 12001, 51005, 54003, 61004, 61004, 61004, 61004, 61004, 0),
(1039, '01000', 4, 'SEQUIS LANTAI', 'BH', 85000.000, -3.000, '1', 2.000, 0.000, 0.000, 0.000, NULL, 1.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(1040, '01001', 3, 'PENNE SPAGETHI', 'PCS', 25000.000, 0.000, '1', 0.000, 0.000, 2.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 25000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(1041, '01002', 1, 'KOL UNGU', 'KG', 65000.000, 0.000, '1', 0.000, 0.000, 1.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 65000.000, 12001, 61004, 61004, 61004, 61004, 61004, 53001, 61004, 0),
(1042, '01003', 1, 'ARCIS', 'KG', 48000.000, 0.000, '1', 0.000, 0.000, 2.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 48000.000, 12001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(1043, '01004', 1, 'BABY CORN', 'KG', 30000.000, 1.000, '1', 0.000, 0.000, 2.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 30000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1044, '01005', 2, 'CEKUH', 'KG', 30000.000, 0.000, '1', 0.000, 0.000, 2.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 30000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1045, '01006', 6, 'KERANJANG BAJU', 'PCS', 220000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 220000.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(1046, '01007', 4, 'FLOOR SIGN AWAS LANTAI LICIN', 'PCS', 52900.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(1047, '01008', 4, 'KESET LANTAI', 'PCS', 58410.000, 0.000, '1', 2.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 58410.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(1048, '01009', 4, 'FLOOR SIGN AWAS LANTAI LICIN', 'PCS', 52900.000, 0.000, '1', 5.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 52900.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1049, '01010', 4, 'TV. LED  LG 32 INCI', 'PCS', 2600000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 2615, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(1050, '01011', 6, 'JASA LAUNDRY CASA GLAN', 'NOTA 1 BLN', 14760900.000, -1.000, '1', 2.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 14760900.000, 52003, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(1051, '01012', 4, 'LILIN 5 CM', 'BOX', 385000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 385000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1052, '01013', 4, 'HCL 10 LITER', 'PAIL', 100000.000, -3.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 10.000, 0.000, 0, 0, 0, 0, 0, 0, 1, 0, 100000.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(1053, '01014', 1, 'CAEVES', 'KG', 80000.000, 0.000, '1', 0.000, 0.000, 1.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 80000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1054, '01015', 4, 'PEN', 'PC', 3000.000, 0.000, '1', 0.000, 0.000, 3.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 3000.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(1055, '01016', 4, 'SAPU DAUN', 'PCS', 75000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 75000.000, 52001, 61004, 61004, 61004, 61004, 61004, 61004, 61004, 0),
(1056, '01017', 4, 'BILL REST', 'BH', 12000.000, -7.000, '1', 0.000, 0.000, 0.000, 7.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1057, '01018', 3, 'BAKSO', 'KG', 60000.000, 0.000, '1', 0.000, 0.000, 1.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 60000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1058, '01019', 4, 'PEMANGGANGAN', 'PCS', 15000.000, 0.000, '1', 0.000, 0.000, 3.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 1, 0, 0, 0, 0, 0, 15000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1059, '01020', 4, 'PEPER CLIP NO 5', 'PAK', 4000.000, -5.000, '1', 0.000, 2.000, 0.000, 3.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 4000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1060, '01021', 10, 'PILLOW CASEs ( salah )', 'pcs', 0.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1061, '01022', 4, 'BOTOL BEER L', 'I BTL', 2000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1062, '01023', 4, 'BOTOL BEER K', '1 BTL', 1300.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 1300.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1063, '01024', 4, 'KRAN GAS', 'BH', 25000.000, -1.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 1.000, 0, 0, 0, 0, 0, 0, 0, 0, 25000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1064, '01025', 4, 'COMPRESOR SWAN', 'BH', 120000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 120000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1065, '01026', 4, 'TOMBOL KRAN TEMO', 'UNIT', 40000.000, -1.000, '1', 0.000, 0.000, 1.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 40000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1066, '01027', 4, 'BESI 8 MM', 'BTG', 46000.000, -1.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 1.000, 0, 0, 0, 0, 0, 0, 0, 0, 46000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1067, '01028', 4, 'KASUT', 'PC', 12000.000, -1.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 1.000, 0, 0, 0, 0, 0, 0, 0, 0, 12000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1068, '01029', 4, 'PASIR', 'KAMPIL', 50000.000, -1.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 1.000, 0, 0, 0, 0, 0, 0, 0, 0, 50000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1069, '01030', 4, 'PLASTIK BENING', 'M', 18000.000, -4.000, '1', 0.000, 0.000, 4.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 18000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1070, '01031', 4, 'TISU NIPKIN', 'PC', 3333.000, -340.000, '1', 0.000, 0.000, 340.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1071, '01032', 4, 'JASA PENJOR LAUNDRY', 'NOTA 1 BLN', 1212500.000, -1.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 1.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 1212500.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1072, '01033', 4, 'RANGKAIAN SPARO', 'BH', 40000.000, -1.000, '1', 2.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 1, 0, 0, 0, 0, 0, 0, 0, 40000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1073, '01034', 3, 'JINTEN', 'KG', 100000.000, -0.900, '1', 0.000, 0.000, 1.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 100000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1074, '01035', 3, 'ALPAPA', 'KG', 40000.000, 0.000, '1', 0.000, 0.000, 1.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 40000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1075, '01036', 4, 'TRIPLEX 9mm', 'LBR', 160000.000, -1.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 1.000, 0, 0, 0, 0, 0, 0, 0, 0, 160000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1076, '01037', 4, 'TRIPLEX 6mm', 'LBR', 100000.000, -1.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 1.000, 0, 0, 0, 0, 0, 0, 0, 0, 100000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1077, '01038', 4, 'CAT NIPPE PUTIH', 'KLG', 65000.000, -1.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 1.000, 0, 0, 0, 0, 0, 0, 0, 0, 65000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1078, '01039', 4, 'AMPLAS 100', 'MTR', 13000.000, -2.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 2.000, 0, 0, 0, 0, 0, 0, 0, 0, 13000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1079, '01040', 3, 'MEZZ CAPOTE CAP 1602', 'KLG', 105000.000, -1.000, '1', 0.000, 0.000, 1.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 105000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1080, '01041', 3, 'PENE', 'KLG', 26000.000, -1.000, '1', 0.000, 0.000, 1.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 26000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1081, '01042', 4, 'RODA', 'SET', 100000.000, -1.000, '1', 1.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 100000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1082, '01043', 4, 'GERGAJI BESI', 'SET', 13000.000, -1.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 1.000, 0, 0, 0, 0, 0, 0, 0, 0, 13000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1083, '01044', 3, 'STAR ANIS', 'ONS', 8000.000, -1.000, '1', 0.000, 0.000, 1.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 80000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1084, '01045', 4, 'JASA LOUNDRY NIRWANA', 'SATU BULAN', 1617000.000, -1.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 1.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 1617000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1085, '01046', 4, 'DAILY MARKET LIST', 'BK', 20000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1086, '01047', 3, 'SUSU ULTRA', 'DZ', 185000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 185000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1087, '01048', 4, 'STELLA REFILL', 'PAK', 28500.000, -2.000, '1', 0.000, 0.000, 0.000, 2.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 28500.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1088, '01049', 4, 'PLASTIC STAND 2 SISI', 'PCS', 60000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1089, '01050', 4, 'DND SIGN(door attention)', 'PCS', 40000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1090, '01051', 4, 'GLASS MELAMINE', 'PC', 17500.000, 0.000, '1', 12.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 17500.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1091, '01052', 4, 'MAINTENANCE ORDER', 'PAD', 12000.000, 0.000, '1', -1.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1092, '01053', 4, 'PARCEL MOTIF', 'PAK', 44840.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 44840.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1093, '01054', 4, 'PITA KADO', 'PCS', 1490.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 1490.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1094, '01055', 4, 'MAP PLASTIK TRANSPARAN', 'PCS', 2000.000, 0.000, '1', 0.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1095, '01056', 3, 'KULIT BABI', 'KG', 65000.000, 0.000, '1', 0.000, 0.000, 3.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 65000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1096, '01057', 3, 'SAMSAM BABI', 'KG', 65000.000, 0.000, '1', 0.000, 0.000, 3.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 65000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1097, '01058', 2, 'ARES', 'BTG', 20000.000, 0.000, '1', 0.000, 0.000, 1.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 20000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1098, '01059', 3, 'DAGING AYAM WHOLE', 'KG', 30000.000, -0.500, '1', 0.000, 0.000, 5.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 30000.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0),
(1099, '01060', 17, 'SANDAL JEPIT', 'PASANG', 12050.000, -6.000, '1', 6.000, 0.000, 0.000, 0.000, NULL, 0.000, 0.000, 0.000, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0.000, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 12001, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbmasterbill`
--

CREATE TABLE `tbmasterbill` (
  `kodemasterbil` varchar(15) NOT NULL,
  `tgl` datetime DEFAULT NULL,
  `bilto` varchar(50) DEFAULT NULL,
  `charge` double(15,2) DEFAULT NULL,
  `payment` double(15,2) DEFAULT NULL,
  `balance` double(15,2) DEFAULT NULL,
  `idoperator` varchar(50) DEFAULT NULL,
  `isroombil` int(1) DEFAULT NULL,
  `idguest` varchar(50) DEFAULT NULL,
  `idreservation` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbmasterbilldetil`
--

CREATE TABLE `tbmasterbilldetil` (
  `id` int(10) NOT NULL,
  `kodemasterbil` varchar(50) DEFAULT NULL,
  `idreservation` varchar(50) DEFAULT NULL,
  `iddetiltrx` varchar(50) DEFAULT NULL,
  `invoicemanual` varchar(50) DEFAULT NULL,
  `idroom` varchar(15) DEFAULT NULL,
  `tgl` datetime DEFAULT NULL,
  `ischarge` int(1) DEFAULT NULL,
  `idarticle` varchar(15) DEFAULT NULL,
  `articlename` text DEFAULT NULL,
  `desc` text DEFAULT NULL,
  `charge` double(15,2) DEFAULT NULL,
  `payment` double(15,2) DEFAULT NULL,
  `idoperator` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbmasterjurnal`
--

CREATE TABLE `tbmasterjurnal` (
  `IdJurnal` varchar(20) NOT NULL,
  `BuktiTransaksi` varchar(100) DEFAULT NULL,
  `Keterangan` varchar(150) DEFAULT NULL,
  `TanggalInput` datetime DEFAULT NULL,
  `TanggalSistem` date DEFAULT NULL,
  `KodeOperator` varchar(20) DEFAULT NULL,
  `IsAktif` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbmenu`
--

CREATE TABLE `tbmenu` (
  `id` int(11) NOT NULL,
  `idmenusubcategory` int(11) DEFAULT NULL,
  `menu` varchar(200) DEFAULT NULL,
  `description` tinytext DEFAULT NULL,
  `price` double(20,2) DEFAULT NULL,
  `tax` double(20,2) DEFAULT NULL,
  `service` double(20,2) DEFAULT NULL,
  `finalprice` double(20,2) DEFAULT NULL,
  `isaktif` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `tbmenu`
--

INSERT INTO `tbmenu` (`id`, `idmenusubcategory`, `menu`, `description`, `price`, `tax`, `service`, `finalprice`, `isaktif`) VALUES
(1, 1, 'MEAL CUPON', '-', 64935.00, 6818.00, 3247.00, 75000.00, 1),
(2, 1, 'CONTINENTAL BREAKFAST', '-', 82644.63, 8264.46, 9090.91, 100000.00, 1),
(3, 1, 'AMERICAN BREAKFAST', '-', 82644.63, 8264.46, 9090.91, 100000.00, 1),
(4, 10, 'SEAFOOD PAPAYA SALAD', '-', 40000.00, 4440.00, 4400.00, 48840.00, 1),
(5, 10, 'CHIKEN TANDORI SALAD', '-', 40000.00, 4440.00, 4400.00, 48840.00, 1),
(6, 3, 'GRILL SNAPPER', '-', 80000.00, 8800.00, 8000.00, 968000.00, 1),
(7, 3, 'TUNA STEAK BBQ SAUCE', '-', 80000.00, 8880.00, 8800.00, 97680.00, 1),
(8, 6, 'APPLE STRAWBERRY PIE', '-', 40000.00, 4440.00, 4400.00, 48840.00, 1),
(9, 6, 'TRIO PANCAKE', '-', 35000.00, 3885.00, 3850.00, 42735.00, 1),
(10, 7, 'SPYDER MAN', '-', 30000.00, 3300.00, 3000.00, 36300.00, 1),
(11, 7, 'SCOOBY DOO', '-', 30000.00, 3330.00, 3300.00, 36630.00, 1),
(12, 8, 'CHICKEN BURGER', '-', 50000.00, 5500.00, 5000.00, 60500.00, 1),
(13, 8, 'BEEF BURGER', '-', 55000.00, 6050.00, 5500.00, 66550.00, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbmenucategory`
--

CREATE TABLE `tbmenucategory` (
  `id` int(11) NOT NULL,
  `category` varchar(45) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `tbmenucategory`
--

INSERT INTO `tbmenucategory` (`id`, `category`, `description`) VALUES
(1, 'FOOD', 'food'),
(2, 'BEVERAGE', '-'),
(3, 'BREAKFAST', '-');

-- --------------------------------------------------------

--
-- Table structure for table `tbmenusubcategory`
--

CREATE TABLE `tbmenusubcategory` (
  `id` int(11) NOT NULL,
  `idcategory` int(11) DEFAULT NULL,
  `subcategory` varchar(200) DEFAULT NULL,
  `description` tinytext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `tbmenusubcategory`
--

INSERT INTO `tbmenusubcategory` (`id`, `idcategory`, `subcategory`, `description`) VALUES
(1, 3, 'BREAKFAST', '-'),
(2, 1, 'APPETISSER', '-'),
(3, 1, 'MAIN COURSE', '-'),
(4, 2, 'BEVERAGES', '-'),
(5, 1, 'SIDE ORDERS', '-'),
(6, 1, 'DESSERT', '-'),
(7, 1, 'KID MENU', '-'),
(8, 1, 'SANDWICHES AND BURGER', '-'),
(9, 1, 'TAPAS MENU', '-'),
(10, 1, 'SALAD', '-'),
(11, 1, 'SOUP', '-');

-- --------------------------------------------------------

--
-- Table structure for table `tbna`
--

CREATE TABLE `tbna` (
  `id` int(11) NOT NULL,
  `curr_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `tglna` datetime DEFAULT NULL,
  `op` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `tbna`
--

INSERT INTO `tbna` (`id`, `curr_date`, `tglna`, `op`) VALUES
(1, '2022-05-27 03:55:34', '2022-05-27 10:55:34', '7'),
(2, '2022-05-28 13:12:48', '2022-05-28 13:12:48', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbpackage`
--

CREATE TABLE `tbpackage` (
  `id` int(11) NOT NULL,
  `package` varchar(50) DEFAULT NULL,
  `packagerate` double(15,2) DEFAULT NULL,
  `remark` varchar(200) DEFAULT NULL,
  `article` varchar(50) DEFAULT NULL,
  `articlerate` double(20,2) DEFAULT NULL,
  `idx` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `tbpackage`
--

INSERT INTO `tbpackage` (`id`, `package`, `packagerate`, `remark`, `article`, `articlerate`, `idx`) VALUES
(5, 'Room', 400000.00, '-', 'Room', 400000.00, 10);

-- --------------------------------------------------------

--
-- Table structure for table `tbpayment`
--

CREATE TABLE `tbpayment` (
  `voucherno` varchar(50) NOT NULL,
  `voucherdate` date DEFAULT NULL,
  `vendor` varchar(100) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `paymentmethod` int(1) DEFAULT NULL,
  `bank` varchar(100) DEFAULT NULL,
  `checkno` varchar(100) DEFAULT NULL,
  `total` double(20,2) DEFAULT NULL,
  `idoperator` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbpaymentdetil`
--

CREATE TABLE `tbpaymentdetil` (
  `id` int(11) NOT NULL,
  `voucherno` varchar(50) DEFAULT NULL,
  `amount` double(20,2) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `rodate` date DEFAULT NULL,
  `kodero` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbpo`
--

CREATE TABLE `tbpo` (
  `kodepo` varchar(20) NOT NULL,
  `idvendor` int(11) DEFAULT NULL,
  `vendor` varchar(200) DEFAULT NULL,
  `dateorder` datetime DEFAULT NULL,
  `idoperator` int(11) DEFAULT NULL,
  `description` tinytext DEFAULT NULL,
  `total` double(20,3) DEFAULT NULL,
  `dateexpected` datetime DEFAULT NULL,
  `method` int(11) DEFAULT 1,
  `ischecked` int(1) DEFAULT 0,
  `status` int(1) DEFAULT 0,
  `catatan` text DEFAULT NULL,
  `verifikatorby` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbpodetil`
--

CREATE TABLE `tbpodetil` (
  `id` int(11) NOT NULL,
  `kodepo` varchar(20) DEFAULT NULL,
  `codeitem` varchar(45) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `unit` varchar(45) DEFAULT NULL,
  `price` double(20,2) DEFAULT NULL,
  `qtyorder` double(10,3) DEFAULT NULL,
  `qtystok` double(10,3) DEFAULT NULL,
  `subtotal` double(20,2) DEFAULT NULL,
  `note` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbpr`
--

CREATE TABLE `tbpr` (
  `kodepr` varchar(20) NOT NULL,
  `iddepartement` int(11) DEFAULT NULL,
  `departement` varchar(50) DEFAULT NULL,
  `dateadd` datetime DEFAULT NULL,
  `idoperator` int(11) DEFAULT NULL,
  `description` tinytext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbprdetil`
--

CREATE TABLE `tbprdetil` (
  `id` int(11) NOT NULL,
  `kodepr` varchar(20) DEFAULT NULL,
  `codeitem` varchar(11) DEFAULT NULL,
  `description` tinytext DEFAULT NULL,
  `unit` varchar(45) DEFAULT NULL,
  `price` double(20,3) DEFAULT NULL,
  `qtyorder` double(10,3) DEFAULT NULL,
  `currstok` double(10,3) DEFAULT NULL,
  `iddepartement` int(11) DEFAULT NULL,
  `departement` varchar(50) DEFAULT NULL,
  `note` text DEFAULT NULL,
  `idoperator` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbpurpose`
--

CREATE TABLE `tbpurpose` (
  `id` int(11) NOT NULL,
  `purpose` varchar(100) DEFAULT NULL,
  `isaktif` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `tbpurpose`
--

INSERT INTO `tbpurpose` (`id`, `purpose`, `isaktif`) VALUES
(1, 'Bussiness', 1),
(2, 'Honeymoon', 1),
(3, 'Vacation', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbreservation`
--

CREATE TABLE `tbreservation` (
  `idreservation` varchar(15) NOT NULL,
  `inputdate` datetime DEFAULT NULL,
  `isgroup` int(11) DEFAULT NULL,
  `idparentgroup` varchar(15) DEFAULT NULL,
  `idroomtype` int(11) DEFAULT NULL,
  `idroom` int(11) DEFAULT NULL,
  `idreservationstatus` int(11) DEFAULT NULL,
  `idguest` varchar(45) DEFAULT NULL,
  `adult` int(11) DEFAULT NULL,
  `child` int(11) DEFAULT NULL,
  `checkin` datetime DEFAULT NULL,
  `checkout` datetime DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `idpurpose` int(11) DEFAULT NULL,
  `idsource` int(11) DEFAULT NULL,
  `idsegment` int(11) DEFAULT NULL,
  `isvip` int(11) DEFAULT NULL,
  `statusreservation` int(11) DEFAULT 1,
  `notes` tinytext DEFAULT NULL,
  `msg` tinytext DEFAULT NULL,
  `idcompany` int(11) DEFAULT NULL,
  `deposit` double(20,2) DEFAULT NULL,
  `kodeop` int(11) DEFAULT NULL,
  `cod` datetime DEFAULT NULL,
  `idagent` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `tbreservation`
--

INSERT INTO `tbreservation` (`idreservation`, `inputdate`, `isgroup`, `idparentgroup`, `idroomtype`, `idroom`, `idreservationstatus`, `idguest`, `adult`, `child`, `checkin`, `checkout`, `duration`, `idpurpose`, `idsource`, `idsegment`, `isvip`, `statusreservation`, `notes`, `msg`, `idcompany`, `deposit`, `kodeop`, `cod`, `idagent`) VALUES
('RS22050001', '2022-05-28 10:31:26', 0, '0', 10, 2, 1, '1', 1, 0, '2022-05-29 00:00:00', '2022-05-31 00:00:00', 2, 1, 4, 1, 0, 1, '-', '-', 0, 0.00, 1, '2022-05-28 00:00:00', 1),
('RS22050002', '2022-05-28 10:37:19', 0, '0', 11, 6, 4, '1', 1, 0, '2022-05-28 00:00:00', '2022-05-29 00:00:00', 1, 2, 5, 2, 0, 1, '-', '-', 1, 0.00, 1, '2022-05-28 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbreservationdetil`
--

CREATE TABLE `tbreservationdetil` (
  `id` int(10) NOT NULL,
  `idreservation` varchar(50) DEFAULT NULL,
  `nightk` int(11) DEFAULT NULL,
  `datep` datetime DEFAULT NULL,
  `idroomtype` varchar(45) DEFAULT NULL,
  `idroom` varchar(45) DEFAULT NULL,
  `rate` double(20,2) DEFAULT NULL,
  `remarkpackage` varchar(45) DEFAULT NULL,
  `isruning` int(11) DEFAULT 0,
  `idpackage` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `tbreservationdetil`
--

INSERT INTO `tbreservationdetil` (`id`, `idreservation`, `nightk`, `datep`, `idroomtype`, `idroom`, `rate`, `remarkpackage`, `isruning`, `idpackage`) VALUES
(1, 'RS22050001', 1, '2022-05-29 00:00:00', '10', '2', 0.00, '-', 0, '5'),
(2, 'RS22050001', 2, '2022-05-29 00:00:00', '10', '2', 0.00, '-', 0, '5'),
(3, 'RS22050002', 1, '2022-05-28 00:00:00', '11', '6', 0.00, '-', 1, '5');

-- --------------------------------------------------------

--
-- Table structure for table `tbreservationguestlist`
--

CREATE TABLE `tbreservationguestlist` (
  `idguest` int(11) DEFAULT NULL,
  `idreservation` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `tbreservationguestlist`
--

INSERT INTO `tbreservationguestlist` (`idguest`, `idreservation`) VALUES
(1, 'RS22050001'),
(1, 'RS22050002');

-- --------------------------------------------------------

--
-- Table structure for table `tbreservationstatus`
--

CREATE TABLE `tbreservationstatus` (
  `id` int(11) NOT NULL,
  `status` varchar(50) DEFAULT NULL,
  `description` tinytext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `tbreservationstatus`
--

INSERT INTO `tbreservationstatus` (`id`, `status`, `description`) VALUES
(1, '6 PM', 'Tamu tidak datang sampai jam 6 sore, maka bookingannya di cancel'),
(2, 'Tentative', 'Apabila Night Audit akan di running tapi tamu belum datang, maka harus di cancel'),
(3, 'Guaranteed', 'Tamu pasti datang dan sudah memberikan deposit sebelumnya. Apabila tidak datang maka tetap harus di check in kan, dan kamarnya tidak boleh dijual'),
(4, 'In House', 'Tamu sudah di kamar'),
(5, 'Out', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbro`
--

CREATE TABLE `tbro` (
  `kodero` varchar(20) NOT NULL,
  `kodepo` varchar(45) DEFAULT NULL,
  `datereceive` datetime DEFAULT NULL,
  `idvendor` int(11) DEFAULT NULL,
  `vendor` varchar(100) DEFAULT NULL,
  `idoperator` int(11) DEFAULT NULL,
  `operator` varchar(100) DEFAULT NULL,
  `isinvoicing` int(11) DEFAULT NULL,
  `noreceipt` varchar(100) DEFAULT NULL,
  `total` double(20,2) DEFAULT NULL,
  `disc` double(20,2) DEFAULT NULL,
  `totalafterdisc` double(20,2) DEFAULT NULL,
  `iscredit` int(11) DEFAULT NULL,
  `duedate` datetime DEFAULT NULL,
  `ispaid` int(11) DEFAULT NULL,
  `dp` double(20,2) DEFAULT NULL,
  `remain` double(20,2) DEFAULT NULL,
  `iddepartement` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbrodetil`
--

CREATE TABLE `tbrodetil` (
  `id` int(11) NOT NULL,
  `kodero` varchar(20) NOT NULL,
  `itemcode` varchar(10) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `unit` varchar(20) DEFAULT NULL,
  `price` double(20,2) DEFAULT NULL,
  `qtyorder` double(20,2) DEFAULT NULL,
  `qty` double(20,2) DEFAULT NULL,
  `subtotal` double(20,2) DEFAULT NULL,
  `stok` double(20,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbroom`
--

CREATE TABLE `tbroom` (
  `id` int(11) NOT NULL,
  `idroomtype` int(11) DEFAULT NULL,
  `idroomstatus` int(11) DEFAULT NULL,
  `roomname` varchar(45) DEFAULT NULL,
  `desc` varchar(200) DEFAULT NULL,
  `isaktif` int(11) DEFAULT 1,
  `currtimestamp` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `tbroom`
--

INSERT INTO `tbroom` (`id`, `idroomtype`, `idroomstatus`, `roomname`, `desc`, `isaktif`, `currtimestamp`) VALUES
(1, 10, 1, 'Villa Kayon River 1', '-', 1, '2019-02-10 01:00:09'),
(2, 10, 1, 'Villa Kayon River 2', '-', 1, '2019-02-10 01:00:25'),
(3, 10, 1, 'Villa Kayon River 3', '-', 1, '2019-02-10 01:00:42'),
(4, 10, 1, 'Villa Kayon River 4', '-', 1, '2019-02-10 01:01:02'),
(5, 10, 1, 'Villa Kayon River 5', '-', 1, '2019-02-10 01:01:51'),
(6, 11, 1, '901', '-', 1, '2019-02-10 01:02:54'),
(7, 11, 1, '902', '-', 1, '2019-02-10 01:02:54'),
(8, 11, 1, '903', '-', 1, '2019-02-10 01:02:54'),
(9, 11, 1, '904', '-', 1, '2019-02-10 01:02:54'),
(10, 11, 1, '905', '-', 1, '2019-02-10 01:02:54'),
(11, 11, 1, '906', '-', 1, '2019-02-10 01:02:54'),
(12, 11, 1, '907', '-', 1, '2019-02-10 01:02:54'),
(13, 11, 1, '908', '-', 1, '2019-02-10 01:02:54'),
(14, 11, 1, '909', '-', 1, '2019-02-10 01:02:54'),
(15, 12, 1, '801', '-', 1, '2019-02-10 01:04:55'),
(16, 12, 1, '802', '-', 1, '2019-02-10 01:02:54'),
(17, 12, 1, '803', '-', 1, '2019-02-10 01:02:54'),
(18, 12, 1, '804', '-', 1, '2019-02-10 01:02:54'),
(19, 12, 1, '805', '-', 1, '2019-02-10 01:02:54'),
(20, 12, 1, '806', '-', 1, '2019-02-10 01:02:54');

-- --------------------------------------------------------

--
-- Table structure for table `tbroomstatus`
--

CREATE TABLE `tbroomstatus` (
  `id` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `last_update` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `tbroomstatus`
--

INSERT INTO `tbroomstatus` (`id`, `status`, `description`, `last_update`) VALUES
(1, 'VCC', 'Vacant Clean Checked', NULL),
(2, 'VCU', 'Vacant Clean Unchecked', NULL),
(3, 'VD', 'Vacant Dirty', NULL),
(4, 'OC', 'Occupied Clean', NULL),
(5, 'OD', 'Occupied Dirty', NULL),
(6, 'OO', 'Out of Orders', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbroomtype`
--

CREATE TABLE `tbroomtype` (
  `id` int(11) NOT NULL,
  `roomtype` varchar(50) DEFAULT NULL,
  `desc` varchar(200) DEFAULT NULL,
  `Rate` double(20,2) DEFAULT NULL,
  `isaktif` int(11) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `tbroomtype`
--

INSERT INTO `tbroomtype` (`id`, `roomtype`, `desc`, `Rate`, `isaktif`) VALUES
(10, 'Villa Kayon River', '-', 1000000.00, 1),
(11, 'Suite Valley', '-', 10000000.00, 1),
(12, 'Deluxe Room', '-', 1000000.00, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbsegment`
--

CREATE TABLE `tbsegment` (
  `id` int(11) NOT NULL,
  `segment` varchar(50) DEFAULT NULL,
  `isaktif` int(11) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `tbsegment`
--

INSERT INTO `tbsegment` (`id`, `segment`, `isaktif`) VALUES
(1, 'FIT INDIVIDUAL', 1),
(2, 'FIT CORPORATE', 1),
(3, 'FIT TRAVEL AGENT', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbsetting`
--

CREATE TABLE `tbsetting` (
  `NamaPerusahaan` text DEFAULT NULL,
  `Alamat` text DEFAULT NULL,
  `NoTlp` text DEFAULT NULL,
  `KategoriAset` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `tbsetting`
--

INSERT INTO `tbsetting` (`NamaPerusahaan`, `Alamat`, `NoTlp`, `KategoriAset`) VALUES
('PMS', '-', '+62 361 288 060', 5);

-- --------------------------------------------------------

--
-- Table structure for table `tbsettingakun`
--

CREATE TABLE `tbsettingakun` (
  `ID` varchar(10) NOT NULL,
  `NamaTransaksi` varchar(50) DEFAULT NULL,
  `AkunDebet` varchar(10) DEFAULT NULL,
  `AkunKredit` varchar(10) DEFAULT NULL,
  `IsTemplate` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `tbsettingakun`
--

INSERT INTO `tbsettingakun` (`ID`, `NamaTransaksi`, `AkunDebet`, `AkunKredit`, `IsTemplate`) VALUES
('1', 'A/Suplier', '21001', '13001', '0'),
('2', 'Inventory Cash', '13001', '11001', '0'),
('3', 'A/Suplier with DP', '13001', '11001', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbsource`
--

CREATE TABLE `tbsource` (
  `id` int(11) NOT NULL,
  `source` varchar(100) DEFAULT NULL,
  `isaktif` int(11) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `tbsource`
--

INSERT INTO `tbsource` (`id`, `source`, `isaktif`) VALUES
(4, 'Facebook', 1),
(5, 'Website', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbsr`
--

CREATE TABLE `tbsr` (
  `kodesr` varchar(20) NOT NULL,
  `iddepartement` int(11) DEFAULT NULL,
  `departement` varchar(50) DEFAULT NULL,
  `dateadd` datetime DEFAULT NULL,
  `idoperator` int(11) DEFAULT NULL,
  `description` tinytext DEFAULT NULL,
  `statusInOut` int(11) DEFAULT 0,
  `isSO` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbsrdetil`
--

CREATE TABLE `tbsrdetil` (
  `id` int(11) NOT NULL,
  `kodesr` varchar(20) DEFAULT NULL,
  `codeitem` varchar(11) DEFAULT NULL,
  `description` tinytext DEFAULT NULL,
  `unit` varchar(45) DEFAULT NULL,
  `price` double(20,3) DEFAULT NULL,
  `qtyorder` double(10,3) DEFAULT NULL,
  `currstok` double(10,3) DEFAULT NULL,
  `iddepartement` int(11) DEFAULT NULL,
  `departement` varchar(50) DEFAULT NULL,
  `idoperator` int(11) DEFAULT NULL,
  `statusInOut` int(11) DEFAULT 0,
  `isSO` int(11) DEFAULT 0,
  `note` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbstokopname`
--

CREATE TABLE `tbstokopname` (
  `id` int(10) NOT NULL,
  `date` datetime DEFAULT NULL,
  `itemcode` varchar(50) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `unit` varchar(50) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `stok` double DEFAULT NULL,
  `inputstok` double DEFAULT NULL,
  `diff` double DEFAULT NULL,
  `iddepartement` int(10) DEFAULT NULL,
  `departement` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbsubcategory`
--

CREATE TABLE `tbsubcategory` (
  `idsubcategory` int(11) NOT NULL,
  `idcategory` int(11) DEFAULT NULL,
  `subcategory` varchar(100) DEFAULT NULL,
  `isaktif` int(11) DEFAULT 1,
  `description` tinytext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `tbsubcategory`
--

INSERT INTO `tbsubcategory` (`idsubcategory`, `idcategory`, `subcategory`, `isaktif`, `description`) VALUES
(1, 1, 'FRUIT', 1, 'FRUIT'),
(2, 1, 'VAGETABLE', 1, 'VAGETABLE'),
(3, 2, 'FOOD', 1, 'FOOD'),
(4, 3, 'MATERIAL', 1, 'MATERIAL'),
(5, 2, 'BEVERAGE', 1, 'BEVERAGE'),
(6, 3, 'LOUNDRY', 1, 'LAUNDRY'),
(7, 4, 'KORAN', 1, 'KORAN'),
(8, 5, 'ACCOUNTING INVENTORY', 1, 'ACCOUNTING INVENTORY'),
(9, 5, 'FRONT OFFICE INVENTORY', 1, 'FRONT OFFICE INVENTORY'),
(10, 5, 'HK GUDANG INVENTORY', 1, 'HK GUDANG INVENTORY'),
(11, 5, 'BAR INVENTORY', 1, 'BAR INVENTORY'),
(12, 5, 'SECURITY INVENTORY', 1, 'SECURITY INVENTORY'),
(13, 5, 'GARDENER AND POOL INVENTORY', 1, 'GARDENER AND POOL INVENTORY'),
(14, 5, 'ENGINEERING INVENTORY', 1, 'ENGINEERING INVENTORY'),
(15, 5, 'HK LINENS INVENTORY', 1, 'HK LINENS INVENTORY'),
(16, 5, 'CLEANING EQUIPMENT INVENTORY', 1, 'CLEANING EQUIPMENT INVENTORY'),
(17, 5, 'AMENITIES INVENTORY', 1, 'AMENITIES INVENTORY'),
(18, 5, 'CHEMICALS INVENTORY', 1, 'CHEMICALS INVENTORY'),
(19, 5, 'RESTAURANT INVENTORY', 1, 'RESTAURANT INVENTORY'),
(20, 5, 'MATERIALS KITCHEN INVENTORY', 1, 'MATERIALS KITCHEN INVENTORY');

-- --------------------------------------------------------

--
-- Table structure for table `tbsubcategorydepartement`
--

CREATE TABLE `tbsubcategorydepartement` (
  `id` int(10) NOT NULL,
  `iddepartement` int(10) DEFAULT NULL,
  `itemcode` varchar(50) DEFAULT NULL,
  `kodesubcategory` int(10) DEFAULT NULL,
  `desc` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `tbsubcategorydepartement`
--

INSERT INTO `tbsubcategorydepartement` (`id`, `iddepartement`, `itemcode`, `kodesubcategory`, `desc`) VALUES
(1, 1, '00003', 3, 'FOOD'),
(2, 1, '00004', 2, 'VAGETABLE');

-- --------------------------------------------------------

--
-- Table structure for table `tbsubklasifikasi`
--

CREATE TABLE `tbsubklasifikasi` (
  `IdSub` varchar(10) NOT NULL,
  `NamaSub` varchar(50) DEFAULT NULL,
  `IsAktif` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `tbsubklasifikasi`
--

INSERT INTO `tbsubklasifikasi` (`IdSub`, `NamaSub`, `IsAktif`) VALUES
('11', 'Harta Lancar', NULL),
('12', 'Persediaan', NULL),
('51', 'Kamar Kantor Depan', '1'),
('52', 'Kamar Tata Graha', '1'),
('53', 'HP Makan dan minuman', '1'),
('54', 'Biaya Mate & MIN', '1'),
('55', 'Biaya Telepon', '1'),
('56', 'Biaya Pool', '1'),
('57', 'Biaya Karyawan', '1'),
('58', 'Biaya Administrasi & Umum ', '1'),
('59', 'Biaya Pemasaran', '1'),
('61', 'Biaya Pemeliharaan dan Energi', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbtable`
--

CREATE TABLE `tbtable` (
  `id` int(10) NOT NULL,
  `tablenumber` varchar(100) DEFAULT NULL,
  `idguest` int(10) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `tbtable`
--

INSERT INTO `tbtable` (`id`, `tablenumber`, `idguest`) VALUES
(1, '1', 1),
(2, '2', 1),
(3, '3', 1),
(4, '4', 1),
(5, '5', 1),
(6, '6', 1),
(7, '7', 2),
(8, '8', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbvendor`
--

CREATE TABLE `tbvendor` (
  `idvendor` int(11) NOT NULL,
  `vendor` varchar(200) DEFAULT NULL,
  `address` tinytext DEFAULT NULL,
  `phone` varchar(200) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `contact` varchar(100) DEFAULT NULL,
  `isaktif` int(11) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `tbvendor`
--

INSERT INTO `tbvendor` (`idvendor`, `vendor`, `address`, `phone`, `email`, `contact`, `isaktif`) VALUES
(4, 'ANEKA LISTRIK', '-', '-', '', '-', 1),
(5, 'AMIDARMO', '-', '-', '', '-', 1),
(6, 'AROMA', '-', '-', '', '-', 1),
(7, 'BAHAGIA', '-', '-', '', '-', 1),
(8, 'BALIMOON', '-', '-', '', '-', 1),
(9, 'BUDI JAYA', '-', '-', '', '-', 1),
(10, 'CASA GLAN', '-', '-', '', '-', 1),
(11, 'DARMANINGSIH', '-', '-', '', '-', 1),
(12, 'DINETA JAYA', '-', '-', '', '-', 1),
(13, 'DAMARAJA', '-', '-', '', '-', 1),
(14, 'DUTA ORCHID', '-', '-', '', '-', 1),
(15, 'EKA JAYA', '-', '-', '', '-', 1),
(16, 'FAJAR MS', '-', '-', '', '-', 1),
(17, 'HATTEN WINE', '-', '-', '', '-', 1),
(18, 'KOPERASI SANTRIAN', '-', '-', '', '-', 1),
(19, 'KHARISMA PERDANA', '-', '-', '', '-', 1),
(20, 'MITRA BERKAT', '-', '-', '', '-', 1),
(21, 'MEGA CAHAYA', '-', '-', '', '-', 1),
(22, 'NIRWANA LAUNDRY', '-', '-', '', '-', 1),
(23, 'PENJOR LAUNDRY', '-', '-', '', '-', 1),
(24, 'PRATAMA', '-', '-', '', '-', 1),
(25, 'SALIMCO JAYA', '-', '-', '', '-', 1),
(26, 'SEDAP MALAM', '-', '-', '', '-', 1),
(27, 'SEKAR SARI', '-', '-', '', '-', 1),
(28, 'SUKSES PRATAMA', '-', '-', '', '-', 1),
(29, 'UDAKA SARI', '-', '-', '', '-', 1),
(30, 'WIJAYA/ADI SETIA', '-', '-', '', '-', 1),
(31, 'WISNU BAKERY', '-', '-', '', '-', 1),
(32, 'KEDONGANAN', '-', '-', '', '-', 1),
(33, 'VANESSA', '-', '-', '', '-', 1),
(34, 'HARDYS', 'JLN DANAU TAMBLINGAN', '', '', '', 1),
(35, 'PANDE PUTRI', 'JLN DANAU  BUYAN', '', '', '', 1),
(36, 'BALI SOFT', 'SESETAN', '081933067911', '', '', 1),
(37, 'ANING DARMO', 'JLN PULAU BANGKA', '221811', '', '', 1),
(38, 'TEGEH.AGUNG', '', '', '', '', 1),
(39, 'SEPLESH.BAKERY', '', '', '', '', 1),
(40, 'WIDURI', '', '', '', '', 1),
(41, 'HARRIS', '', '', '', '', 1),
(42, 'HVS', '', '', '', '', 1),
(43, 'SPBU', '', '', '', '', 1),
(44, 'MERCY PHOTO', '', '', '', '', 1),
(45, 'PARADISE', '', '', '', '', 1),
(46, 'BINTANG BALI INDAH', '', '', '', '', 1),
(47, 'BINTANG BALI INDAH', '', '', '', '', 1),
(48, 'bali COOL', '', '', '', '', 1),
(49, 'gung alit', '', '', '', '', 1),
(50, 'YUDIANTA', '', '', '', '', 1),
(51, 'NASIB AGENSY', '', '', '', '', 1),
(52, 'NASIB AGENSY', '', '', '', '', 1),
(53, 'HVS', '', '', '', '', 1),
(54, 'HVS', '', '', '', '', 1),
(55, 'BALI EKA', '', '', '', '', 1),
(56, 'CHESE WORKI', '', '', '', '', 1),
(57, 'COCA - COLA', '', '', '', '', 1),
(58, 'BALI ES', '', '', '', '', 1),
(59, 'MUTIARA MEDIKA', '', '', '', '', 1),
(60, 'TAMANSARI', '', '', '', '', 1),
(61, 'DEPO AIR MINUM', '', '', '', '', 1),
(62, 'PLASTIK CENTER', '', '', '', '', 1),
(63, 'SPLASH BAKERY', '', '', '', '', 1),
(64, 'HVS', '', '', '', '', 1),
(65, 'POPULAR ', '', '', '', '', 1),
(66, 'ADI SETIA', '', '', '', '', 1),
(67, 'CANGKIR', '', '', '', '', 1),
(68, 'WINTARA', '', '', '', '', 1),
(69, 'PUPULAR', '', '', '', '', 1),
(70, 'SINAR MAYURI', '', '', '', '', 1),
(71, 'INDRA', '', '', '', '', 1),
(72, 'DEWA GORDON', '', '', '', '', 1),
(73, 'EKA RAYA', '', '', '', '', 1),
(74, 'TIRTA INVESTAMA', '', '', '', '', 1),
(75, 'KARISMA PERDANA', '', '', '', '', 1),
(76, 'UDAKA ', '', '', '', '', 1),
(77, 'SANWAN', '', '', '', '', 1),
(78, 'CASA GLAN', '', '', '', '', 1),
(79, 'PURI PANGAN', '', '', '', '', 1),
(80, 'PAK ALE', '', '', '', '', 1),
(81, 'KOPRASI SHATI ARTA', '', '', '', '', 1),
(82, 'KARTIKA CAHAYA', '', '', '', '', 1),
(83, 'TOKO AN', '', '', '', '', 1),
(84, 'BU WAYAN', '', '', '', '', 1),
(85, 'PUTU GALUH', '', '', '', '', 1),
(86, 'KIOS BUDI JAYA', '', '', '', '', 1),
(87, 'TUKANG KUNCI', '', '', '', '', 1),
(88, 'PASAR SINDU', '', '', '', '', 1),
(89, 'DIRECT PAYMENT', NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_tax_report`
--

CREATE TABLE `tb_tax_report` (
  `idreservation` varchar(40) DEFAULT NULL,
  `name` text DEFAULT NULL,
  `state` text DEFAULT NULL,
  `room` text DEFAULT NULL,
  `checkin` varchar(40) DEFAULT NULL,
  `checkout` varchar(40) DEFAULT NULL,
  `night` int(5) DEFAULT NULL,
  `roomrate` double(13,2) DEFAULT NULL,
  `totalroom` double(13,2) DEFAULT NULL,
  `taxroom` double(13,2) DEFAULT NULL,
  `restaurant` double(13,2) DEFAULT NULL,
  `taxrestaurant` double(13,2) DEFAULT NULL,
  `spa` double(13,2) DEFAULT NULL,
  `taxspa` double(13,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(10) NOT NULL,
  `nama_user` tinytext DEFAULT NULL,
  `username` text DEFAULT NULL,
  `password` text DEFAULT NULL,
  `isAktif` int(1) DEFAULT 1,
  `Parent` varchar(30) DEFAULT NULL,
  `Id_Parent` int(15) DEFAULT NULL,
  `outlet_name` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `nama_user`, `username`, `password`, `isAktif`, `Parent`, `Id_Parent`, `outlet_name`) VALUES
(1, 'User', 'admin', '123', 1, '1', 1, 'PMS'),
(2, 'store', 'store', '123', 1, '1', 1, 'PMS'),
(3, 'akunting', 'akunting', '123', 1, '1', 1, 'PMS'),
(4, 'inventory', 'inventory', '123', 1, '1', 1, 'PMS'),
(5, 'front', 'front', '123', 1, '1', 3, NULL),
(6, 'verifikator', 'verifikator', '123', 1, '1', 1, NULL),
(7, 'Admin PMS', 'admin_pms', '123', 1, '1', 3, 'PMS DEMO'),
(8, 'Kayon 1', 'kayon1', '123', 1, '1', 3, 'The Kayon Resort'),
(9, 'Kayon 2', 'kayon2', '123', 1, '1', 3, 'The Kayon Jungle Resort');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `rbac_menu`
--
ALTER TABLE `rbac_menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indexes for table `rbac_role`
--
ALTER TABLE `rbac_role`
  ADD PRIMARY KEY (`id_role`);

--
-- Indexes for table `rbac_submenu`
--
ALTER TABLE `rbac_submenu`
  ADD PRIMARY KEY (`id_submenu`);

--
-- Indexes for table `rbac_submenu2`
--
ALTER TABLE `rbac_submenu2`
  ADD PRIMARY KEY (`id_submenu`);

--
-- Indexes for table `tbakun`
--
ALTER TABLE `tbakun`
  ADD PRIMARY KEY (`NoAkun`);

--
-- Indexes for table `tbarticlecategory`
--
ALTER TABLE `tbarticlecategory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbarticlepayment`
--
ALTER TABLE `tbarticlepayment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbarticlesales`
--
ALTER TABLE `tbarticlesales`
  ADD PRIMARY KEY (`idarticle`);

--
-- Indexes for table `tbaset`
--
ALTER TABLE `tbaset`
  ADD PRIMARY KEY (`idaset`);

--
-- Indexes for table `tbasetdetil`
--
ALTER TABLE `tbasetdetil`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbbadstok`
--
ALTER TABLE `tbbadstok`
  ADD PRIMARY KEY (`kodebs`);

--
-- Indexes for table `tbbadstokdetil`
--
ALTER TABLE `tbbadstokdetil`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbbank`
--
ALTER TABLE `tbbank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbcategory`
--
ALTER TABLE `tbcategory`
  ADD PRIMARY KEY (`idcategory`);

--
-- Indexes for table `tbcheck`
--
ALTER TABLE `tbcheck`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbcheckdetil`
--
ALTER TABLE `tbcheckdetil`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbcheckout`
--
ALTER TABLE `tbcheckout`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbcheckoutdetil`
--
ALTER TABLE `tbcheckoutdetil`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbcompany`
--
ALTER TABLE `tbcompany`
  ADD PRIMARY KEY (`idcompany`);

--
-- Indexes for table `tbdepartement`
--
ALTER TABLE `tbdepartement`
  ADD PRIMARY KEY (`iddepartement`);

--
-- Indexes for table `tbdetiljurnal`
--
ALTER TABLE `tbdetiljurnal`
  ADD PRIMARY KEY (`NoTxn`);

--
-- Indexes for table `tbfbtrx`
--
ALTER TABLE `tbfbtrx`
  ADD PRIMARY KEY (`trxid`);

--
-- Indexes for table `tbfbtrxdetil`
--
ALTER TABLE `tbfbtrxdetil`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbfotrx`
--
ALTER TABLE `tbfotrx`
  ADD PRIMARY KEY (`kodemaster`);

--
-- Indexes for table `tbfotrxdetil`
--
ALTER TABLE `tbfotrxdetil`
  ADD PRIMARY KEY (`kode`);

--
-- Indexes for table `tbguest`
--
ALTER TABLE `tbguest`
  ADD PRIMARY KEY (`idguest`);

--
-- Indexes for table `tbhistoryhutang`
--
ALTER TABLE `tbhistoryhutang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbhistoryitemprice`
--
ALTER TABLE `tbhistoryitemprice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbitem`
--
ALTER TABLE `tbitem`
  ADD PRIMARY KEY (`iditem`);

--
-- Indexes for table `tbmasterbill`
--
ALTER TABLE `tbmasterbill`
  ADD PRIMARY KEY (`kodemasterbil`);

--
-- Indexes for table `tbmasterbilldetil`
--
ALTER TABLE `tbmasterbilldetil`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbmasterjurnal`
--
ALTER TABLE `tbmasterjurnal`
  ADD PRIMARY KEY (`IdJurnal`);

--
-- Indexes for table `tbmenu`
--
ALTER TABLE `tbmenu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbmenucategory`
--
ALTER TABLE `tbmenucategory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbmenusubcategory`
--
ALTER TABLE `tbmenusubcategory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbna`
--
ALTER TABLE `tbna`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbpackage`
--
ALTER TABLE `tbpackage`
  ADD PRIMARY KEY (`idx`);

--
-- Indexes for table `tbpayment`
--
ALTER TABLE `tbpayment`
  ADD PRIMARY KEY (`voucherno`);

--
-- Indexes for table `tbpaymentdetil`
--
ALTER TABLE `tbpaymentdetil`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbpo`
--
ALTER TABLE `tbpo`
  ADD PRIMARY KEY (`kodepo`);

--
-- Indexes for table `tbpodetil`
--
ALTER TABLE `tbpodetil`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbpr`
--
ALTER TABLE `tbpr`
  ADD PRIMARY KEY (`kodepr`);

--
-- Indexes for table `tbprdetil`
--
ALTER TABLE `tbprdetil`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbpurpose`
--
ALTER TABLE `tbpurpose`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbreservation`
--
ALTER TABLE `tbreservation`
  ADD PRIMARY KEY (`idreservation`);

--
-- Indexes for table `tbreservationdetil`
--
ALTER TABLE `tbreservationdetil`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbreservationstatus`
--
ALTER TABLE `tbreservationstatus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbro`
--
ALTER TABLE `tbro`
  ADD PRIMARY KEY (`kodero`);

--
-- Indexes for table `tbrodetil`
--
ALTER TABLE `tbrodetil`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbroom`
--
ALTER TABLE `tbroom`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbroomstatus`
--
ALTER TABLE `tbroomstatus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbroomtype`
--
ALTER TABLE `tbroomtype`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbsegment`
--
ALTER TABLE `tbsegment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbsettingakun`
--
ALTER TABLE `tbsettingakun`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbsource`
--
ALTER TABLE `tbsource`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbsr`
--
ALTER TABLE `tbsr`
  ADD PRIMARY KEY (`kodesr`);

--
-- Indexes for table `tbsrdetil`
--
ALTER TABLE `tbsrdetil`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbstokopname`
--
ALTER TABLE `tbstokopname`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbsubcategory`
--
ALTER TABLE `tbsubcategory`
  ADD PRIMARY KEY (`idsubcategory`);

--
-- Indexes for table `tbsubcategorydepartement`
--
ALTER TABLE `tbsubcategorydepartement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbsubklasifikasi`
--
ALTER TABLE `tbsubklasifikasi`
  ADD PRIMARY KEY (`IdSub`);

--
-- Indexes for table `tbtable`
--
ALTER TABLE `tbtable`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbvendor`
--
ALTER TABLE `tbvendor`
  ADD PRIMARY KEY (`idvendor`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `rbac_menu`
--
ALTER TABLE `rbac_menu`
  MODIFY `id_menu` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `rbac_role`
--
ALTER TABLE `rbac_role`
  MODIFY `id_role` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `rbac_submenu`
--
ALTER TABLE `rbac_submenu`
  MODIFY `id_submenu` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `rbac_submenu2`
--
ALTER TABLE `rbac_submenu2`
  MODIFY `id_submenu` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `tbarticlecategory`
--
ALTER TABLE `tbarticlecategory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbarticlepayment`
--
ALTER TABLE `tbarticlepayment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbaset`
--
ALTER TABLE `tbaset`
  MODIFY `idaset` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbasetdetil`
--
ALTER TABLE `tbasetdetil`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbbadstokdetil`
--
ALTER TABLE `tbbadstokdetil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbbank`
--
ALTER TABLE `tbbank`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbcategory`
--
ALTER TABLE `tbcategory`
  MODIFY `idcategory` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbcheck`
--
ALTER TABLE `tbcheck`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbcheckdetil`
--
ALTER TABLE `tbcheckdetil`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbcheckoutdetil`
--
ALTER TABLE `tbcheckoutdetil`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbcompany`
--
ALTER TABLE `tbcompany`
  MODIFY `idcompany` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbdepartement`
--
ALTER TABLE `tbdepartement`
  MODIFY `iddepartement` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbdetiljurnal`
--
ALTER TABLE `tbdetiljurnal`
  MODIFY `NoTxn` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbfbtrxdetil`
--
ALTER TABLE `tbfbtrxdetil`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbfotrxdetil`
--
ALTER TABLE `tbfotrxdetil`
  MODIFY `kode` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbguest`
--
ALTER TABLE `tbguest`
  MODIFY `idguest` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbhistoryhutang`
--
ALTER TABLE `tbhistoryhutang`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbhistoryitemprice`
--
ALTER TABLE `tbhistoryitemprice`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbitem`
--
ALTER TABLE `tbitem`
  MODIFY `iditem` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1100;

--
-- AUTO_INCREMENT for table `tbmasterbilldetil`
--
ALTER TABLE `tbmasterbilldetil`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbmenu`
--
ALTER TABLE `tbmenu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tbmenucategory`
--
ALTER TABLE `tbmenucategory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbmenusubcategory`
--
ALTER TABLE `tbmenusubcategory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbna`
--
ALTER TABLE `tbna`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbpackage`
--
ALTER TABLE `tbpackage`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbpaymentdetil`
--
ALTER TABLE `tbpaymentdetil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbpodetil`
--
ALTER TABLE `tbpodetil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbprdetil`
--
ALTER TABLE `tbprdetil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbpurpose`
--
ALTER TABLE `tbpurpose`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbreservationdetil`
--
ALTER TABLE `tbreservationdetil`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbreservationstatus`
--
ALTER TABLE `tbreservationstatus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbrodetil`
--
ALTER TABLE `tbrodetil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbroom`
--
ALTER TABLE `tbroom`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `tbroomstatus`
--
ALTER TABLE `tbroomstatus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbroomtype`
--
ALTER TABLE `tbroomtype`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tbsegment`
--
ALTER TABLE `tbsegment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbsource`
--
ALTER TABLE `tbsource`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbsrdetil`
--
ALTER TABLE `tbsrdetil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbstokopname`
--
ALTER TABLE `tbstokopname`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbsubcategory`
--
ALTER TABLE `tbsubcategory`
  MODIFY `idsubcategory` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `tbsubcategorydepartement`
--
ALTER TABLE `tbsubcategorydepartement`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbtable`
--
ALTER TABLE `tbtable`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbvendor`
--
ALTER TABLE `tbvendor`
  MODIFY `idvendor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

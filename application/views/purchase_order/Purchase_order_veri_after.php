<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>
<style type="text/css">
.modal.modal-wide .modal-dialog {
  width: 90%;
}

.modal.modal-wideRevisi .modal-dialog {
  width: 60%;
}


.modal-wide .modal-body {
  overflow-y: auto;
}

#tallModal .modal-body p { margin-bottom: 900px }
.pull-right {
    float: right;
}

.pull-left {
    float: left;
}
 tr:hover td {background:#C2F2FF }

</style>

<section class="content-header">
    <div class="btn-group btn-breadcrumb">
            <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
            <a  class="btn btn-default  btn-xs active">Purchase Order List</a>
        </div>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Receive Item</h3>
           <a style="float:right"  href="<?php echo base_url('index.php/Purchase_order/Purchase_order_add');?>" class="btn btn-primary">
            <i class="glyphicon glyphicon-saved"></i>
            Add PO
          </a>
        </div>
    <!-- /.box-header -->
        <div class="box-body">
        <div class="table-responsive">
          <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr >
                                        <th  style="display: none;"  >No</th>
                                        <th >PO No.</th>
                                        <th >Date</th>
                                        <th >Vendor</th>
                                        <th style="display: none;" >Description</th>
                                         <th style="display: none;" >Catatan</th>
                                        <th >Amount</th>
                                        <th >Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i=1;
                                    foreach($data as $u){
                                       $status = $u->status;
                                      if ($status==0) {
                                       $status = 'Belum Diverifikasi';
                                      }elseif ($status==1) {
                                       $status = 'Disetujui';
                                      }elseif ($status==2) {
                                        $status = 'Direvisi';
                                      }

                                    ?>

                                    <tr  class='odd gradeX context'>
                                        <td  style="display: none;"><?php echo $i ?></td>
                                        <td class="Kodepo"><?php echo $u->kodepo ?></td>
                                        <td class="Date"><?php echo $u->dateorder?></td>
                                        <td class="Vendor"><?php echo $u->vendor?></td>
                                        <td  style="display: none;" class="description"><?php echo $u->description?></td>
                                        <td  style="display: none;" class="catatan"><?php echo $u->catatan?></td>
                                        <td class="Total"><?php echo number_format($u->total, 0, '.', '.') ?></td>
                                         <td ><strong> <?php echo $status ?></strong></td>

                                         <?php if ($u->status==0 or $u->status==2 ) { ?>
                        <td align="center">
                          <a class="btn btn-warning btn-xs "  title="Revisi" href="<?php echo base_url('index.php/Purchase_order/purchase_order_edit/'.$u->kodepo); ?>">   <span class="fa fa-fw fa-edit" ></span> </a>
                           <a class="btn btn-info btn-xs ViewDetail"  title="lihat Detail">  <span class="fa fa-fw fa-list" ></span> </a>
                        </td>
                                        <?php }else{?>
                                         <td align="center"> &nbsp;</td>

                                        <?php }?>

                                        
                                      






                                    </tr>
                                    <?php $i++;} ?>
                                </tbody>
                            </table>
          </div>



        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->

</section><!-- /.content -->

  <div class="modal modal-wide fade" id="myModalNew" role="dialog" style="display:none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" title="close" data-dismiss="modal">&times;</button>
                    <h4 align="center" class="modal-title">Detail PO</h4>
                    <table class="table" style="background-color: #00acd6; border: 1px solid black; "  >
                <tr>
                  <td style="border: 1px solid white; background-color: #FFFFAB; ">Catatan </td>
                  <td  id ="Catatam" colspan="5" style="background-color: #FFFFAB; border: 1px solid white; " ><strong> <span></span> </strong> </td>
                  
                </tr>

                <tr>
                  <td width="10%" style="border: 1px solid white; ">No PO </td>
                  <td  width="25%" id ="Kodepom" style="background-color: #00acd6; border: 1px solid white; " ><strong> <span></span> </strong> </td>
                  <td width="10%" style="border: 1px solid white; ">Vendor</td>
                  <td  width="25%" id ="Vendorm" style="background-color: #00acd6; border: 1px solid white; " ><strong> <span></span> </strong> </td>
                  <td width="10%" style="border: 1px solid white; ">Total</td>
                  <td  width="20%" id ="Totalm" style="background-color: #00acd6; border: 1px solid white; " ><strong> <span></span> </strong> </td>
                </tr>
              </table>
                </div>
              
                <div class="modal-body">   
                </div>
            </div>
        </div>     
    </div>

     <div class="modal modal-wideRevisi fade " id="myModalRevisi" role="dialog" style="display:none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" title="close" data-dismiss="modal">&times;</button>
                    <h4 align="center" class="modal-title">Detail PO</h4>
                    <table class="table" style="background-color: #00acd6; border: 1px solid black; "  >
                <tr>
                  <td width="10%" style="border: 1px solid white; ">No PO </td>
                  <td  width="25%" id ="Kodepomrev" style="background-color: #00acd6; border: 1px solid white; " ><strong> <span></span> </strong> </td>
                  <td width="10%" style="border: 1px solid white; ">Vendor</td>
                  <td  width="25%" id ="Vendormrev" style="background-color: #00acd6; border: 1px solid white; " ><strong> <span></span> </strong> </td>
                  <td width="10%" style="border: 1px solid white; ">Total</td>
                  <td  width="20%" id ="Totalmrev" style="background-color: #00acd6; border: 1px solid white; " ><strong> <span></span> </strong> </td>
                </tr>
              </table>
                </div>
                <div class="modal-body">
                  <input type="text" id="NoPom">
                  <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>
                        <textarea rows="4" cols="70" id="Revisi" placeholder="Silahkan Masukkan Komentar Dibawah Ini..." ></textarea>
                      </label>
                    </div>
                  </div>
                </div>  
                 <div class="form-group">
                <a id="SimpanRevisi" class="btn btn-primary  fa fa-save"> Simpan</a>
              </div> 
                </div>
            </div>
        </div>     
    </div>




<?php
$this->load->view('template/Js');
$this->load->view('template/Foot');
?>


<script type="text/javascript">
  $(document).ready(function(){
     $(".ViewDetail").click(function(e){ 
      var kode = $(this).closest('tr').children('td.Kodepo').text(); 
       var total = $(this).closest('tr').children('td.Total').text(); 
       var vendor = $(this).closest('tr').children('td.Vendor').text();
        var catatan = $(this).closest('tr').children('td.catatan').text();     
      $.ajax({
      type: "POST",
       url: '<?php echo site_url('Purchase_order/purchase_order_list_veri_detil');?>',
        data: {kode:kode},
        success: function(data){
            e.preventDefault();
            var mymodal1 = $('#myModalNew');
            mymodal1.modal({backdrop: 'static', keyboard: false}) 
            var height = $(window).height() - 200;
            mymodal1.find(".modal-body").css("max-height", height);
             $("#Kodepom span").text(kode);
              $("#Totalm span").text(total);
               $("#Vendorm span").text(vendor);
                $("#Catatam span").text(catatan);
            mymodal1.find('.modal-body').html(data);
            mymodal1.modal('show');            
        }
      });
    });
  });
</script>

<script type="text/javascript">
  $(document).ready(function(){
     $(".Revisi").click(function(e){ 
     //var nomi = $('#IdDepartement').val();  
      var kode = $(this).closest('tr').children('td.Kodepo').text(); 
       var total = $(this).closest('tr').children('td.Total').text(); 
       var vendor = $(this).closest('tr').children('td.Vendor').text();     

            e.preventDefault();
            var mymodal1 = $('#myModalRevisi');
            mymodal1.modal({backdrop: 'static', keyboard: false}) 
            var height = $(window).height() - 200;
            mymodal1.find(".modal-body").css("max-height", height);
             $("#Kodepomrev span").text(kode);
              $("#Totalmrev span").text(total);
               $("#Vendormrev span").text(vendor);
                $("#NoPom").val(kode); 
           // mymodal1.find('.modal-body').html(data);
            mymodal1.modal('show');            
    });
  });
</script>


<script type="text/javascript">
$(".Setujui").click(function() {
        event.preventDefault();
         var kode = $(this).closest('tr').children('td.Kodepo').text(); 
        $.confirm({
          title: 'Confirmation',
          content: 'Are You Sure to Save?',
           type: 'blue',
          buttons: {
              Simpan: function () {
                    $.ajax({
                    type: "POST",
                   url: '<?php echo site_url('Purchase_order/purchase_order_add_setujui');?>',
                    data: {kode:kode  },
                    success: function(data){
                        window.location=data;            
                    }
                 });      
              },
              Batal: function () {
                
                  $.alert('Data Tidak Disimpan...');
              },
          }
      });

});
</script>

<script type="text/javascript">
$("#SimpanRevisi").click(function() {
        event.preventDefault();
         var kode = $('#NoPom').val(); 
         var catatan =$("#Revisi").val(); 
         alert(catatan);
        $.confirm({
          title: 'Confirmation',
          content: 'Are You Sure to Save?',
           type: 'blue',
          buttons: {
              Simpan: function () {
                    $.ajax({
                    type: "POST",
                   url: '<?php echo site_url('Purchase_order/purchase_order_add_revisi');?>',
                    data: {kode:kode,catatan:catatan  },
                    success: function(data){
                        window.location=data;            
                    }
                 });      
              },
              Batal: function () {
                
                  $.alert('Data Tidak Disimpan...');
              },
          }
      });

});
</script>



<script>
  $(function () {
    $("#example2").DataTable();
     $("#example11").DataTable();
    $('#example1').DataTable({
      "paging": false,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>

<?php

class Reservation extends CI_Controller{
	private $IdOperator;

	function __construct(){
		parent::__construct();
		if($this->session->userdata('Kode_userPms')==NULL) {
           redirect('Login');
		}
		$this->load->library('session');
		$this->load->helper(array('form', 'url'));
		$this->load->model(array('M_reservation'));

		$this->load->library(array('session','create_pdf','ciqrcode'));
		$this->load->model(array('M_reservation','M_rbac'));
		$this->IdOperator = $this->session->userdata('Kode_userPms');
		
		if (!empty($this->session->userdata("active_db"))) {
			$this->load->database($this->session->userdata("active_db"), FALSE, TRUE);
		}
	}

	function index(){
		//redirect('Reservation/ExpectedArrival_view');
	}

	function ExpectedArrival_view(){
		date_default_timezone_set('Asia/Hong_Kong');
		$this->load->view('reservation/Expected_arrival');
	}
	function ExpectedArrival_view_data(){
		date_default_timezone_set('Asia/Hong_Kong');

			$tglAwal = $this->input->post('tglAwal');
			$tglAkhir = $this->input->post('tglAkhir');

			if (!empty($tglAwal)) {
				$tglAwal1 = DateTime::createFromFormat('m/d/Y',$tglAwal);
				$tglAwal2 = $tglAwal1 ->format("Y-m-d");
				$tglAkhir1 = DateTime::createFromFormat('m/d/Y',$tglAkhir);
				$tglAkhir2 = $tglAkhir1 ->format("Y-m-d");
			}else{
				$tglAwal2="";
				$tglAkhir1 = DateTime::createFromFormat('m/d/Y',$tglAkhir);
				$tglAkhir2 = $tglAkhir1 ->format("Y-m-d");
			};
		$data['tglAwal'] = $tglAwal;
		$data['tglAkhir'] = $tglAkhir;
		$data['data'] = $this->M_reservation->Expected_arrival($tglAwal2, $tglAkhir2);
		//print_r($data['data']);exit();
		$this->load->view('reservation/Expected_arrival_data',$data);
	}

	function ExpectedDeparture_view(){
		date_default_timezone_set('Asia/Hong_Kong');
		$this->load->view('reservation/Expected_departure');

	}
	function ExpectedDeparture_view_data(){
		date_default_timezone_set('Asia/Hong_Kong');

			$tglAwal = $this->input->post('tglAwal');
			$tglAkhir = $this->input->post('tglAkhir');
			//print_r($Tawal);exit();
			if (!empty($Tawal)) {
				$tglAwal1 = DateTime::createFromFormat('m/d/Y',$tglAwal);
				$tglAwal2 = $tglAwal1 ->format("Y-m-d");
				$tglAkhir1 = DateTime::createFromFormat('m/d/Y',$tglAkhir);
				$tglAkhir2 = $tglAkhir1 ->format("Y-m-d");
			}else{
				$tglAwal="";
				$tglAkhir1 = DateTime::createFromFormat('m/d/Y',$tglAkhir);
				$tglAkhir2 = $tglAkhir1 ->format("Y-m-d");
			};
			//print_r($tglAkhir2.' '.$tglAkhir2);exit();
		$data['tglAwal'] = $tglAwal;
		$data['data'] = $this->M_reservation->Expected_departure($tglAwal2,$tglAkhir2);
		$this->load->view('reservation/Expected_departure_data',$data);

	}

	function GuestInHouse_view(){
		$data['data'] = $this->M_reservation->Guest_in_house();
		$this->load->view('reservation/Guest_in_house',$data);
	}

	function Cetak($JenisLaporan,$Text){
		$data['hotel'] = $this->M_rbac->getPerusahaan();
		$data['qr'] = $this->ciqrcode->generateQRcode($JenisLaporan, $Text,1.9);
		$data1= $this->load->view('template/Cetak_head',$data, TRUE);
		return $data1;
	}

	// ------------------ Cetak Laporan EA, ED, GH Start ---------------
	    function CetakLapEA(){
		    $tglAwal =  $this->input->get('TglAwal', TRUE);
			$tglAkhir = $this->input->get('TglAkhir', TRUE);
			if (!empty($tglAwal)) {
				$tglAwal1 = DateTime::createFromFormat('m/d/Y',$tglAwal);
				$tglAwal2 = $tglAwal1 ->format("Y-m-d");
				$tglAwal3 = $tglAwal1 ->format("d-m-Y");
				$tglAkhir1 = DateTime::createFromFormat('m/d/Y',$tglAkhir);
				$tglAkhir2 = $tglAkhir1 ->format("Y-m-d");
				$tglAkhir3 = $tglAkhir1 ->format("d-m-Y");
			}else{
				$tglAwal="";
				$tglAkhir1 = DateTime::createFromFormat('m/d/Y',$tglAkhir);
				$tglAkhir2 = $tglAkhir1 ->format("Y-m-d");
			};
				
				$data['header'] = $this->Cetak("Expected_arrival",  $this->session->userdata('Nama_userPms'). date("d-m-Y_H-i-s"), 1);
			 	$data['konten'] = $this->M_reservation->Expected_arrival($tglAwal2, $tglAkhir2);
				$data['periode'] = array('Tawal' => $tglAwal3, 'Takhir' => $tglAkhir3);
			 	$html=$this->load->view('reservation/Cetak/LapEA',$data, TRUE);
			 	print_r($html);exit();
		    $this->create_pdf->load($html,'Expected_arrival'.' - '.date("d-m-Y"), 'A4-L','');
			}

			function CetakLapED(){
				 $tglAwal =  $this->input->get('TglAwal', TRUE);
			$tglAkhir = $this->input->get('TglAkhir', TRUE);
			if (!empty($tglAwal)) {
				$tglAwal1 = DateTime::createFromFormat('m/d/Y',$tglAwal);
				$tglAwal2 = $tglAwal1 ->format("Y-m-d");
				$tglAwal3 = $tglAwal1 ->format("d-m-Y");
				$tglAkhir1 = DateTime::createFromFormat('m/d/Y',$tglAkhir);
				$tglAkhir2 = $tglAkhir1 ->format("Y-m-d");
				$tglAkhir3 = $tglAkhir1 ->format("d-m-Y");
			}else{
				$tglAwal="";
				$tglAkhir1 = DateTime::createFromFormat('m/d/Y',$tglAkhir);
				$tglAkhir2 = $tglAkhir1 ->format("Y-m-d");
			};

				$data['header'] = $this->Cetak("Expected_departure",  $this->session->userdata('Nama_userPms'). date("d-m-Y_H-i-s"), 1);
			 	$data['konten'] = $this->M_reservation->Expected_departure($tglAwal2,$tglAkhir2);
				$data['periode'] = array('Tawal' => $tglAwal3, 'Takhir' => $tglAkhir3);

			 	$html=$this->load->view('reservation/Cetak/LapED',$data, TRUE);
			 	print_r($html);exit();
		    $this->create_pdf->load($html,'Expected_departure'.' - '.date("d-m-Y"), 'A4-L','');
			}

			function CetakLapGH(){
				$tgl = date("d-m-Y");
				$data['header'] = $this->Cetak("Guest_in_house",  $this->session->userdata('Nama_userPms'). date("d-m-Y_H-i-s"), 1);
			 	$data['konten'] = $this->M_reservation->Guest_in_house();
				$data['periode'] = array('Tawal' => $tgl);

			 	$html=$this->load->view('reservation/Cetak/LapGH',$data, TRUE);
			 	print_r($html);exit();
		    $this->create_pdf->load($html,'Guest_in_house'.' - '.date("d-m-Y"), 'A4-L','');
			}

}
?>

<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
//$this->load->view('template/Topbar');
//$this->load->view('template/Sidebar');
?>
<script src="<?php echo base_url('assets/js/accounting.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/cleave.min.js') ?>" type="text/javascript"></script>

 <script type="text/javascript">
      jQuery(document).ready(function($) {
        new Cleave('.input-1', {
           numeral: true, 
           numeralDecimalMark: ',',
           delimiter: '.' 
      });
        new Cleave('.input-2', {
           numeral: true, 
           numeralDecimalMark: ',',
           delimiter: '.' 
      });
         new Cleave('.input-3', {
           numeral: true, 
           numeralDecimalMark: ',',
           delimiter: '.' 
      });
          });
    </script>

<style type="text/css">
	.col-hg{
		padding-bottom:5px;
		padding-left:2px;
		padding-right:2px;
	}
	.btn-float{
		width:35px; height:35px; float:left; margin-left:5px;
	}
.modal.modal-wide .modal-dialog {
  width: 80%;
}
.modal-wide .modal-body {
  overflow-y: auto;
}
	.label {
		font-weight:normal;
		color:#333;
		font-size:14px;
		line-height: 1.42857143;
		text-align:left;
	}
	.button-grn{
		background-color:#01b3a3;
		border-color:#01b3a3;
		color:#FFF;
	}
	.capt{
		border-bottom:#eee solid 1px; margin-bottom:10px
	}
	.btn-canc{
		height:75px; width:100%; background-color:#666; color:#FFF; font-size:20px;
	}
	.btn-checkin{
		height:75px; width:100%; background-color:#2970e4; color:#FFF; font-size:20px;
	}
    td.action { text-align: center; }
</style>


<!-- Main content -->
<section class="content">
<form method="post" id="Simpan"  action="<?php echo base_url(). 'index.php/Fo_outlet/Res_add'; ?>">
	<div class="row">
        <div class="col-lg-12" style=" background:#CCC; top:0; position:fixed; padding-top:5px; padding-bottom:5px; color:#FFF"><strong><a class="btn btn-warning " title="Update Data"  
                              href="<?php echo base_url('index.php/Fo_outlet/Reservation_list_view'); ?>">  <span class="fa fa-fw fa-backward" ></span>Back </a>
        </strong>
          
        </div>
        <div class="col-lg-4" style="margin-top:30px;">
        <!-- GUEST -->
        	<div class="col-sm-12 col-hg capt">GUEST</div>
        	<div class="col-sm-3 col-hg">
             <div align="right">Name :</div></div>
              <div class="col-sm-8 col-hg">
                <input type="text" id="NamaGuest" value="<?php echo $rsv->nama?>"  class="form-control"> 
                <input type="hidden"  id="IdGuest" name="IdGuest" value="<?php echo $rsv->idguest?>" 
                class="form-control" ></div>

            <div class="col-sm-1 col-hg">
              <a class="btn button-grn" id="AddGuest" title="Add Guest"><span class="fa fa-plus-circle"></span></a>
            </div>
            <div class="col-sm-3 col-hg">
              <div align="right">Phone :</div>
            </div>
            <div class="col-sm-8 col-hg">
              <input type="text" id="PhoneGuest" value="<?php echo $rsv->phone?>" class="form-control">
             </div>
            <div class="col-sm-1 col-hg"></div>
            <div class="col-sm-3 col-hg"></div>
            <div class="col-sm-8 col-hg" style="margin-bottom:10px;"><a  class="btn button-grn SelectGuest">Select Guest</a></div>
            <div class="col-sm-1 col-hg"></div>
            <div class="col-sm-3 col-hg"><div align="right">Source :</div></div>
            <div class="col-sm-8 col-hg">
               <select  class="form-control select2"  style="width: 100%;" id="Source" name="Source">
               <?php foreach($source as $peksur){ ?>
                   <?php if ( $rsv->idsource == $peksur->id ) { ?>
                    <option value="<?php echo $peksur->id; ?>" selected><?php echo $peksur->source; ?></option>
                    <?php  }?>
                        <option value="<?php echo $peksur->id; ?>" ><?php echo $peksur->source; ?></option>
                        <?php } ?>
               </select>
            </div>
            <div class="col-sm-3 col-hg"><div align="right">Purpose :</div></div>
            <div class="col-sm-8 col-hg">
              <select  class="form-control select2"  style="width: 100%;" id="Purpose" name="Purpose">
               <?php foreach($purpose as $pekpur){ ?>
                    <?php if ( $rsv->idpurpose == $pekpur->id ) { ?>
                    <option value="<?php echo $pekpur->id; ?>" selected><?php echo $pekpur->purpose; ?></option>
                    <?php  }?>
                        <option value="<?php echo $pekpur->id; ?>" ><?php echo $pekpur->purpose; ?></option>
                        <?php } ?>
               </select>
            </div>
          <div class="col-sm-3 col-hg"><div align="right">Segment :</div></div>
            <div class="col-sm-8 col-hg">
              <select  class="form-control select2"  style="width: 100%;" id="Segment" name="Segment">
               <?php foreach($segment as $pekseg){ ?>
                       <?php if ( $rsv->idsegment == $pekseg->id ) { ?>
                        <option value="<?php echo $pekseg->id; ?>" selected><?php echo $pekseg->segment; ?></option>
                        <?php  }?>

                        <option value="<?php echo $pekseg->id; ?>" ><?php echo $pekseg->segment; ?></option>
                  <?php } ?>
               </select>
            </div>
        	<div class="col-sm-1 col-hg"></div>
            <div class="col-sm-3 col-hg"><div align="right">Agent :</div></div>
            <div class="col-sm-8 col-hg">
              <select  class="form-control select2"  style="width: 100%;" id="Company" name="Company">
               <?php foreach($company as $pekcom){ ?>
                          <?php if ( $rsv->idcompany == $pekcom->id ) { ?>
                        <option value="<?php echo $pekcom->id; ?>" selected><?php echo $pekcom->name; ?></option>
                        <?php  }?>
                        <option value="<?php echo $pekcom->idcompany; ?>" ><?php echo $pekcom->name; ?></option>
                        <?php } ?>
               </select>
            </div>

            <div class="col-sm-1 col-hg"></div>
            <div class="col-sm-3 col-hg"><div align="right">VIP :</div></div>
            <div class="col-sm-8 col-hg">
              <?php if ( $rsv->isvip ==1 ) { ?>
             <input  readonly="readonly" type="checkbox" name="VIP1" class="flat-red" id="VIP1" checked="1"></span>
            <?php }else{?>
            <input  readonly="readonly" type="checkbox" name="VIP1" class="flat-red" id="VIP1" ></span>
            <?php } ?>
               </select>
            </div>

        	<div class="col-sm-1 col-hg"></div>
            <div class="col-sm-3 col-hg"><div align="right">Adult :</div></div>
            <div class="col-sm-8 col-hg">
            <input type="text" name = "Adult" class="form-control" style="width:60px; float:left"  value="<?php echo $rsv->adult?>">
            <!-- <button class="btn button-grn btn-float" style="font-size:18px">+</button>
            <button class="btn button-grn btn-float" style="font-size:18px">-</button> -->
            </div>
        	<div class="col-sm-1 col-hg"></div>
            <div class="col-sm-3 col-hg"><div align="right">Child :</div></div>
            <div class="col-sm-8 col-hg">
            <input type="text" name="Child" class="form-control" style="width:60px; float:left" value="<?php echo $rsv->child?>">
        
          
            <!-- <button class="btn button-grn btn-float" style="font-size:18px">+</button>
            <button class="btn button-grn btn-float" style="font-size:18px">-</button> -->
            </div>
        	<div class="col-sm-1 col-hg"></div>
            <div class="col-sm-3 col-hg"> 
            </div>
            <div class="col-sm-8 col-hg" style="margin-bottom:10px; display: none;">
              <a class="btn button-grn SelectGuestAll">Guest List</a> 
              <a class="btn button-grn " id="ViewGuest">  <span class="fa fa-fw fa-list" ></span> </a>
            </div>
            <div class="col-sm-1 col-hg"></div>
            
    	<!-- FLIGHT INFORMATION -->        
<div class="col-sm-12 col-hg capt" style="margin-top:20px; display: none;">FLIGHT INFORMATION</div>
  <div class="col-sm-3 col-hg">
    <div class="input-group" style="display: none;">
        <span class="input-group-addon"><input  readonly="readonly" type="checkbox" class="flat-red" checked></span>
        <label for="cbflg" class="form-control label">Pick Up</label>
    </div>
  </div>
<div class="col-sm-4 col-hg" style="display: none;">
   <div style="float:left;display: none;" class="label">Flight</div>
      <input type="text" class="form-control" style="float:left; width:80px;"></div>
        <div class="col-sm-5 col-hg" style="display: none;">
          <div style="float:left;display: none;" class="label">ETA
            </div>
            <div class="bootstrap-timepicker" style="display: none;">
            <div class="form-group">
              <div class="input-group" style="display: none;">
                <input type="text"  name = "DropTime" class="form-control timepicker">
                <div class="input-group-addon">
                  <i class="fa fa-clock-o"></i>
                </div>
              </div>
            </div>
          </div>
            </div>
    <div class="col-sm-3 col-hg" style="display: none;">
        <div class="input-group"><span class="input-group-addon">
        <input  type="checkbox" class="flat-red" checked></span>
        <label for="cbdrop" class="form-control label">Drop &emsp;</label>
        </div>
    </div>
    <div class="col-sm-4 col-hg" style="display: none;">
      <div style="float:left" class="label">Flight</div>
        <input type="text" class="form-control" style="float:left; width:80px;"></div>
          <div class="col-sm-5 col-hg" style="display: none;">
            <div style="float:left" class="label">ETD
          </div>
         <div class="bootstrap-timepicker">
            <div class="form-group">
              <div class="input-group">
                <input type="text"  name = "DropTime" class="form-control timepicker">
                <div class="input-group-addon">
                  <i class="fa fa-clock-o"></i>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>
       


        <div class="col-lg-4" style="margin-top:30px;">
        	<div class="col-sm-12 col-hg capt">RESERVATION INFORMATION</div>
            <div class="col-sm-3 col-hg"><div align="right">Check in :</div></div>
            <div class="col-sm-9 col-hg">
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" name= "checkin" id="checkin" 
                    value="<?php echo date("m/d/Y", strtotime($rsv->checkin)); ?>">
                  </div>
            </div>
            <div class="col-sm-3 col-hg"><div align="right">Check out :</div></div>
            <div class="col-sm-9 col-hg">
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text"  class="form-control pull-right" name="checkout" id="checkout" 
                   value="<?php echo date("m/d/Y", strtotime($rsv->checkout)); ?>">
                  </div>
            </div>
            <div class="col-sm-3 col-hg"><div align="right">Duration :</div></div>
            <div class="col-sm-9 col-hg">
              <input type="text" name = "Duration" id = "Duration" class="form-control" style="float:left; width:50px;"    
                  onkeyup="SumWaktuResv()"  value="<?php echo $rsv->duration; ?>">
            <div style="float:left" class="label">night(s)</div></div>
            <div class="col-sm-3 col-hg"><div align="right">Room type :</div></div>
        <div class="col-sm-9 col-hg">
                    <select id="RoomType" class="form-control select2"  style="width: 100%;" name="RoomType">                 
                      <?php foreach($roomtype as $pek){ ?>
                      <?php if ( $rsv->idroomtype == $pek->id ) { ?>
                    <option value="<?php echo $pek->id; ?>" selected><?php echo $pek->roomtype; ?></option>
                    <?php  }?>
                        <option value="<?php echo $pek->id; ?>" ><?php echo $pek->roomtype; ?></option>
                        <?php } ?>
                    </select>
        </div>
            <div class="col-sm-3 col-hg"><div align="right">Room no :</div></div>
            <div class="col-sm-9 col-hg" id="RoomNodiv">
                <select id="RoomNo" class="form-control select2"  style="width: 100%;" name="RoomNo">                 
                    <option value="<?php echo $rsv->idroom; ?>" ><?php echo $rsv->roomname; ?></option>
                </select>
            </div>
            <div class="col-sm-3 col-hg"><div align="right">Cut-off date :</div></div>
            <div class="col-sm-9 col-hg">
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text"  class="form-control pull-right" name="cod" id="cod" 
                     value="<?php echo date("m/d/Y", strtotime($rsv->cod)); ?>">
                  </div>
            </div>
            <div class="col-sm-3 col-hg"><div align="right">Rsv Status :</div></div>
            <div class="col-sm-9 col-hg">
              <select  class="form-control select2"  style="width: 100%;" name="RsvStatus" id="RsvStatus">                 
                      <?php  foreach($rsvstatus as $pekr){//statusreservation ?>
                      <?php if ( $rsv->statusreservation == $pekr->id ) { ?>
                    <option value="<?php echo $pekr->id; ?>" selected><?php echo $pekr->status; ?></option>
                    <?php  }?>
                        <option value="<?php echo $pekr->id; ?>" ><?php echo $pekr->status; ?></option>
                        <?php } ?>
                    </select>
            </div>
        	<div class="col-sm-3 col-hg"><div align="right">Remark :</div></div>
            <div class="col-sm-9 col-hg"><textarea class="form-control" style="height:150px"></textarea></div>
      	</div>
        <div class="col-lg-4" style="margin-top:30px;">
        	<div class="col-sm-12 col-hg capt">PACKAGE & RATE</div>
            <div class="col-sm-3 col-hg"><div align="right">Package : </div></div>
            <div class="col-sm-9 col-hg">
             <select  class="form-control select2"  style="width: 100%;" id="Package" name="Package">
               <?php foreach($package as $pekage){ ?>
                    <?php if ( $rsvdetil->idpackage == $pekage->id ) { ?>
                    <option value="<?php echo $pekage->id; ?>" selected><?php echo $pekage->package; ?></option>
                    <?php  }?>
                        <option value="<?php echo $pekage->id; ?>" ><?php echo $pekage->package; ?></option>
                        <?php } ?>
               </select>
            </div>
            <div class="col-sm-3 col-hg"><div align="right">Rate :</div></div>
            <div class="col-sm-4 col-hg">
              <input type="text" class="form-control" id="RatePackage1" value="<?php echo number_format($rsvdetil->rate, 0, '.', '.'); ?>" readonly>
              <input type="hidden" class="form-control" id="RatePackage" name="RatePackage" value="<?php echo $rsvdetil->rate ?>">
            </div>
        	<div class="col-sm-5 col-hg"><div class="input-group"><span class="input-group-addon"> <input  type="checkbox" class="flat-red" checked></span><label for="cbflr" class="form-control label">Flate rate</label></div></div>
            <div class="col-sm-3 col-hg"></div>
            <div class="col-sm-9 col-hg" style="margin-bottom:10px;"><button class="btn button-grn">Advance package & rate</button></div>
        	
            <div class="col-sm-12 col-hg capt" style="margin-top:20px">PAYMENT</div>
            <div class="col-sm-3 col-hg"><div align="right">Bill reciever :</div></div>
            <div class="col-sm-9 col-hg">
              <select  class="form-control select2"  style="width: 100%;" id="Company" name="Company">
               <?php foreach($company as $pekcom){ ?>
                    <?php if ( $rsv->idcompany == $pekcom->id ) { ?>
                    <option value="<?php echo $pekcom->id; ?>" selected><?php echo $pekcom->name; ?></option>
                    <?php  }?>
                        <option value="<?php echo $pekcom->idcompany; ?>" ><?php echo $pekcom->name; ?></option>
                        <?php } ?>
               </select>
            </div>

            <div class="col-sm-3 col-hg"><div align="right">Room Total</div></div>
            <div class="col-sm-9 col-hg">
             <input type="text" name="BiayaRoom1" id="BiayaRoom1"  
             value="<?php echo number_format($rsv->duration * $rsvdetil->rate, 0, '.', '.'); ?>" 
             class="form-control" style=" text-align: right; " readonly> 
              <input type="hidden" name="BiayaRoom" id="BiayaRoom"  value="<?php echo $rsv->duration * $rsvdetil->rate; ?>" readonly>
            </div>

            <div class="col-sm-3 col-hg"><div align="right">Deposit</div></div>
            <div class="col-sm-9 col-hg">
               <input type="text" name="BiayaDeposit1" id="BiayaDeposit1" class="form-control input-1" 
                value="<?php echo number_format($rsv->deposit, 0, '.', '.'); ?>" style=" text-align: right;"
               onkeyup="SumTotal()">
             <input type="hidden" name="BiayaDeposit"  value="<?php echo $rsv->deposit?>" 
             id="BiayaDeposit" class="form-control input-1" style=" text-align: right; ">
            </div>

            <div class="col-sm-3 col-hg"><div align="right">Article Payment</div></div>
            <div class="col-sm-9 col-hg">
             <select  class="form-control select2"  style="width: 100%;" id="ArticlePayment" name="ArticlePayment" >
                <option value="0" selected>Personal Account</option>
               <?php 
                          foreach($article_payment as $article){
                      ?>
                        <option value="<?php echo $article->id; ?>" ><?php echo $article->articlepayment; ?></option>
                        <?php } ?>
               </select>
            </div>

             <div class="col-sm-3 col-hg"><div align="right">Invoice NO</div></div>
            <div class="col-sm-9 col-hg">
              <input type="text" name="NoInvoice" id="NoInvoice" class="form-control" style=" text-align:right;">
            </div>

            
             <div class="col-sm-3 col-hg"><div align="right">Balance</div></div>
            <div class="col-sm-9 col-hg">
              <input type="text" name="BiayaBalance1" id="BiayaBalance1" class="form-control" style=" text-align:right;"
               value="<?php echo number_format( ($rsv->duration * $rsvdetil->rate) - $rsv->deposit , 0, '.', '.'); ?>"  readonly>
             <input type="hidden" name="BiayaBalance" id="BiayaBalance" class="form-control" style=" text-align:right;" 
               value="<?php echo  ($rsv->duration * $rsvdetil->rate) - $rsv->deposit; ?>"  readonly>
            </div>
                     
            <div class="col-sm-9 col-hg" style="margin-top:100px;">
              <a type="submit" class="btn btn-success btn-lg"  id="simpan1" class="btn button-grn ">Save <i class='fa fa-check-square-o'></i> </a>
            </div>
        </div>
       
    </div>
   
        </form>
</section>

   <div class="modal modal-wide fade" id="myModalGuest" role="dialog" style="display:none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                          <p id="notify"></p>
                    <button type="button" class="close" title="close" data-dismiss="modal">&times;</button>
                    <h4 align="center" class="modal-title">Guest List</h4>
                </div>
                <div class="modal-body">   
                </div>
            </div>
        </div>     
    </div>

    <div class="modal modal-wide fade" id="myModalGuestAdd" role="dialog" style="display:none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" title="close" data-dismiss="modal">&times;</button>
                    <h4 align="center" class="modal-title">Guest List</h4>
                </div>
                <div class="modal-body">   
                </div>
            </div>
        </div>     
    </div>

    <!-- <div class="modal modal-wide fade" id="myModalAddDeposit" role="dialog" style="display:none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" title="close" data-dismiss="modal">&times;</button>
                    <h4 align="center" class="modal-title">Add Deposit</h4>
                </div>
                <div class="modal-body"> 
                <div class="row">
                  <div class="col-sm-12">
                  <div class="form-group">
                            <label class="control-label">Amount</label>
                            <input type="text" name="Amount" id="Amount"   class="form-control input-1"  placeholder="">
                    </div>
                     <div class="form-group">
                            <label class="control-label">Invoice NO</label>
                            <input type="text" name="InvoiceNo" id="InvoiceNo"  data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out description..."  class="form-control"  placeholder="">
                    </div>
                    <div class="form-group">
                            <label class="control-label">Description</label>
                            <input type="text" name="Description" id="Description"  data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out description..."  class="form-control"  placeholder="">
                    </div>
                  </div>
                  
                </div>  
                </div>
            </div>
        </div>     
    </div> -->

    <div class="modal modal-wide fade" id="myModalGuestAll" role="dialog" style="display:none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" title="close" data-dismiss="modal">&times;</button>
                    <h4 align="center" class="modal-title">Guest</h4>
                </div>
        <div class="modal-body" id="modalbody">
            <div class="table-responsive">
              <table id="example12" class="table table-bordered table-striped">                
                <input type="hidden" value="0" id="index"> 
                    <thead>
                        <tr >
                            <th>index</th>
                            <th>id Guest</th>
                            <th>Name</th>
                            <th>Address</th>
                            <th>State</th>
                            <th>E-Mail</th>
                            <th>Phone</th>
                            <th >Action</th>
                        </tr>
                    </thead>  
                 <tbody id="mytbody">              
                 </tbody>
               </table>
            </div>
        </div>
        </div>     
    </div>


<script type="text/javascript">
$("#simpan1").click(function() {
        event.preventDefault();
        $.confirm({
          title: 'Confirmation',
          content: 'Are You Sure to Save?',
          type:'blue',
          buttons: {
              Simpan: function () {
                  $.LoadingOverlay("show");
                  $("#Simpan").submit();
              },
              Batal: function () {
                
                  $.alert('Data Tidak Disimpan...');
              },
          }
      }); 

});
</script>
<?php
$this->load->view('template/Js');
//$this->load->view('template/Foot');
?>

<script type="text/javascript">
function SumWaktuResv()
{

  var Duration  =$('#Duration').val();  
var Duration1 =  parseInt(Duration)
var checkin = $('#checkin').val();
//alert(checkin);

var date = new Date(checkin);
var newdate = new Date(date);

 newdate.setDate(newdate.getDate() + Duration1); 
var nd = new Date(newdate);

$('#checkout').val(  nd.getMonth()+1 + "/" + nd.getDate() +  "/" + nd.getFullYear() );

}
</script>

<script type="text/javascript">
function SumTotal()
{

  var BiayaRoom  =$('#BiayaRoom').val();  
var Deposit = $('#BiayaDeposit1').val().replace(/\./g, "");
$('#BiayaDeposit').val(Deposit);
//alert(BiayaRoom);
var total = parseFloat(BiayaRoom) - parseFloat(Deposit);

$('#BiayaBalance1').val(total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
$('#BiayaBalance').val(total);



}
</script>

<script type="text/javascript">
 $('#VIP1').is(':checked')
// alert('tes');
</script>

<script type="text/javascript">
$('#Package').on("select2:select", function(e) {
  var idpackage = $('#Package').val();
  $.ajax({
    type: "POST",
    dataType:'json',
   url: '<?php echo site_url('Fo_outlet/Select_package_by_code');?>',
    data: {id: idpackage},
    success: function(data){
      $('#RatePackage1').val(data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
      $('#RatePackage').val(data); 
      var duration = $('#Duration').val();
      var BiayaRoom = data* parseInt(duration);
      $('#BiayaRoom1').val(BiayaRoom.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")); 
      $('#BiayaRoom').val(BiayaRoom);  
     // alert(BiayaRoom);  
    }
  });
});

</script>


<script type="text/javascript">
  $(document).ready(function(){
    $('#example12 tbody').on('click', '.Hapus', function(e){
    var t = $('#example12').DataTable();
    var selectedIndex = t.row($(this).closest('tr')).data()[0]

      $('.'+selectedIndex).remove();
      t.row($(this).closest('tr')).remove().draw(false);
    });
  });
</script>

<script type="text/javascript">
  $(document).ready(function(){
     $("#ViewGuest").click(function(e){ 
        e.preventDefault();
            var mymodal = $('#myModalGuestAll');
            mymodal.modal({backdrop: 'static', keyboard: false}) 
            mymodal.modal('show');            

    });
  });
</script>

<script type="text/javascript">
  $(document).ready(function(){
     $(".SelectGuestAll").click(function(e){ 
      $.ajax({
        type: "POST",
       url: '<?php echo site_url('Fo_outlet/Guest_view');?>',
        data: {status: 'All'  },
        success: function(data){
            e.preventDefault();
            var mymodal = $('#myModalGuest');
            mymodal.modal({backdrop: 'static', keyboard: false}) 
            var height = $(window).height() - 200;
            mymodal.find(".modal-body").css("max-height", height);
            mymodal.find('.modal-body').html(data);
            mymodal.modal('show');            
        }
      });
    });
  });
</script>

<script type="text/javascript">
  $(document).ready(function(){
     $(".AddDeposit").click(function(e){ 
      // $.ajax({
      //   type: "POST",
      //  url: '<?php echo site_url('Fo_outlet/Guest_view');?>',
      //   data: {status: 'All'  },
      //   success: function(data){
      //       e.preventDefault();
      //       var mymodal = $('#myModalGuest');
      //       mymodal.modal({backdrop: 'static', keyboard: false}) 
      //       var height = $(window).height() - 200;
      //       mymodal.find(".modal-body").css("max-height", height);
      //       mymodal.find('.modal-body').html(data);
      //       mymodal.modal('show');            
      //   }
      // });

       e.preventDefault();
            var mymodal = $('#myModalAddDeposit');
            mymodal.modal({backdrop: 'static', keyboard: false}) 
            var height = $(window).height() - 200;
            mymodal.find(".modal-body").css("max-height", height);
            mymodal.find('.modal-body').html(data);
            mymodal.modal('show');  
    });
  });
</script>

<script type="text/javascript">
  $(document).ready(function(){
     $(".SelectGuest").click(function(e){ 
      $.ajax({
        type: "POST",
       url: '<?php echo site_url('Fo_outlet/Guest_view');?>',
        data: {status: '1'  },
        success: function(data){
            e.preventDefault();
            var mymodal = $('#myModalGuest');
            mymodal.modal({backdrop: 'static', keyboard: false}) 
            var height = $(window).height() - 200;
            mymodal.find(".modal-body").css("max-height", height);
            mymodal.find('.modal-body').html(data);
            mymodal.modal('show');            
        }
      });
    });
  });
</script>

<script type="text/javascript">
  $(document).ready(function(){
     $("#AddGuest").click(function(e){ 
      $.ajax({
        type: "POST",
       url: '<?php echo site_url('Fo_outlet/Guest_add');?>',
        data: {id: 'all'  },
        success: function(data){
            e.preventDefault();
            var mymodal = $('#myModalGuestAdd');
            mymodal.modal({backdrop: 'static', keyboard: false}) 
            var height = $(window).height() - 200;
            mymodal.find(".modal-body").css("max-height", height);
            mymodal.find('.modal-body').html(data);
            mymodal.modal('show');            
        }
      });
    });
  });
</script>

 <script type="text/javascript">
      $(document).ready(function() {
       $('#fullscreen').trigger('click');
          });
    </script>

<script type="text/javascript">
   $(document).on('change', '#RoomType', function(){
  var id = $('#RoomType').val();
        $.ajax({
            type: "POST",
            dataType:'html',
           url: '<?php echo site_url("Fo_outlet/select_room_no");?>',
            data: {id: id},
            success: function(data){
                 $('#RoomNodiv').html(data);  
                 $(".select2").select2();
            }
          });
    });
</script>
<script language="JavaScript">
    function fullScreen() {
    var el = document.documentElement
        , rfs = // for newer Webkit and Firefox
               el.requestFullScreen
            || el.webkitRequestFullScreen
            || el.mozRequestFullScreen
            || el.msRequestFullScreen
    ;
    if(typeof rfs!="undefined" && rfs){
      rfs.call(el);
    } else if(typeof window.ActiveXObject!="undefined"){
      // for Internet Explorer
      var wscript = new ActiveXObject("WScript.Shell");
      if (wscript!=null) {
         wscript.SendKeys("{F11}");
      }
    }

    }
// End -->
</script>

 <script>
  $(function () {
    $(".select2").select2();
     $(".select3").select2();

  });
  </script>
      <script>
  $( function() {
    $( "#checkin" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });
	$( "#checkout" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });
	$( "#cod" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });

  });
  </script>
     <script type="text/javascript">
    $(".timepicker").timepicker({
      showInputs: false
    });
</script>

		<script>
        $('.value-plus1').on('click', function(){
            var divUpd = $(this).parent().find('.value1'), newVal = parseInt(divUpd.text(), 10)+1;
            divUpd.text(newVal);
            $('#qty').val(newVal);
        });

        $('.value-minus1').on('click', function(){
            var divUpd = $(this).parent().find('.value1'), newVal = parseInt(divUpd.text(), 10)-1;
            if(newVal>=1) divUpd.text(newVal);
            if(newVal>=1) $('#qty').val(newVal);
        });
        </script>
<script>
  $(function () {
     $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });
  });
</script>
<script>
  $(function () {
    $("#example12").DataTable();
    dtTable= $("#example12").DataTable();
    dtTable.columns([0,1]).visible(false);


    
  });
</script>


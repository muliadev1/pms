<?php

class M_reservation extends CI_Model{

	 function Expected_arrival($tglAwal, $tglAkhir) {
		 $sql="SELECT  tbreservation.idreservation, tbreservationstatus.status, tbroom.roomname AS Expr1, tbroomtype.desc, tbguest.saluation, tbguest.firstname,tbguest.lastname, DATE_FORMAT(tbreservation.checkin,'%m/%d/%Y') AS checkin,
					 DATE_FORMAT(tbreservation.checkout,'%m/%d/%Y') AS checkout, tbreservation.duration, tbguest.state AS Expr2,tbsegment.segment AS Expr3, tbcompany.name AS Expr4, tbreservation.notes, tbreservation.msg
					 FROM tbreservation INNER JOIN tbguest ON tbreservation.idguest = tbguest.idguest INNER JOIN tbroomtype ON tbreservation.idroomtype = tbroomtype.id INNER JOIN
					 tbroom ON tbreservation.idroom = tbroom.id INNER JOIN tbsegment ON tbreservation.idsegment = tbsegment.id INNER JOIN tbcompany ON tbreservation.idcompany = tbcompany.idcompany
					 INNER JOIN tbreservationstatus ON tbreservation.idreservationstatus = tbreservationstatus.id";

		if ($tglAwal=="") {
			$sql=$sql."WHERE Convert(checkin, date) <= '$tglAkhir' AND  idreservationstatus<> 4 AND  idreservationstatus<> 5  
			GROUP BY tbreservation.idreservation ";
		} else{
			$sql=$sql." WHERE Convert(checkin, date) BETWEEN '$tglAwal' AND '$tglAkhir' AND idreservationstatus<> 4 AND  idreservationstatus<> 5  
			GROUP BY tbreservation.idreservation ";
		};
		//print_r($tglAwal.' '.$tglAkhir);exit();
		$data = $this->db->query($sql);
		//print_r($this->db)
 		return $data->result();
    }

	 function Expected_departure($tglAwal,$tglAkhir) {
		 $sql = "SELECT  tbreservation.idreservation, tbreservation.idreservationstatus,tbreservationstatus.status, tbroom.roomname AS Expr1, tbroomtype.desc, tbguest.saluation, tbguest.firstname,tbguest.lastname, DATE_FORMAT(tbreservation.checkin,'%m/%d/%Y') AS checkin,
  					 DATE_FORMAT(tbreservation.checkout,'%m/%d/%Y') AS checkout, tbreservation.duration, tbguest.state AS Expr2,tbsegment.segment AS Expr3, tbcompany.name AS Expr4, tbreservation.notes, tbreservation.msg
  					 FROM tbreservation INNER JOIN tbguest ON tbreservation.idguest = tbguest.idguest INNER JOIN tbroomtype ON tbreservation.idroomtype = tbroomtype.id INNER JOIN
  					 tbroom ON tbreservation.idroom = tbroom.id INNER JOIN tbsegment ON tbreservation.idsegment = tbsegment.id INNER JOIN tbcompany ON tbreservation.idcompany = tbcompany.idcompany
  					 INNER JOIN tbreservationstatus ON tbreservation.idreservationstatus = tbreservationstatus.id";
		if ($tglAwal=="") {
			$sql = $sql. " WHERE DATE_FORMAT(tbreservation.checkout,'%Y-%m-%d') <= '$tglAkhir' AND 
			(tbreservation.idreservationstatus = 4 OR tbreservation.idreservationstatus = 5)";
		} else {
			$sql = $sql. " WHERE DATE_FORMAT(tbreservation.checkout,'%Y-%m-%d') >= '$tglAwal' <= '$tglAkhir' AND 
			(tbreservation.idreservationstatus = 4 OR tbreservation.idreservationstatus = 5)";
		}
		$data = $this->db->query($sql);
		return $data->result();
  }


		function Guest_in_house() {
  		$data = $this->db->query("SELECT tbreservation.idreservation, tbroom.roomname, tbguest.saluation, tbguest.firstname, tbguest.lastname, tbroomtype.roomtype AS Expr1,
  															tbroomtype.desc, DATE_FORMAT(tbreservation.checkin,'%m/%d/%Y') AS checkin, DATE_FORMAT(tbreservation.checkout,'%m/%d/%Y') AS checkout, tbreservation.duration, tbreservation.adult, tbreservation.child,tbsegment.segment AS Expr2,
  															tbcompany.name AS Expr3, tbguest.state AS Expr4, tbreservation.notes, tbreservation.msg, tb_user.nama_user, tbroom.id
  															FROM tbreservation INNER JOIN tbroomtype ON tbreservation.idroomtype = tbroomtype.id INNER JOIN tbroom ON tbreservation.idroom = tbroom.id INNER JOIN tbguest ON tbreservation.idguest = tbguest.idguest
  															INNER JOIN tbsegment ON tbreservation.idsegment = tbsegment.id INNER JOIN tbcompany ON tbreservation.idcompany = tbcompany.idcompany INNER JOIN tb_user ON tbreservation.kodeop = tb_user.id_user
  															WHERE  tbreservation.statusreservation = '1' AND tbreservation.idreservationstatus = 4 ORDER BY tbreservation.idroom ASC");
  		return $data->result();
     }

}
?>

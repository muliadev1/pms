<?php 


class Dashboard extends CI_Controller{
	

	function __construct(){
		parent::__construct();
		if($this->session->userdata('Kode_userPms')==NULL) {
           redirect('Login');
		}
		$this->load->model(array('M_rbac'));
		$this->load->library(array( 'create_pdf','ciqrcode','akuntan_function_template'));
		$this->load->helper('url');
		
		if (!empty($this->session->userdata("active_db"))) {
			$this->load->database($this->session->userdata("active_db"), FALSE, TRUE);
		}
	}
	

	function index(){
		// $data['nasabahAnggota'] = $this->M_dashboard->LoadAnggotaNasabah(); 
		// $data['Tabungan'] = $this->M_dashboard->LoadTabungan($this->TanggalSistem);  
		// $data['Deposito'] = $this->M_dashboard->LoadDeposito($this->TanggalSistem); 
		// $data['Pinjaman'] = $this->M_dashboard->LoadDeposito($this->TanggalSistem);  
		//print_r($this->session->userdata('Kode_userPms'));exit();
		$this->load->view('sistem/Dashboard');
		//phpinfo();
	}

	
	 
	
}

?>
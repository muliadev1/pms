<?php

class M_roomstatus extends CI_Model{

	function RoomStatus_view() {
		$data = $this->db->query("SELECT * FROM tbroomstatus");
		return $data->result();
   }

	 function RoomStatus_addDB($table,$data) {
			 $this->db->insert($table,$data);
	}

	function RoomStatus_view_edit($id) {
	 $this->db->select("*");
	 $this->db->where('id',$id);
	 $this->db->from('tbroomstatus');
	 return $this->db->get()->result();
	}

	function RoomStatus_editDB($table,$data,$id) {
 			 $this->db->where('id',$id );
 			 $this->db->update($table,$data);
  }

}
?>

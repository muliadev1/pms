
<?php 

class M_store_requisition extends CI_Model{	



	function item_view() {   
		 $query = $this->db->query(" SELECT tbitem.* ,tbsubcategory.subcategory as namabsubcategory FROM tbitem INNER JOIN `tbsubcategory`
                                ON tbitem.`idsubcategory` =  tbsubcategory.`idsubcategory`    
       "); 
    return $query->result() ;   


   } 

       function get_kodesr($table) { 
        $tahun = date('y'); 
        $bulan = date('m'); 
        $q = $this->db->query("SELECT MAX(RIGHT(kodesr,4)) AS idmax FROM ".$table);
        $kd = ""; //kode awal
        if($q->num_rows()>0){ //jika data ada
            foreach($q->result() as $k){
                $tmp = ((int)$k->idmax)+1; //string kode diset ke integer dan ditambahkan 1 dari kode terakhir
                $kd = "0000".$tmp; //kode ambil 4 karakter terakhir
                $id = substr($kd, -5);
            }
        }else{ //jika data kosong diset ke kode awal
            $kd = $tahun.$bulan."00001";
        }
        $kar = "SR"; //karakter depan kodenya
        //gabungkan string dengan kode yang telah dibuat tadi
        return $kar.$tahun.$bulan.$id;
   }


   function add_transfer_stok_direct($Departement,$data,$datamastersr,$datadetilsr) { 
 //print_r($data);exit();
       // $this->db->trans_strict(FALSE);
       // $this->db->trans_start(); 

      $this->db->insert( 'tbsr', $datamastersr);
      $this->db->insert_batch( 'tbsrdetil', $datadetilsr); 


     if ($Departement==1) { //````````````
        $set = 'stokBO';
        $setaset = 'jumlahBO';
        $Istok = 'isBO';
        $kondisiawal = 'baikBO';
       }elseif ($Departement==2) {
        $set = 'stokHK';
        $Istok = 'isHK';
         $setaset = 'jumlahHK';
         $kondisiawal = 'baikHK';
       }elseif ($Departement==3) {
         $set = 'stokFO';
         $Istok = 'isFO';
         $setaset = 'jumlahFO';
         $kondisiawal = 'baikFO';
       }elseif ($Departement==4) {
          $set = 'stokFB';
          $Istok = 'isFB';
          $setaset = 'jumlahFB';
          $kondisiawal = 'baikFB';
       }elseif ($Departement==5) {
        $set = 'stokBO';
        $Istok = 'isBO';
        $setaset = 'jumlahBO';
        $kondisiawal = 'baikBO';
       }elseif ($Departement==6) {
        $set = 'stokBar';
        $Istok = 'isBar';
        $setaset = 'jumlahBar';
        $kondisiawal = 'baikBar';
      }elseif ($Departement==7) {
        $set = 'stokEng';
        $Istok = 'isEng';
        $setaset = 'jumlahEng';
        $kondisiawal = 'baikEng';
      }elseif ($Departement==8) {
        $set = 'stokPG';
        $Istok = 'isPG';
        $setaset = 'jumlahPG';
        $kondisiawal = 'baikPG';
      }elseif ($Departement==9) {
        $set = 'stokSec';
        $Istok = 'isSec';
        $setaset = 'jumlahSec';
        $kondisiawal = 'baikSec';
       }else{
         $set = 'stok';
         $setaset = 'jumlah';
         $kondisiawal = 'baik';
       }

      foreach ($data as $row) {
        $this->db->set($set, $set.'+'.$row['qty'].'', FALSE);
        $this->db->where('codeitem',$row['itemcode'] );
        $this->db->update('tbitem'); 

        if (!empty($Istok)) {
          $this->db->set($Istok, 1); 
          $this->db->where('codeitem',$row['itemcode'] );
          $this->db->update('tbitem'); 
        }

        //aset
        $category = $this->Load_Categoryitem_byiditem($row['itemcode']);
        if ($category == $this->session->userdata('KodeKategoriAset')) {
          $this->db->set($setaset, $setaset.'+'.$row['qty'].'', FALSE);
          $this->db->where('codeitem',$row['itemcode'] );
          $this->db->update('tbaset');

           $this->db->set($kondisiawal, $kondisiawal.'+'.$row['qty'].'', FALSE);
          $this->db->where('codeitem',$row['itemcode'] );
          $this->db->update('tbaset');
        }
        
        
      }
   
 		   // $this->db->trans_complete();
      //   if ($this->db->trans_status() === FALSE){
      //       $this->db->trans_rollback();
      //   }else{
      //       $this->db->trans_commit();
      //   }      
   }



    function set_SubDep($itemcode,$kodesubcategory,$iddepartement) { 
        $query = $this->db->query("SELECT * from tbsubcategorydepartement  WHERE iddepartement = '$iddepartement' 
        and itemcode= '$itemcode' "); 
        $data =  $query->row(); 

        $querysub = $this->db->query("SELECT * from tbsubcategory  WHERE idsubcategory = '$kodesubcategory' "); 
        $datasub =  $querysub->row() ; 

        $dataeksekusi = array(
             'iddepartement'=> $iddepartement,
             'itemcode'=> $itemcode,
             'kodesubcategory'=> $kodesubcategory,
             'desc'=> $datasub->description
          );
       // print_r($dataeksekusi);exit();

         if (empty($data)) {
          $this->db->insert('tbsubcategorydepartement',$dataeksekusi);
         }else{
           $this->db->where('id',$data->id );
           $this->db->update('tbsubcategorydepartement',$dataeksekusi);
         }
    } 





  function add_transfer_stok_manual($dataPr,$dataDetilPr,$Departement) { 
       //print_r($dataDetilPr);exit();
       $this->db->trans_strict(FALSE);
       $this->db->trans_start();

      $this->db->insert( 'tbsr', $dataPr);
      $this->db->insert_batch( 'tbsrdetil', $dataDetilPr);  
   
       if ($Departement==1) {
        $set = 'stokBO';
         $setaset = 'jumlahBO';
       }elseif ($Departement==2) {
        $set = 'stokHK';
        $setaset = 'jumlahHK';
       }elseif ($Departement==3) {
         $set = 'stokFO';
          $setaset = 'jumlahFO';
       }elseif ($Departement==4) {
          $set = 'stokFB';
           $setaset = 'jumlahFB';
       }elseif ($Departement==5) {
        $set = 'stokBO';
         $setaset = 'jumlahBO';
      }elseif ($Departement==6) {
        $set = 'stokBar';
        $setaset = 'jumlahBar';
      }elseif ($Departement==7) {
        $set = 'stokEng';
        $setaset = 'jumlahEng';
      }elseif ($Departement==8) {
        $set = 'stokPG';
        $setaset = 'jumlahPG';
      }elseif ($Departement==9) {
        $set = 'stokSec';
        $setaset = 'jumlahSec';
       }else{
         $set = 'stok';
        $setaset = 'jumlah';
       }

      foreach ($dataDetilPr as $row) {
        $this->db->set($set,$set.'+'.$row['qtyorder'].'', FALSE);
        $this->db->where('codeitem',$row['codeitem'] );
        $this->db->update('tbitem'); 

        $this->db->set('stok', 'stok-'.$row['qtyorder'].'', FALSE);
        $this->db->where('codeitem',$row['codeitem'] );
        $this->db->update('tbitem'); 

       // print_r($row['qtyorder']);exit();//aset
        $category = $this->Load_Categoryitem_byiditem($row['codeitem']);
        if ($category == $this->session->userdata('KodeKategoriAset')) {
          $this->db->set($setaset, $setaset.'+'.$row['qtyorder'].'', FALSE);
          $this->db->where('codeitem',$row['codeitem'] );
          $this->db->update('tbaset');
        }
      }
         
       $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else{
            $this->db->trans_commit();
        }      
   } 


  function load_departement(){   
      $query = $this->db->query(" SELECT * from tbdepartement"); 
    return $query->result() ;       
   }
     function load_subDep($IdDep,$itemcode){   
      $query = $this->db->query(" SELECT * from tbsubcategorydepartement where iddepartement = '$IdDep' and itemcode = '$itemcode' "); 
    return $query->row() ;       
   }
   function load_sub(){   
      $query = $this->db->query(" SELECT * from tbsubcategory  "); 
    return $query->result() ;       
   }

 
   function Load_SR_all() { 
        $query = $this->db->query("SELECT `kodesr` ,tbdepartement.`departement`,date_format(dateadd,'%d-%m-%Y') as dateadd ,`description` FROM tbsr LEFT JOIN tbdepartement
                                  ON tbsr.`iddepartement` =  tbdepartement.iddepartement 
                                   "); 
         return $query->result() ;
        } 

         function Load_categoryitem_byiditem($codeitem) { 
        $query = $this->db->query("SELECT idcategory FROM tbitem INNER JOIN tbsubcategory 
                                  ON  tbitem.`idsubcategory` = tbsubcategory.`idsubcategory` WHERE codeitem= '$codeitem' "); 
         $data=  $query->row() ;
         return $data->idcategory;
        } 

         function Load_SR_bycode($kodesr) { 
        $query = $this->db->query("SELECT tbsrdetil.`kodesr`, date_format(dateadd,'%d-%m-%Y') as dateadd,`unit`,tbsrdetil.`description`,`qtyorder`,`price`,tbsr.`description` AS note , tbsrdetil.departement, tbsrdetil.`note` AS notedetil FROM tbsr
                                  INNER JOIN tbsrdetil ON tbsr.`kodesr` =  tbsrdetil.`kodesr`  WHERE tbsrdetil.`kodesr` = '$kodesr' "); 
         return $query->result() ; 
         } 


  function load_akunitem_bycode($codeitem,$Departement) {
   
   //$Departement = $data->row()->iddepartement;
    if ($Departement !=0) {
       if ($Departement==1) { 
        $akun = 'akunBO';
       }elseif ($Departement==2) {
        $akun = 'akunHK';
       }elseif ($Departement==3) {
         $akun = 'akunFO';
       }elseif ($Departement==4) {
          $akun = 'akunFB';
       }elseif ($Departement==5) {
        $akun = 'akunBO';
       }elseif ($Departement==6) {
        $akun = 'akunBar';
      }elseif ($Departement==7) {
        $akun = 'akunEng';
      }elseif ($Departement==8) {
        $akun = 'akunPG';
      }elseif ($Departement==9) {
        $akun = 'akunSec';
       }
      $dataItem = $this->db->query("SELECT $akun as akun,price FROM tbitem  
                                    WHERE codeitem = '$codeitem' ");
      return $dataItem->result();
     }else{
      return '';
     }
 
}


 function Payment_add_manual_dropdepartement($tablemasterJurnal,$datamasterJurnal,$tableDetilJurnal,$datadetilJurnal) { 

       $this->db->trans_strict(FALSE);
       $this->db->trans_start();

        //Modul Akuntansi Start
        date_default_timezone_set('Asia/Hong_Kong');
        $this->db->insert($tablemasterJurnal,$datamasterJurnal);
        $this->db->insert_batch($tableDetilJurnal, $datadetilJurnal);

        foreach($datadetilJurnal as $datadetil1){
         $NoAkun = $datadetil1['NoAkun'];
         $Debet = $datadetil1['Debet'];
         $Kredit = $datadetil1['Kredit'];

        
        $dataSetting = $this->db->query("select left(NoAkun,1) as Klas, IsKontra from tbakun where NoAkun = '".$NoAkun."' ");
        foreach($dataSetting->result() as $dataSetting1){
                $Klas = $dataSetting1->Klas;
                $IsKontra = $dataSetting1->IsKontra;
            }
            if(($Klas == '1' or $Klas=='5' or $Klas=='6' or  $Klas=='9') and $IsKontra == '0' ){
                $this->db->query("update tbakun set Saldo = Saldo +".$Debet. " - ".$Kredit." where NoAkun = '".$NoAkun. "'");   
            }else{
                $this->db->query("update tbakun set Saldo = Saldo -".$Debet. " + ".$Kredit." where NoAkun = '".$NoAkun. "'");
            }       
        }
        //Modul Akuntansi End

       
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else{
            $this->db->trans_commit();
        }      
   } 




  

  


  


      



}
<?php

class Guest extends CI_Controller{

	function __construct(){
		parent::__construct();
		if($this->session->userdata('Kode_userPms')==NULL) {
           redirect('Login');
		}
		$this->load->library(array ('session','country'));
		$this->load->helper(array('form', 'url'));
		$this->load->model(array('M_guest'));
		date_default_timezone_set('Asia/Hong_Kong');
		
		if (!empty($this->session->userdata("active_db"))) {
			$this->load->database($this->session->userdata("active_db"), FALSE, TRUE);
		}
	}

	function index(){
		redirect('Guest/Guest_view');
	}

	function Guest_view(){
		$data['data'] = $this->M_guest->Guest_view();
		$this->load->view('guest/Guest',$data);
	}

	function Guest_add(){
		$data['country'] = $this->country->SelectCountry();
		//print_r($data['country']);exit;
		$this->load->view('guest/Guest_add',$data);
	}

	function Guest_addDB(){
		date_default_timezone_set('Asia/Hong_Kong');
		$datebirthday = $this->input->post('birthday');
		$datebirthday1 = DateTime::createFromFormat('m/d/Y',$datebirthday);
		$datebirthday2 = $datebirthday1->format("Y-m-d");
		$data = array(
			'saluation' => $this->input->post('saluation'),
			'firstname' => $this->input->post('firstname'),
			'lastname' => $this->input->post('lastname'),
			'address' => $this->input->post('address'),
			'idstate' => $this->input->post('state'),
			'state' => $this->input->post('state'),
			'zipcode' => $this->input->post('zipcode'),
			'email' => $this->input->post('email'),
			'phone' => $this->input->post('phone'),
			'gender' => $this->input->post('gender'),
			'birthday' => $datebirthday2,
			'idtype' => $this->input->post('idtype'),
			'idnumber' => $this->input->post('idnumber'),
			'description' => $this->input->post('description'),
			'currtimestamp' => date('Y-m-d H:i:s'),
			'isaktif' => '1'
		);
		$this->M_guest->Guest_addDB('tbguest',$data) ;
		$this->session->set_flashdata('msg', 'Add Guest successfully ...');
		redirect('Guest/Guest_view');
	}

	function Guest_edit($id){
		$data['data'] = $this->M_guest->Guest_view_edit($id) ;
		$data['country'] = $this->country->SelectCountry();
		//print_r($data['country']);exit;
		$this->load->view('guest/Guest_edit',$data);
	}

	function Guest_editDB(){
		date_default_timezone_set('Asia/Hong_Kong');
		$datebirthday = $this->input->post('birthday');
		$datebirthday1 = DateTime::createFromFormat('m/d/Y',$datebirthday);
		$datebirthday2 = $datebirthday1->format("Y-m-d");
		$id = $this->input->post('Id');
		$data = array(
			'saluation' => $this->input->post('saluation'),
			'firstname' => $this->input->post('firstname'),
			'lastname' => $this->input->post('lastname'),
			'address' => $this->input->post('address'),
			'idstate' => $this->input->post('state'),
			'state' => $this->input->post('state'),
			'zipcode' => $this->input->post('zipcode'),
			'email' => $this->input->post('email'),
			'phone' => $this->input->post('phone'),
			'gender' => $this->input->post('gender'),
			'birthday' => $datebirthday2,
			'idtype' => $this->input->post('idtype'),
			'idnumber' => $this->input->post('idnumber'),
			'description' => $this->input->post('description'),
			'currtimestamp' => date('Y-m-d H:i:s'),
			'isaktif' => '1'
	);
		$this->M_guest->Guest_editDB('tbguest',$data,$id);
		$this->session->set_flashdata('msg', 'Edit Guest successfully...');
		redirect('Guest/Guest_view');


	}


}
?>



<!-- jQuery UI 1.11.2 -->
<script src="<?php echo base_url('assets/js/jquery-ui.min.js') ?>" type="text/javascript"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>

<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/knob/jquery.knob.js') ?>" type="text/javascript"></script>

<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') ?>" type="text/javascript"></script>



<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/dist/js/demo.js') ?>" type="text/javascript"></script>

<!-- jQuery 2.2.3 -->
<!-- jQuery 2.1.3 -->

<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/jQuery/jquery-2.2.3.min.js') ?>"></script> 

<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/bootstrap/js/bootstrap.min.js') ?>" type="text/javascript"></script>

<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/datatables/jquery.dataTables.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/datatables/dataTables.bootstrap.min.js') ?>" type="text/javascript"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/slimScroll/jquery.slimscroll.min.js') ?>" type="text/javascript"></script>
<!-- FastClick -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/fastclick/fastclick.js') ?>" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/daterangepicker/daterangepicker.js') ?>" type="text/javascript"></script>

<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/dist/js/app.min.js') ?>" type="text/javascript"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/dist/js/demo.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/select2/select2.full.min.js') ?>" type="text/javascript"></script>
<script src=" <?php echo base_url('assets/AdminLTE-2.0.5/plugins/iCheck/icheck.min.js') ?>"></script>
<!-- datepicker -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/datepicker/bootstrap-datepicker.js') ?>" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.2.0/jquery-confirm.min.js"></script>

<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay_progress.min.js"></script>


<script type="text/javascript" src="<?php echo base_url('assets/js/pnoty/pnotify.js') ?>" src="src/pnotify.js"></script>
 <script type="text/javascript" href="<?php echo base_url('assets/js/pnoty/pnotify.animate.js') ?>" ></script>
  <script type="text/javascript" href="<?php echo base_url('assets/js/pnoty/pnotify.buttons.js') ?>" type="text/javascript" ></script>


<script type="text/javascript" src="<?php echo base_url('assets/js/notify.js') ?>" src="src/pnotify.js"></script>

  
<script type="text/javascript" src="<?php echo base_url('assets/js/autoNumeric.min.js') ?>"></script>
  

<script type="text/javascript">
	$.LoadingOverlaySetup({
    color           : "rgba(255, 255, 255, 0.8)" ,
    image           : "<?php echo base_url('assets/img/klk.png') ?>",
    maxSize         : "100px",
    minSize         : "100px",
    resizeInterval  : 0,
    size            : "100%"
});

$(function () {
	var tgl = "<?php echo $this->session->userdata('tgl_na'); ?>"
	tgl = tgl.split("-");
	tgl = tgl[1]+'/'+tgl[2]+'/'+tgl[0];
	// console.log(tgl);
	$('#tglNA').daterangepicker({
		"singleDatePicker": true,
		"startDate": tgl,
		"endDate": tgl
	}, function(start, end, label) {
	  console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
	});
});

	
	//untuk autonumeric
	const autoNumDecimal = {
		digitGroupSeparator        : '.',
		decimalCharacter           : ',',
		decimalCharacterAlternative: ',',
		currencySymbolPlacement    : 's',
		roundingMethod             : 'U',

		minimumValue            : '0',
		maximumValue            : '999999999',
		decimalPlacesOverride   : '2',
	};
</script>





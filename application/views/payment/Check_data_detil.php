
        <div class="table-responsive">
          <table id="example11" class="table table-bordered table-striped">
                                <thead>
                                    <tr >
                                        <th>Kode MI</th>
                                        <th>Date Receive</th>
                                        <th>Item Code</th>
                                        <th>Description</th>
                                         <th>Qty</th>
                                        <th>Unit</th>
                                        <th>Price (IDR)</th>
                                        <th>Subtotal (IDR)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach($data as $u){ ?>
                                    <tr  class='odd gradeX context'>
                                        <td><?php echo $u->kodero ?></td>
                                        <td><?php echo $u->datereceive?></td>
                                        <td><?php echo $u->itemcode ?></td>
                                        <td><?php echo $u->description?></td>
                                         <td><?php echo number_format($u->qty, 0, '.', '.') ?></td>
                                        <td><?php echo $u->unit?></td>
                                       
                                        
                                         <td><?php echo number_format($u->price, 0, '.', '.') ?></td>
                                          <td><?php echo number_format($u->subtotal, 0, '.', '.') ?></td>
                                        
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
 
<script>
  $(function () {
    $("#example11").DataTable();
  });
</script>


<?php

class Company extends CI_Controller{

	function __construct(){
		parent::__construct();
		if($this->session->userdata('Kode_userPms')==NULL) {
           redirect('Login');
		}
		$this->load->library(array('session','country'));
		$this->load->helper(array('form', 'url'));
		$this->load->model(array('M_Company'));
		date_default_timezone_set('Asia/Hong_Kong');
		
		if (!empty($this->session->userdata("active_db"))) {
			$this->load->database($this->session->userdata("active_db"), FALSE, TRUE);
		}
	}

	function index(){
		redirect('Company/Company_view');
	}

	function Company_view(){
		$data['data'] = $this->M_Company->Company_view();
		$this->load->view('company/company',$data);
	}


	function Company_add(){
		$data['country'] = $this->country->SelectCountry();
		$this->load->view('company/company_add',$data);
	}

	function Company_addDB(){
		$data = array(
			'name' => $this->input->post('name'),
			'address' => $this->input->post('address'),
			'idstate' => $this->input->post('state'),
			'state' => $this->input->post('state'),
			'zipcode' => $this->input->post('zipcode'),
			'email' => $this->input->post('email'),
			'phone' => $this->input->post('phone'),
			'type' => $this->input->post('type'),
			'description' => $this->input->post('description'),
			'currtimestamp' => date('Y-m-d H:i:s'),
			'isaktif' => '1'
		);
		$this->M_Company->Company_addDB('tbcompany',$data) ;
		$this->session->set_flashdata('msg', 'Add Company successfully ...');
		redirect('Company/Company_view');
	}

	function Company_edit($id){
		$data['data'] = $this->M_Company->Company_view_edit($id);
		$data['country'] = $this->country->SelectCountry();
		$this->load->view('company/Company_edit',$data);
	}

	function Company_editDB(){
		$id = $this->input->post('IdCompany');
		$data = array(
			'name' => $this->input->post('name'),
			'address' => $this->input->post('address'),
			'idstate' => $this->input->post('state'),
			'state' => $this->input->post('state'),
			'zipcode' => $this->input->post('zipcode'),
			'email' => $this->input->post('email'),
			'phone' => $this->input->post('phone'),
			'type' => $this->input->post('type'),
			'description' => $this->input->post('description'),
			'currtimestamp' => date('Y-m-d H:i:s'),
			'isaktif' => '1'

	);
		$this->M_Company->Company_editDB('tbcompany',$data,$id) ;
		$this->session->set_flashdata('msg', 'Edit Company successfully...');
		redirect('Company/Company_view');


	}


}
?>

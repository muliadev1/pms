<?php

class Category extends CI_Controller{

	function __construct(){
		parent::__construct();
		if($this->session->userdata('Kode_userPms')==NULL) {
           redirect('Login');
		}
		$this->load->library('session');
		$this->load->helper(array('form', 'url'));
		$this->load->model(array('M_category'));
		
		if (!empty($this->session->userdata("active_db"))) {
			$this->load->database($this->session->userdata("active_db"), FALSE, TRUE);
		}
	}

	function index(){
		redirect('Category/Category_view');
	}

	function Category_view(){
		$data['data'] = $this->M_category->Category_view();
		$this->load->view('category/category',$data);
	}


	function Category_add(){
		$this->load->view('Category/Category_add');
	}

	function Category_addDB(){
		$data = array(
			'category' => $this->input->post('category'),
			'description' => $this->input->post('description')
		);
		$this->M_category->Category_addDB('tbcategory',$data) ;
		$this->session->set_flashdata('msg', 'Add Item Category successfully ...');
		redirect('Category/Category_view');
	}

	function Category_edit($id){
		$data['data'] = $this->M_category->Category_view_edit($id) ;
		$this->load->view('category/category_edit',$data);
	}

	function Category_editDB(){
		$id = $this->input->post('Id');
		$data = array(
			'category' => $this->input->post('category'),
			'description' => $this->input->post('description')

	);
		$this->M_category->Category_editDB('tbcategory',$data,$id) ;
		$this->session->set_flashdata('msg', 'Edit Item Category successfully...');
		redirect('Category/Category_view');


	}


}
?>

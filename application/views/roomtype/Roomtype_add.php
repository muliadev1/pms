<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>

<!-- Content Header (Page header) -->
<section class="content-header">
   <div class="btn-group btn-breadcrumb">
            <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
             <a href="<?php echo base_url('index.php/RoomType/RoomType_view');?>" class="btn btn-default  btn-xs">Room Type</a>
            <a  class="btn btn-default  btn-xs active">Add Room Type</a>
        </div>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
   <div class="col-md-12">
    <div class="box box-info">
     <div class="box-header">
      <h3 class="box-title">Add New Room Type</h3>
     </div>

     <div class="box-body">
      <form method="post" id="Simpan"  action="<?php echo base_url(). 'index.php/RoomType/RoomType_addDB'; ?>">
        <div class="form-group">
         <label class="control-label">Room Type</label>
         <input type="text" name="roomtype" id="roomtype"  data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out category description..."  class="form-control"  placeholder="Room Type">
        </div>

        <div class="form-group">
         <label class="control-label">Description</label>
         <textarea type="text" name="description" id="description" data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out firstname..."  class="form-control"></textarea>
        </div>

        <div class="form-group">
         <label class="control-label">Rate</label>
         <input type="text" name="rate" id="rate" data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out firstname..."  class="form-control"  placeholder="Rate">
        </div>

          <div class="form-group">
            <button type="submit" value="Validate" class="btn btn-default"><i class='glyphicon glyphicon-ok'></i> Save</button>
          </div>
        </form>
       </div>
      </div>
     </div>
    </div>
</section>


<script type="text/javascript">
$("#Simpan").submit(function() {
  var roomtype = $('#roomtype').val();
  var description = $('#description').val();
  var rate = $('#rate').val();
     if (roomtype == ''|| description == ''|| rate == ''){
            File_Kosong(); return false;
        }else{
        event.preventDefault();
        $.confirm({
          title: 'Confirmation',
          content: 'Are You Sure to Save?',
           type: 'blue',
          buttons: {
              Save: function () {
                  $.LoadingOverlay("show");
                  $("#Simpan").submit();
              },
              Cancel: function () {

                  $.alert('Data Not Saved...');
              },
          }
      });
    }

});
</script>

<?php
$this->load->view('template/Js');
$this->load->view('template/Foot');
?>

<script type="text/javascript">
  function File_Kosong() {
  $.alert({
    title: 'Caution!!',
    content: 'Add Data Invalid!',
    icon: 'fa fa-warning',
    type: 'orange',
});
}
</script>

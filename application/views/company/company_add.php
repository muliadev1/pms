<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>

<!-- Content Header (Page header) -->
<section class="content-header">
   <div class="btn-group btn-breadcrumb">
            <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
             <a href="<?php echo base_url('index.php/Company/Company_view');?>" class="btn btn-default  btn-xs">Company</a>
            <a  class="btn btn-default  btn-xs active">Add Company</a>
        </div>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
   <div class="col-md-12">
    <div class="box box-info">
     <div class="box-header">
      <h3 class="box-title">Add New Company</h3>
     </div>

     <div class="box-body">
      <form method="post" id="Simpan"  action="<?php echo base_url(). 'index.php/Company/Company_addDB'; ?>">

       <div class="form-group">
        <label class="control-label">Company Name</label>
        <input type="text" name="name" id="name"  data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out category description..."  class="form-control"  placeholder="Company Name">
       </div>

       <div class="form-group">
        <label>Description</label>
        <textarea type="text" name="description" id="description"  class="form-control"></textarea>
       </div>

       <div class="form-group">
        <label>Country</label>
        <select id="state" class="form-control select2"  style="width: 100%;" name="state">
          <?php
           foreach($country as $u){
             //print_r($country);exit();
               ?>
             <option value="<?php echo $u; ?>"><?php echo $u; ?></option>
               <?php } ?>
        </select>
      </select>
       </div>

       <div class="form-group">
        <label class="control-label">Phone</label>
        <input type="text" name="phone" id="phone"  data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out category description..."  class="form-control"  placeholder="Phone">
       </div>

       <div class="form-group">
        <label class="control-label">Zip Code</label>
        <input type="text" name="zipcode" id="zipcode"  data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out category description..."  class="form-control"  placeholder="Zip Code">
       </div>

       <div class="form-group">
        <label class="control-label">E-Mail</label>
        <input type="text" name="email" id="email"  data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out category description..."  class="form-control"  placeholder="Email">
       </div>

       <div class="form-group">
        <label>Address</label>
        <textarea type="text" name="address" id="address"  class="form-control"></textarea>
       </div>

       <div class="form-group">
        <div class="row">
         <div class="col-md-1">
          <label class="control-label">Type</label>
         </div>
        </div>
        <div class="row">
         <div class="col-md-2">
          <input type="radio" name="type" id="type" class="flat-red" value="Company" checked> Company
         </div>
         <div class="col-md-2">
          <input type="radio" name="type" id="type1" class="flat-red" value="Agens"> Agens
         </div>
         <div class="col-md-2">
          <input type="radio" name="type" id="type2" class="flat-red" value="Online Agens"> Online Agens
         </div>
        </div>
       </div>

          <div class="form-group">
            <button type="submit" value="Validate" class="btn btn-default"><i class='glyphicon glyphicon-ok'></i> Save</button>
          </div>
        </form>
       </div>
      </div>
     </div>
    </div>
</section>


<script type="text/javascript">
$("#Simpan").submit(function() {
    var name = $('#name').val();
    var description = $('#description').val();
    var state = $('#state').val();
    var phone = $('#phone').val();
    var zipcode = $('#zipcode').val();
    var email = $('#email').val();
    var address = $('#address').val();
    var type = $('#type').val();

        if (name == ''|| description==''|| state == ''|| phone==''|| zipcode == ''|| email==''|| address == ''|| type==''){
            File_Kosong(); return false;
        }else{
        event.preventDefault();
        $.confirm({
          title: 'Confirmation',
          content: 'Are You Sure to Save?',
           type: 'blue',
          buttons: {
              Save: function () {
                  $.LoadingOverlay("show");
                  $("#Simpan").submit();
              },
              Cancel: function () {

                  $.alert('Data Not Saved...');
              },
          }
      });
    }

});
</script>

<?php
$this->load->view('template/Js');
$this->load->view('template/Foot');
?>



<script type="text/javascript">
  function File_Kosong() {
  $.alert({
    title: 'Caution!!',
    content: 'Add Company Invalid!',
    icon: 'fa fa-warning',
    type: 'orange',
});
}
</script>

<script>
$(function () {
  $(".select2").select2();
   $(".select3").select2();

});
</script>
<script type="text/javascript">
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });
</script>

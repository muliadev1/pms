<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>


<!-- Content Header (Page header) -->
<section class="content-header">
   <div class="btn-group btn-breadcrumb">
            <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
            <a  class="btn btn-default  btn-xs active">Data Subklasifikasi</a>
        </div>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-xs-12">
     
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Data SubKlasifikasi</h3>
          <a style="float:right"  href="<?php echo base_url('index.php/Akun/AddSubklas');?>" class="btn btn-primary">
            <i class="glyphicon glyphicon-saved"></i>
            Tambah
          </a>
        </div>
    <!-- /.box-header -->
        <div class="box-body">
        <div class="table-responsive">
          <table class="table table-bordered table-hover" id="example1">
                                <thead>
                                    <tr>
                                        <th>Id Subklasifikasi</th>
                                        <th>Nama Subklasifikasi</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no=1;
                                    foreach($data as $u){
                                    ?>
                                    <tr  class='odd gradeX context'>
                                        <td><?php echo $u->IdSub ?></td>
                                        <td><?php echo $u->NamaSub ?></td>



                                        
                                        
                                        
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
          </div>
          
          
          
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->

</section><!-- /.content -->


<?php
$this->load->view('template/js');
?>


<script>
function doconfirm()
{
    var nama = $('#btnhapus').val();
    job=confirm("Apakah Anda Yakin Menonaktifkan Nasabah "  + nama + " ?" );
    if(job!=true)
    {
        return false;
    }
}
</script>

<script>
function doconfirmaktif()
{
    var nama = $('#btnaktif').val();
    job=confirm("Apakah Anda Yakin Mengaktifkan Nasabah "  + nama + " ?" );
    if(job!=true)
    {
        return false;
    }
}
</script>

<script>
function doconfirmAdd()
{
    var nama = $('#btnaddAG').val();
    job=confirm("Apakah Anda Yakin Menambahkan Nasabah "  + nama + " Sebagai Anggota ?" );
    if(job!=true)
    {
        return false;
    }
}
</script>

<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>

<?php
$this->load->view('template/foot');
?>
<?php

class M_Guest extends CI_Model{

	function Guest_view() {
		$data = $this->db->query("Select * FROM tbguest WHERE isaktif=1");
		return $data->result();
   }

	 function Guest_addDB($table,$data) {
			 $this->db->insert($table,$data);
	}

	function Guest_view_edit($id) {
	 $data = $this->db->query("SELECT idguest, saluation, firstname, lastname, address, idstate, state, zipcode, email, phone,
       											gender, DATE_FORMAT(birthday,'%m/%d/%Y') AS birthday, idtype, idnumber, description FROM tbguest WHERE idguest = $id");
	 return $data->result();
	}

	function Guest_editDB($table,$data,$id) {
 			 $this->db->where('idguest',$id );
 			 $this->db->update($table,$data);

  }

}
?>

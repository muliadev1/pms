<style media="screen">
  .tes{
    max-height: 250px;
    min-height: 250px;
    min-width: auto;
    overflow-y: visible;
    overflow-x: hidden;

  }
  tr:hover td {background:#f39c12 }
  tr.tr-click{
    background-color: #f9f9f9;
  }
	td.td-click{
		cursor: pointer;
	}
  #selectedmenu tr{
    background-color: #00a65a;
  }

  
  .card {
  background: #fff;
  border-radius: 2px;
  display: inline-block;
 
  margin: 1rem;
  position: relative;
  
}
.card-3 {
  box-shadow: 0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);
}
 .box-widget {
border: #adadad solid 1px;


}





#tallModal .modal-body p { margin-bottom: 900px }
.pull-right {
    float: right;
}
</style>

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    <h3 class="modal-title" align="center" id="myModalLabel" style="font-weight:bold;">TABLE <?php echo $nama; ?></h3>
    <i>Open by : <b>Operator</b></i>
    <input type="hidden" name="tableId" value="<?php echo $nama; ?>">
  </div>
  <div class="modal-body1">
    <div class="col-sm-12">
      <div class="col-sm-5">
        <div class="box box-primary card card-3 bg-light-blue-gradient" style="min-height: 560px; max-height: 560px;  " >
          <div class="box-header">
            <h3 class="box-title" style="color:#ffffff;"><span class"fa fa-list"> <strong>Menu List</strong></span> </h3>
          </div>
          <div class="box-body">
            <div class="col-sm-5 tes" style="border: solid #ffffff 1.5px; padding:7px;">
              <div class="btn-group">
                <?php foreach ($subcategory as $key => $value) { ?>
                  <a href="javascript:void(0)" onclick="getmenu(<?php echo $key; ?>)" class="btn btn-info btn-md col-lg-12" style="margin-bottom:5px; white-space: normal; min-height: 50px; border-color:#ffffff">
                    <?php echo $value; ?>
                  </a>
                <?php } ?>
              </div>
            </div>
            <div class="col-sm-7 tes" style="border: solid #ffffff 1.5px; padding:5px;">
              <div class="table-responsive" style="border: solid #ffffff 2px; ">
                <table class="table table-responsive"  id="restomenu">
                  <thead>
                    <tr>
                      <th width="50%">MENU</th>
                      <th width="50%">PRICE</th>
                    </tr>
                  </thead>
                  <tbody id="bodymenu">

                  </tbody>
                </table>
              </div>
            </div>
            <p style="padding-top:250px"></p>
            <div class="col-sm-12" style="border: solid #ffffff 2px; padding:5px;">
              <div class="table-responsive" style=" max-height: 170px; min-height: 170px;">
                <table id="selectedmenu" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th style="display:none">ID</th>
                      <th>MENU</th>
                      <th>QTY</th>
                      <th>REQUEST</th>
                      <th width="3px">&nbsp;</th>
                    </tr>
                  </thead>
                  <tbody id="selectedmenubody">
                  </tbody>
                </table>
              </div>
            </div>

            <div class="col-sm-12" style="padding-top: 20px;">
              <div class="pull-right">
                <button type="button" id="menu_cancel" class="btn btn-default btn-md"><i class="fa fa-minus-square fa-lg"></i></button>
                <button type="button" id="menu_okay" class="btn btn-default btn-md"><i class="fa fa-check-square fa-lg"></i></button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-7">
        <div class="box box-success card card-3 bg-green-gradient" style="min-height: 560px; max-height: 560px">
          <div class="box-header">
            <b><h4 class="box-title" style="color:#ffffff;"><strong> Order Detail</strong></h4></b>
          </div>
          <div class="box-body">
            <div class="col-md-9" id="NamaGuest">
              <p > <b style="color:blue;" class="classGuestId">Guest : <?php echo (!empty($masterTrx)) ? $guestName : 'WAKL IN'; ?></b></p>
              <input type="hidden" id="guestId" value="<?php echo (!empty($masterTrx)) ? $masterTrx->idguest : '1'; ?>">
            </div>
            <div class="col-sm-3">
              <div class="pull-right">
                <a type="button" class="btn btn-default btn-md" id="ShowModalGuest" title="Add Guest"><i class="fa fa-user-plus fa-lg"></i></a>
                <a type="button" class="btn btn-default btn-md"><i class="fa fa-user-secret fa-lg"></i></a>
                <a type="button" class="btn btn-default btn-md"><i class="fa fa-exchange fa-lg"></i></a>
              </div>
            </div>

            <p style="padding-top:50px"></p>

            <div class="col-sm-12" >
              <div class="table-responsive"style="border: solid #ffffff 2px; " >
                <table id="example1" class="table table-bordered table-striped"  width="100%" style=" overflow: hidden;">
                  <thead style="display: inline-block; width: 679px;">
                    <tr>
                      <th width="111px">Menu</th>
                      <th width="98px">Price</th>
                      <th width="98px">Disc</th>
                      <th width="98px">Price 2</th>
                      <th width="66px">Qty</th>
                      <th width="98px">Sub Total</th>
                      <th width="94px">Operator</th>
                      <th  style="display:none;"></th>
                    </tr>
                  </thead>
                  <tbody style="display: inline-block; width: 100%; min-height: 191px; max-height: 191px; overflow-y: scroll; overflow-x: hidden;" id="fixedMenu">
                    <?php if (!empty($masterTrx)){
                      $detailMenus = $this->M_resto->Get_trx_menu_detail($masterTrx->trxid)->result();
                      foreach ($detailMenus as $row) {
                        $price2 = $row->price - (($row->price*$row->disp)/100); ?>
                  			<tr style='background-color: #f39c12'>
                  			<td width='111px' style='background-color: #f39c12' ><?php echo $row->menu ?></td>
                  			<td width='98px' style='background-color: #f39c12'> <?php echo number_format($row->price, 2, ',', '.') ?></td>
                  			<td width='98px' style='background-color: #f39c12'><?php echo number_format($row->disp, 2, ',', '.')?></td>
                  			<td width='98px' style='background-color: #f39c12'> <?php echo number_format($price2, 2, ',', '.')?></td>
                  			<td width='66px' style='background-color: #f39c12'> <?php echo number_format($row->qty, 0, ',', '.') ?></td>
                  			<td width='98px' style='background-color: #f39c12'><?php echo number_format($row->subtotal, 2, ',', '.')?></td>
                  			<td width='94px' colspan='2' style='background-color: #f39c12'> <?php echo $row->idoperator ?></td>
                  			</tr>
                        <?php } ?>
                        <tr style="display:none;"><td><input type="text" id="payNewID" value="<?php echo $masterTrx->trxid; ?>"></td></tr>
                    <?php }else { ?>
                     <tr>
                        <td colspan="7" style='background-color: #00a65a' width="679px">No data available in table</td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="col-sm-7" style="padding-top:135px">
             <button type="button" class="btn btn-default btn-md"><i class="fa fa-print fa-lg"></i></button>
                <button type="button" class="btn btn-default btn-md"><i class="fa fa-edit fa-lg"></i></button>
                <button type="button" class="btn btn-default btn-md"><i class="fa fa-times-circle fa-lg"></i></button>
            </div>
            
            <div class="col-sm-2" style="padding-top:30px;  background-color:#99FFD6; margin-top:10px">
              <b>TOTAL</b> <br>
              <b>DISC</b> <br>
              <b>TOTAL 2</b> <br>
              <b style="color:blue;">SERVICE</b> <br>
              <b>TAX</b> <br>
              <b>GRAND TOTAL</b>
            </div>
            <div class="col-sm-1" style="padding-top:30px;  background-color:#99FFD6; margin-top:10px">
              <b>:</b> <br>
              <b>:</b> <br>
              <b>:</b> <br>
              <b style="color:blue;">:</b> <br>
              <b>:</b> <br>
              <b>:</b> <br>
            </div>
            <div class="col-sm-2" style="padding-top:30px; background-color:#99FFD6; margin-top:10px" id="paymentMenu"  >
              <?php if (!empty($masterTrx)){ ?>
                <b class='pull-right' id='pmTotal'><?php echo  number_format($masterTrx->total, 2, ',', '.')?></b><br>
            		<b class='pull-right' id='pmDisc'><?php echo  number_format($masterTrx->disc, 2, ',', '.') ?></b><br>
            		<b class='pull-right' id='pmTotal2'><?php echo  number_format($masterTrx->total2, 2, ',', '.')?></b><br>
            		<b class='pull-right' id='pmService' style='color:blue;'> <?php echo  number_format($masterTrx->service, 2, ',', '.') ?></b><br>
            		<b class='pull-right' id='pmTax'> <?php echo  number_format($masterTrx->tax, 2, ',', '.') ?></b><br>
            		<b class='pull-right' id='pmGrandTotal'> <?php echo  number_format($masterTrx->grandtotal, 2, ',', '.') ?></b><br>
              <?php }else { ?>
                <b class="pull-right" id="pmTotal">0,00</b><br>
                <b class="pull-right" id="pmDisc">0,00</b><br>
                <b class="pull-right" id="pmTotal2">0,00</b><br>
                <b class="pull-right" id="pmService" style="color:blue;">0,00</b><br>
                <b class="pull-right" id="pmTax">0,00</b><br>
                <b class="pull-right" id="pmGrandTotal">0,00</b><br>
              <?php } ?>
            </div>


            <div class="col-sm-12" style="padding-top: 24px;">
             <!--  <div class="pull-right">
                <button type="button" class="btn btn-default btn-md"><i class="fa fa-print fa-lg"></i></button>
                <button type="button" class="btn btn-default btn-md"><i class="fa fa-edit fa-lg"></i></button>
                <button type="button" class="btn btn-default btn-md"><i class="fa fa-times-circle fa-lg"></i></button>
              </div> -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  


<script type="text/javascript">
  $(document).ready(function(){
     $("#ShowModalGuest").click(function(e){ 
     //var idvendor = $('#IdVendor').val();        
      $.ajax({
        type: "POST",
       url: '<?php echo site_url('Resto/select_guest');?>',
        data: {idvendor: '2'  },
        success: function(data){
            e.preventDefault();
            var mymodal1 = $('#myModalGuest');
            mymodal1.modal({backdrop: 'static', keyboard: false}) 
            var height = $(window).height() - 200;
            mymodal1.find(".modal-body").css("max-height", height);
            mymodal1.find('.modal-body').html(data);
            mymodal1.modal('show');            
        }
      });
    });
  });
</script>


<script type="text/javascript">
  var pV = {};

  $(document).ready(function() {
    pV.tb = $('#selectedmenu').DataTable({
      "ordering" : false,
      "searching" : false,
      "lengthChange" : false,
      "paging" : false,
      "info" : false,
      "iDisplayLength" : "100",

      columns : [
        {data : 'ID', "bVisible": false},
        {data : 'MENU'},
        {data : 'QTY'},
        {data : 'REQUEST'},
        {data : 'ACTION'}
      ]
    });


  });

  function addNewRow(id_menu, menu, qty, request) {
    var datas = [{
      'ID' : id_menu,
      'MENU' : menu,
      'QTY' : qty.replace(/\B(?=(\d{3})+(?!\d))/g, "."),
      'REQUEST' :  request,
      'ACTION' : '<center><button name="deletRow" class="btn btn-danger btn-xs" ><i class="fa fa-close"></i></button></center>'
    }];
    datas.forEach(function(dat) {
      pV.tb.row.add(dat).draw();
    });
    hitung_total();
  }

  function getmenu(id) {
    $.ajax({
      type: "POST",
      url: '<?php echo site_url('Resto/Get_menu_from_subcategory');?>',
      dataType : "html",
      data: {id: id},
      success: function(data){
        $("#bodymenu").html(data);
        $("#bodymenu td.td-click").click(function(){

          var idmenu = $(this).parent().attr("id-menu");
          var nama = $(this).parent().attr("nm-menu");
          inputQty(nama, idmenu);
        });
      }
    });
  }

  function menu_req_sama(i, qty, subtotal) {
    qty = parseInt(qty.replace(/\./g, '')) + parseInt(pV.tb.cell(i, 2).data().replace(/\./g, ''));
    pV.tb.cell(i, 2).data(qty.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
    hitung_total();
  }

  $('#selectedmenu tbody').on('click', 'button[name=deletRow]', function() {
    pV.tb.row($(this).parents('tr')).remove().draw(false);
    hitung_total();
  });

  function customAlert(msg) {
    $.alert({
      title: 'Caution!!',
      content: msg,
      icon: 'fa fa-warning',
      type: 'orange',
    });
  }

  function inputQty(menu_name, menu_id) {
    $('#addMenuQtyReq').modal({backdrop: 'static', keyboard: false});
    $("#qtyHeader").html(menu_name);
    $('#addMenuQtyReq').find(".qty").val("1");
    $('#addMenuQtyReq').find(".req").val("");
    $("#qMenuId").val(menu_id);
    $("#qMenu").val(menu_name);
    // $.confirm({
    //   title: '<b>'+menu_name+'</b>',
    //   content: '' +
    //   '<input type="hidden" id="qMenuId" value="'+menu_id+'">' +
    //   '<input type="hidden" id="qMenu" value="'+menu_name+'">' +
    //   '<div class="form-group">' +
    //   '<label>Quantity</label>' +
    //   '<input type="text" placeholder="Qty" class="qty form-control input-1" required value="1"/>' +
    //   '</div>' +
    //   '<div class="form-group">' +
    //   '<label>Request</label>' +
    //   '<textarea placeholder="Request" class="req form-control" rows="3" autocomplete="off"></textarea>' +
    //   '</div>',
    //   tabindex: -100,
    //   buttons: {
    //     formSubmit: {
    //         text: 'Submit',
    //         btnClass: 'btn-blue',
    //         action: function () {
    //             var qty = this.$content.find('.qty').val();
    //             if(!qty){
    //                 customAlert('Please input Quantity field ...');
    //                 return false;
    //             }
    //             var id_menu = this.$content.find('#qMenuId').val();
    //             var menu = this.$content.find('#qMenu').val();
    //             var req = this.$content.find('.req').val();
    //             newMenu(id_menu, menu, qty, req);
    //             // $.alert(id_menu+" ||| "+menu+" ||| "+qty+" ||| "+req);
    //         }
    //     },
    //     cancel: function () {
    //         //close
    //     },
    //   },
    //   onContentReady: function () {
    //     // bind to events
    //     var jc = this;
    //     this.$content.find('form').on('submit', function (e) {
    //         // if the user submits the form by pressing enter in the field.
    //         e.preventDefault();
    //         jc.$$formSubmit.trigger('click'); // reference the button and click it
    //     });
    //   }
    // });
  }

  function confrimAlert() {
    var pmTotal = $("#pmTotal").html().replace(/\./g, '');
    var pmDisc = $("#pmDisc").html().replace(/\./g, '');
    var pmTotal2 = $("#pmTotal2").html().replace(/\./g, '');
    var pmService = $("#pmService").html().replace(/\./g, '');
    var pmTax = $("#pmTax").html().replace(/\./g, '');
    var pmGrandTotal = $("#pmGrandTotal").html().replace(/\./g, '');
    $.confirm({
      title: 'Confirmation',
      content: 'Are you sure all of the order ?',
      buttons: {
        formSubmit: {
          text: 'Confrim',
          btnClass: 'btn-blue',
          action: function () {
            var datas = pV.tb.rows().data().toArray();
            $.ajax({
              type: "POST",
              url: '<?php echo site_url("Resto/addMenuToDB"); ?>',
              dataType: 'html',
              cache: false,
              data: {datas: datas, payNewID: $("#payNewID").val(), guestId : $("#guestId").val(), operator: "1", tableId: $("input[name=tableId]").val(),
              pmTotal : pmTotal, pmDisc : pmDisc, pmTotal2 : pmTotal2, pmService : pmService, pmTax : pmTax, pmGrandTotal : pmGrandTotal},
              success: function(respon){
                $("#fixedMenu").html(respon);
                pV.tb.rows().clear().draw();
              }
            });
          }
        },
        cancel: function () {

        }
      }
    });
  }

  function hitung_total() {
    var datas = pV.tb.rows().data().toArray();
    var pmTotal = $("#pmTotal").html().replace(/\./g, '');
    var pmDisc = $("#pmDisc").html().replace(/\./g, '');
    var pmTotal2 = $("#pmTotal2").html().replace(/\./g, '');
    var pmService = $("#pmService").html().replace(/\./g, '');
    var pmTax = $("#pmTax").html().replace(/\./g, '');
    var pmGrandTotal = $("#pmGrandTotal").html().replace(/\./g, '');
    var masterTRX = $("#payNewID").val();
    $.ajax({
      type: "POST",
      url: '<?php echo site_url("Resto/Count_total_menu"); ?>',
      dataType: 'html',
      cache: false,
      data: {datas: datas, pmTotal : pmTotal, pmDisc : pmDisc, pmTotal2 : pmTotal2, pmService : pmService, pmTax : pmTax, pmGrandTotal : pmGrandTotal, masterTRX : masterTRX},
      success: function(respon){
        $("#paymentMenu").html(respon);
      }
    });
  }

  $(document).on("click", "#menu_cancel", function() {
    pV.tb.rows().clear().draw();
  });
</script>


<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>
<style type="text/css">
  .box-widget {
 box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
}
.modal.modal-wide .modal-dialog {
  width: 90%;
}

.modal-wide .modal-body {
  overflow-y: auto;
}

#tallModal .modal-body p { margin-bottom: 900px }
.pull-right {
    float: right;
}

.pull-left {
    float: left;
}
</style>

<!-- Content Header (Page header) -->
<section class="content-header" align="center" style="margin-bottom: 24px; ">
   
    <h3> 
        Payment Report
    </h3>
    
</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-xs-12">
    <?php $this->load->view('template/msg_sukses'); ?>


  <div class="col-xs-12">
      <div class="box box-success box-widget">
        <div class="box-header" >
          <h6 class="box-title" style="font-size: 15px;" >MEMORANDUM INVOICE</h6>
        </div>
        <div class="box-body">
    <div class="row">
      <div class="col-sm-4">
         <div class="form-group">
            <label>NO Receive Order</label>
            <div class="input-group date">
            <input type="text" name="NoRo" id="NoRo"  class="form-control input-2">
            <div class="input-group-addon " id="CariRo" >
                <i class="fa fa-search"></i>
              </div>
            </div>
          </div>
        </div>
     
        <div class="col-sm-2">
          <div class="form-group">
             <a style="margin-top: 24px; " class="btn btn-default MI"><i class='glyphicon glyphicon-print'></i> Cetak</a>
          </div>
        </div> 
      </div>
       <div class="row">
      <div class="col-sm-4">
         <div class="form-group">
            <label>Dari Tanggal</label>
            <div class="input-group date">
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
              <input type="text"  data-validation="date" data-validation-format="mm/dd/yyyy" data-validation-error-msg="Tanggal Lahir Belum Dipilih"  data-validation-require-leading-zero="false" class="form-control pull-right" name= "TglLahir" id="TglAwalMI">
            </div>
          </div>
        </div>
      <div class="col-sm-4">
          <div class="form-group">
            <label>Sampai Tanggal</label>
            <div class="input-group date">
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
              <input type="text"  data-validation="date" data-validation-format="mm/dd/yyyy" data-validation-error-msg="Tanggal Lahir Belum Dipilih"  data-validation-require-leading-zero="false" class="form-control pull-right" name= "TglLahir" id="TglAkhirMI">
            </div>
          </div>
        </div>
        <div class="col-sm-2">
          <div class="form-group">
             <a style="margin-top: 24px; " class="btn btn-default CetakTglMI"><i class='glyphicon glyphicon-print'></i> Cetak</a>
          </div>
        </div> 
      </div>
    </div> 
   </div>
  </div>


   <div class="col-xs-12">
      <div class="box box-success box-widget">
        <div class="box-header" >
          <h6 class="box-title" style="font-size: 15px;">PAYMENT VOUCHER</h6>
        </div>
        <div class="box-body">
    <div class="row">
      <div class="col-sm-4">
         <div class="form-group">
            <label>NO Receive Order</label>
            <div class="input-group date">
            <input type="text" name="NoPay" id="NoPay"  class="form-control input-2">
            <div class="input-group-addon " id="CariNoPay" >
                <i class="fa fa-search"></i>
              </div>
            </div>
          </div>
        </div>
     
        <div class="col-sm-2">
          <div class="form-group">
             <a style="margin-top: 24px; " class="btn btn-default PaymentV"><i class='glyphicon glyphicon-print'></i> Cetak</a>
          </div>
        </div> 
      </div>
    </div> 
   </div>
  </div>

     <div class="col-xs-12">
      <div class="box box-success box-widget">
        <div class="box-header" >
          <h6 class="box-title" style="font-size: 15px;">REKAP VOUCHER</h6>
        </div>
        <div class="box-body">
            <div class="row">
            <div class="col-md-3" >
                   <div class="form-group">
                      <label> Dari No Voucher</label>
                    <select id="DariVoucherNo" class="form-control select2"  style="width: 100%;" name="DariVoucherNo">
                       <option value="0" >Pilih No Voucher</option>
                      <?php foreach($voucherno as $voucher){ ?>
                        <option value="<?php echo $voucher->voucherno; ?>" ><?php echo $voucher->voucherno; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                 <div class="col-md-3" >
                   <div class="form-group">
                      <label>Sampai No Voucher</label>
                    <select id="SampaiVoucherNo" class="form-control select2"  style="width: 100%;" name="SampaiVoucherNo">
                       <option value="0" >Pilih No Voucher</option>
                       <?php foreach($voucherno as $voucher1){ ?>
                        <option value="<?php echo $voucher1->voucherno; ?>" ><?php echo $voucher1->voucherno; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                  <div class="col-sm-2">
                    <div class="form-group">
                       <a style="margin-top: 24px; " class="btn btn-default Cetak_rekap_voucher_byno"><i class='glyphicon glyphicon-print'></i> Cetak</a>
                    </div>
                  </div>
                </div>

    <div class="row">
            <div class="col-md-2" >
                   <div class="form-group">
                      <label>Method</label>
                    <select id="MethodVoucher" class="form-control select2"  style="width: 100%;" name="BankCek">
                       <option value="0" >All Method</option>
                       <option value="1" >Cash</option>
                       <option value="2" >Check</option>
                    </select>
                  </div>
                </div>
                 <div class="col-md-3">
                    <div class="form-group">
                          <label class="control-label">Date From</label>
                          <input type="text" name="DateFromVo" id="DateFromVo" value="<?= date('d/m/Y') ?>" class="form-control">
                     </div>
                  </div>
              
                <div class="col-md-3">
                    <div class="form-group">
                          <label class="control-label">Date To</label>
                          <input type="text" name="DateToVo" id="DateToVo" value="<?= date('d/m/Y') ?>" class="form-control">
                     </div>
                  </div>
                  <div class="col-sm-2">
                  <div class="form-group">
                     <a style="margin-top: 24px; " class="btn btn-default RelapVo"><i class='glyphicon glyphicon-print'></i> Cetak</a>
                  </div>
                </div> 
                  </div>
                </div> 
               </div>
              </div>





    <div class="col-xs-12">
      <div class="box box-success box-widget">
        <div class="box-header" >
          <h6 class="box-title" style="font-size: 15px;">CHECK</h6>
        </div>
        <div class="box-body">
    <div class="row">
            <div class="col-md-2" >
                   <div class="form-group">
                      <label>Bank</label>
                    <select id="BankCek" class="form-control select2"  style="width: 100%;" name="BankCek">
                       <option value="0" >ALL BANK</option>
                      <?php foreach($bank as $bank1){ ?>
                        <option value="<?php echo $bank1->id; ?>" ><?php echo $bank1->namabank; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                  <div class="col-md-3" >
                   <div class="form-group">
                      <label>Vendor</label>
                    <select id="IdVendor" class="form-control select2"  style="width: 100%;" name="IdVendor">
                      <option value="0" >ALL VENDOR</option>
                      <?php foreach($datavendor as $vendor){?>
                        <option value="<?php echo $vendor->idvendor; ?>" ><?php echo $vendor->vendor; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                          <label class="control-label">Date To</label>
                          <input type="text" name="DateCek" id="DateCek" value="<?= date('d/m/Y') ?>" class="form-control">
                     </div>
                  </div>

                  <div class="col-sm-2">
                    <div class="form-group">
                       <a style="margin-top: 24px; " class="btn btn-default Cek"><i class='glyphicon glyphicon-print'></i> Cetak</a>
                    </div>
                  </div>
                </div>


            <div class="row">
                  <div class="col-md-2" >
                   <div class="form-group">
                      <label>Bank</label>
                    <select id="BankCekPeriode" class="form-control select2"  style="width: 100%;" name="BankCekPeriode">
                       <option value="0" >ALL BANK</option>
                      <?php foreach($bank as $bank1){ ?>
                        <option value="<?php echo $bank1->id; ?>" ><?php echo $bank1->namabank; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                  <div class="col-md-3" >
                   <div class="form-group">
                      <label>Vendor</label>
                    <select id="IdVendor" class="form-control select2"  style="width: 100%;" name="IdVendorPeriode">
                      <option value="0" >ALL VENDOR</option>
                      <?php foreach($datavendor as $vendor){?>
                        <option value="<?php echo $vendor->idvendor; ?>" ><?php echo $vendor->vendor; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                          <label class="control-label">Date From</label>
                          <input type="text" name="DateCekFromPeriode" id="DateCekFromPeriode" value="<?= date('d/m/Y') ?>" class="form-control">
                     </div>
                  </div>
                <div class="col-md-2">
                    <div class="form-group">
                          <label class="control-label">Date To</label>
                          <input type="text" name="DateCek" id="DateCekPeriode" value="<?= date('d/m/Y') ?>" class="form-control">
                     </div>
                  </div>
                  
     
        <div class="col-sm-2">
          <div class="form-group">
             <a style="margin-top: 24px; " class="btn btn-default CekPeriode"><i class='glyphicon glyphicon-print'></i> Cetak</a>
          </div>
      </div>
      </div>
    </div> 
   </div>
  </div>


      <div class="col-xs-12">
      <div class="box box-success box-widget">
        <div class="box-header" >
          <h6 class="box-title" style="font-size: 15px;">CASH</h6>
        </div>
        <div class="box-body">
    <div class="row">
            
                  <div class="col-md-3" >
                   <div class="form-group">
                      <label>Vendor</label>
                    <select id="IdVendorCash" class="form-control select2"  style="width: 100%;" name="IdVendorCash">
                      <option value="0" >ALL VENDOR</option>
                      <?php foreach($datavendor as $vendor){?>
                        <option value="<?php echo $vendor->idvendor; ?>" ><?php echo $vendor->vendor; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                          <label class="control-label">Date To</label>
                          <input type="text" name="DateCash" id="DateCash" value="<?= date('d/m/Y') ?>" class="form-control">
                     </div>
                  </div>
                  
     
        <div class="col-sm-2">
          <div class="form-group">
             <a style="margin-top: 24px; " class="btn btn-default Cash"><i class='glyphicon glyphicon-print'></i> Cetak</a>
          </div>
        </div> 
      </div>
    </div> 
   </div>
  </div>


<!--         <div class="col-xs-12">
      <div class="box box-success box-widget">
        <div class="box-header" >
          <h6 class="box-title" style="font-size: 15px;">REALISASI PO</h6>
        </div>
        <div class="box-body">
    <div class="row">
            
                  <div class="col-md-3" >
                   <div class="form-group">
                      <label>Vendor</label>
                    <select id="IdVendorRpo" class="form-control select2"  style="width: 100%;" name="IdVendorRpo">
                      <option value="0" >ALL VENDOR</option>
                      <?php foreach($datavendor as $vendor){?>
                        <option value="<?php echo $vendor->idvendor; ?>" ><?php echo $vendor->vendor; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                          <label class="control-label">Date From</label>
                          <input type="text" name="DateFromRpo" id="DateFromRpo" value="<?= date('d/m/Y') ?>" class="form-control">
                     </div>
                  </div>
                <div class="col-md-2">
                    <div class="form-group">
                          <label class="control-label">Date To</label>
                          <input type="text" name="DateToRpo" id="DateToRpo" value="<?= date('d/m/Y') ?>" class="form-control">
                     </div>
                  </div>
                  
     
        <div class="col-sm-2">
          <div class="form-group">
             <a style="margin-top: 24px; " class="btn btn-default Rpo"><i class='glyphicon glyphicon-print'></i> Cetak</a>
          </div>
        </div> 
      </div>
    </div> 
   </div>
  </div> -->


  <div class="col-xs-12" style="display:none;">
      <div class="box box-success box-widget">
        <div class="box-header" >
          <h6 class="box-title" style="font-size: 15px;">PURCHASE ORDER</h6>
        </div>
        <div class="box-body">
    <div class="row">
      <div class="col-sm-4">
         <div class="form-group">
            <label>NO Purchase Order</label>
            <div class="input-group date">
            <input type="text" name="NoPO" id="NoPO"  class="form-control input-2">
            <div class="input-group-addon " id="CariNoPO" >
                <i class="fa fa-search"></i>
              </div>
            </div>
          </div>
        </div>
     
        <div class="col-sm-2">
          <div class="form-group">
             <a style="margin-top: 24px; " class="btn btn-default PO"><i class='glyphicon glyphicon-print'></i> Cetak</a>
          </div>
        </div> 
      </div>
    </div> 
   </div>
  </div>

  <div class="col-xs-12" style="display:none;">
      <div class="box box-success box-widget">
        <div class="box-header" >
          <h6 class="box-title" style="font-size: 15px;">PURCHASE REQUISITION</h6>
        </div>
        <div class="box-body">
    <div class="row">
      <div class="col-sm-4">
         <div class="form-group">
            <label>NO Purchase Requisition</label>
            <div class="input-group date">
            <input type="text" name="NoPR" id="NoPR"  class="form-control input-2">
            <div class="input-group-addon " id="CariNoPR" >
                <i class="fa fa-search"></i>
              </div>
            </div>
          </div>
        </div>
     
        <div class="col-sm-2">
          <div class="form-group">
             <a style="margin-top: 24px; " class="btn btn-default PR"><i class='glyphicon glyphicon-print'></i> Cetak</a>
          </div>
        </div> 
      </div>
    </div> 
   </div>
  </div>

   <div class="col-xs-12" style="display:none;">
      <div class="box box-success box-widget">
        <div class="box-header" >
          <h6 class="box-title" style="font-size: 15px;">STORE REQUISITION</h6>
        </div>
        <div class="box-body">
    <div class="row">
      <div class="col-sm-4">
         <div class="form-group">
            <label>NO Store Requisition</label>
            <div class="input-group date">
            <input type="text" name="NoSR" id="NoSR"  class="form-control input-2">
            <div class="input-group-addon " id="CariNoSR" >
                <i class="fa fa-search"></i>
              </div>
            </div>
          </div>
        </div>
     
        <div class="col-sm-2">
          <div class="form-group">
             <a style="margin-top: 24px; " class="btn btn-default SR"><i class='glyphicon glyphicon-print'></i> Cetak</a>
          </div>
        </div> 
      </div>
    </div> 
   </div>
  </div>

  <div class="col-xs-12" style="display:none;">
      <div class="box box-success box-widget">
        <div class="box-header" >
          <h6 class="box-title" style="font-size: 15px;">STOCK OPNAME</h6>
        </div>
        <div class="box-body">
    <div class="row">
      <div class="col-sm-4">
         <div class="form-group">
            <label>Stock Opname Date</label>
            <div class="input-group date">
            <input type="text" name="NoSO" id="NoSO"  class="form-control input-2">
            <div class="input-group-addon " id="CariNoSO" >
                <i class="fa fa-search"></i>
              </div>
            </div>
          </div>
        </div>
     
        <div class="col-sm-2">
          <div class="form-group">
             <a style="margin-top: 24px; " class="btn btn-default SO"><i class='glyphicon glyphicon-print'></i> Cetak</a>
          </div>
        </div> 
      </div>
    </div> 
   </div>
  </div>

    <div class="col-xs-12" style="display:none;">
      <div class="box box-success box-widget">
        <div class="box-header" >
          <h6 class="box-title" style="font-size: 15px;">BAD STOCK</h6>
        </div>
        <div class="box-body">
    <div class="row">
      <div class="col-sm-4">
         <div class="form-group">
            <label>Bad Strok NO</label>
            <div class="input-group date">
            <input type="text" name="NoBS" id="NoBS"  class="form-control input-2">
            <div class="input-group-addon " id="CariNoBS" >
                <i class="fa fa-search"></i>
              </div>
            </div>
          </div>
        </div>
     
        <div class="col-sm-2">
          <div class="form-group">
             <a style="margin-top: 24px; " class="btn btn-default BS"><i class='glyphicon glyphicon-print'></i> Cetak</a>
          </div>
        </div> 
      </div>
    </div> 
   </div>
  </div>


      <div class="col-xs-12" style="display:none;">
      <div class="box box-success box-widget">
        <div class="box-header" >
          <h6 class="box-title" style="font-size: 15px;">STOCK CARD</h6>
        </div>
        <div class="box-body">
        <div class="row">
          <div class="col-sm-3">
             <div class="form-group">
                    <label>Date From</label>
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text"   value="<?= date('m/d/Y') ?>"     class="form-control pull-right" name= "Tgl1BS" id="Tgl1BS">
                      </div>
                  </div>
            </div>
      
          <div class="col-sm-3">
             <div class="form-group">
                    <label>Date To</label>
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text"   value="<?= date('m/d/Y') ?>"     class="form-control pull-right" name= "Tgl2BS" id="Tgl2BS">
                      </div>
                  </div>
            </div>
        </div> 
         <div class="row">
            <div class="col-sm-6">
             <div class="form-group">
                    <label>Item</label>
                      <div class="form-group">
                         <input type="text" name="CariItemBS" id="CariItemBS"  class="form-control input-2">
                          <input type="hidden" name="NOItemBS" id="NOItemBS"  class="form-control input-2">
                      </div>
                  </div>
            </div>
        
        <div class="col-sm-2">
          <div class="form-group">
             <a style="margin-top: 24px; " class="btn btn-default CS"><i class='glyphicon glyphicon-print'></i> Cetak</a>
          </div>
        </div>
      </div>

      </div>
    </div> 
   </div>
  </div>


        
</section><!-- /.content -->
      <div class="modal modal-wide fade" id="ModalRo" role="dialog">
          <div class="modal-dialog modal-sm">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 align="center" class="modal-title">Receive Order</h4>
                  </div>
                  <div class="modal-body">
                      
                  </div>
                  
              </div>
          </div>
      </div>

      <div class="modal modal-wide fade" id="ModalVoucher" role="dialog">
          <div class="modal-dialog modal-sm">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 align="center" class="modal-title">Payment Voucher</h4>
                  </div>
                  <div class="modal-body">
                      
                  </div>
                  
              </div>
          </div>
      </div>

       <div class="modal modal-wide fade" id="ModalPR" role="dialog">
          <div class="modal-dialog modal-sm">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 align="center" class="modal-title">Purchase Requisition List</h4>
                  </div>
                  <div class="modal-body">
                      
                  </div>
                  
              </div>
          </div>
      </div>

      <div class="modal modal-wide fade" id="ModalSR" role="dialog">
          <div class="modal-dialog modal-sm">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 align="center" class="modal-title">Store Requisition List</h4>
                  </div>
                  <div class="modal-body">
                      
                  </div>
                  
              </div>
          </div>
      </div>

      <div class="modal modal-wide fade" id="ModalPO" role="dialog">
          <div class="modal-dialog modal-sm">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 align="center" class="modal-title">Purchase Order List</h4>
                  </div>
                  <div class="modal-body">
                      
                  </div>
                  
              </div>
          </div>
      </div>

       <div class="modal modal-wide fade" id="ModalSO" role="dialog">
          <div class="modal-dialog modal-sm">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 align="center" class="modal-title">Stock Opname List</h4>
                  </div>
                  <div class="modal-body">
                      
                  </div>
                  
              </div>
          </div>
      </div>

       <div class="modal modal-wide fade" id="ModalBS" role="dialog">
          <div class="modal-dialog modal-sm">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 align="center" class="modal-title">Bad Stock List</h4>
                  </div>
                  <div class="modal-body">
                      
                  </div>
                  
              </div>
          </div>
      </div>

       <div class="modal modal-wide fade" id="ModalCS" role="dialog">
          <div class="modal-dialog modal-sm">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 align="center" class="modal-title">Item List</h4>
                  </div>
                  <div class="modal-body">
                      
                  </div>
                  
              </div>
          </div>
      </div>



<script type="text/javascript">
  $(function () {  
  $(".MI").click(function(){
      var NoRO = $( "#NoRo" ).val();
      window.open("<?php echo base_url(). 'index.php/Purchase_order/CetakMI/';?>?NoRo="+NoRO ,"MyTargetWindowName")
  }); 
});
</script>
<script type="text/javascript">
  $(function () {  
  $(".CetakTglMI").click(function(){
      var TglAwal = $( "#TglAwalMI" ).val();
      var TglAkhir = $( "#TglAkhirMI" ).val();
      window.open("<?php echo base_url(). 'index.php/Purchase_order/CetakMI_byTgl/';?>?Status="+ 'Semua' +"&TglAwal="+ TglAwal+"&TglAkhir="+TglAkhir ,"MyTargetWindowName")
  }); 
});
</script>

<script type="text/javascript">
  $(function () {  
  $(".Cek").click(function(){
    var DateCek  = $( "#DateCek" ).val();
    var IdVendor = $( "#IdVendor" ).val();
    var BankCek= $( "#BankCek" ).val();

      window.open("<?php echo base_url(). 'index.php/Payment/Cetak_payment_cek/';?>?Status="+ 'Semua' +"&DateCek="+ DateCek+"&IdVendor="+IdVendor+"&BankCek="+BankCek ,"MyTargetWindowName")
  }); 
});
</script>

<script type="text/javascript">
  $(function () {  
  $(".CekPeriode").click(function(){
    var DateCek  = $( "#DateCekPeriode" ).val();
    var DateCekFromPeriode  = $( "#DateCekFromPeriode" ).val();
    var IdVendor = $( "#IdVendor" ).val();
    var BankCek= $( "#BankCek" ).val();

      window.open("<?php echo base_url(). 'index.php/Payment/Cetak_payment_cek_periode/';?>?Status="+ 'Semua' +"&DateCek="+ DateCek+"&IdVendor="+IdVendor+"&BankCek="+BankCek+"&DateCekFromPeriode="+DateCekFromPeriode ,"MyTargetWindowName")
  }); 
});
</script>

<script type="text/javascript">
  $(function () {  
  $(".Cash").click(function(){
    var DateCash  = $( "#DateCash" ).val();
    var IdVendorCash = $( "#IdVendorCash" ).val();

      window.open("<?php echo base_url(). 'index.php/Payment/Cetak_payment_cash/';?>?Status="+ 'Semua' +"&DateCash="+ DateCash+"&IdVendorCash="+IdVendorCash ,"MyTargetWindowName")
  }); 
});
</script>
<script type="text/javascript">
  $(function () {  
  $(".Rpo").click(function(){
    var DateFromRpo  = $( "#DateFromRpo" ).val();
    var DateToRpo  = $( "#DateToRpo" ).val();
    var IdVendorRpo = $( "#IdVendorRpo" ).val();

      window.open("<?php echo base_url(). 'index.php/Payment/Cetak_realisasi_po/';?>?Status="+ 'Semua' +"&DateFromRpo="+ DateFromRpo+"&DateToRpo="+DateToRpo+"&IdVendorRpo="+IdVendorRpo ,"MyTargetWindowName")
  }); 
});
</script>

<script type="text/javascript">
  $(function () {  
  $(".RelapVo").click(function(){
    var DateFromVo  = $( "#DateFromVo" ).val();
    var DateToVo  = $( "#DateToVo" ).val();
    var MethodVoucher = $( "#MethodVoucher" ).val();

        window.open("<?php echo base_url(). 'index.php/Payment/Cetak_rekap_voucher/';?>?Status="+ 'Semua' +"&DateFromVo="+ DateFromVo+"&DateToVo="+DateToVo+"&MethodVoucher="+MethodVoucher ,"MyTargetWindowName")
  }); 
});
</script>

<script type="text/javascript">
  $(function () {  
  $(".Cetak_rekap_voucher_byno").click(function(){
    var nodari  = $( "#DariVoucherNo" ).val();
    var nosampai  = $( "#SampaiVoucherNo" ).val();
        window.open("<?php echo base_url(). 'index.php/Payment/Cetak_rekap_voucher_byno/';?>?Status="+ 'Semua' +"&nodari="+ nodari+"&nosampai="+nosampai ,"MyTargetWindowName")
  }); 
});
</script>



<script type="text/javascript">
  $(function () {  
  $(".PaymentV").click(function(){
      var NoPay = $( "#NoPay" ).val();
      window.open("<?php echo base_url(). 'index.php/Purchase_order/CetakPV/';?>?NoPay="+NoPay ,"MyTargetWindowName")
  }); 
});
</script>

<script type="text/javascript">
  $(function () {  
  $(".PR").click(function(){
      var kodepr = $( "#NoPR" ).val();
      window.open("<?php echo base_url(). 'index.php/Purchase_requisition/CetakPR/';?>?KodePR="+kodepr ,"MyTargetWindowName")
  }); 
});
</script>

<script type="text/javascript">
  $(function () {  
  $(".SR").click(function(){
      var kodesr = $( "#NoSR" ).val();
      window.open("<?php echo base_url(). 'index.php/Store_requisition/CetakSR/';?>?KodeSR="+kodesr ,"MyTargetWindowName")
  }); 
});
</script>

<script type="text/javascript">
  $(function () {  
  $(".PO").click(function(){
      var kodepo = $( "#NoPO" ).val();
      window.open("<?php echo base_url(). 'index.php/Purchase_order/CetakPO/';?>?NoPO="+kodepo ,"MyTargetWindowName")
  }); 
});
</script>

<script type="text/javascript">
  $(function () {  
  $(".SO").click(function(){
      var kodeso = $( "#NoSO" ).val();
      window.open("<?php echo base_url(). 'index.php/Stok_opname/CetakSO/';?>?Tgl="+kodeso ,"MyTargetWindowName")
  }); 
});
</script>

<script type="text/javascript">
  $(function () {  
  $(".BS").click(function(){
      var nobs = $( "#NoBS" ).val();
      window.open("<?php echo base_url(). 'index.php/Bad_stok/CetakBS/';?>?nobs="+nobs ,"MyTargetWindowName")
  }); 
});
</script>

<script type="text/javascript">
  $(function () {  
  $(".CS").click(function(){
      var TglAwal = $( "#Tgl1BS" ).val();
      var TglAkhir = $( "#Tgl2BS" ).val();
      var kode = $( "#NOItemBS" ).val();
       window.open("<?php echo base_url(). 'index.php/Stok_card/CetakSC/';?>?codeitem="+ kode+"&tgl1="+ TglAwal+"&tgl2="+TglAkhir ,"MyTargetWindowName")
          // window.open("<?php echo base_url(). 'index.php/Bad_stok/CetakSC/';?>?tgl1="+ TglAwal+"&tgl2="+TglAkhir ,"MyTargetWindowName")

  }); 
});
</script>






<?php
$this->load->view('template/Js');
$this->load->view('template/Foot');
?>

<script type="text/javascript">
        $("#CariNoPO").click(function(e){

      $.ajax({
        type: "POST",
        url: '<?php echo site_url('Purchase_order/purchase_order_list');?>',
        data: {id: '1'},
        success: function(data){

             e.preventDefault();
             var mymodal = $('#ModalPO');
             var height = $(window).height() - 200;
            mymodal.find(".modal-body").css("max-height", height);
             mymodal.find('.modal-body').html(data);
            mymodal.modal('show');           
        }
      });
  });
</script> 
   
<script type="text/javascript">
        $("#CariRo").click(function(e){

      $.ajax({
        type: "POST",
        url: '<?php echo site_url('Purchase_order/Select_ro');?>',
        data: {id: '1'},
        success: function(data){

             e.preventDefault();
             var mymodal = $('#ModalRo');
             var height = $(window).height() - 200;
            mymodal.find(".modal-body").css("max-height", height);
             mymodal.find('.modal-body').html(data);
            mymodal.modal('show');           
        }
      });
  });
</script> 

<script type="text/javascript">
        $("#CariNoPay").click(function(e){

      $.ajax({
        type: "POST",
        url: '<?php echo site_url('Purchase_order/Select_payment');?>',
        data: {id: '1'},
        success: function(data){

             e.preventDefault();
             var mymodal = $('#ModalVoucher');
             var height = $(window).height() - 200;
            mymodal.find(".modal-body").css("max-height", height);
             mymodal.find('.modal-body').html(data);
            mymodal.modal('show');           
        }
      });
  });
</script> 

<script type="text/javascript">
        $("#CariNoPR").click(function(e){

      $.ajax({
        type: "POST",
        url: '<?php echo site_url('Purchase_requisition/daftar_pr');?>',
        data: {id: '1'},
        success: function(data){

             e.preventDefault();
             var mymodal = $('#ModalPR');
             var height = $(window).height() - 200;
            mymodal.find(".modal-body").css("max-height", height);
             mymodal.find('.modal-body').html(data);
            mymodal.modal('show');           
        }
      });
  });
</script> 

<script type="text/javascript">
        $("#CariNoSR").click(function(e){

      $.ajax({
        type: "POST",
        url: '<?php echo site_url('Store_requisition/daftar_sr');?>',
        data: {id: '1'},
        success: function(data){

             e.preventDefault();
             var mymodal = $('#ModalSR');
             var height = $(window).height() - 200;
            mymodal.find(".modal-body").css("max-height", height);
             mymodal.find('.modal-body').html(data);
            mymodal.modal('show');           
        }
      });
  });
</script>

<script type="text/javascript">
        $("#CariNoSO").click(function(e){

      $.ajax({
        type: "POST",
        url: '<?php echo site_url('Stok_opname/daftar_so');?>',
        data: {id: '1'},
        success: function(data){

             e.preventDefault();
             var mymodal = $('#ModalSO');
             var height = $(window).height() - 200;
            mymodal.find(".modal-body").css("max-height", height);
             mymodal.find('.modal-body').html(data);
            mymodal.modal('show');           
        }
      });
  });
</script> 


<script type="text/javascript">
    $("#CariNoBS").click(function(e){
      $.ajax({
        type: "POST",
        url: '<?php echo site_url('Bad_stok/daftar_bs');?>',
        data: {id: '1'},
        success: function(data){

             e.preventDefault();
             var mymodal = $('#ModalBS');
             var height = $(window).height() - 200;
            mymodal.find(".modal-body").css("max-height", height);
             mymodal.find('.modal-body').html(data);
            mymodal.modal('show');           
        }
      });
  });
</script> 


<script type="text/javascript">
    $("#CariItemBS").click(function(e){
      $.ajax({
        type: "POST",
        url: '<?php echo site_url('Stok_card/daftar_item');?>',
        data: {id: '1'},
        success: function(data){

             e.preventDefault();
             var mymodal = $('#ModalCS');
             var height = $(window).height() - 200;
            mymodal.find(".modal-body").css("max-height", height);
             mymodal.find('.modal-body').html(data);
            mymodal.modal('show');           
        }
      });
  });
</script> 




  <script>
  $( function() {
    $( "#TglAkhirMI" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });
    $( "#TglAwalMI" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });
    $( "#TglAwalPer" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });
    $( "#TglAkhirPer" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });
     $( "#PerTanggal" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });
      $( "#DateCek" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });
       $( "#DateCekPeriode" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });
        $( "#DateCekFromPeriode" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });
       $( "#DateCash" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });
         $( "#DateFromRpo" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });
           $( "#DateToRpo" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });
      $( "#DateFromVo" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });
       $( "#DateToVo" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });
    $(".select2").select2();


  });

</script>
      <script>
  $( function() {
    $( "#Tgl1BS" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });
    $( "#Tgl2BS" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });


  });
  </script>


<script type="text/javascript">
  $(function () {  
  $(".SemuaNasabah").click(function(){
      var TglAwal = $( "#TglAwalSemua" ).val();
      var TglAkhir = $( "#TglAkhirSemua" ).val();
      window.open("<?php echo base_url(). 'index.php/Tabungan/CetakSemuaNasabah/';?>?Status="+ 'Semua' +"&TglAwal="+ TglAwal+"&TglAkhir="+TglAkhir ,"MyTargetWindowName")
  }); 
});
</script>

<script type="text/javascript">
  $(function () {  
  $(".PerNasabah").click(function(){
      var TglAwal = $( "#TglAwalPer" ).val();
      var TglAkhir = $( "#TglAkhirPer" ).val();
       var Norek = $( "#Norek" ).val();
      window.open("<?php echo base_url(). 'index.php/Tabungan/CetakPerNasabah/';?>?Norek="+ Norek +"&TglAwal="+ TglAwal+"&TglAkhir="+TglAkhir ,"MyTargetWindowName")
  }); 
});
</script>
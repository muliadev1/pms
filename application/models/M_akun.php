<?php 

class M_akun extends CI_Model{

	function load(){	
		$data = $this->db->query("SELECT IdSub,NamaSub from tbsubklasifikasi where isaktif ='1'");
         return $data;
	}

    function loadAkun_byid($IdAkun){    
        $data = $this->db->query("SELECT * FROM tbakun WHERE NoAkun = '$IdAkun' ");
         return $data->row();
    }


    function loadIdSubklas($IdKlas){    
        $data = $this->db->query("select idsub from tbsubklasifikasi where left(idsub, 1)= '".$IdKlas."' ORDER BY idsub DESC LIMIT 1");
         
         return $data;
    }

    function loadAkun(){    
        $data = $this->db->query("SELECT  NoAkun , NamaSub, NamaAkun   FROM tbakun INNER JOIN tbsubklasifikasi ON tbakun.idsub  =  tbsubklasifikasi.idsub ");
         
         return $data;
    }

     function loadIdAkun($IdSub){    
        $data = $this->db->query("select NoAkun from tbakun where left(NoAkun, 2)= '".$IdSub."' ORDER BY NoAkun DESC LIMIT 1");
         
         return $data;
    }

    function loadTabunganByNorek($Norek){    
        $data = $this->db->query("SELECT Norek,NamaNasabah,DATE_FORMAT(tbtabungan.TanggalInput,'%d-%m-%Y') AS TanggalBuka , SaldoRendah, SaldoTinggi, Saldo FROM tbtabungan INNER JOIN tbnasabah ON tbtabungan.nonasabah =  tbnasabah.nonasabah where tbtabungan.Norek ='".$Norek."'");
         return $data;
    }

    function loadTabunganDetilByNorek($Norek){    
        $data = $this->db->query("SELECT DATE_FORMAT(TanggalInput,'%d-%m-%Y  %H:%i:%s') AS Tanggal , Debet, Kredit, SaldoAkhir FROM tbdetiltabungan where tbDetiltabungan.Norek ='".$Norek."'");
         return $data;
    }

    

    function addSubKlas($data,$tableSubklas){
         $this->db->trans_start();

        $this->db->insert($tableSubklas,$data);
        
         $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else{
            $this->db->trans_commit();
        }
    }

    function Akun_editDB($data,$id) {
             $this->db->where('NoAkun',$id );
             $this->db->update('tbAkun',$data);
  }

   


    
}

?>
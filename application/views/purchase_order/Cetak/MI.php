
<?php echo $header?>

<style type="text/css">
  #TabelKonten tr td {
    padding-right: 7px;
    padding-left:  7px;
    font-size: 12px;
  }
</style>

<div style="margin-buttom : 15px;" >
<table width="100%" border="0"  >
    <tr>
     <td colspan="2" align="center" style="font-size:14px;"> <strong> MEMORANDUM INVOICE  </strong>  </td>
    </tr>
    
   
  </table>
<table width="100%" border="0" style="font-size:11px;"  >
   <tr>
     <td>&nbsp;</td>
     <td align="left">&nbsp;</td>
      <td>&nbsp;</td>
     <td align="left">&nbsp;</td>
    </tr>
  <tr>
     <td width="10%"><strong>NO</strong> </td>
     <td align="left">: <?php echo $konten[0]->kodero;?></td>
     <td width="10%" ><strong>Vendor</strong></td>
     <td align="left">: <?php echo $konten[0]->vendor;?></td>
    </tr>
     <tr>
     <td width="10%"><strong>PO NO</strong> </td>
     <td align="left">: <?php echo $konten[0]->kodepo;?></td>
      <td width="10%"><strong>Date</strong></td>
     <td align="left">: <?php echo $konten[0]->datereceive;?></td>
    </tr>
    <tr>
     <td>&nbsp;</td>
     <td align="left">&nbsp;</td>
      <td>&nbsp;</td>
     <td align="left">&nbsp;</td>
    </tr>
  </table>
</div>

<table id="TabelKonten"  border="1" style="border-collapse: collapse; border-color:#000000; margin-bottom : 130px;"  width="100%"   >
    <thead  >
        <tr align="center" class="header">
            <th  width="15%" >Item Code</th>
            <th width="15%">Unit</th>
            <th width="40%">Description</th>
            <th width="10%">Unit Price</th>
            <th width="10%">Qty</th>
            <th width="10%">SubTotal</th>
        </tr>
    </thead>
    <tbody>
      <?php $total=0; foreach ($konten as $row) { ?>
    <tr>
      <td><?php echo $row->itemcode; ?></td>
      <td><?php echo $row->unit; ?></td>
      <td><?php echo $row->description; ?></td>
      <td align="right"><?php echo number_format($row->price, 0, '.', '.'); ?></td>
      <td align="right"><?php echo number_format($row->qty, 2, '.', '.'); ?></td>
      <td align="right"><?php echo number_format($row->subtotal, 0, '.', '.'); ?></td>
      <?php $total+=$row->subtotal;} ?>
    </tr>
    <tr style="background-color:#E6EBE6;">
      <td colspan="5" align="center"><strong>TOTAL</strong></td>
       <td align="right"><strong><?php echo number_format($total, 0, '.', '.'); ?></strong></td>
    </tr>
    </tbody>
    

  </table>



<table width="100%" border="0" style="font-size:11px; "  >
    <tr>
      <td >&nbsp; </td>
      <td >&nbsp; </td>     
      <td >&nbsp; </td>
    </tr>
     <tr>
      <td >&nbsp; </td>
      <td >&nbsp; </td>     
      <td >&nbsp; </td>
    </tr>
  <tr>
     <td ><strong>QUANTYTY AND QUALITY APPROVED</strong> </td>
     <td ><strong>PRICE APPROVED</strong></td>
     <td ><strong>RECEIVED</strong></td>
    </tr>
         <tr>
      <td >&nbsp; </td>     
      <td >&nbsp; </td>
      <td >&nbsp; </td>
    </tr>
     <tr>
      <td >&nbsp; </td>     
      <td >&nbsp; </td>
      <td >&nbsp; </td>
    </tr>
     <tr>
      <td >&nbsp; </td>     
      <td >&nbsp; </td>
      <td >&nbsp; </td>
    </tr>
     <tr>
      <td ><hr style="color:#000000"> </td>     
      <td ><hr style="color:#000000"> </td>
      <td ><hr style="color:#000000"> </td>
    </tr>
   
  </table>


  






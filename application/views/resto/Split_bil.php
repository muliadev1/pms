<style type="text/css">
  .bold{
    font-weight: bold;
     
},
.WarnaDisc{
    font-weight: bold;
   color: #333;
}
</style>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    <h3 class="modal-title" align="center" id="myModalLabel" style="font-weight:bold;"> <span class="label label-info">PROCCESS PAYMENT TABLE <?php echo $data[0]->tableid;?></span></h3>
  </div>
<form method="post" id="Simpan" action="<?php echo base_url(). 'index.php/Resto/add_payment'; ?>" enctype="multipart/form-data">
<section class="content" id="SplitBillKonten">
<input type="hidden" name="TrxId" id="TrxId" value="<?php echo $data[0]->trxid;?>">
<input type="hidden" name="tableidKirim" id="tableidKirim" value="<?php echo $data[0]->tableid;?>">  
<input type="hidden" name="idguestKirim" id="idguestKirim" value="<?php echo $data[0]->idguest;?>"> 
<input type="hidden" name="idoperatoropenorderKirim" id="idoperatoropenorderKirim" value="<?php echo $data[0]->idoperatoropenorder;?>">
  <div class="row"> <p></p>
    <div class="col-md-6">
      <div class="box box-info bg-blue" width="100%" style="overflow-y:hidden;" >
        <div class="box-body" >
          <div class="table-responsive" >
            <table id="exampleguest" class="table " >
                                <thead>
                                    <tr >
                                        <th >ID</th>
                                        <th >Menu</th>
                                        <th >Price</th>
                                        <th>Disc</th>
                                        <th>Price*</th>
                                        <th>Qty</th>
                                        <th>Subtotal</th>
                                        <th>Operator</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $TotalKiri=0; $DiscKiri=0;
                                    foreach($data as $u){
                                      $TotalKiri  +=$u->subtotal;
                                      $DiscKiri  +=$u->dism;
                                      $Total1 =$u->price *$u->qty-$u->dism ;
                                    ?>
                                    <tr  class='odd gradeX context'>
                                        <td class="id bold"><?php echo $u->iddetil ?></td>
                                        <td class="menu bold"><?php echo $u->qty?>X  <?php echo $u->menu?></td>
                                        <td class="price bold"><?php echo number_format($u->price, 0, '.', '.') ?></td>
                                        <td class="idsm bold"><?php echo number_format($u->dism, 0, '.', '.') ?></td>
                                        <td class="price1 bold"><?php echo number_format($Total1, 0, '.', '.')  ?></td>
                                        <td class="qty bold"> <?php echo $u->qty?></td>
                                        <td class="subtotal bold"><?php echo number_format($u->subtotal, 0, '.', '.') ?></td>
                                         <td class="namauser bold"><?php echo $u->nama_user?></td>
                                        <td align="left">
                                          <a class="btn btn-warning btn-xs PilihPay"  >  <span class="fa fa-fw fa-hand-o-right" ></span> </a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                                </table>
                                </div>
                            </div>
                        </div>
                      </div>
    <div class="col-md-6">
      <div class="box box-info bg-green">
        <div class="box-body" >
          <div class="table-responsive"   >
            <table id="SplitTable" class="table"  >
                                <thead>
                                    <tr >
                                        <th >ID</th>
                                        <th >Menu</th>
                                        <th >Price</th>
                                        <th>Disc</th>
                                        <th>Price*</th>
                                        <th>Qty</th>
                                        <th>Subtotal</th>
                                        <th>Operator</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="mytbody">
                                   
                                </tbody>
                                </table>
                                </div>
                            </div>
                        </div>
                      </div>
                  </div>
        <div class="row">
        <div class="col-sm-3" >
         <div class="form-group">
        <a class="btn btn-primary btn-lg" id="AddAllKeKanan"><i class="fa   fa-hand-o-right"></i> Add All &emsp; &emsp; &emsp;&nbsp; &nbsp;</a>
        </div>
         <div class="form-group">
        <a class="btn btn-success btn-lg " id="PaymentForm"><i class="fa  fa-check-square-o"></i> Payment Process</a>
        </div>
         <div class="form-group">
         <a class="btn btn-warning btn-lg " id="CetakBil1"><i class="fa fa-print"></i> Print &emsp; &emsp; &emsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
         </div>
         </div>
          <div class="col-lg-3 col-xs-6" style="margin-left:1px; margin-top:20px;">
            <!-- small box -->
            <div class="small-box bg-blue">
              <div class="inner">
                <table width="100%">
                  <tr> 
                    <td align="left">TOTAL</td>
                    <td align="right"><strong><input type="text"  value="<?php echo number_format($TotalKiri, 0, '.', '.');?>" name ="TotalKiri" id="TotalKiri" style="text-align: right;" class="form-control" readonly="readonly"  ></strong></td>
                  </tr>
                  <tr> 
                    <td align="left">DISC</td>
                    <td align="right"><strong><input type="text"  value="<?php echo number_format($DiscKiri, 0, '.', '.'); ?>" name ="DiscKiri" id="DiscKiri" style="text-align: right;" class="form-control" readonly="readonly"></strong></td>
                  </tr>
                  <tr> 
                    <td align="left">TOTAL*</td>
                    <td align="right"><strong><input type="text"  value="<?php echo  number_format($TotalKiri-$DiscKiri, 0, '.', '.');?>" name ="Total1Kiri" id="Total1Kiri" style="text-align: right;" class="form-control" readonly="readonly"></strong></td>
                  </tr>
                </table>
              </div>
              
              <a href="#" class="small-box-footer">Payment <i class="fa fa-money"></i></a>
            </div>
          </div>
        <div class="col-sm-3" style="position:fixed; margin-left:675px;" >
         <div class="form-group">
           <a class="btn btn-danger btn-lg" id="RemoveDariKanan"><i class="fa   fa-hand-o-left"></i> Remove All </a>
        </div>
        </div>
          <div class="col-lg-3 col-xs-6" style="float:right; margin-top:19px;">
            <!-- small box -->
            <div class="small-box bg-green">
              <div class="inner">
                 <table width="100%">
                  <tr> 
                    <td align="left">TOTAL</td> 
                    <td align="right"><strong><input type="text"   value="0"  name ="TotalKanan" id="TotalKanan" style="text-align: right;" class="form-control" readonly="readonly"></strong></td>
                  </tr>
                  <tr> 
                    <td align="left">DISC</td>
                    <td align="right"><strong><input type="text"  value="0" name ="DiscKanan" id="DiscKanan" style="text-align: right;" class="form-control" readonly="readonly"></strong></td>
                  </tr>
                  <tr> 
                    <td align="left">TOTAL*</td>
                    <td align="right"><strong><input type="text"  value="0" name ="Total1Kanan" id="Total1Kanan" style="text-align: right;" class="form-control" readonly="readonly"></strong></td>
                  </tr>
                </table>
              </div> 
              <a href="#" class="small-box-footer">Payment <i class="fa fa-money"></i></a>
            </div>
          </div>
        </div>
       </section>
       <?php $this->load->view('resto/Payment_add'); ?>
    </form>


<script type="text/javascript">
    $("#CetakBil1").click(function(e){
       var tabeltemp= $("#SplitTable").DataTable();
        var datadetils = tabeltemp.rows().data().toArray();
       var  TrxId=$("#TrxId").val();
      $.ajax({
        type: "POST",
       url: '<?php echo site_url('Resto/Add_tempbill1');?>',
       cache: false,
        data: {datadetil:datadetils,idtrx:TrxId},
        success: function(data){ 
         window.open("<?php echo base_url(). 'index.php/Resto/Cetak_Bill1/';?>?xxxporn="+'1' ,"MyTargetWindowName")
        }
      });
  });
</script>

  
 <script type="text/javascript">
    $("#PaymentForm").click(function(e){
      var Total = $('#TotalKanan').val();
      var Disc =  $('#DiscKanan').val();
      var AfterDiscount = $('#Total1Kanan').val();

      event.preventDefault();
       $.ajax({
        type: "POST",
       url: '<?php echo site_url('Resto/Get_Setting_resto');?>',
       dataType:'json',
       cache: false,
        data: {Total:Total,Disc:Disc, AfterDiscount  },
        success: function(data){ 
        var totalhendel = $('#TotalKanan').val().replace(/\./g, "");
        if (totalhendel==0) {InvalidTransaction();return false};

       
        $('#Total').val(data['TotalNumber']);
        $('#Total1').val(data['Total']);

        $('#Discount').val(data['Disc']);
        $('#Discount1').val(data['DiscNumber']);

        $('#Service').val(data['ServiceNumber']);
        $('#Service1').val(data['Service']);

        $('#Tax').val(data['TaxNumber']);
        $('#Tax1').val(data['Tax']);

        $('#AfterDiscount').val(data['AfterDiscountNumber']);
        $('#AfterDiscount1').val(data['AfterDiscount']);

        $('#GrandTotal').val(data['GrandtotalNumber']);
        $('#GrandTotal1').val(data['Grandtotal'] );

        $('#SplitBillKonten').fadeOut(0,300);
        $('#PaymentKonten').fadeIn(0,300);
        $('#Service').focus();
        }
      });

       
    });
</script>

<script type="text/javascript">
     $("#BackToSplit").click(function(e){
        $('#Total').val('0');
        $('#Total1').val('0');
        $('#Discount').val('0');
        $('#Discount1').val('0');    
        $('#Service').val('0');
        $('#Service1').val('0');
        $('#Tax').val('0');
        $('#Tax1').val('0');
        $('#GrandTotal').val('0');
        $('#GrandTotal1').val('0');
        $('#PayAmount').val('0');
        $('#PayAmount1').val('0');
        $('#Remain').val('0');
        $('#Remain1').val('0');

        $('#AfterDiscount').val('0');
        $('#AfterDiscount1').val('0');
        $('#SplitBillKonten').fadeIn(0,300);
        $('#PaymentKonten').fadeOut(0,300);

        var tbpay = $('#paymenttabel').DataTable();
    var GrandTotal1=$('#GrandTotal1').val();
   
    var infotbpay =tbpay.page.info();

        if (infotbpay.end>0) {
         for (var x = infotbpay.start; x < infotbpay.end; x++) {
           var idpay= tbpay.row(x).data()[0]; 
       $('.'+idpay+'Pay').remove();
         }
        };
        tbpay.clear().draw();
   

    });
</script>
 
<script type="text/javascript">
     $("#DiscInputOK").click(function(e){
       var tabelkanan = $('#SplitTable').DataTable();
       var tabelkanan1 = $('#SplitTable').dataTable();
       var myDisc = $('#myDisc');
       var qtyambil = $('#QtyDisc').val().replace(/\./g, "");
       var priceambilhitung = $('#PriceDisc').val().replace(/\./g, "");
       var priceambil = $('#PriceDisc').val();
       var discsebelumnya = $('#DiscInputBayangan').val().replace(/\./g, "");

       var  nominal=  $('#DiscInput').val();
       var  nominalhitung=  $('#DiscInput').val().replace(/\./g, "");

       var pricehitung  = parseFloat(priceambilhitung);//+parseFloat(discsebelumnya);
       var price  = pricehitung.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");

       var price1hitung = parseFloat(pricehitung)-parseFloat(nominalhitung);
       var price1  = price1hitung.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");

       var  nominalsebelumnya=  $('#DiscInputBayangan').val(); 
       var  subtotalama=  $('#SubtotalDiscModal').val().replace(/\./g, "");
       var subtotabaruhitung  =parseFloat(price1hitung)*parseFloat(qtyambil);
 
       var subtotabaru = subtotabaruhitung.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");


       var idx  =  $('#IdMenuModalDisc').val();
       var idmenu  =  $('#IdMenuDiscModal').val();
       //subtotal idsm
       tabelkanan1.fnUpdate(price1,idx , 4 );
      tabelkanan1.fnUpdate(price,idx , 2 );
      tabelkanan1.fnUpdate(nominal,idx , 3 );
      tabelkanan1.fnUpdate(subtotabaru,idx , 6 );
      tabelkanan.row(idx).nodes().to$().css('color', 'yellow');
      var namasubtotal  = 'subtotal'+'['+idmenu+']';
      var namaidsm  = 'idsm'+'['+idmenu+']';
      var namaprice  = 'price'+'['+idmenu+']';
      var namaprice1  = 'price1'+'['+idmenu+']';

      $('input[name="'+namasubtotal+'"]').val(subtotabaruhitung);
      $('input[name="'+namaidsm+'"]').val(nominal.replace(/\./g, ""));
      $('input[name="'+namaprice+'"]').val(pricehitung);
       $('input[name="'+namaprice1+'"]').val(price1hitung);

      HitungTotalKanan();
      $('#MenuModal').text('');
      $('#IdMenuModalDisc').val('');
      $('#DiscInput').val('');
       myDisc.modal('hide');
    
    });

</script>

<script type="text/javascript">
     $('#SplitTable tbody').on('click', '.Disc', function(event){
      event.preventDefault();
      var tabelkanan = $('#SplitTable').DataTable();
      var tabelkanan1 = $('#SplitTable').dataTable();
      var myDisc = $('#myDisc');

      var idmenu = tabelkanan.row($(this).closest('tr')).data()[0];
      var menu = tabelkanan.row($(this).closest('tr')).data()[1];
      var info =tabelkanan.page.info();
      var idx = tabelkanan.row( $(this).closest('tr') ).index();
      var nominalsebelumnya =  tabelkanan.row($(this).closest('tr')).data()[3];
      var subtotalama =  tabelkanan.row($(this).closest('tr')).data()[6];
      var qty =  tabelkanan.row($(this).closest('tr')).data()[5];
      var pricedisc =  tabelkanan.row($(this).closest('tr')).data()[2];
      $('#myDisc').modal({backdrop: 'static', keyboard: false}) ;
      myDisc.modal('show');
      $('#MenuModal').text(menu);
      $('#DiscInputBayangan').val(nominalsebelumnya);
      $('#DiscInput').val(nominalsebelumnya);
      $('#IdMenuModalDisc').val(idx);
      $('#SubtotalDiscModal').val(subtotalama);
      $('#IdMenuDiscModal').val(idmenu); 
      $('#QtyDisc').val(qty); 
      $('#PriceDisc').val(pricedisc);
    
      return false;
    });

</script>
<script type="text/javascript">
    $("#AddAllKeKanan").click(function(e){
      var tabelkiri = $('#exampleguest').DataTable();
      var tabelkanan = $('#SplitTable').DataTable();
      var info =tabelkiri.page.info();
       if ( info.end==0) {File_Kosong();return false};

  for (var i = info.start; i < info.end; i++) {
   id = tabelkiri.row(i).data()[0]; 
   menu = tabelkiri.row(i).data()[1];
   price = tabelkiri.row(i).data()[2];
   idsm = tabelkiri.row(i).data()[3];
   price1 =  tabelkiri.row(i).data()[4];
   qty = tabelkiri.row(i).data()[5]; 
   subtotal = tabelkiri.row(i).data()[6];
   namauser = tabelkiri.row(i).data()[7];
   var btnedit =  " <a class='btn btn-warning btn-xs PilihEditKanan'  >  <span class='fa fa-fw fa-hand-o-left' ></span> </a>  <a class='btn btn-danger btn-xs Disc'  >  <span class='fa fa-fw  fa-minus-square' ></span> </a>";
   
   var row = tabelkanan.row.add( [
            id,
            menu,
            price,
            idsm,
            price1,
            qty,
            subtotal,
            namauser,
            btnedit
        ] ).draw(false);
        tabelkanan.row(row).column(0).nodes().to$().addClass('No bold');
        tabelkanan.row(row).column(1).nodes().to$().addClass('ItemCode bold');
        tabelkanan.row(row).column(2).nodes().to$().addClass('Description bold');
        tabelkanan.row(row).column(3).nodes().to$().addClass('Qty bold');
        tabelkanan.row(row).column(4).nodes().to$().addClass('Unit bold');
        tabelkanan.row(row).column(5).nodes().to$().addClass('Price bold');
        tabelkanan.row(row).column(6).nodes().to$().addClass('Subtotal bold');
        tabelkanan.row(row).column(7).nodes().to$().addClass('Note bold');
        tabelkanan.row(row).column(8).nodes().to$().addClass('PilihEditKanan');
        GenetateElement(id,menu,price,idsm,price1,qty,subtotal,namauser);
       
       
     }
      var rows = tabelkiri
    .rows()
    .remove()
    .draw();
     HitungTotalKanan();
     HitungTotalKiri();
  });
</script>

<script type="text/javascript">
    $("#RemoveDariKanan").click(function(e){
      var tabelkiri = $('#exampleguest').DataTable();
      var tabelkanan = $('#SplitTable').DataTable();
      var idsmhitung ='0'; 
      var info =tabelkanan.page.info();
     if ( info.end==0) {File_Kosong();return false};

  for (var i = info.start; i < info.end; i++) {
   id = tabelkanan.row(i).data()[0]; 
   menu = tabelkanan.row(i).data()[1];
   price = tabelkanan.row(i).data()[2];
   idsm = tabelkanan.row(i).data()[3];
   price1 =  tabelkanan.row(i).data()[4];
   qty = tabelkanan.row(i).data()[5]; 
   subtotal = tabelkanan.row(i).data()[6];
   namauser = tabelkanan.row(i).data()[7];
   var btnedit =  " <a class='btn btn-warning btn-xs PilihPay'  >  <span class='fa fa-fw fa-hand-o-right' ></span> </a>";
   if (idsm>0) {
        price = parseFloat(price.replace(/\./g, "")) ;
         price1 = parseFloat(price); 
         subtotal = parseFloat(price1) * parseFloat(qty); 
         price = price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
         price1 = price1.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
         subtotal = subtotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
         idsm = '0';
        };
   var row = tabelkiri.row.add( [
            id,
            menu,
            price,
            idsm,
            price1,
            qty,
            subtotal,
            namauser,
            btnedit
        ] ).draw(false);
        tabelkiri.row(row).column(0).nodes().to$().addClass('No bold');
        tabelkiri.row(row).column(1).nodes().to$().addClass('ItemCode bold');
        tabelkiri.row(row).column(2).nodes().to$().addClass('Description bold');
        tabelkiri.row(row).column(3).nodes().to$().addClass('Qty bold');
        tabelkiri.row(row).column(4).nodes().to$().addClass('Unit bold');
        tabelkiri.row(row).column(5).nodes().to$().addClass('Price bold');
        tabelkiri.row(row).column(6).nodes().to$().addClass('Subtotal bold');
        tabelkiri.row(row).column(7).nodes().to$().addClass('Note bold');
        tabelkiri.row(row).column(8).nodes().to$().addClass('PilihEditKanan');
        $('.'+id).remove();
       subtotal ='0';idsmhitung='0';
     }
      var rows = tabelkanan
    .rows()
    .remove()
    .draw();
     HitungTotalKanan();
     HitungTotalKiri();
  });
</script>







<script type="text/javascript">
$(function () {
   $('#exampleguest tbody').on('click', '.PilihPay', function(e){
     var tabelkiri = $('#exampleguest').DataTable();
     var tabelkanan = $('#SplitTable').DataTable();

   id = tabelkiri.row($(this).closest('tr')).data()[0]; 
   menu = tabelkiri.row($(this).closest('tr')).data()[1];
   price = tabelkiri.row($(this).closest('tr')).data()[2];
   idsm = tabelkiri.row($(this).closest('tr')).data()[3];
   price1 =  tabelkiri.row($(this).closest('tr')).data()[4];
   qty = tabelkiri.row($(this).closest('tr')).data()[5]; 
   subtotal = tabelkiri.row($(this).closest('tr')).data()[6];
   namauser = tabelkiri.row($(this).closest('tr')).data()[7];
   var btnedit =  " <a class='btn btn-warning btn-xs PilihEditKanan'  ><span class='fa fa-fw fa-hand-o-left' ></span></a>  <a class='btn btn-danger btn-xs Disc'>  <span class='fa fa-fw  fa-minus-square' ></span> </a>";
   var row = tabelkanan.row.add( [
            id,
            menu,
            price,
            idsm,
            price1,
            qty,
            subtotal,
            namauser,
            btnedit
        ] ).draw(false);
        tabelkanan.row(row).column(0).nodes().to$().addClass('No bold');
        tabelkanan.row(row).column(1).nodes().to$().addClass('ItemCode bold');
        tabelkanan.row(row).column(2).nodes().to$().addClass('Description bold');
        tabelkanan.row(row).column(3).nodes().to$().addClass('Qty bold');
        tabelkanan.row(row).column(4).nodes().to$().addClass('Unit bold');
        tabelkanan.row(row).column(5).nodes().to$().addClass('Price bold');
        tabelkanan.row(row).column(6).nodes().to$().addClass('Subtotal bold');
        tabelkanan.row(row).column(7).nodes().to$().addClass('Note bold');
        tabelkanan.row(row).column(8).nodes().to$().addClass('PilihEditKanan bold');
        GenetateElement(id,menu,price,idsm,price1,qty,subtotal,namauser);
        tabelkiri.row($(this).closest('tr')).remove().draw(false);
         HitungTotalKanan();
        HitungTotalKiri();

  });
});
</script>


<script type="text/javascript">
$(function () {
   $('#SplitTable tbody').on('click', '.PilihEditKanan', function(e){

     var tabelkiri = $('#exampleguest').DataTable();
     var tabelkanan = $('#SplitTable' ).DataTable();
     var idsmhitung = '0';
  
   id = tabelkanan.row($(this).closest('tr')).data()[0]; 
   menu = tabelkanan.row($(this).closest('tr')).data()[1];
   price = tabelkanan.row($(this).closest('tr')).data()[2];
   idsm = tabelkanan.row($(this).closest('tr')).data()[3];
   price1 =  tabelkanan.row($(this).closest('tr')).data()[4];
   qty = tabelkanan.row($(this).closest('tr')).data()[5]; 
   subtotal = tabelkanan.row($(this).closest('tr')).data()[6];
   namauser = tabelkanan.row($(this).closest('tr')).data()[7];
   var btnedit =  " <a class='btn btn-warning btn-xs PilihPay'  >  <span class='fa fa-fw fa-hand-o-right' ></span> </a>";
   if (idsm>0) {
         price = parseFloat(price.replace(/\./g, "")) ;
         price1 = parseFloat(price); 
         subtotal = parseFloat(price1) * parseFloat(qty); 
         price = price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
         price1 = price1.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
         subtotal = subtotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
         idsm = '0';
        };
   var row = tabelkiri.row.add([
            id,
            menu,
            price,
            idsm,
            price1,
            qty,
            subtotal,
            namauser,
            btnedit
        ]).draw(false);
        tabelkiri.row(row).column(0).nodes().to$().addClass('No bold');
        tabelkiri.row(row).column(1).nodes().to$().addClass('ItemCode bold');
        tabelkiri.row(row).column(2).nodes().to$().addClass('Description bold');
        tabelkiri.row(row).column(3).nodes().to$().addClass('Qty bold');
        tabelkiri.row(row).column(4).nodes().to$().addClass('Unit bold');
        tabelkiri.row(row).column(5).nodes().to$().addClass('Price bold');
        tabelkiri.row(row).column(6).nodes().to$().addClass('Subtotal bold');
        tabelkiri.row(row).column(7).nodes().to$().addClass('Note bold');
        tabelkiri.row(row).column(8).nodes().to$().addClass('PilihEditKanan bold');
         $('.'+id).remove();
       tabelkanan.row($(this).closest('tr')).remove().draw(false);
        HitungTotalKanan();
        HitungTotalKiri();
       
  });
});
</script>
<script type="text/javascript">
  function GenetateElement(id,menu,price,idsm,price1,qty,subtotal,namauser) {
 $('<input>').attr({
        type: 'hidden',
        id: id+'idDetil',
        class: id,
        name: 'idDetil['+id+']', 
        value: id
        }).appendTo('p');
 $('<input>').attr({
        type: 'hidden',
        id: id+'menu',
        class: id,
        name: 'menu['+id+']', 
        value: menu
        }).appendTo('p');
 $('<input>').attr({
        type: 'hidden',
        id: id+'price',
        class: id,
        name: 'price['+id+']', 
        value: price.replace(/\./g, "")   
        }).appendTo('p');
  $('<input>').attr({
        type: 'hidden',
        id: id+'idsm',
        class: id,
        name: 'idsm['+id+']', 
        value: idsm.replace(/\./g, "")
        }).appendTo('p');
  $('<input>').attr({
        type: 'hidden',
        id: id+'price1',
        class: id,
        name: 'price1['+id+']', 
        value: price1.replace(/\./g, "")
        }).appendTo('p');
    $('<input>').attr({
        type: 'hidden',
        id: id+'qty',
        class: id,
        name: 'qty['+id+']', 
        value: qty
        }).appendTo('p');
    $('<input>').attr({
        type: 'hidden',
        id: id+'subtotal',
        class: id,
        name: 'subtotal['+id+']', 
        value: subtotal.replace(/\./g, "")
        }).appendTo('p');
  }
</script>

<script type="text/javascript">
  function HitungTotalKanan() {
    var tabelkanan = $('#SplitTable').DataTable();
    var infokanan =tabelkanan.page.info();
    var DiscLastKanan = 0 ;
    var TotalLastKanan=0 ;
        if (infokanan.end>0) {
         for (var i = infokanan.start; i < infokanan.end; i++) {
           DiscLastKanan+= parseFloat(tabelkanan.row(i).data()[3].replace(/\./g, ""))*parseFloat(tabelkanan.row(i).data()[5].replace(/\./g, ""));
           TotalLastKanan+= parseFloat(tabelkanan.row(i).data()[2].replace(/\./g, ""))  ;
           // TotalLastKanan= parseFloat(TotalLastKanan)*parseFloat(tabelkanan.row(i).data()[5].replace(/\./g, ""));
           // alert(tabelkanan.row(i).data()[5]);
         }
        };
       // alert(DiscLastKanan);
    var Total1LastKanan = parseFloat(TotalLastKanan)-parseFloat(DiscLastKanan);
    //var TotalLastKanan = parseFloat(TotalLastKanan)+parseFloat(DiscLastKanan);
    $('#TotalKanan').val('');
    $('#TotalKanan').val(TotalLastKanan.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."),);
    $('#DiscKanan').val('');
    $('#DiscKanan').val(DiscLastKanan.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."),);
    $('#Total1Kanan').val('');
    $('#Total1Kanan').val(Total1LastKanan.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."),);
    
  }
</script>



<script type="text/javascript">
  function HitungTotalKiri() {
    var tabelkiri = $('#exampleguest').DataTable();
    var infokiri =tabelkiri.page.info();

    var TotalLastKiri = 0 ;
    var DiscLastKiri=0 ;
        if (infokiri.end>0) {
         for (var i = infokiri.start; i < infokiri.end; i++) {
           DiscLastKiri+= parseFloat(tabelkiri.row(i).data()[3].replace(/\./g, ""));
           TotalLastKiri+= parseFloat(tabelkiri.row(i).data()[6].replace(/\./g, ""));
         }
        };
    var Total1LastKiri = parseFloat(TotalLastKiri)-parseFloat(DiscLastKiri);
    
    $('#TotalKiri').val('');
    $('#TotalKiri').val(TotalLastKiri.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."),);
    $('#DiscKiri').val('');
    $('#DiscKiri').val(DiscLastKiri.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."),);
    $('#Total1Kiri').val('');
    $('#Total1Kiri').val(Total1LastKiri.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."),);
    
  }
</script>












<script type="text/javascript">
  $(function () {
  $('#exampleguest').DataTable({
      "ordering" : false,
      "searching" : false,
      "lengthChange" : false,
      "paging" : false,
      "info" : false,
      "iDisplayLength" : "100",
      // "scrollY": "250px",
      // scrollCollapse: true
      "scrollY": 300,
       // "scrollX": true
    });
   
    dtTable= $("#exampleguest").DataTable();
    dtTable.columns([0,5]).visible(false);
  });
  </script>

<script type="text/javascript">
$(function () {
  $('#SplitTable').DataTable({
      "ordering" : false,
      "searching" : false,
      "lengthChange" : false,
      "paging" : false,
      "info" : false,
      "iDisplayLength" : "100",
      // "scrollY": "250px",
      // scrollCollapse: true
       "scrollY": 300,
       // "scrollX": true
    });

    dtTable= $("#SplitTable").DataTable();
    dtTable.columns([0,5]).visible(false);
  });
  </script>

<script type="text/javascript">
  function File_Kosong() {
  $.alert({
    title: 'Caution!!',
    content: 'No Item Selected!',
    icon: 'fa fa-warning',
    type: 'orange',
});
}
</script>
<script type="text/javascript">
  function InvalidTransaction() {
  $.alert({
    title: 'Caution!!',
    content: 'Invalid Transaction!',
    icon: 'fa fa-warning',
    type: 'orange',
});
}
</script>

  <script type="text/javascript">
   $("#CloseDisc").click(function(e){
     var myDisc = $('#myDisc');
      myDisc.modal('hide');
    });

</script>
<?php 

class Store_requisition extends CI_Controller{
	private $IdOperator;
	private $IdAset;
	private $IdDep;

	function __construct(){
		parent::__construct();
		if($this->session->userdata('Kode_userPms')==NULL) {
           redirect('Login');
		}
		$this->load->library(array('session','create_pdf','ciqrcode','akuntan_function'));
		$this->load->model(array('M_purchase_requisition','M_store_requisition','M_rbac','M_purchase_order','M_aset'));
		$this->IdOperator = $this->session->userdata('Kode_userPms');
		$this->IdAset = $this->session->userdata('KodeKategoriAset');
		$this->IdDep = $this->session->userdata('id_DepartementPms');
		
		if (!empty($this->session->userdata("active_db"))) {
			$this->load->database($this->session->userdata("active_db"), FALSE, TRUE);
		}
	}


	function index(){
		$this->load->view('dashboard1');
	}

	function store_requisition_add(){
		$data['data'] = $this->M_store_requisition->item_view();
		$data['dataDepaetemen'] = $this->M_store_requisition->load_departement();
		$data['sub'] = $this->M_store_requisition->load_sub();
		$this->load->view('store_requisition/Store_requisition',$data);
	}

	function select_sub_dep(){
		$iddep = $this->input->post('iddep');
		$itemcode = $this->input->post('ItemCode');
		$SubDep = $this->M_store_requisition->load_subDep($iddep,$itemcode);
		echo json_encode($SubDep);
		
	}

	function store_requisition_view(){
		$data['data'] = $this->M_purchase_requisition->pr_view();
		$this->load->view('purchase_requisition/Purchase_requisition_list',$data);
	}
	function store_requisition_view_detail(){
		$id = $this->input->post('id');
		$data['data'] = $this->M_purchase_requisition->pr_view_detil($id);
		$this->load->view('purchase_requisition/Purchase_requisition_detail',$data);
	}

	function daftar_item(){
		$data['data'] = $this->M_store_requisition->item_view();
		$this->load->view('store_requisition/Daftar_item',$data);
	}

	function add_sr(){
		$itemcode = $this->input->post('itemcode');
		//print_r($itemcode);exit();
		$Qty = $this->input->post('Qty');
		$Description = $this->input->post('Description');
		$Note = $this->input->post('Note');
		$Unit = $this->input->post('Unit');
		$IdDepartement = $this->input->post('IdDepartement'); 
		$NamaDepartemen = $this->input->post('NamaDepartemen');
		$Price = $this->input->post('Price');
		$Stok = $this->input->post('Stok');
		$kode =$this->M_store_requisition->get_kodesr('tbsr');
		$NotePr = $this->input->post('NotePr');
		$kodesubcategory = $this->input->post('subcategorydep');

		$itemcode1 = array_combine(range(1, count($itemcode)), array_values($itemcode));
		$Price1 = array_combine(range(1, count($Price)), array_values($Price));
		$Qty1 = array_combine(range(1, count($Qty)), array_values($Qty));
		$Description1 = array_combine(range(1, count($Description)), array_values($Description));
		$Unit1 = array_combine(range(1, count($Unit)), array_values($Unit));
		$Stok1 = array_combine(range(1, count($Stok)), array_values($Stok));
		$Note1 = array_combine(range(1, count($Note)), array_values($Note));


			$data = array('kodesr' => $kode,
	 				 		'iddepartement' => $IdDepartement,
	 				 		'departement' => $NamaDepartemen,
	 						'dateadd' => date('Y-m-d H:i:s'),
	 						'idoperator' => $this->IdOperator,
	 						'description' => $NotePr	
	 		 			);



		
				$Jumlah = count($Stok);
				for ($i=1; $i <= $Jumlah ; $i++) { 
					$qty = str_replace('.','', $Qty1[$i]); 
					$qtyfix = str_replace(',', '.', $qty);
				 	 $dataDetil[] = array('kodesr' => $kode,
						 				 'iddepartement' => $IdDepartement,
						 				 'departement' => $NamaDepartemen,
						 				'codeitem' => $itemcode1[$i],
						 				'price' => $Price1[$i],
						 				'qtyorder' => $qtyfix,
						 				'description' => $Description1[$i],
						 				'unit' => $Unit1[$i],
						 				'note' => $Note1[$i],
						 				'currstok' => $Stok1[$i],
						 				'idoperator' => $this->IdOperator
						 		 );
				 	 $this->M_store_requisition->set_SubDep($itemcode1[$i],$kodesubcategory[$i],$IdDepartement);
				 } 

				 	//Aset
		 		for ($i=1; $i <= $Jumlah ; $i++) { 
		 			$isaset = $this->M_purchase_order->Load_subcategory_byiditem($itemcode1[$i]); 
		 			if ($isaset==1){
		 				$codeaset = $this->M_purchase_order->Load_aset_byiditem($itemcode1[$i]); 
		 				$qty = str_replace('.', '', $Qty1[$i]); 
						$qtyfix = str_replace(',', '.', $qty);
						
		 				 $dataDetilaset[] = array('codeaset' => $codeaset,
						 				 'iddepartement' => $IdDepartement,
						 				 'departement' => $NamaDepartemen,
						 				 'kondisiawal' => 'Baik',
						 				 'kondisiakhir' => 'Baik',
						 				 'qty' => $qtyfix,
						 				 'note' => 'IN',
						 				 'inputdate' => date('Y-m-d H:i:s'),
						 				 'idoperator' => $this->IdOperator,
						 				 'operator' => $this->Operator
						 		 );
		 				
		 			}
				 	
				 }

				 //jika dia drop ke departement
				for ($i=1; $i <= $Jumlah ; $i++) { 
					$dataItem = $this->M_store_requisition->load_akunitem_bycode($itemcode1[$i],$IdDepartement);
					if (!empty($dataItem)) {
						
						foreach ($dataItem as $row) {
							$dataAkunDrop =  array('NamaTransaksi' => 'SR'.' '.$kode,
											 		'AkunDebet' => $row->akun,
											 		'AkunKredit' => '12001'
													);	
							$dataJurnalDrop = $this->akuntan_function->generate_jurnal('',$invoice1[$i],$row->price, 
							$this->IdOperator,$dataAkunDrop);

							$dataMasterJurnaldrop = $dataJurnalDrop[0];
							$dataDetilJurnaldrop=  array($dataJurnalDrop[1],$dataJurnalDrop[2]);
							
							$this->M_store_requisition->Payment_add_manual_dropdepartement('tbmasterjurnal',$dataMasterJurnaldrop,
							'tbdetiljurnal',$dataDetilJurnaldrop);
							
					}		
				}			
			}
			//print_r($dataDetilaset);exit();

		$this->M_store_requisition->add_transfer_stok_manual($data,$dataDetil,$IdDepartement);
		if (!empty($dataDetilaset)) {
			$this->M_aset->Add_detilAset_byorder('tbasetdetil',$dataDetilaset,$IdDepartement); 
		} 
		$this->session->set_flashdata('msg', 'Store Requisition Add successfully... ');
		$this->session->set_flashdata('kodesr', $kode);
		redirect('store_requisition/store_requisition_add');
	}


	function daftar_sr(){
		$data['data'] = $this->M_store_requisition->Load_SR_all();
		$this->load->view('store_requisition/Cetak/Daftar_sr',$data);
	}


	  function load_item_bysubDep(){
    $iddepartement = $this->input->post('iddepartement');
    $kodesubcategory = $this->input->post('kodesubcategory');
  
    $sub =  $this->M_store_requisition->load_item_bysubDep($kodesubcategory, $iddepartement);
   //print_r($sub);exit();
    echo ' <div class="form-group" id="groupsrItem">';
    echo ' <label>Item</label> ';
    echo  '<select id="SrItem" class="form-control select2"  style="width: 100%;" name="SrItem"> ';
    echo ' <option value="" >--Select Item--</option>';
    foreach ($sub as $pek ){
                    echo ' <option value="'.$pek->itemcode.'" >'.$pek->description.'</option>';
    }
    echo '</select>';
    echo '</div>';

  }

	function Cetak($JenisLaporan,$Text){
		$data['hotel'] = $this->M_rbac->getPerusahaan();
		$data['qr'] = $this->ciqrcode->generateQRcode($JenisLaporan, $Text,1.9);
		$data1= $this->load->view('template/Cetak_head',$data, TRUE);
		return $data1;
	}

	function CetakSR(){
		$KodeSR = $this->input->get('KodeSR', TRUE);
	    $data['header'] = $this->Cetak("Payment_voucher",  $this->session->userdata('Nama_userPms').'-'.$data['kontenSr'][0]->kodesr.'-'. date("d-m-Y_H-i-s"), 1);
	   $data['kontenSr'] = $this->M_store_requisition->Load_SR_bycode($KodeSR);
	    $html=$this->load->view('store_requisition/Cetak/SR',$data, TRUE);
     	$this->create_pdf->load($html,'store_requisition'.'-'.$data['kontenSr'][0]->kodesr, 'A4-P');	
	}

	function CetakSR_tgl(){
		date_default_timezone_set('Asia/Hong_Kong');
		$idsub = $this->input->get('idsub', TRUE);
		$sub = $this->input->get('sub', TRUE);
		$departement = $this->input->get('dep', TRUE);
		$iddepartement = $this->input->get('iddep', TRUE);

		$tgl = $this->input->get('Tgl', TRUE);
		$tgl1 = DateTime::createFromFormat('m/d/Y',$tgl);
		$tgl2 = $tgl1->format("Y-m-d");
		$tgl3 = $tgl1->format("d-m-Y");//print_r($tgl2);exit();

		$tgl1 = $this->input->get('Tgl1', TRUE);
		$tgl11 = DateTime::createFromFormat('m/d/Y',$tgl1);
		$tgl21 = $tgl11->format("Y-m-d");
		$tgl31 = $tgl11->format("d-m-Y");


	    $data['header'] = $this->Cetak("Stok_opname",  $this->session->userdata('Nama_userPms'). date("d-m-Y_H-i-s"), 1);
	    $data['head'] =  array('NamaDep' =>$departement,'NamaSub' =>$sub,'Tanggal' =>$tgl3,'Tanggal1' =>$tgl31);
	    $data['kontenSr'] = $this->M_store_requisition->load_sr_tgl($tgl2,$tgl21,$idsub,$iddepartement); 
	   // print_r( $data['kontenSO']);exit();
	    $html=$this->load->view('store_requisition/Cetak/SR_tgl',$data, TRUE);
     	$this->create_pdf->load($html,'store_requisition'.'-'.$data['kontenSO'][0]->dateadd, 'A4-P');	
	}


	function CetakSR_tgl_item(){
		//print_r('sukses');exit();
		date_default_timezone_set('Asia/Hong_Kong');
		$idsub = $this->input->get('idsub', TRUE);
		$sub = $this->input->get('sub', TRUE);
		$departement = $this->input->get('dep', TRUE);
		$iddepartement = $this->input->get('iddep', TRUE);
		$iditem = $this->input->get('iditem', TRUE);

		$tgl = $this->input->get('Tgl', TRUE);
		$tgl1 = DateTime::createFromFormat('m/d/Y',$tgl);
		$tgl2 = $tgl1->format("Y-m-d");
		$tgl3 = $tgl1->format("d-m-Y");//print_r($tgl2);exit();

		$tgl1 = $this->input->get('Tgl1', TRUE);
		$tgl11 = DateTime::createFromFormat('m/d/Y',$tgl1);
		$tgl21 = $tgl11->format("Y-m-d");
		$tgl31 = $tgl11->format("d-m-Y");


	    $data['header'] = $this->Cetak("SR",  $this->session->userdata('Nama_userPms'). date("d-m-Y_H-i-s"), 1);
	    $data['head'] =  array('NamaDep' =>$departement,'NamaSub' =>$sub,'Tanggal' =>$tgl3,'Tanggal1' =>$tgl31);
	    $data['kontenSr'] = $this->M_store_requisition->load_sr_tgl_item($tgl2,$tgl21,$idsub,$iddepartement,$iditem); 
	   // print_r( $data['kontenSO']);exit();
	    $html=$this->load->view('store_requisition/Cetak/SR_tgl_item',$data, TRUE);
     	$this->create_pdf->load($html,'store_requisition'.'-'.$data['kontenSr'][0]->dateadd, 'A4-P');	
	}
	


}
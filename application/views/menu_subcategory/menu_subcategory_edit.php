<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>

<!-- Content Header (Page header) -->
<section class="content-header">
   <div class="btn-group btn-breadcrumb">
            <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
             <a href="<?php echo base_url('index.php/Menu_subcategory/MenuSubcategory_view');?>" class="btn btn-default  btn-xs">Menu Sub Category</a>
            <a  class="btn btn-default  btn-xs active">Edit Menu Sub Category</a>
        </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
             <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Edit Menu Sub Category</h3>
                </div>
                <div class="box-body">
                    <form method="post" id="Simpan" action="<?php echo base_url().'index.php/Menu_subcategory/MenuSubcategory_editDB'; ?>">
                    <div class="form-group">
                      <label>Category</label>
                      <select class="form-control select2" style="width: 100%;" name="idcategory">
                      <?php
                          foreach($data1 as $u){
                            if ($data2[0]->idcategory == $u->id) {
                      ?>
                              <option selected="selected"  value="<?php echo $u->id; ?>" ><?php echo $u->category; ?></option>
                      <?php
                            }else{
                      ?>
                              <option  value="<?php echo $u->id; ?>" ><?php echo $u->category; ?></option>
                      <?php
                            }
                      ?>
                        <?php } ?>
                      </select>
                    </div>
                    <div class="form-group">
                      <label class="control-label">Menu Sub Category</label>
                      <input type="text" name="subcategory" value="<?php echo $data2[0]->subcategory; ?>"  data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out sub category name..."  class="form-control"  placeholder="">
                      <input type="hidden" name="Id"  value="<?php echo $data2[0]->id; ?>" class="form-control"  placeholder="">
                    </div>
                    <div class="form-group">
                            <label class="control-label">Description</label>
                            <input type="text" name="description"  value="<?php echo $data2[0]->description; ?>" data-validation="length"    class="form-control">
                    </div>
                   <div class="form-group">
                      <button type="submit" value="Validate" class="btn btn-default"><i class='glyphicon glyphicon-ok'></i> Save</button>
                    </div>
                    </form>
                </div>
               </div>
         </div>
    </div>


</section>
<script type="text/javascript">
$("#Simpan").submit(function() {
    var subcategory = $('#subcategory').val();
     var description = $('#description').val();
        if (subcategory == ''|| description==''){
            File_Kosong(); return false;
        }else{
        event.preventDefault();
        $.confirm({
          title: 'Confirmation',
          content: 'Are You Sure to Save?',
           type: 'blue',
          buttons: {
              Simpan: function () {
                  $.LoadingOverlay("show");
                  $("#Simpan").submit();
              },
              Batal: function () {

                  $.alert('Data Tidak Disimpan...');
              },
          }
      });
    }

});
</script>

<?php
$this->load->view('template/Js');
$this->load->view('template/Foot');
?>



<script type="text/javascript">
  function File_Kosong() {
  $.alert({
    title: 'Caution!!',
    content: 'Transaction Is Invalid!',
    icon: 'fa fa-warning',
    type: 'orange',
});
}
</script>

<?php
$this->load->view('template/Foot');
$this->load->view('template/Js');
?>

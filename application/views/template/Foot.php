<style type="text/css">
.modal.modal-wideClosing .modal-dialog {
  width: 30%;
}

.modal-wide .modal-body {
  overflow-y: auto;
}

#tallModal1 .modal-body p { margin-bottom: 900px }
.pull-right {
    float: right;
}

.pull-left {
    float: left;
}



</style>
  <div class="modal modal-wideClosing fade" id="ModalProses" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" title="close" data-dismiss="modal">&times;</button>
                    <h4 align="center" class="modal-title">Tutup Hari</h4>
                </div>
                <div class="modal-body">
                <form method="post"  enctype="multipart/form-data">


                   
                    <div class="form-group" align="center">
                        <label class="control-label">Pilih Tanggal Selanjutnya</label>
                          <div class="input-group date">
                            <div class="input-group-addon">
                              <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" value=" <?= $this->session->userdata('TanggalSistem') ?>" data-validation="date" data-validation-format="mm/dd/yyyy" data-validation-error-msg="Tanggal Awal..."  
                            data-validation-require-leading-zero="false" class="form-control pull-right" name= "Tgl" id="datepicker">
                          </div>
                         </div>
                       <div class="form-group">
                            <a  class="btn btn-default Simpan"><i class='glyphicon glyphicon-upload'></i> Proses</a>
                       </div>
                    </form>
                <!-- /.input group -->
              </div>
                </div>
                
            </div>
        </div>

</div><!-- /.content-wrapper -->


<footer class="main-footer" >
    <div class="pull-right hidden-xs" >
        <b>Version</b> 1.0.1
    </div>
    <strong>Copyright &copy; 2017 <a href="http://maxinous.co.id" target="_blank">Max|nuos-Bali</a>.</strong> All rights reserved.
</footer>



<script type="text/javascript">
      // $(".Simpan").click(function(){
      //    var tgl = $('#datepicker').val(); 
      // $.ajax({
      //   type: "POST",
      //   url: '<?php echo site_url('Rbac/change_tgl');?>',
      //   data: {Tgl: tgl},
      //   success: function(msg){
      //     if (msg!="") {
      //       var response = $.parseJSON(msg);
      //        window.location = response;
            
      //     };
      //    }
      // });
      //   });
</script>


<script type="text/javascript">
$(".Simpan").click(function() {
    var tgl = $('#datepicker').val(); 
        event.preventDefault();
        $.confirm({
          title: 'Konfirmasi',
          content: 'Apakah Anda Yakin Mengganti Hari Ke :'+tgl+' ?',
           type: 'blue',
          buttons: {
              Proses: function () {
                $.LoadingOverlay("show");
                $.ajax({
                  type: "POST",
                  url: '<?php echo site_url('Rbac/change_tgl');?>',
                  data: {Tgl: tgl},
                  success: function(msg){
                    if (msg!="") {
                      var response = $.parseJSON(msg);
                       window.location = response;
                      
                    };
                   }
                });
              },
              Batal: function () {
                
                  $.alert('Tanggal Sistem Tidak Diganti...');
              },
          }
      });
});
</script>


<script type="text/javascript">
   //  $(".change").click(function(){   
 		// 	$.ajax({
			// 	type: "POST",
			// 	url: '<?php echo site_url('Rbac/change_role');?>',
			// 	data: {id_role: $(this).attr("idK")},
			// 	success: function(msg){
			// 		if (msg!="") {
			// 			var response = $.parseJSON(msg);
			// 			 window.location = response;
						
			// 		};
			// 	}
			// });
   //      });
</script>

<script type="text/javascript">
$(".change").click(function() {
      var role = $(this).attr("idK"); 
        event.preventDefault();
        $.confirm({
          title: 'Konfirmasi',
          content: 'Are You Sure to Change Role ?',
           type: 'blue',
          buttons: {
              Change: function () {
                $.LoadingOverlay("show");
                $.ajax({
                  type: "POST",
                  url: '<?php echo site_url('Rbac/change_role');?>',
                  data: {id_role: role},
                  success: function(msg){
                    if (msg!="") {
                      var response = $.parseJSON(msg);
                       window.location = response;
                      
                    };
                   }
                });
              },
              Cancel: function () {
                
                  $.alert('Canceled...');
              },
          }
      });
});
</script>




 <script>
  $( function() { 
    $( "#datepicker" ).datepicker({
      autoclose: true,
      dateFormat: 'mm/dd/yy'
    });


  });
  </script>

        <script type="text/javascript">
    //  $(".Proses").click(function(e){         
            
    //         e.preventDefault();
    //         var mymodal = $('#ModalProses');
    //         $('#ModalProses').modal({backdrop: 'static', keyboard: false}) ;
    //          mymodal.modal('show');  
       
    // });
</script>


</div><!-- ./wrapper -->

 <?php $this->load->view('template/msg_sukses'); ?>

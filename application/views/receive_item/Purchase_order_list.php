<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>
<style type="text/css">
.modal.modal-wide .modal-dialog {
  width: 90%;
}

.modal-wide .modal-body {
  overflow-y: auto;
}

#tallModal .modal-body p { margin-bottom: 900px }
.pull-right {
    float: right;
}

.pull-left {
    float: left;
}

</style>

<section class="content-header">
    <div class="btn-group btn-breadcrumb">
            <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
            <a  class="btn btn-default  btn-xs active">Purchase Order List</a>
        </div>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Receive Item</h3>
        </div>
    <!-- /.box-header -->
        <div class="box-body">
        <div class="table-responsive">
          <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr >
                                        <th >No</th>
                                        <th >PO No.</th>
                                        <th >Date</th>
                                        <th >Vendor</th>
                                        <th >Description</th>
                                        <th >Expected Ship</th>
                                        <th >Amount</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i=1;
                                    foreach($data as $u){

                                    ?>

                                    <tr  class='odd gradeX context'>
                                         <td><?php echo $i ?></td>
                                        <td class="Kodepo"><?php echo $u->kodepo ?></td>
                                        <td class="Date"><?php echo $u->dateorder?></td>
                                        <td><?php echo $u->vendor?></td>
                                        <td><?php echo $u->description?></td>
                                        <td ><?php echo $u->dateexpected?></td>
                                        <td ><?php echo number_format($u->total, 0, '.', '.') ?></td>
                                         <td align="center">
                                         <a class="btn btn-default Detail"   href="<?php echo base_url(). 'index.php/Receive_item/Receive_item/'.$u->kodepo; ?>" title="Receive Order Item">  <span class="fa fa-fw fa-list" ></span> </a>

                                        </td>






                                    </tr>
                                    <?php $i++;} ?>
                                </tbody>
                            </table>
          </div>



        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->

</section><!-- /.content -->



<?php
$this->load->view('template/Js');
$this->load->view('template/Foot');
?>


<!-- <script type="text/javascript">
  $(document).ready(function(){
     $(".Detail").click(function(e){
      var kodepr =$(this).closest('tr').children('td.Kodepr').text();
      var date =$(this).closest('tr').children('td.Date').text();
      $.ajax({
        type: "POST",
        data: {id: kodepr},
        success: function(data){
             e.preventDefault();
            var mymodal = $('#myModal');
            var height = $(window).height() - 200;
            mymodal.find(".modal-append").css("max-height", height);
            $("#kodeprm span").text(kodepr);
            $("#Datem span").text(date);
            mymodal.find('.modal-append').html(data);
            mymodal.modal('show');
        }
      });
    });
  });
</script> -->
<?php if($this->session->flashdata('kodero')): ?>
<?php $koderocetak =  $this->session->flashdata('kodero');?> 
<script type="text/javascript">
  $(document).ready(function() {
    var kodero = "<?php echo $koderocetak;?>"
    
          window.open("<?php echo base_url(). 'index.php/Purchase_order/CetakMI/';?>?NoRo="+kodero ,"MyTargetWindowName")
  });
  </script>

  <?php endif; ?>


<script>
  $(function () {
    $("#example2").DataTable();
     $("#example11").DataTable();
    $('#example1').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": false
    });
  });
</script>

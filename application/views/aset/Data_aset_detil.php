
        <div class="table-responsive">
          <table id="example11" class="table table-bordered table-striped">
                                <thead>
                                    <tr >
                                        <th >Code Aset</th>
                                        <th >Description</th>
                                        <th >Kondisi Awal</th>
                                        <th >Kondisi Akhir</th>                         
                                        <th >Qty</th>
                                        <th >Note</th>
                                        <th >Input Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i=1;
                                    foreach($data as $u){
                                      //codeaset,description, baik,kurangbaik, $set as masuk, out
                                    ?>
                                    <tr  class='odd gradeX context'>
                                        <td><?php echo $u->codeaset ?></td>
                                        <td><?php echo $u->description?></td>
                                        <td><?php echo $u->kondisiawal?></td>
                                        <td><?php echo $u->kondisiakhir?></td>
                                        <td><?php echo number_format($u->qty, 0, '.', '.')?></td>
                                        <td><?php echo $u->note?></td>
                                        <td><?php echo $u->inputdate?></td>

                                    </tr>
                                    <?php $i++;} ?>
                                </tbody>
                            </table>
          </div>

         

        </div><!-- /.box-body -->
        </form>
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->





<script>
  $(function () {
    
    $('#example11').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
  });
</script>

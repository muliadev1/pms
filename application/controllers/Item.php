<?php

class Item extends CI_Controller{

	function __construct(){
		parent::__construct();
		if($this->session->userdata('Kode_userPms')==NULL) {
           redirect('Login');
		}
		$this->load->library(array('session','create_pdf','ciqrcode'));
		$this->load->helper(array('form', 'url'));
		$this->load->model(array('M_category','M_category','M_item','M_aset','M_subcategory','M_rbac'));
		
		if (!empty($this->session->userdata("active_db"))) {
			$this->load->database($this->session->userdata("active_db"), FALSE, TRUE);
		}

	}

	function index(){
		redirect('Item/Item_view');
	}

	function Item_view(){
		$data['data'] = $this->M_item->Item_view();

		$this->load->view('item/item',$data);
	}
	function Stok_view(){
		$data['dataDepaetemen'] = $this->M_item->loaddepartementall();
		$this->load->view('item/Stok',$data);
	}
	function Stok_view_rekap(){
		$data['dataDepaetemen'] = $this->M_item->loaddepartementall();
		$this->load->view('item/Stok_rekap',$data);
	}
	function Stok_view_data(){
		$Subcategory = $this->input->post('Subcategory');
		$iddep = $this->input->post('iddep');
		$data['data'] = $this->M_item->load_stok_bydepartement($iddep,$Subcategory);
		$this->load->view('item/Stok_data',$data);
	}
	function Stok_view_data_rekap(){
		$Subcategory = $this->input->post('Subcategory');
		$iddep = $this->input->post('iddep');
 		$DateFrom = $this->input->post('Tgl1');
 		$DateTo = $this->input->post('Tgl2');
	   $DateFrom1 = DateTime::createFromFormat('m/d/Y',$DateFrom);
	   $DateFrom2 = $DateFrom1->format("Y-m-d");
	   $DateTo1 = DateTime::createFromFormat('m/d/Y',$DateTo);
	   $DateTo2 = $DateTo1->format("Y-m-d");

		$data['where']= array('Subcategory' => $Subcategory,'iddep'=>$iddep,'DateFrom'=>$DateFrom2,'DateTo'=>$DateTo2, );
		$data['data'] = $this->M_item->load_item_bydepartement_rekap($iddep,$Subcategory);
		$this->load->view('item/Stok_rekap_data',$data);
	}

	function Cetak($JenisLaporan,$Text){
		$data['hotel'] = $this->M_rbac->getPerusahaan();
		$data['qr'] = $this->ciqrcode->generateQRcode($JenisLaporan, $Text,1.9);
		$data1= $this->load->view('template/Cetak_head',$data, TRUE);
		return $data1;
	}

	function Stok_cetak(){
		$idsub = $this->input->get('idsub', TRUE);
		$sub = $this->input->get('sub', TRUE);
		$departement = $this->input->get('dep', TRUE);
		$iddepartement = $this->input->get('iddep', TRUE);

		$sub = $this->input->get('sub', TRUE);
	    $data['header'] = $this->Cetak("Form_Stok_opname",  $this->session->userdata('Nama_userPms'). date("d-m-Y_H-i-s"), 1);
	     $data['data'] =  $this->M_item->load_stok_bydepartement($iddepartement,$idsub);
	    $data['head'] =  array('NamaDep' =>$departement,'NamaSub' =>$sub);
	    $html=$this->load->view('item/cetak/Stok',$data, TRUE);
     	$this->create_pdf->load($html,'Form_Stok_opname', 'A4-P');	
	}

	function Stok_cetak_rekap(){
		$idsub = $this->input->get('idsub', TRUE);
		$sub = $this->input->get('sub', TRUE);
		$departement = $this->input->get('dep', TRUE);
		$iddepartement = $this->input->get('iddep', TRUE);
		$DateFrom = $this->input->get('tgl1', TRUE); 
 		$DateTo = $this->input->get('tgl2', TRUE); 

		$DateFrom1 = DateTime::createFromFormat('m/d/Y',$DateFrom);
	   $DateFrom2 = $DateFrom1->format("Y-m-d");
	    $DateFrom3 = $DateFrom1->format("m-d-Y");
	   $DateTo1 = DateTime::createFromFormat('m/d/Y',$DateTo);
	   $DateTo2 = $DateTo1->format("Y-m-d");
	    $DateTo3 = $DateTo1->format("m-d-Y");

		$data['where']= array('Subcategory' => $idsub,'iddep'=>$iddepartement,'DateFrom'=>$DateFrom2,'DateTo'=>$DateTo2, );


		$sub = $this->input->get('sub', TRUE);
	    $data['header'] = $this->Cetak("Form_Stok_opname",  $this->session->userdata('Nama_userPms'). date("d-m-Y_H-i-s"), 1);
	    $data['data'] = $this->M_item->load_item_bydepartement_rekap($iddepartement,$idsub);
	    $data['head'] =  array('NamaDep' =>$departement,'NamaSub' =>$sub,'Subcategory' => $Subcategory,'iddep'=>$iddep,
	    	'DateFrom'=>$DateFrom3,'DateTo'=>$DateTo3,);
	    $html=$this->load->view('item/cetak/Stok_rekap',$data, TRUE);
     	$this->create_pdf->load($html,'Form_Stok_opname', 'A4-P');	
	}

	function Item_add(){
		$data['data'] = $this->M_subcategory->Subcategory_view();
		$this->load->view('Item/Item_add',$data);
	}

	function view_history_priceitem(){
		$codeitem = $this->input->post('codeitem');
		$data['data'] = $this->M_item->Load_history_priceitem($codeitem);
		$this->load->view('Item/History_price_item',$data);
	}

	function Item_addDB(){
		$codeitem= $this->M_item->getkodeitem();
		$codeaset= $this->M_aset->getkodeaset();
		$data = array(
			'codeitem' => $codeitem,
			'idsubcategory' => $this->input->post('idsubcategory'),
			'description' => $this->input->post('description'),
			'unit' => $this->input->post('unit'),
			'price' => $this->input->post('price')
		);

		if ($this->input->post('idsubcategory')==5) {
			$dataAset = array(
			'codeaset' => $codeaset,
			'codeitem' =>$codeitem
		);
			$this->M_aset->Aset_addDB('tbaset',$dataAset);
		};
		
		$this->M_item->Item_addDB('tbitem',$data);
		$this->session->set_flashdata('msg', 'Add Item successfully ...');
		redirect('Item/Item_view');
	}
	function Item_add_trx($dari){
		$data['data'] = $this->M_subcategory->Subcategory_view();
		if ($dari=='PO') {
			$data['dari'] =  array('dari' => 'Purchase_order/purchase_order_add');
		}elseif ($dari=='POD') {
			$data['dari'] =  array('dari' => 'Purchase_order/purchase_order_direct_add');
		}
			
		$this->load->view('Item/Item_add_trx',$data);
	}
	function Item_addDB_trx(){
		$codeitem= $this->M_item->getkodeitem();
		$codeaset= $this->M_aset->getkodeaset();
		$dari = $this->input->post('dari');
		$data = array(
			'codeitem' => $codeitem,
			'idsubcategory' => $this->input->post('idsubcategory'),
			'description' => $this->input->post('description'),
			'unit' => $this->input->post('unit'),
			'price' => $this->input->post('price'),
			'lastprice' => $this->input->post('price')
		);
		if ($this->input->post('idsubcategory')==5) {
				$dataAset = array(
				'codeaset' => $codeaset,
				'codeitem' =>$codeitem
			);
				$this->M_aset->Aset_addDB('tbaset',$dataAset);
			};
		$this->M_item->Item_addDB('tbitem',$data);
		$this->session->set_flashdata('msg', 'Add Item successfully ...');
		redirect($dari);
	}

	function backup_stok(){
		$datadetil = $this->input->post('data');
		$iddepartement = $this->input->post('iddep') ;
		$idsubcategory = $this->input->post('Subcategory') ;
		//print_r($datadetil);exit();
		$data = array(
			'iddepartement' => $iddepartement,
			'idsubcategory' => $idsubcategory,
			'date' => date("Y-m-d")
		);
		
		$this->M_item->backup_stok($data, $datadetil,$this->input->post('iddepartement'),  $this->input->post('idsubcategory'));
		$this->session->set_flashdata('msg', 'Add Item successfully ...');
		redirect($dari);
	}

	function Item_edit($id){
		$data['data'] = $this->M_subcategory->Subcategory_view();
		$data['data2'] = $this->M_item->Item_view_edit($id) ; 
		$this->load->view('item/item_edit',$data);
	}
	function Item_edit_akun($id){
		$data['data'] = $this->M_subcategory->Subcategory_view();
		$data['dataakun'] = $this->M_item->load_akun(); 
		$data['data2'] = $this->M_item->Item_view_edit($id) ; 
		$this->load->view('item/item_edit_akun',$data);
	}

	function Item_editDB(){
		$id = $this->input->post('Id');
		$data = array(

			'idsubcategory' => $this->input->post('idsubcategory'),
			'description' => $this->input->post('description'),
			'unit' => $this->input->post('unit'),
			'price' => $this->input->post('price')
	);
		$this->M_item->Item_editDB('tbitem',$data,$id) ;
		$this->session->set_flashdata('msg', 'Edit Item successfully...');
		redirect('Item/Item_view');
	}

	function Item_edit_aset($id){
		//$id = $this->input->post('Id');
		$data = array(
			'isaktif' => 0
	);
		$this->M_item->Item_editDB('tbitem',$data,$id) ;
		$this->session->set_flashdata('msg', 'Non Aktif Item successfully...');
		redirect('Item/Item_view');
	}

		function Item_editDB_akun(){
		$id = $this->input->post('Id');
		$data = array(
			'akunHK'=> $this->input->post('akunHK'),
			'akunFO'=> $this->input->post('akunFO'),
			'akunFB'=> $this->input->post('akunFB'),
			'akunBO'=> $this->input->post('akunBO'),
			'akunBar'=> $this->input->post('akunBar'),
			'akunSec'=> $this->input->post('akunSec'),
			'akunPG'=> $this->input->post('akunPG'),
			'akunEng'=> $this->input->post('akunEng')
	);
		$this->M_item->Item_editDB('tbitem',$data,$id) ;
		$this->session->set_flashdata('msg', 'Edit Item successfully...');
		redirect('Item/Item_view');
	}


}
?>

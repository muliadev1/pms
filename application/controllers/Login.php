<?php 

class Login extends CI_Controller{

	function __construct(){
		$this->CI =& get_instance(); 
		parent::__construct();		
		$this->load->model(array('M_login','M_rbac'));
		$this->load->helper(array('url'));	
		$this->load->library( array('create_menu'));	 

	}



		function index(){
		redirect(site_url('Login/login'));
	}

	


	  function login(){

		//jika sudah login, arahkan ke home page
		if($this->session->userdata('Kode_userPms')!=NULL){
			redirect('Rbac');
		}
		//jika belum, munculkan form login
		$this->load->library('form_validation');
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('sistem/Login');
			return;
		}
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$result = $this->M_login->getdataOP($username, $password)->result();

		$active_db = "demo";
		if (strtoupper($username) == 'KAYON1') {
			$active_db = "kayon1";
		}elseif (strtoupper($username) == 'KAYON2') {
			$active_db = "kayon2";
		}
			
		$this->load->database($active_db, FALSE, TRUE);
		$dNA = $this->db->query("SELECT DATE_FORMAT(tglna,'%Y-%m-%d') as tgl FROM tbna ORDER BY tglna DESC LIMIT 1")->row();

		if (!empty($result)) {

			$role_user =  $this->M_rbac->get_role_user($result[0]->id_user);
			$menu_user =  $this->M_rbac->get_menu_user($role_user[0]->id_role);			
			$menu_aktif =  $this->create_menu->generate_menu($menu_user);
			$perusahaan = $this->M_rbac->getPerusahaan();
			$submenu_all = $this->M_rbac->getLinkSubmenu();
			$departement =  $this->M_rbac->get_departement_user($result[0]->id_user); 
		

			$user_sessionPms= array(
				'session_idPms' => $this->session->userdata('session_id'),
				'Kode_userPms' => $result[0]->id_user,
				'Nama_userPms' => $result[0]->nama_user,
				'UsernamePms' => $result[0]->username,
				'PasswordPms' => $result[0]->password,
				'ParentPms' => $result[0]->Parent,
				'id_ParentPms' => $result[0]->Id_Parent,
				'id_DepartementPms' => $departement->iddepartement,
				'rolePms'			=> $role_user,
				'role_aktifPms'	=> $role_user[0]->nama_role,
				'id_role_aktifPms'	=> $role_user[0]->id_role,
				'submenu_allPms'	=> $submenu_all,
				'menu_aktifPms'	=> $menu_aktif,
				'NamaPerusahaan'=> $perusahaan[0]->NamaPerusahaan,
				'AlamatPerusahaan'=> $perusahaan[0]->Alamat,
				'NoTlpPerusahaan'=> $perusahaan[0]->NoTlp,
				'KodeKategoriAset'=> $perusahaan[0]->KategoriAset,
				'active_db' => $active_db,
				'tgl_na' => $dNA->tgl,
				'outlet_name' => $result[0]->outlet_name

			);
			$this->session->set_userdata($user_sessionPms);


			

			// $target = '';
			// $call_from = $this->input->get('call_from');
			// if(!empty($call_from)){
			// 	$target = $call_from;
			// }else{
				$target = 'Rbac/index';
			//}
			$response['errno'] 		= 0;
			$response['message'] 	= site_url($target);
			echo json_encode($response);

		}else{
			$response['errno']  	= 1;
			$response['message']   	= "Kombinasi username dan password salah.";
			echo json_encode($response);
		}


	}

	function logout(){
	$this->session->sess_destroy();
	redirect('login');
	}

}
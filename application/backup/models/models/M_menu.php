<?php

class M_Menu extends CI_Model{

	function Menu_view() {
		$data = $this->db->query("SELECT tbmenu.id,tbmenu.idmenusubcategory,tbmenusubcategory.subcategory,
                                     tbmenu.description,tbmenu.menu,tbmenu.price,
                                     tbmenu.tax,tbmenu.service,tbmenu.finalprice
															FROM tbmenu INNER JOIN tbmenusubcategory
															ON tbmenu.idmenusubcategory = tbmenusubcategory.id
															WHERE tbmenu.isaktif = 1");
		return $data->result();
   }

	 function Menu_addDB($table,$data) {
			 $this->db->insert($table,$data);
			// print_r($this->db->last_query());
	}

	function Menu_view_edit($id) {
		$data = $this->db->query("	SELECT tbmenu.id,tbmenu.idmenusubcategory, tbmenusubcategory.subcategory,
                                     tbmenu.description,tbmenu.menu,tbmenu.price,
                                     tbmenu.tax,tbmenu.service,tbmenu.finalprice
															FROM tbmenu INNER JOIN tbmenusubcategory
															ON tbmenu.idmenusubcategory = tbmenusubcategory.id
															WHERE tbmenu.isaktif = 1 AND tbmenu.id = $id ");
	 return $data->result();
	}

	function Menu_editDB($table,$data,$id) {
 			 $this->db->where('id',$id );
 			 $this->db->update($table,$data);
  }

}
?>

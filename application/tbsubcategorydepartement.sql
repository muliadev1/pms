/*
SQLyog Ultimate v9.30 
MySQL - 5.5.5-10.1.10-MariaDB : Database - pmsupdate
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`pmsupdate` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `pmsupdate`;

/*Table structure for table `tbsubcategorydepartement` */

DROP TABLE IF EXISTS `tbsubcategorydepartement`;

CREATE TABLE `tbsubcategorydepartement` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `iddepartement` int(10) DEFAULT NULL,
  `itemcode` varchar(50) DEFAULT NULL,
  `kodesubcategory` int(10) DEFAULT NULL,
  `desc` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `tbsubcategorydepartement` */

insert  into `tbsubcategorydepartement`(`id`,`iddepartement`,`itemcode`,`kodesubcategory`,`desc`) values (1,1,'00003',3,'FOOD'),(2,1,'00004',2,'VAGETABLE');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>

<section class="content-header">
    <div class="btn-group btn-breadcrumb">
            <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
            <a  class="btn btn-default  btn-xs active">Package</a>
        </div>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Package</h3>
          <a style="float:right"  href="<?php echo base_url('index.php/Package/Package_add_view');?>" class="btn btn-primary">
            <i class="glyphicon glyphicon-saved"></i>
            Add Package
          </a>
        </div>
    <!-- /.box-header -->
        <div class="box-body">
        <div class="table-responsive">
          <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr >
                                        <th >ID</th>
                                        <th >Package Name</th>
                                        <th >Package Rate</th>
                                        <th ></th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach($data as $u){
                                    ?>
                                    <tr  class='odd gradeX context'>
                                        <td><?php echo $u->id ?></td>
                                        <td><?php echo $u->package?></td>
                                        <td><?php echo number_format($u->packagerate)?></td>
                                        <td align="center">
                                         <a class="btn btn-warning btn-xs"  href="<?php echo base_url('index.php/Package/Package_view_edit/'.$u->id); ?>">  <span class="fa fa-fw fa-edit" ></span> </a>
                                        </td>






                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
          </div>



        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->

</section><!-- /.content -->


<?php
$this->load->view('template/Js');
$this->load->view('template/Foot');
?>
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>

<?php 


class Akun extends CI_Controller{

	function __construct(){
		parent::__construct();
		if($this->session->userdata('Kode_userPms')==NULL) {
           redirect('Login');
		}
				
		$this->load->model('M_akun');
		$this->load->helper('url');
		$this->load->library('auth');
		
		
		if (!empty($this->session->userdata("active_db"))) {
			$this->load->database($this->session->userdata("active_db"), FALSE, TRUE);
		}
	}
	
	function index(){
		$data['data'] = $this->M_akun->load()->result();
		
		$this->load->view('akun/Data_subklas',$data);
	}
	function indexAkun(){
		$data['data'] = $this->M_akun->loadAkun()->result();
		
		$this->load->view('akun/Data_akun_add',$data);
	}

	function DataAkun(){
		$data['data'] = $this->M_akun->loadAkun()->result();
		
		$this->load->view('akun/Data_akun',$data);
	}

	function SelectIdSubklas(){
		$IdKlas = $this->input->post('id');
		$idLama = $this->M_akun->loadIdSubklas($IdKlas)->result();

		if(empty($idLama)){
			$idBaru = $IdKlas."1";
		}else {
			foreach ($idLama as $row) {
				$idBaru = $row->idsub + 1;
			}
		}

		echo json_encode($idBaru);
		
	}

	function selectAkunId(){
		$IdSub = $this->input->post('id');
		$idLama = $this->M_akun->loadIdAkun($IdSub)->result();
		if(empty($idLama)){
			$idBaru = $IdSub."01";
		}else {
			foreach ($idLama as $row) {
				$idBaru = $row->NoAkun + 1;
			}
		}
		echo json_encode($idBaru);
	}

	function AddSubklas(){
		
		$this->load->view('akun/Data_subklas_add');
	}

	function AddSubklasDB(){
	$data = array(
			'IdSub' => $this->input->post('idSub'),
			'NamaSub' => $this->input->post('subKlas'),
			'IsAktif' => "1"
			);
	    $this->session->set_flashdata('msg', ' Data Berhasil Disimpan... ');
		$this->M_akun->addSubKlas($data, 'tbSubklasifikasi');
		redirect('Akun/AddSubklas');
	}

	function AddAkun(){
		$data['data'] = $this->M_akun->load()->result();
		$this->load->view('akun/Data_akun_add', $data);
	}

	function EditAkun($id){
		$data['edit'] = $this->M_akun->loadAkun_byid($id); 
		$data['data'] = $this->M_akun->load()->result();
		$this->load->view('akun/Data_akun_edit', $data);
	}

	function AddAkunDB(){

		$IsKontra = $this->input->post('Kontra');
		if ($IsKontra=='1') {
			$IsKontra='1';
		}else{
			$IsKontra= '0';
		}
	$data = array(
			'NoAKun' => $this->input->post('kodeAkun'),
			'IdSub' => $this->input->post('klas'),
			'NamaAKun' => $this->input->post('namaAkun'),
			'Saldo' => '0',
			'IsKontra' => $IsKontra,
			'IsAktif' => "1"
			);

	
		$this->M_akun->addSubKlas($data, 'tbakun');
		$this->session->set_flashdata('msg', ' Data Berhasil Disimpan... ');
		redirect('Akun/AddAkun');
	}

	function EditAkunDB(){

		$IsKontra = $this->input->post('Kontra');
		if ($IsKontra=='1') {
			$IsKontra='1';
		}else{
			$IsKontra= '0';
		}
		$id =  $this->input->post('kodeAkun');
	$data = array(
			'IdSub' => $this->input->post('klas'),
			'NamaAKun' => $this->input->post('namaAkun'),
			'Saldo' => '0',
			'IsKontra' => $IsKontra,
			'IsAktif' => "1"
			);

	
		$this->M_akun->Akun_editDB($data,$id);
		$this->session->set_flashdata('msg', ' Data Berhasil Disimpan... ');
		redirect('Akun/DataAkun');
	}


	// function SetorTabungan(){
		
	// 	$this->load->view('v_transaksi_tabungan_setor_langsung');
	// }
	

	
}

?>
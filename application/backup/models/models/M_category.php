<?php

class M_category extends CI_Model{

	function Category_view() {
		$data = $this->db->query("Select * from tbcategory where isaktif=1");
		return $data->result();
   }

	 function Category_addDB($table,$data) {
			 $this->db->insert($table,$data);
			// print_r($this->db->last_query());
	}

	function Category_view_edit($id) {
	 $this->db->select("*");
	 $this->db->where('idcategory',$id);
	 $this->db->from('tbcategory');
	 return $this->db->get()->result();
	}

	function Category_editDB($table,$data,$id) {
 			 $this->db->where('idcategory',$id );
 			 $this->db->update($table,$data);
  }

}
?>


<?php

class M_resto extends CI_Model{



	function item_view() {
		$this->db->select("*");
		$this->db->from('tbitem');
		return $this->db->get()->result();

   }

       

  function Table_view() {
  
    $data = $this->db->query("SELECT tbtable.*, lastname,firstname, trxid FROM tbtable
		LEFT JOIN tbguest ON tbtable.`idguest` =  tbguest.idguest
		LEFT JOIN tbfbtrx ON tbtable.`tablenumber` = tbfbtrx.`tableid` AND tbfbtrx.`totalpayment` IS NULL
		ORDER BY id ASC");
    //print_r($data->result());exit();
    return $data->result();

 }

  function get_last_trx() {
  
    $data = $this->db->query("SELECT trxid,tableid FROM tbfbtrx WHERE totalpayment IS NULL ORDER BY trxid DESC LIMIT 1");
    return $data->row();

 }

 function Guest_view() {
		$data = $this->db->query("Select * FROM tbguest WHERE isaktif=1");
		return $data->result();
   }
   function Operator_view() {
		$data = $this->db->query("SELECT * FROM tb_user ");
		return $data->result();
   }
   function Waiter_view() {
		$data = $this->db->query("Select * FROM tbwaiter WHERE isaktif=1");
		return $data->result();
   }
   function Select_clossing_byop($idop) {
		$data = $this->db->query("SELECT id,DATE_FORMAT(DATE,'%d-%m-%Y %h:%i') AS tgl FROM `tbclosingrestomaster` WHERE id_operator = '$idop'");
		return $data->result();
   }

   




	function Get_menu($id){
		$data = $this->db->query('SELECT * FROM tbmenu WHERE idmenusubcategory = '.$id);
		return $data->result();
	}

	function Get_one_menu($id){
		$data = $this->db->query('SELECT * FROM tbmenu WHERE id = '.$id);
		return $data->row();
	}

	function Add_new_menus($operator, $tableId,$idWaiter,$WaiterName){
		$tahun = date('y');
		$bulan = date('m');
		$checkId = "FB".$tahun.$bulan;
		$q = $this->db->query("SELECT MAX(RIGHT(trxid, 5)) AS idmax FROM tbfbtrx WHERE trxid LIKE '%".$checkId."%' ");
		if($q->num_rows()>0){
			foreach($q->result() as $k){
				$tmp = ((int)$k->idmax)+1;
				$id = "0000".$tmp;
				$kd = substr($id, -5);
			}
		}else{
				$kd = "00001";
		}
		$newID = "FB".$tahun.$bulan.$kd;

		$this->db->query('INSERT INTO tbfbtrx(`trxid`,`trxdate`,`tableid`, `idoperatoropenorder`,`waitername`,`waiterid`) 
			VALUES("'.$newID.'", "'.date("Y-m-d H:i:s").'", "'.$tableId.'", "'.$operator.'", "'.$WaiterName.'", "'.$idWaiter.'"  )');

		return $newID;
	}
		function Load_kdmaster(){
		$tahun = date('y');
		$bulan = date('m');
		$checkId = "FB".$tahun.$bulan;
		$q = $this->db->query("SELECT MAX(RIGHT(trxid, 5)) AS idmax FROM tbfbtrx  where month(trxdate) = month(curdate()) order by  MAX(RIGHT(trxid, 5)) desc  ");
		if($q->num_rows()>0){
			foreach($q->result() as $k){
				$tmp = ((int)$k->idmax)+1;
				$id = "0000".$tmp;
				$kd = substr($id, -5);
			}
		}else{
				$kd = "00001";
		}
		$newID = "FB".$tahun.$bulan.$kd;

		return $newID;
	}
	function Resto_get_all_menusubcategory(){
		$result = $this->db->query('SELECT id, subcategory FROM tbmenusubcategory');
		return $result;
	}

	function Add_new_detail_menu($newID, $id_menu, $menu, $price, $qty, $request, $operator,$idWaiter,$WaiterName,$OrderNoManual){
		$this->db->query('INSERT INTO tbfbtrxdetil(`trxid`,`trxdate`,`menuid`,`menu`,`price`,`disp`,`dism`,`qty`,`subtotal`,`addrequest`,`idoperator`,`waitername`,`waiterid`,`ordernomanual`)
		VALUES ("'.$newID.'", "'.date("Y-m-d H:i:s").'", "'.$id_menu.'", "'.$menu.'", "'.$price.'", "0.00", "0.00", "'.$qty.'", "'.$price*$qty.'", "'.$request.'", "'.$operator.'", "'.$WaiterName.'", "'.$idWaiter.'", "'.$OrderNoManual.'"  )');
	}

	function Get_trx_menu_detail($idTrx){
		return $this->db->query('SELECT * FROM tbfbtrxdetil WHERE trxid = "'.$idTrx.'"');
	}
	function Get_trx_menu_detail_cetak($idTrx){
		$data= $this->db->query("SELECT tbfbtrxdetil.*, category,tbwaiter.`id` AS idwaiter, ordernomanual,tbfbtrxdetil.waitername FROM 							tbfbtrxdetil INNER JOIN `tbmenu` 
								ON tbfbtrxdetil.`menuid` = tbmenu.`id` INNER JOIN `tbmenusubcategory`
								ON tbmenu.`idmenusubcategory` = tbmenusubcategory.`id`INNER JOIN `tbmenucategory`
								ON tbmenusubcategory.`idcategory` = tbmenucategory.`id` 
								LEFT JOIN tbwaiter
								ON tbfbtrxdetil.`waiterid` = tbwaiter.`id`
								WHERE trxid='$idTrx'");
		return $data->result();
	}

	function Get_trx_menu_detail_add_guest($idTrx){
		$data= $this->db->query("SELECT tbfbtrxdetil.*, category,tbwaiter.`id` AS idwaiter, ordernomanual,tbfbtrxdetil.waitername FROM 							tbfbtrxdetil INNER JOIN `tbmenu` 
								ON tbfbtrxdetil.`menuid` = tbmenu.`id` INNER JOIN `tbmenusubcategory`
								ON tbmenu.`idmenusubcategory` = tbmenusubcategory.`id`INNER JOIN `tbmenucategory`
								ON tbmenusubcategory.`idcategory` = tbmenucategory.`id` 
								LEFT JOIN tbwaiter
								ON tbfbtrxdetil.`waiterid` = tbwaiter.`id`
								WHERE trxid='$idTrx'");
		return $data->result();
	}

	function Get_trx_menu_detail_category_cetak($idTrx,$category){
		$data= $this->db->query("SELECT tbfbtrxdetil.*, category,tbwaiter.`id` as idwaiter, ordernomanual,tbfbtrxdetil.waitername FROM tbfbtrxdetil INNER JOIN `tbmenu` 
								ON tbfbtrxdetil.`menuid` = tbmenu.`id` INNER JOIN `tbmenucategory`
								ON tbmenu.`idmenusubcategory` = tbmenucategory.`id` INNER JOIN tbwaiter
								ON tbfbtrxdetil.`waiterid` = tbwaiter.`id`
  								WHERE trxid ='$idTrx' and category='$category' ");
		return $data->result();
	}
	function Get_trx_menu_detail_category_cetak_bill1($idTrx,$category){
		

		$data= $this->db->query("SELECT tbfbtrxdetil.*, category,tbwaiter.`id` as idwaiter, ordernomanual,tbfbtrxdetil.waitername,
			tbtempbill.disc FROM tbfbtrxdetil INNER JOIN `tbmenu` 
								ON tbfbtrxdetil.`menuid` = tbmenu.`id` INNER JOIN `tbmenusubcategory`
								ON tbmenu.`idmenusubcategory` = tbmenusubcategory.`id`INNER JOIN `tbmenucategory`
								ON tbmenusubcategory.`idcategory` = tbmenucategory.`id`  LEFT JOIN tbwaiter
								ON tbfbtrxdetil.`waiterid` = tbwaiter.`id` INNER JOIN tbtempbill
								ON tbfbtrxdetil.`id` = tbtempbill.iddetil
  								WHERE trxid IN (SELECT idtrx from tbtempbill where aktif='1') and category='$category' ");
		return $data->result();
	}
	function Get_trx_menu_detail_category_cetak_closing($idTrx,$category){
		

		$data= $this->db->query("SELECT SUM(subtotal) as total FROM(
			SELECT tbfbtrxdetil.*
			 FROM tbfbtrxdetil INNER JOIN `tbmenu` 
								ON tbfbtrxdetil.`menuid` = tbmenu.`id` INNER JOIN `tbmenusubcategory`
								ON tbmenu.`idmenusubcategory` = tbmenusubcategory.`id`INNER JOIN `tbmenucategory`
								ON tbmenusubcategory.`idcategory` = tbmenucategory.`id`  LEFT JOIN tbwaiter
								ON tbfbtrxdetil.`waiterid` = tbwaiter.`id` 
  								WHERE  category='$category' and trxid = '$idTrx' ) AS a " );
		return $data->row();
	}
		function Get_trx_menu_master_cetak($idTrx){
		$data= $this->db->query("SELECT tbfbtrx.*, DATE_FORMAT(trxdate,'%d-%m-%Y') AS tanggaltrx , CONCAT(`firstname`, ' '  ,lastname) AS nama,roomname,tablenumber
  FROM `tbfbtrx` INNER JOIN tbguest
  ON tbfbtrx.`idguest` = tbguest.`idguest` LEFT JOIN tbroom
  ON tbfbtrx.`idroom` = tbroom.`id` INNER JOIN tbtable
  ON tbfbtrx.`tableid` = tbtable.`id`
 WHERE trxid='$idTrx'");
		return $data->row();
	}
	function Get_master_clossing($idop){
		$data= $this->db->query("SELECT tbfbtrx.*, DATE_FORMAT(trxdate,'%d-%m-%Y') AS tanggaltrx , CONCAT(`firstname`, ' '  ,lastname) AS nama,roomname,tablenumber
  FROM `tbfbtrx` INNER JOIN tbguest
  ON tbfbtrx.`idguest` = tbguest.`idguest` LEFT JOIN tbroom
  ON tbfbtrx.`idroom` = tbroom.`id` INNER JOIN tbtable
  ON tbfbtrx.`tableid` = tbtable.`id` 
   WHERE `idoperatorprocess`='$idop' AND isclosing =0");
		return $data->result();
	}
	function Get_master_clossing_cetak($idop,$idmaster){
		$data= $this->db->query("SELECT tbfbtrx.*, DATE_FORMAT(trxdate,'%d-%m-%Y') AS tanggaltrx , CONCAT(`firstname`, ' '  ,lastname) AS nama,roomname,tablenumber
  FROM `tbfbtrx` INNER JOIN tbguest
  ON tbfbtrx.`idguest` = tbguest.`idguest` LEFT JOIN tbroom
  ON tbfbtrx.`idroom` = tbroom.`id` INNER JOIN tbtable
  ON tbfbtrx.`tableid` = tbtable.`id` 
   WHERE  isclosing =1 and 
   tbfbtrx.trxid IN (
SELECT idtrx FROM `tbclossingresto` INNER JOIN `tbclosingrestomaster`
ON tbclossingresto.`idmaster` = tbclosingrestomaster.`id` WHERE tbclosingrestomaster.id='$idmaster' AND id_operator = '$idop') ");
		return $data->result();
	}

	function Get_clossing_master_cetak($idop,$idmaster){
		$data= $this->db->query("SELECT tbclosingrestomaster.* ,DATE_FORMAT(date,'%d-%m-%Y / %h:%m') AS tanggaltrx ,nama_user FROM
			`tbclosingrestomaster` INNER JOIN tb_user on tbclosingrestomaster.id_operator = tb_user.id_user
			 WHERE tbclosingrestomaster.id='$idmaster' 
			AND id_operator = '$idop' ");
		return $data->row();
	}

	

	function Get_payment_clossing($idtrx){
		$data= $this->db->query("SELECT tbfbtrxdetilpay.*, articlepayment,initial, tbpaymentstatus.paymentstatus FROM `tbfbtrxdetilpay` LEFT JOIN `tbarticlepayment`
				ON tbfbtrxdetilpay.`idarticlepayment` = tbarticlepayment.`id` LEFT JOIN `tbfbtrx`
				ON tbfbtrxdetilpay.`idtrx` = tbfbtrx.`trxid` LEFT JOIN `tbpaymentstatus`
				ON tbfbtrx.`paymentstatus` = tbpaymentstatus.`id` WHERE tbfbtrxdetilpay.`idtrx` = '$idtrx'");
		return $data->result();
	}

	

	

	  
	

	function Update_new_menus($newID, $guestId, $pmTotal, $pmDisc, $pmTotal2, $pmService, $pmTax, $pmGrandTotal){
		$this->db->query('UPDATE tbfbtrx SET `idguest` = "'.$guestId.'", `total` = "'.$pmTotal.'", `disc` = "'.$pmDisc.'", `total2` = "'.$pmTotal2.'", `service` = "'.$pmService.'", `tax` = "'.$pmTax.'", `grandtotal` = "'.$pmGrandTotal.'"
		WHERE `trxid` = "'.$newID.'"');
	}

	function Set_tb_table($tableId, $guestId, $status){
		if ($status == "open") {
			$this->db->query('UPDATE tbtable SET `idguest` = "'.$guestId.'" WHERE `tablenumber` = "'.$tableId.'"');
		}
	}

	function Get_one_trx($idTrx){
		return $this->db->query('SELECT * FROM tbfbtrx WHERE trxid = "'.$idTrx.'"');
	}
	function Get_one_trx_detil($idTrx){
		return $this->db->query('SELECT * FROM tbfbtrxdetil WHERE trxid = "'.$idTrx.'"');
	}

	function Get_one_trx_forpayment($idTrx){
		return $this->db->query('SELECT tbfbtrx.* , firstname,lastname NAME FROM tbfbtrx 
								 INNER JOIN tbguest ON tbfbtrx.`idguest` = tbguest.`idguest`
								 WHERE trxid = "'.$idTrx.'"');
	}
	function Get_article_payment(){
		$query= $this->db->query("SELECT * FROM tbarticlepayment WHERE status =1" );
		return $query->result();
	}
	function Get_Setting_resto(){
		$query= $this->db->query("SELECT * FROM tbsettingresto" );
		return $query->row();
	}

	

	function Get_one_trx_forsplitbill($idTrx){
		$query= $this->db->query("SELECT tbfbtrx.`trxid`,tbfbtrx.`trxdate`,`tableid`,tbfbtrx.`idguest`,`idreservation`,`idroom`,`total`,`disc`,`total2`,`service`,`tax`,
							`grandtotal`,`idarticle`,`idoperatoropenorder`,
							 firstname,lastname ,`menuid`,`menu`,`price`,`disp`,`dism`,`qty`,`subtotal`,`addrequest`,tbfbtrxdetil.`description` ,`id` AS iddetil,nama_user
							FROM tbfbtrx 
							INNER JOIN tbguest ON tbfbtrx.`idguest` = tbguest.`idguest`
							INNER JOIN tbfbtrxdetil ON tbfbtrx.trxid = tbfbtrxdetil.trxid
							INNER JOIN tb_user ON tb_user.id_user = tbfbtrx.`idoperatoropenorder`
							 WHERE tbfbtrx.trxid = '$idTrx'");
		return $query->result();
	}
	function Get_one_trx_formobetable($idTrx){
		$query= $this->db->query("SELECT tbfbtrx.`trxid`,tbfbtrx.`trxdate`,`tableid`,tbfbtrx.`idguest`,`idreservation`,`idroom`,`total`,`disc`,`total2`,`service`,`tax`,
							`grandtotal`,`idarticle`,`idoperatoropenorder`,
							 firstname,lastname ,`menuid`,`menu`,`price`,`disp`,`dism`,`qty`,`subtotal`,`addrequest`,tbfbtrxdetil.`description` ,`id` AS iddetil,nama_user
							FROM tbfbtrx 
							INNER JOIN tbguest ON tbfbtrx.`idguest` = tbguest.`idguest`
							INNER JOIN tbfbtrxdetil ON tbfbtrx.trxid = tbfbtrxdetil.trxid
							INNER JOIN tb_user ON tb_user.id_user = tbfbtrx.`idoperatoropenorder`
							 WHERE tbfbtrx.trxid = '$idTrx'");
		return $query->result();
	}

	  function table_check_movetable($tableid) { 
		    $data = $this->db->query("SELECT tbtable.*, lastname,firstname, trxid FROM tbtable
				LEFT JOIN tbguest ON tbtable.`idguest` =  tbguest.idguest
				LEFT JOIN tbfbtrx ON tbtable.`tablenumber` = tbfbtrx.`tableid` AND tbfbtrx.`totalpayment` IS NULL
				 where tableid = '$tableid' ");
		    return $data->result();
    }
    function table_check_detil_movetable_lama($trxid) { 
		    $data = $this->db->query(" SELECT * from tbfbtrxdetil where trxid='$trxid'");
		    return $data->result();
    }

	function payment_add_splitbill_detil($datadetil) {
	 		$this->db->where('id', $datadetil['id']);
	         $this->db->update('tbfbtrxdetil',$datadetil);
    }
    function payment_add_splitbill_master($datamaster) {
 			 $this->db->insert( 'tbfbtrx', $datamaster);
   }

    function add_splitbill1_temp($data) {
 			 $this->db->insert_batch('tbtempbill', $data);
   }

    function add_paymentdetil($data) {
 			 $this->db->insert_batch('tbfbtrxdetilpay', $data);
   }
    function Add_clossing($data) {
 			 $this->db->insert_batch('tbclossingresto', $data);
   }

    function Add_clossing_master($data) {
 			 $this->db->insert('tbclosingrestomaster', $data);
 			 return $this->db->insert_id();
   }

    function update_clossing($data,$idTrx) {
	 		$this->db->where('trxid', $idTrx);
	         $this->db->update('tbfbtrx',$data);
    }
   function select_splitbill1_temp() { 
		    $data = $this->db->query(" SELECT * from tbtempbill where aktif='1' LIMIT 1");
		    return $data->row()->idtrx;
    }
    function update_splitbill1_temp() {
	 		$data = $this->db->query(" UPDATE  tbtempbill set  aktif=0 ");
    }

  function payment_add_splitbill_detil_cash($datadetil) {
	 		$this->db->where('id', $datadetil['id']);
	         $this->db->update('tbfbtrxdetil',$datadetil);
    }
    function payment_add_splitbill_master_cash($datamaster,$id) {
 			$this->db->where('trxid',$id);
	        $this->db->update('tbfbtrx',$datamaster);
   }

    function update_detil_trx_formovetable($trxidLama,$trxidBaru) {
	 		$this->db->query("UPDATE tbfbtrxdetil set trxid='$trxidBaru' where id = '$trxidLama'  ");
    }

     function delete_master_trx_formovetable($trxid) {
	 		$this->db->query("DELETE FROM tbfbtrx where trxid='$trxid' ");
    }
    function add_movetable_emptytable_master($datamaster) {
 			 $this->db->insert( 'tbfbtrx', $datamaster);
   }
   function add_movetable_emptytable_detil($trxidLama,$trxidBaru) {
	 		$this->db->query("UPDATE tbfbtrxdetil set trxid='$trxidBaru' where id = '$trxidLama'  ");
    }







	






}

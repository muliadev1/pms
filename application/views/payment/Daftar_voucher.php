

          <div class="table-responsive">
                    <table id="example11" class="table table-bordered table-striped">
                                <thead>
                                    <tr >
                                       <th >Voucher No</th>
                                        <th >Voucher Date</th>
                                        <th >Total</th>
                                        <th>Action</th>
                                       
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    foreach($datav as $u){
                                    ?>
                                    <tr>
                                         <td class="id" ><?php echo $u->voucherno ?></td>
                                         <td   class="tgl"><?php echo $u->tglvoucher ?></td>
                                         <td align="right" class="total"><?php echo number_format($u->total, 0, '.', '.') ?></td>
                                      <td align="center">
                                       <a class='btn btn-default Pilih' id="" data-toggle='modal'     >  <span class='fa fa-fw  fa-check-square-o' ></span> </a>
                                      </td>

                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
          </div>
          
<script type="text/javascript">
  $(document).ready(function(){
     $(".Pilih").click(function(e){          
           var  VoucherNo   =$(this).closest('tr').children('td.id').text();  
           var  DateVoucher =$(this).closest('tr').children('td.tgl').text();  
           var   amount =$(this).closest('tr').children('td.total').text();  
        var mymodal = $('#myModal');
        $('#VoucherNo').val(VoucherNo);
        $('#DateVoucher').val(DateVoucher);
        $('#amount').val(amount);

             mymodal.modal('hide');    
    });
  });
</script>



  <script type="text/javascript">
  $(function () {
    $("#example11").DataTable();
  });
  </script>
        
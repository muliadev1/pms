<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>

<section class="content-header">
    <div class="btn-group btn-breadcrumb">
            <a href="#" id= "Progres" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
            <a  class="btn btn-default  btn-xs active">Night Audit</a>
        </div>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-xs-12">
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Night Audit</h3>
        </div>
    <!-- /.box-header -->
        <div class="box-body">
     
  <div class="row">
        <div class="col-lg-3 col-xs-6 " style="margin-right: 20px;">
          <div class="small-box bg-aqua box-widget" style="width: 300px; height: 105px;">
            <div class="inner">
              <p>NA Check</p>
            </div>
            <div class="icon">
              <i class="fa fa-refresh" ></i>
            </div>
        <a id="Clossing" class="small-box-footer" style="cursor: pointer;"> Click Here <i class="fa fa-arrow-circle-down"></i></a>
              
          </div>
        </div>

      <div class="col-lg-3 col-xs-6 " style="margin-left: 20px;">
          <div class="small-box bg-green box-widget" style="width: 300px; height: 105px;">
            <div class="inner">
              <p>NA Process</p>
            </div>
            <div class="icon">
              <i class="fa fa-users" ></i>
            </div>
        <a id="simpan1" class="small-box-footer" style="cursor: pointer;"> Click Here <i class="fa fa-arrow-circle-down"></i></a>
              
          </div>
        </div>

      </div>

         <div class="table-responsive tabelkosong">
          <table id="example1" class="table table-bordered table-striped ">
                                <thead>
                                     <tr >
                                        <th >NO</th>
                                        <th >Guest</th>
                                        <th >Article</th>
                                        <th >Amount</th>
                                        <th >Outlet</th>                         
                                        <th >Note</th>
                                        <th >User</th>
                                         <th style="display: none;">Idtrx</th> 
                                       </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
          </div>

        
            <div id="tabelada" hidden="hidden">
           </div>

            </div>

             

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->

</section><!-- /.content -->


<?php
$this->load->view('template/Js');
$this->load->view('template/Foot');
?>


<script type="text/javascript">
$("#simpan1").click(function() {    
        $.confirm({
          title: 'Konformasi',
          content: 'Apakah Anda Yakin Menyimpan Data ?',
           type: 'blue',
          buttons: {
              Simpan: function () {
                 var data= $("#exampleada").DataTable();
                 var datas = data.rows().data().toArray();
                $.ajax({
                type: "POST",
               url: '<?php echo site_url("Fo_outlet/Na_addDB");?>',
                data: {data: datas},
                success: function(data){
                 window.location = '<?php echo site_url('Fo_outlet/Op_closing');?>';
                    
                }
          });
              },
              Batal: function () {
                
                  $.alert('Data Tidak Disimpan...');
              },
          }
      });

});
</script>


<script type="text/javascript">
  $(document).ready(function(){
     $("#Clossing").click(function(e){ 
        $.LoadingOverlay("show");
      $.ajax({
        type: "POST",
        dataType: 'html',
        url: '<?php echo site_url('Fo_outlet/Na_view_data');?>',
        data: {id: '1'},
        success: function(data){
            e.preventDefault();
             $('.tabelkosong').fadeOut(30);
             $('#tabelada').fadeIn(0,300);  
            $('#tabelada').html(data);
            $('#exampleada').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
          });
           $.LoadingOverlay("hide");
        }
      });
    });
  });
</script>

<?php if($this->session->flashdata('CetakClossing')): ?>
<script type="text/javascript">
  $(document).ready(function() {
    
          window.open("<?php echo base_url(). 'index.php/Fo_outlet/Cetak_closing/';?>?idmaster="+'1' ,"MyTargetWindowName")


  });
  </script>

  <?php endif; ?>



<script type="text/javascript">
  function Kesalahan(text) {
  $.alert({
    title: 'Perhatian!!',
    content: text,
    icon: 'fa fa-warning',
    type: 'orange',
});
}
</script>
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>


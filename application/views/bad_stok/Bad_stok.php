<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>
<script src="<?php echo base_url('assets/js/accounting.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/cleave.min.js') ?>" type="text/javascript"></script>
<style type="text/css">
.modal.modal-wide .modal-dialog {
  width: 60%;
}
.modal-wide .modal-body {
  overflow-y: auto;
}

td.No { display: none
 }


#tallModal .modal-body p { margin-bottom: 900px }
.pull-right {
    float: right;
}

.pull-left {
    float: left;
}

select {
    width: 300px;
}

</style>
    <script type="text/javascript">
      jQuery(document).ready(function($) {
        new Cleave('.input-1', {
           numeral: true, 
           numeralDecimalMark: ',',
           delimiter: '.' 
      });
          });
    </script>
    <script type="text/javascript">
      function doMath()
      {
          var qty = document.getElementById('Qty1').value;
          document.getElementById('Qty').value =  qty.replace(/\./g, "");
      }
    </script>

<!-- Content Header (Page header) -->
<section class="content-header">
   <div class="btn-group btn-breadcrumb">
            <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
            <a  class="btn btn-default  btn-xs active">Bad Stock Add</a>
        </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
             <div class="box box-info">
              <form method="post" id="Simpan" action="<?php echo base_url(). 'index.php/Bad_stok/add_bad_stock'; ?>" enctype="multipart/form-data">
                <div class="box-header">
                  <h3 class="box-title" ><strong>Bad Stock </strong> </h3>
                  <h3 class="box-title" style="float:right" ><strong><?= date('d-m-Y') ?></strong> </h3>
                  <hr style="border-top: 1px solid #8c8b8b; padding: 1px; margin-top: 5px; margin-bottom: 0px;">
                </div>
                <div class="box-body">
              
              <div class="row">
                 <div class="col-md-6">
                   <div class="form-group">
                      <label>Departement</label>
                    <select id="IdDepartement" class="form-control select2"  style="width: 100%;" name="IdDepartement">
                    <option value="0" >Store</option>                 
                      <?php
                          foreach($dataDepaetemen as $u){
                      ?>
                        <option value="<?php echo $u->iddepartement; ?>" ><?php echo $u->departement; ?></option>
                        <?php } ?>
                    </select>
                    <input type="hidden" name="NamaDepartemen" id="NamaDepartemen" value="0">
                  </div>
                </div>
              </div>
               <div class="row">
                 <div class="col-md-6">
                   <div class="form-group">
                      <label>Note</label>
                    <textarea type="text" name="NotePr" id="NotePr"  class="form-control"> </textarea>
                  </div>
                </div>
              </div>
               <div class="row">
                  <div class="col-md-2">
                    <div class="form-group">
                          <label class="control-label">Item Code</label>
                          <input type="text" name="ItemCode" id="ItemCode"  class="form-control">
                     </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                          <label class="control-label">Description</label>
                          <input type="text" name="Description" id="Description"  class="form-control">
                     </div>
                  </div>
                  <div class="col-md-1">
                    <div class="form-group">
                          <label class="control-label">Qty</label>
                          <input type="text" name="Qty1" id="Qty1"  class="form-control input-1"  onkeyup="doMath()">
                          <input type="hidden" name="Qty" id="Qty"  onkeyup="doMath()">
                     </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                          <label class="control-label">Note</label>
                          <input type="text" name="Note" id="Note"  class="form-control">
                           <input type="hidden" name="Unit" id="Unit"  class="form-control">
                           <input type="hidden" name="Price" id="Price"  class="form-control">
                           <input type="hidden" name="Stok" id="Stok"  class="form-control">
                     </div>
                  </div>
                  <div class="col-md-1">
                    <div class="form-group" >
                        <a style="margin-top:25px; margin-right:10px; " class="btn btn-primary btn-flat btn-xs Add"   ><i class='glyphicon glyphicon-arrow-down'></i>  Proses  </a>  
                     </div>
                  </div>
                </div>
                <div class="row">
                <div class="col-md-6" >
                  

                </div>
               
                
                </div>  

          

                      <div class="table-responsive">
                     
                         <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                <th style="display:none">No</th>
                                    <th>Item Code</th>
                                    <th>Description</th>
                                    <th>Note </th>
                                    <th>Qty</th>
                                    <th>Unit</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>
                      </div> 
                       
                       <div class="col-md-12" align="left" style="margin-top :30px;">
                          <div class="form-group" >
                              <a   type="submit" class="btn btn-success btn-flat"  id="simpan1"  ><i class='fa fa-check-square-o'></i>  Simpan  </a>  
                          </div>
                      </div>
        </form>  
      </div>
     </div>
</div>

    <div class="modal modal-wide fade" id="myModal" role="dialog" style="display:none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" title="close" data-dismiss="modal">&times;</button>
                    <h4 align="center" class="modal-title">Item List</h4>
                </div>
                <div class="modal-body">   
                </div>
            </div>
        </div>     
    </div>
        
 

</section>

  

<?php
$this->load->view('template/Js');
$this->load->view('template/Foot');
?>

<script type="text/javascript">
  $(document).ready(function(){
     $("#ItemCode").click(function(e){         
      $.ajax({
        type: "POST",
       url: '<?php echo site_url('Bad_stok/daftar_item');?>',
        data: {id: 'all'  },
        success: function(data){
            e.preventDefault();
            var mymodal = $('#myModal');
            var height = $(window).height() - 200;
            mymodal.find(".modal-body").css("max-height", height);
            mymodal.find('.modal-body').html(data);
            mymodal.modal('show');            
        }
      });
    });
  });
</script>

<script type="text/javascript">
  $(document).ready(function() {
    var t = $('#example1').DataTable();
    var i = 1;
 
    $('.Add').on( 'click', function () {
        var itemcode = $('#ItemCode').val();
        var Description = $('#Description').val();
        var Qty = $('#Qty').val();
        var Note = $('#Note').val();
        var Unit = $('#Unit').val();
        var Price = $('#Price').val();
         var Stok = $('#Stok').val();
        var btndelete =  "<a class='btn btn-danger btn-xs' title='Remove Item'>  <span class=' fa fa-minus-circle' ></span> </a> "
        //alert(itemcode);
        if (itemcode=='' ||itemcode==null  ) {
         File_Kosong()
        }else if (Qty=='' || parseFloat(Qty)<=0) {
         File_Kosong()
        }else{

       var row = t.row.add( [
            i,
            itemcode,
            Description,
            Note,
            Qty,
            Unit,
            btndelete
        ] ).draw(false);
        t.row(row).column(0).nodes().to$().addClass('No');

        $('<input>').attr({
        type: 'hidden',
        class: i,
        name: 'itemcode['+i+']', 
        value: itemcode
        }).appendTo('form');

        $('<input>').attr({
        type: 'hidden',
        class: i,
        name: 'Description['+i+']', 
        value: Description
        }).appendTo('form');

        $('<input>').attr({
        type: 'hidden',
        class: i,
        name: 'Qty['+i+']', 
        value: Qty
        }).appendTo('form');


         $('<input>').attr({
        type: 'hidden',
        class: i,
        name: 'Note['+i+']', 
        value: Note
        }).appendTo('form');

        $('<input>').attr({
        type: 'hidden',
        class: i,
        name: 'Unit['+i+']', 
        value: Unit
        }).appendTo('form');

        $('<input>').attr({
        type: 'hidden',
        class: i,
        name: 'Price['+i+']', 
        value: Price
        }).appendTo('form');

        $('<input>').attr({
        type: 'hidden',
        class: i,
        name: 'Stok['+i+']', 
        value: Stok
        }).appendTo('form');
 
        i++;
         $('#ItemCode').val('');
         $('#Description').val('');
         $('#Qty').val('');
         $('#Qty1').val('');
         $('#Note').val('');
         $('#Unit').val('');
    }
    });

} );
</script>

<script type="text/javascript">
$("#simpan1").click(function() {
        event.preventDefault();
        $.confirm({
          title: 'Confirmation',
          content: 'Are You Sure to Save?',
          type: 'blue',
          buttons: {
              Simpan: function () {
                  $.LoadingOverlay("show");
                  $("#Simpan").submit();
              },
              Batal: function () {
                
                  $.alert('Data Tidak Disimpan...');
              },
          }
      }); 

});
</script>



 <script>
  $('table').on('click', 'a', function(e){
    var t = $('#example1').DataTable();
    var id =$(this).closest('tr').children('td.No').text();
     $('.'+id).remove();
      t.row($(this).closest('tr')).remove().draw( false );
   //$(this).closest('tr').remove().draw(false);

})
  </script>

  <script type="text/javascript">
$('#example1').dataTable({
    "paging": false,
    "ordering": false,
    "searching": false
});
  </script>

  <script type="text/javascript">
function createAutoClosingAlert(selector, delay) {
   var alert = $(selector).alert();
   window.setTimeout(function() { alert.alert('close') }, delay);
}
  </script>




  <script>
  $(function () {
    $(".select2").select2();
     $(".select3").select2();

  });
  </script>


<script type="text/javascript">
   $(document).on('change', '#IdDepartement', function(){
    var NamaDep =$("#IdDepartement option:selected").text(); 
    $("#NamaDepartemen").val('');  
      $("#NamaDepartemen").val(NamaDep);
    });
</script>

   <script type="text/javascript">
  function File_Kosong() {
  $.alert({
    title: 'Caution!!',
    content: 'Invalid Data!',
    icon: 'fa fa-warning',
    type: 'orange',
});
}
</script>


<?php if($this->session->flashdata('kodebs')): ?>
<?php $kodebscetak =  $this->session->flashdata('kodebs');?> 
<script type="text/javascript">
  $(document).ready(function() {
    var kodebs = "<?php echo $kodebscetak;?>"
    
         window.open("<?php echo base_url(). 'index.php/Bad_stok/CetakBS/';?>?nobs="+kodebs ,"MyTargetWindowName")


  });
  </script>

  <?php endif; ?>

    








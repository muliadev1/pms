
<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');

?>
<style type="text/css">
  .box-widget {
 box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
}
.modal.modal-wide .modal-dialog {
  width: 90%;
}

.modal-wide .modal-body {
  overflow-y: auto;
}

#tallModal .modal-body p { margin-bottom: 900px }
.pull-right {
    float: right;
}

.pull-left {
    float: left;
}
</style>

<!-- Content Header (Page header) -->
<section class="content-header" align="center" style="margin-bottom: 24px; ">

    <p class=" badge bg-blue" style="font-size: 20px;">
        Front Office Report
    </p>

</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-xs-12">
    <?php $this->load->view('template/msg_sukses'); ?>


  <div class="col-xs-12">
      <div class="box box-success box-widget">
        <div class="box-header" >
          <h6 class="box-title" style="font-size: 15px;" >CLOSSING REPORT</h6>
        </div>
        <div class="box-body">
    <div class="row">
      <div class="col-sm-4">
         <div class="form-group">
            <label>Operator</label>
              <select id="Operator" class="form-control select2"  style="width: 100%;" name="Operator">
                    <option value="-" selected>--Select Operator--</option>
                     <?php
                          foreach($operator as $op){
                      ?>
                        <option value="<?php echo $op->id_user; ?>" ><?php echo $op->nama_user; ?></option>
                        <?php } ?>
                    </select>
          </div>
        </div>
        <div class="col-sm-4">
         <div class="form-group"  id="groupClos">
            <label>Date Clossing</label>
              <select id="DateClosing" class="form-control select2"  style="width: 100%;" >
                    <option value="-" selected>--Select Date--</option>

                    </select>
          </div>
        </div>


        <div class="col-sm-2">
          <div class="form-group">
             <a style="margin-top: 24px; " class="btn btn-default FOCR"><i class='glyphicon glyphicon-print'></i> Cetak</a>
          </div>
        </div>
      </div>

    </div>
   </div>
  </div>


    <div class="col-xs-12">
      <div class="box box-success box-widget">
        <div class="box-header" >
          <h6 class="box-title" style="font-size: 15px;" >MASTER BILL</h6>
        </div>
        <div class="box-body">
    <div class="row">
      <div class="col-sm-3">
         <div class="form-group">
            <label>Guest</label>
              <select id="Guest" class="form-control select2"  style="width: 100%;" >
                    <option value="-" selected>--Select Guest--</option>
                     <?php
                          foreach($guest as $gu){
                      ?>
                        <option value="<?php echo $gu->idguest; ?>" ><?php echo $gu->nama; ?></option>
                        <?php } ?>
                    </select>
          </div>
        </div>
        <div class="col-sm-3">
         <div class="form-group"  id="groupBill">
            <label>Date Checkout</label>
              <select id="DateBill" class="form-control select2"  style="width: 100%;" >
                    <option value="-" selected>--Select Date--</option>

                    </select>
          </div>
        </div>

        <!-- <div class="col-sm-2">
          <div class="form-group"  >
            <label>Article Payment</label>
              <select id="ArticlePayment" class="form-control select2"  style="width: 100%;" >
                 <option value="-" selected>--Select Article Payment--</option>
                 <?php foreach ($article_payment as $key => $artp): ?>
                    <option value="<?php echo $artp->id; ?>" ><?php echo $artp->articlepayment; ?></option>
                 <?php endforeach ?>
              </select>
          </div>
        </div> -->

        <div class="col-sm-2">
          <div class="form-group"  >
            <label>BILL</label>
              <select id="JenisBill" class="form-control select2"  style="width: 100%;" >
                     <option value="1" selected>Room</option>
                     <option value="2" selected>Comsume</option>
                     <option value="3" selected>Summary</option>


                    </select>
          </div>
        </div>


        <div class="col-sm-2">
          <div class="form-group">
             <a style="margin-top: 24px; " class="btn btn-default BILL"><i class='glyphicon glyphicon-print'></i> Cetak</a>
          </div>
        </div>
      </div>

    </div>
   </div>
  </div>


    <div class="col-xs-12">
      <div class="box box-success box-widget">
        <div class="box-header" >
          <h6 class="box-title" style="font-size: 15px;">ROOM REVENUE BREAKDOWN</h6>
        </div>
        <div class="box-body">
        <div class="row">
          <div class="col-sm-3">
             <div class="form-group">
                    <label>Date From</label>
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text"   value="<?= date('m/d/Y') ?>"     class="form-control pull-right" name= "Tgl1rr" id="Tgl1rr">
                      </div>
                  </div>
            </div>

          <div class="col-sm-3">
             <div class="form-group">
                    <label>Date To</label>
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text"   value="<?= date('m/d/Y') ?>"     class="form-control pull-right" name= "Tgl2rr" id="Tgl2rr">
                      </div>
                  </div>
            </div>
             <div class="col-sm-2">
          <div class="form-group">
             <a style="margin-top: 24px; " class="btn btn-default RR"><i class='glyphicon glyphicon-print'></i> Cetak</a>
          </div>
        </div>
        </div>
      </div>
    </div>
   </div>



     <div class="col-xs-12">
       <div class="box box-success box-widget">
         <div class="box-header" >
           <h6 class="box-title" style="font-size: 15px;">TAX REPORT</h6>
         </div>
         <div class="box-body">
         <div class="row">
           <div class="col-sm-3">
              <div class="form-group">
                     <label>Date From</label>
                       <div class="input-group date">
                         <div class="input-group-addon">
                           <i class="fa fa-calendar"></i>
                         </div>
                         <input type="text"   value="<?= date('m/d/Y') ?>"     class="form-control pull-right" name= "Tgl1tax" id="Tgl1tax">
                       </div>
                   </div>
             </div>

           <div class="col-sm-3">
              <div class="form-group">
                     <label>Date To</label>
                       <div class="input-group date">
                         <div class="input-group-addon">
                           <i class="fa fa-calendar"></i>
                         </div>
                         <input type="text"   value="<?= date('m/d/Y') ?>"     class="form-control pull-right" name= "Tgl2tax" id="Tgl2tax">
                       </div>
                   </div>
             </div>
              <div class="col-sm-2">
           <div class="form-group">
              <a style="margin-top: 24px; " class="btn btn-default tax"><i class='glyphicon glyphicon-print'></i> Cetak</a>
           </div>
         </div>
         </div>
       </div>
     </div>
    </div>





</section><!-- /.content -->

       <div class="modal modal-wide fade" id="ModalCS" role="dialog">
          <div class="modal-dialog modal-sm">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 align="center" class="modal-title">Item List</h4>
                  </div>
                  <div class="modal-body">

                  </div>

              </div>
          </div>
      </div>








<?php
$this->load->view('template/Js');
$this->load->view('template/Foot');
?>
<script type="text/javascript">
   $(document).on('change', '#Operator', function(){
    var idop = $("#Operator").val();
       $.ajax({
            type: "POST",
            dataType:'html',
           url: '<?php echo site_url("Fo_outlet/Select_clossing_byop");?>',
            data: {idop: idop},
            success: function(data){
              $('#Subcategory').remove();
                 $('#groupClos').html(data);
                 $(".select2").select2();
            }
          });

    });
</script>
<script type="text/javascript">
   $(document).on('change', '#Guest', function(){
    var idguest = $("#Guest").val();
   // alert(idguest);
       $.ajax({
            type: "POST",
            dataType:'html',
           url: '<?php echo site_url("Fo_outlet/select_checkout_bill");?>',
            data: {idguest: idguest},
            success: function(data){
              // $('#Subcategory').remove();
              //alert(data)
                 $('#groupBill').html(data);
                 $(".bill").select2();
            }
          });

    });
</script>


<script type="text/javascript">
  $(function () {
  $(".BILL").click(function(){
      var guest = $( "#Guest" ).val();
      var idbill = $( "#DateBill" ).val();
      var jenisbill = $( "#JenisBill" ).val();
         window.open("<?php echo base_url(). 'index.php/Fo_outlet/CetakBill_guest/';?>?jenisbill="+ jenisbill+"&idbill="+ idbill+"&guest="+guest ,"MyTargetWindowName")
  });
});
</script>

<script type="text/javascript">
  $(function () {
  $(".FOCR").click(function(){
      var kode = $( "#DateClosing" ).val();
         window.open("<?php echo base_url(). 'index.php/Fo_outlet/Cetak_closing_bytgl/';?>?kode="+kode ,"MyTargetWindowName")
  });
});
</script>

<script type="text/javascript">
  $(function () {
  $(".RR").click(function(){
      var TglAwal = $( "#Tgl1rr" ).val();
      var TglAkhir = $( "#Tgl2rr" ).val();
      window.open("<?php echo base_url(). 'index.php/Fo_outlet/CetakRR_byTgl/';?>?Status="+ 'Semua' +"&TglAwal="+ TglAwal+"&TglAkhir="+TglAkhir ,"MyTargetWindowName")
  });
  $(".tax").click(function(){
      var TglAwal = $( "#Tgl1tax" ).val();
      var TglAkhir = $( "#Tgl2tax" ).val();
      window.open("<?php echo base_url(). 'index.php/Fo_outlet/Cetak_tax_rpt/';?>?TglAwal="+ TglAwal+"&TglAkhir="+TglAkhir ,"MyTargetWindowName")
  });
});
</script>

 <script>
  $(function () {
    $(".select2").select2();
     $(".select3").select2();

  });
  </script>

    <script>
  $( function() {
    $( "#Tgl1rr" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });
    $( "#Tgl2rr" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });
    $( "#Tgl1tax" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });
    $( "#Tgl2tax" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });
    $( "#TglAwalPer" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });
    $( "#TglAkhirPer" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });
     $( "#PerTanggal" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });
     $( "#TglSO" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });
    $(".select2").select2();


  });

</script>

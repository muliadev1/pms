
<?php 

class M_bad_stok extends CI_Model{	



	function item_view() {   
		 $query = $this->db->query(" SELECT tbitem.* ,subcategory FROM tbitem INNER JOIN `tbsubcategory`
                                ON tbitem.`idsubcategory` =  tbsubcategory.`idsubcategory`    
       "); 
    return $query->result() ;   


   } 

    function get_kodebs($table) { 
        $tahun = date('y'); 
        $bulan = date('m'); 
        $q = $this->db->query("SELECT MAX(RIGHT(kodebs,4)) AS idmax FROM ".$table);
        $kd = ""; //kode awal
        if($q->num_rows()>0){ //jika data ada
            foreach($q->result() as $k){
                $tmp = ((int)$k->idmax)+1; //string kode diset ke integer dan ditambahkan 1 dari kode terakhir
                $kd = "0000".$tmp; //kode ambil 4 karakter terakhir
                $id = substr($kd, -5);
            }
        }else{ //jika data kosong diset ke kode awal
            $kd = $tahun.$bulan."00001";
        }
        $kar = "BS"; //karakter depan kodenya
        //gabungkan string dengan kode yang telah dibuat tadi
        return $kar.$tahun.$bulan.$id;
   }


   function add_bad_stok($Departement,$datamasterbs,$datadetilbs) { 
 //print_r($data);exit();
       $this->db->trans_strict(FALSE);
       $this->db->trans_start();

        $this->db->insert( 'tbbadstok', $datamasterbs);
      $this->db->insert_batch( 'tbbadstokdetil', $datadetilbs); 

     if ($Departement==1) {
        $set = 'stokBO';
       }elseif ($Departement==2) {
        $set = 'stokHK';
       }elseif ($Departement==3) {
         $set = 'stokFO';
       }elseif ($Departement==4) {
          $set = 'stokFB';
       }elseif ($Departement==5) {
        $set = 'stokBO';
       }else{
         $set = 'stok';
       }

      foreach ($datadetilbs as $row) {
        $this->db->set($set, $set.'-'.$row['qty'].'', FALSE);
        $this->db->where('codeitem',$row['codeitem'] );
        $this->db->update('tbitem'); 
        
      }
   
 		   $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else{
            $this->db->trans_commit();
        }      
   }





  function add_transfer_stok_manual($dataPr,$dataDetilPr,$Departement) { 
       //print_r($dataDetilPr);exit();
       $this->db->trans_strict(FALSE);
       $this->db->trans_start();

      $this->db->insert( 'tbsr', $dataPr);
      $this->db->insert_batch( 'tbsrdetil', $dataDetilPr);  
   
       if ($Departement==1) {
        $set = 'stokBO';
       }elseif ($Departement==2) {
        $set = 'stokHK';
       }elseif ($Departement==3) {
         $set = 'stokFO';
       }elseif ($Departement==4) {
          $set = 'stokFB';
       }elseif ($Departement==5) {
        $set = 'stokBO';
       }else{
         $set = 'stok';
       }

      foreach ($dataDetilPr as $row) {
        $this->db->set($set,$set.'+'.$row['qtyorder'].'', FALSE);
        $this->db->where('codeitem',$row['codeitem'] );
        $this->db->update('tbitem'); 

        $this->db->set('stok', 'stok-'.$row['qtyorder'].'', FALSE);
        $this->db->where('codeitem',$row['codeitem'] );
        $this->db->update('tbitem');  
      }
         
       $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else{
            $this->db->trans_commit();
        }      
   } 


  function load_departement(){   
      $query = $this->db->query(" SELECT * from tbdepartement"); 
    return $query->result() ;       
   }

  function Load_BS_all() { 
        $query = $this->db->query("SELECT `kodebs` ,tbdepartement.`departement`,DATE_FORMAT(dateadd,'%d-%m-%Y') AS dateadd ,`description` FROM tbbadstok LEFT JOIN tbdepartement
                                  ON tbbadstok.`iddepartement` =  tbdepartement.iddepartement 
                                   "); 
         return $query->result() ;
  } 

      function Load_BS_bycode($kodebs) { 
        $query = $this->db->query("SELECT tbbadstokdetil.`kodebs`, DATE_FORMAT(dateadd,'%d-%m-%Y') AS dateadd,`unit`,tbbadstokdetil.`description`,`qty`,`price`,
                                    tbbadstok.`description` AS note , tbbadstokdetil.departement, tbbadstokdetil.`note` AS notedetil FROM `tbbadstok`
                                     INNER JOIN `tbbadstokdetil` ON tbbadstok.`kodebs` =  tbbadstokdetil.`kodebs` WHERE tbbadstokdetil.`kodebs` = '$kodebs' "); 
         return $query->result() ; 
      } 

 
  



  

  


  


      



}
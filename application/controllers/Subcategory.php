<?php

class Subcategory extends CI_Controller{

	function __construct(){
		parent::__construct();
		if($this->session->userdata('Kode_userPms')==NULL) {
           redirect('Login');
		}
		$this->load->library('session');
		$this->load->helper(array('form', 'url'));
		$this->load->model(array('M_category'));
		$this->load->model(array('M_subcategory'));
		
		if (!empty($this->session->userdata("active_db"))) {
			$this->load->database($this->session->userdata("active_db"), FALSE, TRUE);
		}
	}

	function index(){
		redirect('Subcategory/Subcategory_view');
	}

	function Subcategory_view(){
		$data['data'] = $this->M_subcategory->Subcategory_view();
		$this->load->view('subcategory/subcategory',$data);
	}


	function Subcategory_add(){
		$data['data'] = $this->M_category->Category_view();
		$this->load->view('Subcategory/Subcategory_add',$data);
	}

	function Subcategory_addDB(){
		$isAset = $this->input->post('isAset');
		if($isAset == '') {
			$isAset = '0';
		}
		$data = array(
			'idcategory' => $this->input->post('idcategory'),
			'subcategory' => $this->input->post('subcategory'),
			'description' => $this->input->post('description'),
			'isaset' => $isAset
		);
		$this->M_subcategory->Subcategory_addDB('tbsubcategory',$data) ;
		$this->session->set_flashdata('msg', 'Add Item Sub Category successfully ...');
		redirect('Subcategory/Subcategory_view');
	}
	function Subcategory_edit($id){
		$data['data'] = $this->M_category->Category_view();
		$data['data2'] = $this->M_subcategory->Subcategory_view_edit($id);
		// print_r($data['data2']); exit();
		$this->load->view('subcategory/subcategory_edit',$data);
	}

	function Subcategory_editDB(){
		$id = $this->input->post('Id');
		$isAset = $this->input->post('isAset');
		if($isAset == '') {
			$isAset = '0';
		}
		// print_r($isAset); exit();
		$data = array(
			'idcategory' => $this->input->post('idcategory'),
			'subcategory' => $this->input->post('subcategory'),
			'description' => $this->input->post('description'),
			'isaset' => $isAset

	);
		$this->M_subcategory->Subcategory_editDB('tbsubcategory',$data,$id) ;
		$this->session->set_flashdata('msg', 'Edit Item Sub Category successfully...');
		redirect('Subcategory/Subcategory_view');


	}


}
?>

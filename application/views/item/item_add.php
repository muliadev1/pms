<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>
<script src="<?php echo base_url('assets/js/accounting.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/cleave.min.js') ?>" type="text/javascript"></script>

    <script type="text/javascript">
      function doMath()
      {
          var price = document.getElementById('price1').value;
          document.getElementById('price').value =  price.replace(/\./g, "");
      }
    </script>
    <script type="text/javascript">
      jQuery(document).ready(function($) {
        new Cleave('.input-1', {
           numeral: true, 
           numeralDecimalMark: ',',
           delimiter: '.' 
      });
          });
    </script>

<!-- Content Header (Page header) -->
<section class="content-header">
   <div class="btn-group btn-breadcrumb">
            <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
             <a href="<?php echo base_url('index.php/Item/Item_view');?>" class="btn btn-default  btn-xs">Item</a>
            <a  class="btn btn-default  btn-xs active">Add Item</a>
        </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
             <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Add New Item</h3>
                </div>
                <div class="box-body">
                    <form method="post"  id="Simpan" action="<?php echo base_url(). 'index.php/Item/Item_addDB'; ?>">
                    <div class="form-group" hidden="hidden">
                            <label class="control-label">Item Code</label>
                            <input type="text" name="codeitem"  data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out item code..."  class="form-control"  placeholder="">
                    </div>
                    <div class="form-group">
                      <label>Sub Category</label>
                      <select class="form-control select2" style="width: 100%;" name="idsubcategory">
                      <?php
                          foreach($data as $u){
                      ?>
                        <option value="<?php echo $u->idsubcategory; ?>" ><?php echo $u->subcategory; ?></option>
                        <?php } ?>
                      </select>
                    </div>

                    <div class="form-group">
                            <label class="control-label">Description</label>
                            <input type="text" name="description" id="description"  data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out description..."  class="form-control"  placeholder="">
                    </div>
                    <div class="form-group">
                            <label class="control-label">Unit</label>
                            <input type="text" name="unit"  data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out unit..."  class="form-control"  placeholder="">
                    </div>
                    <div class="form-group">
                            <label class="control-label">Price</label>
                            <input type="text" name="price1" id="price1" autocomplete="off" data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out price..."  class="form-control input-1"  onkeyup="doMath()">
                            <input type="hidden" name="price" id="price"  onkeyup="doMath()">
                    </div>

                   <div class="form-group">
                      <button type="submit" value="Validate" class="btn btn-default"><i class='glyphicon glyphicon-ok'></i> Save</button>
                    </div>
                    </form>
                </div>
               </div>
         </div>
    </div>


</section>



<script type="text/javascript">
$("#Simpan").submit(function() {
    var price = $('#price').val();
     var description = $('#description').val();
        if (description == ''|| price==''){
            File_Kosong(); return false; 
        }else{
        event.preventDefault();
        $.confirm({
          title: 'Confirmation',
          content: 'Are You Sure to Save?',
           type: 'blue',
          buttons: {
              Simpan: function () {
                  $.LoadingOverlay("show");
                  $("#Simpan").submit();
              },
              Batal: function () {
                
                  $.alert('Data Tidak Disimpan...');
              },
          }
      });
    } 
});
</script>

<?php
$this->load->view('template/Js');
$this->load->view('template/Foot');
?>



<script type="text/javascript">
  function File_Kosong() {
  $.alert({
    title: 'Caution!!',
    content: 'Transaction Is Invalid!',
    icon: 'fa fa-warning',
    type: 'orange',
});
}
</script>

<script>
  $(function () {
    $(".select2").select2();
  });
  </script>
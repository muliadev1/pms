<?php

class M_roomtype extends CI_Model{

	function RoomType_view() {
		$data = $this->db->query("SELECT * FROM tbroomtype WHERE isaktif=1");
		return $data->result();
   }

	 function RoomType_addDB($table,$data) {
			 $this->db->insert($table,$data);
	}

	function RoomType_view_edit($id) {
	 $this->db->select("*");
	 $this->db->where('id',$id);
	 $this->db->from('tbroomtype');
	 return $this->db->get()->result();
	}

	function RoomType_editDB($table,$data,$id) {
 			 $this->db->where('id',$id );
 			 $this->db->update($table,$data);
  }

}
?>

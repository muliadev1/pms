<?php

class Menu extends CI_Controller{

	function __construct(){
		parent::__construct();
		if($this->session->userdata('Kode_userPms')==NULL) {
           redirect('Login');
		}
		$this->load->library('session');
		$this->load->helper(array('form', 'url'));
		$this->load->model(array('M_menu_category','M_menu_subcategory','M_menu'));
		
		if (!empty($this->session->userdata("active_db"))) {
			$this->load->database($this->session->userdata("active_db"), FALSE, TRUE);
		}
	}

	function index(){
		redirect('Menu/Menu_view');
	}

	function Menu_view(){
		$data['data'] = $this->M_menu->Menu_view();
		$this->load->view('menu/menu',$data);
	}


	function Menu_add(){
		$data['data'] = $this->M_menu_subcategory->MenuSubcategory_view();
		$this->load->view('Menu/Menu_add',$data);
	}

	function Menu_addDB(){
		$data = array(
			'idmenusubcategory' => $this->input->post('idmenusubcategory'),
			'menu' => $this->input->post('menu'),
			'description' => $this->input->post('description'),
			'price' => $this->input->post('price'),
			'service' => $this->input->post('service'),
			'tax' => $this->input->post('tax'),
			'finalprice' => $this->input->post('fp'),
			'isaktif' => 1
		);
		$this->M_menu->Menu_addDB('tbmenu',$data);
		$this->session->set_flashdata('msg', 'Add Menu successfully ...');
		redirect('Menu/Menu_view');
	}
	function Menu_edit($id){
		$data['data1'] = $this->M_menu_subcategory->MenuSubcategory_view();
		$data['data2'] = $this->M_menu->Menu_view_edit($id) ;
		$this->load->view('menu/menu_edit',$data);
	}

	function Menu_editDB(){
		$id = $this->input->post('id');
		$data = array(
			'idmenusubcategory' => $this->input->post('idmenusubcategory'),
			'menu' => $this->input->post('menu'),
			'description' => $this->input->post('description'),
			'price' => $this->input->post('price'),
			'service' => $this->input->post('service'),
			'tax' => $this->input->post('tax'),
			'finalprice' => $this->input->post('fp')
	);
		$this->M_menu->Menu_editDB('tbmenu',$data,$id) ;
		$this->session->set_flashdata('msg', 'Edit Menu successfully...');
		redirect('Menu/Menu_view');


	}


}
?>

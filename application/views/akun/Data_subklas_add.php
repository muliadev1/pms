<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>

<!-- Content Header (Page header) -->
<section class="content-header">
   <div class="btn-group btn-breadcrumb">
            <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
             <a href="<?php echo base_url('index.php/Akun/index');?>" class="btn btn-default  btn-xs">Subklasifikasi</a>
            <a  class="btn btn-default  btn-xs active">Tambah Subklasifikasi</a>
        </div>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-xs-12">
     
      <div class="box box-info">
        <div class="box-header">
          <h3 class="box-title">Tambah Subklasifikasi Akun</h3>
          
        </div>
    <!-- /.box-header -->
        <div class="box-body">
         <div id="page-wrapper">

            <div class="container-fluid">

               

                <div class="row">
                    <div class="col-lg-8">
                        <form action="<?php echo base_url(). 'index.php/Akun/AddSubklasDB'; ?>" method="post" id="Simpan">
          <div class="row">
            <div class="col-lg-8">
              

                            <div class="form-group">
                      <label>Subklasifikasi</label>
                    <select  class="form-control select2" name="klas" id="klas" style="width: 100%;" >                 
                          <option value="0">---- Pilih Klasifikasi ----</option>
                          <option value="1">Harta</option>
                          <option value="2">Kewajiban</option>
                          <option value="3">Modal</option>
                          <option value="4">Pendapatan</option>
                          <option value="5">Beban</option>
                    </select>
                  </div>

            </div>
            <div class="col-lg-4">
              <div class="form-group" id="idCls">
                                <label>Id Klasifikasi</label>
                                <input type="text" class="form-control" name="idSub" id="idSub" readonly>
                            </div>
              </div>
            </div>
              <div class="form-group" id="subCls">
                                <label>Sub Klasifikasi</label>
                                <input type="text" class="form-control" name="subKlas" id="subKlas" onchange="function validate('subKlas','subCls')">
                            </div>
              <button type="submit" class="btn btn-default">Simpan</button>
            </form>
                    </div>
                </div>
                <!-- /.row -->


            </div>
            <!-- /.container-fluid -->

        </div>
          
          
          
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->

</section><!-- /.content -->


<script type="text/javascript">
$("#Simpan").submit(function() {
    
        event.preventDefault();
        $.confirm({
          title: 'Confirmation',
          content: 'Anda Yakin Menyimpan Data?',
           type: 'blue',
          buttons: {
              Simpan: function () {
                  $.LoadingOverlay("show");
                  $("#Simpan").submit();
              },
              Batal: function () {
                
                  $.alert('Data Tidak Disimpan...');
              },
          }
      });

});
</script>

<?php
$this->load->view('template/js');
?>
<script type="text/javascript">
   $(document).on('change', '#klas', function(){
    var id =$("#klas").val();
    $.ajax({
          type: "POST",
           url: "<?php echo base_url('');?>/index.php/Akun/SelectIdSubklas/",
          dataType: 'json',
          data: {id: id  },
          success: function (data) {
            $("#idSub").val(data);
          }
      });
   
    });
</script>


<script type="text/javascript">

  // function autoId() {
  //     var valOption = document.getElementById("klas").value;
  //     if (valOption == 0 ) {
  //       document.getElementById("idSub").value = "";
  //       document.getElementById("idCls").className = "form-group has-error";
  //     }else {
  //     $.ajax({
  //         type: "POST",
  //         url: "<?php echo base_url('');?>/index.php/Akun/SelectIdSubklas/"+valOption, 
  //         dataType: 'text',
  //         success: function (data) {
  //          alert(data);
  //           document.getElementById("idSub").value = data;
  //         }
  //     });
  //     document.getElementById("idCls").className = "form-group has-success";
  //     $("#subKlas").focus();
  //   }
  //   validate("klas","opCls");
  // }

  function validate(id,cls) {
    var isId = document.getElementById(id);
    var isCls = document.getElementById(cls);
    if (isId.value == 0 || isId.value == null || isId.value == "") {
      isCls.className = "form-group has-error";
      return false;
    }else {
      isCls.className = "form-group has-success";
      return true;
    }
  }

  function validateForm(){
    var val1 = validate("klas","opCls");
    var val3 = validate('subKlas','subCls');
    if (val1 == false || val3 == false ) {
      alert("Data Belum Lengkap!");
      return false;
    }
  }
</script>

<?php
$this->load->view('template/foot');
?>
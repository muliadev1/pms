<?php

class Roomtype extends CI_Controller{

	function __construct(){
		parent::__construct();
		if($this->session->userdata('Kode_userPms')==NULL) {
           redirect('Login');
		}
		$this->load->library('session');
		$this->load->helper(array('form', 'url'));
		$this->load->model(array('M_roomtype'));
		date_default_timezone_set('Asia/Hong_Kong');
		
		if (!empty($this->session->userdata("active_db"))) {
			$this->load->database($this->session->userdata("active_db"), FALSE, TRUE);
		}
	}

	function index(){
		redirect('Roomtype/RoomType_view');
	}

	function RoomType_view(){
		$data['data'] = $this->M_roomtype->RoomType_view();
		$this->load->view('roomtype/roomtype',$data);
	}


	function RoomType_add(){
		$this->load->view('roomtype/Roomtype_add');
	}

	function RoomType_addDB(){
		$data = array(
			'roomtype' => $this->input->post('roomtype'),
			'desc' => $this->input->post('description'),
			'rate' => $this->input->post('rate'),
			'isaktif' => '1'
		);
		$this->M_roomtype->RoomType_addDB('tbroomtype',$data) ;
		$this->session->set_flashdata('msg', 'Add Item Room Type successfully ...');
		redirect('Roomtype/RoomType_view');
	}

	function RoomType_edit($id){
		$data['data'] = $this->M_roomtype->RoomType_view_edit($id) ;
		$this->load->view('roomtype/Roomtype_edit',$data);
	}

	function RoomType_editDB(){
		$id = $this->input->post('Id');
		$data = array(
			'roomtype' => $this->input->post('roomtype'),
			'desc' => $this->input->post('description'),
			'rate' => $this->input->post('rate')
	);

		$this->M_roomtype->RoomType_editDB('tbroomtype',$data,$id) ;
		$this->session->set_flashdata('msg', 'Edit Room Type successfully...');
		redirect('Roomtype/RoomType_view');
	}

}
?>

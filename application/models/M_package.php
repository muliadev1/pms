<?php

class M_package extends CI_Model{

	function Package_view() {
		$data = $this->db->query("SELECT * FROM `tbpackage` GROUP BY id ");
		return $data->result();
   }
   function get_idpackage() {
		$data = $this->db->query("SELECT id FROM tbpackage ORDER BY id DESC LIMIT 1")->row();
		$idx= $data;
		$idnew = $idx->id + 1;
		return $idnew;
   }

	 function package_addDB($data) {
			  $this->db->insert_batch( 'tbpackage', $data);
			
	}

	function package_view_edit($id) {
	 $this->db->select("*");
	 $this->db->where('id',$id);
	 $this->db->from('tbpackage');
	 return $this->db->get()->result();
	}

	function Company_editDB($table,$data,$id) {
 			 $this->db->where('idcompany',$id );
 			 $this->db->update($table,$data);
  }

}
?>

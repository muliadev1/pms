<?php

class Receive_item extends CI_Controller{
	private $IdOperator;
	private $Operator;
	private $IdAset;
	
	function __construct(){
		parent::__construct();
		if($this->session->userdata('Kode_userPms')==NULL) {
           redirect('Login');
		}
		$this->load->library('session');
		$this->load->helper(array('form', 'url'));
	$this->load->model(array('M_purchase_order','M_category','M_item','M_subcategory','M_store_requisition','M_receive_order','M_aset'));
		$this->IdOperator = $this->session->userdata('Kode_userPms');
		$this->Operator = $this->session->userdata('Nama_userPms');
		$this->IdAset = $this->session->userdata('KodeKategoriAset');
		
		if (!empty($this->session->userdata("active_db"))) {
			$this->load->database($this->session->userdata("active_db"), FALSE, TRUE);
		}
	}

	function index(){
		redirect('Receive_item/Purchase_order_list');
	}

	function Purchase_order_list(){
		$data['data'] = $this->M_purchase_order->load_po();
		$this->load->view('receive_item/Purchase_order_list',$data);
	}

	function receive_item($nopo){
		$data['data'] = $this->M_purchase_order->load_po_by_kode($nopo);
		$data['dataitem'] = $this->M_purchase_order->load_po_item_by_kode($nopo);
		$data['dataDepaetemen'] = $this->M_receive_order->load_departement();
		$this->load->view('receive_item/receive_item',$data);
	}

	function select_item_by_itemcode_kodepo(){
		$itemcode = $this->input->post('itemcode');
		$kodepo = $this->input->post('kodepo');
		$data = $this->M_purchase_order->select_item_by_itemcode_kodepo($itemcode,$kodepo);
		echo(json_encode($data));
		//$this->load->view('Receive_item/receive_item',$data);
	}

	function add_Ro(){
		$Total = 0;
		$IdDepartement = $this->input->post('IdDepartement'); 
		$NamaDepartemen = $this->input->post('NamaDepartemen');
		$dp =$this->input->post('DP');
		$remain =$this->input->post('Remain');
		date_default_timezone_set('Asia/Hong_Kong');
		$DueDate = $this->input->post('DueDate');
		$DueDate1 = DateTime::createFromFormat('m/d/Y',$DueDate);
		$DueDate2 = $DueDate1->format("Y-m-d");
		$iscredit =$this->input->post('IsCash');
		$discount =$this->input->post('Disc');
		$totalafterdisc = $Total - $discount;
		 $noreceipt =$this->input->post('receiptno');
		 $vendor = $this->input->post('vendor');
		  $idvendor = $this->input->post('idvendor');
		 $Subtotal = $this->input->post('Subtotal');
		 $Stok = $this->input->post('Stok');
		 $Unit = $this->input->post('Unit');
		 $Description = $this->input->post('Description');
		 $Qty = $this->input->post('Qty');
		 $Price = $this->input->post('Price');
		 $itemcode = $this->input->post('itemcode');
		 $kodePo = $this->input->post('kodepo');
		 $kodeRo =$this->M_receive_order->get_kodero('tbro');
		 $kodeSr =$this->M_store_requisition->get_kodesr('tbsr');

		 //print_r($iscredit);exit();
		
		 if ($iscredit=='1') {
			$isinvoicing ='1';
			$ispaid = '0';
		}else{
			$isinvoicing ='0';
			$ispaid = '0';
		}

		$itemcode1 = array_combine(range(1, count($itemcode)), array_values($itemcode));
		$Price1 = array_combine(range(1, count($Price)), array_values($Price));
		$Qty1 = array_combine(range(1, count($Qty)), array_values($Qty));
		$Description1 = array_combine(range(1, count($Description)), array_values($Description));
		$Unit1 = array_combine(range(1, count($Unit)), array_values($Unit));
		$Stok1 = array_combine(range(1, count($Stok)), array_values($Stok));
		$Subtotal1 = array_combine(range(1, count($Subtotal)), array_values($Subtotal));

			$Jumlah = count($Stok);
			for ($i=1; $i <= $Jumlah ; $i++) { 
			 	 $dataDetilRO[] = array('kodero' => $kodeRo,
					 				'itemcode' => $itemcode1[$i],
					 				'price' => $Price1[$i],
					 				'qtyorder' => $Qty1[$i],
					 				'qty' => $Qty1[$i],
					 				'description' => $Description1[$i],
					 				'unit' => $Unit1[$i],
					 				'stok' => $Stok1[$i],
					 				'subtotal' => $Subtotal1[$i]
					 		 );
			 	 $Total+= $Subtotal1[$i];
			 } 

 			$totalafterdisc = $Total - $discount;
	 		$dataRO = array('kodero' => $kodeRo,
			 		'kodepo' => $kodePo,
			 		'vendor' => $vendor,
					'datereceive' => date('Y-m-d H:i:s'),
					'idvendor' => $idvendor,
					'vendor' => $vendor,
					'idoperator' => $this->IdOperator,
					'operator' => $this->Operator,
					'noreceipt' => $noreceipt,
					'isinvoicing'=> $isinvoicing,
					'ispaid'=> $ispaid,
					'total' => $Total,
					'disc' => $discount,
					'totalafterdisc' => $totalafterdisc,	
					'iscredit' => $iscredit,
					'duedate' => $DueDate2,
					'dp' => $dp,
					'remain' => $remain,
					'iddepartement' => $IdDepartement			
	 			);
	 		//SR
	 		$datamastersr = array('kodesr' => $kodeSr,
	 				 		'iddepartement' => $IdDepartement,
	 				 		'departement' => $NamaDepartemen,
	 						'dateadd' => date('Y-m-d H:i:s'),
	 						'idoperator' => $this->IdOperator,
	 						'description' => 'Transfer Direct By System'	
	 		 			);



		
				$Jumlah = count($Stok);
				for ($i=1; $i <= $Jumlah ; $i++) { 
				 	 $dataDetilsr[] = array('kodesr' => $kodeSr,
						 				 'iddepartement' => $IdDepartement,
						 				 'departement' => $NamaDepartemen,
						 				'codeitem' => $itemcode1[$i],
						 				'price' => $Price1[$i],
						 				'qtyorder' => $Qty1[$i],
						 				'description' => $Description1[$i],
						 				'unit' => $Unit1[$i],
						 				'note' => 'Transfer Direct By System',
						 				'currstok' => $Stok1[$i],
						 				'idoperator' => $this->IdOperator
						 		 );
				 } 

	 		//History Price
		 		$Jumlah = count($Stok);
				for ($i=1; $i <= $Jumlah ; $i++) { 
					$dataHPP = $this->M_receive_order->load_price_item($itemcode1[$i]);
					$StokLamaHPP = $dataHPP->stok;

					$PriceLamaHPP= $dataHPP->price;
					$StokBaruHPP = $dataHPP->stok + $Qty1[$i];
					$PriceBaruHPP=  $Price1[$i];
					//print_r($PriceBaruHPP);exit();
					$HargaBaruHPP = (($StokLamaHPP*$PriceLamaHPP) + ($StokBaruHPP*$PriceBaruHPP)) /($StokLamaHPP+$StokBaruHPP);
						$dataHistoryPrice[] = array('codeitem' => $itemcode1[$i],
						 				'price' => $PriceLamaHPP,
						 				'newprice' => $Price1[$i],
						 				'date' => date('Y-m-d H:i:s'),
						 				'idvendor' => $idvendor,
						 				'pricehpp' => $HargaBaruHPP,
						 				'kodero' => $kodeRo
						 		 );	
				 }
				 //Aset
		 		for ($i=1; $i <= $Jumlah ; $i++) { 
		 			$isaset = $this->M_purchase_order->Load_subcategory_byiditem($itemcode1[$i]);
		 			if ($isaset==1) {
		 				$codeaset = $this->M_purchase_order->Load_aset_byiditem($itemcode1[$i]); 
		 				 $dataDetilaset[] = array('codeaset' => $codeaset,
						 				 'iddepartement' => $IdDepartement,
						 				 'departement' => $NamaDepartemen,
						 				 'kondisiawal' => 'Baik',
						 				 'kondisiakhir' => 'Baik',
						 				 'qty' => $Qty1[$i],
						 				 'note' => 'IN',
						 				 'inputdate' => date('Y-m-d H:i:s'),
						 				 'idoperator' => $this->IdOperator,
						 				 'operator' => $this->Operator
						 		 );
		 				
		 			}
				 	
				 }
				 // ($table,$data,$Departement) 
				 
	 	$this->M_receive_order->Ro_add($dataRO,$dataDetilRO,$kodePo,$dataHistoryPrice);
		$this->M_store_requisition->add_transfer_stok_direct($IdDepartement,$dataDetilRO,$datamastersr,$dataDetilsr);
		if (!empty($dataDetilaset)) {
			$this->M_aset->Add_detilAset_byorder('tbasetdetil',$dataDetilaset,$IdDepartement); 
		} 
	 	$this->session->set_flashdata('msg', ' Add Receive Item successfully... ');
	 	$this->session->set_flashdata('kodero', $kodeRo);
		redirect('receive_item/Purchase_order_list');

	}

	


		



}
?>

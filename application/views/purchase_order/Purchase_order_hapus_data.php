
          <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr >
                                        <th  style="display: none;"  >No</th>
                                        <th >PO No.</th>
                                        <th >Date</th>
                                        <th >Vendor</th>
                                        <th style="display: none;" >Description</th>
                                         <th style="display: none;" >Catatan</th>
                                        <th >Amount</th>
                                        <th >Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i=1;
                                    foreach($data as $u){
                                      

                                    ?>

                                    <tr  class='odd gradeX context'>
                                        <td  style="display: none;"><?php echo $i ?></td>
                                        <td class="Kodepo"><?php echo $u->kodepo ?></td>
                                        <td class="Date"><?php echo $u->dateorder1?></td>
                                        <td class="Vendor"><?php echo $u->vendor?></td>
                                        <td  style="display: none;" class="description"><?php echo $u->description?></td>
                                        <td  style="display: none;" class="catatan"><?php echo $u->catatan?></td>
                                        <td class="Total"><?php echo number_format($u->total, 0, '.', '.') ?></td>

                                         <td align="center">
                       <a class="btn btn-danger btn-xs Hapus"  title="Hapus">  <span class="fa fa-fw fa-times" ></span> </a>
                       <a class="btn btn-info btn-xs ViewDetail"  title="lihat Detail">  <span class="fa fa-fw fa-list" ></span> </a>

                                        </td>
                                    </tr>
                                    <?php $i++;} ?>
                                </tbody>
                            </table>
         

  <div class="modal modal-wide fade" id="myModalNew" role="dialog" style="display:none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" title="close" data-dismiss="modal">&times;</button>
                    <h4 align="center" class="modal-title">Detail PO</h4>
                    <table class="table" style="background-color: #00acd6; border: 1px solid black; "  >
                <tr>
                  <td width="10%" style="border: 1px solid white; ">No PO </td>
                  <td  width="25%" id ="Kodepom" style="background-color: #00acd6; border: 1px solid white; " ><strong> <span></span> </strong> </td>
                  <td width="10%" style="border: 1px solid white; ">Vendor</td>
                  <td  width="25%" id ="Vendorm" style="background-color: #00acd6; border: 1px solid white; " ><strong> <span></span> </strong> </td>
                  <td width="10%" style="border: 1px solid white; ">Total</td>
                  <td  width="20%" id ="Totalm" style="background-color: #00acd6; border: 1px solid white; " ><strong> <span></span> </strong> </td>
                </tr>
              </table>
                </div>
              
                <div class="modal-body">   
                </div>
            </div>
        </div>     
    </div>




<script type="text/javascript">
  $(document).ready(function(){
     $(".ViewDetail").click(function(e){ 
      var kode = $(this).closest('tr').children('td.Kodepo').text(); 
       var total = $(this).closest('tr').children('td.Total').text(); 
       var vendor = $(this).closest('tr').children('td.Vendor').text();     
      $.ajax({
      type: "POST",
       url: '<?php echo site_url('Purchase_order/purchase_order_list_veri_detil');?>',
        data: {kode:kode},
        success: function(data){
            e.preventDefault();
            var mymodal1 = $('#myModalNew');
            mymodal1.modal({backdrop: 'static', keyboard: false}) 
            var height = $(window).height() - 200;
            mymodal1.find(".modal-body").css("max-height", height);
             $("#Kodepom span").text(kode);
              $("#Totalm span").text(total);
               $("#Vendorm span").text(vendor);
            mymodal1.find('.modal-body').html(data);
            mymodal1.modal('show');            
        }
      });
    });
  });
</script>




<script type="text/javascript">
$(".Hapus").click(function() {
        event.preventDefault();
         var kode = $(this).closest('tr').children('td.Kodepo').text(); 
        $.confirm({
          title: 'Confirmation',
          content: 'Are You Sure to Delete PO?',
           type: 'blue',
          buttons: {
              Simpan: function () {
                    $.ajax({
                    type: "POST",
                   url: '<?php echo site_url('Purchase_order/purchase_order_add_hapus');?>',
                    data: {kode:kode  },
                    success: function(data){
                        // window.location=data;
                         $("#Proses").trigger('click'); 
                          $.alert('Received PO Successfully...');          
                    }
                 });      
              },
              Batal: function () {
                
                  $.alert('Data Tidak Disimpan...');
              },
          }
      });

});
</script>


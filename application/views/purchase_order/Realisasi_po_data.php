
      <form method="post" id="Simpan" action="<?php echo base_url(). 'index.php/Stok_opname/add_proses_opname'; ?>">
<!--       <input type="hidden" name="Istok"  value="<?php echo $departement['istok'] ;?>">
      <input type="hidden" name="Set"  value="<?php echo $departement['set'] ;?>">
      <input type="hidden" name="Departement"  value="<?php echo $departement['dep'] ;?>">
      <input type="hidden" name="iddepartement"  value="<?php echo $departement['iddep'] ;?>"> -->
        <div class="table-responsive">
          <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr >
                                       <th >No</th>
                                        <th >Tanggal</th>
                                           <?php if ($periode['IdVendor'] ==0) { ?>
                                           <th >Vendor</th>
                                            <?php }?>
                                        <th >No PO</th>
                                        <th >PO Value</th>
                                        <th >No Receive</th>                         
                                        <th >Receive Value</th>
                                        <th >History</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i=1;$totalqtyorder = 0;$totalqtyreceive = 0;
                                    foreach($konten as $u){
                                      //codeaset,description, baik,kurangbaik, $set as masuk, out
                                    ?>
                                    <tr  class='odd gradeX context'>
                                       <td ><?php echo $i ?></td> 
                                        <td ><?php echo $u->datereceive ?></td>
                                         <?php if ($periode['IdVendor'] ==0) { ?>
                                        <td><?php echo $u->vendor?></td>
                                        <?php }?>
                                        <td class="kode"><?php echo $u->kodepo?></td>
                                         <td align="right" style="background-color: #8CFF8C"><?php echo number_format($u->totalpo, 0, '.', '.')?></td>
                                        <td><?php echo $u->kodero?></td>
                                         <td align="right" style="background-color: #70B8FF"><?php echo number_format($u->totalro, 0, '.', '.')?></td>
                                        <td align="center"> 
                                        <a class="btn btn-info btn-xs ViewDetail" title="View History Price"   style="cursor:pointer" >  <span class="fa fa-fw fa-list " ></span> </a>
                                        </td>
                                    </tr>
                                    <?php $i++;  $totalqtyorder+=$u->totalpo;  $totalqtyreceive+=$u->totalro;} ?>
                                </tbody>
                                <tfoot>
                                   <?php if ($periode['IdVendor'] !=0) { ?>
                                  <tr>
                                    <td colspan="3" style="background-color: #F0F0F0">TOTAL</td>
        <td align="right" style="background-color: #8CFF8C" ><strong><?php echo number_format($totalqtyorder, 0, '.', '.')?></strong></td>
                                     <td style="background-color: #F0F0F0">&nbsp;</td>
       <td align="right" style="background-color: #70B8FF"><strong><?php echo number_format($totalqtyreceive, 0, '.', '.')?></strong></td>
                                      <td style="background-color: #F0F0F0">&nbsp;</td>
                                  </tr>
                                   <?php }else{ ?>
                                    <td colspan="4" style="background-color: #F0F0F0">TOTAL</td>
        <td align="right" style="background-color: #8CFF8C" ><strong><?php echo number_format($totalqtyorder, 0, '.', '.')?></strong></td>
                                     <td style="background-color: #F0F0F0">&nbsp;</td>
       <td align="right" style="background-color: #70B8FF"><strong><?php echo number_format($totalqtyreceive, 0, '.', '.')?></strong></td>
                                      <td style="background-color: #F0F0F0">&nbsp;</td>
                                   <?php } ?>
                                </tfoot>
                            </table>
          </div>



        </div><!-- /.box-body -->
        </form>
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->


<script type="text/javascript">
     $(".ViewDetail").click(function(e){ 
     //var dep = $('#IdDepartement').val();  
      var kodepo = $(this).closest('tr').children('td.kode').text(); 
      // var namadep =  $("#IdDepartement option:selected").text();
      //  var namaaset = $(this).closest('tr').children('td.desc').text();     
      $.ajax({
        type: "POST",
       url: '<?php echo site_url('Purchase_order/realisasi_po_view_data_detil');?>',
        data: {kodepo: kodepo },
        success: function(data){
            e.preventDefault();
            var mymodal1 = $('#myModalNew');
            mymodal1.modal({backdrop: 'static', keyboard: false}) 
            var height = $(window).height() - 200;
            mymodal1.find(".modal-body").css("max-height", height);
             // $("#CodeAsetm span").text(namaaset);
             //  $("#Departementm span").text(namadep);
            mymodal1.find('.modal-body').html(data);
            mymodal1.modal('show');            
        }
      });
    });
</script>


<script type="text/javascript">
  function File_Kosong() {
  $.alert({
    title: 'Caution!!',
    content: 'Transaction Is Invalid!',
    icon: 'fa fa-warning',
    type: 'orange',
});
}
</script>

<script>
  $(function () {
    
    $('#example1').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
  });
</script>

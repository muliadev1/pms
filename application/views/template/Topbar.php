</head>


<body class="skin-blue animate" id="sidebar">
    <!-- Site wrapper -->
    <div  class="wrapper">



        <header class="main-header">
            <a href="#" class="logo" style="background-color:#0bc3c3;"><b>P</b>MS</a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top navbar3" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- Messages: style can be found in dropdown.less-->
                       

                         <li class="dropdown tasks-menu">
                            <a href="#" class="dropdown-toggle"  data-toggle="dropdown">
                                <i style="font-size: 18px;" class="fa fa-sliders"></i>
                                <?= $this->session->userdata('role_aktifPms') ?>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header" align="center" >Pilih Role..</li>
                                <li>
                                    <!-- inner menu: contains the actual data -->
                                    <ul class="menu">
                                    <?php
                                          foreach($this->session->userdata('rolePms') as $role) {  
                                        ?>
                                        <li><!-- Task item -->
                                        
                                             <a href="#">
                                                <h3>
                                                   <li href=""  class="change" idK="<?php echo $role->id_role; ?>" ><strong><?php echo $role->nama_role ?> </strong></li> 
                                                   
                                                </h3>
                                                
                                            </a>
                                        </li><!-- end task item -->
                                        <?php
                                        }
                                        ?>
                                    </ul>
                                </li>
                                
                            </ul>
                        </li>

                        <!-- Notifications: style can be found in dropdown.less -->
                        
                        <!-- Tasks: style can be found in dropdown.less -->
                       
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="<?php echo base_url('assets/AdminLTE-2.0.5/dist/img/usertop.png') ?>" class="user-image" alt="User Image"/>
                                <span class="hidden-xs"><?php echo $this->session->userdata('Nama_userPms')?></span>
                            </a>

                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header" style="background-color:#009cff;">
                                    <img src="<?php echo base_url('assets/AdminLTE-2.0.5/dist/img/usertop.png') ?>" class="img-circle" alt="User Image" />
                                    <p>
                                        <?php echo $this->session->userdata('Nama_userPms')?>
                                        <small><?php echo $this->session->userdata('Jabatan')?></small>
                                    </p>
                                </li>

                                <!-- Menu Body -->
                               <!--  <li class="user-body">
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Followers</a>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Sales</a>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Friends</a>
                                    </div>
                                </li> -->
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="#" class="btn btn-default btn-flat">Ubah Password</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="<?php echo base_url('index.php/Login/logout'); ?>" class="btn btn-default btn-flat">Keluar</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                         
                    </ul>
                </div>
            </nav>

    
   
        </header>

        <!-- =============================================== -->

        
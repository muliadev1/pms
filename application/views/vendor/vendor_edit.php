<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>

<!-- Content Header (Page header) -->
<section class="content-header">
   <div class="btn-group btn-breadcrumb">
            <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
             <a href="<?php echo base_url('index.php/Vendor/Vendor_view');?>" class="btn btn-default  btn-xs">Vendor</a>
            <a  class="btn btn-default  btn-xs active">Edit Vendor</a>
        </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
             <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Edit Vendor </h3>
                </div>
                <div class="box-body">
                    <form method="post" id= "Simpan" action="<?php echo base_url(). 'index.php/Vendor/Vendor_editDB'; ?>">
                    <div class="form-group">
                      <div class="form-group">
                              <label class="control-label">Vendor</label>
                              <input type="text"  value="<?php echo $data2[0]->vendor; ?>"  name="vendor"  id="vendor"  data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out vendor name..."  class="form-control"  placeholder="">
                              <input type="hidden"  value="<?php echo $data2[0]->idvendor; ?>"  name="Id"  data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out vendor name..."  class="form-control"  placeholder="">
                      </div>

                    </div>
                    <div class="form-group">
                            <label class="control-label">Address</label>
                            <input type="text" value="<?php echo $data2[0]->address; ?>"  name="address"  data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out vendor address..."  class="form-control"  placeholder="">
                    </div>
                    <div class="form-group">
                            <label class="control-label">Phone</label>
                            <input type="text" name="phone" value="<?php echo $data2[0]->phone; ?>" data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out vendor phone number..."  class="form-control"  placeholder="">
                    </div>
                    <div class="form-group">
                            <label class="control-label">Email</label>
                            <input type="email" name="email" value="<?php echo $data2[0]->email; ?>"  data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out vendor email..."  class="form-control"  placeholder="">
                    </div>
                    <div class="form-group">
                            <label class="control-label">Contact Person</label>
                            <input type="text" name="contact" value="<?php echo $data2[0]->contact; ?>" data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out contact person..."  class="form-control"  placeholder="">
                    </div>
                   <div class="form-group">
                      <button type="submit" value="Validate" class="btn btn-default"><i class='glyphicon glyphicon-ok'></i> Save</button>
                    </div>
                    </form>
                </div>
               </div>
         </div>
    </div>


</section>



<script type="text/javascript">
$("#Simpan").submit(function() {
    var vendor = $('#vendor').val();
        if (vendor == ''){
            File_Kosong(); return false; 
        }else{
        event.preventDefault();
        $.confirm({
          title: 'Confirmation',
          content: 'Are You Sure to Save?',
           type: 'blue',
          buttons: {
              Simpan: function () {
                  $.LoadingOverlay("show");
                  $("#Simpan").submit();
              },
              Batal: function () {
                
                  $.alert('Data Tidak Disimpan...');
              },
          }
      });
    } 

});
</script>

<?php
$this->load->view('template/Js');
$this->load->view('template/Foot');
?>



<script type="text/javascript">
  function File_Kosong() {
  $.alert({
    title: 'Caution!!',
    content: 'Transaction Is Invalid!',
    icon: 'fa fa-warning',
    type: 'orange',
});
}
</script>

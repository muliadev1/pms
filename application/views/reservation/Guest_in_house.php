<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>

<section class="content-header">
    <div class="btn-group btn-breadcrumb">
      <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
      <a  class="btn btn-default  btn-xs active">Guest In House</a>
    </div>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Guest In House</h3>
          <div class="row" align="right">
            <div class="col-md-12">
            <a class="btn btn-default GH"><i class='glyphicon glyphicon-print'></i> Print</a>
          </div>
        </div>

        </div>
    <!-- /.box-header -->
        <div class="box-body">
        <div class="table-responsive">
          <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr >
                                        <th >No</th>
                                        <th >#Room</th>
                                        <th >Guest Name</th>
                                        <th >R. Type</th>
                                        <th >Check In</th>
                                        <th >Check Out</th>
                                        <th >Night</th>
                                        <th >Adult</th>
                                        <th >Child</th>
                                        <th >Segment</th>
                                        <th >Company</th>
                                        <th >Nationality</th>
                                        <th >Notes</th>
                                        <th >Message</th>
                                        <th >User</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $No = 1; foreach($data as $u){
                                    ?>
                                    <tr  class='odd gradeX context'>
                                        <td><?php echo $No ?></td>
                                        <td><?php echo $u->roomname?></td>
                                        <td><?php echo $u->saluation?> <?php echo $u->firstname?> <?php echo $u->lastname?></td>
                                        <td><?php echo $u->Expr1?> - <?php echo $u->desc?></td>
                                        <td><?php echo $u->checkin?></td>
                                        <td><?php echo $u->checkout?></td>
                                        <td><?php echo $u->duration?></td>
                                        <td><?php echo $u->adult?></td>
                                        <td><?php echo $u->child?></td>
                                        <td><?php echo $u->Expr2?></td>
                                        <td><?php echo $u->Expr3?></td>
                                        <td><?php echo $u->Expr4?></td>
                                        <td><?php echo $u->notes?></td>
                                        <td><?php echo $u->msg?></td>
                                        <td><?php echo $u->nama_user?></td>

                                    </tr>
                                    <?php $No++; } ?>
                                </tbody>
                            </table>
          </div>



        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->

</section><!-- /.content -->


<?php
$this->load->view('template/Js');
$this->load->view('template/Foot');
?>
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>

<script>
$( function() {
$( "#tglAwal" ).datepicker({
autoclose: true,
dateFormat: 'yy/mm/dd'
});
});
</script>

<script>
$( function() {
$( "#tglAkhir" ).datepicker({
autoclose: true,
dateFormat: 'yy/mm/dd'
});
});
</script>

<script type="text/javascript">
  $(function () {  
  $(".GH").click(function(){
      var TglAwal = $( "#tglAwal" ).val();
      var TglAkhir = $( "#tglAkhir" ).val();
      window.open("<?php echo base_url(). 'index.php/Reservation/CetakLapGH/';?>?Status="+ 'Semua',"MyTargetWindowName")
  }); 
});
</script>


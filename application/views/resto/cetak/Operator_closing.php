
<?php echo $header?>

<style type="text/css">
  .TabelKonten tr td {
    padding-right: 7px;
    padding-left:  7px;
    font-size: 12px;
  }
</style>
<table class="TabelKonten"  border="0" style="border-collapse: collapse; border-color:#000000; padding-bottom : 50px ;"  width="100%" >
<tr>
  <td colspan="2" align="center" style="font-size: 14px;"> <strong> SUMMARY OF SALES</strong></td>
</tr>
<tr>
  <td width="10%"> <strong>Operator</strong></td>
  <td>:<?php echo $dataclos->tanggaltrx ?></td>
</tr>
<tr>
  <td width="10%"> <strong> Date Clossing</strong></td>
  <td>:<?php echo $dataclos->nama_user ?></td>
</tr>
</table>
<br>

<table class="TabelKonten"  border="1" style="border-collapse: collapse; border-color:#000000; padding-top : 50px;"  width="100%" >                      <thead>
                                    <tr >
                                        <th >NO</th>
                                        <th >ID</th>
                                        <th >Room NO</th>
                                        <th >Persons</th>
                                        <th >Food</th>
                                        <th >Beverage</th>                         
                                        <th >Other</th>
                                        <th >Total</th>
                                        <th >Payment</th>
                                        <th >Remark</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <?php $no=1; $gTotalFood=0; $gTotalBev=0;  foreach ($master as $mas){  ?>

                                  <tr>
                                    <td><?php echo $no ?></td>
                                    <td><?php echo $mas->trxid ?></td>
                                    <td><?php echo $mas->roomname ?></td>
                                     <td><?php echo $mas->nama ?></td>
                            <?php  $jumlahfood = $this->M_resto->Get_trx_menu_detail_category_cetak_closing($mas->trxid,'FOOD');?>
                              <?php   $jumlahbev = $this->M_resto->Get_trx_menu_detail_category_cetak_closing($mas->trxid,'BEVERAGE');
                                $jumlahot = $this->M_resto->Get_trx_menu_detail_category_cetak_closing($mas->trxid,'OTHER');
                              //print_r( $jumlahbev);exit();
                              ?>
                            
                                <?php if (!empty($jumlahfood->total)){ 
                                  $TotalFood=$jumlahfood->total; 
                                }else{ 
                                $TotalFood='0';
                                } ?>

                                <?php if (!empty($jumlahbev->total)){ 
                                  $TotalBev=$jumlahbev->total; 
                                }else{ 
                                $TotalBev='0';
                                } ?>

                                <?php if (!empty($jumlahot->total)){ 
                                  $TotalOt=$jumlahot->total; 
                                }else{ 
                                $TotalOt='0';
                                } ?>

                                   <td align="right"><?php echo number_format($TotalFood, 2, ',', '.') ?></td>
                                   <td  align="right"><?php echo number_format($TotalBev, 2, ',', '.') ?></td>
                                   <td  align="right"><?php echo number_format($TotalOt, 2, ',', '.') ?></td>
                                   <td  align="right"><?php echo number_format($TotalFood+$TotalBev+$TotalOt, 2, '.', '.') ?></td>


                                   <?php $payment = $this->M_resto->Get_payment_clossing($mas->trxid);  ?>
                                   
                                     <td>
                                  <?php foreach ($payment as $pay ){?>
                                      <?php if ($pay->idarticlepayment!=-1){ 
                                           echo $pay->initial.':'.' '.number_format($pay->amount, 2, ',', '.') ;
                                         }else{
                                          echo $pay->paymentstatus.':'.' '.number_format($pay->amount, 2, ',', '.') ;
                                      } ?>
                                      
                                      <br>
                                  <?php } ?> 
                                      </td>
                                  
                                    <td><?php echo $mas->notes; ?></td>
                                  </tr>
                                    
                                  <?php $no++; $gTotalBev+= $TotalBev; $gTotalFood+= $TotalFood;} ?>
                                    
                                </tbody>
                                <tfoot>
                                  <tr style="background-color: #f0f0f0; font-weight: bold;">
                                    <td colspan="4"><strong>TOTAL</strong></td>
                                     <td  align="right"><strong><?php echo number_format($gTotalFood, 2, ',', '.') ?></strong></td>
                                    <td  align="right"><strong><?php echo number_format($gTotalBev, 2, ',', '.') ?></strong></td>
                                      <td>&nbsp;</td>
                                  <td  align="right"><strong><?php echo number_format($gTotalFood+$gTotalBev, 2, ',', '.') ?></strong></td>
                                      <td>&nbsp;</td>
                                       <td>&nbsp;</td>
                                  </tr>
                                </tfoot>
                            </table>

         

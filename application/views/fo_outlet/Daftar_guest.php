
        <div class="table-responsive" >
          <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr >
                                        <th >ID</th>
                                        <th >Saluation</th>
                                        <th >Address</th>
                                        <th>State</th>
                                        <th>E-Mail</th>
                                        <th>Phone</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach($data as $u){
                                    ?>
                                    <tr  class='odd gradeX context'>
                                        <td class="idguest"><?php echo $u->idguest ?></td>
                                        <td class="namaguest"><?php echo $u->saluation?> <?php echo $u->firstname?> <?php echo $u->lastname?></td>
                                        <td class="address"><?php echo $u->address?></td>
                                        <td class="state"><?php echo $u->state?></td>
                                        <td class="email"><?php echo $u->email?></td>
                                        <td class="phone"><?php echo $u->phone?></td>
                                        <td align="center">
                                        <?php  if ($status['status'] =='All') {  ?>
                                          <a class="btn btn-success btn-xs Select1"  >  <span class="fa fa-users" ></span> </a>
                                        <?php }else{  ?>
                                          <a class="btn btn-success btn-xs Select"  >  <span class="fa fa-user" ></span> </a>
                                        <?php } ?>

                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
          </div>



<script type="text/javascript">
  $(document).ready(function(){
     $(".Select").click(function(){       
         var id =$(this).closest('tr').children('td.idguest').text();
         var nama =$(this).closest('tr').children('td.namaguest').text();
         var phone =$(this).closest('tr').children('td.phone').text();
          var mymodal11 = $('#myModalGuest');
        $('#IdGuest').val(id);
         $('#NamaGuest').val(nama);
         $('#PhoneGuest').val(phone);
        mymodal11.modal('hide');    
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    var t = $('#example12').DataTable();
    var i = 1;
    var iditem = 0;
      
    $('.Select1').on( 'click', function () {
      
        var idguest =  $(this).closest('tr').children('td.idguest').text();
        var name =  $(this).closest('tr').children('td.namaguest').text();
        var address =$(this).closest('tr').children('td.address').text();
        var state = $(this).closest('tr').children('td.state').text();
        var email =  $(this).closest('tr').children('td.email').text();
        var phone = $(this).closest('tr').children('td.phone').text();
        var remove =  "<a class='btn btn-danger btn-xs Hapus' title='Remove Item'>  <span class=' fa fa-close' ></span> </a> " 
        var index = $('#index').val();
           var indexnew = parseInt(index)+1;
          
       var row = t.row.add([
            indexnew,
            idguest,
            name,
            address,
            state,
            email,
            phone,
            remove
        ] ).draw(false);
        t.row(row).column(0).nodes().to$().addClass('index');
        t.row(row).column(1).nodes().to$().addClass('idguest');
        t.row(row).column(2).nodes().to$().addClass('Name');
        t.row(row).column(3).nodes().to$().addClass('Address');
        t.row(row).column(4).nodes().to$().addClass('State');
        t.row(row).column(5).nodes().to$().addClass('Email');
        t.row(row).column(6).nodes().to$().addClass('Phone');
         t.row(row).column(7).nodes().to$().addClass('action');

        $('<input>').attr({
        type: 'text',
        class: indexnew,
        name: 'idguestAll['+indexnew+']', 
        value: idguest
        }).appendTo('form');

       $('#index').val(indexnew);

       $("#notify").notify("Guest"+" '"+ name +"' "+"Successfully Add","success",
       { position:"left"});

    

} );
} );
</script>

<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>

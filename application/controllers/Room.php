<?php

class Room extends CI_Controller{

	function __construct(){
		parent::__construct();
		if($this->session->userdata('Kode_userPms')==NULL) {
           redirect('Login');
		}
		$this->load->library(array ('session'));
		$this->load->helper(array('form', 'url'));
		$this->load->model(array('M_room'));
		date_default_timezone_set('Asia/Hong_Kong');
		
		if (!empty($this->session->userdata("active_db"))) {
			$this->load->database($this->session->userdata("active_db"), FALSE, TRUE);
		}
	}

	function index(){
		redirect('Room/Room_view');
	}

	function Room_view(){
		$data['data'] = $this->M_room->Room_view();
		$this->load->view('room/Room',$data);
	}

	function Room_add(){
		$data['room_type'] = $this->M_room->Room_type();
		$data['room_status'] = $this->M_room->Room_status();
	//print_r($data['room_type']);exit;
		$this->load->view('room/Room_add',$data);
	}

	function Room_addDB(){
		$data = array(
			'idroomtype' => $this->input->post('roomtype'),
			'idroomstatus' => $this->input->post('roomstatus'),
			'roomname' => $this->input->post('roomname'),
			'desc' => $this->input->post('desc'),
			'isaktif' => '1',
			'currtimestamp' => date('Y-m-d H:i:s')
		);
		$this->M_room->Room_addDB('tbroom',$data) ;
		$this->session->set_flashdata('msg', 'Add Room successfully ...');
		redirect('Room/Room_view');
	}

	function Room_edit($id){
		$data['data'] = $this->M_room->Room_view_edit($id) ;
		$data['room_type'] = $this->M_room->Room_type();
		$data['room_status'] = $this->M_room->Room_status();
		// print_r($data['data']);exit;
		$this->load->view('room/Room_edit',$data);
	}

	function Room_editDB(){
		$id = $this->input->post('Id');
		$data = array(
			'idroomtype' => $this->input->post('roomtype'),
			'idroomstatus' => $this->input->post('roomstatus'),
			'roomname' => $this->input->post('roomname'),
			'desc' => $this->input->post('desc'),
			'isaktif' => '1',
			'currtimestamp' => date('Y-m-d H:i:s')
	);

		$this->M_room->Room_editDB('tbroom',$data,$id);
		$this->session->set_flashdata('msg', 'Edit Room successfully...');
		redirect('Room/Room_view');

	}

}
?>

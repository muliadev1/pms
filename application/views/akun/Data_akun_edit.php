<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>



<!-- Content Header (Page header) -->
<section class="content-header">
   <div class="btn-group btn-breadcrumb">
            <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
             <a href="<?php echo base_url('index.php/Akun/DataAkun');?>" class="btn btn-default  btn-xs">Akun</a>
            <a  class="btn btn-default  btn-xs active">Edit Akun</a>
        </div>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-xs-12">
     
      <div class="box box-info">
        <div class="box-header">
          <h3 class="box-title">Edit Akun</h3>
          
        </div>
    <!-- /.box-header -->
        <div class="box-body">
         <div id="page-wrapper">

        

            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8">
                        <form action="<?php echo base_url(). 'index.php/Akun/EditAkunDB'; ?>" method="post"  id="Simpan" >
          <div class="row">
            <div class="col-lg-8">
              <div class="form-group">
                      <label>Subklasifikasi</label>
                    <select  class="form-control select2"  name="klas" id="klas" style="width: 100%;" >                 
                      <?php foreach($data as $row){ ?>
                      <?php if ($row->IdSub==$edit->IdSub){ ?>
                         <option value="<?php echo $row->IdSub ?>" selected><?php echo $row->NamaSub?> </option>
                      <?php } ?>
                        <option value="<?php echo $row->IdSub ?>"><?php echo $row->NamaSub ?> </option>
                        <?php } ?>
                    </select>
                  </div>

            </div>
            <div class="col-lg-4">
              <div class="form-group" id="CKodeAkun">
                                <label>Id Akun</label>
                                <input type="text"  value="<?php echo $edit->NoAkun ?>"  class="form-control" name="kodeAkun" id="kodeAkun" readonly>
                            </div>
              </div>
            </div>
              <div class="form-group" id="CNamaAkun" name="namaCls">
                                <label>Nama Akun</label>
                                <input type="text" class="form-control" value="<?php echo $edit->NamaAkun ?>"  name="namaAkun" id="namaAkun" onchange="validate('namaAkun','namaCls')">
               </div>

                <div class="form-group">
                <div class="checkbox">
                    <label>
                      <input type="checkbox" name="Kontra" value="1">
                      Kontra Akun
                    </label>
                  </div>

                  
              
              <button type="submit" class="btn btn-default">Simpan</button>
            </form>
                    </div>
                </div>


            </div>
            <!-- /.container-fluid -->

        </div>
          
          
          
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->

</section><!-- /.content -->

<script type="text/javascript">
$("#Simpan").submit(function() {
    
        event.preventDefault();
        $.confirm({
          title: 'Confirmation',
          content: 'Anda Yakin Menyimpan Data?',
           type: 'blue',
          buttons: {
              Simpan: function () {
                  $.LoadingOverlay("show");
                  $("#Simpan").submit();
              },
              Batal: function () {
                
                  $.alert('Data Tidak Disimpan...');
              },
          }
      });

});
</script>


<?php
$this->load->view('template/Js');
?>


<script type="text/javascript">
   $(document).on('change', '#klas', function(){
    var id =$("#klas").val();
    $.ajax({
          type: "POST",
          url: "<?php echo base_url('');?>/index.php/Akun/selectAkunId/", 
          dataType: 'json',
          data: {id: id  },
          success: function (data) {
            $("#kodeAkun").val(data);
          }
      });
   
    });
</script>



<script type="text/javascript">

  // function autoId() {
  //     var valOption = document.getElementById("klas").value;
  //     if (valOption == 0 ) {
  //       document.getElementById("klas").value = "";
  //       document.getElementById("CSubKlas").className = "form-group has-error";
  //     }else {
      
  //     document.getElementById("CSubKlas").className = "form-group has-success";
  //     document.getElementById("CKodeAkun").className = "form-group has-success";
  //     $("#subKlas").focus();
  //   }
  //   validate("klas","opCls");
  // }

  function validate(id,cls) {
    var isId = document.getElementById(id);
    var isCls = document.getElementById(cls);
    if (isId.value == 0 || isId.value == null || isId.value == "") {
      isCls.className = "form-group has-error";
      return false;
    }else {
      isCls.className = "form-group has-success";
      return true;
    }
  }

  function validateForm(){
    var val1 = validate("klas","CSubKlas");
    var val3 = validate('namaAkun','CNamaAkun');
    if (val1 == false || val3 == false ) {
      alert("Data Belum Lengkap!");
      return false;
    }
  }
</script>

 <script>
  $(function () {
    $(".select2").select2();
     $(".select3").select2();

  });
  </script>

<?php
$this->load->view('template/foot');
?>

<?php echo $header?>

<style type="text/css">
  #TabelKonten tr td {
    padding-right: 7px;
    padding-left:  7px;
    font-size: 12px;
  }
  tr.noBorder td {
  border: 0;
}
 
</style>

<div style="margin-buttom : 15px;" >
<table width="100%" border="0"  >
    <tr><td colspan="2" align="center" style="font-size:14px;"> <strong> MEMORANDUM INVOICE  </strong>  </td></tr>
    <tr><td  colspan="2" align="center" style="font-size:8px;"> <strong> Date: </strong><?php echo $periode['TglAwal'].' '.'-'.' '.$periode['TglAkhir'] ?>  </td></tr>
    <tr><td colspan="2" align="left">&nbsp;</td></tr>
    </table>
    
   


<table id="TabelKonten"  border="1" style="border-collapse: collapse; border-color:#000000; margin-bottom : 130px;"  width="100%"   >
 
    <tbody>
      <?php $total=0; $NoRo = ''; $count = 0;$no = 1; foreach ($konten as $row) { ?>
        <?php if ($row->kodero != $NoRo ) { ?>
          
             <?php if ($count!=0 ) { ?>
                <tr style="background-color:#D9E0D9;">
                  <td colspan="4" align="center"><strong>TOTAL</strong></td>
                   <td align="right"><strong><?php echo number_format($total, 0, '.', '.'); $total =0;?></strong></td>
                </tr>
                  <tr class="noBorder">
                   <td  >&nbsp;</td>
                   <td colspan="4" align="left">&nbsp;</td>
                  </tr>
                  <tr class="noBorder">
                   <td >&nbsp;</td>
                   <td colspan="4" >&nbsp;</td>
                  </tr> 
                <?php }?>

            <tr >
             <td width="10%" style="border-right: 0; border-bottom: 0;"><strong>NO</strong>  </td>
             <td align="left" colspan="3"  style="border-left: 0; border-bottom: 0;">: <?php echo $row->kodero;?></td>
             <td align="right"  ><strong><?php echo $no; ?></strong></td>
             <?php $no++;?>
            </tr>
            <tr >
             <td style="border-top: 0; border-bottom: 0; border-right: 0;"><strong>Vendor</strong></td>
             <td colspan="4" align="left" style=" border: 0;" style="border-top: 0; border-bottom: 0; border-left: 0;">: <?php echo $row->vendor;?></td>
            </tr>
            <tr >
             <td style="border-top: 0; border-bottom: 0; border-right: 0;" ><strong>Date</strong></td>
             <td style=" border: 0;" style="border-top: 0; border-bottom: 0; border-left: 0;" colspan="4" align="left" >: <?php echo $row->datereceive;?></td>
            </tr>
                  <?php if ($count!=0 ) { ?>
                    <tr align="center" class="header">
                      <td  width="15%" align="center"><strong>Item Code</strong></td>
                      <td width="15%" align="center"><strong>Unit</strong></td>
                      <td width="40%" align="center"><strong>Description</strong></td>
                      <td width="15%" align="center"><strong>Unit Price</strong></td>
                      <td width="15%" align="center"><strong>Total</strong></td>
                  </tr>
                  <?php }else{?>
                   <tr align="center" class="header">
                      <td  width="15%" align="center"><strong>Item Code</strong></td>
                      <td width="15%" align="center"><strong>Unit</strong></td>
                      <td width="40%" align="center"><strong>Description</strong></td>
                      <td width="15%" align="center"><strong>Unit Price</strong></td>
                      <td width="15%" align="center"><strong>Total</strong></td>
                  </tr>

                  <?php }?>

          

        <?php }?>

    <tr>
      <td><?php echo $row->itemcode; ?></td>
      <td><?php echo $row->unit; ?></td>
      <td><?php echo $row->description; ?></td>
      <td align="right"><?php echo number_format($row->price, 0, '.', '.'); ?></td>
      <td align="right"><?php echo number_format($row->subtotal, 0, '.', '.'); ?></td>
      <?php $total+=$row->subtotal; $NoRo = $row->kodero; $count++;} ?>
    </tr>
    
   <!--  total tetakhir -->
    <tr style="background-color:#D9E0D9;">
                  <td colspan="4" align="center"><strong>TOTAL</strong></td>
                   <td align="right"><strong><?php echo number_format($total, 0, '.', '.'); $total =0;?></strong></td>
                </tr>
                  <tr class="noBorder">
                   <td  >&nbsp;</td>
                   <td colspan="4" align="left">&nbsp;</td>
                  </tr>
                  <tr class="noBorder">
                   <td >&nbsp;</td>
                   <td colspan="4" >&nbsp;</td>
                  </tr> 
  
    </tbody>
    

  </table>



<table width="100%" border="0" style="font-size:11px; "  >
    <tr>
      <td >&nbsp; </td>
      <td >&nbsp; </td>     
      <td >&nbsp; </td>
    </tr>
     <tr>
      <td >&nbsp; </td>
      <td >&nbsp; </td>     
      <td >&nbsp; </td>
    </tr>
  <tr>
     <td ><strong>QUANTYTY AND QUALITY APPROVED</strong> </td>
     <td ><strong>PRICE APPROVED</strong></td>
     <td ><strong>RECEIVED</strong></td>
    </tr>
         <tr>
      <td >&nbsp; </td>     
      <td >&nbsp; </td>
      <td >&nbsp; </td>
    </tr>
     <tr>
      <td >&nbsp; </td>     
      <td >&nbsp; </td>
      <td >&nbsp; </td>
    </tr>
     <tr>
      <td >&nbsp; </td>     
      <td >&nbsp; </td>
      <td >&nbsp; </td>
    </tr>
     <tr>
      <td ><hr style="color:#000000"> </td>     
      <td ><hr style="color:#000000"> </td>
      <td ><hr style="color:#000000"> </td>
    </tr>
   
  </table>


  






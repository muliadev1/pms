<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>
<script src="<?php echo base_url('assets/js/accounting.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/cleave.min.js') ?>" type="text/javascript"></script>
<style type="text/css">

.modal.modal-wide .modal-dialog {
  width: 80%;
}

.modal-wide .modal-body {
  overflow-y: auto;
}




#tallModal .modal-body p { margin-bottom: 900px }
.pull-right {
    float: right;
}

.pull-left {
    float: left;
}

select {
    width: 300px;
}

</style>
    <script type="text/javascript">
      jQuery(document).ready(function($) {
        new Cleave('.input-1', {
           numeral: true, 
           numeralDecimalMark: ',',
           delimiter: '.' 
      });
        new Cleave('.input-2', {
           numeral: true, 
           numeralDecimalMark: ',',
           delimiter: '.' 
      });
          });
    </script>
    <script type="text/javascript">
      function doMath()
      {
          var qty = document.getElementById('Qty1').value;
          document.getElementById('Qty').value =  qty.replace(/\./g, "");

          var price = document.getElementById('Price1').value;
          document.getElementById('Price').value =  price.replace(/\./g, "");
      }
    </script>

<!-- Content Header (Page header) -->
<!-- Content Header (Page header) -->
<section class="content-header">
   <div class="btn-group btn-breadcrumb">
            <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
  <a href="<?php echo base_url('index.php/Purchase_order/purchase_order_list_veri_after');?>" class="btn btn-default  btn-xs">PO List</a>
            <a  class="btn btn-default  btn-xs active">Edit PO</a>
        </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
             <div class="box box-info">
              <form method="post" id="Simpan" action="<?php echo base_url(). 'index.php/Purchase_order/add_po_edit'; ?>" enctype="multipart/form-data">
                <input type="text" name="kodepoEdit" value="<?php echo $master->kodepo ?>">
                <div class="box-header">
                  <h3 class="box-title" ><strong>Edit Purchase Order</strong> </h3>
                  <h3 class="box-title" style="float:right" ><strong><?= date('d-m-Y') ?></strong> </h3>
                  <hr style="border-top: 1px solid #8c8b8b; padding: 1px; margin-top: 5px; margin-bottom: 0px;">
                </div>
                <div class="box-body">


               <div class="row">
                 <div class="col-md-6">
                   <div class="form-group">
                      <label>Vendor</label>
                    <select id="IdVendor" class="form-control select2"  style="width: 100%;" name="IdVendor">                 
                      <?php foreach($data as $u){ ?>
                      <?php if ($master->idvendor == $u->idvendor ) { ?>
                       <option value="<?php echo $u->idvendor; ?>" selected><?php echo $u->vendor; ?></option>
                       <?php }?>
                        <option value="<?php echo $u->idvendor; ?>" ><?php echo $u->vendor; ?></option>
                        <?php } ?>
                    </select>
                    <input type="hidden" name="Vendor" id="Vendor" value="<?php echo  $master->vendor ?>">
                  </div>
                </div>
              </div>
              <div class="row">
                 <div class="col-md-6">
                    <div class="form-group">
                    <label>Expeted Date</label>
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text"   value="<?= date('m/d/Y') ?>"     class="form-control pull-right" name= "ExpetedDate" id="ExpetedDate">
                      </div>
                  </div>
                </div>
              <div class="col-md-6">
                   <div class="form-group">
                     <a style="margin-top:25px; margin-right:10px; " class="btn btn-primary btn-flat  " id="View"     ><i class='glyphicon glyphicon-th-list'></i>  Request  </a>  
                  </div>
                </div>
              </div>
               <div class="row">
                  <div class="col-md-2">
                    <div id ="divItemCode" class="form-group">
                          <label class="control-label">Item Code</label>
                          <input type="text" name="ItemCodeInput" id="ItemCodeInput"  class="form-control">
                     </div>
                  </div>
                  <div class="col-md-4">
                    <div id ="divDescription" class="form-group">
                          <label class="control-label">Description</label>
                          <input type="text" name="Description" id="Description" readonly="readonly" class="form-control">
                     </div>
                  </div>
                  <div  class="col-md-1">
                    <div id ="divQty" class="form-group">
                          <label class="control-label">Qty</label>
                          <input type="text" name="Qty1" id="Qty1" autocomplete="off"  class="form-control " onkeyup="doMath()" >
                          <input type="hidden" name="Qty" id="Qty"  onkeyup="doMath()">
                     </div>
                  </div>
                  <div  class="col-md-2">
                    <div id ="divPrice" class="form-group">
                          <label class="control-label">Price</label>
                          <input type="text" name="Price1" id="Price1"  class="form-control input-2" onkeyup="doMath()">
                           <input type="hidden" name="Price" id="Price"  class="form-control" onkeyup="doMath()">
                           <input type="hidden" name="IndexNew" id="IndexNew" value="<?php echo count($detil) ?>" class="form-control">
                           <input type="hidden" name="UnitNew" id="UnitNew"  class="form-control">
                           <input type="hidden" name="index" id="index" value="0"  class="form-control">
                            <input type="hidden" name="StokAdd" id="StokAdd"   class="form-control">

                     </div>
                  </div>
                  
                </div>
                <div class="row">
                  <div  class="col-md-9">
                   <div  class="form-group">
                          <label class="control-label">Note</label>
                          <input type="text" name="NoteText" id="NoteText" autocomplete="off"  class="form-control "  > 
                     </div>
                  </div>
                  <div  class="col-md-3">
                    <div class="form-group" >
                        <a style="margin-top:25px; margin-right:10px; " class="btn btn-primary btn-flat disabled Edit" id="Edit"  ><i class='glyphicon glyphicon-arrow-down'></i>  Proses  </a> 
                        <a style="margin-top:25px; margin-right:10px; " class="btn btn-warning btn-flat disabled " id="Batal"  ><i class='glyphicon glyphicon-remove'></i>  Batal  </a>  
                     </div>
                  </div>
                </div>

          

                      <div class="table-responsive">
                     
                         <table id="tbdetil" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th >No</th>
                                    <th>Item Code</th>
                                    <th>Description</th>
                                    <th>Qty Order</th>
                                    <th>Unit</th>
                                    <th>Price</th>
                                    <th>Subtotal</th>
                                     <th>Note</th>
                                    <th>Action</th>
                                     <th >Stok</th>
                                    
                                </tr>
                            </thead>
                            <tbody id="mytbody">
                              <?php $i=0; foreach ($detil as $pek) { ?> 
                              <tr>
                                <td><?php echo $i ?></td>
                                <td><?php echo $pek->codeitem ?></td>
                                <td><?php echo $pek->description ?></td>
                                <td><?php echo $pek->qtyorder ?></td>
                                <td><?php echo $pek->unit ?></td>
                                <td><?php echo number_format($pek->price, 0, '.', '.') ?></td>
                                <td><?php echo number_format($pek->subtotal, 0, '.', '.') ?></td>
                                <td><?php echo $pek->note ?></td>
                                <td><?php echo "<a class='btn btn-warning btn-xs PilihEdit' title='Edit Item'>  <span class=' fa fa-edit' ></span></a> <a class='btn btn-danger btn-xs Hapus' title='Remove Item'>  <span class=' fa fa-close' ></span> </a>" ?></td>
                                <td><?php echo $pek->qtystok ?></td>
                              </tr>

    

                              <?php }?>
                          
                                
                            </tbody>
                        </table>
                      </div> 
                    <div class="col-md-12" align="left" style="margin-top :30px;">
                      <div class="form-group" >
                       <textarea name="NotePo" class="form-control" placeholder="NOTE..."><?php echo $master->description; ?></textarea>
                      </div>
                    </div>
                       
                       <div class="col-md-12" align="left" style="margin-top :30px;">
                          <div class="form-group" >
                              <a   type="submit" class="btn btn-success btn-flat"  id="simpan1"  ><i class='fa fa-check-square-o'></i>  Simpan  </a>  
                          </div>
                      </div>
                       <?php $i1=0; foreach ($detil as $pek1) { ?>
      <input type="hidden" class="<?php echo $i1 ?>" name="itemcode[<?php echo $i1 ?>]" value="<?php echo $pek1->codeitem ?>" >
        <input type="hidden" class="<?php echo $i1 ?>" name="Description[<?php echo $i1 ?>]" value="<?php echo $pek1->description ?>" >
         <input type="hidden" class="<?php echo $i1 ?>" name="Qty[<?php echo $i1 ?>]" value="<?php echo $pek1->qtyorder ?>" >
         <input type="hidden" class="<?php echo $i1 ?>" name="Unit[<?php echo $i1 ?>]" value="<?php echo $pek1->unit ?>" >
         <input type="hidden" class="<?php echo $i1 ?>" name="Price[<?php echo $i1 ?>]" value="<?php echo $pek1->price ?>" >
         <input type="hidden" class="<?php echo $i1 ?>" name="Subtotal[<?php echo $i1 ?>]" value="<?php echo $pek1->subtotal ?>" >
          <input type="hidden" class="<?php echo $i1 ?>" name="Note[<?php echo $i1 ?>]" value="<?php echo $pek1->note ?>" >
          <input type="hidden" class="<?php echo $i1 ?>" name="Stok[<?php echo $i1 ?>]" value="<?php echo $pek1->qtystok ?>" >
                       <?php $i1++;}?>

        </form>  
      </div>
     </div>
</div>

    <div class="modal modal-wide fade" id="myModal" role="dialog" style="display:none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" title="close" data-dismiss="modal">&times;</button>
                    <h4 align="center" class="modal-title">Purchase Requisition List</h4>
                </div>
                <div class="modal-body">   
                </div>
            </div>
        </div>     
    </div>

        <div class="modal modal-wide fade" id="myModalNew" role="dialog" style="display:none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" title="close" data-dismiss="modal">&times;</button>
                    <h4 align="center" class="modal-title">Item List</h4>
                </div>
                <div class="modal-body">   
                </div>
            </div>
        </div>     
    </div>

 

</section>

  

<?php
$this->load->view('template/Js');
$this->load->view('template/Foot');
?>

 <script>
        var iditem = '';
        var itemcode = '';
        var Description ='';
        var Qty = '';
        var Unit =  '';
        var Price = '';
        var Subtotal = '';
        var Stok = '';
        var note = '';
 $('#tbdetil tbody').on('click', '.PilihEdit', function(e){
  var t = $('#tbdetil').DataTable();
  //$('table').on('click', '.PilihEdit', function(e){
         var t = $('#tbdetil').DataTable();
         iditem = t.row($(this).closest('tr')).data()[0]; //$(this).closest('tr').children('td.No').text();
         itemcode = t.row($(this).closest('tr')).data()[1]; //$(this).closest('tr').children('td.ItemCode').text();
         Description = t.row($(this).closest('tr')).data()[2];//$(this).closest('tr').children('td.Description').text();
         Qty = t.row($(this).closest('tr')).data()[3];//$(this).closest('tr').children('td.Qty').text();
         Unit =  t.row($(this).closest('tr')).data()[4];//$(this).closest('tr').children('td.Unit').text();
         Price = t.row($(this).closest('tr')).data()[5]; //$(this).closest('tr').children('td.Price').text();
         Subtotal = t.row($(this).closest('tr')).data()[6];//$(this).closest('tr').children('td.Subtotal').text();
         Stok = t.row($(this).closest('tr')).data()[9];//$(this).closest('tr').children('td.StokEditTd').text();
          Note = t.row($(this).closest('tr')).data()[7];//$(this).closest('tr').children('td.Note').text();

        $('#NoteText').val(Note);
        $('#Id').val(iditem);
        $('#ItemCodeInput').val(itemcode);
        $('#Description').val(Description);
        $('#Qty').val(Qty);
        $('#Qty1').val(Qty);
        $('#Price').val(Price);
        $('#Price1').val(Price);
        
        styleEdit()
      
     $('.'+iditem).remove();
      t.row($(this).closest('tr')).remove().draw(false);

  })


  $('#Batal').on('click', function(e){ 
  var t = $('#tbdetil').DataTable();
   PriceHitung =Price.replace(/\./g, "");
   QtyHitung = Qty.replace(/\./g, "");
   iditemNew = $('#IndexNew').val();
  var btnedit =  "<a class='btn btn-warning btn-xs PilihEdit' title='Edit Item'>  <span class=' fa fa-edit' ></span></a> <a class='btn btn-danger btn-xs Hapus' title='Remove Item'>  <span class=' fa fa-close' ></span> </a>" 

  if (iditemNew == '') {
        var row = t.row.add( [
            iditem,
            itemcode,
            Description,
            Qty,
            Unit,
            Price,
            Subtotal,
            Note,
            btnedit,
            Stok
        ] ).draw(false);
        t.row(row).column(0).nodes().to$().addClass('No');
        t.row(row).column(1).nodes().to$().addClass('ItemCode');
        t.row(row).column(2).nodes().to$().addClass('Description');
        t.row(row).column(3).nodes().to$().addClass('Qty');
        t.row(row).column(4).nodes().to$().addClass('Unit');
        t.row(row).column(5).nodes().to$().addClass('Price');
        t.row(row).column(6).nodes().to$().addClass('Subtotal');
        t.row(row).column(7).nodes().to$().addClass('Note');
        t.row(row).column(8).nodes().to$().addClass('Edit');
        t.row(row).column(9).nodes().to$().addClass('StokEditTd');


        $('<input>').attr({
        type: 'hidden',
        class: iditem,
        name: 'index', 
        value: iditem
        }).appendTo('form');

        $('<input>').attr({
        type: 'hidden',
        class: iditem,
        name: 'itemcode['+iditem+']', 
        value: itemcode
        }).appendTo('form');

        $('<input>').attr({
        type: 'hidden',
        class: iditem,
        name: 'Description['+iditem+']', 
        value: Description
        }).appendTo('form');

        $('<input>').attr({
        type: 'hidden',
        class: iditem,
        name: 'Qty['+iditem+']', 
        value: Qty.replace(/\./g, "")
        }).appendTo('form');


         $('<input>').attr({
        type: 'hidden',
        class: iditem,
        name: 'Unit['+iditem+']', 
        value: Unit
        }).appendTo('form');

        $('<input>').attr({
        type: 'hidden',
        class: iditem,
        name: 'Price['+iditem+']', 
        value: Price.replace(/\./g, "")
        }).appendTo('form');

        $('<input>').attr({
        type: 'hidden',
        class: iditem,
        name: 'Subtotal['+iditem+']', 
        value: Subtotal.replace(/\./g, "")
        }).appendTo('form');

        $('<input>').attr({
          type: 'text',
          class: iditem,
          name: 'Note['+iditem+']', 
          value: Note
          }).appendTo('form');

        $('<input>').attr({
        type: 'hidden',
        class: iditem,
        name: 'Stok['+iditem+']', 
        value: Stok
        }).appendTo('form');

        styleEditProses();
    }else{
      styleEditProses();
    }
  })

  $('#Edit').on('click', function(e){ 
    var t = $('#tbdetil').DataTable();
  var btnedit =  "<a class='btn btn-warning btn-xs PilihEdit' title='Edit Item'>  <span class=' fa fa-edit' ></span> </a> <a class='btn btn-danger btn-xs Hapus' title='Remove Item'>  <span class=' fa fa-close' ></span> </a>" 

       itemcode = $('#ItemCodeInput').val();
        iditemNew = $('#IndexNew').val();
        UnitNew = $('#UnitNew').val();
       Description = $('#Description').val();
       Qty = $('#Qty1').val();
       Price =$('#Price1').val();
       Stok = $('#StokAdd').val();
       Note = $('#NoteText').val();
       PriceHitung =Price.replace(/\./g, "");
       QtyHitung = Qty;//.replace(/\./g, "");
       Subtotal = PriceHitung*QtyHitung;
                                

       if (iditemNew != '') {
        var index = $('#index').val();
        iditem = parseInt(index)+1;;
        Unit = UnitNew;
         $('#index').val(iditem);
       };

        var row = t.row.add( [
            iditem,
            itemcode,
            Description,
            Qty,
            Unit,
            Price,
            Subtotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."),
            Note,
            btnedit,
            Stok
        ] ).draw(false);
        t.row(row).column(0).nodes().to$().addClass('No');
        t.row(row).column(1).nodes().to$().addClass('ItemCode');
        t.row(row).column(2).nodes().to$().addClass('Description');
        t.row(row).column(3).nodes().to$().addClass('Qty');
        t.row(row).column(4).nodes().to$().addClass('Unit');
        t.row(row).column(5).nodes().to$().addClass('Price');
        t.row(row).column(6).nodes().to$().addClass('Subtotal');
        t.row(row).column(7).nodes().to$().addClass('Note');
        t.row(row).column(8).nodes().to$().addClass('Edit');
        t.row(row).column(9).nodes().to$().addClass('StokEditTd');


        $('<input>').attr({
        type: 'hidden',
        class: iditem,
        name: 'index', 
        value: iditem
        }).appendTo('form');

        $('<input>').attr({
        type: 'hidden',
        class: iditem,
        name: 'itemcode['+iditem+']', 
        value: itemcode
        }).appendTo('form');

        $('<input>').attr({
        type: 'hidden',
        class: iditem,
        name: 'Description['+iditem+']', 
        value: Description
        }).appendTo('form');

        $('<input>').attr({
        type: 'hidden',
        class: iditem,
        name: 'Qty['+iditem+']', 
        value: Qty//.replace(/\./g, ",")
        }).appendTo('form');


         $('<input>').attr({
        type: 'hidden',
        class: iditem,
        name: 'Unit['+iditem+']', 
        value: Unit
        }).appendTo('form');

        $('<input>').attr({
        type: 'hidden',
        class: iditem,
        name: 'Price['+iditem+']', 
        value: Price.replace(/\./g, "")
        }).appendTo('form');

        $('<input>').attr({
        type: 'hidden',
        class: iditem,
        name: 'Subtotal['+iditem+']', 
        value: Subtotal
        }).appendTo('form');

       $('<input>').attr({
          type: 'hidden',
          class: iditem,
          name: 'Note['+iditem+']', 
          value: Note
          }).appendTo('form');

        $('<input>').attr({
        type: 'hidden',
        class: iditem,
        name: 'Stok['+iditem+']', 
        value: Stok
        }).appendTo('form');
        styleEditProses();
    })
    

 $('#tbdetil tbody').on('click', '.Hapus', function(e){
 //$('#tbdetil tbody').on('click', '.Hapus', function(e){
       var t = $('#tbdetil').DataTable();
      iditem = t.row($(this).closest('tr')).data()[0];//$(this).closest('tr').children('td.No').text();
      $('.'+iditem).remove();
      t.row($(this).closest('tr')).remove().draw(false);
      $('#IndexDelete').val(iditem);
 })
  </script>


<script type="text/javascript">
$("#simpan1").click(function() {
        event.preventDefault();
        $.confirm({
          title: 'Confirmation',
          content: 'Are You Sure to Save?',
          type:'blue',
          buttons: {
              Simpan: function () {
                  $.LoadingOverlay("show");
                  $("#Simpan").submit();
              },
              Batal: function () {
                
                  $.alert('Data Tidak Disimpan...');
              },
          }
      }); 

});
</script>



<script type="text/javascript">
  $(document).ready(function(){
     $("#View").click(function(e){ 
     var idvendor = $('#IdVendor').val();        
      $.ajax({
        type: "POST",
       url: '<?php echo site_url('Purchase_order/select_pr_byvendor');?>',
        data: {idvendor: idvendor  },
        success: function(data){
            e.preventDefault();
            var mymodal = $('#myModal');
            mymodal.modal({backdrop: 'static', keyboard: false}) 
            var height = $(window).height() - 200;
            mymodal.find(".modal-body").css("max-height", height);
            mymodal.find('.modal-body').html(data);
            mymodal.modal('show');            
        }
      });
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function(){
     $("#ItemCodeInput").click(function(e){ 
     var idvendor = $('#IdVendor').val();        
      $.ajax({
        type: "POST",
       url: '<?php echo site_url('Purchase_order/daftar_item');?>',
        data: {idvendor: idvendor, dari:'PO'  },
        success: function(data){
            e.preventDefault();
            var mymodal1 = $('#myModalNew');
            mymodal1.modal({backdrop: 'static', keyboard: false}) 
            var height = $(window).height() - 200;
            mymodal1.find(".modal-body").css("max-height", height);
            mymodal1.find('.modal-body').html(data);
            mymodal1.modal('show');            
        }
      });
    });
  });
</script>

  <script type="text/javascript">
function styleEdit() {
        $('#View').addClass('disabled')
        $('#simpan1').addClass('disabled');
        $('#Batal').removeClass('disabled');
        $('#Edit').removeClass('disabled');
        $('.PilihEdit').addClass('disabled');
        $('#divItemCode').addClass('has-success');
        $('#divDescription').addClass('has-success');
        $('#divQty').addClass('has-success');
        $('#divPrice').addClass('has-success');
        $('#IndexNew').val('');
          
}
function styleEditProses() {
        $('#View').removeClass('disabled');;
        $('#simpan1').removeClass('disabled');
        $('#Batal').addClass('disabled');
        $('#Edit').addClass('disabled');
        $('.PilihEdit').removeClass('disabled');
        $('#divItemCode').removeClass('has-success');
        $('#divDescription').removeClass('has-success');
        $('#divQty').removeClass('has-success');
        $('#divPrice').removeClass('has-success');
        
        $('#Id').val('');
        $('#ItemCodeInput').val('');
        $('#Description').val('');
        $('#Qty').val('');
        $('#Qty1').val('');
        $('#Price').val('');
        $('#Price1').val('');
        $('#IndexNew').val('');
        $('#StokAdd').val('');
         $('#NoteText').val('');
}
  </script>



<script type="text/javascript">
  $(function () {
  $("#tbdetil").DataTable();
    dtTable= $("#tbdetil").DataTable();
    dtTable.columns([0,9]).visible(false);
   });
  </script>


  <script>
  $(function () {
    $(".select2").select2();
     $(".select3").select2();

  });
  </script>

   <script type="text/javascript">
  function File_Kosong() {
  $.alert({
    title: 'Caution!!',
    content: 'Invalid Data!',
    icon: 'fa fa-warning',
    type: 'orange',
});
}
</script>

      <script>
  $( function() {
    $( "#ExpetedDate" ).datepicker({
      autoclose: true,
      dateFormat: 'yy/mm/dd'
    });


  });
  </script>

<script type="text/javascript">
   $(document).on('change', '#IdVendor', function(){
    var NamaVen =$("#IdVendor option:selected").text(); 
    $("#Vendor").val('');  
      $("#Vendor").val(NamaVen);
    });
</script>


<?php if($this->session->flashdata('kodepo')): ?>
<?php $kodepocetak =  $this->session->flashdata('kodepo');?> 
<script type="text/javascript">
  $(document).ready(function() {
    var kodepo = "<?php echo $kodepocetak;?>"
    
          window.open("<?php echo base_url(). 'index.php/Purchase_order/CetakPO/';?>?NoPO="+kodepo ,"MyTargetWindowName")


  });
  </script>

  <?php endif; ?>






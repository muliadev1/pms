<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>

<section class="content-header">
    <div class="btn-group btn-breadcrumb">
            <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
            <a  class="btn btn-default  btn-xs active">FO Invoice</a>
        </div>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Guest List</h3>
          
        </div>
    <!-- /.box-header -->
        <div class="box-body">
        <div class="table-responsive">
          <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr >
                                        <th >ID</th>
                                        <th >Saluation</th>
                                        <th >Address</th>
                                        <th>State</th>
                                        <th>E-Mail</th>
                                        <th>Phone</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach($data as $u){
                                    ?>
                                    <tr  class='odd gradeX context'>
                                        <td><?php echo $u->idguest ?></td>
                                        <td><?php echo $u->saluation?> <?php echo $u->firstname?> <?php echo $u->lastname?></td>
                                        <td><?php echo $u->address?></td>
                                        <td><?php echo $u->state?></td>
                                        <td><?php echo $u->email?></td>
                                        <td><?php echo $u->phone?></td>
                                        <td align="center">
                                          <a class="btn btn-warning btn-xs"   
                            href="<?php echo base_url('index.php/fo_outlet/Fo_invoice_view/'.$u->idreservation); ?>">  <span class="fa fa-fw fa-edit" ></span> </a>
                                         <input type="hidden" id="btnhapus" value="<?php echo $u->saluation ?>" >


                                        </td>






                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
          </div>



        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->

</section><!-- /.content -->


<?php
$this->load->view('template/Js');
$this->load->view('template/Foot');
?>
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>

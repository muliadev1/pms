
      <form method="post" id="Simpan" action="<?php echo base_url(). 'index.php/Stok_opname/add_proses_opname'; ?>">
      <input type="hidden" name="Istok"  value="<?php echo $departement['istok'] ;?>">
      <input type="hidden" name="Set"  value="<?php echo $departement['set'] ;?>">
      <input type="hidden" name="Departement"  value="<?php echo $departement['dep'] ;?>">
      <input type="hidden" name="iddepartement"  value="<?php echo $departement['iddep'] ;?>">
        <div class="table-responsive">
          <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr >
                                        <th width="20%">Item Code</th>
                                        <th width="30%">Sub Category</th>
                                        <th width="40%">Description</th>                         
                                        <th width="10%">Stok</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i=1;
                                    foreach($data as $u){

                                    ?>
                                    <tr  class='odd gradeX context'>
                                        <td><?php echo $u->codeitem ?></td>
                                        <td><?php echo $u->subcategory?></td>
                                        <td><?php echo $u->description?></td>
                                        
                                         <td align="left">
                                         <input style="width:100%" type="text" name="CurStok[<?php echo $i?>]" class="form-control has-success" value="-" autocomplete="off" >
                                          <input type="hidden" name="Stok[<?php echo $i?>]" class="form-control" value="<?php echo $u->stok?>" >
                                         <input type="hidden" name="codeitem[<?php echo $i?>]" value="<?php echo $u->codeitem ?>" class="form-control"  >
                                         <input type="hidden" name="description[<?php echo $i?>]" class="form-control" value="<?php echo $u->description ?>" >
                                         <input type="hidden" name="price[<?php echo $i?>]" class="form-control" value="<?php echo $u->price ?>" >
                                         <input type="hidden" name="unit[<?php echo $i?>]" class="form-control" value="<?php echo $u->unit ?>" >
                                        </td>
                                    </tr>
                                    <?php $i++;} ?>
                                </tbody>
                            </table>
          </div>
          <div class="form-group" style="padding-top:20px;">
            <a   id="Simpan1" class="btn btn-primary"><i class='fa fa-paper-plane'></i> Process</a>

          </div>
        </div><!-- /.box-body -->
        </form>
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->



<script type="text/javascript">
$("#Simpan1").click(function() {
    
        event.preventDefault();
        $.confirm({
          title: 'Confirmation',
          content: 'Are You Sure to Save?',
           type: 'blue',
          buttons: {
              Simpan: function () {
                  $.LoadingOverlay("show");
                  $("#Simpan").submit();
              },
              Batal: function () {
                
                  $.alert('Data Tidak Disimpan...');
              },
          }
      });
     

});
</script>
<script type="text/javascript">
  function File_Kosong() {
  $.alert({
    title: 'Caution!!',
    content: 'Transaction Is Invalid!',
    icon: 'fa fa-warning',
    type: 'orange',
});
}
</script>

<script>
  $(function () {
    
    $('#example1').DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>

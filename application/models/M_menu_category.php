<?php
/**
 *
 */
class M_menu_category extends CI_Model{

  function MenuCategory_view()  {
    $data = $this->db->query("SELECT * FROM tbmenucategory");
		return $data->result();
  }

  function MenuCategory_addDB($table,$data) {
    $this->db->insert($table,$data);
   // print_r($this->db->last_query());
  }

  function MenuCategory_view_edit($id) {
    $this->db->select("*");
    $this->db->where('id',$id);
    $this->db->from('tbmenucategory');
    return $this->db->get()->result();
  }

  function MenuCategory_editDB($table,$data,$id) {
    $this->db->where('id',$id );
    $this->db->update($table,$data);
  }
}

?>

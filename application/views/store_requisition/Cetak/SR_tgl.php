
<?php echo $header?>

<style type="text/css">
  #TabelKonten tr td {
    padding-right: 7px;
    padding-left:  7px;
    font-size: 12px;
  }
</style>

<div style="margin-buttom : 15px;" >
<table width="100%" border="0"  >
    <tr>
     <td colspan="2" align="center" style="font-size:14px;"> <strong> STORE REQUISITION  </strong>  </td>
    </tr>
    
   
  </table>
<table width="100%" border="0" style="font-size:11px;"  >
  
    <tr>
     <td width="20%" ><strong>DATE</strong></td>
     <td align="left">: <?php echo $head['Tanggal'];?> - <?php echo $head['Tanggal1'];?></td>
    </tr>

     <tr>
     <td width="15%"><strong>DEPARTEMENT</strong> </td>
     <td align="left">: <?php echo $head['NamaDep'];?></td>
    </tr>
    <tr>
     <td width="15%"><strong>SUBCATEGORY</strong> </td>
     <td align="left">: <?php echo $head['NamaSub'];?></td>
    </tr >
    <tr>
     <td>&nbsp;</td>
     <td align="left">&nbsp;</td>
    </tr>
  </table>
</div>

<table id="TabelKonten"  border="1" style="border-collapse: collapse; border-color:#000000; margin-bottom : 130px;"  width="100%"   >
  <tbody>
    <tr>
      <th rowspan="2" scope="col">NO</th>
      <th rowspan="2" scope="col">Description</th>
      <th rowspan="2" scope="col">Date</th>
      <th rowspan="2"  scope="col">Qty</th>
      <th height="35" colspan="2" scope="col">Price</th>
      <th rowspan="2" scope="col">Remark</th>
    </tr>
    <tr>
      <td align="center"><strong>Unit</strong> </td>
      <td align="center"><strong>Total</strong></td>
    </tr>
    <?php $i=1; $total = 0 ;  foreach ($kontenSr as $pek ) { ?>
    <tr>
      <td><?php echo $i?></td>
      <td><?php echo $pek->description ?></td>
      <td><?php echo $pek->dateadd ?></td>
       <td align="right"><?php echo number_format($pek->qtyorder, 2, '.', '.') ?></td>
      <td align="right"><?php echo number_format($pek->price, 2, '.', '.') ?></td>
      <td align="right"><?php echo number_format($pek->qtyorder*$pek->price, 2, '.', '.')  ?></td>
      <td align="center"><?php echo $pek->notedetil ?></td>
    </tr>
   <?php $i++; $total += $pek->qtyorder*$pek->price;} ?> 
    
  </tbody>
  <tfoot>
    <tr style="background-color: #f0f0f0;">
      <td  colspan="5"><strong>TOTAL</strong> </td>
      <td align="right"><strong><?php echo number_format($total, 2, ',', '.') ?></strong></td>
      <td >&nbsp;</td>
    </tr>
  </tfoot>
</table>

<!-- 
<table width="100%" border="0" style="font-size:11px; "  >
    <tr>
      <td >&nbsp; </td>
      <td >&nbsp; </td>     
      <td >&nbsp; </td>
    </tr>
     <tr>
      <td >&nbsp; </td>
      <td >&nbsp; </td>     
      <td >&nbsp; </td>
    </tr>
    
    <tr>
     <td align="center"><strong>PURCHASING</strong> </td>
     <td  align="center"><strong>APPROVED</strong></td>
     <td  align="center"><strong>DEPARTEMENT</strong></td>
    </tr>
    <tr>
       <td >&nbsp; </td>
      <td >&nbsp; </td>     
      <td >&nbsp; </td>
    </tr>
    <tr>
      <td >&nbsp; </td>
      <td >&nbsp; </td>     
      <td >&nbsp; </td>
    </tr>
    <tr>
       <td >&nbsp; </td>
      <td >&nbsp; </td>     
      <td >&nbsp; </td>
    </tr>
     <tr>
      <td >&nbsp; </td>
      <td >&nbsp; </td>     
      <td >&nbsp; </td>
    </tr>
    <tr>
      <td >&nbsp; </td>
      <td >&nbsp; </td>     
      <td >&nbsp; </td>
    </tr>
     <tr>
     <td align="center"> <hr style="color:#000000"> </td>
     <td align="center"> <hr style="color:#000000"></td>
     <td align="center"> <hr style="color:#000000"></td>
    </tr>
   
   
  </table>
 -->

  






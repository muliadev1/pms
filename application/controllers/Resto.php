<?php

class Resto extends CI_Controller{
	private $IdOperator;
	private $Operator;

	function __construct(){
		parent::__construct();
		if($this->session->userdata('Kode_userPms')==NULL) {
           redirect('Login');
		}
		$this->load->library(array('session','direct_print','create_pdf','ciqrcode'));
		$this->load->helper(array('form', 'url'));
		$this->load->model(array('M_resto','M_category','M_item','M_subcategory','M_stok_opname','M_rbac'));
		$this->IdOperator = $this->session->userdata('Kode_userPms');
		$this->Operator = $this->session->userdata('Nama_userPms');
		
		if (!empty($this->session->userdata("active_db"))) {
			$this->load->database($this->session->userdata("active_db"), FALSE, TRUE);
		}

	}

	function index(){
		redirect('Item/Item_view');
	}

	function Table_view(){
		$data['data'] = $this->M_resto->Table_view(); 
		$data['table'] = $this->M_resto->get_last_trx();
		//$this->direct_print->connectLokal('epsontm on Desktop-b7t2d5d');
		//print_r($print);exit();
		$this->load->view('resto/Table_list',$data);
	}
	function Op_closing(){
		$this->load->view('resto/Operator_closing');
	}
	function Cetak_home(){
		$data['operator'] = $this->M_resto->Operator_view(); 
		$this->load->view('resto/cetak/Home',$data);
	}
	function Op_closing_data(){
		 $data['master'] = $this->M_resto->Get_master_clossing($this->IdOperator); 
		$this->load->view('resto/Operator_closing_data',$data);
	}
	function select_guest(){
		$data['data'] = $this->M_resto->Guest_view();
		$this->load->view('Resto/Daftar_guest',$data);
	}
	function select_waiter(){
		$data['data'] = $this->M_resto->Waiter_view();
		$this->load->view('Resto/Daftar_waiter',$data);
	}
	function payment_process(){
		$idTrx = $this->input->post('idtrx');
		$data['data'] = $this->M_resto->Get_one_trx($idTrx)->row();
		//print_r($data['data']);exit();
		$this->load->view('Resto/payment_add',$data);
	}
	function Get_Setting_resto(){
		$setting = $this->M_resto->Get_Setting_resto();
		$Total = str_replace('.', '', $this->input->post('Total')); 
        $Total = str_replace(',', '.', $Total);
		$Disc = str_replace('.', '', $this->input->post('Disc')); 
        $Disc = str_replace(',', '.', $Disc);
        $AfterDiscount = str_replace('.', '', $this->input->post('AfterDiscount')); 
        $AfterDiscount = str_replace(',', '.', $AfterDiscount);
		 
		 $Tax = $setting->tax/100*$AfterDiscount;
         $Service= ($Tax+$AfterDiscount)*$setting->service/100 ;

         $grandtotal = $AfterDiscount+$Tax+$Service;
         $data= array('Total' =>$Total,
         			   'TotalNumber' =>number_format($Total, 2, ',', '.'),
         			   'Disc' =>$Disc,
         			   'DiscNumber' =>number_format($Disc, 2, ',', '.'),
         			   'AfterDiscount' =>$AfterDiscount,
         			   'AfterDiscountNumber' =>number_format($AfterDiscount, 2, ',', '.'),
         			   'Tax' =>$Tax,
         			   'TaxNumber' =>number_format($Tax, 2, ',', '.'),
         			   'Service' =>$Service,
         			   'ServiceNumber' =>number_format($Service, 2, ',', '.'),
         			   'Grandtotal' =>$grandtotal,
         			   'GrandtotalNumber' =>number_format($grandtotal, 2, ',', '.')
          );
         //print_r($data);exit();


		echo json_encode($data);
	}
	
	function split_bil(){
		$idTrx = $this->input->post('idtrx');
		$data['data'] = $this->M_resto->Get_one_trx_forsplitbill($idTrx);
		$data['payment'] = $this->M_resto->Get_article_payment();
		$this->load->view('Resto/Split_bil',$data);
	}
	function move_table(){
		$idTrx = $this->input->post('idtrx');
		$data['table'] = $this->M_resto->Table_view(); 
		$data['data'] = $this->M_resto->Get_one_trx_formobetable($idTrx);
		$this->load->view('Resto/Move_table',$data);
	}

	
	function Add_guest(){
		$data['nama'] = $this->input->post("status");
		$data['guestName'] = $this->input->post('guestName');
		$idTrx = $this->input->post('idtrx');
		$subreq = $this->M_resto->Resto_get_all_menusubcategory();
		foreach ($subreq->result() as $row) {
			$subcategory[$row->id] = $row->subcategory;
		}
		$data['masterTrx'] = NULL;
		if (!empty($idTrx)) {
			$data['masterTrx'] = $this->M_resto->Get_one_trx($idTrx)->row();
			$data['detil'] = $this->M_resto->Get_trx_menu_detail_add_guest($idTrx); 
		}
		$data['subcategory'] = $subcategory;
		$this->load->view('resto/Guest_add', $data);
	}

	function Add_tempbill1(){
		$datadetil = $this->input->post("datadetil");
		//print_r($datadetil);exit();
		$idTrx = $this->input->post('idtrx');
		$Jumlah= count($datadetil)-1;
		for ($i=0; $i <= $Jumlah ; $i++) { 
		    $disc = str_replace('.', '', $datadetil[$i][3]); 
            $disc = str_replace(',', '.', $disc);

			$data[] = array(
				'idtrx'=> $idTrx,
				'iddetil'=> $datadetil[$i][0],
				'disc'=> $disc,
				'ip'=>  $this->input->ip_address()
 			);

		}
		//print_r($data);exit();
		$this->M_resto->add_splitbill1_temp($data); 
	}

	function Get_menu_from_subcategory(){
		$id = $this->input->post('id');

		$req = $this->M_resto->Get_menu($id);

		foreach ($req as $row) {
			echo "<tr style='background-color: #00a65a;' class='tr-click' id-menu='".$row->id."' nm-menu='".$row->menu."'><td class='td-click'>".$row->menu."</td><td class='td-click'>Rp ".number_format($row->price, '2', ',', '.')."</td></tr>";
		}
	}

	function Add_guestDB(){
		//print_r($this->input->post());exit();
		//$data['data'] = $this->M_resto->Table_view();
		$this->load->view('resto/Guest_add');
	}

	

	function Count_total_menu(){
		$datas = $this->input->post('datas');
		$Total = str_replace(",", ".", $this->input->post('pmTotal'));
		$Disc = str_replace(",", ".", $this->input->post('pmDisc'));
		$Total2 = str_replace(",", ".", $this->input->post('pmTotal2'));
		$Service = str_replace(",", ".", $this->input->post('pmService'));
		$Tax = str_replace(",", ".", $this->input->post('pmTax'));
		$Grandtotal = str_replace(",", ".", $this->input->post('pmGrandTotal'));
		$masterTrx = $this->input->post('masterTRX');
		$subtotal = 0;

		if (empty($masterTrx)) {
			$Grandtotal = 0;
			$Tax = 0;
			$Service = 0;
			$Total = 0;
			$Total2 = 0;
			$Disc = 0;
		}else {
			$menuMaster = $this->M_resto->Get_one_trx($masterTrx)->row();
			$Grandtotal = $menuMaster->grandtotal;
			$Tax = $menuMaster->tax;
			$Service = $menuMaster->service;
			$Total = $menuMaster->total;
			$Total2 = $menuMaster->total2;
			$Disc = $menuMaster->tax;
		}
// print_r($Service);exit();
		if (!empty($datas)) {
			foreach ($datas as $key => $value) {
				$menu = $this->M_resto->Get_one_menu($value['ID']);
				$subtotal = $menu->price * str_replace(".", "", $value['QTY']);
				$Total += $subtotal;
				$subtotal = $menu->price * str_replace(".", "", $value['QTY']);
				$Total2 += $subtotal;
				$subtotal = $menu->tax * str_replace(".", "", $value['QTY']);
				$Tax += $subtotal;
				$subtotal = $menu->service * str_replace(".", "", $value['QTY']);
				$Service += $subtotal;
				$subtotal = $menu->finalprice * str_replace(".", "", $value['QTY']);
				$Grandtotal += $subtotal;
				$subtotal = 0;
			}
		}
echo "<div class='small-box bg-aqua'>
         <div class='inner'> 
		<table width='100%''>  
				<tr> 
		        <td align='center'style='background-color:#f39c12;'><strong>TOTAL</strong></td> 
		        <td align='center'style='background-color:#f39c12;'><strong>DISC</strong></td>
		        <td align='center'style='background-color:#f39c12;'><strong>TOTAL*</strong></td>
		      </tr>
		       <tr> 
		        <td align='right'><strong><input type='text'   value='".number_format($Total, 2, ',', '.')."'  name ='pmTotal' id='pmTotal' style='text-align: right;' class='form-control' readonly='readonly'></strong></td>
		        <td align='right'><strong><input type='text'  value='".number_format($Disc, 2, ',', '.')."'name ='pmDisc' id='pmDisc' style='text-align: right;' class='form-control' readonly='readonly'></strong></td>
		        <td align='right'><strong><input type='text'  value='".number_format($Total2, 2, ',', '.')."' name ='pmTotal2' id='pmTotal2' style='text-align: right;' class='form-control' readonly='readonly'></strong></td>
		      </tr>
		      <tr> 
		        <td align='center'style='background-color:#f39c12;'><strong>Service</strong></td>
		        <td align='center'style='background-color:#f39c12;'><strong>Tax</strong></td>
		        <td align='center'style='background-color:#f39c12;'><strong>Grandtotal</strong></td>
		      </tr>
		       <tr> 
		        <td align='right'><strong><input type='text'  value='".number_format($Service, 2, ',', '.')."' name ='pmService' id='pmService' style='text-align: right;' class='form-control' readonly='readonly'></strong></td> 
		        <td align='right'><strong><input type='text'  value='".number_format($Tax, 2, ',', '.')."' name ='pmTax' id='pmTax' style='text-align: right;' class='form-control' readonly='readonly'></strong></td> 
		        <td align='right'><strong><input type='text'  value='".number_format($Grandtotal, 2, ',', '.')."' name ='pmGrandTotal' id='pmGrandTotal' style='text-align: right;' class='form-control' readonly='readonly'></strong></td>
		      </tr>
		      </table>
		      </div>
		       <a href='#' class='small-box-footer'>Payment <i class='fa fa-money'></i></a>
		      </div>" ;
		// echo "<b class='pull-right' id='pmTotal'>".number_format($Total, 2, ',', '.')."</b> <br>
		// <b class='pull-right' id='pmDisc'>".number_format($Disc, 2, ',', '.')."</b> <br>
		// <b class='pull-right' id='pmTotal2'>".number_format($Total2, 2, ',', '.')."</b> <br>
		// <b class='pull-right' id='pmService' style='color:blue;'>".number_format($Service, 2, ',', '.')."</b> <br>
		// <b class='pull-right' id='pmTax'>".number_format($Tax, 2, ',', '.')."</b> <br>
		// <b class='pull-right' id='pmGrandTotal'>".number_format($Grandtotal, 2, ',', '.')."</b> <br>";
	}

	function addMenuToDB(){
		$datas = $this->input->post('datas');
		$operator = $this->input->post('operator');
		$guestId = $this->input->post('guestId');
		$newID = $this->input->post('payNewID');
		$tableId = $this->input->post('tableId');
		$idWaiter = $this->input->post('idWaiter');
		$WaiterName = $this->input->post('WaiterName');
		$OrderNoManual = $this->input->post('OrderNoManual');
		$pmTotal = str_replace(",", ".", $this->input->post('pmTotal'));
		$pmDisc = str_replace(",", ".", $this->input->post('pmDisc'));
		$pmTotal2 = str_replace(",", ".", $this->input->post('pmTotal2'));
		$pmService = str_replace(",", ".", $this->input->post('pmService'));
		$pmTax = str_replace(",", ".", $this->input->post('pmTax'));
		$pmGrandTotal = str_replace(",", ".", $this->input->post('pmGrandTotal'));
//print($WaiterName);exit();
		if (empty($newID)) {
			$newID = $this->M_resto->Add_new_menus($operator, $tableId,$idWaiter,$WaiterName);
		}
		if (!empty($datas)) {
			foreach ($datas as $key => $value) {
				$dMenu = $this->M_resto->Get_one_menu($value['ID']);
				$this->M_resto->Add_new_detail_menu($newID, $value['ID'], $dMenu->menu, $dMenu->price, str_replace(".", "", $value['QTY']), $value['REQUEST'], $operator,$idWaiter,$WaiterName,$OrderNoManual);
			}
		}

		$this->M_resto->Update_new_menus($newID, $guestId, $pmTotal, $pmDisc, $pmTotal2, $pmService, $pmTax, $pmGrandTotal);
		$this->M_resto->Set_tb_table($tableId, $guestId, "open");

		$fixedMenu = $this->M_resto->Get_trx_menu_detail($newID);
		foreach ($fixedMenu->result() as $row) {
			$price2 = $row->price - (($row->price*$row->disp)/100);
			echo "<tr style='background-color: #f9f9f9'>
			<td width='111px' style='background-color:#f39c12;'>".$row->menu."</td>
			<td width='98px' style='background-color:#f39c12;'>".number_format($row->price, 2, ',', '.')."</td>
			<td width='98px' style='background-color:#f39c12;'>".number_format($row->disp, 2, ',', '.')."</td>
			<td width='98px' style='background-color:#f39c12;'>".number_format($price2, 2, ',', '.')."</td>
			<td width='66px' style='background-color:#f39c12;'>".number_format($row->qty, 0, ',', '.')."</td>
			<td width='98px' style='background-color:#f39c12;'>".number_format($row->subtotal, 2, ',', '.')."</td>
			<td width='94px' colspan='2' style='background-color:#f39c12;'>".$row->idoperator."</td>
			</tr>";
		}
		echo '<tr style="display:none"><td><input type="text" id="payNewID" value="'.$newID.'"></td></tr>';
	}

	function add_payment(){
		$iddetil = $this->input->post('idDetil');
		$dism = $this->input->post('idsm');
		$subtotal = $this->input->post('subtotal');
		$price = $this->input->post('price');
		$pricenew = $this->input->post('price1');
		$totalMaster = str_replace('.', '', $this->input->post('TotalKanan'));       
		$discMaster = str_replace('.', '', $this->input->post('DiscKanan')); 
		$totalMaster1 = str_replace('.', '', $this->input->post('Total1Kanan')); 
		$service = $this->input->post('Service1');
		$tax = $this->input->post('Tax1');
		$grandTotal = $this->input->post('GrandTotal1');
		$payAmount = $this->input->post('PayAmount1'); 
		$remain = $this->input->post('Remain1'); 
		$note = $this->input->post('Note');  
		$idoperatoropenorder = $this->input->post('idoperatoropenorderKirim');

		$PayMethod= $this->input->post('PayMethod');
		$PayAmountPay= $this->input->post('PayAmountPay'); 
		$PayMethodPay= $this->input->post('PayMethodPay');  

		$idguest = $this->input->post('idguestKirim');
		$tableid = $this->input->post('tableidKirim');
		$Kodemaster = $this->M_resto->Load_kdmaster();
		$idTrx=$this->input->post('TrxId');

		$iddetil1 = array_combine(range(1, count($iddetil)), array_values($iddetil));
		$dism1 = array_combine(range(1, count($dism)), array_values($dism));
		$subtotal1 = array_combine(range(1, count($subtotal)), array_values($subtotal));
		$price1 = array_combine(range(1, count($price)), array_values($price));
		$pricenew1 = array_combine(range(1, count($pricenew)), array_values($pricenew));

		$PayAmountPay1 = array_combine(range(1, count($PayAmountPay)), array_values($PayAmountPay));
		$PayMethodPay1 = array_combine(range(1, count($PayMethodPay)), array_values($PayMethodPay));


	

		$dataMasterSplit = array(
				'trxid' => $Kodemaster,
				'trxdate' =>date('Y-m-d H:i:s'),
				'tableid' => $tableid,
				'idguest' => $idguest,
				'idreservation' => '-',
				'idroom' => '-',
				'idreservationcharge' => '-',
				'idroomcharge' => '-',
				'total' => $totalMaster,
				'disc' => $discMaster,
				'total2' => $totalMaster1,
				'service' => $service,
				'tax' => $tax,
				'grandtotal' => $grandTotal,
				'idarticle' => '-',
				'totalpayment' => $payAmount,
				'remain' => $remain,
				'statuspayment' => 1,
				'notes' => $note, 
				'paymentstatus' => $PayMethod, 
				'idoperatoropenorder' => $idoperatoropenorder,
				'idoperatorprocess' => $this->IdOperator 
			);
		$dataMasterCash = array(
				'trxdate' =>date('Y-m-d H:i:s'),
				'tableid' => $tableid,
				'idguest' => $idguest,
				'idreservation' => '-',
				'idroom' => '-',
				'idreservationcharge' => '-',
				'idroomcharge' => '-',
				'total' => $totalMaster,
				'disc' => $discMaster,
				'total2' => $totalMaster1,
				'service' => $service,
				'tax' => $tax,
				'grandtotal' => $grandTotal,
				'idarticle' => '-',
				'totalpayment' => $payAmount,
				'remain' => $remain,
				'statuspayment' => 1,
				'paymentstatus' => $PayMethod, 
				'notes' => $note, 
				'idoperatoropenorder' => $idoperatoropenorder,
				'idoperatorprocess' => $this->IdOperator 
			);

		$Jumlah = count($iddetil);
		for ($i=1; $i <= $Jumlah ; $i++){ 
			$dataDetiSplit[] = array(
				'id' => $iddetil1[$i],
				'trxid' => $Kodemaster,
				'dism' => $dism1[$i],
				'subtotal' => $subtotal1[$i] ,
				'price' => $price1[$i] ,
				'price1' => $pricenew1[$i] 
			);
			$dataDetilCash[] = array(
				'id' => $iddetil1[$i],
				'dism' => $dism1[$i],
				'subtotal' => $subtotal1[$i] ,
				'price' => $price1[$i] ,
				'price1' => $pricenew1[$i] 
			);
		}

		$datadiDetil =  $this->M_resto->Get_one_trx_forsplitbill($idTrx);
		$JumlahdatadiDetil =count($datadiDetil);
		$Jumlahdetilhitung = count($dataDetiSplit);

		if ($Jumlahdetilhitung<$JumlahdatadiDetil) {
			$Jumlahdetil = count($dataDetiSplit)-1;
				for ($x=0; $x <= $Jumlahdetil ; $x++) { 
					$this->M_resto->payment_add_splitbill_detil($dataDetiSplit[$x]);
					$datatemp[] = array( //temp bill2
						'idtrx'=> $Kodemaster,
						'iddetil'=> $dataDetiSplit[$x]['id'],
						'disc'=>  $dataDetiSplit[$x]['dism'],
						'ip'=>  $this->input->ip_address()
					);
				}
				//payment
			   if ($PayMethod=='1') {
				    $JumlahPay = count($PayAmountPay1);
					for ($p=1; $i <= $JumlahPay ; $p++){ 
						 $amountpay = str_replace('.', '', $PayAmountPay[$p]); 
                         $amountpay = str_replace(',', '.', $amountpay);
						$dataPayDetil[] = array(
							'idtrx'=> $Kodemaster,
							'idarticlepayment'=> $PayMethodPay1[$p],
							'amount'=>  $amountpay
						);
					}
					}else{
						$dataPayDetil[] = array(
							'idtrx'=> $Kodemaster,
							'idarticlepayment'=> -1,
							'amount'=> $grandTotal
						);
					}

			$this->M_resto->payment_add_splitbill_master($dataMasterSplit);
			$this->M_resto->add_splitbill1_temp($datatemp);
			$this->M_resto->add_paymentdetil($dataPayDetil); 
			

		}else{
			$Jumlahdetil = count($dataDetilCash)-1;
				for ($x=0; $x <= $Jumlahdetil ; $x++) { 
					$this->M_resto->payment_add_splitbill_detil_cash($dataDetilCash[$x]);
					$datatemp[] = array( //temp bill2
						'idtrx'=> $idTrx,
						'iddetil'=> $dataDetilCash[$x]['id'],
						'disc'=>  $dataDetilCash[$x]['dism'],
						'ip'=>  $this->input->ip_address()
					);
				}
				//payment
			   if ($PayMethod=='1') {
				    $JumlahPay = count($PayAmountPay1);
					for ($p=1; $p <= $JumlahPay ; $p++){ 
						 $amountpay = str_replace('.', '', $PayAmountPay[$p]); 
                         $amountpay = str_replace(',', '.', $amountpay);
						$dataPayDetil[] = array(
							'idtrx'=> $idTrx,
							'idarticlepayment'=> $PayMethodPay1[$p],
							'amount'=> $amountpay
						);
					}
					}else{
						$dataPayDetil[] = array(
							'idtrx'=> $idTrx,
							'idarticlepayment'=> -1,
							'amount'=> $grandTotal
						);
					}
					//print_r($dataPayDetil);exit();

			$this->M_resto->payment_add_splitbill_master_cash($dataMasterCash,$idTrx);
			$this->M_resto->add_splitbill1_temp($datatemp);
			$this->M_resto->add_paymentdetil($dataPayDetil); 
		}

		
		$this->session->set_flashdata('msg', 'Payment Add successfully...');
		$this->session->set_flashdata('cetakbill2', '1');
		redirect('Resto/Table_view');
	}










	function move_tableDB(){
		$iddetil = $this->input->post('idDetil');
		$dism = $this->input->post('idsm');
		$subtotal = $this->input->post('subtotal');
		$price = $this->input->post('price');
		$pricenew = $this->input->post('price1');
		$totalMaster = str_replace('.', '', $this->input->post('TotalKanan'));       
		$discMaster = str_replace('.', '', $this->input->post('DiscKanan')); 
		$totalMaster1 = str_replace('.', '', $this->input->post('Total1Kanan')); 
		$service = $this->input->post('Service1');
		$tax = $this->input->post('Tax1');
		$grandTotal = $this->input->post('GrandTotal1');
		$payAmount = $this->input->post('PayAmount1'); 
		$remain = $this->input->post('Remain1'); 
		$note = $this->input->post('Note');  
		$idoperatoropenorder = $this->input->post('idoperatoropenorderKirim');

		$idguest = $this->input->post('idguestKirim');
		$tableid = $this->input->post('tableidKirim');
		$Kodemaster = $this->M_resto->Load_kdmaster();
		$idTrx=$this->input->post('TrxId');
		$ToTable =$this->input->post('ToTable');

		$iddetil1 = array_combine(range(1, count($iddetil)), array_values($iddetil));
		$dism1 = array_combine(range(1, count($dism)), array_values($dism));
		$subtotal1 = array_combine(range(1, count($subtotal)), array_values($subtotal));
		$price1 = array_combine(range(1, count($price)), array_values($price));
		$pricenew1 = array_combine(range(1, count($pricenew)), array_values($pricenew));


		$dataMasterCash = array(
				'trxid' => $Kodemaster,
				'trxdate' =>date('Y-m-d H:i:s'),
				'tableid' => $ToTable,
				'idguest' => $idguest,
				'idreservation' => '-',
				'idroom' => '-',
				'idreservationcharge' => '-',
				'idroomcharge' => '-',
				'notes' => $note, 
				'idoperatoropenorder' => $idoperatoropenorder,
				'idoperatorprocess' => $this->IdOperator 
			);

		$Jumlah = count($iddetil);
		for ($i=1; $i <= $Jumlah ; $i++){ 
			$dataDetiSplit[] = array(
				'id' => $iddetil1[$i],
				'trxid' => $Kodemaster,
				'dism' => $dism1[$i],
				'subtotal' => $subtotal1[$i] ,
				'price' => $price1[$i] ,
				'price1' => $pricenew1[$i] 
			);
			$dataDetilCash[] = array(
				'id' => $iddetil1[$i],
				'dism' => $dism1[$i],
				'subtotal' => $subtotal1[$i] ,
				'price' => $price1[$i] ,
			);
		}
		$datatablebaru =  $this->M_resto->table_check_movetable($ToTable);
		$datatablelama=  $this->M_resto->table_check_detil_movetable_lama($idTrx);

		$JumlahdetilBaru= count($dataDetilCash);
		$JumlahdetilLama = count($datatablelama);
		
		
		if (!empty($datatablebaru)) {
			$trxidBaru = $datatablebaru[0]->trxid;
			$Jumlahdetil = count($dataDetilCash)-1;
				for ($x=0; $x <= $Jumlahdetil ; $x++) {
					$this->M_resto->update_detil_trx_formovetable($dataDetilCash[$x]['id'],$trxidBaru);
				}
				if ($JumlahdetilBaru==$JumlahdetilLama) {
					$this->M_resto->delete_master_trx_formovetable($idTrx);
				}
			
		}else{
			//print_r($dataDetilCash[$x]['id']);exit();
			$Jumlahdetil = count($dataDetilCash)-1;
			for ($x=0; $x <= $Jumlahdetil ; $x++) {
				$this->M_resto->add_movetable_emptytable_detil($dataDetilCash[$x]['id'],$Kodemaster);
				}
			$this->M_resto->add_movetable_emptytable_master($dataMasterCash);
			if ($JumlahdetilBaru==$JumlahdetilLama) {
					$this->M_resto->delete_master_trx_formovetable($idTrx);
				}
			 
		}
		//print_r($dataDetilCash);exit();
		


		
		
		$this->session->set_flashdata('msg', 'Payment Add successfully...');
		redirect('Resto/Table_view');
	}

  function Cetak_Order(){
  	 $IdOrder = $this->input->get('idorder', TRUE);
	 //print_r( $IdOrder);exit();
	  // $data['header'] = $this->Cetak("Memorandum_invoice",  $this->session->userdata('Nama_userPms'). date("d-m-Y_H-i-s"), 1);
	   $data['mastermenu'] = $this->M_resto->Get_trx_menu_master_cetak($IdOrder);
	   $data['detilmenu'] = $this->M_resto->Get_trx_menu_detail_cetak($IdOrder);
	   $data['periode'] =  array('idorder' => $IdOrder );
	   $this->load->view('resto/Cetak/Order',$data);
	   //print_r($html);exit();

	 // $this->create_pdf->load($html,'Resto-Order'.$IdOrder., 'A4-P','');	
	}

	  function Cetak_Bill1(){
	  // $data['header'] = $this->Cetak("Memorandum_invoice",  $this->session->userdata('Nama_userPms'). date("d-m-Y_H-i-s"), 1);
	    $IdOrder = $this->M_resto->select_splitbill1_temp();
	   $data['mastermenu'] = $this->M_resto->Get_trx_menu_master_cetak($IdOrder); 	    
	   $data['detilmenu'] = $this->M_resto->Get_trx_menu_detail_cetak($IdOrder);
	   $data['periode'] =  array('idorder' => $IdOrder );
	   //$this->M_resto->update_splitbill1_temp();
	   $this->load->view('resto/Cetak/Bill_1',$data);
	   //print_r($html);exit();

	 // $this->create_pdf->load($html,'Resto-Order'.$IdOrder., 'A4-P','');	
	}

	 function Cetak_Bill2(){
	    $IdOrder = $this->M_resto->select_splitbill1_temp();
	   $data['mastermenu'] = $this->M_resto->Get_trx_menu_master_cetak($IdOrder); 	     
	   $data['detilmenu'] = $this->M_resto->Get_trx_menu_detail_cetak($IdOrder);//print_r( $data['detilmenu']);exit();
	   $data['periode'] =  array('idorder' => $IdOrder );
	   $this->load->view('resto/Cetak/Bill_2',$data);
	}


	function Add_clossing(){
		$data= $this->input->post('data');
		$Jumlah = count($data)-1;
		$datamaster = array(
			'id_operator'=>'1',
			'date'=> date('Y-m-d H:i:s')
			);

		$idmaster = $this->M_resto->Add_clossing_master($datamaster);
		//print_r($idmaster);exit();
		for ($i=0; $i <= $Jumlah ; $i++) { 
		$dataClos[] = array(
			'idtrx'=> $data[$i][1],
			'idoperator'=>$this->IdOperator ,
			'date'=> date('Y-m-d H:i:s'),
			'idmaster'=>$idmaster,
			);
		$dataupdate = array(
			'isclosing'=>'1',
			'dateclosing'=> date('Y-m-d H:i:s')
			);
		$this->M_resto->update_clossing($dataupdate,$data[$i][1]);
	}
	$this->M_resto->Add_clossing($dataClos);
	//print_r($dataClos);exit();
	$this->session->set_flashdata('idmaster', $idmaster);	
	}

	function Cetak($JenisLaporan,$Text){
		$data['hotel'] = $this->M_rbac->getPerusahaan();
		$data['qr'] = $this->ciqrcode->generateQRcode($JenisLaporan, $Text,1.9);
		$data1= $this->load->view('template/Cetak_head',$data, TRUE);
		return $data1;
	}


	function Cetak_closing(){
	   $data['header'] = $this->Cetak("Rekap_clossing",  $this->session->userdata('Nama_userPms'). date("d-m-Y_H-i-s"), 1);
	    $idmaster = $this->input->get('idmaster', TRUE);
	  $data['master'] = $this->M_resto->Get_master_clossing_cetak($this->IdOperator,$idmaster);
	  $data['dataclos'] = $this->M_resto->Get_clossing_master_cetak($this->IdOperator,$idmaster); 
	  $html= $this->load->view('resto/Cetak/Operator_closing',$data,TRUE);
	  //  print_r($html);exit();
	  $this->create_pdf->load($html,'Operator_closing'.' '.$this->Operator, 'A4-L','');	
	}
   function Select_clossing_byop(){
   	$IdOperator = $this->input->post('idop', TRUE);
 	 $date = $this->M_resto->Select_clossing_byop($IdOperator);
		    echo ' <div class="form-group" id="groupSubcategory">';
		    echo ' <label>Subcategory</label> ';
		    echo  '<select id="DateClosing" class="form-control select2"  style="width: 100%;"  ';
		    echo ' <option value="" >Semua Subcategory</option>';
		    foreach ($date as $pek ) {
		                    echo ' <option value="'.$pek->id.'" >'.$pek->tgl.'</option>';
		    }
		    echo '</select>';
		    echo '</div>';

   }

}
?>

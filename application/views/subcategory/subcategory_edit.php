<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>

<!-- Content Header (Page header) -->
<section class="content-header">
   <div class="btn-group btn-breadcrumb">
            <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
             <a href="<?php echo base_url('index.php/Category/Category_view');?>" class="btn btn-default  btn-xs">Category</a>
            <a  class="btn btn-default  btn-xs active">Edit SubCategory</a>
        </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
             <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Edit Item Sub Category</h3>
                </div>
                <div class="box-body">
                    <form method="post" id="Simpan" action="<?php echo base_url(). 'index.php/Subcategory/Subcategory_editDB'; ?>">
                    <div class="form-group">
                      <label>Category</label>
                      <select class="form-control select2" style="width: 100%;" name="idcategory">
                      <?php
                          foreach($data as $u){
                            if ($data2[0]->idcategory == $u->idcategory) {
                      ?>
                              <option selected="selected"  value="<?php echo $u->idcategory; ?>" ><?php echo $u->category; ?></option>
                      <?php
                              # code...
                            }else{
                      ?>
                              <option  value="<?php echo $u->idcategory; ?>" ><?php echo $u->category; ?></option>
                      <?php
                            }
                      ?>
                        <?php } ?>
                      </select>
                    </div>
                    <div class="form-group">
                            <label class="control-label">Sub Category</label>
                            <input type="text" name="subcategory" value="<?php echo $data2[0]->subcategory; ?>"  data-validation="length" data-validation-length="min4" data-validation-error-msg="Please fill out sub category name..."  class="form-control"  placeholder="">
                            <input type="hidden" name="Id"  value="<?php echo $data2[0]->idsubcategory; ?>" class="form-control"  placeholder="">

                    </div>
                    <div class="form-group">
                            <label class="control-label">Description</label>
                            <input type="text" name="description"  value="<?php echo $data2[0]->description; ?>" data-validation="length"    class="form-control">
                    </div>
                    <div class="form-group">
                          <label>
                            <?php if($data2[0]->isaset == '1') { ?>
                            <input id="aset" checked="checked" class="flat-green" type="checkbox" name="isAset" value="1">
                            <?php } else { ?>
                            <input id="aset" class="flat-green" type="checkbox" name="isAset" value="0">
                            <?php } ?>
                            Aset
                          </label>
                    </div>
                   <div class="form-group">
                      <button type="submit" value="Validate" class="btn btn-default"><i class='glyphicon glyphicon-ok'></i> Save</button>
                    </div>
                    </form>
                </div>
               </div>
         </div>
    </div>


</section>



<script type="text/javascript">
$("#Simpan").submit(function() {
    var subcategory = $('#subcategory').val();
     var description = $('#description').val();
        if (subcategory == ''|| description==''){
            File_Kosong(); return false; 
        }else{
        event.preventDefault();
        $.confirm({
          title: 'Confirmation',
          content: 'Are You Sure to Save?',
           type: 'blue',
          buttons: {
              Simpan: function () {
                  $.LoadingOverlay("show");
                  $("#Simpan").submit();
              },
              Batal: function () {
                
                  $.alert('Data Tidak Disimpan...');
              },
          }
      });
    } 

});
</script>

<?php
$this->load->view('template/Js');
$this->load->view('template/Foot');
?>



<script type="text/javascript">
  function File_Kosong() {
  $.alert({
    title: 'Caution!!',
    content: 'Transaction Is Invalid!',
    icon: 'fa fa-warning',
    type: 'orange',
});
}
</script>

<?php
$this->load->view('template/Foot');
$this->load->view('template/Js');
?>

<script type="text/javascript">
    $('input[type="checkbox"].flat-green, input[type="radio"].flat-green').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });
</script>

<script type="text/javascript">
  $('#aset').on('ifChanged',function(){
    $('#aset').val('1');
  });
</script>




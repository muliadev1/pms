
      <form method="post" id="Simpan" action="<?php echo base_url(). 'index.php/Stok_opname/add_proses_opname'; ?>">
     <!--  <input type="hidden" name="Istok"  value="<?php echo $departement['istok'] ;?>">
      <input type="hidden" name="Set"  value="<?php echo $departement['set'] ;?>">
      <input type="hidden" name="Departement"  value="<?php echo $departement['dep'] ;?>">
      <input type="hidden" name="iddepartement"  value="<?php echo $departement['iddep'] ;?>"> -->
        <div class="table-responsive">
           <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr >
                                        <th>Item Code</th>
                                        <th>Description</th>
                                        <th>Stok Awal</th>
                                        <th>Stok Masuk</th>
                                        <th>Stok Keluar</th>
                                        <th>Sisa Stok</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    foreach($data as $u){
                      $DatastokIn=  $this->M_item->load_stokIN_by_item($where['iddep'],$u->codeitem,$where['DateFrom'],$where['DateTo']);
                      $DatastokOut=  $this->M_item->load_stokOut_by_item($where['iddep'],$u->codeitem,$where['DateFrom'],$where['DateTo']);
                       $DatastokAwal= $this->M_item->load_stok_awal($where['iddep'],$where['Subcategory'],$u->codeitem,$where['DateFrom']) ;
                     // print_r( $DatastokAwal);exit();
                                    if (!empty($DatastokIn)) {
                                     $stokIn = $DatastokIn->stok;
                                    }else{
                                      $stokIn = '0';
                                    }
                                    if (!empty($DatastokOut)) {
                                     $stokOut = $DatastokOut->stok;
                                    }else{
                                      $stokOut = '0';
                                    }
                                    if (!empty($DatastokAwal)) {
                                     $stokAwal = $DatastokAwal->stock;
                                    }else{
                                      $stokAwal = '0';
                                    }
                                     $stokFinal =  $stokAwal-$stokIn-$stokOut;

                                    ?>
                                    <tr  class='odd gradeX context'>
                                        <td class="codeitem"><?php echo $u->codeitem ?></td>
                                        <td><?php echo $u->description?></td>
                                        <td align="right" ><?php echo  number_format($stokAwal, 2, ',', '.') ?></td>
                                        <td align="right"> <?php echo  number_format($stokIn, 2, ',', '.') ?></td>
                                        <td align="right"> <?php echo  number_format($stokOut, 2, ',', '.') ?></td>
                                        <td align="right"> <?php echo  number_format($stokFinal, 2, ',', '.') ?></td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
          </div>



        </div><!-- /.box-body -->
        </form>
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->


<script type="text/javascript">
  $(document).ready(function(){
     $(".ViewDetail").click(function(e){ 
     var dep = $('#IdDepartement').val();  
      var codeaset = $(this).closest('tr').children('td.codeaset').text(); 
      var namadep =  $("#IdDepartement option:selected").text();
       var namaaset = $(this).closest('tr').children('td.desc').text();     
      $.ajax({
        type: "POST",
       url: '<?php echo site_url('Aset/Aset_view_detil');?>',
        data: {dep: dep, codeaset:codeaset  },
        success: function(data){
            e.preventDefault();
            var mymodal1 = $('#myModalNew');
            mymodal1.modal({backdrop: 'static', keyboard: false}) 
            var height = $(window).height() - 200;
            mymodal1.find(".modal-body").css("max-height", height);
             $("#CodeAsetm span").text(namaaset);
              $("#Departementm span").text(namadep);
            mymodal1.find('.modal-body').html(data);
            mymodal1.modal('show');            
        }
      });
    });
  });
</script>


<script type="text/javascript">
  function File_Kosong() {
  $.alert({
    title: 'Caution!!',
    content: 'Transaction Is Invalid!',
    icon: 'fa fa-warning',
    type: 'orange',
});
}
</script>

<script>
  $(function () {
    
    $('#example1').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
  });
</script>

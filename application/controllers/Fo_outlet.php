<?php

class Fo_outlet extends CI_Controller{
	private $IdOperator;
	private $Operator;

	function __construct(){
		parent::__construct();
		if($this->session->userdata('Kode_userPms')==NULL) {
           redirect('Login');
		}
		$this->load->library(array('session','country','create_pdf','ciqrcode'));
		$this->load->helper(array('form', 'url'));
		$this->load->model(array('M_fo_outlet','M_guest','M_roomtype','M_room'));
		$this->IdOperator = $this->session->userdata('Kode_userPms');
		$this->Operator = $this->session->userdata('Nama_userPms');
		
		if (!empty($this->session->userdata("active_db"))) {
			$this->load->database($this->session->userdata("active_db"), FALSE, TRUE);
		}
	}

	function index(){
		redirect('Fo_outlet/Fo_outlet_view');
	}

	function Fo_outlet_view(){
		$data['data'] = $this->M_fo_outlet->FO_view();
		$this->load->view('fo_outlet/Fo_outlet',$data);
	}
	function Fo_outlet_view1(){
		$data['data'] = $this->M_fo_outlet->FO_view();
		$this->load->view('fo_outlet/Fo_outlet',$data);
	}

	function Op_closing(){
		$this->load->view('fo_outlet/Operator_closing');
	}
	function Na_view(){
		$this->load->view('fo_outlet/Na');
	}
	
	function Op_closing_data(){
		$data['dataclosing'] = $this->M_fo_outlet->Op_closing_data();
		$this->load->view('fo_outlet/Operator_closing_data',$data);
	}
	function Na_view_data(){
		$this->load->view('fo_outlet/Na_data');
	}


	function Reservation_list_view(){
		$data['data'] = $this->M_fo_outlet->Reservation_list_view();
		$this->load->view('fo_outlet/Reservation_list',$data);
	}
	function checkin($idreservation){
		$data['room'] = $this->M_fo_outlet->get_room();
		$data['roomtype'] = $this->M_fo_outlet->get_roomtype();
		$data['company'] = $this->M_fo_outlet->Company_view();
		$data['package'] = $this->M_fo_outlet->get_package();
		$data['rsvstatus'] = $this->M_fo_outlet->get_rsvstatus();
		$data['purpose'] = $this->M_fo_outlet->get_purpose();
		$data['source'] = $this->M_fo_outlet->get_source();
		$data['segment'] = $this->M_fo_outlet->get_segment();
		$data['article_payment'] = $this->M_fo_outlet->get_articlepayment();

		$data['guest'] = $this->M_fo_outlet->checkin_guest($idreservation);
		$data['guestrsvdetil'] = $this->M_fo_outlet->checkin_guest_rsv_detil($idreservation);//print_r($data['guestrsvdetil']);exit();
		$data['guestdetil'] = $this->M_fo_outlet->checkin_guest_detil($idreservation);
		$data['guesfodetil'] = $this->M_fo_outlet->checkin_guest_fo_detil($idreservation); 
		$this->load->view('fo_outlet/Checkin',$data);
	}

	function Fo_invoice_view($IdReservation){
		$data['article_sales'] = $this->M_fo_outlet->get_article_sales();
		$data['article_payment'] = $this->M_fo_outlet->get_article_payment();
		$data['invoice'] = $this->M_fo_outlet->get_fo_invoice($IdReservation);
		$data['guest'] = $this->M_fo_outlet->checkin_guest($IdReservation);
		$data['guestrsvdetil'] = $this->M_fo_outlet->checkin_guest_rsv_detil($IdReservation);
		$data['roomtype'] = $this->M_roomtype->RoomType_view_edit($data['guestrsvdetil']->idroomtype);
		$data['room'] = $this->M_room->Room_view_edit($data['guestrsvdetil']->idroom);
		$data['IdReservation'] = $IdReservation;
		// print_r($data);
		$this->load->view('fo_outlet/Fo_invoicing',$data);
	}

	function Select_package_by_code(){
		$idpackage = $this->input->post('id');
		$data = $this->M_fo_outlet->get_package_by_code($idpackage);
		echo json_encode($data->packagerate);	
	}

	function reservation(){
		$data['room'] = $this->M_fo_outlet->get_room();
		$data['roomtype'] = $this->M_fo_outlet->get_roomtype();
		$data['company'] = $this->M_fo_outlet->Company_view();
		$data['package'] = $this->M_fo_outlet->get_package();
		$data['rsvstatus'] = $this->M_fo_outlet->get_rsvstatus();
		$data['purpose'] = $this->M_fo_outlet->get_purpose();
		$data['source'] = $this->M_fo_outlet->get_source();
		$data['segment'] = $this->M_fo_outlet->get_segment();
		$data['article_payment'] = $this->M_fo_outlet->get_articlepayment();
		$this->load->view('fo_outlet/Reservation',$data);
	}

	function reservation_update($idresv){
		$data['rsv'] = $this->M_fo_outlet->Reservation_list_view_bykode($idresv);
		$data['rsvdetil'] = $this->M_fo_outlet->Reservation_list_view_bykode_detil($idresv); //print_r($data['rsvdetil']);exit();
		$data['rsvdetilguest'] = $this->M_fo_outlet->Reservation_list_view_bykode_detilguest($idresv); 
		$data['room'] = $this->M_fo_outlet->get_room();
		$data['roomtype'] = $this->M_fo_outlet->get_roomtype();
		$data['company'] = $this->M_fo_outlet->Company_view();
		$data['package'] = $this->M_fo_outlet->get_package();
		$data['rsvstatus'] = $this->M_fo_outlet->get_rsvstatus();
		$data['purpose'] = $this->M_fo_outlet->get_purpose();
		$data['source'] = $this->M_fo_outlet->get_source();
		$data['segment'] = $this->M_fo_outlet->get_segment();

		$this->load->view('fo_outlet/Reservation_update',$data);
	}

	function select_room_no(){
		$idtype = $this->input->post('id');
		$room = $this->M_fo_outlet->get_room_by_type($idtype);
		echo  '<select id="RoomNo" class="form-control select2"  style="width: 100%;" name="RoomNo"> ';
		foreach ($room as $pek ) {
                    echo ' <option value="'.$pek->id.'" >'.$pek->roomname.'</option>';
		}
		echo '</select>';
	}
	function get_room_staus(){
		$idroom = $this->input->post('idroom');
		$data = $this->M_fo_outlet->get_room_staus($idroom);
		
		echo json_encode($data);
	}
	

	function Guest_view(){
		$status = $this->input->post('status');
		$data['status'] = array('status' =>$status );
		$data['data'] = $this->M_fo_outlet->Guest_view();
		$this->load->view('fo_outlet/Daftar_guest',$data);
	}
	function Guest_add(){
		$data['country'] = $this->country->SelectCountry();
		$this->load->view('fo_outlet/Guest_add',$data);
	}
	function Res_add(){
		$idreservation = $this->M_fo_outlet->get_kodersv('tbreservation');
		$idroomtype = $this->input->post('RoomType');
		$idroom = $this->input->post('RoomNo');
		$idreservationstatus = $this->input->post('RsvStatus');
		$idguest = $this->input->post('IdGuest');
		$adult = $this->input->post('Adult');
		$child = $this->input->post('Child');

		$VIP1 = $this->input->post('VIP1');

		$checkin = $this->input->post('checkin');
		$checkin1 = DateTime::createFromFormat('m/d/Y',$checkin);
		$checkin2 = $checkin1->format("Y-m-d");

		$checkout = $this->input->post('checkout');
		$checkout1 = DateTime::createFromFormat('m/d/Y',$checkout);
		$checkout2 = $checkout1->format("Y-m-d"); 

		$cod = $this->input->post('cod');
		$cod1 = DateTime::createFromFormat('m/d/Y',$cod);
		$cod2 = $cod1->format("Y-m-d"); 

		$duration = $this->input->post('Duration');
		$idpurpose = $this->input->post('Purpose');
		$idsource = $this->input->post('Source');
		$idsegment = $this->input->post('Segment'); 
		$statusreservation = $this->input->post('RsvStatus'); 
		$idagent = $this->input->post('Agent');
		$idcompany = $this->input->post('Company');
		$deposit = $this->input->post('BiayaDeposit');
		$package = $this->input->post('Package');
		$ratePackage  = $this->input->post('RatePackage');

		$payment  = $this->input->post('RatePackage');
		$NoInvoice = $this->input->post('NoInvoice');

		$idGuestAll = $this->input->post('idguestAll');
		if (!empty($idGuestAll)) {
			$idGuestAll1 = array_combine(range(1, count($idGuestAll)), array_values($idGuestAll));
			 $Jumlah = count($idGuestAll1);
				 for ($i=1; $i <= $Jumlah ; $i++) { 
					$dataDetilGuest[] =  array('idreservation' =>$idreservation , 
											  'idguest' => $idGuestAll1[$i]
											);
				}
			
		} else {
			$dataDetilGuest[] = array('idreservation' =>$idreservation,
									  'idguest' => $idguest);
		}
		

		for ($i=1; $i <= $duration ; $i++) { 
			$dataDetil[] = array(
				'idreservation' =>$idreservation,
				'nightk' => $i,
				'datep' => $checkin2,
				'idroomtype' => $idroomtype,
				'idroom' => $idroom,
				'rate' => $ratePackage,
				'remarkpackage' => '-',
				'idpackage' => $package
				
				);
		}

	$isSaveCheckin = $this->input->post('isSaveCheckin');
	if ($isSaveCheckin == 1) {
		$statusRSV = 4;
	}else{
		$statusRSV = $idreservationstatus;
	}

		$data = array(
			'idreservation' =>$idreservation,
			'inputdate' => date('Y-m-d H:i:s'),
			'isgroup' =>'0',
			'idparentgroup' => '0',
			'idroomtype' =>$idroomtype,
			'idroom' =>$idroom,
			'idreservationstatus' =>$statusRSV,
			'idguest' =>$idguest,
			'adult' =>$adult ,
			'child' =>$child,
			'checkin' =>$checkin2,
			'checkout' =>$checkout2,
			'duration' =>$duration,
			'idpurpose' =>$idpurpose,
			'idsource' =>$idsource,
			'idsegment' =>$idsegment,
			'isvip' =>$VIP1,
			'statusreservation' =>$statusreservation,
			'notes' =>'-',
			'msg' =>'-',
			'idcompany' =>$idcompany,
			'idagent' =>$idagent,
			'deposit' =>$deposit,
			'kodeop' =>$this->IdOperator,
			'cod' =>$cod2
			);
		//print_r($data);exit();
	       
			 
	$this->M_fo_outlet->add_rsv($data,$dataDetilGuest,$dataDetil);

	if ($isSaveCheckin==1) {
		// $idfotrx = $this->M_fo_outlet->get_fotrx('tbfotrx');
		$datafo = array(
			'kodemaster' =>$idreservation,
			'tgl' =>date('Y-m-d H:i:s'),
			'kodereservation' =>$idreservation,
		);

		$datafodetil[] = array(
			'kodemaster' =>$idreservation,
			'invoicemanual' =>$NoInvoice,
			'idroom' =>$idroom,
			'tgl' =>$checkin2,
			'tipe' =>$idroomtype,
			'idarticle' =>$package,
			'articlename' =>'-',
			'desc' =>'Deposit',
			'charge' =>'-',
			'payment' =>$deposit,
			'idoperator' =>$this->IdOperator,
		);
		//print_r($datafodetil);exit();
		$this->M_fo_outlet->add_fotrx($datafo,$datafodetil);
		
		$kodebill = $this->M_fo_outlet->get_kodebill('tbmasterbill');
			$databillmaster = array(
				'kodemasterbil'=>$kodebill,
				'tgl'=>date('Y-m-d H:i:s'),
				'bilto'=>$idcompany,
				'charge'=>$deposit,
				'payment'=>$deposit,
				'balance'=>$deposit,
				'idoperator'=>$this->IdOperator,
				'isroombil'=>0,
				'idguest'=>$idguest,
				'idreservation'=>$idreservation
			);
			//print_r($databillmaster);exit();

	$databilldetil[] = array(
			'kodemasterbil'=>$kodebill,
			'idreservation'=>$idreservation,
			'iddetiltrx'=>$kodebill,
			'invoicemanual'=>'-',
			'idroom'=>$idroom,
			'tgl'=>date('Y-m-d H:i:s'),
			'ischarge'=>1,
			'idarticle'=>$idreservation,
			'articlename'=>$package,
			'desc'=>'-',
			'charge'=>$deposit,
			'payment'=>$deposit,
			'idoperator'=>$this->IdOperator
		);
			$this->M_fo_outlet->add_biltrx($databillmaster,$databilldetil);
		

	}


	$this->session->set_flashdata('msg', ' Add Reservation successfully... ');
	if ($isSaveCheckin == 1) {
		redirect('Reservation/GuestInHouse_view');
	}else{
		redirect('Fo_outlet/Reservation_list_view');
	}
	// redirect('Fo_outlet/reservation');
	}

	function Fo_invoice_add_sales(){

		 $charge = str_replace('.', '', $this->input->post('Charge')); 
         $charge = str_replace(',', '.', $charge);

		$datafodetil[] = array(
			'kodemaster' => $this->input->post('idRsv'),
			'invoicemanual' =>$this->input->post('Invoice'),
			'idroom' =>$this->input->post('idRoom'),
			'tgl' =>$this->session->userdata('tgl_na'),
			'tipe' =>$this->input->post('idRoomType'), 
			'idarticle' =>$this->input->post('idAs'), 
			'articlename' =>$this->input->post('NamaAs'), 
			'desc' =>$this->input->post('NamaAs'),
			'charge' =>$charge,
			'payment' =>0,
			'idoperator' =>$this->IdOperator
		);
		//print_r($datafodetil);exit();
		$this->M_fo_outlet->add_fotrx_invoice($datafodetil);
		$this->session->set_flashdata('msg', ' Add Transsaction successfully... ');
		//redirect('Fo_outlet/Fo_invoice_view/'.$this->input->post('idRsv'));
	}

	function Fo_invoice_add_payment(){

		 $payment = str_replace('.', '', $this->input->post('Payment')); 
         $payment = str_replace(',', '.', $payment);

		$datafodetil[] = array(
			'kodemaster' => $this->input->post('idRsv'),
			'invoicemanual' =>$this->input->post('Invoice'),
			'idroom' =>$this->input->post('idRoom'),
			'tgl' =>$this->session->userdata('tgl_na'),
			'tipe' =>$this->input->post('idRoomType'), 
			'idarticle' =>$this->input->post('idarticle'), 
			'articlename' =>$this->input->post('articlename'), 
			'desc' => $this->input->post('articlename').' : '.$this->input->post('DescriptionPay'),
			'charge' =>0,
			'payment' =>$payment,
			'idoperator' =>$this->IdOperator
		);
		//print_r($datafodetil);exit();
		$this->M_fo_outlet->add_fotrx_invoice($datafodetil);
		$this->session->set_flashdata('msg', ' Add Transsaction successfully... ');
		//redirect('Fo_outlet/Fo_invoice_view/'.$this->input->post('idRsv'));
	}


	function Na_addDB(){
		$data = $this->input->post('data');

		$lastNA = $this->db->query("SELECT * FROM tbna ORDER BY tglna DESC LIMIT 1")->row();

		$jumlah = count($data);
		for ($i=0; $i <$jumlah ; $i++) { 
			if ($data[$i][4]=='FO') { 
				$dataDetilRsv = $this->M_fo_outlet->get_detil_rsv($data[$i][7]);
				$datafodetil[] = array(
					'kodemaster' => $dataDetilRsv->idreservation,
					'invoicemanual' =>'Room Charge (NA)',
					'idroom' =>$dataDetilRsv->idroom,
					'tgl' =>date('Y-m-d H:i:s'),
					'tipe' =>$dataDetilRsv->idroomtype,
					'idarticle' =>'CHG001', 
					'articlename' =>'Room', 
					'desc' =>'Room Charge (NA)',
					'charge' =>$dataDetilRsv->rate,
					'payment' =>0,
					'isna' =>1,
					'dateclosing'=>date('Y-m-d H:i:s'),
					'isclosing'=>1,
					// 'tglna' =>date('Y-m-d H:i:s'),
					'tglna' =>$lastNA->tglna,
					'night' =>$dataDetilRsv->nightk,
					'idoperator' =>$this->IdOperator
				);
				$updNAdetil[] = array('kodemaster' => $dataDetilRsv->idreservation, 'isna' => 1, 'tglna' => $lastNA->tglna);
				$dataiddetilRsv[] = array(
					'id' => $data[$i][7]
				);
			}else{
				$dataiddetilfb[] = array(
					'id' => $data[$i][7]
				);
			}	
		}
		$dataFb = array(
		    'isNA'=>1,
			'dateclosingNA'=>$lastNA->tglna
		);
		$result = $this->M_fo_outlet->Na_addDB($datafodetil,$dataiddetilRsv,$dataFb,$dataiddetilfb,$updNAdetil);
		// print_r($this->db->last_query());
		// exit();

		if ($result) {
			$newNA = strtotime("+1 day", strtotime($lastNA->tglna));
			$newNA = date('Y-m-d', $newNA).' '.date('H:i:s');
			$this->db->insert('tbna', array('tglna' => $newNA, 'op' => $this->IdOperator));

			$dNA = $this->db->query("SELECT DATE_FORMAT(tglna,'%Y-%m-%d') as tgl FROM tbna ORDER BY tglna DESC LIMIT 1")->row();
			$this->session->set_userdata(array('tgl_na' => $dNA->tgl));
		}

		$this->session->set_flashdata('msg', ' Add NA successfully... ');
	}


	function Guest_addDB(){
		date_default_timezone_set('Asia/Hong_Kong');
		$dateInput = DateTime::createFromFormat('m/d/Y',$this->input->post('birthday'));
		$date = $dateInput->format("Y-m-d");
		$data = array(
			'saluation' => $this->input->post('saluation'),
			'firstname' => $this->input->post('firstname'),
			'lastname' => $this->input->post('lastname'),
			'address' => $this->input->post('address'),
			'idstate' => $this->input->post('state'),
			'state' => $this->input->post('state'),
			'zipcode' => $this->input->post('zipcode'),
			'email' => $this->input->post('email'),
			'phone' => $this->input->post('phone'),
			'gender' => $this->input->post('gender'),
			'birthday' => $date,
			'idtype' => $this->input->post('idtype'),
			'idnumber' => $this->input->post('idnumber'),
			'description' => $this->input->post('description'),
			'currtimestamp' => date('Y-m-d H:i:s'),
			'isaktif' => '1'
		);
		// print_r($data); exit();
		$this->M_fo_outlet->add_guest($data);
	}

function checkoutAddDB(){
		$dataRoom = $this->input->post('datarooms');
		$dataCon = $this->input->post('datacoms');
		$dataSum = $this->input->post('datasums');
		$idreservation = $this->input->post('idreservation');
		$jumlahRoom = count($dataRoom);
		$jumlahCon = count($dataCon);
		$jumlahSum = count($dataSum);

		$idmaster = $this->M_fo_outlet->get_kodecheckout();
		$dataMaster = array(
			'id'=> $idmaster,
			'idreservation'=> $idreservation,
			'datecheckout'=> date('Y-m-d H:i:s'),
			'idoperator'=> $this->IdOperator
		);

	   for($i=0; $i <$jumlahRoom ; $i++) { 
	   	 $TglRom1 = DateTime::createFromFormat('m-d-Y',$dataRoom[$i][1]);
	     $TglRom2 = $TglRom1->format("Y-m-d");
			$dataRoomDetil[] = array(
					'idmaster'=> $idmaster,
					'date'=> $TglRom2,
					'invoicemanual'=> $dataRoom[$i][2],
					'description'=> $dataRoom[$i][3],
					'charge'=> $dataRoom[$i][4],
					'payment'=> $dataRoom[$i][5],
					'trxid'=> $dataRoom[$i][6],
					'status'=> 1
				);			
	    }


	   for($i=0; $i <$jumlahCon ; $i++) { 
	   	 $TglCon1 = DateTime::createFromFormat('m-d-Y',$dataCon[$i][1]);
	     $TglCon2 = $TglCon1->format("Y-m-d");
			$dataConDetil[] = array(
					'idmaster'=> $idmaster,
					'date'=> $TglCon2,
					'invoicemanual'=> $dataCon[$i][2],
					'description'=> $dataCon[$i][3],
					'charge'=> $dataCon[$i][4],
					'payment'=> $dataCon[$i][5],
					'trxid'=> $dataCon[$i][6],
					'status'=> 2
				);			
	    }

	    for($i=0; $i <$jumlahSum ; $i++) { 
	     $TglSum1 = DateTime::createFromFormat('m-d-Y',$dataSum[$i][1]);
	     $TglSum2 = $TglSum1->format("Y-m-d");
			$dataSumDetil[] = array(
					'idmaster'=> $idmaster,
					'date'=> $TglSum2,
					'invoicemanual'=> $dataSum[$i][2],
					'description'=> $dataSum[$i][3],
					'charge'=> $dataSum[$i][4],
					'payment'=> $dataSum[$i][5],
					'trxid'=> $dataSum[$i][6],
					'status'=> 3
				);			
	    }
	   // print_r($dataMaster);exit();
	  $this->M_fo_outlet->checkoutAddDB($idreservation,$dataMaster,$dataRoomDetil,$dataConDetil,$dataSumDetil);
		

		$this->session->set_flashdata('msg', ' Checkout successfully... ');
	}

	function checkin_add(){
		date_default_timezone_set('Asia/Hong_Kong');
		$IdReservation = $this->input->post('IdReservation');
		$duration = $this->input->post('Duration');
		$deposit = $this->input->post('BiayaDeposit');

		$checkin = $this->input->post('checkin');
		$checkin1 = DateTime::createFromFormat('m/d/Y',$checkin);
		$checkin2 = $checkin1->format("Y-m-d");

		$checkout = $this->input->post('checkout');
		$checkout1 = DateTime::createFromFormat('m/d/Y',$checkout);
		$checkout2 = $checkout1->format("Y-m-d");


		$data = array(
			'checkin' =>$checkin2,
			'checkout' =>$checkout2,
			'duration' =>$duration,
			'deposit' =>$deposit,
			'idreservationstatus' => 4
			);
		
		$this->M_fo_outlet->checkin_add($data,$IdReservation);

		$datafodetil = array(
			'payment' =>$deposit
		);

		$this->M_fo_outlet->update_fotrx($datafodetil,$IdReservation);
		
			$databillmaster = array(
				'charge'=>$deposit,
				'payment'=>$deposit,
				'balance'=>$deposit
			);
			$databilldetil = array(
					'charge'=>$deposit,
					'payment'=>$deposit
				);
			$this->M_fo_outlet->update_biltrx($databillmaster,$databilldetil,$IdReservation);

		redirect('reservation/ExpectedArrival_view');
	}


	function Add_clossing(){

		$data = $this->input->post('data');
		$jumlah = count($data);
		for ($i=0; $i <$jumlah ; $i++) { 
			$data1 = array(
				'isclosing' => 1,
				'dateclosing' =>date('Y-m-d H:i:s')
			);
			$this->M_fo_outlet->Add_clossing($data1,$data[$i][9]);
		}
		$this->session->set_flashdata('msg', ' Add Transsaction successfully... ');
		$this->session->set_flashdata('CetakClossing', '1');
	}

	function Checkout($idRsv){
		date_default_timezone_set('Asia/Hong_Kong');

			
		$data['master'] = $this->M_fo_outlet->checkout_master($idRsv);
		//print_r($data['master']);exit();
		$this->load->view('fo_outlet/Checkout',$data);

	}

	function Cetak($JenisLaporan,$Text){
		$data['hotel'] = $this->M_rbac->getPerusahaan();
		$data['qr'] = $this->ciqrcode->generateQRcode($JenisLaporan, $Text,1.9);
		$data1= $this->load->view('template/Cetak_head',$data, TRUE);
		return $data1;
	}
	function Cetak_home(){
		$data['operator'] = $this->M_fo_outlet->Operator_view(); 
		$data['guest'] = $this->M_fo_outlet->get_guest_bill(); 
		$this->load->view('fo_outlet/cetak/Home',$data);
	}


	function Cetak_closing(){
		// $data['header'] = $this->Cetak("Operator_closing_fo",  $this->session->userdata('Nama_userPms'). date("d-m-Y_H-i-s"), 1);
		$idmaster = $this->input->get('idmaster', TRUE);
		$data['dataclosing'] = $this->M_fo_outlet->Op_closing_data_cetak(date('Y-m-d:H')); 
		$html= $this->load->view('fo_outlet/cetak/Operator_closing',$data,TRUE);
		print_r($html);exit();
		// $this->create_pdf->load($html,'Operator_closing_fo'.' '.$this->Operator, 'A4-L','');	
	}
	function Cetak_closing_bytgl(){
	  // $data['header'] = $this->Cetak("Operator_closing_fo",  $this->session->userdata('Nama_userPms'). date("d-m-Y_H-i-s"), 1);
	  $kode = $this->input->get('kode', TRUE); 
	  $tgljam = $this->M_fo_outlet->Select_jam_closing($kode);
	  $data['dataclosing'] = $this->M_fo_outlet->Op_closing_data_cetak($tgljam); 
	  $html= $this->load->view('fo_outlet/cetak/Operator_closing',$data,TRUE);
	   print_r($html);exit();
	  // $this->create_pdf->load($html,'Operator_closing_fo'.' '.$this->Operator, 'A4-L','');	
	}
	function CetakRR_byTgl(){
	   $TglAwal = $this->input->get('TglAwal', TRUE);
	   $TglAkhir = $this->input->get('TglAkhir', TRUE);

	   $TglAwal1 = DateTime::createFromFormat('m/d/Y',$TglAwal);
	   $TglAwal2 = $TglAwal1->format("Y-m-d");
	   $TglAwal3 = $TglAwal1->format("d-m-Y");
	   $TglAkhir1 = DateTime::createFromFormat('m/d/Y',$TglAkhir);
	   $TglAkhir2 = $TglAkhir1->format("Y-m-d");
	    $TglAkhir3 = $TglAkhir1->format("d-m-Y");

	   $data['header'] = $this->Cetak("Memorandum_invoice",  $this->session->userdata('Nama_userPms'). date("d-m-Y_H-i-s"), 1);
	   $data['konten'] = $this->M_fo_outlet->CetakRR_byTgl($TglAwal2, $TglAkhir2);
	   $data['periode'] =  array('TglAwal' => $TglAwal3 , 'TglAkhir' => $TglAkhir3);
	   $html=$this->load->view('fo_outlet/cetak/Room_revenue',$data, TRUE);
	  // print_r($html);exit();
	  $this->create_pdf->load($html,'Memorandum_invoice'.' '.$TglAwal3.' '.'-'.' '.$TglAkhir3, 'A4-L','');
		
		
	}
	
	function CetakBill_guest(){
	  $idbill = $this->input->get('idbill', TRUE);
      $jenisbill = $this->input->get('jenisbill', TRUE);
      $guest = $this->input->get('guest', TRUE);   	

	  $datarsv= $this->M_fo_outlet->get_idrsv_by_checkout($idbill);

	   $data['header'] = $this->Cetak("master_Bill",  $this->session->userdata('Nama_userPms'). date("d-m-Y_H-i-s"), 1);
	    $data['jenisbill'] =  $jenisbill;
	   $data['room'] = $this->M_fo_outlet->get_masterbill_cetak($idbill,'1');
	   $data['comsume'] = $this->M_fo_outlet->get_masterbill_cetak($idbill,'2');
	    $data['summary'] = $this->M_fo_outlet->get_masterbill_cetak($idbill,'3');
	    $data['master'] = $this->M_fo_outlet->checkout_master($datarsv->idreservation);

	    //print_r($data['room']);exit();

	   $data['periode'] =  array('TglAwal' => $TglAwal3 , 'TglAkhir' => $TglAkhir3);
	   $html=$this->load->view('fo_outlet/cetak/Bill_all',$data, TRUE);
	   print_r($html);exit();
	  $this->create_pdf->load($html,'master_Bill'.' '.$TglAwal3.' '.'-'.' '.$TglAkhir3, 'A4-L','');
		
		
	}


		function CetakBill_guest_checkout(){
	  $idreservation = $this->input->get('idreservation', TRUE);
      $jenisbill = $this->input->get('jenisbill', TRUE);

	  $datarsv= $this->M_fo_outlet->get_idrsv_by_checkout($idbill);

	   // $data['header'] = $this->Cetak("master_Bill",  $this->session->userdata('Nama_userPms'). date("d-m-Y_H-i-s"), 1);
	    $data['jenisbill'] =  $jenisbill;

	    if ($jenisbill=='1') {
	    	$data['Dataroom']=$this->M_fo_outlet->get_roombill($idreservation); 
	    }elseif ($jenisbill=='2') {
	    	$data['Datacomsume']=$this->M_fo_outlet->get_comsumebill_dee($idreservation);
	    }else{
	    	$data['Dataroom']=$this->M_fo_outlet->get_roombill($idreservation); 
	    	$data['Datacomsume']=$this->M_fo_outlet->get_comsumebill_dee($idreservation);
	    }
 $data['master'] = $this->M_fo_outlet->checkout_master($idreservation);

	   $data['periode'] =  array('jenisbill' => $jenisbill);
	   $html=$this->load->view('fo_outlet/cetak/Bill_checkout',$data, TRUE);
	  print_r($html);exit();
	  // $this->create_pdf->load($html,'Bill'.'-'.$idreservation, 'A4-L','');
		
		
	}

	function Select_clossing_byop(){
   	$IdOperator = $this->input->post('idop', TRUE);
 	 $date = $this->M_fo_outlet->Select_clossing_byop($IdOperator);
 	//print_r($date);exit();
		    echo ' <div class="form-group" id="groupSubcategory">';
		    echo ' <label>Date Clossing</label> ';
		    echo  '<select id="DateClosing" class="form-control select2"  style="width: 100%;"  ';
		    foreach ($date as $pek ) {
		                    echo ' <option value="'.$pek->kode.'" >'.$pek->tgl.'</option>';
		    }
		    echo '</select>';
		    echo '</div>';

   }

   function select_checkout_bill(){
   	$idguest = $this->input->post('idguest', TRUE);
 	 $date = $this->M_fo_outlet->get_checkout_bill($idguest);
 	 //print_r($date);
		    echo ' <div class="form-group" id="groupBill">';
		    echo ' <label>Date Checkout</label> ';
		    echo  '<select id="DateBill" class="form-control bill"  style="width: 100%;"  ';
		    echo ' <option value="-" >--Select Date--</option>';
		    foreach ($date as $pek ) {
		                    echo ' <option value="'.$pek->id.'" >'.$pek->tanggal.'</option>';
		    }
		    echo '</select>';
		    echo '</div>';

   }

   


	

	
	


	


}
?>

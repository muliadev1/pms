<?php 

class Purchase_requisition extends CI_Controller{
	private $IdOperator;

	function __construct(){
		parent::__construct();
		if($this->session->userdata('Kode_userPms')==NULL) {
           redirect('Login');
		}
		$this->load->library(array('session','create_pdf','ciqrcode'));
		$this->load->model(array('M_purchase_requisition','M_rbac'));
		$this->IdOperator = $this->session->userdata('Kode_userPms');
		
		if (!empty($this->session->userdata("active_db"))) {
			$this->load->database($this->session->userdata("active_db"), FALSE, TRUE);
		}
	}
 
	function index(){
		$this->load->view('dashboard1');
	}

	function purchase_requisition_add(){
		$data['data'] = $this->M_purchase_requisition->item_view();
		$data['departement'] = $this->M_purchase_requisition->load_departement();
		$this->load->view('purchase_requisition/Purchase_requisition',$data);
	}
	function purchase_requisition_view(){
		$data['data'] = $this->M_purchase_requisition->pr_view();
		$this->load->view('purchase_requisition/Purchase_requisition_list',$data);
	}
	function purchase_requisition_view_detail(){
		$id = $this->input->post('id');
		$data['data'] = $this->M_purchase_requisition->pr_view_detil($id);
		//print_r($id);exit();
		$this->load->view('purchase_requisition/Purchase_requisition_detail',$data);
	}
	function cetakpemesanan(){
	    $data['header'] = $this->Cetak("Purchase_order",  $this->session->userdata('Nama_userPms'). date("d-m-Y_H-i-s"), 1);
		$data['data'] = $this->M_purchase_requisition->pr_view();
	    $html=$this->load->view('purchase_order/Cetak/PO',$data, TRUE);
	    //print_r($html);exit();
     	$this->create_pdf->load($html,'Purchase_order'.'-'.$data['kontenPO'][0]->kodepo, 'A4-P');	
	}
	function add_pr(){

		$itemcode = $this->input->post('itemcode');
		$Qty = $this->input->post('Qty');
		$Description = $this->input->post('Description');
		$Note = $this->input->post('NotePr');
		$Unit = $this->input->post('Unit');
		$IdDepartement = $this->input->post('IdDepartement'); 
		$NamaDepartemen = $this->input->post('NamaDepartemen');
		$Price = $this->input->post('Price');
		$Stok = $this->input->post('Stok');
		$kode =$this->M_purchase_requisition->get_kodepr('tbpr');
		$NotePr = $this->input->post('Note');
		//print_r($Note);exit();

		$itemcode1 = array_combine(range(1, count($itemcode)), array_values($itemcode));
		$Price1 = array_combine(range(1, count($Price)), array_values($Price));
		$Qty1 = array_combine(range(1, count($Qty)), array_values($Qty));
		$Description1 = array_combine(range(1, count($Description)), array_values($Description));
		$Unit1 = array_combine(range(1, count($Unit)), array_values($Unit));
		$Stok1 = array_combine(range(1, count($Stok)), array_values($Stok));
		$NotePr1 = array_combine(range(1, count($NotePr)), array_values($NotePr));


			$data = array('kodepr' => $kode,
	 				 		'iddepartement' => $IdDepartement,
	 				 		'departement' => $NamaDepartemen,
	 						'dateadd' => date('Y-m-d H:i:s'),
	 						'idoperator' => $this->IdOperator,
	 						'description' => $Note	
	 		 			);

		
				$Jumlah = count($Stok);
				for ($i=1; $i <= $Jumlah ; $i++) { 
				 	 $dataDetil[] = array('kodepr' => $kode,
						 				 'iddepartement' => $IdDepartement,
						 				 'departement' => $NamaDepartemen,
						 				'codeitem' => $itemcode1[$i],
						 				'price' => $Price1[$i],
						 				'qtyorder' => $Qty1[$i],
						 				'description' => $Description1[$i],
						 				'unit' => $Unit1[$i],
						 				'currstok' => $Stok1[$i],
						 				'note' => $NotePr1[$i],
						 				'idoperator' => $this->IdOperator
						 		 );
				 } 	
		//print_r($dataDetil);exit();
		$this->M_purchase_requisition->Pr_add($data,$dataDetil);
		$this->session->set_flashdata('msg', ' Add Purchase Requisition successfully... ');
		$this->session->set_flashdata('kodepr', $kode);
		redirect('purchase_requisition/purchase_requisition_view');
	}

	function daftar_item(){
		$Departement = $this->input->post('departement'); 
	   if ($Departement==1) { 
        $set = 'stokBO';
       }elseif ($Departement==2) {
        $set = 'stokHK';
       }elseif ($Departement==3) {
         $set = 'stokFO';
       }elseif ($Departement==4) {
          $set = 'stokFB';
       }elseif ($Departement==5) {
        $set = 'stokBO';
       }elseif ($Departement==6) {
        $set = 'stokBar';
       }else{
         $set = 'stok';
       }
      // print_r($Departement );exit();
       	$data['set'] = array('set' => $Departement );
		$data['data'] = $this->M_purchase_requisition->item_view();
		$this->load->view('purchase_requisition/Daftar_item',$data);
	}

	function daftar_pr(){
		$data['data'] = $this->M_purchase_requisition->Load_PR_all();
		$this->load->view('purchase_requisition/Cetak/Daftar_pr',$data);
	}

	function Cetak($JenisLaporan,$Text){
		$data['hotel'] = $this->M_rbac->getPerusahaan();
		$data['qr'] = $this->ciqrcode->generateQRcode($JenisLaporan, $Text,1.9);
		$data1= $this->load->view('template/Cetak_head',$data, TRUE);
		return $data1;
	}

	function CetakPR(){
		$KodePR = $this->input->get('KodePR', TRUE);
	    $data['header'] = $this->Cetak("Payment_voucher",  $this->session->userdata('Nama_userPms'). date("d-m-Y_H-i-s"), 1);
	   $data['kontenPr'] = $this->M_purchase_requisition->Load_PR_bycode($KodePR);
	    $html=$this->load->view('purchase_requisition/Cetak/PR',$data, TRUE);
     	$this->create_pdf->load($html,'purchase_requisition'.'-'.$data['kontenPr'][0]->kodepr, 'A4-L');	
	}


}


 <style type="text/css">
    @media print {
       body {
        -webkit-print-color-adjust: exact;
      }
      #warna{
        -webkit-print-color-adjust: exact;
      }
      @page {
        size: 300px 442px; 
        margin:0px;
        }
    }
  </style>

 
  
  <body onload="printDiv('printableArea')" id="printableArea">
        <table  style="margin: auto;" border="0" width="100%" align="center">
          <tbody>
            <tr>
              <th  style="padding-right: 10px;" width="10%" rowspan="4" ><img height="30px" src="<?php echo base_url().'assets/img-sistem/logo-laporan.png'; ?>"> </th> 
              <th width="60%" align="center" style="font-size:16px" ><strong>RESTAURANT</strong> <br> 
                <font style="font-size: 15px;"></font> SHANTI </th>
              
         </tr>
            <tr>
             <td align="center" style="font-size:10px;"> Phone : 288060-289146 </td>
            </tr>
            <tr>
              <td align="center" style="font-size:10px;"> </td>
            </tr>
            <tr>
              <td align="center" style="font-size:10px;"> </td>
            </tr>
            <tr>
              <td colspan="3" > <hr style="color: '#000066'; ">  </td>
            </tr>
          </tbody>
          </table>  


           <table  border="0" style="border-collapse: collapse; border-color:#000000; font-size: 9px;" width="100%" align="center">
            <tr>
              <th align="center" colspan="2">BILL</th>
            </tr>
             <tr>
              <th align="center" colspan="2">&nbsp; </th>
            </tr>
             <tr>
              <th align="left">ORDER NO</th>
              <th align="left">: <?php echo $detilmenu[0]->trxid ?></th>
            </tr>
            <tr>
              <th align="left">DATE</th>
              <th align="left">: <?php echo $mastermenu->tanggaltrx ?></th>
            </tr>
            <tr>
              <th align="left">ROOM NO</th>
              <th align="left">: <?php echo $mastermenu->roomname ?></th>
            </tr>
            <tr>
              <th align="left">PERSONS</th>
              <th align="left">: <?php echo $mastermenu->nama ?></th>
            </tr>
            <tr>
              <th align="left">TABLE NO</th>
              <th align="left">: <?php echo $mastermenu->tablenumber ?></th>
            </tr>
            <tr>
              <th align="left">WAITER NO</th>
              <th align="left">: <?php echo $detilmenu[0]->idwaiter ?></th>
            </tr>
             <tr>
               <td align="center" colspan="2"> <hr style="color: '#000000'; "></td>
            </tr>
           </table>

           <table  border="0" style="border-collapse: collapse; border-color:#000000; font-size: 8px;" width="100%" align="center">
            <tr>
              <td> <strong> FOOD</strong></td>
              <td>&nbsp;</td>
            </tr>
        <?php $Subtotal = 0; $Disc=0; $dataFood = $this->M_resto->Get_trx_menu_detail_category_cetak_bill1($detilmenu[0]->trxid,'FOOD') ?>
            <?php foreach ($dataFood as $food ){ ?>
              <tr>
                <td><?php echo $food->menu; ?></td>
                <td align="right"><?php echo number_format($food->qty, 2, '.', '.'); ?></td>
                 <td align="right"><?php echo number_format($food->subtotal, 2, '.', '.'); ?></td>
              </tr>
            <?php $Subtotal+=$food->subtotal; $Disc+= $food->disc; } ?>

            <tr>
              <td><strong>BEVERAGE</strong></td>
              <td>&nbsp;</td>
            </tr>
             <?php $dataBev = $this->M_resto->Get_trx_menu_detail_category_cetak_bill1($detilmenu[0]->trxid,'BEVERAGE') ?>
            <?php foreach ($dataBev as $bev ){ ?>
              <tr>
                <td><?php echo $bev->menu; ?></td>
                <td align="right"><?php echo number_format($bev->qty, 2, '.', '.'); ?></td>
                 <td align="right"><?php echo number_format($bev->subtotal, 2, '.', '.'); ?></td>
              </tr>
            <?php $Subtotal+=$food->subtotal; $Disc+= $food->disc;} ?>
          <tr>
            <tr>
              <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
              <td  colspan="3"> <hr style="color: '#000066'; "></td>
            </tr>
           </table>
           <?php  $this->M_resto->update_splitbill1_temp();
             $Tax = 10/100*($Subtotal-$Disc);
             $Service= ($Tax+$Subtotal)*10/100 ;
             $AfterDisc =$Subtotal-$Disc; ?>


            <table  border="0" style="border-collapse: collapse; border-color:#000000; font-size: 9px;" width="100%" align="center">
            <tr>
              <td align="left">SUBTOTAL*</td>
              <td align="right"><?php echo number_format($Subtotal, 2, '.', '.') ?></td>
            </tr>
             <tr>
              <td align="left">DISCOUNT</td>
              <td align="right"><?php echo number_format($Disc, 2, '.', '.') ?></td>
            </tr>
             <tr>
              <td align="left">SUBTOTAL</td>
              <td align="right"><?php echo number_format($AfterDisc, 2, '.', '.') ?></td>
            </tr>
            <tr>
              <td align="center" colspan="2"> <hr style="color: '#000000'; "></td>
            </tr>
            <tr>
              <td align="left">TAX</td>
              <td align="right"><?php echo number_format($Tax, 2, '.', '.') ?></td>
            </tr>
            <tr>
              <td align="left">SERVICE</td>
              <td align="right"><?php echo number_format($Service, 2, '.', '.') ?></td>
            </tr>
             <tr>
              <td align="left">GRANDTOTAL</td>
              <td align="right"><?php echo number_format($Tax+$Service+$AfterDisc, 2, '.', '.') ?></td>
            </tr>
             <tr>
              <td  colspan="2"> <hr style="color: '#000066'; "></td>
            </tr>
             <tr>
              <th align="center" colspan="2">&nbsp; </th>
            </tr>
             <tr>
              <th align="center" colspan="2">&nbsp; </th>
            </tr>

          </table>


            <table  border="0" style="border-collapse: collapse; border-color:#000000; font-size: 9px;" width="100%" align="center">
            <tr>
              <th align="left">NO BILL</th>
              <th align="left">CASHIER SIGNATURE</th>
            </tr>
             <tr>
            <tr>
              <th colspan="2">&nbsp;</th>
            </tr>
            <tr>
              <th colspan="2">&nbsp;</th>
            </tr>
            <tr>
              <th colspan="2">&nbsp;</th>
            </tr>
            <tr>
              <th align="left">-------------------------------</th>
              <th align="left">--------------------------------</th>
            </tr>
            
           </table>



    
</body>


<script>
function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}
</script>

 





    $(document).ready(function () {
        $('#tblSample').each(function () {
            var Column_number_to_Merge = 2;
             var Column_number_to_Merge1 = 3;
 
            // Previous_TD holds the first instance of same td. Initially first TD=null.
            var Previous_TD = null;
            var Previous_TD1 = null;
            var i = 1;
            var x = 1;
            $("tbody",this).find('tr').each(function () {
                // find the correct td of the correct column
                // we are considering the table column 1, You can apply on any table column
                var Current_td = $(this).find('td:nth-child(' + Column_number_to_Merge + ')');
                var Current_td1 = $(this).find('td:nth-child(' + Column_number_to_Merge1 + ')');
                 
                if (Previous_TD == null) {
                    // for first row
                    Previous_TD = Current_td;
                    i = 1;
                } 
                else if (Current_td.text() == Previous_TD.text()) {
                    // the current td is identical to the previous row td
                    // remove the current td
                    Current_td.remove();
                    // increment the rowspan attribute of the first row td instance
                    Previous_TD.attr('rowspan', i + 1);
                    i = i + 1;
                } 
                else {
                    // means new value found in current td. So initialize counter variable i
                    Previous_TD = Current_td;
                    i = 1;
                }


                if (Previous_TD1 == null) {
                    // for first row
                    Previous_TD1 = Current_td1;
                    x = 1;
                } 
                else if (Current_td1.text() == Previous_TD1.text()) {
                    // the current td is identical to the previous row td
                    // remove the current td
                    Current_td1.remove();
                    // increment the rowspan attribute of the first row td instance
                    Previous_TD1.attr('rowspan', x + 1);
                    x = i + 1;
                } 
                else {
                    // means new value found in current td. So initialize counter variable i
                    Previous_TD1 = Current_td1;
                    x = 1;
                }



            });
        });
    });

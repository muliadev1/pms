<?php
$this->load->view('template/Head');
$this->load->view('template/Css');
$this->load->view('template/Topbar');
$this->load->view('template/Sidebar');
?>

<!-- Content Header (Page header) -->
<section class="content-header">
   <div class="btn-group btn-breadcrumb">
            <a href="#" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-home"></i></a>
             <a href="<?php echo base_url('index.php/Akun/DataAkun');?>" class="btn btn-default  btn-xs">Data Akun</a>
        </div>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-xs-12">
     
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Data Akun</h3>
          <a style="float:right"  href="<?php echo base_url('index.php/Akun/AddAkun');?>" class="btn btn-primary">
            <i class="glyphicon glyphicon-saved"></i>
            Tambah
          </a>
        </div>
    <!-- /.box-header -->
        <div class="box-body">
        <div class="table-responsive">
          <table class="table table-bordered table-hover" id="example1">
                                <thead>
                                    <tr>
                                        <th>Kode Akun</th>
                                        <th>Subklasifikasi</th>
                                        <th>Nama Akun</th>
                                        <th></th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    foreach($data as $u){
                                      $no=0;
                                      $no+=1;
                                    ?>
                                    <tr  class='odd gradeX context'>
                                        <td><?php echo $u->NoAkun ?></td>
                                        <td><?php echo $u->NamaSub ?></td>
                                         <td><?php echo $u->NamaAkun ?></td>
                                         <td><a class="btn btn-warning btn-xs" title="Edit Data"  href="<?php echo base_url('index.php/Akun/EditAkun/'.$u->NoAkun); ?>">  <span class="fa fa-fw fa-edit" ></span> </a></td>
                                          



                                       
                                        
                                        
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
          </div>
          
          
          
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->

</section><!-- /.content -->


<?php
$this->load->view('template/Js');
?>


<script>
function doconfirm()
{
    var nama = $('#btnhapus').val();
    job=confirm("Apakah Anda Yakin Menonaktifkan Nasabah "  + nama + " ?" );
    if(job!=true)
    {
        return false;
    }
}
</script>

<script>
function doconfirmaktif()
{
    var nama = $('#btnaktif').val();
    job=confirm("Apakah Anda Yakin Mengaktifkan Nasabah "  + nama + " ?" );
    if(job!=true)
    {
        return false;
    }
}
</script>

<script>
function doconfirmAdd()
{
    var nama = $('#btnaddAG').val();
    job=confirm("Apakah Anda Yakin Menambahkan Nasabah "  + nama + " Sebagai Anggota ?" );
    if(job!=true)
    {
        return false;
    }
}
</script>

<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>

<?php
$this->load->view('template/Foot');
?>